(function() {
	'use strict';
	
	angular
	.module 	('catnetCrmTips', ['ngSanitize', 'cd.resizer'])
	.controller ('Cindex' 		, Cindex 			)
	.filter 	('HtmlSanitize'	, FilterHtmlSanitize)
	.filter  	('orderObjectBy', orderObjectBy 	);

	function orderObjectBy() {
		return function(items, field, reverse) {
			var filtered = [];
			angular.forEach(items, function(item) {
				filtered.push(item);
			});
			filtered.sort(function (a, b) {
				return (a[field] > b[field] ? 1 : -1);
			});
			if(reverse) filtered.reverse();
			return filtered;
		};
	}
	
	FilterHtmlSanitize.$inject = ['$sce'];
	function FilterHtmlSanitize($sce) {
		return function (value) {
			return $sce.trustAsHtml(value);
		};
	}

	Cindex.$inject = ['$scope', '$timeout', '$http'];
	function Cindex($scope, $timeout, $http) {
		var maxCrmWidth = (((document.documentElement.clientWidth || window.innerWidth) * 0.80) - 7.5);
		angular.element("#resizer-vertical").attr("resizer-max-width", maxCrmWidth);

		$scope.tips 		= {};
		$scope.addTip 		= addTip;
		$scope.removeTip 	= removeTip;
		$scope.feedback 	= feedback;


		$('#catnet-frame').on('load', function (e) {
			var path = $(this).contents()[0].location.pathname;
			path ==  '/'+FOLDERADD+'/' ? $(this).contents()[0].location.href = '/'+FOLDERADD+'/home/indexnocrm': false;
		});

		$http.get('/'+FOLDERADD+'/tips/gettips').success(getTipsSuccess);


		function getTipsSuccess(data) {
			if (data.status == 'ok') {
				for(var i = 0; i < data.tips.length; i++){
					$scope.addTip(data.tips[i], false);
				}
			}
		}

		function addTip(tip, apply) {
			$scope.tips['tip-'+tip.tipId] = tip;
			apply != false ? $scope.$apply() : false;
			$timeout(refreshCarrousel);
		}

		function removeTip(tipId, apply) {
			$scope.tips['tip-'+tipId] ? delete $scope.tips['tip-'+tipId] : false;
			apply != false ? $scope.$apply() : false;
			$timeout(refreshCarrousel);
		}

		function feedback(tipId, feedback) {
			$scope.tips['tip-'+tipId].feedback = 'FALSE';
			$http
			.post('/'+FOLDERADD+'/tips/feedback', $.param({'tipId':tipId, 'feedback': feedback}), {headers : {'Content-Type' : 'application/x-www-form-urlencoded'}})
			.success(function (data) {
				if (data.status == 'invalid') {
					$scope.tips['tip-'+tipId].feedback = 'TRUE';
				}
			});
		}

		function refreshCarrousel() {
			$('.tips .carousel-inner .item').removeClass('active').first().addClass('active');
		}
	}
})();