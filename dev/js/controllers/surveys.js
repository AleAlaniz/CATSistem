(function() {
	'use strict';

	angular
	.module 	('catnet'								)
	.config 	(SurveyConfig							)
	.factory	('survey_message'	,survey_message		)
	.controller	('Csurvey'			,Csurvey 			)
	.controller	('Csurvey_report'	,Csurvey_report		)
	.controller	('Csurvey_details'	,Csurvey_details	)
	.controller	('Csurvey_myanswers',Csurvey_myanswers	)
	.controller	('Csurvey_create'	,Csurvey_create		)
	.controller	('Csurvey_edit'	    ,Csurvey_edit		);



	SurveyConfig.$inject = ['$routeProvider'];
	function SurveyConfig($routeProvider) {
		$routeProvider.
		when('/surveys', {
			templateUrl: '/'+FOLDERADD+'/surveys',
			controller: 'Csurvey'
		})
		.when('/surveys/report/:id',{
			templateUrl: '/'+FOLDERADD+'/surveys/report',
			controller : 'Csurvey_report'
		})
		.when('/surveys/details/:id',{
			templateUrl: '/'+FOLDERADD+'/surveys/details',
			controller : 'Csurvey_details'
		})
		.when('/surveys/create', {
			templateUrl: '/'+FOLDERADD+'/surveys/create',
			controller: 'Csurvey_create'
		})
		.when('/surveys/edit/:surveyId', {
			templateUrl: '/'+FOLDERADD+'/surveys/edit',
			controller: 'Csurvey_edit'
		})
		.when('/surveys/myanswers/:id',{
			templateUrl: '/'+FOLDERADD+'/surveys/myanswers',
			controller : 'Csurvey_myanswers'
		})
	}





	survey_message.$inject = ['$rootScope'];
	function survey_message($rootScope) {
		var data = {}
		data.message = '';
		data.setMessage = function(message, auto_update) {
			this.message = message;
			if (auto_update) {
				$rootScope.$broadcast('survey_message');
			}
		}
		return data;
	}

	Csurvey.$inject = ['$scope','nav', '$http', 'survey_message'];
	function Csurvey($scope, nav, $http, survey_message)
	{
		nav.setSelected('surveys');
		
		$scope.surveys = [];
		$scope.loading = true;
		$scope.message = survey_message.message;

		$scope.initCtrl = initCtrl;
		
		$scope.$on('survey_message', function () {
			$scope.message = survey_message.message;
		});
		
		$scope.$on("$destroy", function(){
			survey_message.setMessage('', false);
		});

		function initCtrl() {
			$scope.loading = true;

			$http
			.get('/'+FOLDERADD+'/surveys/getsurveys')
			.success(function(data) {
				if (data.status == 'ok') {
					$scope.surveys = data.surveys;
				}
				$scope.loading = false;
			});
		};

		$scope.initCtrl();

		$('#confirm-delete').on('show.bs.modal', function (event) {
			var button 	= $(event.relatedTarget) 
			var surveyId= button.data('survey')
			var title 	= button.data('title')
			var modal 	= $(this)
			modal.find('[role=delete]').show();
			modal.find('[role=deleteLoading]').hide();

			modal.find('[role=delete]').attr('datas-url', '/'+FOLDERADD+'/surveys/delete/'+surveyId);
			modal.find('[role="survey-title"]').text('"'+title+'"');
		})


		$('[role="delete"]').click(function(e) {
			var btn = $(this);
			$('[role=delete]').hide();
			$('[role=deleteLoading]').show();

			$http
			.get(btn.attr('datas-url'))
			.success(function(data) {
				if (data.status == 'ok') {
					$('#confirm-delete').modal('hide');
					survey_message.setMessage(data.message, true);
					$scope.initCtrl();

				}
			});
		});


		$('#confirm-activate').on('show.bs.modal', function (event) {
			var button 	= $(event.relatedTarget) 
			var surveyId= button.data('survey')
			var modal 	= $(this)
			modal.find('[role=activate]').show();
			modal.find('[role=activateLoading]').hide();

			modal.find('[role=activate]').attr('datas-url', '/'+FOLDERADD+'/surveys/activate/'+surveyId);
		})



		$('[role="activate"]').click(function(e) {
			var btn = $(this);
			$('[role=activate]').hide();
			$('[role=activateLoading]').show();

			$http
			.get(btn.attr('datas-url'))
			.success(function(data)
			{
				if (data.status == 'ok') 
				{
					$('#confirm-activate').modal('hide');
					survey_message.setMessage(data.message, true);
					$scope.initCtrl();
				}

			});
		});
	}


	Csurvey_report.$inject = ['$scope','nav', '$http', '$routeParams'];
	function Csurvey_report($scope, nav, $http, $routeParams)
	{
		nav.setSelected('surveys');

		$scope.survey 		= null;
		$scope.loading 		= true;
		$scope.colors 		= ["#af94e0", "#ffc60a", "#7591FF", "#F886A2", "#2E3E4E", "#eeff41", "#64ffda", "#448aff", "#9e9e9e", "#78909c", "#ff5722", "#4caf50"];
		$scope.colorsDark 	= ["#a180da", "#efb700", "#5b7dff", "#f66e90", "#2a2b3c", "#c6ff00", "#1de9b6", "#00b0ff", "#757575", "#546e7a", "#f4511e", "#388e3c"];

		$scope.initCtrl 	= initCtrl;


		function initCtrl() {
			$scope.loading = true;

			$http
			.get('/'+FOLDERADD+'/surveys/getsurveyreport/'+$routeParams.id)
			.success(function(data) {
				if (data.status == 'ok') {
					$scope.survey = data.survey;
					setTimeout(createAllCharts, 100);
				}
				$scope.loading = false;
			});
		};

		var expandCanvas = true;
		if(navigator.appName == "Microsoft Internet Explorer"){
			var ie = navigator.userAgent.toLowerCase();
			var version = parseInt(ie.split('msie')[1]);
			if (version <= 8) {
				expandCanvas = false;
			}
		}

		function createAllCharts() {
			if (expandCanvas) {

				jQuery.each($('canvas'), function (i, canvas) {
					canvas = $(canvas);
					canvas.width(canvas.parent().width());
				});
			}
			function createPieChart(questionId, data) {
				var options = {
					segmentShowStroke : true,
					segmentStrokeColor : "#E7E7E7",
					segmentStrokeWidth : 1,
					responsive: false,
					tooltipTemplate: "<%if (label){%><%=label%><%}%>",
				};

				var ctx = $("[question="+questionId+"]").get(0).getContext("2d");
				var chart = new Chart(ctx).Pie(data, options);


				$("[legend="+questionId+"]").html(chart.generateLegend());
			}
			jQuery.each($scope.survey.questions , function (key, question) {
				if (question.type == 1 || question.type == 2) {
					var data 						= [];

					jQuery.each(question.answers , function (keyA, answer) {

						var tempData			= {};
						tempData.label 			= answer.answer;
						tempData.value 			= answer.responses.length;
						tempData.color 			= ($scope.colors[keyA]) ? $scope.colors[keyA] : $scope.colors[$scope.colors.length - 1];
						tempData.highlight 		= ($scope.colorsDark[keyA]) ? $scope.colorsDark[keyA] : $scope.colorsDark[$scope.colorsDark.length - 1];

						data.push(tempData);
					});
					createPieChart(question.surveyquestionId, data);
				}
				else if (question.type == 4) {
					var data 					= [];

					var tempData 			= {};
					tempData.label 			= $scope.trueLang;
					tempData.value 			= question.answers['true'].length;
					tempData.color 			= '#0cc2aa';
					tempData.highlight 		= '#0bb69f';
					data[0]					= tempData;

					tempData				= {};
					tempData.label 			= $scope.falseLang;
					tempData.value 			= question.answers['false'].length;
					tempData.color 			= '#f55060';
					tempData.highlight 		= '#f3384a';
					data[1]					= tempData;

					createPieChart(question.surveyquestionId, data);

				}
			}); 

			$('[role="dataTable"]').DataTable( {
				language: {
					"sProcessing":     "Procesando...",
					"sLengthMenu":     "Mostrar _MENU_ usuarios",
					"sZeroRecords":    "No se encontraron resultados",
					"sEmptyTable":     "Ningún dato disponible en esta tabla",
					"sInfo":           "Mostrando usuarios del _START_ al _END_ de un total de _TOTAL_ usuarios",
					"sInfoEmpty":      "Mostrando usuarios del 0 al 0 de un total de 0 usuarios",
					"sInfoFiltered":   "(filtrado de un total de _MAX_ usuarios)",
					"sInfoPostFix":    "",
					"sSearch":         "Buscar:",
					"sUrl":            "",
					"sInfoThousands":  ",",
					"sLoadingRecords": "Cargando...",
					"oPaginate": {
						"sFirst":    "Primero",
						"sLast":     "Último",
						"sNext":     "Siguiente",
						"sPrevious": "Anterior"
					},
					"oAria": {
						"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
						"sSortDescending": ": Activar para ordenar la columna de manera descendente"
					}
				},
				"paging":   false,
				"info":     false
			} );


		}

		$scope.initCtrl();
	}

	Csurvey_details.$inject = ['$scope','nav', '$http', '$routeParams'];
	function Csurvey_details($scope, nav, $http, $routeParams)
	{
		nav.setSelected('surveys');

		$scope.survey 		= null;
		$scope.loading 		= true;

		$http
		.get('/'+FOLDERADD+'/surveys/getsurveydetails/'+$routeParams.id)
		.success(function(data) {
			if (data.status == 'ok') {
				$scope.survey = data.survey;
			}
			$scope.loading = false;
		});
	}

	Csurvey_create.$inject = ['$scope','nav', '$http', 'survey_message', '$location', 'tinyMCE', '$timeout'];
	function Csurvey_create($scope, nav, $http, survey_message, $location, tinyMCE, $timeout)
	{
		nav.setSelected('surveys');

		$('[role="date"]').datepicker({
			language	: 'es',
			autoclose	: true
		});

		if(typeof String.prototype.trim !== 'function') {
			String.prototype.trim = function() {
				return this.replace(/^\s+|\s+$/g, ''); 
			}
		}

		$scope.sending		= false;
		$scope.questions 	= [];

		$scope.addQuestion = function() {
			var newQuestion = {
				question 	: '',
				required 	: true,
				type 		: '1',
				description	: '',
				isTrue	    : 'TRUE',
				ind 		: $scope.questions.length,
				answers 	: [],
				classTag 		: 'animated bounceIn',
				remove : function(i) {
					if ($scope.questions.length > 1) 
					{
						$scope.questions.splice(i, 1);
					}
					else
					{
						this.classTag = "animated shake";
					}
				},
				addAnswer : function () {
					var newAnswer = {
						answer  	: '',
						isTrue 		: false,
						classTag 		: 'animated bounceIn',
						remove : function(i,j) {
							if ($scope.questions[i].answers.length > 1) 
							{
								$scope.questions[i].answers.splice(j, 1);
							}
							else
							{
								this.classTag = "animated shake";
							}
						}
					}

					this.answers.push(newAnswer);
					$timeout(function() {
						$('[data-toggle="tooltip"]').tooltip()
					});

				}
			};
			newQuestion.addAnswer();
			$scope.questions.push(newQuestion);
			$timeout(function() {
				$('[data-toggle="tooltip"]').tooltip();
				tinyMCE.init('input[name*="question"]');
			});
		}
		
		$scope.addQuestion();


		$scope.submitSurvey = function() {

			var questions = $('input[name*="question"]'),
			incomplete = false;
			jQuery.each(questions, function(i, input) {
				input = $(input);
				var name = input.attr('name');

				var value = input.prev().find('iframe').contents().find('#tinymce').html();
				if (value == '<p><br data-mce-bogus="1"></p>')
				{
					incomplete=true;
				}
				else
				{
					input.val(value);
					var type = input.parent().parent().parent().find("select[name*='type']").first().val();
					if (type == '1' || type == '2') {
						var answers = input.parent().parent().parent().find("input[name*='answer']");
						jQuery.each(answers, function(j, answer) {
							if ($(answer).val().trim() == "") {incomplete=true;}
						});
					}
				}
			});

			var haveTo = false;

			jQuery.each($('input[name="sections[]"]'), function(i, section) {
				if(section.checked)
				{
					haveTo = true;
					return;
				}
			});

			if (!haveTo) {
				haveTo = false;
				jQuery.each($('input[name="sites[]"]'), function(i, site) {
					if(site.checked)
					{
						haveTo = true;
						return;
					}
				});
				if (!haveTo) {
					haveTo = false;
					jQuery.each($('input[name="roles[]"]'), function(i, role) {
						if(role.checked)
						{
							haveTo = true;
							return;
						}
					});

					if (!haveTo) {
						haveTo = false;
						jQuery.each($('input[name="userGroups[]"]'), function(i, userGroup) {
							if(userGroup.checked)
							{
								haveTo = true;
								return;
							}
						});

						if (!haveTo) {
							haveTo = false;
							if ($('input[name="users[]"]').length <= 0) {
								incomplete = true;
							}
						}
					}

				}

			}
			if ($('input[name="name"]').val().trim() == "") {incomplete=true;}
			if(incomplete)
			{
				$('#no-complete').modal('show');
			}
			else
			{
				$scope.sending = true;
				$('#form-survey').ajaxSubmit({
					url : '/'+FOLDERADD+'/surveys/create',
					dataType : 'json',
					success: function(data) {
						if (data.status == 'ok') {
							survey_message.setMessage(data.message, false);
							$location.path('/surveys');
							$scope.$apply()

						}
						$scope.sending = false;
						$scope.$apply()
					}
				});

			}
		};


		$scope.users = [];

		$scope.removeUser = function(i) {
			$scope.users.splice(i, 1);
		};

		$scope.addUser = function(i) {
			$scope.users.push($scope.userOptions[i]);
			$scope.userOptions.splice(i, 1);
		};

		$scope.userOptions = [];

		$scope.findLike = '';


		$scope.searchNumber = 0;
		$scope.searchActual = 0;

		$scope.searchUsers = function() {
			if($scope.findLike != '') 
			{
				var thisSearch = $scope.searchNumber;
				$scope.searchNumber = $scope.searchNumber+1;
				$.ajax({
					method 	: "POST",
					url		: '/'+FOLDERADD+'/surveys/searchusers',
					data 	: {'like': $scope.findLike}
				}).done(function (data) {
					if(data == 'invalid'){

					}
					else if(data != 'empty' && thisSearch >= $scope.searchActual)
					{
						$scope.searchActual = thisSearch;
						$scope.$apply($scope.addUserOptions(data));
					}
					else if (thisSearch >= $scope.searchActual)
					{
						$scope.searchActual = thisSearch;
						$scope.$apply($scope.userOptions = []);
					}
				});
			}
			else
			{
				$scope.$apply($scope.userOptions = []);
			}
		};

		$scope.addUserOptions = function (users) {
			try{

				users = $.parseJSON(users);
			}catch(e){
				return;
			}

			$scope.userOptions = [];
			jQuery.each(users, function(i, user) {
				var exist = false;
				jQuery.each($scope.users, function(i, userExist) {
					if (userExist.userId == user.userId) {exist = true;}
				});
				if (!exist) {$scope.userOptions.push(user);}
			});


		};

		$scope.selectClick = function (e) {
			$('.select-input', e.currentTarget).focus();
		};
	}


	Csurvey_edit.$inject = ['$scope','nav', '$http', 'survey_message', '$location', 'tinyMCE','$routeParams', '$timeout'];
	function Csurvey_edit($scope, nav, $http, survey_message, $location, tinyMCE,$routeParams, $timeout)
	{
		nav.setSelected('surveys');

		$('[role="date"]').datepicker({
			language	: 'es',
			autoclose	: true
		});

		if(typeof String.prototype.trim !== 'function') {
			String.prototype.trim = function() {
				return this.replace(/^\s+|\s+$/g, ''); 
			}
		}


		$scope.sending		= false;
		$scope.questions 	= [];
		getData();

		$scope.addQuestion = function(question) 
		{
			var IsEmptyQuestion = !question;


			var existData=question || -1;
			if(existData == -1)
			{
				question=
				{
					question : '',
					type     : '1',
					required : true,
					isTrue   : 'TRUE',
					description: ''
				};
			}

			var newQuestion = {
				question 	: question.question,
				type 		: question.type,
				description	: question.description,
				required	: question.required,
				isTrue	    : question.isTrue,
				ind 		: $scope.questions.length,
				answers 	: [],
				classTag 		: 'animated bounceIn',
				remove : function(i) {
					if ($scope.questions.length > 1) 
					{
						$scope.questions.splice(i, 1);
					}
					else
					{
						this.classTag = "animated shake";
					}
				},
				addAnswer : function (answer)
				{
					existData= answer || -1;

					if(existData == -1)
					{
						answer=
						{
							answer:'',
							isTrue:false,
						};

					}

					var newAnswer = {
						answer  	: answer.answer,
						isTrue 		: answer.isTrue,
						classTag 		: 'animated bounceIn',
						remove : function(i,j) 
						{

							if ($scope.questions[i].answers.length > 1) 
							{
								$scope.questions[i].answers.splice(j, 1);
							}
							else
							{
								this.classTag = "animated shake";
							}
						}
					}

					this.answers.push(newAnswer);
					setTimeout(function() {
						$('[data-toggle="tooltip"]').tooltip()
					}, 10);
				}
			};

			if(existData == -1)
			{
				newQuestion.addAnswer();
			}
			else
			{
				jQuery.each(question.answers, function(i, answer)
				{
					answer.isTrue=(answer.isTrue == 'TRUE');
					newQuestion.addAnswer(answer);						
				});
			}

			$scope.questions.push(newQuestion);

			if (IsEmptyQuestion) {
				$timeout(function() {
					$('[data-toggle="tooltip"]').tooltip();
					/*tinymce.EditorManager.editors = [];*/ 
					tinyMCE.init('input[name*="question"]');
				});
			}
		}


		$scope.submitSurvey = function() {
			var questions = $('input[name*="question"]'),incomplete = false;

			jQuery.each(questions, function(i, input) {
				input = $(input);
				var name = input.attr('name');

				var value = input.parent().children().first().find('iframe').contents().find('#tinymce').html();

				if (value == '<p><br data-mce-bogus="1"></p>')
				{
					incomplete=true;
				}
				else
				{
					input.val(value);
					var type = input.parent().parent().parent().find("select[name*='type']").first().val();
					if (type == '1' || type == '2') {
						var answers = input.parent().parent().parent().find("input[name*='answer']");
						jQuery.each(answers, function(j, answer) {
							if ($(answer).val().trim() == "") {incomplete=true;}
						});
					}
				}
			});

			var haveTo = false;

			jQuery.each($('input[name="sections[]"]'), function(i, section) {
				if(section.checked)
				{
					haveTo = true;
					return;
				}
			});

			if (!haveTo) {
				haveTo = false;
				jQuery.each($('input[name="sites[]"]'), function(i, site) {
					if(site.checked)
					{
						haveTo = true;
						return;
					}
				});

				if (!haveTo) {
					haveTo = false;
					jQuery.each($('input[name="roles[]"]'), function(i, role) {
						if(role.checked)
						{
							haveTo = true;
							return;
						}
					});

					if (!haveTo) {
						haveTo = false;
						jQuery.each($('input[name="userGroups[]"]'), function(i, userGroup) {
							if(userGroup.checked)
							{
								haveTo = true;
								return;
							}
						});

						if (!haveTo) {
							haveTo = false;
							if ($('input[name="users[]"]').length <= 0) {
								incomplete = true;
							}
						}
					}

				}

			}
			if ($('input[name="name"]').val().trim() == "") {incomplete=true;}
			if(incomplete)
			{
				$('#no-complete').modal('show');
			}
			else
			{
				$scope.sending = true;
				$('#form-survey').ajaxSubmit({
					url : '/'+FOLDERADD+'/surveys/editSurvey',
					dataType : 'json',
					success: function(data) {
						if (data.status == 'ok') {

							survey_message.setMessage(data.message, false);
							$location.path('/surveys');
							$scope.$apply()
						}
						$scope.sending = false;
						$scope.$apply()
					}
				});

			}
		};


		$scope.users = [];

		$scope.removeUser = function(i) {
			$scope.users.splice(i, 1);
		};

		$scope.addUser = function(i) {
			$scope.users.push($scope.userOptions[i]);
			$scope.userOptions.splice(i, 1);
		};

		$scope.userOptions = [];

		$scope.findLike = '';


		$scope.searchNumber = 0;
		$scope.searchActual = 0;

		$scope.searchUsers = function() {
			if($scope.findLike != '') 
			{
				var thisSearch = $scope.searchNumber;
				$scope.searchNumber = $scope.searchNumber+1;
				$.ajax({
					method 	: "POST",
					url		: '/'+FOLDERADD+'/surveys/searchusers',
					data 	: {'like': $scope.findLike}
				}).done(function (data) {
					if(data == 'invalid'){

					}
					else if(data != 'empty' && thisSearch >= $scope.searchActual)
					{
						$scope.searchActual = thisSearch;
						$scope.$apply($scope.addUserOptions(data));
					}
					else if (thisSearch >= $scope.searchActual)
					{
						$scope.searchActual = thisSearch;
						$scope.$apply($scope.userOptions = []);
					}
				});
			}
			else
			{
				$scope.$apply($scope.userOptions = []);
			}
		};

		$scope.addUserOptions = function (users) {
			try{

				users = $.parseJSON(users);
			}catch(e){
				return;
			}

			$scope.userOptions = [];
			jQuery.each(users, function(i, user) {
				var exist = false;
				jQuery.each($scope.users, function(i, userExist) {
					if (userExist.userId == user.userId) {exist = true;}
				});
				if (!exist) {$scope.userOptions.push(user);}
			});


		};

		$scope.selectClick = function (e) {
			$('.select-input', e.currentTarget).focus();
		};

		$scope.survey=[];

		function getData()
		{
			$.ajax(
			{
				method:'POST',
				url:'/'+FOLDERADD+'/surveys/getSurveyData',
				data:{'surveyId':$routeParams.surveyId}
			})
			.done(function(res)
			{
				if(res != "error")
				{
					$scope.survey=jQuery.parseJSON(res);

					$scope.$apply(function()
					{
						$('#startDate').datepicker('update',$scope.survey.startDate);
						$('#endDate').datepicker('update',$scope.survey.endDate);

						$scope.required=($scope.survey.required == "TRUE");

						jQuery.each($scope.survey.users, function(i, user)
						{
							$scope.users.push({ completeName : user.name, userId : user.userId, userName : user.userName});
						});

						jQuery.each($scope.survey.questions, function(i, question)
						{
							question.required = question.required == "TRUE";
							$scope.addQuestion(question);						
						});

						$timeout(function() {
							tinyMCE.init('input[name*="question"]');
						});

					});
				}
				else
				{
					$location.path("#/");
					$scope.$apply();
				}
			});

		}
	}

	Csurvey_myanswers.$inject = ['$scope','nav', '$http', '$routeParams'];
	function Csurvey_myanswers($scope, nav, $http, $routeParams)
	{
		nav.setSelected('surveys');

		$scope.survey 		= null;
		$scope.loading 		= true;

		$http
		.get('/'+FOLDERADD+'/surveys/getsurveyanswers/'+$routeParams.id)
		.success(function(data) {
			if (data.status == 'ok') {
				$scope.survey = data.survey;
			}
			$scope.loading = false;
		});
	}
})();
