(function() {
	'use strict';
	angular
	.module('catnet')
	.config(['$routeProvider', function($routeProvider) {
		$routeProvider
		.when('/chat/unique', {
			templateUrl	: '/'+FOLDERADD+'/chat/unique',
			controller 	: 'Cchatu'
		})
		.when('/chat/unique/:id', {
			templateUrl	: '/'+FOLDERADD+'/chat/unique',
			controller 	: 'Cchatu'
		})
		.when('/chat/newunique', {
			templateUrl	: '/'+FOLDERADD+'/chat/newunique',
			controller 	: 'Cnewchatu'
		})
		;
	}])
	.controller ('Cchatu'		, Cchatu)
	.controller ('Cnewchatu'	, Cnewchatu)
	.factory 	('chatsu'		, FchatsU)
	.directive 	('schrollBottom', function () {
		return {
			scope: {
				schrollBottom: "="
			},
			link: function (scope, element) {
				scope.$watchCollection('schrollBottom', function (newValue) {
					if (newValue)
					{
						
						setTimeout(function() {
							$(element).scrollTop($(element)[0].scrollHeight - 20);
						}, 10);
						
					}
				});
			}
		}
	});

	Cnewchatu.$inject = ['$scope', 'nav', '$location', 'realTime', 'chatsu', 'chatsm'];
	function Cnewchatu($scope, nav, $location, realTime, chatsu, chatsm) {
		nav.setSelected('chat');
		$scope.message = '';

		$scope.submitChat 	= submitChat;
		$scope.unreadu 		= chatsu.unread;
		$scope.unreadm 		= chatsm.unread;

		$scope.$on("update_unread_chatu", function() {
			$scope.unreadu = chatsu.unread;
		});

		$scope.$on("update_unread_chatm", function() {
			$scope.unreadm = chatsm.unread;
		});

		function submitChat() {
			var incomplete = false;
			var haveTo = false;

			if ($('input[name="user"]').length <= 0 || $scope.message == '') {
				incomplete = true;
			}

			if(incomplete)
			{
				$('#no-complete').modal('show');
			}
			else
			{
				$('#chatu_create').attr('disabled', 'disabled');
				$('#chatu_create').html("<i class='fa fa-refresh fa-spin fa-lg fa-fw'></i>");
				var data = {
					message : $scope.message,
					user 	: $('input[name="user"]').val()
				};
				$.ajax({
					method 		: 'POST',
					url 		: '/'+FOLDERADD+'/chat/newunique',
					data 		: data,
					dataType 	: 'json'
				}).done(function(data) {
					if (data.status == 'ok') {
						var datas = {
							id 		: data.chat.chatuId,
							message : data.message,
							to 		: [data.chat.fromuserId, data.chat.touserId]
						};	
						realTime.socket.emit('chatu_message', datas);
						$location.path('/chat/unique/'+data.chat.chatuId);
						$scope.$apply()
					}
				})

			}
		}

		$scope.finding = false;
		$scope.users = [];
		$scope.removeUser = function(i) {
			$scope.users.splice(i, 1);
		};
		$scope.addUser = function(i) {
			$scope.users = [];
			$scope.users.push($scope.userOptions[i]);
			$scope.userOptions = [];
			$scope.findLike = '';

		};
		$scope.userOptions = [];
		$scope.findLike = '';
		$scope.searchNumber = 0;
		$scope.searchActual = 0;
		$scope.searchUsers = function() {
			if($scope.findLike != '') 
			{
				$scope.finding = true;
				var thisSearch = $scope.searchNumber;
				$scope.searchNumber = $scope.searchNumber+1;
				$.ajax({
					method 	: "POST",
					url		: '/'+FOLDERADD+'/chat/searchusers',
					data 	: {'like': $scope.findLike},
					dataType : 'json'
				}).done(function (data) {
					if(data.status == 'ok' && thisSearch >= $scope.searchActual && $scope.findLike != '')
					{
						$scope.searchActual = thisSearch;
						$scope.$apply(function() {
							$scope.userOptions = data.users;
							$scope.finding = false;
						});
					}
					else if (thisSearch >= $scope.searchActual)
					{
						$scope.searchActual = thisSearch;
						$scope.$apply(function() {
							$scope.finding = false;
							$scope.userOptions = [];	
						});
					}
				});
			}
			else
			{
				$scope.userOptions = [];
				$scope.finding = false;
			}
		};

		$scope.selectClick = function (e) {
			$('.select-input', e.currentTarget).focus();
		};
	}

	Cchatu.$inject = ['$scope' ,'nav', 'chatsu', 'realTime', '$routeParams', '$filter', '$location', 'chatsm'];
	function Cchatu($scope, nav, chatsu, realTime, $routeParams, $filter, $location, chatsm) {
		nav.setSelected('chat');
		$scope.chatfilter 		= '';
		$scope.chatIdSelected 	= $routeParams.id;
		$scope.unreadu 			= chatsu.unread;
		$scope.unreadm 			= chatsm.unread;

		$scope.getOldMessages 	= getOldMessages;
		$scope.sendMessage		= sendMessage;
		
		$scope.$on("update_unread_chatu", function() {
			$scope.unreadu = chatsu.unread;
		});

		$scope.$on("update_unread_chatm", function() {
			$scope.unreadm = chatsm.unread;
		});

		$scope.$on('$destroy', destroy)




		chatsu
		.getChats()
		.success(function(data) {
			if (data.status == 'ok') {
				$scope.chats = data.chats;
				realTime.suscribe('chatsu');
				if ($scope.chatIdSelected) {
					chatsu
					.getChatById($scope.chatIdSelected)
					.success(getChatSuccess);
				}
				$scope.$on('new_chatu_message', function() {
					chatsu
					.getChats()
					.success(function(data) {
						if (data.status == 'ok') {
							$scope.chats = data.chats;
						}
					});
				});
			}
		});


		function getChatSuccess(data) {
			if (data.status == 'ok') {
				$scope.chat 			= data.chat;
				$scope.chat.messages 	= data.chat.messages;
				$scope.$on('chatu_message '+$scope.chat.chatuId, function(e,m) {
					AppendMessage(m, true);
				});
				realTime.suscribe('chatu', $scope.chat.chatuId);
				if (data.chat.unreads) {
					chatsu.getUnread();
				}
			}
		}

		function AppendMessage(message, apply) {
			if (message.userId != userData.userId) {	
				chatsu.viewMessage(message.chatumessageId);
			}

			var exist = $filter('filter')($scope.chat.messages, {chatumessageId : message.chatumessageId});
			if (exist && exist.length == 0) {
				if ($scope.chat.chatuId == message.chatuId) {	
					$scope.chat.messages.push(message);
					if (apply) {$scope.$apply();}
				}
			}
		}


		$('#confirm-delete').on('show.bs.modal', function (event) {
			var id 	= $(event.relatedTarget).data('id')
			$(this).find('#delete-yes').data('chatid', id);
		});

		$('#delete-yes').click(function(e) {
			var btn = $(this);
			btn.hide();
			$('#delete-loading').show();
			chatsu
			.deleteChat(btn.data('chatid'))
			.success(deleteChatSuccess);
		});

		function sendMessage() {
			if ($scope.chat.newmessage && $scope.chat.newmessage != '') {
				var data = { 'id' : $scope.chatIdSelected, 'message': $scope.chat.newmessage};
				chatsu
				.sendMessage(data)
				.success(function(data) {
					if (data.status == 'ok') {
						AppendMessage(data.message);
						var data = {
							id 		: data.message.chatuId,
							message : data.message,
							to 		: [$scope.chat.fromuserId, $scope.chat.touserId]
						};
						realTime.socket.emit('chatu_message', data);
					}
				});
				$scope.chat.newmessage = '';
			}
		}

		function getOldMessages() {
			$scope.gettingOldMessages = true;
			var first = $filter('orderBy')($scope.chat.messages, 'timestamp')[0].chatumessageId;
			chatsu
			.getOldMessages($scope.chatIdSelected,first)
			.success(getOldMessagesSuccess);
		}

		function getOldMessagesSuccess(data) {
			if (data.status == 'ok') {
				$scope.chat.messages = $scope.chat.messages.concat(data.messages);
				$scope.gettingOldMessages = false;
			}
			else if (data.status == 'empty') {
				$scope.fullOldMessages = true;
				$scope.gettingOldMessages = false;
			}
		}

		function deleteChatSuccess(data) {
			if (data.status == 'ok') {
				$('#confirm-delete').modal('hide');
				$location.path('/chat/unique')
			}
		}




		function destroy() {
			if ($scope.chatIdSelected) {
				realTime.unsuscribe('chatu', $scope.chatIdSelected);
			}
			realTime.unsuscribe('chatsu');
		}
	}

	FchatsU.$inject = ['$rootScope', '$http'];
	function FchatsU($rootScope, $http) {
		var sv = {
			unread 					: 0,
			getChats 				: function() {
				return $http.get('/'+FOLDERADD+'/chat/getchatsu');
			},
			getChatById				: function(id) {
				return $http.get('/'+FOLDERADD+'/chat/getchatubyid?id='+id);
			},
			getOldMessages	: function(id, first) {
				return $http.get('/'+FOLDERADD+'/chat/getoldmessages?id='+id+'&first='+first);
			},
			deleteChat				: function(id) {
				return $http.get('/'+FOLDERADD+'/chat/deletechatu?id='+id);
			},
			sendMessage				: function(data) {
				return $http.post('/'+FOLDERADD+'/chat/newmessageu', data);
			},
			getNewMessages	: function(id, last) {
				return $http.get('/'+FOLDERADD+'/chat/getnewmessagesu?id='+id+'&last='+last);
			},
			viewMessage	: function(id) {
				return $http.get('/'+FOLDERADD+'/chat/addmessageuview?id='+id );
			},
			getUnread 	: function() {
				$http
				.get('/'+FOLDERADD+'/chat/getunreadu')
				.success(function(data) {
					if (data.status == 'ok') {
						sv.unread = data.unread;
						$rootScope.$broadcast('update_unread_chatu');
					}
				})
			}
		}
		return sv;
	}


})();