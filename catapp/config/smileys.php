<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------
| SMILEYS
| -------------------------------------------------------------------
| This file contains an array of smileys for use with the emoticon helper.
| Individual images can be used to replace multiple smileys.  For example:
| :-) and :) use the same image replacement.
|
| Please see user guide for more info:
| http://codeigniter.com/user_guide/helpers/smiley_helper.html
|
*/
$smileys = array(

//	smiley				emoji name					eje X		eje Y 		title

	'0\:\)'			=>	array('angel',				'-726',		'-132', 	'0:)'),
	'\:\)'			=>	array('grin',				'-660',		'-132', 	':)'),
	'\:D'			=>	array('grin lol',			'-638',		'-132', 	':D'),
	'\;\)'			=>	array('wink',				'-770',		'-132', 	';)'),
	'\:S'			=>	array('confused',			'-1056',	'-132',		':S'),
	'\:\('			=>	array('frowning',			'-1408',	'-132', 	':('),
	'D\:'			=>	array('anguished',			'-1430',	'-132', 	'D:'),
	'\:angry\:'		=>	array('angry',				'-1276',	'-132',		':angry:'),
	'\:angryred\:'	=>	array('angryred',			'-1298',	'-132', 	':angryred:'),
	'\:wow\:'		=>	array('surprise',			'-1452',	'-132',		':wow:'),
	'\:\/'			=>	array('hummm',				'-1034',	'-132',		':/'),
	'\:\o'			=>	array('ooooh',				'-1606',	'-132', 	':o'),
	'\:\O'			=>	array('OOOOH',				'-1584',	'-132',		':O'),
	'Xo'			=>	array('antonished',			'-1672',	'-132', 	'Xo'),
	'XO'			=>	array('dizzi',				'-1738',	'-132', 	'XO'),
	'\:slepping\:'	=>	array('slepping',			'-1716',	'-132',		':slepping:'),
	'\:(P|p)'		=>	array('tongue laugh',		'-1166',	'-132',		':P'),
	'\;(P|p)'		=>	array('tongue wink',		'-1188',	'-132',		';P'),
	'XP'			=>	array('tongue lol',			'-1210',	'-132',		'XP'),
	'\:\|'			=>	array('poker face',			'-924',		'-132',		':|'),
	'\:mouth\:'		=>	array('mouth',				'-1760',	'-132',		':mouth:'),
	'B\|'			=>	array('sunglasses',			'-880',		'-132',		'B|'),
	'XD'			=>	array('lol',				'-704',		'-132', 	'XD'),
	'\¬\¬'			=>	array('unamused',			'-968',		'-132', 	'¬¬'),
	'\-\_\-'		=>	array('expessionless',		'-946',		'-132', 	'-_-'),	
	'\:\\\\'		=>	array('smirking',			'-902',		'-132',		':\\'),
	'\:&#039;\('	=>	array('cry face',			'-1386',	'-132',		':&#039;('),
	'\:&quot;\('	=>	array('crying',				'-1562',	'-132',		':&quot;('),
	'\:loveface\:'	=>	array('mouth',				'-858',		'-132',		':loveface:'),
	'\:\*'			=>	array('kiss',				'-1100',	'-132', 	':*'),
	'\:3'			=>	array('kissing',			'-1078',	'-132', 	':3'),
	'\:kiss\:'		=>	array('kiss',				'-1122',	'-132',		':kiss:'),
	'&lt;3'			=>	array('heart',				'-1914',	'-176', 	'&lt;3'),
	'\:poop\:'		=>	array('poop',				'-748',		'-88', 		':poop:'),
	'\(y\)'			=>	array('like',				'-726',		'-66', 		'(y)'),
);

$remplaceSpecial = array(
//	smiley				emoji name					eje X		eje Y 		title
	'&lt;3'				=>	array('<3', '<3'),
	'\:&quot;\('		=>	array(':\\\"(', ':\\"('),
	'\:&#039;\('		=>	array(':\\\'(', ':\\\'('),
	'\:\\\\'			=>	array(':\\\\\\\\', ':\\\\\\'),
);
