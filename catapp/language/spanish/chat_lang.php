<?php

defined('BASEPATH') OR exit('No direct script access allowed');

$lang['chat_unique'] 				= 'Unicos';
$lang['chat_uniquetitle'] 			= 'Chats unicos';
$lang['chat_multipletitle'] 		= 'Chats multiples';
$lang['chat_multi'] 				= 'Multiples';
$lang['chat_empty'] 				= 'No hay chats<br>';
$lang['chat_press']                 = 'Presione <i class="fa fa-plus"></i> para iniciar un nuevo chat';
$lang['chat_new'] 					= 'Nuevo Chat';
$lang['chat_selectchat'] 			= 'Seleccione un Chat';
$lang['chat_loadmessages'] 			= 'Cargar Mensajes';
$lang['chat_finduser'] 				= 'Buscar';
$lang['chat_message'] 				= 'Mensaje';
$lang['chat_writemessage'] 			= 'Escriba un mensaje';
$lang['chat_newto'] 				= 'Para';
$lang['chat_start'] 				= 'Comenzar';
$lang['chat_newmessage'] 			= 'Nuevo mensaje de chat';
$lang['chat_chooseaction']			= '¿Qué acción deseas realizar?';
$lang['chat_deletechatm']			= 'Eliminar chat';
$lang['chat_exitchatm']				= 'Salir del chat';
$lang['chat_deleteareyousure'] 		= '¿Está seguro que quiere eliminar este chat? Si elimina el chat ya no estara disponible para ningun usuario';
$lang['chat_deleteuareyousure'] 	= '¿Está seguro que quiere eliminar este chat?';
$lang['chat_exitareyousure'] 		= '¿Está seguro que quiere salir de este chat?';
$lang['user_deleteareyousure']		= '¿Está seguro que quiere eliminar este usuario?';
$lang['chat_new_userselects'] 		= 'Usuarios seleccionados';
$lang['chat_messagerequired'] 		= 'El campo Mensaje es obligatorio.';
$lang['chat_subjectrequired'] 		= 'El campo Asunto es obligatorio.';
$lang['chat_subject'] 				= 'Asunto';
$lang['chat_searchchat'] 			= 'Buscar un chat';
$lang['chat_users'] 				= 'Participantes';
$lang['chat_info'] 					= 'Informacion';
$lang['chat_modify_subject'] 		= 'Modificar asunto';
$lang['hover_double_click'] 		= 'Doble click para modificar el título';
$lang['chat_user_out']              = 'El usuario acaba de salir del chat';

//busqueda de chats
$lang['chat_search']                = 'Buscar chats';
$lang['chat_search_2']              = 'Busqueda';
$lang['chat_search_keyword']        = 'Palabra clave';
$lang['chat_search_daterange']      = 'Rango de fecha';
$lang['chat_search_tl']             = 'Team leaders o supervisores';
$lang['chats_search_results']       = 'Resultados';
$lang['chats_search_no_results']    = 'No hay resultados encontrados';
$lang['chats_search_no_filter']     = 'No se ha añadido ningun filtro';
$lang['chat_search_advanced']       = 'Busqueda avanzada';
$lang['chat_infoMessage']           = 'Visto por:';
$lang['chat_noViewed']              = 'El mensaje aun no ha sido visto';
?>