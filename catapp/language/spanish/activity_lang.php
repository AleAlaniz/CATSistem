<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['activity_create'] 				  = 'Nueva actividad';
$lang['activity_edit']   				  = 'Editar actividad';
$lang['activity_required']				  = 'Actividad obligatoria';
$lang['activity_capacity']				  = 'Agregar cupo';
$lang['activity_coupons']				  = 'Cupones';
$lang['activity_create_message'] 		  = 'Actividad creada correctamente.';
$lang['activity_empty'] 		          = 'No hay actividades';
$lang['activity_available'] 		      = 'Cupos disponibles';
$lang['activity_confirm_join'] 		      = 'Desea unirse a esta actividad?';
$lang['activity_confirm_discharge']  	  = 'Esta seguro que desea darse de baja de la actividad?';
$lang['activity_create_confirm_message']  = 'Una vez activada la actividad no podra ser editada, ¿Desea continuar?';
$lang['activity_edit_message'] 		      = 'Actividad editada correctamente.';
$lang['activity_complete_message'] 		  = 'Actividad cargada correctamente';
$lang['activity_confirm'] 		 		  = 'Responder';
$lang['activity_edit_opinion'] 		 	  = 'Cambié de opinión';
$lang['activity_report_total_assist'] 	  = 'Personas que respondieron a la actividad';
$lang['activity_report_total_not_assist'] = 'Personas que todavia no respondieron';
$lang['activity_confirmAnswer'] 		  = 'Asistiré';
$lang['activity_denyAnswer']  			  = 'No asistiré';
$lang['activity_answer']	 		  	  = 'Respuesta';
$lang['activity_deleted']                 = 'Actividad eliminada correctamente';
$lang['activity_empty_coupon']            = 'NO quedan cupos';
$lang['activity_end_date_user']           = 'Tenes tiempo de inscribirte hasta';
$lang['activity_finished']                = 'Las inscripciones finalizaron el';
$lang['activity_end_date']                = 'Fecha de finalización de las inscripciones';
$lang['activity_start_date']              = 'Fecha de inicio de las inscripciones';

?>