<?php

defined('BASEPATH') OR exit('No direct script access allowed');

$lang['press_title'] 						= 'Prensa';
$lang['press_empty'] 						= 'No hay notas de prensa';
$lang['press_full'] 						= 'No hay mas notas de prensa';
$lang['press_notetitle'] 					= 'Titulo';
$lang['press_loadpressnotes'] 				= 'Cargar mas notas de prensa';
$lang['press_successmessage'] 				= 'Nota de prensa creada correctamente';
$lang['press_deletemessage'] 				= 'Nota de prensa eliminada correctamente';
$lang['press_editmessage'] 					= 'Nota de prensa editada correctamente';
$lang['press_ntitle'] 						= 'Titulo';
$lang['press_create'] 						= 'Nota de prensa';
$lang['press_edit'] 						= 'Editar nota de prensa';
$lang['press_pressnote'] 					= 'Nota de prensa';
$lang['press_newsdeleteareyousure'] 		= 'Esta seguro que quiere eliminar esta nota de prensa?';
$lang['press_clippingempty'] 				= 'No hay clipping de prensa';
$lang['press_clippingfull'] 				= 'No hay mas clipping de prensa';
$lang['press_loadclippingnotes'] 			= 'Cargar mas clipping de prensa';
$lang['press_clippingsuccessmessage'] 		= 'Clipping de prensa creado correctamente';
$lang['press_clippingdeletemessage'] 		= 'Clipping de prensa eliminado correctamente';
$lang['press_clippingeditmessage'] 			= 'Clipping de prensa editado correctamente';
$lang['press_createclipping'] 				= 'Clipping de prensa';
$lang['press_editclipping'] 				= 'Editar Clipping de prensa';
$lang['press_clippingpress'] 				= 'Clipping de prensa';
$lang['press_clippingdeleteareyousure'] 	= 'Esta seguro que quiere eliminar este clipping de prensa?';
$lang['press_info']						 	= 'Informacion';
$lang['press_createnote'] 					= 'Crear nota de prensa';
$lang['press_createclipp'] 					= 'Crear clipping de prensa';
?>