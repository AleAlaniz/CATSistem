<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['survey_create_message'] 			= 'Encuesta creada correctamente.';
$lang['survey_delete_message'] 			= 'Encuesta eliminada correctamente.';
$lang['survey_activate_message'] 			= 'La encuesta "%1$s" ahora se encuentra activa.';
$lang['survey_edit_message'] 			= 'Encuesta editada correctamente.';
$lang['survey_empty'] 					= 'No hay encuestas.';
$lang['survey_create'] 					= 'Nueva encuesta';
$lang['survey_edit'] 					= 'Editar encuesta';
$lang['survey_delete_confirm_title'] 	= 'Eliminar encuesta';
$lang['survey_delete_confirm_message'] 	= 'Esta seguro que quiere eliminar la encuesta ';
$lang['survey_name'] 					= 'Nombre';
$lang['survey_description'] 			= 'Descripcion';
$lang['survey_start_date'] 				= 'Fecha de inicio';
$lang['survey_end_date']	 			= 'Fecha de finalizacion';
$lang['survey_info']	 				= 'Infomacion';
$lang['survey_answers']	 				= 'Respuestas';
$lang['survey_date_incorrect']	 		= 'La fecha es incorrecta';
$lang['survey_questions']	 			= 'Preguntas';
$lang['survey_remove']	 				= 'Remover';
$lang['survey_clear']	 				= 'Limpiar';
$lang['survey_atention'] 				= 'Atencion';
$lang['survey_completeall']				= 'Complete todos los campos marcados con ';
$lang['survey_view']					= 'Ver detalles';
$lang['survey_create_confirm_message']  = 'Una vez creada la encuesta no podra ser editada, presione crear si está seguro que todas las preguntas y respuestas son correctas.';
$lang['survey_activate_confirm_message']  = 'Una vez activada la encuesta no podra ser editada, ¿Desea continuar?';
$lang['survey_options']	 				= 'Opciones';
$lang['survey_new']	 					= 'Nueva';
$lang['survey_continue_edit']			= 'Continuar editando';
$lang['survey_whose']					= 'Quien lo puede ver';
$lang['survey_writename']				= 'Escriba el nombre';
$lang['survey_ok']						= 'Listo';
$lang['survey_answer_all']				= 'Debes responder todas las preguntas.';
$lang['survey_closemessage']			= 'Mensaje de cierre';
$lang['survey_details']					= 'Detalles';
$lang['survey_inprogress']				= 'Esta encuesta ya comenzo';
$lang['survey_nostart']					= 'Esta encuesta aun no ah comenzado';
$lang['survey_isend']					= 'Esta encuesta ya finalizo';
$lang['survey_useranswers']				= 'Respuestas de los usuarios';
$lang['survey_required']				= 'Encuesta obligatoria';
$lang['survey_required_question']		= 'Pregunta obligatoria';

$lang['survey_complete_message']  		= 'Por favor, contestá esta breve encuesta para seguir navegando en CATnet y ayudanos así a mejorar tu experiencia en CAT';

$lang['survey_question_type'] 			= 'Tipo';
$lang['survey_question'] 				= 'Pregunta';
$lang['survey_question_addquestion'] 	= 'Agregar pregunta';
$lang['survey_question_addanswer'] 		= 'Agregar respuesta';
$lang['survey_question_removequestion'] = 'Remover pregunta';
$lang['survey_question_optionunique']	= 'Respuesta unica';
$lang['survey_question_severaloptions'] = 'Varias respuestas';
$lang['survey_question_input'] 			= 'Llenar campo';
$lang['survey_question_trueorfalse'] 	= 'Verdadero o falso';

$lang['survey_answer']	 				= 'Respuesta';
$lang['survey_answer_correct']	 		= 'Respuesta correcta';
$lang['survey_answer_incorrect']	 	= 'Respuesta incorrecta';
$lang['survey_answer_iscorrect']	 	= 'La respuesta es correcta';
$lang['survey_answer_iscorrect_help'] 	= 'Marca esta casilla si la respuesta es correcta';

$lang['survey_report']					= 'Reporte';
$lang['survey_report_totalcompletes']	= 'Personas que completaron la encuesta';
$lang['survey_report_totalincompletes']	= 'Personas que entraron y no completaron la encuesta';
$lang['survey_count']					= 'Cantidad';
$lang['survey_percent']					= 'Porcentaje';
$lang['survey_peoplecount']				= 'Personas que eligieron esta respuesta';


$lang['survey_myanswers']				= 'Ver mis respuestas';
$lang['survey_completesurvey']			= 'Responder encuesta';
$lang['survey_myanswer']				= 'Mi respuesta';

$lang['survey_user_name']				= 'Nombre';
$lang['survey_user_lastname']			= 'Apellido';
$lang['survey_user_campaing']			= 'Campaña / Área';
$lang['survey_user_turn']				= 'Turno';
$lang['survey_user_sex']				= 'Sexo';
$lang['survey_user_studies']			= 'Nivel de estudios';
$lang['survey_user_birthday']			= 'Fecha de nacimiento';
$lang['survey_undefined']				= 'Indefinido';
$lang['survey_user_date']				= 'Fecha y hora';
$lang['survey_correctresponses']		= 'Respuestas correctas';
$lang['survey_youcorrectresponses']		= 'Tus respuestas correctas fueron';

$lang['survey_view_preview']			= 'Preview';

$lang['surveyCompleted']                = 'Encuesta cargada correctamente.';
$lang['userInCompleteSurvey']           = 'La encuesta para el usuario seleccionado ya fue cargada.';
$lang['notUser']                        = '¡No se ha ingresado un usuario!';

$lang['survey_export_report']           = 'Exportar reporte a excel';
$lang['survey_edit_enddate']            = 'Editar fecha de finalización';
$lang['survey_confirm_edit']            = 'Confirmar cambio';
$lang['survey_empty_date']              = 'Llene el campo de fecha';
$lang['survey_survey_results']          = 'Exportar resultados de la encuesta a excel'; 
?>