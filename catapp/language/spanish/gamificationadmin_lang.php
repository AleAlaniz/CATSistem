<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*$lang['achievement_name'] = 'Nombre';
$lang['achievement_date']='Fecha creación';
$lang['achievement_description'] = 'Descripción';
$lang['achievement_quantity'] = 'Cantidad objetivo';
$lang['achievement_triggertype']='Tipo de evento';
$lang['achievement_trigger']='Evento';
$lang['achievement_image']='Imagen';
$lang['achievement_triggertype_manual']='Manual';
$lang['achievement_triggertype_auto']='Automatico';

$lang['medal_image']='Imagen';
$lang['medal_name']='Nombre';
$lang['medal_description']='Descripción';


$lang['prize_image']='Imagen';
$lang['prize_name']='Nombre';
$lang['prize_description']='Descripción';
$lang['prize_stock']='Stock';

$lang['challenge_image']="Imagen";
$lang['challenge_name']='Nombre';
$lang['challenge_description']='Descripción';
$lang['challenge_limitdate']='Fecha límite';


$lang['achievement_example_name']='Ej: Amante de la intranet.';
$lang['achievement_example_description']="Describa de que trata el objetivo. Ej: Inicie sesión en la intranet diez veces.";
$lang['achievement_example_quantity']='Indique la cantidad deseada para completar el objetivo. Ej: 10';

$lang['medal_example_name']='Ej: Medalla Cat.';
$lang['medal_example_description']='Ej: Reconocimiento al mejor vendedor. Felicitaciones!!.';

$lang['prize_example_name']='Ej: 30 minutos de Break.';
$lang['prize_example_description']='Ej: Vale por 30 minutos de descanso para cuando gustes.';
$lang['prize_example_stock']='';

$lang['challenge_example_name']='Ej: Desafio extremo. (Tenga en cuenta que los desafios son los más dificiles de conseguir).';
$lang['challenge_example_description']='Ej: Completa este desafio y conságrate el mejor.';
$lang['challenge_example_date']='Seleccione la fecha que desea finalizar el desafio. ';


$lang['gamificationadmin_achievements']='Objetivos';
$lang['gamificationadmin_challenges']='Desafios';
$lang['gamificationadmin_medals']='Medallas';
$lang['gamificationadmin_prizes']='Premios';


$lang['gamificationadmin_create_achievement']='Crear objetivo';
$lang['gamificationadmin_edit_achievement']='Editar objetivo';

$lang['gamificationadmin_create_medal']='Crear medalla';
$lang['gamificationadmin_edit_medal']='Editar medalla';


$lang['gamificationadmin_create_prize']='Crear premio';
$lang['gamificationadmin_edit_prize']='Editar premio';

$lang['gamificationadmin_create_challenge']='Crear desafio';
$lang['gamificationadmin_edit_challenge']='Editar desafio';


$lang['gamification']='Gamification';

$lang['gamificationadmin_achievement_createsuccess']='Objetivo creado correctamente';
$lang['gamificationadmin_achievement_editsuccess']='Objetivo editado correctamente';
$lang['gamificationadmin_medal_createsuccess']='Medalla creada correctamente';
$lang['gamificationadmin_medal_editsuccess']='Medalla editada correctamente';
$lang['gamificationadmin_prize_createsuccess']='Premio creado correctamente';
$lang['gamificationadmin_prize_editsuccess']='Premio editado correctamente';
$lang['gamificationadmin_challenge_createsuccess']='Desafio creado correctamente';
$lang['gamificationadmin_challenge_editsuccess']='Desafio editado correctamente';


$lang['trigger_noexist']='El evento no existe.';

$lang['achievement_visibility']='¿Quien puede verlo?';
$lang['achievement_visibility_all']='Todos';
$lang['achievement_visibility_other']='Seleccionar quien';

$lang['achievement_visibility_details_title']='¿Quienes pueden ver este objetivo?';
$lang['challenge_visibility_details_title']='¿Quienes pueden ver este desafio?';
$lang['challenge_details_title']='Detalles del desafio';

$lang['general_noresults']='No hay resultados disponibles';

$lang['gamificationadmin_allcansee']='Todos los usuarios pueden ver este objetivo.';

$lang['gamificationadmin_challenges_prevpage']='Anterior conjunto de desafios';
$lang['gamificationadmin_challenges_prevslide']='Ver anterior';
$lang['gamificationadmin_challenges_nextslide']='Ver siguiente';
$lang['gamificationadmin_challenges_nextpage']='Siguiente conjunto de desafios';
$lang['challenges_details_title']='Detalles del desafio';
$lang['challenge_nolimitdate']="sin limite";*/
?>
