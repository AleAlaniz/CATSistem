<?php

defined('BASEPATH') OR exit('No direct script access allowed');

$lang['gamification_achievementcomplete'] 	= 'Objetivo completado';

$lang['general_crear'] 				= 'Crear';
$lang['general_create'] 			= 'Crear';
$lang['general_clear'] 				= 'Vaciar';
$lang['general_answer'] 			= 'Responder';
$lang['general_config'] 			= 'Configuracion';
$lang['general_miprofile'] 			= 'Mi Perfil';
$lang['general_send'] 				= 'Enviar';
$lang['general_find'] 				= 'Buscar';
$lang['general_goback'] 			= 'Volver atrás';
$lang['general_success'] 			= 'Listo';
$lang['general_next'] 				= 'Siguiente';
$lang['general_required'] 			= 'Campo obligatorio';
$lang['general_accept'] 			= 'Aceptar';
$lang['general_yes'] 				= 'Si';
$lang['general_no'] 				= 'No';
$lang['general_ok']					= 'Ok';
$lang['general_usergroups'] 		= 'Equipos / Grupos de usuarios';
$lang['general_wait']				='Espere...';
$lang['general_completeall'] 		= 'Complete todos los campos marcados con';
$lang['general_completeall_error']	='Complete todos los campos obligatorios.';
$lang['general_removing']			='Eliminando...';
$lang['general_tips'] 				= 'Tips';
$lang['general_info'] 				= 'Información';
$lang['general_to'] 				= 'Para quién';
$lang['general_good']				= 'Me sirvió';
$lang['general_bad']				= 'No me sirvió';
$lang['general_add']				= 'Agregar';
$lang['general_other']				= 'Otro';
$lang['general_error']				= 'Ha ocurrido un error inesperado. Inténtelo más tarde.';
$lang['general_attention']			= 'Atención';
$lang['general_social_reason']      = 'Empresa';

$lang['general_corporate_docs']		= 'Documentos corporativos';

$lang['home_thewall'] 				= 'El muro de CAT';
$lang['home_whatsnew'] 				= 'Que hay de nuevo?';
$lang['home_statusplaceholder'] = 'Actualiza tu estado';
$lang['home_post'] = 'Estado';
$lang['home_submitpost'] = 'Publicar';
$lang['home_postempty'] = 'El estado no puede estar vacio.';
$lang['home_postsempty'] = 'No hay mas publicaciones.';
$lang['home_alerttitle'] = 'Información importante';
$lang['home_updatestatusok'] = 'Estado actualizado correctamente.';
$lang['home_newposts'] = 'Nuevas publicaciones';
$lang['home_like'] = '';
$lang['home_comments'] = 'Comentarios';
$lang['home_deletepostareyousure'] = 'Esta seguro que quiere eliminar esta publicacion?';
$lang['home_deletecommentareyousure'] = 'Esta seguro que quiere eliminar este comentario?';
$lang['home_commentplaceholder'] = 'Comentario';
$lang['home_comment'] = 'Comentario';
$lang['home_submitcomment'] = 'Comentar';
$lang['home_commentempty'] = 'El comentario no puede estar vacio.';
$lang['home_inbox'] = 'Bandeja de entrada';

$lang['home_welcomemessageempty'] = 'No hay nada para saber hoy';

$lang['home_newcommentinboxsubject'] = 'Nuevo comentario del Muro de CAT';
$lang['home_newcommentinboxmessage'] = 'Tiene un nuevo comentario del Muro de CAT';
$lang['home_newcommentinboxlink'] = 'Ir a la publicacion';
$lang['home_surveycomplete'] = 'Muchas gracias por tomarte el tiempo de completar la encuesta!';

$lang['survey_insert_user'] 	= 'Ingrese el nombre del usaurio al cual desea cargar la encuesta';
$lang['survey_insert_alert'] 	= 'Recuerde que una vez ingresados los datos, estos no pueden ser modificados';

$lang['chat_title'] = 'Chat';



$lang['inbox_deletemessagemessage'] 	= 'Mensaje movido a la papelera.';
$lang['inbox_restoremessagemessage'] 	= 'Mensaje restaurado correctamente.';
$lang['inbox_newmessagemessage'] 		= 'Mensaje enviado correctamente.';
$lang['inbox_clearmessagemessage'] 		= 'Papelera vaciada correctamente.';
$lang['inbox_in'] 						= 'Recibidos';
$lang['inbox_answer'] 					= 'Responder';
$lang['inbox_out'] 						= 'Enviados';
$lang['inbox_cleartrash'] 				= 'Vaciar papelera';
$lang['inbox_new'] 						= 'Nuevo';
$lang['inbox_newtitle'] 				= 'Nuevo mensaje';
$lang['inbox_new_to'] 					= 'Enviar a';
$lang['inbox_selecto'] 					= 'Seleccionar usuarios';
$lang['inbox_messagebody'] 				= 'Cuerpo del mensaje';
$lang['inbox_messagebodyempty'] 		= 'El campo Cuerpo del mensaje es obligatorio.';
$lang['inbox_removed'] 					= 'Papelera';
$lang['inbox_intitle'] 					= 'Mensajes recibidos';
$lang['inbox_noresults'] 				= 'No se han encontrado usuarios.';
$lang['inbox_outtitle'] 				= 'Mensajes enviados';
$lang['inbox_trahstitle'] 				= 'Mensajes en papelera';
$lang['inbox_subject'] 					= 'Asunto';
$lang['inbox_new_toall'] 				= 'Todos';
$lang['inbox_restore'] 					= 'Restaurar';
$lang['inbox_nosubject'] 				= 'Sin asunto.';
$lang['inbox_new_userselects'] 			= 'Usuarios seleccionados';
$lang['inbox_new_usernoselectable'] 	= 'Ya selecciono este usuario';
$lang['inbox_from'] 					= 'Remitente';
$lang['inbox_to'] 						= 'Destinatarios';
$lang['inbox_date'] 					= 'Fecha';
$lang['inbox_inempty'] 					= 'No hay mensajes.';
$lang['inbox_deletemessageareyousure'] 	= '¿Esta seguro que quiere eliminar este mensaje?';
$lang['inbox_clearthashareyousure']		= '¿Esta seguro que quiere vaciar la papelera?';
$lang['inbox_deletemessagesareyousure']	= '¿Esta seguro de enviar los mensajes seleccionados a la papelera?';
$lang['inbox_movetotrashmessagsareyousure']	= '¿Esta seguro de enviar este mensaje a la papelera?';
$lang['inbox_important']				=  'Este mensaje es importante';
$lang['inbox_attached_file']			=  'Archivo adjunto';
$lang['inbox_file_allowtypes']			=	'Tipos de archivos permitidos: gif, jpg, png, pdf, doc, docx, pptx, ppt, xlsx, xls, txt, html';
$lang['inbox_downloadfile']				= 'Ver/Descargar adjunto';

$lang['inbox_finduser'] 				= 'Buscar';
$lang['inbox_nodestinataries'] 			= 'Debe seleccionar los destinatarios';
$lang['inbox_completeall_error']		='Completa todos los campos obligatorios.';
$lang['inbox_new_message']				='¡Nuevo!';
$lang['inbox_mark_all']					="Marcar todos";
$lang['inbox_unmark_all']				="Desmarcar todos";


$lang['general_wecat'] 					= 'Somos CAT';
$lang['general_teaming'] 				= 'Teaming';
$lang['general_wellnes'] 				= 'Tu Bienestar';
$lang['general_inspired'] 				= 'Inspirate';
$lang['general_catinaction'] 			= '¡CAT en acción!';
$lang['general_tryout'] 				= '¡Ponete a prueba!';

$lang['general_sections'] = 'Divisiones';
$lang['general_section'] = 'División';
$lang['general_upload'] = 'Subir';
$lang['general_download'] = 'Ver / Descargar';
$lang['administration_sections_name'] = 'Nombre';
$lang['administration_sections_icon'] = 'Icono';
$lang['administration_sections_empty'] = 'No se han encontrado Divisiones';
$lang['administration_sections_nameexist'] = 'Ya existe una División con este nombre';
$lang['administration_sections_successmessage'] = 'División creada correctamente';
$lang['administration_sections_deletemessage'] = 'División eliminada correctamente';
$lang['administration_sections_editmessage'] = 'División editada correctamente';
$lang['administration_sections_deletemessage_subsections'] = 'Si elimina esta División se eliminaran todas las Secciones pertenecientes.';
$lang['administration_sections_create'] = 'Crear División';
$lang['administration_sections_delete'] = 'Eliminar División';
$lang['administration_sections_edit'] = 'Editar División';
$lang['administration_sections_details'] = 'Detalles de la División';
$lang['administration_sections_delete_areyousure'] = 'Esta seguro que quiere eliminar esta División?';
$lang['section_favorites'] = 'Favoritos';
$lang['section_favoritesempty'] = 'No tienes favoritos';


$lang['general_subsections'] = 'Secciones';
$lang['administration_subsections_name'] = 'Nombre';
$lang['administration_subsections_section'] = 'División';
$lang['administration_subsections_subsection'] = 'Sección';
$lang['administration_subsections_icon'] = 'Icono';
$lang['administration_subsections_successmessage'] = 'Sección creada correctamente';
$lang['administration_subsections_deletemessage'] = 'Sección eliminada correctamente';
$lang['administration_subsections_editmessage'] = 'Sección editada correctamente';
$lang['administration_subsections_empty'] = 'No se han encontrado Secciones';
$lang['administration_subsections_create'] = 'Crear Sección';
$lang['administration_subsections_delete'] = 'Eliminar Sección';
$lang['administration_subsections_details'] = 'Detalles de la Sección';
$lang['administration_subsections_edit'] = 'Editar Sección';
$lang['administration_subsections_delete_areyousure'] = 'Esta seguro que quiere eliminar esta Sección?';
$lang['administration_subsections_sectionexist'] = 'Esta Seccion no existe';
$lang['administration_subsections_subsectionexist'] = 'Esta Sección no existe';
$lang['administration_subsections_appendto'] = 'Anexar a';

$lang['administration_welcomemessage_message'] = 'Editar que hay que saber hoy';
$lang['administration_welcomemessage_edit'] = 'Editar que hay que saber hoy';
$lang['administration_welcomemessage_editmessage'] = 'Mensaje editado correctamente';

$lang['administration_panelmessage_title'] = 'Mensaje principal';
$lang['administration_panelmessage_message'] = 'Editar mensaje principal';
$lang['administration_panelmessage_edit'] = 'Editar mensaje principal';
$lang['administration_panelmessage_editmessage'] = 'Mensaje editado correctamente';
$lang['administration_panelmessage_vars'] = 'Variables disponibles: {{name}} , {{lastname}}';

$lang['administration_alert_message'] = 'Editar mensaje';
$lang['general_alert'] = 'Informacion importante';
$lang['administration_alert_editmessage'] = 'Mensaje editado correctamente';
$lang['administration_alert_empty'] = 'No hay informacion importante.';
$lang['administration_deletealertareyousure'] = 'Esta seguro que quiere eliminar esta informacion importante?';

$lang['general_items'] = 'Items';
$lang['administration_items_message'] = 'Gestionar Items de las Secciones';
$lang['administration_items_successmessage'] = 'Item creado correctamente';
$lang['administration_items_deletemessage'] = 'Item eliminado correctamente';
$lang['administration_items_editmessage'] = 'Item editado correctamente';
$lang['administration_items_name'] = 'Nombre';
$lang['administration_items_subsection'] = 'Sección';
$lang['administration_items_empty'] = 'No se han encontrado Items';
$lang['administration_items_subsectionexist'] = 'Esta Sección no existe';
$lang['administration_items_create'] = 'Crear Item';
$lang['administration_items_edit'] = 'Editar Item';
$lang['administration_items_delete'] = 'Eliminar Item';
$lang['administration_items_delete_areyousure'] = 'Esta seguro que quiere eliminar este Item?';
$lang['administration_items_description'] = 'Descripción';
$lang['administration_items_information'] = 'Información del Item';
$lang['administration_items_nodescription'] = 'No hay descripción para este Item.';
$lang['administration_items_nodiary'] = 'No hay eventos en la agenda.';
$lang['administration_items_diary'] = 'Agenda';
$lang['administration_items_notices'] = 'Anuncios';
$lang['administration_items_doc'] = 'Documentos';
$lang['administration_items_trivia'] = 'Trivias, encuestas y evaluaciones';
$lang['administration_items_forums'] = 'Foros';
$lang['administration_items_comeitem'] = 'Ir al Item';
$lang['administration_items_addevent'] = 'Agregar evento';
$lang['administration_items_deleteallevent'] = 'Eliminar todos los eventos';
$lang['administration_items_addnotice'] = 'Agregar anuncio';
$lang['administration_items_deleteallnotice'] = 'Eliminar todos los anuncios';
$lang['administration_items_nonotice'] = 'No hay anuncios.';

$lang['administration_items_event_title'] = 'Título';
$lang['administration_items_event_details'] = 'Detalles';
$lang['administration_items_event_date'] = 'Fecha';
$lang['administration_items_event_time'] = 'Hora';
$lang['administration_items_event_minute'] = 'minuto';
$lang['administration_items_event_duration'] = 'Duracion';
$lang['administration_items_event_location'] = 'Ubicacion';
$lang['administration_items_event_createmessage'] = 'El Evento fué agregado a la agenda.';
$lang['administration_items_event_deletemessage'] = 'El Evento fué eliminado de la agenda.';
$lang['administration_items_event_editmessage'] = 'Evento editado correctamente.';
$lang['administration_items_event_deleteallmessage'] = 'Se eliminaron todos los eventos de la agenda.';
$lang['administration_items_event_deleteareyousure'] = '¿Esta seguro que quiere eliminar este evento?';
$lang['administration_items_event_deleteallareyousure'] = '¿Esta seguro que quiere eliminar todos los eventos?';

$lang['administration_items_notice_create'] = 'Agregar anuncio';
$lang['administration_items_notice_deleteall'] = 'Eliminar todos los anuncios';
$lang['administration_items_notice_deleteallareyousure'] = '¿Esta seguro que quiere eliminar todos los anuncios?';
$lang['administration_items_notice_deleteareyousure'] = '¿Esta seguro que quiere eliminar este anuncio?';
$lang['administration_items_notice_createmessage'] = 'Anuncio creado correctamente.';
$lang['administration_items_notice_deletemessage'] = 'Anuncio eliminado correctamente.';
$lang['administration_items_notice_editmessage'] = 'Anuncio editado correctamente.';
$lang['administration_items_notice_deleteallmessage'] = 'Se eliminaron todos los anuncios.';
$lang['administration_items_notice_empty'] = 'No hay anuncios.';
$lang['administration_items_notice_createdday'] = 'Publicado el';
$lang['administration_items_notice_title'] = 'Titulo';
$lang['administration_items_notice_details'] = 'Detalles';

$lang['administration_items_doc_upload'] = 'Subir archivo';
$lang['administration_items_doc_createmessage'] = 'Archivo agregado correctamente.';
$lang['administration_items_doc_deletemessage'] = 'Archivo eliminado correctamente.';
$lang['administration_items_doc_editmessage'] = 'Archivo editado correctamente.';
$lang['administration_items_doc_empty'] = 'No hay documentos.';
$lang['administration_items_doc_name'] = 'Nombre';
$lang['administration_items_doc_document'] = 'Documento';
$lang['administration_items_doc_createfolder'] = 'Crear Carpeta';
$lang['administration_items_doc_nofolder'] = 'Raiz';
$lang['administration_items_doc_folder'] = 'Carpeta';
$lang['administration_items_doc_createfoldermessage'] = 'Carpeta creada correctamente.';
$lang['administration_items_doc_deletefoldermessage'] = 'Carpeta eliminada correctamente.';
$lang['administration_items_doc_editfoldermessage'] = 'Carpeta editada correctamente.';
$lang['administration_items_doc_folderexist'] = 'Esta carpeta no existe.';
$lang['administration_items_doc_comment'] = 'Comentario';
$lang['administration_items_doc_size'] = 'Tamaño';
$lang['administration_items_doc_allowtypes'] = 'Tipos de archivos permitidos: gif, jpg, png, pdf, doc, docx, pptx, ppt, xlsx, xls, txt, html, odt, ods';
$lang['administration_items_doc_lastmodified'] = 'Ultima modificacion';
$lang['administration_items_doc_deleteareyousure'] = '¿Esta seguro que quiere eliminar este documento?';
$lang['administration_items_doc_deletefolderareyousure'] = '¿Esta seguro que quiere eliminar esta carpeta?';

$lang['administration_items_selectto'] = 'Quien lo puede ver';
$lang['item_addtofavorites'] = 'Agregar a favoritos';
$lang['item_removefromfavorites'] = 'Quitar de favoritos';

$lang['general_administration'] = 'Administración';
$lang['general_back'] = 'Volver';
$lang['general_true'] = 'Verdadero';
$lang['general_false'] = 'Falso';
$lang['general_end'] = 'Terminar';
$lang['general_welcomemessage'] = '¿Qué tenés que saber hoy?';
$lang['general_atention'] = 'Atencion';
$lang['general_users'] = 'Usuarios';
$lang['general_user'] = 'Usuario';
$lang['general_home'] = 'Inicio';
$lang['general_cancel'] = 'Cancelar';
$lang['general_save'] = 'Guardar';
$lang['general_show_all'] = 'Mostrar todo';
$lang['general_delete'] = 'Eliminar';
$lang['general_edit'] = 'Editar';
$lang['general_exit'] = 'Salir';
$lang['general_comebacklist'] = 'Volver a la lista';
$lang['administration_title'] = 'Administración';
$lang['administration_config'] = 'Configurar';
$lang['administration_users_message'] = 'Gestionar Usuarios';
$lang['administration_sections_message'] = 'Gestionar Divisiones';
$lang['administration_subsections_message'] = 'Gestionar Secciones';
$lang['administration_themes_message'] = 'Gestionar Tematica';
$lang['general_themes'] = 'Tematicas';
$lang['administration_comeback'] = 'Volver a la aplicacion';
$lang['general_upload_excel'] = 'Subir archivo excel';


$lang['administration_users_beforestate'] = 'Situacion Anterior';
$lang['administration_users_completestudies'] = 'Nivel de estudios';
$lang['administration_users_studies1'] = 'Secundario';
$lang['administration_users_studies2'] = 'Terciario';
$lang['administration_users_studies3'] = 'Universitario';
$lang['administration_users_studies4'] = 'Posgrado';
$lang['administration_users_actualestudies1'] = 'Completo';
$lang['administration_users_actualestudies2'] = 'Incompleto';
$lang['administration_users_actualestudies3'] = 'En curso';
$lang['administration_users_actualstudies'] = 'Estudios actual / grado de avance';
$lang['administration_users_schuedule'] = 'Horario';
$lang['administration_users_schuedule_example'] = 'Formato 24 horas. Ej: 21:00 o 09:00';
$lang['administration_users_campain'] = 'Campaña';
$lang['administration_users_birthday'] = 'Fecha de nacimiento';
$lang['administration_users_birthdayplaceholder'] = 'Fecha de nacimiento (dd/mm/aaaa)';
$lang['administration_users_ingressdate'] = 'Fecha de ingreso';
$lang['administration_users_checkin'] = 'Horario de ingreso';
$lang['administration_users_checkout'] = 'Horario de egreso';

$lang['administration_users_boss'] = 'Jefe a cargo';
$lang['administration_users_work'] = 'Tarea';
$lang['administration_users_sectionerror'] = 'Esta division no existe';
$lang['administration_users_turn']='Turno';
$lang['administration_users_campaign']='Área';
$lang['administration_users_nationality'] = 'Nacionalidad';



$lang['users_gender'] 						= 'Género';
$lang['users_civilstate'] 					= 'Estado civil';
$lang['users_rent'] 						= 'Alquila';
$lang['users_has_child'] 					= 'Tiene hijo/s';
$lang['users_personsincharge'] 				= 'Personas a cargo';
$lang['users_turn'] 						= 'Turno';
$lang['users_usernumber'] 					= 'Número de legajo';
$lang['users_dni'] 							= 'DNI';
$lang['users_phone'] 						= 'Teléfono fijo';
$lang['users_cell_phone'] 					= 'Teléfono celular';
$lang['users_area_code'] 					= 'Código de área';
$lang['users_number'] 						= 'Número (sin puntos ni guiones)';
$lang['users_number_phone'] 				= 'Número (sin puntos, guiones ni el 15)';
$lang['users_hobbies'] 						= 'Hobbies/Actividades en tiempo libre';
$lang['users_add_hobby']					= 'Agregar hobby';
$lang['users_transports'] 					= 'Medios de transporte';
$lang['users_add_transport']				= 'Agregar medio de transporte';
$lang['users_family_members']				= 'Grupo familiar';
$lang['users_name']							= 'Nombre';
$lang['users_lastName']						= 'Apellido';
$lang['users_relation']						= 'Parentesco';
$lang['users_birthdate']					= 'Fecha de nacimiento';
$lang['users_nomembers']					= 'No hay miembros';
$lang['users_before_works']					= 'Trabajos anteriores';
$lang['users_no_before_works']				= 'No tienes trabajos anteriores';
$lang['users_details']						= 'Detalles';
$lang['users_studies']						= 'Estudios';
$lang['users_no_studies']					= 'No tienes estudios';
$lang['users_level']						= 'Nivel';
$lang['users_status']						= 'Estado';
$lang['users_degree']						= 'Carrera / Orientación';
$lang['users_notify_complete_data']			= 'Aviso de actualizacion de datos';
$lang['users_notify_complete_data_config']	= 'Configurar aviso de actualizacion de datos';
$lang['users_notify_complete_data_success']	= 'Aviso configurado correctamente';
$lang['users_zipcode'] 						= 'Código postal';
$lang['users_institution'] 					= 'Institución';


$lang['administration_users_name'] = 'Nombre';
$lang['administration_users_lastname'] = 'Apellido';
$lang['administration_users_username'] = 'Nombre de usuario';
$lang['administration_users_password'] = 'Contraseña';
$lang['administration_users_role'] = 'Perfil';
$lang['administration_users_email'] = 'Correo Electrónico';
$lang['administration_users_cellPhone'] = 'Teléfono / Celular';
$lang['administration_users_cellPhone_placeholder'] = 'Sin cero (0), con codigo de área y quince (15)';
$lang['administration_users_phone2'] = 'Teléfono 2';
$lang['administration_users_state'] = 'Provincia';
$lang['administration_users_city'] = 'Localidad / Municipio';
$lang['administration_users_address'] = 'Dirección';
$lang['administration_users_delete_areyousure'] = 'Esta seguro que quiere eliminar este usuario?';
$lang['administration_users_operator'] = 'Operador';
$lang['administration_users_superviser'] = 'Supervisor / TL';
$lang['administration_users_officemanager'] = 'Manager';
$lang['administration_users_administrator'] = 'Administrador';
$lang['administration_users_usernameexist'] = 'El Nombre de Usuario esta en uso.';
$lang['administration_users_passwordmatch'] = 'Las Contraseñas no coinciden.';
$lang['administration_users_emailexist'] = 'El Correo Electrónico esta en uso.';
$lang['administration_users_successmessage'] = 'Usuario creado correctamente.';
$lang['administration_users_editmessage'] = 'Usuario editado correctamente.';
$lang['administration_users_deletemessage'] = 'Usuario eliminado correctamente.';
$lang['administration_users_selectbackground'] = 'Cambiar fondo';
$lang['administration_users_selectbackground_message'] = 'Elige el fondo de Catnet que mas te guste';
$lang['administration_users_selectbackground_success'] = 'Fondo cambiado correctamente';
$lang['administration_users_state_notexists'] 	= 'La provincia no existe';
$lang['administration_users_city_notexists'] 	= 'La localidad no existe';

$lang['administration_users_configimages']='Imágenes añadidas correctamente.';

$lang['administration_users_deletemessage_sections'] = 'Si elimina este usuario se eliminaran todas las Divisiones que haya creado.';
$lang['administration_users_deletemessage_subsections'] = 'Si elimina este usuario se eliminaran todas las Secciones que haya creado.';
$lang['administration_delete_user'] = 'Eliminar Usuario';
$lang['administration_delete'] = 'Eliminar';
$lang['administration_details_user'] = 'Detalles del usuario';
$lang['administration_details'] = 'Detalles';
$lang['administration_edit_user'] = 'Editar usuario';
$lang['administration_users_passwordconfirm'] = 'Confirmar contraseña';
$lang['administration_users_config'] = 'Editar datos';
$lang['administration_users_editconfigmessage'] = 'Datos guardados correctamente.';
$lang['administration_users_configphoto'] = 'Cambiar foto';
$lang['administration_users_configpassword'] = 'Cambiar contraseña';
$lang['administration_users_alert'] = 'Para poder navegar por CATNET, tenés que cambiar tu contraseña por una diferente a cat*123. ¡Gracias!';
$lang['administration_users_data'] = 'Datos';
$lang['administration_users_photo'] = 'Foto';
$lang['administration_users_formatdate_error']='El formato de fecha no es correcto.';

$lang['administration_select_photo']='Seleccionar foto de perfil';
$lang['administration_select_photo_message']='Elegí la foto que más te gusta';
$lang['administration_select_photo_selected']='Imagen actual';
$lang['administration_select_photo_enable']='Imágenes disponibles';

$lang['administration_users_photos'] = 'Fotos';
$lang['administration_users_actualphoto'] = 'Foto';
$lang['administration_users_newphoto'] = 'Nueva foto';
$lang['administration_users_datamessage'] = 'Editar datos';
$lang['administration_users_photomessage'] = 'Cambiar foto';
$lang['administration_users_editphotomessage'] = 'Foto guardada correctamente.';
$lang['administration_users_passwordmessage'] = 'Cambiar contraseña';
$lang['administration_users_oldpassword'] = 'Contraseña actual';
$lang['administration_users_incorrectoldpassword'] = 'Contraseña actual incorrecta';
$lang['administration_users_newpassword'] = 'Contraseña nueva';
$lang['administration_users_editpasswordmessage'] = 'Contraseña guardada correctamente.';
$lang['administration_create_user'] = 'Crear Usuario';
$lang['administration_create_userrolenoexist'] = 'El Perfil no existe.';
$lang['administration_users_photo'] = 'Foto';
$lang['administration_create_usersitenoexist'] = 'El Site no existe';
$lang['administration_users_resetpassword'] = 'Resetear contraseña';
$lang['administration_users_resetpasswordmessage'] = 'Cambiar contraseñas olvidadas.';
$lang['administration_users_resetpasswordnouser'] = 'El Usuario no existe.';
$lang['administration_users_active'] = 'Activo';
$lang['administration_users_maxFiles']='Atención: Has alcanzado el límite de imágenes. Si quieres subir otra, elimina una.';
$lang['administration_configImages']='Configurar imágenes';
$lang['administration_schedule_error']='La hora de ingreso es incorrecta.';

$lang['administration_create'] = 'Crear';
$lang['administration_edit'] = 'Editar';

$lang['administration_usergroups'] = 'Equipos / Grupos de usuarios';
$lang['administration_usergroups_create'] = 'Crear Equipo / Grupo de usuarios';
$lang['administration_usergroups_message'] = 'Gestionar equipos / Grupos de usuarios';
$lang['administration_usergroups_successmessage'] = 'Equipo / Grupo de usuarios creado correctamente';
$lang['administration_usergroups_deletemessage'] = 'Equipo / Grupo de usuarios eliminado correctamente';
$lang['administration_usergroups_editmessage'] = 'Equipo / Grupo de usuarios editado correctamente';
$lang['administration_usergroups_name'] = 'Nombre';
$lang['administration_usergroups_users'] = 'Usuarios';
$lang['administration_usergroups_findusers'] = 'Buscar';
$lang['administration_usergroups_delete'] = 'Eliminar Equipo / Grupo de usuarios';
$lang['administration_usergroups_edit'] = 'Editar Equipo / Grupo de usuarios';
$lang['administration_usergroups_delete_areyousure'] = 'Esta seguro que quiere eliminar este Equipo / Grupo de usuarios?';
$lang['administration_usergroups_usersselects'] = 'Usuarios seleccionados';
$lang['administration_usergroups_nameerror'] = 'El campo Nombre es obligatorio.';
$lang['administration_usergroups_details'] = 'Detalles del Equipo / Grupo de usuarios';
$lang['administration_usergroups_shared'] = 'Compartido';

$lang['user_group_empty']='No hay grupos.';

$lang['administration_themes_successmessage'] = 'Tematica creada correctamente';
$lang['administration_themes_deletemessage'] = 'Tematica eliminada correctamente';
$lang['administration_themes_editmessage'] = 'Tematica editada correctamente';
$lang['administration_themes_savemessage'] = 'Tematica guardada correctamente';
$lang['administration_themes_empty'] = 'No se han encontrado Tematicas';
$lang['administration_themes_name'] = 'Nombre';
$lang['administration_themes_nameerror'] = 'El campo nombre es obligatorio.';
$lang['administration_themes_active'] = 'Activo';
$lang['administration_themes_nameexist'] = 'Ya existe una Tematica con este nombre';
$lang['administration_themes_create'] = 'Crear tematica';
$lang['administration_themes_images'] = 'Imagen/es';
$lang['administration_themes_imagesallowtypes'] = 'Tipos de archivos permitidos: gif, jpg, png';
$lang['administration_themes_details'] = 'Detalles de la tematica';
$lang['administration_themes_edit'] = 'Editar tematica';
$lang['administration_themes_delete'] = 'Eliminar tematica';
$lang['administration_themes_delete_areyousure'] = 'Esta seguro que quiere eliminar esta tematica?';
$lang['administration_themes_deleteimage_areyousure'] = 'Esta seguro que quiere eliminar esta imagen?';

$lang['suggestbox_title'] = 'Buzón de Sugerencias';

$lang['newsletter_title'] = 'Novedades';

$lang['press_title'] = 'Prensa';

$lang['general_roles'] = 'Perfiles';
$lang['roles_message'] = 'Gestionar Perfiles';
$lang['roles_successmessage'] = 'Perfil creado correctamente';
$lang['roles_deletemessage'] = 'Perfil eliminado correctamente';
$lang['roles_editmessage'] = 'Perfil editado correctamente';
$lang['roles_details'] = 'Detalles del Perfil';
$lang['roles_delete'] = 'Eliminar Perfil';
$lang['roles_edit'] = 'Editar Perfil';
$lang['roles_name'] = 'Nombre';
$lang['roles_permissions'] = 'Permisos';
$lang['roles_role'] = 'Perfil';
$lang['roles_empty'] = 'No hay Perfiles';
$lang['roles_nosetpermission'] = 'No se ha definido el permiso';
$lang['roles_truepermission'] = 'Tiene permiso';
$lang['roles_falsepermission'] = 'No tiene permiso';
$lang['roles_permission'] = 'Permiso';
$lang['roles_value'] = 'Valor';
$lang['roles_emptypermissions'] = 'No hay permisos';
$lang['roles_nameexist'] = 'Yo existe un perfil con este nombre.';
$lang['roles_deleteareyousure'] = 'Esta seguro que quiere eliminar este perfil?';
$lang['roles_selectall'] = 'Marcar todos';
$lang['roles_unselectall'] = 'Desmarcar todos';
$lang['roles_create'] = 'Crear Perfil';

$lang['general_sites'] = 'Sites';
$lang['general_site'] = 'Site';
$lang['sites_message'] = 'Gestionar Sites';
$lang['sites_successmessage'] = 'Site creado correctamente';
$lang['sites_deletemessage'] = 'Site eliminado correctamente';
$lang['sites_editmessage'] = 'Site editado correctamente';
$lang['sites_details'] = 'Detalles del Site';
$lang['sites_delete'] = 'Eliminar Site';
$lang['sites_edit'] = 'Editar Site';
$lang['sites_name'] = 'Nombre';
$lang['sites_empty'] = 'No hay Sites';
$lang['sites_deleteareyousure'] = 'Esta seguro que quiere eliminar este Site?';
$lang['sites_nameexist'] = 'Yo existe un site con este nombre';
$lang['sites_create'] = 'Crear site';


$lang['tools_title'] = 'Herramientas';
$lang['tools_excel'] = 'Tablas Excel';
$lang['tools_mytables'] = 'Mis Tablas';
$lang['tools_goto'] = 'Ir a la herramienta';
$lang['tools_excel_gototable'] = 'Ir a la tabla';
$lang['tools_excel_empty'] = 'No hay tablas'; 
$lang['tools_excel_createtable'] = 'Crear Tabla';
$lang['tools_excel_title'] = 'Titulo';
$lang['tools_excel_rows'] = 'Filas';
$lang['tools_excel_cols'] = 'Columnas';
$lang['tools_excel_size'] = 'Tamaño';
$lang['tools_excel_successmessage'] = 'Tabla creada correctamente';
$lang['tools_excel_deletemessage'] = 'Tabla eliminada correctamente';
$lang['tools_excel_editmessage'] = 'Tabla editada correctamente';
$lang['tools_excel_editdetails'] = 'Editar Detalles';
$lang['tools_excel_deleteareyousure'] = 'Esta seguro que quiere eliminar esta tabla?';
$lang['tools_notes'] = 'Notas adhesivas';
$lang['tools_notes_create'] = 'Crear Nota';
$lang['tools_notes_error'] = 'Ocurrio un error con la nota';
$lang['tools_notes_note'] = 'Nota';
$lang['tools_notes_color'] = 'Color';
$lang['tools_notes_deleteareyousure'] = 'Esta seguro que quiere eliminar esta nota?';
$lang['tools_message'] = 'Los datos que se vuelquen en esta herramienta son solamente para un control personal pero que no constituyen la base de liquidación de comisiones ni premios, y solamente se tendrán en cuenta las operaciones efectivamente concretadas conforme las bases de datos de nuestros clientes y bajo la plataforma asignada para tal fin.';
$lang['tools_popup'] = 'Enviar mensaje PopUp';
$lang['tools_popupreceiver'] = 'Destinatario';
$lang['tools_manual'] = 'Manual de usuario';
$lang['tools_notes_tip']='No olvides guardar tus notas.';
$lang['tools_notes_stack']='Apilar';


$lang['gamification_dashboard'] 			= 'Dashboard';
//Gamification admin

$lang['achievement_name'] = 'Nombre';
$lang['achievement_date']='Fecha creación';
$lang['achievement_description'] = 'Descripción';
$lang['achievement_quantity'] = 'Cantidad objetivo';
$lang['achievement_triggertype']='Tipo de evento';
$lang['achievement_trigger']='Evento';
$lang['achievement_image']='Imagen';
$lang['achievement_triggertype_manual']='Manual';
$lang['achievement_triggertype_auto']='Automatico';
$lang['achievement_select']='Seleccione los objetivos:';

$lang['medal_image']='Imagen';
$lang['medal_name']='Nombre';
$lang['medal_description']='Descripción';
$lang['medal_select']='Seleccione las medallas:';

$lang['prize_image']='Imagen';
$lang['prize_name']='Nombre';
$lang['prize_description']='Descripción';
$lang['prize_stock']='Stock';
$lang['prize_select']='Seleccione los premios:';
$lang['prize_stock_available']="Premios ganados";



$lang['challenge_image']="Imagen";
$lang['challenge_name']='Nombre';
$lang['challenge_description']='Descripción';
$lang['challenge_limitdate']='Fecha límite';


$lang['achievement_example_name']='Ej: Amante de la intranet.';
$lang['achievement_example_description']="Describa de que trata el objetivo. Ej: Inicie sesión en la intranet diez veces.";
$lang['achievement_example_quantity']='Indique la cantidad deseada para completar el objetivo. Ej: 10';

$lang['medal_example_name']='Ej: Medalla Cat.';
$lang['medal_example_description']='Ej: Reconocimiento al mejor vendedor. Felicitaciones!!.';


$lang['prize_example_name']='Ej: 30 minutos de Break.';
$lang['prize_example_description']='Ej: Vale por 30 minutos de descanso para cuando gustes.';



$lang['gamificationadmin_achievements']='Objetivos';
$lang['gamificationadmin_challenges']='Desafios';
$lang['gamificationadmin_medals']='Medallas';
$lang['gamificationadmin_prizes']='Premios';


$lang['gamificationadmin_create_achievement']='Crear objetivo';
$lang['gamificationadmin_edit_achievement']='Editar objetivo';

$lang['gamificationadmin_create_medal']='Crear medalla';
$lang['gamificationadmin_edit_medal']='Editar medalla';


$lang['gamificationadmin_create_prize']='Crear premio';
$lang['gamificationadmin_edit_prize']='Editar premio';

$lang['gamificationadmin_create_challenge']='Crear desafio';
$lang['gamificationadmin_edit_challenge']='Editar desafio';


$lang['gamification']='Gamification';

$lang['gamificationadmin_achievement_createsuccess']='Objetivo creado correctamente';
$lang['gamificationadmin_achievement_editsuccess']='Objetivo editado correctamente';
$lang['gamificationadmin_medal_createsuccess']='Medalla creada correctamente';
$lang['gamificationadmin_medal_editsuccess']='Medalla editada correctamente';
$lang['gamificationadmin_prize_createsuccess']='Premio creado correctamente';
$lang['gamificationadmin_prize_editsuccess']='Premio editado correctamente';
$lang['gamificationadmin_challenge_createsuccess']='Desafio creado correctamente';
$lang['gamificationadmin_challenge_editsuccess']='Desafio editado correctamente';


$lang['trigger_noexist']='El evento no existe.';

$lang['achievement_visibility']='¿Quien puede verlo?';
$lang['achievement_visibility_all']='Todos';
$lang['achievement_visibility_other']='Seleccionar quien';

$lang['achievement_visibility_details_title']='¿Quienes pueden ver este objetivo?';
$lang['challenge_visibility_details_title']='¿Quienes pueden ver este desafio?';
$lang['challenge_details_title']='Detalles del desafio';

$lang['general_noresults']='No hay resultados disponibles';

$lang['gamificationadmin_allcansee']='Todos los usuarios pueden ver este objetivo.';
$lang['gamificationadmin_medaldetailstitle']='Desafios nesesarios para ganar';

$lang['gamificationadmin_challenges_prevpage']='Anterior conjunto de desafios';
$lang['gamificationadmin_challenges_prevslide']='Ver anterior';
$lang['gamificationadmin_challenges_nextslide']='Ver siguiente';
$lang['gamificationadmin_challenges_nextpage']='Siguiente conjunto de desafios';
$lang['challenges_details_title']='Detalles del desafio';
$lang['challenge_nolimitdate']="sin límite";

$lang['general_turns']="Turnos";
$lang['turn_create']="Crear turno";
$lang['turn_edit']="Editar turno";
$lang['turn_delete']="Eliminar turno";
$lang['turn_error_exist']='Este nombre ya se encuentra en uso';
$lang['turn_name']='Nombre';
$lang['turn_deleteareyousure']='¿Estás seguro de eliminar este turno?';
$lang['turn_successmessage'] = 'Turno creado correctamente';
$lang['turn_deletemessage'] = 'Turno eliminado correctamente';
$lang['turn_editmessage'] = 'Turno editado correctamente';
$lang['turn_empty'] = 'No hay turnos';
$lang['administration_turn_no_exist']='El turno no existe';
$lang['no_turn_value']="No definido";
$lang['turn_checkIn']="Ingreso";
$lang['turn_checkOut']="Egreso";
$lang['turn_error_checkin_bigger_or_equal']='Recuerde que el ingreso no debe superar o igualar al horario de egreso';


$lang['general_campaigns']					= 'Áreas';
$lang['campaign_create']					= 'Crear Área';
$lang['campaign_edit']						= 'Editar Área';
$lang['campaign_delete']					= 'Eliminar Área';
$lang['campaign_error_exist']				= 'Este nombre ya se encuentra en uso';
$lang['campaign_name']						= 'Nombre';
$lang['campaign_deleteareyousure']			= '¿Estás seguro de eliminar esta Área?';
$lang['campaign_successmessage'] 			= 'Campaña creada correctamente';
$lang['campaign_deletemessage'] 			= 'Campaña eliminada correctamente';
$lang['campaign_editmessage'] 				= 'Campaña editada correctamente';
$lang['campaign_empty'] 					= 'No hay campañas';
$lang['campaign_url_server']				= 'Url de servidor';
$lang['campaign_url_server_example']		= 'Agregue la dirección web del CRM asociado a la campaña';
$lang['campaign_url_error']					= 'La url es incorrecta';
$lang['administration_campaign_no_exist']	= 'El área no existe';
$lang['no_campaign_value']					= 'No definido';
$lang['campaign_nosection']					= 'Sin division';
$lang['campaign_noclient']					= 'Sin cliente';
$lang['campaign_nourl']						= 'Sin Url';

$lang['gender_value_m']						= 'Masculino';
$lang['gender_value_f']						= 'Femenino';
$lang['gender_value_o']						= 'Otro';

$lang['administration_gender_no_exist']		= 'El género no existe.';


$lang['survey_surveys'] 		= 'Encuestas';
$lang['survey_activate'] 		= 'Activar';
$lang['survey_manual_charge'] 	= 'Carga manual';

$lang['general_clients']			= 'Clientes';
$lang['general_client']				= 'Cliente';
$lang['clients_create']				= 'Crear cliente';
$lang['clients_edit']				= 'Editar cliente';
$lang['clients_delete']				= 'Eliminar cliente';
$lang['clients_error_exist']		= 'Este nombre ya se encuentra en uso';
$lang['clients_name']				= 'Nombre';
$lang['clients_deleteareyousure']	= '¿Estás seguro de eliminar este cliente?';
$lang['clients_successmessage'] 	= 'Cliente creado correctamente';
$lang['clients_deletemessage'] 		= 'Cliente eliminado correctamente';
$lang['clients_editmessage'] 		= 'Cliente editado correctamente';
$lang['clients_empty'] 				= 'No hay clientes';
$lang['clients_no_exist']			= 'El cliente no existe';

$lang['teaming_agreement'] 				= 'Sumarme';
$lang['teaming_update'] 				= 'Actualizar mi monto';
$lang['teaming_send'] 					= 'Enviar solicitud';
$lang['teaming_suggest_institution']	= 'Sugerir institución';
$lang['teaming_suggest_institution_no_contact_data']	= '¡No ha ingresado ningun dato de contacto! Debe proporcionar al menos uno.';



$lang['general_administration'] 	= 'Administrar';
$lang['general_pending_forms'] 		= 'Solicitudes Pendientes';
$lang['general_joined_users'] 		= 'Usuarios Adheridos';
$lang['general_comm_channel'] 		= 'Canales de comunicación';
$lang['general_reports'] 			= 'Reportes';

$lang['general_manual'] 			= 'Carga Manual';
$lang['general_institutions'] 		= 'Instituciones sugeridas';
$lang['my_institutions'] 			= 'Mis instituciones sugeridas';
$lang['teaming_suggested_user'] 	= 'Mis instituciones sugeridas';


$lang['teaming_division_edit'] 		= 'Editar';
$lang['teaming_division_save'] 		= 'Guardar';
$lang['teaming_division_accept']	= 'Aceptar formulario';
$lang['teaming_division_reject']	= 'Rechazar formulario';
$lang['teaming_division_print'] 	= 'Imprimir formulario';
$lang['teaming_division_discharge'] = 'Dar de baja a la iniciativa';
$lang['teaming_division_add_table'] = 'Solicitudes de adhesión';
$lang['teaming_division_mod_table'] = 'Solicitudes de actualización de monto';
$lang['teaming_division_no_forms'] 	= 'No hay formularios para';
$lang['teaming_delete_not_empty'] 	= 'El campo de la observación no debe ser vacío.';




$lang['teaming_form_id'] = 'DNI';
$lang['teaming_form_company_id'] = 'Legajo';
$lang['teaming_form_name'] = 'Nombre y Apellido';
$lang['teaming_form_date'] = 'Fecha';
$lang['teaming_form_celnumber'] = 'Teléfono / Celular';
$lang['teaming_form_institution'] = 'Institución';
$lang['teaming_form_amount'] = 'Monto';
$lang['teaming_form_amount_letters'] = 'Monto en letras';
$lang['teaming_form_agree_terms'] = 'Politicas de Privacidad';
$lang['teaming_form_area_campaign'] = 'Área / Campaña';
$lang['teaming_form_area_observation'] = 'Observación';
$lang['teaming_popup_welcome'] = '¡<span class ="teaming-nav" style="font-size: 25px;">Teaming</span> llegó a CATnet!';
$lang['teaming_popup_accepted'] = 'La institución sugerida por vos ya fue aprobada. ¡Muchas gracias!';
$lang['teaming_popup_go_teaming'] = 'Ir a Teaming';
$lang['teaming_popup_go_my_institutions'] = 'Ir a mis sugerencias';

$lang['teaming_divisions_available'] = 'Divisiones en las que Teaming está disponible';
$lang['teaming_institution_suggestions'] = 'Sugerir instituciones';
$lang['teaming_form_antiquity'] = 'Antigüedad';
$lang['teaming_form_age'] = 'edad';
$lang['teaming_form_genre'] = 'género';
$lang['teaming_form_doc_tipe_number'] = 'tipo y número de documento';
$lang['teaming_form_site'] = 'Site';
$lang['teaming_form_observations'] = 'observaciones';
$lang['teaming_delete_observation_not_empty'] = 'El campo de la observación no debe ser vacío.';


$lang['teaming_joined_users'] = 'Usuarios adheridos';
$lang['teaming_joined_update_record'] = 'Historial';
$lang['teaming_joined_update_record_descrip'] = 'Historial de actualizaciones de monto de los usuarios';

$lang['teaming_user'] = 'Usuario de teaming en CATnet';

$lang['teaming_actions'] = 'Acciones';
$lang['select_columns'] = 'Seleccione las columnas que desea visualizar';

$lang['teaming_user_details'] 			= 'Detalles del usuario';
$lang['teaming_user_details_teaming'] 	= 'Datos de Teaming';
$lang['teaming_user_details_personal'] 	= 'Datos personales';
$lang['teaming_user_details_history']	= 'Historial del usuario';
$lang['teaming_user_details_export_pdf']	= 'Exportar PDF';
$lang['teaming_user_details_export_excel']	= 'Exportar Excel';

$lang['teaming_user_not_joined']	= 'El usuario no esta actualmente adherido a Teaming';

$lang['teaming_user_history_date_time']	= 'Fecha y hora';

$lang['teaming_user_history_action']	= 'Acción';

$lang['administration_users_teaming_amount'] = 'Monto a donar';

$lang['administration_users_teaming_amount_letters'] = 'Monto en letras';
$lang['administration_users_teaming_join_date'] = 'Fecha de adhesión';
$lang['administration_users_teaming_active'] = 'Activo';
$lang['administration_users_teaming_action'] = 'Acción';
$lang['general_users_teaming'] = 'Usuarios unidos a Teaming';
$lang['general_users_teaming_update'] = 'Usuarios que solicitaron modificar el monto a donar';

$lang['general_discharge_teaming'] 	= 'Baja a esp Teaming';
$lang['teaming_suggested_from'] 	= 'Institución sugerida por';


$lang['teaming_suggested_institutions'] = 'Instituciones sugeridas por los usuarios';



$lang['admin_discharge_teaming'] = 'Baja por esp rechazo';
$lang['admin_discharge_fired'] = 'Baja por esp desvinculación';

$lang['teaming_records'] = 'Historial';

$lang['teaming_details'] = 'Detalles del usuario';
$lang['teaming_history'] = 'Historial del usuario';

$lang['teaming_modal_observation_text'] = 'Ingrese una observación en el campo de texto antes de eliminar al usuario.';
$lang['teaming_delete_are_you_sure'] = '¿Esta seguro que desea dar de baja a este usuario?';

$lang['teaming_group'] = 'Adheridos';
$lang['teaming_not_group'] = 'No adheridos';

$lang['teaming_joined_table'] = 'Datos de los usuarios adheridos a Teaming';
$lang['teaming_not_joined_table'] = 'Usuarios no adheridos a Teaming';
$lang['administration_users_teaming_join_term'] = 'Fecha en que se unió';
$lang['general_days'] = 'dias';

$lang['teaming_min_amount'] 				= 'Monto mínimo:';
$lang['teaming_max_amount'] 				= 'Monto máximo:';
$lang['teaming_range_amount_search'] 		= 'Búsqueda por rango de monto';
$lang['teaming_min_date'] 					= 'Fecha de inicio:';
$lang['teaming_max_date'] 					= 'Fecha de fin:';
$lang['teaming_range_date_search'] 			= 'Búsqueda por rango de fechas';

$lang['teaming_filters_complete_name'] 		= 'Ingrese nombre y apellido';
$lang['teaming_filters_id'] 				= 'Ingrese dni';
$lang['teaming_filters_company_id'] 		= 'Ingrese legajo';
$lang['teaming_filters_area_campaign'] 		= 'Ingrese área / campaña';
$lang['teaming_filters_institution_name'] 	= 'Ingrese nombre de la institución';
$lang['teaming_filters_date_search'] 		= 'Ingrese rango de fechas';
$lang['teaming_filters_email'] 				= 'Ingrese Correo Electrónico';
$lang['teaming_filters_genre'] 				= 'Seleccione el género';
$lang['teaming_filters_civil_state'] 		= 'Seleccione el estado civil';
$lang['teaming_filters_site'] 				= 'Seleccione site';
$lang['teaming_filters_turn'] 				= 'Seleccione turno';
$lang['teaming_filters_mobile_number'] 		= 'Ingrese número de Teléfono / Celular';

$lang['teaming_send_message'] = 'Enviar mensaje';
$lang['teaming_send_newsletter'] = 'Enviar novedad';
$lang['teaming_send_survey'] = 'Enviar encuesta';

$lang['teaming_manual_join'] = 'Adhesión';
$lang['teaming_manual_update'] = 'Actualización de monto';
$lang['teaming_manual_join_explanation'] = 'Formulario de adhesión';
$lang['teaming_manual_update_explanation'] = 'Formulario de actualización de monto';

$lang['teaming_form_original_amount'] = 'Monto con el que se registró';

$lang['teaming_discharge_discharge_update_story'] = 'Historial de actualizaciones';


$lang['teaming_joined_records'] = 'Historial de usuarios';
$lang['teaming_discharge_join_date'] = 'Fecha de adhesión';
$lang['teaming_discharge_discharge_date'] = 'Fecha de baja';
$lang['teaming_discharge_discharge_reason'] = 'Razón';
$lang['teaming_export_excel'] = 'Exportar archivo de retenciones';
$lang['general_comm_report'] = 'Reportes';

$lang['teaming_joined_table_communication'] = 'Opciones de comunicación con usuarios adheridos a Teaming';
$lang['teaming_not_joined_table_communication'] = 'Opciones de comunicación con usuarios que no estan adheridos a Teaming';

$lang['teaming_reports'] 		= 'Usuarios';
$lang['teaming_newsletter'] 	= 'Novedades';
$lang['teaming_messages'] 		= 'Mensajes';
$lang['teaming_survey'] 		= 'Encuestas';

$lang['teaming_reports_reports'] 		= 'Reportes de Usuarios';
$lang['teaming_newsletter_reports'] 	= 'Reportes de Novedades';
$lang['teaming_messages_reports']		= 'Reportes de Mensajes';
$lang['teaming_survey_reports'] 		= 'Reportes de Encuestas';

$lang['general_comm_report_date_range_search'] = 'Búsqueda por rango de fecha';
$lang['general_comm_report_date_from'] = "Fecha desde:";
$lang['general_comm_report_date_until'] = "Fecha hasta:";
$lang['general_comm_report_joined'] = 'Total de usuarios registrados en Teaming';
$lang['general_comm_report_joined_last_month'] = 'Usuarios registrados en';
$lang['general_comm_report_discharged_last_month'] = 'Bajas reportadas en';
$lang['general_comm_report_discharged_in_date_range'] = 'Bajas reportadas en la fecha ingresada';
$lang['general_comm_report_max_amount'] = 'Monto máximo donado registrado en la fecha ingresada';
$lang['general_comm_report_max_updates'] = 'Cantidad máxima de actualizaciones de monto';
$lang['general_comm_report_amount_total'] = 'Monto total donado por los usuarios';
$lang['general_comm_report_avg_amount'] = 'Promedio de monto total donado por los usuarios';

$lang['general_comm_news_report_search_by_name'] = 'Buscar por titulo de novedad';
$lang['general_comm_news_report_total_news'] = 'Novedades enviadas';
$lang['general_comm_news_report_news'] = 'Datos de cada novedad';
$lang['general_comm_news_report_creation_date'] = 'Fecha de creación: ';
$lang['general_comm_news_report_show_recipients_data'] = 'Mostrar datos de destinatarios';
$lang['general_comm_news_report_hide_recipients_data'] = 'Ocultar datos';

$lang['general_comm_msg_report_search_by_name'] = 'Buscar por asunto del mensaje';
$lang['general_comm_msg_report_total_msg'] = 'Mensajes enviados';
$lang['general_comm_msg_report_messages'] = 'Datos de cada mensaje';
$lang['general_comm_msg_report_joined_msg'] = 'Usuarios adheridos';
$lang['general_comm_msg_report_not_joined_msg'] = 'Usuarios no adheridos';

$lang['teaming_form_no_story']				= 'El usuario no tiene actualizaciones de monto';
$lang['teaming_form_no_story_discharge'] 	= 'El usuario no realizó actualizaciones';

$lang['general_comm_msg_report_recipients'] 	= 'Datos de los destinatarios';
$lang['general_comm_msg_report_subject'] 		= 'Asunto: ';
$lang['general_comm_msg_report_body'] 			= 'Cuerpo del mensaje: ';
$lang['general_comm_msg_recipient_name'] 		= 'Nombre del destinatario';
$lang['general_comm_msg_recipient_saw'] 		= 'Mensaje visto';
$lang['general_comm_msg_recipient_isJoinedNow'] = 'Pertenece a Teaming';


$lang['general_comm_srv_name'] = 'Nombre de la encuesta';
$lang['general_comm_srv_data'] = 'Opciones';

$lang['report'] 	= 'Reporte';
$lang['details'] 	= 'Detalles';

$lang['teaming_form_celnumber_CodZone'] = 'código de país';
$lang['teaming_form_celnumber_CodArea'] = 'código de área';

$lang['teaming_form_load_type'] 		= 'Tipo de carga';
$lang['teaming_form_load_type_manual'] 	= 'online';
$lang['teaming_form_load_type_online'] 	= 'manual';

$lang['general_comm_report_joined_this_month'] = 'Usuarios registrados este mes';
$lang['general_comm_report_discharged_this_month'] = 'Bajas reportadas este mes';

$lang['general_comm_report_max_updates_last_month'] = 'Actualizaciones aprobadas en ';

$lang['general_comm_report_max_updates_in_date_range'] = 'Actualizaciones aprobadas en la fecha ingresada';

$lang['general_comm_report_joined_today'] = 'Usuarios registrados en la fecha ingresada';

$lang['teaming_filters'] 			= 'Filtros';
$lang['teaming_filters_selection'] 	= 'Seleccione filtros a aplicar: ';
$lang['teaming_apply_filters'] 		= 'Aplicar filtros';
$lang['teaming_columns'] 			= 'Columnas';

$lang['teaming_intro_first_paragraph']	= '<span class="teaming-color-panel-letters">Teaming</span> es una iniciativa solidaria internacional por la que los empleados de <span style="font-family:"Segoe UI Negrita"; font-weight:="bolder;">CAT Technologies</span> donan de forma voluntaria un monto de su sueldo cada mes.<br><br>
				Adicionalmente, por cada peso donado, <span style= "font-family:"Segoe UI Negrita"; font-weight:="bolder;">CAT</span> duplica dicho monto para incrementar la donación a instituciones de bien público. <br>
				Las instituciones pueden ser sugeridas por vos. Tu aporte es muy valioso para nosotros.
				<br><br>';
$lang['teaming_intro_second_paragraph']	= 'Teaming forma parte de nuestras actividades de RSE (Responsabilidad Social Empresaria) desde el año 2010 y tiene como pilar fundamental fomentar el trabajo en equipo y la cooperación.<br><br>
					De este modo, sumamos entre todos esfuerzos para ayudar a quienes más lo necesitan.<br><br>';

$lang['teaming_intro_third_paragraph']	= 'Podés sumarte a <span class ="teaming-color-panel-letters">Teaming</span> completando el formulario en <span class ="teaming-color-panel-letters">“SUMARME”</span> o podés modificar el importe de tu donación accediendo al formulario de <span class ="teaming-color-panel-letters">“ACTUALIZAR MI MONTO”.</span>
				';
$lang['teaming_intro_last_paragraph']	= '<span class="teaming-color-panel-letters">¡Participá! ¡Entre todos unimos a muchos por muy poco!</span>';

$lang['teaming_form_company_id'] = 'Legajo nº';

$lang['teaming_form_agreement']			= 'Formulario de adhesión a Teaming';
$lang['teaming_form_update']			= 'Formulario de Actualización de mi Monto a Donar';
$lang['teaming_form_first_paragraph']	= 'Teaming es una iniciativa solidaria internacional de micro donaciones por la que los empleados de CAT Technologies donan de forma voluntaria un monto de su sueldo cada mes. Adicionalmente, por cada peso donado, CAT duplica dicho monto para incrementar la donación a instituciones de bien público. Las instituciones pueden ser sugeridas por vos. Tu aporte es muy valioso para nosotros.<br><br>
					Teaming forma parte de nuestras actividades de RSE (Responsabilidad Social Empresaria) desde el año 2010 y tiene como pilar fundamental fomentar el trabajo en equipo y la cooperación.<br><br>De este modo, sumamos entre todos esfuerzos para ayudar a quienes más lo necesitan.<br><br>';


$lang['teaming_form_update_exclamation']	= 'Actualizá el monto de tu donación a través de este formulario.<br><br>';
$lang['teaming_form_agreement_exclamation']	= 'Sumate a Teaming a través de este formulario.<br><br>';
$lang['teaming_form_final_exclamation']		= '¡Entre todos unimos a muchos por muy poco!<br>';


$lang['teaming_form_mobile_number'] 	 	 = 'Teléfono / Celular';
$lang['Teaming_suggestion_message_body']	 = 'Hola,<br>
Por favor indicanos el nombre de la institución que deseas sugerir y algún dato adicional, como teléfono, email u otro medio de contacto.<br>
¡Gracias por tu participación!<br>'	;
$lang['teaming_accepted_suggestion'] 	 	 = '¡Gracias por tu sugerencia!';
$lang['teaming_suggestion_date'] 		 = 'Fecha en que se realizó la sugerencia';


$lang['teaming_form_accepted'] = '¡Gracias por tu interés!<br>Nos pondremos en contacto a la brevedad.';
$lang['teaming_form_institution_suggestion'] = 'Si estás interesado en sugerir una institución, por favor incluí los datos a continuación';



//SectionId
$lang['teaming_sign_title_section_2']	= 'CAT Technologies Argentina S. A.';
$lang['teaming_sign_title_section_3']	= 'CAT Technologies Argentina S. A.';
$lang['teaming_sign_title_section_4'] 	= 'ADVAL S.A';
$lang['teaming_sign_title_section_5'] 	= 'CAT Technologies Argentina S. A.';
$lang['teaming_sign_title_section_6'] 	= 'CAT Technologies Customer Experiences S.A.';

//SiteId
$lang['teaming_sign_body_site_2'] 	= 'Tte. Gral. Perón 867, C.A.B.A.<br>C1038AAQ - Buenos Aires, Argentina.';
$lang['teaming_sign_body_site_3'] 	= 'Bartolomé Mitre 853 PB<br>C1036AAO - Buenos Aires, Argentina.';
$lang['teaming_sign_body_site_4'] 	= 'Av. Universitaria S/N ITC<br>Universidad de La Punta<br>La Punta (5710) - San Luis - Argentina';
$lang['teaming_sign_body_site_5'] 	= 'Bartolomé Mitre 853 PB<br>C1036AAO - Buenos Aires, Argentina.';
$lang['teaming_sign_body_site_6'] 	= 'Tte. Gral. Perón 867. C.A.B.A.<br>C1038AAQ - Buenos Aires, Argentina. ';


$lang['survey_insert_user']     = 'Ingrese el nombre del usuario al cual desea cargar la encuesta'; 
$lang['survey_insert_alert']    = 'Recuerde que una vez ingresados los datos, estos no pueden ser modificados';
$lang['survey_manual_charge']   = 'Carga manual';

$lang['general_notifications']               = 'Notificaciones';
$lang['attached_file']                       = 'Archivo Adjunto';
$lang['general_birth_date_range_search']     = 'Busqueda por rango de fecha de cumpleaños';
$lang['general_start_date'] 				 = 'Fecha de inicio';
$lang['general_end_date'] 					 = 'Fecha de fin';
$lang['notifications_confirmation'] 		 = 'Confirmo que he visto la notificación';
$lang['notifications_continue'] 		 	 = 'Ir a catnet';
$lang['teaming_join_no_forms']			 	 = 'No hay formularios de adhesión';
$lang['general_search_user'] 				 = 'Buscar usuario';
$lang['suscribed_teaming'] 					 = 'Adherido a Teaming';
$lang['general_select_genre'] 				 = 'Seleccione género';
$lang['apply_filter'] 						 = 'Aplicar filtros';
$lang['general_select_turn'] 			     = 'Seleccione turno:';
$lang['general_turn_m'] 					 = 'Mañana';
$lang['general_turn_t'] 					 = 'Tarde';
$lang['general_turn_n'] 					 = 'Noche';
$lang['general_turn_f'] 					 = 'Full Time';

$lang['add_users'] 							 = 'Agregar usuarios';
$lang['add_to_chat'] 						 = 'Agregar al chat los usuarios seleccionados';
$lang['add_success'] 						 = 'Agregado correctamente';

$lang['general_faq'] 						 = 'Preguntas frecuentes';

$lang['help_wecat'] 						 = 'Nuestros valores en CATnet';
$lang['help_teaming'] 						 = 'Teaming, iniciativa solidaria';
$lang['help_faq'] 							 = '¿Tenes alguna duda? Aquí podes encontrar la respuesta.';
$lang['help_inbox'] 						 = 'Ver los mensajes recibidos,enviados o enviar un mensaje';
$lang['help_newsletter'] 					 = '¡Mantenete informado de todo lo nuevo en Cat!';
$lang['help_suggestbox'] 					 = 'Tu opinión nos importa y esta sección está pensada en vos';
$lang['help_press']   						 = 'Toda la prensa de nuestra empresa';
$lang['help_chat'] 							 = 'El chat te sirve para mantenerte comunicado con la gente en nuestra página';
$lang['help_section'] 						 = 'Herramientas de la sección';
$lang['help_tools'] 						 = 'Herramientas generales de CATnet';
$lang['help_surveys'] 						 = 'Ver o administrar las encuestas';
$lang['help_corporatedocs'] 				 = 'Administración de documentos corporativos';
$lang['help_notifications'] 				 = 'Administración de las notificaciones en CATnet';
$lang['help_referred'] 						 = 'En esta seccion podes enviar a tus referidos y ganar grandes premios';
$lang['help_activities'] 					 = 'Ver o administrar actividades';


$lang['general_backgrounds'] 				 = 'Fondos';
$lang['administration_backgrounds_message']  = 'Gestionar los fondos para los usuarios';
$lang['background_add'] 					 = 'Agregar Fondo';
$lang['background_add_new'] 				 = 'Agregar nuevo fondo';
$lang['background_deleteareyousure'] 		 = '¿Estas seguro de eliminar este fondo?';
$lang['background_empty'] 					 = 'No hay fondos agregados';
$lang['background_added'] 					 = '¡Fondo agregado correctamente!';
$lang['admin_background'] 					 = 'Gestión de fondos';
$lang['teaming_update_no_forms']			 = 'No hay formularios de actualización de monto';

$lang['question_question']							= 'Pregunta';
$lang['question_security_questions']				= 'Preguntas de seguridad';
$lang['question_answer']							= 'Respuesta';
$lang['question_create_success']					= 'Pregunta de seguridad creada correctamente.';
$lang['question_edit_success'] 						= 'Pregunta de seguridad editada correctamente.';
$lang['question_delete_success'] 					= 'Pregunta de seguridad eliminada correctamente.';
$lang['question_error_no_available']				= 'La pregunta de seguridad ya no se encuentra disponible.';
$lang['question_error_general']						= 'No se pudo realizar la acción solicitada. Inténtelo más tarde.';
$lang['question_error_in_use']						= 'La acción solicitada no pudo realizarse debido a que la pregunta de seguridad se encuentra en uso.';
$lang['question_error_no_access']					= 'Usted no tiene acceso a realizar acciones en esta pregunta.';
$lang['question_error_limit_exceeded']				= 'Ha superado el límite de preguntas de seguridad permitidas.';
$lang['question_user_add_success']					= 'La pregunta de seguridad se agregó correctamente.';
$lang['question_user_edit_success']					= 'La pregunta de seguridad se editó correctamente.';
$lang['question_user_delete_success']				= 'La pregunta de seguridad se eliminó correctamente.';
$lang['question_security_question_id']				= 'No es posible realizar la acción, faltan campos necesarios.';
$lang['question_incorrect_answer']					= 'La respuesta ingresada es incorrecta.';
$lang['question_config_details']					= 'Configura preguntas de seguridad para reestablecer tu contraseña.';
$lang['questions_my_questions_empty'] 				= 'No posees ninguna pregunta de seguridad.';
$lang['question_created_by'] 						= 'Creado por';
$lang['question_create'] 							= 'Crear pregunta de seguridad';
$lang['question_edit'] 								= 'Editar pregunta de seguridad';
$lang['question_delete'] 							= 'Eliminar pregunta de seguridad';
$lang['question_questions_empty'] 					= 'No hay preguntas de seguridad creadas.';
$lang['question_name_example'] 						= 'Consejo: La pregunta debe poseer una respuesta específica para el usuario.';
$lang['question_error_restore_password']			= 'Ocurrió un error al reestablecer su contraseña. Comuníquese con su administrador.';
$lang['question_are_you_sure_delete']				= '¿Estás seguro que deseas eliminar la pregunta de seguridad?';
$lang['question_question_exist']					= 'La pregunta de seguridad ya existe.';
$lang['question_select_one'] 						= 'Seleccione una pregunta de seguridad';
$lang['question_answer_example'] 					= 'Utilice una respuesta que solo usted sepa.';
$lang['question_selected_question_exist_already'] 	= 'No puede utilizar la misma pregunta mas de una vez. Por favor seleccione otra.';
$lang['question_question_no_exist']					= 'Una de las preguntas de seguridad seleccionada no existe.';
$lang['questions_user_edit_success'] 				= 'Las preguntas de seguridad se guardaron correctamente.';
$lang['question_select_valid_question'] 			= 'Por favor seleccione una Pregunta válida.';

$lang['teaming_suggest_name']			 	 					= 'Nombre de la institución';
$lang['teaming_suggest_contact_data']		 					= 'Datos de la institución';
$lang['teaming_suggest_contact_data_social_network_web_page']	= 'Correo Electrónico / Página Web / Redes Sociales';
$lang['teaming_suggest_contact_data_referrer']		 			= 'Nombre del referente';
$lang['teaming_suggest_adm_suggested_by'] 						= 'Sugerida por';

$lang['administration_themes_resetimage_areyousure'] = 'Esta acción borrará su actual foto, está seguro?';
$lang['administration_delete_image_text'] 			 = 'Eliminar foto';
$lang['items_notice_notavailable'] 					 = 'No se pueden crear nuevos anuncios ya que se excedió el limite de 100. Elimine algunos anuncios para poder proseguir.';

$lang['general_showhide_content'] 					 = 'Mostrar/Ocultar contenido';
$lang['see_all']                 					 = 'Ver todos'; 
$lang['see_less']               					 = 'Ver menos';

$lang['referred_title'] 							 = 'Referidos';
$lang['referred_sended'] 							 = 'Referidos enviados';
$lang['referred_send'] 								 = 'Enviar referido';
$lang['referred_send_referred'] 					 = 'Enviar referido';
$lang['referred_message_body'] 						 = 'Aquí podes enviarnos los datos de tus referidos.';
$lang['referred_accepted'] 							 = 'Hemos recibido los datos de tu referido. ¡Muchas gracias!';
$lang['general_showhide_content'] 					 = 'Mostrar/Ocultar contenido';
$lang['referred_candidates']      					 = 'Lista de referidos';
$lang['referred_by']              					 = 'Persona que lo refirió';
$lang['referred_month']           					 = 'Mes en que fue referido';
$lang['referred_empty'] 		  					 = '¡No hay referidos!';
$lang['referred_desc'] 			  					 = 'Nuestro canal de referidos es el medio por el cual todos los empleados de CAT TECHNOLOGIES pueden referir a sus conocidos y/o amigos para que trabajen con nosotros. 
Si conoces a una persona con secundario completo y disponibilidad horaria, que se encuentra en búsqueda de trabajo, sólo tenés que enviarnos su nombre completo, nro. de DNI y teléfono de contacto, y nosotros nos pondremos en contacto para contarle nuestra propuesta.
';

$lang['activity_activities'] 				= 'Actividades / eventos';
$lang['activity_delete_confirm_message'] 	= 'Esta seguro que quiere eliminar la actividad';
$lang['activity_activate_confirm_message'] 	= 'Una vez habilitada la actividad no podra ser editada, ¿Desea continuar?';
$lang['attached_image'] 					= 'Imagen adjunta';

$lang['subcampaigns_index']         = 'Campañas';
$lang['subcampaign'] 				= 'Campaña';
$lang['subcampaigns_no_exist'] 		= 'La campaña seleccionada no existe';
$lang['subcampaigns_not_apply'] 	= 'No aplica';
?>
