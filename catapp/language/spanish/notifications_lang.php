<?php

defined('BASEPATH') OR exit('No direct script access allowed');
$lang['notifications_empty'] 					= 'No hay notificaciones';
$lang['notifications_create'] 					= 'Crear Notificación';
$lang['notifications_edit']                     = 'Editar Notificación';
$lang['notifications_deleteareyousure'] 	    = '¿Esta seguro que quiere eliminar esta notificación?';
$lang['notifications_corporatedoc'] 			= 'Notificaciones';
$lang['notifications_name'] 					= 'Nombre';
$lang['notification_title']                     = 'Notificación';
$lang['notifications_createmessage']            = 'Notificación correctamente creada';
$lang['notifications_editmessage']              = 'Notificación correctamente editada';
$lang['notifications_deletemessage']            = 'Notificación correctamente eliminada';
$lang['notifications_delete']                   = 'Eliminar notificación';
$lang['notifications_salutation']               = '¡Felicidades usted ha sido beneficiario de un voucher!';
$lang['notifications_user_empty']               = '¡No hay más notificaciones para mostrar!';

$lang['events_create']                          = 'Crear evento';
$lang['events_empty']                           = 'No hay eventos';
$lang['event_edit']                             = 'Editar evento';
$lang['event_delete']                           = 'Eliminar evento';
$lang['event_send']                             = 'Enviar evento';
$lang['event_sent']                             = 'Evento enviado';
$lang['event_deleteareyousure'] 	            = '¿Esta seguro que quiere eliminar este evento?';
$lang['events_file_allowtypes']                 = 'Tipos de archivos permitidos: jpg,png,gif';
$lang['events_file_allowsizes']                 = 'Tamaño máximo : 3megabytes (MB)';
$lang['event_created']                          = 'Evento correctamente creado';
$lang['event_edited']                           = 'Evento correctamente editado';
$lang['event_deletemessage']                    = 'Evento eliminado';
$lang['event_photo']                            = 'Foto actualmente guardada';
$lang['event_reminders']                        = 'Recordatorios';
$lang['event_show']                             = 'Mostrar evento';
$lang['event_wrong_date']                       = '¡La fecha de inicio no se puede editar!';
$lang['event_size']                             = 'El tamaño del popup dependerá de las propiedades de la imagen subida';

$lang['start_date_help']                        = 'La fecha de inicio no puede ser modificada, ¡el evento ya ha iniciado!';
$lang['reminders_help']                         = 'Con el calendario se pueden seleccionar varias fechas, cada fecha se tomará como un recordatorio. La fecha máxima es la fecha de  fin seleccionada. Por defecto se guarda un recordatorio con el valor de la fecha inicial.';
$lang['invalid_date']                           = 'Se ha ingresado una fecha inválida';
$lang['reminders']                              = 'Recordatorios';
$lang['delete_filter']                          = 'Borrar filtro';

$lang['event_report']                           = 'Ver reporte del evento';
$lang['event_name']                             = 'Nombre:';
$lang['events_report']                          = 'Ver reporte del evento';
$lang['events_total']                           = 'Total';
$lang['events_users_empty']                     = 'No hay usuarios';
?>