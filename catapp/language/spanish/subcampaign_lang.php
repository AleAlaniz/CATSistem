<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$lang['subcampaign_name']               = 'Nombre';
$lang['subcampaign_successmessage']     = 'Campaña correctamente creada';
$lang['subcampaign_editmessage']        = 'Campaña correctamente editada';
$lang['subcampaign_deletemessage']      = 'Campaña correctamente eliminada';
$lang['subcampaign_delete_areyousure']  = '¿Está seguro que desea eliminar esta Campaña?';
?>