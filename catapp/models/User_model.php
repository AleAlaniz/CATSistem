<?php
Class User_model extends CI_Model {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		$this->lang->load('login_lang');
	}

	public function getDefaultPassword()
	{
		return 'cat*123';
	}

	public function CreateUser()
	{
		//Reglas userPersonalData
		$this->form_validation->set_rules('name', 'lang:administration_users_name', 'required|max_length[128]');
		$this->form_validation->set_rules('lastName', 'lang:administration_users_lastname', 'required|max_length[128]');

		//Reglas users
		$this->form_validation->set_rules(
			'userName', 
			'lang:login_username', 
			'required|min_length[4]|max_length[32]|is_unique[users.userName]',
			array('is_unique' => $this->lang->line('administration_users_usernameexist'))
		);

		$this->form_validation->set_rules('password', 'lang:login_password', 'min_length[6]|max_length[32]|required');
		$this->form_validation->set_rules('passwordConfirm', 'lang:administration_users_passwordconfirm', 'required|matches[password]',
			array(
				'matches'     => $this->lang->line('administration_users_passwordmatch')
			));

		$sql 	= "SELECT * FROM roles ORDER BY name";
		$roles 	= $this->db->query($sql)->result();
		$Ids = '';
		foreach ($roles as $value) {
			$Ids .= $value->roleId.',';
		}

		$this->form_validation->set_rules('role', 'lang:administration_users_role','required|in_list['.$Ids.']',
			array('in_list' => $this->lang->line('administration_create_userrolenoexist')));


		$sql = "SELECT * FROM sites ORDER BY name";
		$sites = $this->db->query($sql)->result();
		$Ids = '';
		foreach ($sites as $value) {
			$Ids .= $value->siteId.',';
		}


		$this->form_validation->set_rules('site', 'lang:general_site','required|in_list['.$Ids.']',
			array('in_list' => $this->lang->line('administration_create_usersitenoexist')));


		$sql = "SELECT * FROM sections ORDER BY name";
		$sections = $this->db->query($sql)->result();
		$sectionsIds = '';
		foreach ($sections as $value) {
			$sectionsIds .= $value->sectionId.',';
		}
		$this->form_validation->set_rules('section', 'lang:general_section','required|in_list['.$sectionsIds.']',
			array('in_list' => $this->lang->line('administration_users_sectionerror')));
		
		$this->form_validation->set_rules('active', 'lang:administration_users_active', 'in_list[active]');


		//Reglas userComplementaryData
		$this->form_validation->set_rules('userNumber', 'lang:users_usernumber', 'numeric|min_length[1]|max_length[6]|is_unique[userComplementaryData.userNumber]');


		if ($this->form_validation->run() == FALSE) {
			$query = array(
				'sections' 	=> $sections,
				'roles' 	=> $roles,
				'sites' 	=> $sites
			);

			$this->load->view('_shared/_administrationlayoutheader');
			$this->load->view('users/create', $query);
			$this->load->view(PRELAYOUTFOOTER);
		}
		else {
			$userInsert = array(
				'userName' 	=> htmlspecialchars($this->input->post('userName')),
				'password' 	=> crypt($this->input->post('password')),	
				'roleId' 	=> $this->input->post('role'), 
				'sectionId' => $this->input->post('section'),
				'siteId' 	=> $this->input->post('site'),
				'active' 	=> $this->input->post('active') ? 1 : 0
			);
			$this->db->insert('users', $userInsert);

			$userId = $this->db->insert_id();

			$objectInsert = array(
				'timestamp' => time(),
				'userId' 	=> $userId
			);
			$this->db->insert('completeDataUsers', $objectInsert);

			$userPersonalData = array(
				'userId' 	=> $userId,
				'name' 		=> htmlspecialchars($this->input->post('name')),
				'lastName' 	=> htmlspecialchars($this->input->post('lastName'))
			);

			$this->db->insert('userPersonalData', $userPersonalData);

			$userComplementaryData = array(
				'userId' 		=> $userId,
				'userNumber' 	=> $this->input->post('userNumber')
			);
			$this->db->insert('userComplementaryData', $userComplementaryData);


			$config['upload_path']          = './catapp/_profilepictures/';
			$config['allowed_types']        = 'gif|jpg|png';
			$config['max_size']             = 10240;
			$config['file_ext_tolower']     = TRUE;
			$config['remove_spaces']     	= FALSE;

			if (isset($_FILES['userfile']) && $_FILES['userfile'] != NULL) {
				$config['file_name']= $userId.'-'.$this->session->UserId.'-'.time().'-'.$_FILES['userfile']['name'];
			}

			$this->load->library('upload', $config);
			
			if ($this->upload->do_upload('userfile')) {

				
				$userComplementaryData = array(
					'photo' 	=> $this->upload->data('file_name')
				);

				$this->db->where('userId', $userId);
				$this->db->update('userComplementaryData', $userComplementaryData);

				$objectAdd = array(
					'photo' => $this->upload->data('file_name'),
					'userId'=> $userId
				);


				$this->db->insert('imagesUser', $objectAdd);

				$config['image_library'] 	= 'gd2';
				$config['source_image'] 	= './catapp/_profilepictures/'.$this->upload->data('file_name');
				$config['create_thumb'] 	= FALSE;
				$config['maintain_ratio'] 	= TRUE;
				$config['width']         	= 200;
				$config['height']       	= 200;
				$config['new_image']       	= './catapp/_profilepictures/200x200-'.$this->upload->data('file_name');

				$this->load->library('image_lib', $config);
				$this->image_lib->resize();
			}


			$this->session->set_flashdata('userMessage', 'create');
			header('Location:/'.FOLDERADD.'/users');
		}
	}

	public function getUserphotos()
	{
		$sql 			= "SELECT imageId FROM imagesUser WHERE UserId=?";
		$data 			= new StdClass();
		$data->images 	= $this->db->query($sql,$this->session->UserId)->result();
		$data->UserId 	= $this->session->UserId;

		$this->load->view(PRELAYOUTHEADER);
		$this->load->view('users/selectphoto', $data);
		$this->load->view(PRELAYOUTFOOTER);
	}

	public function selectImage()
	{
		$imageId=$this->input->get('imageId');

		if ($this->input->get('imageId'))
		{
			$sql="SELECT photo,UserId from imagesUser where imageId=?";
			$query=$this->db->query($sql,$imageId)->row();

			if(isset($query) && count($query) > 0)
			{
				$userEdit= array('photo' => $query->photo );

				$this->db->where('userId', $query->UserId);
				$this->db->update('userComplementaryData', $userEdit);
			}
		}

		header('Location:/'.FOLDERADD.'/users/config?cmd=selectphoto');
	}

	public function GetProfilePhotos()
	{
		$this->load->helper('download');

		if ($this->input->get('imageId')) {

			$sql = "SELECT * FROM imagesUser WHERE imageId = ?";
			$object = array($this->input->get('imageId'));
			$query = $this->db->query($sql,$object)->row();
			if (isset($query))
			{

				if (isset($query->photo) && $query->photo != '-') {
					if ($this->input->get('wah'))
					{
						$wha = $this->input->get('wah').'x'.$this->input->get('wah').'-';
					}
					if (file_exists('./catapp/_profilepictures/'.$wha.$query->photo)) {
						force_download('./catapp/_profilepictures/'.$wha.$query->photo, NULL);
					}
					else
					{
						force_download('./catapp/_profilepictures/'.$query->photo, NULL);
					}
				}
				else
				{
					show_404();
					force_download('./catapp/libraries/images/offline_user.png', NULL);
				}
			}
		}
		else
		{	
			force_download('./catapp/libraries/images/offline_user.png', NULL);
		}
	}

	public function addImageToUser()
	{
		$userId=$this->uri->segment(3);

		$sql="select * from imagesUser where UserId=?";
		$data=new StdClass();
		$data->images=$this->db->query($sql,$userId)->result();
		$data->count=count($data->images);
		$data->error=NULL;



		$uploadOK=FALSE;
		$ok=FALSE;

		if($data->count < 5)
		{
			$ok=TRUE;		
		}
		else
		{
			$data->error=$this->lang->line('administration_users_maxFiles');
		}

		if(isset($_FILES['userfiles']) && $ok)
		{

			$config['upload_path']          = './catapp/_profilepictures/';
			$config['allowed_types']        = 'gif|jpg|png';
			$config['max_size']             = 10240;
			$config['file_ext_tolower']     = TRUE;
			$config['remove_spaces']     	= FALSE;


			$config['image_library'] = 'gd2';
			$config['create_thumb'] = FALSE;
			$config['maintain_ratio'] = TRUE;
			$config['width']         = 200;
			$config['height']       = 200;


			$this->load->library('upload',$config);
			$this->load->library('image_lib',$config);

			$count = count($_FILES['userfiles']['name']);
			$files=$_FILES;

			for($i=0; $i<$count; $i++)
			{	
				$_FILES['userfiles']['name']= $files['userfiles']['name'][$i];
				$_FILES['userfiles']['type']= $files['userfiles']['type'][$i];
				$_FILES['userfiles']['tmp_name']= $files['userfiles']['tmp_name'][$i];
				$_FILES['userfiles']['error']= $files['userfiles']['error'][$i];
				$_FILES['userfiles']['size']= $files['userfiles']['size'][$i];	

				$timestampFile = time();
				$config['file_name']= $this->session->UserId.'-'.$timestampFile.'-'.$_FILES['userfiles']['name'];

				$this->upload->initialize($config);

				if ($this->upload->do_upload('userfiles'))
				{
					$objecttoInsert = array(
						'photo' => $this->upload->data('file_name'),
						'UserId' => $userId, 
					);
					$this->db->insert('imagesUser', $objecttoInsert);

					$config['source_image'] = './catapp/_profilepictures/'.$this->upload->data('file_name');		
					$config['new_image']       = './catapp/_profilepictures/200x200-'.$this->upload->data('file_name');

					$this->image_lib->initialize($config);
					$this->image_lib->resize();
					$uploadOk=TRUE;
				}
				else
				{	
					continue;
				}
			}

			if($uploadOk)
			{
				$this->session->set_flashdata('userMessage', 'configImages');			
			}
			header('Location:/'.FOLDERADD.'/users/userimages/'.$userId);

		}
		else
		{	
			$sql 	= "SELECT u.userId, up.name, up.lastName FROM users AS u INNER JOIN (SELECT name, lastName, userId FROM userPersonalData) AS up ON u.userId = up.userId WHERE u.userId = ?";
			$user 	= array($this->uri->segment(3));
			$query 	= $this->db->query($sql,$user)->row();

			if ($query) {
				$data->navBar = $this->load->view('users/_navbar', $query, TRUE);

				$this->load->view('_shared/_administrationlayoutheader');
				$this->load->view('users/configimages', $data);
				$this->load->view(PRELAYOUTFOOTER);
			}
			else {
				show_404();
			}

		}
	}

	public function DeleteUser()
	{
		if ($this->uri->segment(3)) {
			$sql = "
			SELECT u.userId, up.name, up.lastName, u.userName, u.active, r.name AS 'role', s.name AS 'section', si.name AS 'site'
			FROM users AS u 
			INNER JOIN (SELECT name, lastName, userId 	FROM userPersonalData) 	AS up 	ON u.userId = up.userId
			INNER JOIN (SELECT name, roleId 	FROM roles) 					AS r 	ON u.roleId = r.roleId
			INNER JOIN (SELECT name, sectionId 	FROM sections) 					AS s 	ON u.sectionId = s.sectionId
			INNER JOIN (SELECT name, siteId 	FROM sites) 					AS si 	ON u.siteId = si.siteId
			WHERE u.userId = ?
			";

			$user = array($this->uri->segment(3));
			$query = $this->db->query($sql,$user)->row();
			if (isset($query))
			{
				if ($this->input->post('userId') && $this->input->post('userId') == $query->userId ) 
				{
					$userDelete = array(
						'userId' => $this->input->post('userId')
					);

					$sql="SELECT photo
					FROM imagesUser
					WHERE UserId=?";

					$photos=$this->db->query($sql,$userDelete)->result();

					foreach($photos as $photo)
					{
						unlink('./catapp/_profilepictures/'.$photo->photo);
						unlink('./catapp/_profilepictures/200x200-'.$photo->photo);
					}

					$this->db->delete('users', $userDelete);
					$this->session->set_flashdata('userMessage', 'delete');
					insert_audit_logs('users','DELETE',$query);
					
					header('Location:/'.FOLDERADD.'/users');
				}
				else{
					$query->navBar = $this->load->view('users/_navbar', $query, TRUE);
					$this->load->view('_shared/_administrationlayoutheader');
					$this->load->view('users/delete', $query);
					$this->load->view(PRELAYOUTFOOTER);
				}
			}
			else
			{
				show_404();
			}
		}
		else{
			show_404();
		}
	}

	public function DeletePhoto()
	{

		if ($this->uri->segment(3)) {
			$sql = "SELECT * FROM imagesUser WHERE imageId = ?";
			$query = $this->db->query($sql,array($this->uri->segment(3)))->row();
			if (isset($query))
			{
				$objecttoDetele = array(
					'imageId' => $query->imageId
				);
				$this->db->delete('imagesUser', $objecttoDetele);
				unlink('./catapp/_profilepictures/'.$query->photo);
				unlink('./catapp/_profilepictures/200x200-'.$query->photo);

				$sql 	= "SELECT userId FROM userComplementaryData where photo = ?";
				$data 	= $this->db->query($sql,$query->photo)->row();

				if(isset($data->userId))
				{
					$objectEdit = array('photo' =>'-' );

					$this->db->where('userId', $data->userId);
					$this->db->update('userComplementaryData', $objectEdit);
				}

				header('Location:/'.FOLDERADD.'/users/userimages/'.$query->UserId);	
			}
			else 
			{
				show_404();
			}
		}
		else{
			show_404();
		}
	}

	public function ResetPhoto()
	{
		if ($this->uri->segment(3)) {
			$userId = $this->uri->segment(3);
			$query = "SELECT userId FROM users WHERE userId = $userId";
			$res = $this->db->query($query)->row();
			if(isset($res))
			{
				$objectEdit = array('photo'=>'-');
				
				$this->db->where('userId', $res->userId);
				$this->db->update('userComplementaryData', $objectEdit);

				header('Location:/'.FOLDERADD.'/users/config/');	
			}
			else {
				show_404();
			}
		}
		else {
			show_404();
		}
	}

	public function DetailsUser()
	{
		if ($this->uri->segment(3))
		{
			$sql = "
			SELECT u.userId, up.name, up.lastName, u.userName, u.active, r.name AS 'role', s.name AS 'section', si.name AS 'site'
			FROM users AS u 
			INNER JOIN (SELECT name, lastName, userId FROM userPersonalData) 	AS up 	ON u.userId = up.userId
			INNER JOIN (SELECT name, roleId 	FROM roles) 					AS r 	ON u.roleId = r.roleId
			INNER JOIN (SELECT name, sectionId 	FROM sections) 					AS s 	ON u.sectionId = s.sectionId
			INNER JOIN (SELECT name, siteId 	FROM sites) 					AS si 	ON u.siteId = si.siteId
			WHERE u.userId = ?
			";

			$query = $this->db->query($sql, $this->uri->segment(3))->row();

			if (isset($query))
			{
				$query->navBar = $this->load->view('users/_navbar', $query, TRUE);
				$this->load->view('_shared/_administrationlayoutheader');
				$this->load->view('users/details', $query);
				$this->load->view(PRELAYOUTFOOTER);
			}
			else
			{
				show_404();
			}
		}
		else{
			show_404();
		}
	}

	public function EditUser()
	{
		if ($this->uri->segment(3)) {
			$sql = "
			SELECT u.userId, up.name, up.lastName, u.userName, u.active, u.roleId, u.sectionId, u.siteId, uc.userNumber
			FROM users AS u 
			INNER JOIN (SELECT name, lastName, userId 				FROM userPersonalData WHERE userId = ?) 		AS up ON u.userId = up.userId
			INNER JOIN (SELECT photo, userNumber, userId 			FROM userComplementaryData WHERE userId = ?) 	AS uc ON u.userId = uc.userId
			WHERE u.userId = ?
			";
			$query = $this->db->query($sql, array($this->uri->segment(3), $this->uri->segment(3), $this->uri->segment(3)))->row();
			if (isset($query))
			{
				if ($this->input->post('userId') && $this->input->post('userId') == $query->userId ) {

					$this->form_validation->set_rules('name', 'lang:administration_users_name', 'required|max_length[128]');
					$this->form_validation->set_rules('lastName', 'lang:administration_users_lastname', 'required|max_length[128]');


					$userRules = 'required|min_length[4]|max_length[32]|is_unique[users.userName]';
					$userRulesMessages = array('is_unique' => $this->lang->line('administration_users_usernameexist'));
					if ($this->input->post('userName') && $query->userName == $this->input->post('userName')) {
						$userRules = 'required|min_length[4]|max_length[32]';
						$userRulesMessages = NULL;
					}
					$this->form_validation->set_rules('userName', 'lang:login_username', $userRules, $userRulesMessages);


					$sql 	= "SELECT * FROM roles ORDER BY name";
					$roles 	= $this->db->query($sql)->result();

					if ($this->Identity->Validate('users/create/setallroles')) {
						$Ids = '';
						foreach ($roles as $value) {
							$Ids .= $value->roleId.',';
						}

						$this->form_validation->set_rules('role', 'lang:administration_users_role','required|in_list['.$Ids.']',
							array('in_list' => $this->lang->line('administration_create_userrolenoexist')));
					}


					$sql = "SELECT * FROM sites ORDER BY name";
					$sites = $this->db->query($sql)->result();
					$Ids = '';
					foreach ($sites as $value) {
						$Ids .= $value->siteId.',';
					}


					$this->form_validation->set_rules('site', 'lang:general_site','required|in_list['.$Ids.']',
						array('in_list' => $this->lang->line('administration_create_usersitenoexist')));


					$sql = "SELECT * FROM sections ORDER BY name";
					$sections = $this->db->query($sql)->result();
					$sectionsIds = '';
					foreach ($sections as $value) {
						$sectionsIds .= $value->sectionId.',';
					}
					$this->form_validation->set_rules('section', 'lang:general_section','required|in_list['.$sectionsIds.']',
						array('in_list' => $this->lang->line('administration_users_sectionerror')));

					$this->form_validation->set_rules('active', 'lang:administration_users_active', 'in_list[active]');



					//Reglas userComplementaryData
					$this->form_validation->set_rules('userNumber', 'lang:users_usernumber', 
						(
							$this->input->post('userNumber') && $query->userNumber == $this->input->post('userNumber') ? 
							'numeric|min_length[1]|max_length[6]' :
							'numeric|min_length[1]|max_length[6]|is_unique[userComplementaryData.userNumber]'
						));

					if ($this->form_validation->run() == FALSE) {
						$query->sections 	= $sections;
						$query->roles 		= $roles;
						$query->sites 		= $sites;
						$query->navBar 		= $this->load->view('users/_navbar', $query, TRUE);
						
						$this->load->view('_shared/_administrationlayoutheader');
						$this->load->view('users/edit',$query);
						$this->load->view(PRELAYOUTFOOTER);
					}
					else {
						$userEdit = array(
							'userName' 	=> $this->input->post('userName'),	
							'roleId' 	=> $this->Identity->Validate('users/create/setallroles') ? $this->input->post('role') : $query->roleId, 
							'sectionId' => $this->input->post('section'),
							'siteId' 	=> $this->input->post('site'),
							'active' 	=> $this->input->post('active') ? 1 : 0
						);
						$this->db->where('userId', $query->userId);
						$this->db->update('users', $userEdit);


						$userPersonalData = array(
							'name' 		=> $this->input->post('name'),
							'lastName' 	=> $this->input->post('lastName')
						);
						$this->db->where('userId', $query->userId);
						$this->db->update('userPersonalData', $userPersonalData);


						$userComplementaryData = array(
							'userNumber' 	=> $this->input->post('userNumber')
						);
						$this->db->where('userId', $query->userId);
						$this->db->update('userComplementaryData', $userComplementaryData);

						$config['upload_path']      = './catapp/_profilepictures/';
						$config['allowed_types']    = 'gif|jpg|png';
						$config['max_size']         = 10240;
						$config['file_ext_tolower'] = TRUE;
						$config['remove_spaces']    = FALSE;

						if (isset($_FILES['userfile']) && $_FILES['userfile'] != NULL)
						{
							$config['file_name']= $query->userId.'-'.$this->session->UserId.'-'.time().'-'.$_FILES['userfile']['name'];
						}

						$this->load->library('upload', $config);
						if ($this->upload->do_upload('userfile'))
						{
							if (isset($query->photo) && $query->photo != '-' && $query->photo != '') {
								unlink('./catapp/_profilepictures/'.$query->photo);
								unlink('./catapp/_profilepictures/200x200-'.$query->photo);
								$sql="select imageId from imagesUser where photo=?";
								$data=$this->db->query($sql,$query->photo)->row();
							}


							$objectEdit = array(
								'photo' => $this->upload->data('file_name') 
							);

							$this->db->where('userId', $query->userId);
							$this->db->update('userComplementaryData', $objectEdit);


							if(isset($data))
							{
								$this->db->where('imageId', $data->imageId);
								$this->db->update('userComplementaryData', $userComplementaryData);
							}
							else
							{

								$sql="select imageId from imagesUser where userId=?";
								$aux=new StdClass();
								$aux->images=$this->db->query($sql,$query->userId)->result();

								if(count($aux->images) < 5)
								{
									$objectAdd = array(
										'photo' => $this->upload->data('file_name'),
										'userId'=>$query->userId
									);

									$this->db->insert('imagesUser', $objectAdd);	
								}
								else
								{
									$objectEdit = array(
										'photo' => $this->upload->data('file_name') );

									$this->db->where('imageId', $aux->images[0]->imageId);
									$this->db->update('imagesUser', $objectEdit);
								}
							}

							$config['image_library']  = 'gd2';
							$config['source_image']   = './catapp/_profilepictures/'.$this->upload->data('file_name');
							$config['create_thumb']   = FALSE;
							$config['maintain_ratio'] = TRUE;
							$config['width']          = 200;
							$config['height']         = 200;
							$config['new_image']      = './catapp/_profilepictures/200x200-'.$this->upload->data('file_name');

							$this->load->library('image_lib', $config);
							$this->image_lib->resize();
						}



						$this->session->set_flashdata('userMessage', 'edit');
						header('Location:/'.FOLDERADD.'/users/details/'.$query->userId);
					}
				}
				else{
					$sql = "SELECT * FROM sections ORDER BY name";
					$query->sections = $this->db->query($sql)->result();
					$query->navBar = $this->load->view('users/_navbar', $query, TRUE);
					$sql = "SELECT * FROM roles ORDER BY name";
					$query->roles = $this->db->query($sql)->result();
					$sql = "SELECT * FROM sites ORDER BY name";
					$query->sites = $this->db->query($sql)->result();

					$this->load->view('_shared/_administrationlayoutheader');
					$this->load->view('users/edit', $query);
					$this->load->view(PRELAYOUTFOOTER);
				}
			}
			else
			{
				show_404();
			}
		}
		else{
			show_404();
		}
	}

	public function Config()
	{
		if ($this->Identity->Validate('users/config')) {
			$this->load->view(PRELAYOUTHEADER);
			$this->load->view('users/configdef');
			$this->load->view(PRELAYOUTFOOTER);
		}
		else{
			header('Location:/'.FOLDERADD);
		}
	}

	public function ConfigUser()
	{
		$sql 	= 'SELECT u.siteId, uc.campaignId,subCampaignId, up.email, up.address, up.cityId, up.birthDate, up.gender, up.dni, c.zipCode, c.stateId, up.civilState, uc.rent, uc.hasChild, uc.personsInCharge, uc.turn, up.phone, up.phoneAreaCode, up.cellPhone, up.cellPhoneAreaCode,uc.nationalityId
		FROM users AS u 
		INNER JOIN 	(SELECT campaignId,subCampaignId, rent, hasChild, personsInCharge, turn, nationalityId, userId 	FROM userComplementaryData 	WHERE userId = ?) 	AS uc 	ON u.userId 	= uc.userId 
		INNER JOIN 	(SELECT email, address, cityId, birthDate, gender, dni, civilState, phone, phoneAreaCode, cellPhone, cellPhoneAreaCode, userId 		FROM userPersonalData 		WHERE userId = ?) 	AS up 	ON u.userId 	= up.userId 
		LEFT JOIN 	(SELECT stateId, zipCode, cityId 							FROM cities) 									AS c 	ON up.cityId 	= c.cityId 
		WHERE u.userId = ?';
		$query 	= $this->db->query($sql, array($this->session->UserId, $this->session->UserId, $this->session->UserId))->row();
		
		// if ($this->Identity->Validate('users/isagent')) {
		// 	$sql="SELECT c.campaignId, c.name, cl.name client FROM campaigns AS c 
		// 	LEFT JOIN clients AS cl ON c.clientId = cl.clientId
		// 	WHERE sectionId = ? ORDER BY name";
		// 	$query->campaigns = $this->db->query($sql, $this->session->sectionId)->result();
		// }
		// else{
			$sql="SELECT name, campaignId FROM campaigns WHERE sectionId IS NULL ORDER BY name";
			$query->campaigns = $this->db->query($sql)->result();
		// }

		$sql = "SELECT * FROM subCampaigns ORDER BY name";
		$query->subCampaigns = $this->db->query($sql)->result();

		$sql = "SELECT name, siteId FROM sites ORDER BY name";
		$query->sites = $this->db->query($sql)->result();

		$sql = 'SELECT state, stateId FROM states ORDER BY state';
		$query->states = $this->db->query($sql)->result();

		$sql = 'SELECT * FROM nationality ORDER BY value';
		$query->nationalities = $this->db->query($sql)->result();
		
		$this->load->library('map_tables');
		$query->genders = $this->map_tables->get_table('genders');

		$query->civilStates = $this->map_tables->get_table('civilStates');

		$query->turns = $this->map_tables->get_table('turns');

		$sql = 'SELECT relation, gender, familyRelationId FROM familyRelations ORDER BY relation';
		$query->familyRelations =  $this->db->query($sql)->result();

		$sql = 'SELECT workDetail, workTaskId, userWorkId FROM userWorks WHERE userId = ?';
		$query->userWorks =  $this->db->query($sql, $this->session->UserId)->result();

		$sql = 'SELECT task, haveDetails, workTaskId FROM workTasks';
		$query->workTasks =  $this->db->query($sql)->result();

		$sql = 'SELECT careerId, career, level FROM careers ORDER BY career';
		$query->careers =  $this->db->query($sql)->result();

		$sql = 'SELECT institutionId, institution FROM institutions ORDER BY institution';
		$query->institutions =  $this->db->query($sql)->result();

		$sql = 'SELECT status, level, careerId, institutionId, userStudyId FROM userStudies WHERE userId = ?';
		$query->userStudies =  $this->db->query($sql, $this->session->UserId)->result();

		$query->studyLevels 	= $this->map_tables->get_table('studyLevels');

		$query->studyLevelsHasInstitution = array();
		foreach ($query->studyLevels as $k => $v) {
			$query->studyLevelsHasInstitution[$k] = $this->map_tables->get_study_has_institution($k);
		}

		$query->studyStatues 	= $this->map_tables->get_table('studyStatues');


		/* Actualizacion de campos no obligatorios */

		//Hobbies
		$deletedHobbies	= $this->input->post('deletedHobbies[]');
		$hobbiesCount  	= count($deletedHobbies);
		if ($deletedHobbies && is_array($deletedHobbies)) {
			for ($i=0; $i < $hobbiesCount; $i++) { 

				$objecttoDetele = array(
					'hobbyId' 	=> $deletedHobbies[$i],
					'userId' 	=> $this->session->UserId
				);
				$this->db->delete('userHobbies', $objecttoDetele);
			}
		}

		$userHobbies 		= $this->input->post('userHobbies[]');
		$userHobbiesHobby 	= $this->input->post('userHobbiesHobby[]');

		if ($userHobbies && is_array($userHobbies)) {

			$userHobbiesCount = count($userHobbies);

			for ($i=0; $i < $userHobbiesCount; $i++) { 
				if ($userHobbies[$i] == 'new') {
					$hobbyLike 	= $this->db->escape_like_str($userHobbiesHobby[$i]);

					$sql 		= "SELECT h.hobbyId, (SELECT count(hobbyId) FROM userHobbies WHERE userId = ? && hobbyId = h.hobbyId LIMIT 1) AS isSelected FROM hobbies AS h WHERE  hobby LIKE '%$hobbyLike%'";
					$exists 	=  $this->db->query($sql, $this->session->UserId)->row();
					if ($exists) {
						if ($exists->isSelected == 0) {
							$objectInsert = array(
								'hobbyId' 	=> $exists->hobbyId,
								'userId' 	=> $this->session->UserId
							);
							$this->db->insert('userHobbies', $objectInsert);
						}
					}
					else {
						$objectInsert = array(
							'hobby' 	=> $userHobbiesHobby[$i]
						);
						$this->db->insert('hobbies', $objectInsert);

						$id = $this->db->insert_id();

						$newHobby 			= new StdClass();
						$newHobby->hobbyId 	= $id;
						$newHobby->hobby 	= $userHobbiesHobby[$i];

						insert_audit_logs('userHobbies','CREATE',$newHobby);

						$objectInsert = array(
							'hobbyId' 	=> $id,
							'userId' 	=> $this->session->UserId
						);
						$this->db->insert('userHobbies', $objectInsert);
					}

				}
				else{
					$sql 	= 'SELECT h.hobbyId, (SELECT count(hobbyId) FROM userHobbies WHERE userId = ? && hobbyId = ? LIMIT 1) AS isSelected FROM hobbies AS h WHERE h.hobbyId = ?';
					$exists =  $this->db->query($sql, array($this->session->UserId, $userHobbies[$i], $userHobbies[$i]))->row();
					if ($exists && $exists->isSelected == 0) {
						$objectInsert = array(
							'hobbyId' 	=> $userHobbies[$i],
							'userId' 	=> $this->session->UserId
						);
						$this->db->insert('userHobbies', $objectInsert);
					}
				}
			}
		}

		//Transports
		$deletedTransports 	= $this->input->post('deletedTransports[]');
		if ($deletedTransports && is_array($deletedTransports)) {

			$deletedTransportsCount = count($deletedTransports);

			for ($i=0; $i < $deletedTransportsCount ; $i++) { 

				$objecttoDetele = array(
					'transportId' 	=> $deletedTransports[$i],
					'userId' 		=> $this->session->UserId
				);
				$this->db->delete('userTransports', $objecttoDetele);
			}
		}

		$userTransports 			= $this->input->post('userTransports[]');
		$userTransportsTransport 	= $this->input->post('userTransportsTransport[]');

		if ($userTransports && is_array($userTransports)) {

			$userTransportsCount = count($userTransports);			

			for ($i=0; $i < $userTransportsCount; $i++) { 
				if ($userTransports[$i] == 'new') {
					$transportLike 	= $this->db->escape_like_str($userTransportsTransport[$i]);

					$sql 		= "SELECT h.transportId, (SELECT count(transportId) FROM userTransports WHERE userId = ? && transportId = h.transportId LIMIT 1) AS isSelected FROM transports AS h WHERE  transport LIKE '%$transportLike%'";
					$exists 	=  $this->db->query($sql, $this->session->UserId)->row();
					if ($exists) {
						if ($exists->isSelected == 0) {
							$objectInsert = array(
								'transportId' 	=> $exists->transportId,
								'userId' 	=> $this->session->UserId
							);
							$this->db->insert('userTransports', $objectInsert);
						}
					}
					else {
						$objectInsert = array(
							'transport' 	=> $userTransportsTransport[$i]
						);
						$this->db->insert('transports', $objectInsert);

						$id = $this->db->insert_id();

						$newTransport 				= new StdClass();
						$newTransport->transportId 	= $id;
						$newTransport->transport 	= $userTransportsTransport[$i];

						insert_audit_logs('userTransports','CREATE',$newTransport);

						$objectInsert = array(
							'transportId' 	=> $id,
							'userId' 	=> $this->session->UserId
						);
						$this->db->insert('userTransports', $objectInsert);
					}
				}
				else{
					$sql 	= 'SELECT h.transportId, (SELECT count(transportId) FROM userTransports WHERE userId = ? && transportId = ? LIMIT 1) AS isSelected FROM transports AS h WHERE h.transportId = ?';
					$exists =  $this->db->query($sql, array($this->session->UserId, $userTransports[$i], $userTransports[$i]))->row();
					if ($exists && $exists->isSelected == 0) {
						$objectInsert = array(
							'transportId' 	=> $userTransports[$i],
							'userId' 	=> $this->session->UserId
						);
						$this->db->insert('userTransports', $objectInsert);
					}
				}
			}
		}

		//Family Members
		$deletedFamilyMembers 	= $this->input->post('deletedFamilyMembers[]');
		if ($deletedFamilyMembers && is_array($deletedFamilyMembers)) {

			$deletedFamilyMembersCount = count($deletedFamilyMembers); 
			for ($i=0; $i < $deletedFamilyMembersCount; $i++) { 

				$objecttoDetele = array(
					'familyMemberId' 	=> $deletedFamilyMembers[$i],
					'userId' 			=> $this->session->UserId
				);
				$this->db->delete('familyMembers', $objecttoDetele);
			}
		}

		$familyMembers = $this->input->post('familyMembers[]');

		if ($familyMembers && is_array($familyMembers)) {

			$familyRelationIds = array();
			$familyRelationsCount = count($query->familyRelations);
			for ($i=0; $i < $familyRelationsCount; $i++) { 
				$familyRelationIds["ID".$query->familyRelations[$i]->familyRelationId] = $query->familyRelations[$i]->gender;
			}

			$familyMembersCount = count($familyMembers); 
			for ($i=0; $i < $familyMembersCount; $i++) { 
				$familyMember = json_decode($familyMembers[$i]);

				if (is_object($familyMember) 
					&& isset($familyMember->gender) 
					&& isset($query->genders[$familyMember->gender])
					&& isset($familyMember->familyRelationId, $familyRelationIds)
					&& isset($familyRelationIds["ID".$familyMember->familyRelationId])
					&& $familyRelationIds["ID".$familyMember->familyRelationId] == $familyMember->gender
					&& isset($familyMember->birthDate)
				) {
						/**///
					$date_array	= explode('/', $familyMember->birthDate);
					if(count($date_array) == 3 && checkdate($date_array[1], $date_array[0], $date_array[2])){

						$objectInsert = array(
							'gender' 			=> $familyMember->gender, 
							'familyRelationId' 	=> $familyMember->familyRelationId, 
							'birthDate' 		=> $date_array[2].'-'.$date_array[1].'-'.$date_array[0],
							'userId' 			=> $this->session->UserId
						);
						if (isset($familyMember->familyMemberId)) {
							$this->db->where('familyMemberId', $familyMember->familyMemberId);
							$this->db->where('userId', $this->session->UserId);
							$this->db->update('familyMembers', $objectInsert);
						}
						else {
							$this->db->insert('familyMembers', $objectInsert);
						}
					}
				}
			}
		}


		//User works
		$deletedUserWorks 	= $this->input->post('deletedUserWorks[]');
		if ($deletedUserWorks && is_array($deletedUserWorks)) {

			$deletedUserWorksCount = count($deletedUserWorks);
			for ($i=0; $i < $deletedUserWorksCount; $i++) { 

				$objecttoDetele = array(
					'userWorkId' => $deletedUserWorks[$i],
					'userId' 	=> $this->session->UserId
				);
				$this->db->delete('userWorks', $objecttoDetele);
			}
		}

		$userWorks = $this->input->post('userWorks[]');

		if ($userWorks && is_array($userWorks)) {

			$workTaskIds = array();

			$workTasksCount = count($query->workTasks);
			for ($i=0; $i < $workTasksCount; $i++) { 
				$workTaskIds[] = $query->workTasks[$i]->workTaskId;
			}

			$userWorksCount = count($userWorks);
			for ($i=0; $i < $userWorksCount; $i++) { 
				$userWork = json_decode($userWorks[$i]);

				if (is_object($userWork) 
					&& isset($userWork->workTaskId, $workTaskIds)
					&& in_array($userWork->workTaskId, $workTaskIds)
				) {
						/**///


					$objectInsert = array(
						'workTaskId' => $userWork->workTaskId, 
						'workDetail' => isset($userWork->workDetail) ? $userWork->workDetail : null, 
						'userId' 	=> $this->session->UserId
					);
					if (isset($userWork->userWorkId)) {
						$this->db->where('userWorkId', $userWork->userWorkId);
						$this->db->where('userId', $this->session->UserId);
						$this->db->update('userWorks', $objectInsert);
					}
					else {
						$this->db->insert('userWorks', $objectInsert);
					}
				}
			}
		}

		//User studies
		$deletedUserStudies 	= $this->input->post('deletedUserStudies[]');
		if ($deletedUserStudies && is_array($deletedUserStudies)) {

			$deletedUserStudiesCount = count($deletedUserStudies);
			for ($i=0; $i < $deletedUserStudiesCount; $i++) { 

				$objecttoDetele = array(
					'userStudyId' 	=> $deletedUserStudies[$i],
					'userId' 		=> $this->session->UserId
				);
				$this->db->delete('userStudies', $objecttoDetele);
			}
		}

		$userStudies = $this->input->post('userStudies[]');

		if ($userStudies && is_array($userStudies) && count($userStudies) >= count($query->studyLevels)) {

			$careersIds = array();
			$carrersCount = count($query->careers);
			for ($i=0; $i < $carrersCount; $i++) { 
				$careersIds["ID".$query->careers[$i]->careerId] = $query->careers[$i]->level;
			}

			$institutionsIds = array();
			$institutionsCount = count($query->institutions);
			for ($i=0; $i < $institutionsCount; $i++) { 
				$institutionsIds[] = $query->institutions[$i]->institutionId;
			}

			$userStudiesCount = count($userStudies); 
			for ($i=0; $i < $userStudiesCount; $i++) { 
				$userStudy = json_decode($userStudies[$i]);

				if (is_object($userStudy) 
					&& isset($userStudy->level) 
					&& isset($query->studyLevels[$userStudy->level])
					&& isset($userStudy->status) 
					&& isset($query->studyStatues[$userStudy->status])
				) {
						/**///

					$career_id = NULL;

					if (isset($userStudy->careerId)) {
						if ($userStudy->careerId == 'new' && isset($userStudy->career) && is_string($userStudy->career) && strlen($userStudy->career) > 0) {

							$objectInsert = array(
								'career' 	=> $userStudy->career
							);
							$this->db->insert('careers', $objectInsert);

							$career_id = $this->db->insert_id();

							$newCareer 				= new StdClass();
							$newCareer->careerId 	= $career_id;
							$newCareer->career 		= $userStudy->career;

							insert_audit_logs('careers','CREATE',$newCareer);
						}
						elseif($userStudy->careerId == 'new'){
							$userStudy->careerId = NULL;
						}
					}

					$institution_id = NULL;
					if (isset($userStudy->institutionId) && $query->studyLevelsHasInstitution[$userStudy->level]) {
						
						if ($userStudy->institutionId == 'new' && isset($userStudy->institution) && is_string($userStudy->institution) && strlen($userStudy->institution) > 0) {

							$objectInsert = array(
								'institution' 	=> $userStudy->institution
							);
							$this->db->insert('institutions', $objectInsert);

							$institution_id = $this->db->insert_id();

							$newInstitution 				= new StdClass();
							$newInstitution->institutionId 	= $institution_id;
							$newInstitution->institution 	= $userStudy->institution;

							insert_audit_logs('institutions','CREATE',$newInstitution);

						}
						elseif($userStudy->institutionId == 'new'){
							$userStudy->institutionId = NULL;
						}
					}


					if (
						(
							!isset($userStudy->careerId) ||
							(isset($careersIds["ID".$userStudy->careerId]) && $careersIds["ID".$userStudy->careerId] == $userStudy->level) 
							|| isset($career_id)
						) 
						&& 
						(
							!isset($userStudy->institutionId) 
							|| (in_array($userStudy->institutionId, $institutionsIds) || isset($institution_id))
						)
					) {

						if (isset($career_id)) {
							$userStudy->careerId = $career_id;
						}

						if (isset($institution_id)) {
							$userStudy->institutionId = $institution_id;
						}
						elseif (!$query->studyLevelsHasInstitution[$userStudy->level]) {
							$userStudy->institutionId = NULL;
						}
						$objectInsert = array(
							'status' 			=> $userStudy->status, 
							'level' 			=> $userStudy->level, 
							'careerId' 			=> $userStudy->careerId, 
							'institutionId' 	=> $userStudy->institutionId, 
							'userId' 			=> $this->session->UserId
						);
						if (isset($userStudy->userStudyId)) {
							$this->db->where('userStudyId', $userStudy->userStudyId);
							$this->db->where('userId', $this->session->UserId);
							$this->db->update('userStudies', $objectInsert);
						}
						else {
							$this->db->insert('userStudies', $objectInsert);
						}
					}

				}
			}
		}

		/* FIN de actualizacion de campos no obligatorios */

		$sql = 'SELECT h.hobby, h.hobbyId FROM userHobbies AS uh INNER JOIN hobbies AS h ON uh.hobbyId = h.hobbyId WHERE uh.userId = ?';
		$query->userHobbies = $this->db->query($sql, $this->session->UserId)->result();

		$sql = 'SELECT h.hobby, h.hobbyId FROM hobbies AS h LEFT JOIN (SELECT hobbyId FROM userHobbies WHERE userId = ?) AS uh ON h.hobbyId = uh.hobbyId WHERE uh.hobbyId IS NULL ';
		$query->hobbies = $this->db->query($sql, $this->session->UserId)->result();

		$sql = 'SELECT t.transport, t.transportId FROM userTransports AS ut INNER JOIN transports AS t ON ut.transportId = t.transportId WHERE ut.userId = ?';
		$query->userTransports = $this->db->query($sql, $this->session->UserId)->result();

		$sql = 'SELECT t.transport, t.transportId FROM transports AS t LEFT JOIN (SELECT transportId FROM userTransports WHERE userId = ?) AS ut ON t.transportId = ut.transportId WHERE ut.transportId IS NULL ';
		$query->transports = $this->db->query($sql, $this->session->UserId)->result();

		$sql = 'SELECT gender, birthDate, familyRelationId, familyMemberId FROM familyMembers WHERE userId = ?';
		$query->familyMembers = $this->db->query($sql, $this->session->UserId)->result();

		$familyMembersCount = count($query->familyMembers); 
		for ($i=0; $i < $familyMembersCount; $i++) { 
			if ($query->familyMembers[$i]->birthDate != NULL) {
				$query->familyMembers[$i]->birthDate = date('d/m/Y', strtotime($query->familyMembers[$i]->birthDate));
			}
		}

		if ($query->birthDate != NULL) {
			$query->birthDate = date('d/m/Y', strtotime($query->birthDate));
		}


		if ($this->input->post('request') && $this->input->post('request') == 'post' ) {

			$emailRules = 'required|valid_email|is_unique[userPersonalData.email]';
			$emailRulesMessages = array('is_unique' => $this->lang->line('administration_users_emailexist'));
			if ($this->input->post('email') && $query->email == $this->input->post('email')) {
				$emailRules = 'required|valid_email';
				$emailRulesMessages = NULL;
			}
			$this->form_validation->set_rules('email', 'lang:administration_users_email', $emailRules, $emailRulesMessages);

			$genderChars = '';
			foreach ($query->genders as $k => $g) {
				$genderChars .= $k.',';
			}
			$this->form_validation->set_rules('gender', 'lang:users_gender','required|in_list['.$genderChars.']',
				array('in_list' => $this->lang->line('administration_gender_no_exist')));


			$civilStateChars = '';
			foreach ($query->civilStates as $k => $v) {
				$civilStateChars .= $k.',';
			}
			$this->form_validation->set_rules('civilState', 'lang:users_civilstate','required|in_list['.$civilStateChars.']');

			$this->form_validation->set_rules('address', 'lang:administration_users_address', 'required');
			$this->form_validation->set_rules('birthDate', 'lang:administration_users_birthday', 'required');
			$this->form_validation->set_rules('dni', 'lang:users_dni', 'required|numeric|max_length[8]');
			// $this->form_validation->set_rules('zipCode', 'lang:users_zipcode', 'required|numeric|max_length[4]');

			if ($this->input->post('state') == '-1') {
				unset($_POST['state']);
			}

			$Ids = '';
			foreach ($query->states as $value) {
				$Ids .= $value->stateId.',';
			}
			$this->form_validation->set_rules('state', 'lang:administration_users_state','required|in_list['.$Ids.']',
				array('in_list' => $this->lang->line('administration_users_state_notexists')));
			
			$this->form_validation->set_rules('city','lang:administration_users_city','required');

			$date 			= explode('/',$this->input->post('birthDate'));
			$isCorrectDate 	= FALSE;
			if(count($date) == 3){
				$isCorrectDate = checkdate($date[1], $date[0], $date[2]);
			}

			if(!$isCorrectDate) {
				$query->dateError=$this->lang->line('administration_users_formatdate_error');
			}

			$Ids = '';
			foreach ($query->campaigns as $value) {
				$Ids .= $value->campaignId.',';
			}

			if ($this->input->post('campaign') == '-1') {
				unset($_POST['campaign']);
			}

			$this->form_validation->set_rules('campaign', 'lang:administration_users_campaign','in_list['.$Ids.']',
				array('in_list' => $this->lang->line('administration_campaign_no_exist')));
				
			//validacion de subcampaña
			// $sql = "SELECT campaignId FROM campaigns WHERE name = 'Operaciones'";
			// $scId = $this->db->query($sql)->row();
			// if($this->input->post('campaign') == $scId->campaignId){
			// 	$this->form_validation->set_rules('subcampaign','lang:subcampaign','required');
			// }

			$Ids = '';
			foreach ($query->subCampaigns as $value) {
				$Ids .= $value->subCampaignId.',';
			}
			// if ($this->input->post('subcampaign') == '-1') {
			// 	unset($_POST['subcampaign']);
			// }

			// validacion de subcampaña
			$sql = "SELECT campaignId FROM campaigns WHERE name = 'Operaciones'";
			$scId = $this->db->query($sql)->row();
			if($this->input->post('campaign') != $scId->campaignId){
				unset($_POST['subcampaign']);
			}
			$this->form_validation->set_rules('subcampaign', 'lang:subcampaign','in_list['.$Ids.']',
				array('in_list' => $this->lang->line('subcampaigns_no_exist')));


			//fin de validacion de subcampaña

			if ($this->input->post('city') == '-1') {
				unset($_POST['city']);
			}

			$Ids = '';
			foreach ($query->sites as $value) {
				$Ids .= $value->siteId.',';
			}
			$this->form_validation->set_rules('site', 'lang:general_site','required|in_list['.$Ids.']',
				array('in_list' => $this->lang->line('administration_create_usersitenoexist')));

			$this->form_validation->set_rules('rent', 'lang:users_rent','required|in_list[0,1]');
			$this->form_validation->set_rules('hasChild', 'lang:users_has_child','required|in_list[0,1]');
			$this->form_validation->set_rules('personsInCharge', 'lang:users_personsincharge','required|in_list[0,1]');


			$turnsChars = '';
			foreach ($query->turns as $k => $v) {
				$turnsChars .= $k.',';
			}
			$this->form_validation->set_rules('turn', 'lang:users_turn','required|in_list['.$turnsChars.']');

			$this->form_validation->set_rules('phoneAreaCode', 'lang:users_area_code','numeric|max_length[4]');
			$this->form_validation->set_rules('phone', 'lang:users_number','numeric|max_length[8]');

			$this->form_validation->set_rules('cellPhoneAreaCode', 'lang:users_area_code','required|numeric|max_length[4]');
			$this->form_validation->set_rules('cellPhone', 'lang:users_cell_phone','required|numeric|max_length[8]');

			$nationalityIds = '';
			foreach ($query->nationalities as $value) {
				$nationalityIds .= $value->nationalityId.',';
			}
			$this->form_validation->set_rules('nationality','lang:administration_users_nationality','required|in_list['.$nationalityIds.']');

			if ($this->form_validation->run() == FALSE || !$isCorrectDate)
			{
				$this->load->view(PRELAYOUTHEADER);
				$this->load->view('users/config', $query);
				$this->load->view(PRELAYOUTFOOTER);
			}
			else
			{
				if ($this->input->post('city')) {
					$sql 			= 'SELECT cityId FROM cities WHERE cityId = ?';
					$city_exists 	=  $this->db->query($sql, $this->input->post('city'))->row();

					if (isset($city_exists) && $city_exists->cityId != $query->cityId) {
						$_POST['city'] = $city_exists->cityId;
					}
				} 

				if($this->input->post('zipCode'))
				{
					$data = array(
						'zipCode' => $this->input->post('zipCode')
					);
					$this->db->where('cityId', $city_exists->cityId);
					$this->db->update('cities', $data);
				}



				$personalDataChanged = array();		
				if ($query->email 				!= $this->input->post('email')) 			{ $personalDataChanged['email'] 			= $this->input->post('email');				}
				if ($query->gender 				!= $this->input->post('gender')) 			{ $personalDataChanged['gender'] 			= $this->input->post('gender');				}
				if ($query->address 			!= $this->input->post('address')) 			{ $personalDataChanged['address'] 			= $this->input->post('address');			}
				if ($query->birthDate 			!= $this->input->post('birthDate')) 		{ $personalDataChanged['birthDate'] 		= $date[2].'-'.$date[1].'-'.$date[0];		}
				if ($query->dni 				!= $this->input->post('dni')) 				{ $personalDataChanged['dni'] 				= $this->input->post('dni');				}
				if ($query->cityId 				!= $this->input->post('city')) 				{ $personalDataChanged['cityId'] 			= $this->input->post('city');				}
				if ($query->civilState 			!= $this->input->post('civilState'))		{ $personalDataChanged['civilState'] 		= $this->input->post('civilState');			}
				if ($query->phoneAreaCode 		!= $this->input->post('phoneAreaCode'))		{ $personalDataChanged['phoneAreaCode'] 	= $this->input->post('phoneAreaCode');		}
				if ($query->phone 				!= $this->input->post('phone'))				{ $personalDataChanged['phone'] 			= $this->input->post('phone');				}
				if ($query->cellPhoneAreaCode 	!= $this->input->post('cellPhoneAreaCode'))	{ $personalDataChanged['cellPhoneAreaCode'] = $this->input->post('cellPhoneAreaCode');	}
				if ($query->cellPhone 			!= $this->input->post('cellPhone'))			{ $personalDataChanged['cellPhone'] 		= $this->input->post('cellPhone');			}

				if (count($personalDataChanged) > 0) {
					$this->db->where('userId', $this->session->UserId);
					$this->db->update('userPersonalData', $personalDataChanged);

					insert_audit_logs('userPersonalData','UPDATE',$personalDataChanged);
				}
				$userChanged = array();
				if ($query->siteId != $this->input->post('site')) { $userChanged['siteId'] = $this->input->post('site'); }

				if (count($userChanged) > 0) {
					$this->db->where('userId', $this->session->UserId);
					$this->db->update('users', $userChanged);

					insert_audit_logs('users','UPDATE',$userChanged);
				}


				$userComplementaryDataChanged = array();
				if ($query->campaignId 		!= $this->input->post('campaign')) 			{ $userComplementaryDataChanged['campaignId'] 		= $this->input->post('campaign'); 			}
				if ($query->subCampaignId	!= $this->input->post('subcampaign'))		{ $userComplementaryDataChanged['subCampaignId'] 	= $this->input->post('subcampaign'); 		}
				if ($query->rent 			!= $this->input->post('rent')) 				{ $userComplementaryDataChanged['rent'] 			= $this->input->post('rent'); 				}
				if ($query->hasChild 		!= $this->input->post('hasChild')) 			{ $userComplementaryDataChanged['hasChild'] 		= $this->input->post('hasChild'); 			}
				if ($query->personsInCharge != $this->input->post('personsInCharge')) 	{ $userComplementaryDataChanged['personsInCharge'] 	= $this->input->post('personsInCharge'); 	}
				if ($query->turn 			!= $this->input->post('turn')) 				{ $userComplementaryDataChanged['turn'] 			= $this->input->post('turn'); 				}
				if ($query->nationalityId	!= $this->input->post('nationality'))		{ $userComplementaryDataChanged['nationalityId']	= $this->input->post('nationality');		}

				if (count($userComplementaryDataChanged) > 0)
				{
					$this->db->where('userId', $this->session->UserId);
					$this->db->update('userComplementaryData', $userComplementaryDataChanged);

					insert_audit_logs('userComplementaryData','UPDATE',$userComplementaryDataChanged);
				}

				//Config pool

				$objecttoDetele = array(
					'userId' 			=> $this->session->UserId
				);
				$this->db->delete('completeDataUsers', $objecttoDetele);

				$this->session->set_flashdata('userconfigMessage', 'edit');
				header('Location:/'.FOLDERADD.'/users/config?cmd=data');
			}
		}
		else{
			$this->load->view(PRELAYOUTHEADER);
			$this->load->view('users/config', $query);
			$this->load->view(PRELAYOUTFOOTER);
		}	
	}

	public function GetCitiesByState($stateId)
	{
		$sql = 'SELECT city, zipCode, cityId FROM cities WHERE stateId = ?';
		return $this->db->query($sql, $stateId)->result();
	}

	public function ConfigPhoto()
	{
		$sql 	= 'SELECT u.userId, uc.photo FROM users AS u INNER JOIN (SELECT photo, userId FROM userComplementaryData) uc ON u.userId = uc.userId WHERE u.userId = ?';
		$query 	= $this->db->query($sql, $this->session->UserId)->row();

		$this->form_validation->set_rules('request', 'lang:administration_users_newphoto', 'required');

		if ($this->form_validation->run() == FALSE)
		{
			$query->error = NULL;
			$this->load->view(PRELAYOUTHEADER);
			$this->load->view('users/photo', $query);
			$this->load->view(PRELAYOUTFOOTER);
		}
		else
		{
			$config['upload_path']          = './catapp/_profilepictures/';
			$config['allowed_types']        = 'gif|jpg|png';
			$config['max_size']             = 10240;
			$config['file_ext_tolower']     = TRUE;
			$config['remove_spaces']     	= FALSE;


			if (isset($_FILES['userfile']) && $_FILES['userfile'] != NULL) {
				$config['file_name']= $query->userId.'-'.$this->session->UserId.'-'.time().'-'.$_FILES['userfile']['name'];
			}

			$this->load->library('upload', $config);
			if ( ! $this->upload->do_upload('userfile'))
			{
				$query->error = $this->upload->display_errors();
				$this->load->view(PRELAYOUTHEADER);
				$this->load->view('users/photo', $query);
				$this->load->view(PRELAYOUTFOOTER);
			}
			else
			{
				$data ="";
				if (isset($query->photo) && $query->photo != '-') 
				{
					unlink('./catapp/_profilepictures/'.$query->photo);
					unlink('./catapp/_profilepictures/200x200-'.$query->photo);
					
					$sql="select imageId from imagesUser where photo=?";
					$data=$this->db->query($sql,$query->photo)->row();
				}


				$objectEdit = array(
					'photo' => $this->upload->data('file_name') ,
				);

				$this->db->where('userId', $query->userId);
				$this->db->update('userComplementaryData', $objectEdit);

				if(count($data) > 0)
				{
					$this->db->where('imageId', $data->imageId);
					$this->db->update('imagesUser', $objectEdit);
				}
				else
				{

					$sql="select imageId from imagesUser where userId=?";
					$aux=new StdClass();
					$aux->images=$this->db->query($sql,$query->userId)->result();

					if(count($aux->images) < 5)
					{
						$objectAdd = array(
							'photo' => $this->upload->data('file_name'),
							'userId'=>$query->userId
						);

						$this->db->insert('imagesUser', $objectAdd);	
					}
					else
					{
						$objectEdit = array(
							'photo' => $this->upload->data('file_name') );
						$this->db->where('imageId', $aux->images[0]->imageId);
						$this->db->update('imagesUser', $objectEdit);		
					}
				}

				$config['image_library'] = 'gd2';
				$config['source_image'] = './catapp/_profilepictures/'.$this->upload->data('file_name');
				$config['create_thumb'] = FALSE;
				$config['maintain_ratio'] = TRUE;
				$config['width']         = 200;
				$config['height']       = 200;
				$config['new_image']       = './catapp/_profilepictures/200x200-'.$this->upload->data('file_name');

				$this->load->library('image_lib', $config);

				$this->image_lib->resize();

				$this->session->set_flashdata('userconfigMessage', 'editphoto');
				header('Location:/'.FOLDERADD.'/users/config?cmd=photo');
			}
		}
	}

	public function GetProfilePhoto()
	{
		$this->load->helper('download');


		if ($this->input->get('userId')) {

			$sql = "SELECT photo FROM userComplementaryData WHERE userId = ?";
			$object = array($this->input->get('userId'));
			$query = $this->db->query($sql,$object)->row();
			if (isset($query))
			{

				if (isset($query->photo) && $query->photo != '-' && $query->photo != '') {
					$wha = NULL;
					if ($this->input->get('wah')) {
						$wha = $this->input->get('wah').'x'.$this->input->get('wah').'-';
					}
					if (file_exists('./catapp/_profilepictures/'.$wha.$query->photo)) {
						force_download('./catapp/_profilepictures/'.$wha.$query->photo, NULL);
					}
					else
					{
						force_download('./catapp/_profilepictures/'.$query->photo, NULL);
					}
				}
				else
				{
					force_download('./catapp/libraries/images/offline_user.png', NULL);
				}
			}
		}
		else
		{	
			force_download('./catapp/libraries/images/offline_user.png', NULL);
		}
	}

	public function ConfigPassword()
	{

		$sql = 'SELECT userId, userName, password, roleId FROM users WHERE userId = ?';
		$query = $this->db->query($sql, $this->session->UserId)->row();
		if (isset($query) && $this->input->post('oldPassword') && hash_equals($query->password,crypt($this->input->post('oldPassword'), $query->password))) {

			$this->form_validation->set_rules('oldPassword', 'lang:administration_users_oldpassword', 'min_length[6]|max_length[32]|required');
			$this->form_validation->set_rules('password', 'lang:login_password', 'min_length[6]|max_length[32]|required');
			$this->form_validation->set_rules('passwordConfirm', 'lang:administration_users_passwordconfirm', 'required|matches[password]',
				array(
					'matches'     => $this->lang->line('administration_users_passwordmatch')
				));


			if ($this->form_validation->run() == FALSE)
			{
				$this->load->view(PRELAYOUTHEADER);
				$this->load->view('users/password');
				$this->load->view(PRELAYOUTFOOTER);
			}
			else
			{
				$objectEdit = array(
					'password' => crypt($this->input->post('password'))
				);

				$this->db->where('userId', $query->userId);
				$this->db->update('users', $objectEdit);

				$this->session->set_flashdata('userconfigMessage', 'editpassword');
				header('Location:/'.FOLDERADD.'/users/config?cmd=password');
			}
		}
		else{
			if ($this->input->post('oldPassword')) {
				$data['PasswordInvalid'] = TRUE;
			}
			else
			{
				$data['PasswordInvalid'] = FALSE;
			}
			$this->load->view(PRELAYOUTHEADER);
			$this->load->view('users/password', $data);
			$this->load->view(PRELAYOUTFOOTER);
		}
	}

	public function ResetPassword()
	{
		$this->form_validation->set_rules('userName', 'lang:login_username','required');
		$this->form_validation->set_rules('password', 'lang:login_password', 'min_length[6]|max_length[32]|required');
		$this->form_validation->set_rules('passwordConfirm', 'lang:administration_users_passwordconfirm', 'required|matches[password]',
			array(
				'matches'     => $this->lang->line('administration_users_passwordmatch')
			));


		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('_shared/_administrationlayoutheader');
			$this->load->view('users/resetpassword');
			$this->load->view(PRELAYOUTFOOTER);
		}
		else
		{
			$sql = "SELECT userId, userName FROM users WHERE userName = ?";
			$query = $this->db->query($sql,$this->input->post('userName'))->row();
			if (isset($query) && $query != NULL) {

				$objectEdit = array(
					'password' => crypt($this->input->post('password'))
				);

				$this->db->where('userId', $query->userId);
				$this->db->update('users', $objectEdit);

				$this->session->set_flashdata('flashMessage', 'editpassword');
			}
			else{
				$this->session->set_flashdata('nouserMessage', 'nouser');
			}
			header('Location:/'.FOLDERADD.'/users/resetpassword');
		}
	}

	public function ResetPasswordByQuestion($userName)
	{
		$sql = "SELECT userId FROM users WHERE userName = ?";
		$query = $this->db->query($sql, $userName)->row();
		$response = FALSE;

		if (isset($query))
		{
			$objectEdit = array('password' => crypt($this->getDefaultPassword()));

			$this->db->where('userId', $query->userId);
			$this->db->update('users', $objectEdit);

			$response = TRUE;
		}

		return $response;
	}

	public function SelectBackground()
	{
		$data = new StdClass();
		$sql = "SELECT background FROM userComplementaryData WHERE userId = ?";
		$data->user = $this->db->query($sql, $this->session->UserId)->row();
		$this->db->select('userBackgroundId,name');
		$data->images = $this->db->get('userBackground')->result();
		
		$imageId = $this->input->get('select');
		if ($imageId != NULL) {
			$this->db->select('userBackgroundId,name');
			$image = $this->db->get_where('userBackground', array('userBackgroundId' => $imageId))->row();
			if($image != NULL){
				$userEdit= array('background' => $image->name);
				$this->db->where('userId', $this->session->UserId);
				$this->db->update('userComplementaryData', $userEdit);
				$data->message = $this->lang->line('administration_users_selectbackground_success');
			}
		}

		$this->load->view(PRELAYOUTHEADER);
		$this->load->view('users/selectbackground', $data);
		$this->load->view(PRELAYOUTFOOTER);
	}

	function SearchUsers($like)
	{	
		if ($like != '') {

			$sql = "SELECT concat_ws(' ', up.name, up.lastName) as completeName, u.userId, u.userName FROM users AS u INNER JOIN (SELECT name, lastName, userId FROM userPersonalData) AS up ON u.userId = up.userId WHERE concat_ws(' ', up.name, up.lastName) LIKE '%$like%' || u.userName LIKE '%$like%' ORDER BY completeName LIMIT 20";
			$query = $this->db->query($sql)->result();
			if (count($query) > 0) {
				$response = new stdClass();
				$response->status = 'ok';
				$response->data = $query;
				return $response;
			}
			else
			{
				return "empty";
			}
		}
		else
		{
			return "empty";
		}
	}

	public function FindUsers()
	{
		$likeuser = $this->db->escape_like_str($this->input->post('userLike'));

		if ($likeuser != '') {

			$foundUsers = $this->SearchUsers($likeuser);

			if ((gettype($foundUsers)=='object') && (count($foundUsers->data) > 0)) {

				foreach ($foundUsers->data as $key => $user) {
					?>
					<div class="btn userFinds-user label label-default" user="<?php echo $user->userId ?>" ><?php echo $user->completeName; ?> <i class="fa fa-plus text-primary"></i></div>
					<script>
						userselectToggle($('#userFinds > [user="<?php echo $user->userId ?>"]')); 
					</script>
					<?php
				}
			}
			else
			{
				echo "empty";
			}
		}
		else
		{
			echo "nolike";
		}
	}

	function GetUsersBySite($usersIds)
	{
		$this->db->select('count(users.userId) as number,users.siteId,sites.name');
		$this->db->join('sites', 'users.siteId = sites.siteId', 'left');
		$this->db->where_in('users.userId', $usersIds);
		$this->db->group_by('users.siteId');
		$response = $this->db->get('users')->result();
		
		echo (json_encode($response));		
	}

	function GetTl()
	{
		$response = new StdClass();
		$response->status = "error";

		$this->db->select('users.userId,concat_ws(" ",name,lastName) as completeName');
		$this->db->join('userPersonalData us', 'us.userId = users.userId');
		$this->db->join('teamLeaders', 'teamLeaders.userId = users.userId');
		$response->data = $this->db->get('users')->result();
		if($response->data){
			$response->status = "ok";
		}

		echo(json_encode($response));
		
	}

	public function InsertByFile($users,$userData)
	{
		$this->db->insert_batch('users', $users);
		$usersToUpdate = [];
		foreach ($userData as $user) {
			
			$this->db->select('userId');
			$userId = $this->db->get_where('users',array('userName' => $user['userName']))->row()->userId;
			$user['userId'] = $userId;
			unset($user['userName']);
			$usersToUpdate[] = $user;
		}
		
		$this->db->insert_batch('userPersonalData', $usersToUpdate);

		$complementary = [];
		foreach ($usersToUpdate as $user) {
			$complementary[] = array('userId' => $user['userId']);
		}

		$this->db->insert_batch('userComplementaryData', $complementary);
		return "success";
	}

	public function DeleteByFile($users)
	{
		$usersToDelete = [];
		foreach ($users as $user) {
			$usersToDelete[] = array(
				'userName' 	=> $user,
				'active'	=> 0
			);
		}
		
		$this->db->update_batch('users',$usersToDelete,'userName');
		return "success";
	}
}
