<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Chat Model Class
 * 
 * Model For Chat
 *
 * @package 	CATNET
 * @subpackage 	Models
 * @category 	Chat
 * @author 		Ivan
*/
Class Chat_model extends CI_Model {

	/**
	* Number Of Messages Loads Per Request
	*
	* @var int
	*/
	var $messagesLoad = 10;


	function GetChatsU(){
		$sql = "SELECT c.chatuId, c.timestamp, c.fromuserId, c.touserId FROM chatU AS c WHERE (c.fromuserId = ? || c.touserId = ?) && NOT EXISTS (SELECT 1 FROM chatuDeletes AS cd WHERE c.chatuId = cd.chatuId && cd.userId = ? && cd.currentlyRemoved = 'TRUE' )";
		$chats = $this->db->query($sql, array($this->session->UserId, $this->session->UserId, $this->session->UserId))->result();
		foreach ($chats as $chat) {
			$sql = "SELECT cm.fromuserId, cm.message,cm.timestamp FROM chatuMessagesTemp AS cm WHERE cm.chatuId = ? AND visible = 'TRUE' ORDER BY cm.timestamp DESC LIMIT 1" ;
			$chat->message = $this->db->query($sql, array($chat->chatuId))->row();

			if ($chat->message != NULL) {
				$chat->timestamp = $chat->message->timestamp;
			}

			$chat->date = $this->Layout->GetPast($chat->timestamp);

			$sql = 'SELECT name, lastName, userId FROM userPersonalData WHERE userId = ?';

			if ($chat->fromuserId == $this->session->UserId) {
				if ($chat->touserId) {
					$chat->user = $this->db->query($sql, $chat->touserId)->row();
				}
			}
			else if ($chat->fromuserId) {
				$chat->user = $this->db->query($sql, $chat->fromuserId)->row();
			}


			$sql = "SELECT lastmessageIdDelete FROM chatuDeletes WHERE userId = ? && chatuId = ? && currentlyRemoved = 'FALSE' ORDER BY chatudeleteId DESC";
			$lastDelete = $this->db->query($sql, array($this->session->UserId, $chat->chatuId))->row();	
			if ($lastDelete == NULL) {
				$lastDelete = 0;
			}
			else
			{
				$lastDelete = $lastDelete->lastmessageIdDelete;
			}

			$sql = "SELECT count(cm.hash_id) c FROM chatuMessagesTemp AS cm  WHERE cm.chatuId = ? && cm.chatumessageId > ? && cm.fromuserId != ? AND cm.visible = 'TRUE' && NOT EXISTS (SELECT 1 FROM chatumessageViews AS cmv WHERE cm.hash_id = cmv.chatuMessageId && cmv.userId = ?)";
			$noReads = $this->db->query($sql, array($chat->chatuId, $lastDelete, $this->session->UserId, $this->session->UserId))->row();

			$chat->noRead = ($noReads) ? $noReads->c : 0;
		}
		return $chats;
	}

	function GetChatUById($id,$search=false){
		if($search){
			$sql = "SELECT c.chatuId, c.fromuserId, c.touserId FROM chatU AS c 
			WHERE c.chatuId = ? && NOT EXISTS (SELECT 1 FROM chatuDeletes AS cd WHERE c.chatuId = cd.chatuId && cd.currentlyRemoved = 'TRUE' )";
			$chat = $this->db->query($sql,$id)->row();
		}
		else{
			$sql = "SELECT c.chatuId, c.fromuserId, c.touserId FROM chatU AS c 
			WHERE (c.fromuserId = ? || c.touserId = ?) && c.chatuId = ? && NOT EXISTS (SELECT 1 FROM chatuDeletes AS cd WHERE c.chatuId = cd.chatuId && cd.userId = ? && cd.currentlyRemoved = 'TRUE' )";
			$chat = $this->db->query($sql, array($this->session->UserId, $this->session->UserId, $id, $this->session->UserId))->row();	
		}
		if ($chat != NULL) {
			$sql = "SELECT name, lastName, userId FROM userPersonalData WHERE userId = ?";
			if ($chat->fromuserId == $this->session->UserId) {
				if ($chat->touserId) {
					$chat->user = $this->db->query($sql, $chat->touserId)->row();
				}
			}
			else if ($chat->fromuserId) {
				$chat->user = $this->db->query($sql, $chat->fromuserId)->row();
			}

			// $sql = "SELECT lastmessageIdDelete FROM chatuDeletes WHERE userId = ? && chatuId = ? && currentlyRemoved = 'FALSE' ORDER BY chatudeleteId DESC";
			// $lastDelete = $this->db->query($sql, array($this->session->UserId, $chat->chatuId))->row();	
			// if ($lastDelete == NULL) {
			// 	$lastDelete = 0;
			// }
			// else
			// {
			// 	$lastDelete = $lastDelete->lastmessageIdDelete;
			// }

			$sql = "SELECT cm.hash_id, cm.fromuserId as userId, cm.message, cm.timestamp, up.name, up.lastName
			FROM chatuMessagesTemp AS cm
			INNER JOIN (SELECT name, lastName, userId FROM userPersonalData WHERE userId IN (? ,?) ) AS up ON cm.fromuserId = up.userId
			WHERE cm.chatuId = ? AND cm.visible = 'TRUE' ORDER BY cm.hash_id DESC LIMIT 0,".$this->messagesLoad ;
			$chat->messages = $this->db->query($sql, array($chat->fromuserId, $chat->touserId, $chat->chatuId ))->result();

			foreach ($chat->messages as $message) {
				$message->date = date('d/m/Y H:i', $message->timestamp);
			}

			$sql = "SELECT cm.hash_id,cm.chatumessageId FROM chatuMessagesTemp AS cm  WHERE cm.chatuId = ? && cm.fromuserId != ? AND cm.visible = 'TRUE' && NOT EXISTS (SELECT 1 FROM chatumessageViews AS cmv WHERE cm.hash_id = cmv.chatuMessageId && cmv.userId = ?)";
			$noReads = $this->db->query($sql, array($chat->chatuId, $this->session->UserId, $this->session->UserId))->result();

			$chat->unreads = count($noReads) > 0 ? TRUE : FALSE;

			foreach ($noReads as $message) {
				$objectInsert = array(
					'timestamp' => time(),
					'userId' => $this->session->UserId,
					'chatumessageId' => $message->hash_id
					);
				$this->db->insert('chatumessageViews', $objectInsert);
			}

		}
		$chat->deleted = $this->isChatDeleted($chat->chatuId);

		return $chat;
	}

	function GetOldMessages($id, $first){
		$sql = "SELECT c.chatuId, c.fromuserId, c.touserId FROM chatU AS c 
		WHERE (c.fromuserId = ? || c.touserId = ?) && c.chatuId = ? && NOT EXISTS (SELECT 1 FROM chatuDeletes AS cd WHERE c.chatuId = cd.chatuId && cd.userId = ? && cd.currentlyRemoved = 'TRUE' )";
		$chat = $this->db->query($sql, array($this->session->UserId, $this->session->UserId, $id, $this->session->UserId))->row();	
		$messages = NULL;
		if ($chat != NULL) {

			$sql = "SELECT lastmessageIdDelete FROM chatuDeletes WHERE userId = ? && chatuId = ? && currentlyRemoved = 'FALSE' ORDER BY chatudeleteId DESC";
			$lastDelete = $this->db->query($sql, array($this->session->UserId, $chat->chatuId))->row();	
			if ($lastDelete == NULL) {
				$lastDelete = 0;
			}
			else
			{
				$lastDelete = $lastDelete->lastmessageIdDelete;
			}

			$sql = "SELECT cm.chatumessageId, cm.fromuserId as userId, cm.message, cm.timestamp, up.name, up.lastName
			FROM chatuMessagesTemp AS cm
			INNER JOIN (SELECT name, lastName, userId FROM userPersonalData WHERE userId IN (?, ?) ) AS up ON cm.fromuserId = up.userId
			WHERE cm.chatuId = ? && cm.chatumessageId > ? && cm.chatumessageId < ? AND cm.visible = 'TRUE' ORDER BY cm.chatumessageId DESC LIMIT 0,".$this->messagesLoad ;
			$messages = $this->db->query($sql, array($chat->fromuserId, $chat->touserId, $chat->chatuId, $lastDelete, $first ))->result();

			foreach ($messages as $message) {
				$message->date = date('d/m/Y H:i', $message->timestamp);
			}
		}
		return $messages;
	}

	function DeleteChatU($id){
		$sql = "SELECT c.chatuId FROM chatU AS c 
		WHERE (c.fromuserId = ? || c.touserId = ?) && c.chatuId = ? && NOT EXISTS (SELECT 1 FROM chatuDeletes AS cd WHERE c.chatuId = cd.chatuId && cd.userId = ? && cd.currentlyRemoved = 'TRUE' )";
		$chat = $this->db->query($sql, array($this->session->UserId, $this->session->UserId, $id, $this->session->UserId))->row();	
		if ($chat != NULL) {
			
			$sql = "SELECT chatumessageId FROM chatuMessagesTemp WHERE chatuId = ? ORDER BY chatumessageId DESC LIMIT 1" ;
			$lastMessage = $this->db->query($sql, $chat->chatuId)->row();

			if ($lastMessage == NULL) {
				$lastMessage->chatumessageId = 0;
			}

			$objectDelete = array(
				'userId' 				=> $this->session->UserId,
				'chatuId' 				=> $chat->chatuId
				);
			$this->db->delete('chatuDeletes', $objectDelete);


			$objectInsert = array(
				'lastmessageIdDelete' 	=> $lastMessage->chatumessageId,
				'timestamp' 			=> time(),
				'userId' 				=> $this->session->UserId,
				'chatuId' 				=> $chat->chatuId,
				'currentlyRemoved' 		=> 'TRUE'
				);
			$this->db->insert('chatuDeletes', $objectInsert);
			
			$res = new StdClass();
			$res ->chat =  $this->db->get_where('chatU',array('chatuId' => $chat->chatuId))->row();
			$res->status ='ok';
			
			return $res;
		}
		else
		{
			return 'invalid';
		}
	}

	function newMessageU($id, $newmessage){
		$sql = "SELECT c.chatuId, c.fromuserId, c.touserId FROM chatU AS c 
		WHERE (c.fromuserId = ? || c.touserId = ?) && c.chatuId = ? && NOT EXISTS (SELECT 1 FROM chatuDeletes AS cd WHERE c.chatuId = cd.chatuId && cd.userId = ? && cd.currentlyRemoved = 'TRUE' )";
		$chat = $this->db->query($sql, array($this->session->UserId, $this->session->UserId, $id, $this->session->UserId))->row();	
		$message = NULL;
		if ($chat != NULL) {
			$hash_id = genereteRandomString();
			$objectInsert = array(
				'hash_id'	 => $hash_id,
				'message' 	 => $newmessage,
				'timestamp'  => time(),
				'fromuserId' => $this->session->UserId,
				'chatuId' 	 => $chat->chatuId,
				'visible' 	 => 'TRUE'
				);
			$this->db->insert('chatuMessagesTemp', $objectInsert);
			$useuserId = $chat->fromuserId;
			if ($this->session->UserId == $chat->fromuserId) {
				$useuserId = $chat->touserId;
			}

			$messageId = $this->db->insert_id();

			$sql = "SELECT chatudeleteId FROM chatuDeletes WHERE userId = ? && chatuId = ? ORDER BY chatudeleteId DESC LIMIT 1" ;
			$chat->chatuDelete = $this->db->query($sql, array($useuserId, $chat->chatuId))->row();	
			if ($chat->chatuDelete != NULL) {
				$objectUpdate = array(
					'currentlyRemoved' => 'FALSE'
					);
				$this->db->where('userId', $useuserId);
				$this->db->where('chatuId', $chat->chatuId);
				$this->db->update('chatuDeletes', $objectUpdate);		
			}

			$sql = "SELECT cm.chatumessageId, cm.hash_id, cm.fromuserId as userId, cm.message, cm.timestamp, up.name, up.lastName, cm.chatuId
			FROM ( (SELECT chatumessageId, hash_id, fromuserId, message, timestamp, chatuId FROM chatuMessagesTemp WHERE chatumessageId = ? AND chatuMessagesTemp.visible = 'TRUE') AS cm
			INNER JOIN (SELECT name, lastName, userId FROM userPersonalData WHERE userId = ?) AS up ON cm.fromuserId = up.userId ) " ;
			$message = $this->db->query($sql, array($messageId, $this->session->UserId))->row();

			if ($message) {
				$message->date = date('d/m/Y H:i', $message->timestamp);
			}
		}
		return $message;
	}

	function AddMessageUView($id){

		$sql = "SELECT cm.hash_id
		FROM chatuMessagesTemp AS cm
		WHERE cm.hash_id = ? && EXISTS (SELECT 1 FROM chatU AS c WHERE c.chatuId = cm.chatuId && (c.fromuserId = ? || c.touserId = ?) )" ;
		$message = $this->db->query($sql, array($id, $this->session->UserId, $this->session->UserId))->row();

		if ($message) {
			$objectInsert = array(
				'timestamp' => time(),
				'userId' => $this->session->UserId,
				'chatumessageId' => $message->hash_id
				);
			$this->db->insert('chatumessageViews', $objectInsert);
			return 'ok';
		}
		else
		{
			return 'invalid';
		}
	}

	function GetNewMessagesU($id, $last){

		$sql = "SELECT c.chatuId FROM chatU AS c 
		WHERE (c.fromuserId = ? || c.touserId = ?) && c.chatuId = ? && NOT EXISTS (SELECT 1 FROM chatuDeletes AS cd WHERE c.chatuId = cd.chatuId && cd.userId = ? && cd.currentlyRemoved = 'TRUE' )";
		$chat = $this->db->query($sql, array($this->session->UserId, $this->session->UserId, $id, $this->session->UserId))->row();	
		$messages = NULL;
		if ($chat != NULL) {

			$sql = "SELECT lastmessageIdDelete FROM chatuDeletes WHERE userId = ? && chatuId = ? && currentlyRemoved = 'FALSE' ORDER BY chatudeleteId DESC";
			$lastDelete = $this->db->query($sql, array($this->session->UserId, $chat->chatuId))->row();	
			if ($lastDelete == NULL) {
				$lastDelete = 0;
			}
			else
			{
				$lastDelete = $lastDelete->lastmessageIdDelete;
			}

			$sql = "SELECT cm.chatumessageId,cm.hash_id, cm.fromuserId as userId, cm.message, cm.timestamp, up.name, up.lastName, cm.chatuId
			FROM chatuMessagesTemp AS cm
			INNER JOIN (SELECT name, lastName, userId FROM userPersonalData) AS up ON cm.fromuserId = up.userId
			WHERE cm.chatuId = ? && cm.chatumessageId > ? AND cm.visible = 'TRUE' ORDER BY cm.chatumessageId DESC";
			$messages = $this->db->query($sql, array($chat->chatuId, $last ))->result();

			foreach ($messages as $message) {
				$message->date = date('d/m/Y H:i', $message->timestamp);
			}
		}
		return $messages;
	}

	function NewUnique(){

		$this->form_validation->set_rules('message', 'lang:chat_message', 'required');
		$this->form_validation->set_rules('user', 'lang:chat_newto', 'required');

		if ($this->form_validation->run() == FALSE)
		{	
			$this->load->view('chat/newu', array('navbar' => $this->load->view('chat/_navbar', array('title' => $this->lang->line('chat_new')), TRUE)));
		}
		else
		{
			$validRole = " AND (userId IN(SELECT userId FROM teamLeaders) OR roleId = 3) AND roleId != ".$this->session->roleId;
			if ($this->Identity->Validate('home/inbox/new/torole')) {
				$validRole = '';
			}
			$sql = "SELECT userId FROM users WHERE userId != ? && userId = ? ".$validRole;
			$userExist = $this->db->query($sql, array($this->session->UserId, $this->input->post('user')))->row();
			if (isset($userExist)) {

				$sql = "SELECT * FROM chatU WHERE (fromuserId = ? && touserId = ?) || (fromuserId = ? && touserId = ?)" ;
				$query = $this->db->query($sql, array($this->session->UserId, $this->input->post('user'), $this->input->post('user') ,$this->session->UserId))->row();	
				if ($query != NULL) {

					$hash_id = genereteRandomString();
					$objectInsert = array(
						'hash_id'	 => $hash_id,
						'message' 	 => $this->input->post('message'),
						'timestamp'  => time(),
						'fromuserId' => $this->session->UserId,
						'chatuId' 	 => $query->chatuId,
						'visible'	 => 'TRUE'
						);
					$this->db->insert('chatuMessagesTemp', $objectInsert);

					$sql = "SELECT chatudeleteId FROM chatuDeletes WHERE chatuId = ? && currentlyRemoved = 'TRUE' " ;
					$query->chatuDelete = $this->db->query($sql, $query->chatuId)->row();	
					if ($query->chatuDelete != NULL) {
						$objectUpdate = array(
							'currentlyRemoved' => 'FALSE'
							);
						$this->db->where('chatuId', $query->chatuId);
						$this->db->update('chatuDeletes', $objectUpdate);

					}

					$data = new StdClass();
					$data->status = 'ok';
					$data->chat = $query;

					$sql = "SELECT cm.chatumessageId, cm.hash_id, cm.fromuserId as userId, cm.message, cm.timestamp, u.name, u.lastName, cm.chatuId
					FROM chatuMessagesTemp AS cm
					INNER JOIN (SELECT name, lastName, userId FROM userPersonalData) AS u ON cm.fromuserId = u.userId
					WHERE cm.chatuId = ? AND cm.visible = 'TRUE' ORDER BY chatumessageId DESC LIMIT 1" ;
					$data->message = $this->db->query($sql, $query->chatuId)->row();

					if ($data->message) {
						$data->message->date = date('d/m/Y H:i', $data->message->timestamp);
					}

					echo escapeJsonString($data, FALSE);
				}
				else
				{
					$objectInsert = array(
						'timestamp' => time(),
						'fromuserId' => $this->session->UserId,
						'touserId' => $this->input->post('user')
						);
					$this->db->insert('chatU', $objectInsert);

					$sql = "SELECT * FROM chatU WHERE fromuserId = ? && touserId = ?" ;
					$query = $this->db->query($sql, array($this->session->UserId, $this->input->post('user')))->row();	
					if ($query != NULL) {

						$hash_id = genereteRandomString();
						$objectInsert = array(
							'hash_id'	 => $hash_id,	
							'message' 	 => $this->input->post('message'),
							'timestamp'  => time(),
							'fromuserId' => $this->session->UserId,
							'chatuId' 	 => $query->chatuId,
							'visible'	 => 'TRUE'
							);
						$this->db->insert('chatuMessagesTemp', $objectInsert);
						$data = new StdClass();
						$data->status = 'ok';
						$data->chat = $query;
						echo escapeJsonString($data, FALSE);
					}
					else
					{
						echo '{"status":"invalid"}';
					}
				}

			}
			else
			{
				echo '{"status":"invalid"}';
			}
		}
	}

	function GetChatsM(){
		$sql = "SELECT cmu.timestamp, c.userId, c.label, c.chatmId FROM chatmUsers AS cmu 
		INNER JOIN  chatM AS c ON cmu.chatmId = c.chatmId
		WHERE cmu.userId = ? && visible = 'TRUE' GROUP BY cmu.chatmId";
		$chats = $this->db->query($sql, $this->session->UserId)->result();	
		foreach ($chats as $chat) {
			$sql = "SELECT cm.message, cm.timestamp FROM chatmMessagesTemp AS cm WHERE cm.chatmId = ? AND visible = 'TRUE' ORDER BY cm.timestamp DESC LIMIT 1" ;
			$chat->message = $this->db->query($sql, array($chat->chatmId))->row();

			if ($chat->message != NULL) {
				$chat->timestamp = $chat->message->timestamp;
			}

			$chat->date = $this->Layout->GetPast($chat->timestamp);

			$sql = "SELECT count(cm.hash_id) c FROM chatmMessagesTemp AS cm 
			WHERE cm.chatmId = ? && cm.userId != ? AND visible = 'TRUE'
			&& NOT EXISTS (SELECT 1 FROM chatmmessageViews AS cmv WHERE cm.hash_id = cmv.chatmmessageId && cmv.userId = ?)";
			$noReads = $this->db->query($sql, array($chat->chatmId, $this->session->UserId, $this->session->UserId))->row();

			$chat->noRead = ($noReads) ? $noReads->c : 0;
		}
		return $chats;
	}

	function DeleteChatM($id){
		$sql="SELECT chatmuserId FROM chatmUsers WHERE chatmId= ? && userId= ?";
		$chat = $this->db->query($sql, array($id, $this->session->UserId))->row();
		if ($chat != NULL) {
			$objectDelete = array(
				'chatmuserId' => $chat->chatmuserId
				);

			$this->db->delete('chatmUsers',$objectDelete);

			//si es teamleader el que sale se hace invisible el chat.
			$this->db->select('u.userId');
			$this->db->join('teamLeaders tl', 'tl.userId = u.userid');
			$userId = $this->db->get('users u', 1)->row();
			if($userId){
				$this->db->set('visible', 'FALSE');
				$this->db->where('chatmId', $chat->chatmuserId);
				$this->db->update('chatM');
			}
			return 'ok';
		}
		else
		{
			return 'invalid';
		}
	}

	function DeleteAllChatM($id){
		$sql="SELECT c.chatmId FROM chatM AS c WHERE chatmId= ? && userId= ? && visible = 'TRUE'";
		$chat=$this->db->query($sql, array($id, $this->session->UserId))->row();
		if ($chat != NULL) {
			$objectDelete = array(
				'chatmId' => $chat->chatmId
				);
			$this->db->delete('chatmUsers',$objectDelete);
			return 'ok';
		}
		else
		{
			return 'invalid';
		}
	}

	function GetChatMById($id,$search=false){
		if($search){
			$sql = "SELECT cmu.timestamp, c.userId, c.label, c.chatmId FROM chatmUsers AS cmu 
			INNER JOIN  chatM AS c ON cmu.chatmId = c.chatmId
			WHERE cmu.chatmId = ? && visible = 'TRUE' LIMIT 1";
			$chat = $this->db->query($sql, $id)->row();
		}
		else{
			$sql = "SELECT cmu.timestamp, c.userId, c.label, c.chatmId FROM chatmUsers AS cmu 
			INNER JOIN  chatM AS c ON cmu.chatmId = c.chatmId
			WHERE cmu.userId = ? && cmu.chatmId = ?  && visible = 'TRUE' LIMIT 1";
			$chat = $this->db->query($sql, array($this->session->UserId, $id))->row();
		}
		if ($chat) {
			$sql = "SELECT cmu.userId, up.name, up.lastName FROM chatmUsers AS cmu  INNER JOIN (SELECT name, lastName, userId FROM userPersonalData) AS up ON cmu.userId = up.userId WHERE cmu.chatmId = ?";
			$chat->users = $this->db->query($sql, $id)->result();	

			$sql = "SELECT cm.chatmmessageId, cm.hash_id, cm.userId, cm.message, cm.timestamp, up.name, up.lastName
			FROM chatmMessagesTemp AS cm
			INNER JOIN (SELECT name, lastName, userId FROM userPersonalData) AS up ON cm.userId = up.UserId
			WHERE cm.chatmId = ? AND cm.visible = 'TRUE' ORDER BY cm.chatmmessageId DESC LIMIT 0,".$this->messagesLoad ;
			$chat->messages = $this->db->query($sql, $chat->chatmId)->result();

			foreach ($chat->messages as $message) {
				$message->date = date('d/m/Y H:i', $message->timestamp);
			}

			$sql = "SELECT cm.hash_id,cm.chatmmessageId FROM chatmMessagesTemp AS cm  WHERE cm.chatmId = ? && cm.userId != ? AND cm.visible = 'TRUE' && NOT EXISTS (SELECT 1 FROM chatmmessageViews AS cmv WHERE cm.hash_id = cmv.chatmmessageId && cmv.userId = ?)";
			$noReads = $this->db->query($sql, array($chat->chatmId, $this->session->UserId, $this->session->UserId))->result();
			
			$chat->unreads = count($noReads) > 0 ? TRUE : FALSE;

			foreach ($noReads as $message) {
				$objectInsert = array(
					'timestamp' 		=> time(),
					'userId' 			=> $this->session->UserId,
					'chatmmessageId' 	=> $message->hash_id
					);
				$this->db->insert('chatmmessageViews', $objectInsert);
			}

		}
		return $chat;
	}
	
	function GetOldMMessages($id, $first){
		$sql = "SELECT c.chatmId FROM chatM AS c WHERE c.chatmId = ? && visible = 'TRUE' && EXISTS (SELECT 1 FROM chatmUsers AS cmu WHERE cmu.chatmId = c.chatmId && cmu.userId = ?) LIMIT 1";
		$chat = $this->db->query($sql, array($id, $this->session->UserId))->row();	
		$messages = NULL;
		if ($chat != NULL) {
			$sql = "SELECT cm.chatmmessageId, cm.userId, cm.message, cm.timestamp, up.name, up.lastName
			FROM chatmMessagesTemp AS cm
			INNER JOIN (SELECT name, lastName, userId FROM userPersonalData) AS up ON cm.userId = up.UserId
			WHERE cm.chatmId = ? && cm.chatmmessageId < ? AND cm.visible = 'TRUE' ORDER BY cm.chatmmessageId DESC LIMIT 0,".$this->messagesLoad ;
			$messages = $this->db->query($sql, array($chat->chatmId, $first))->result();
			foreach ($messages as $message) {
				$message->date = date('d/m/Y H:i', $message->timestamp);
			}
		}
		return $messages;
	}

	function GetNewMessagesM($id, $last){
		$sql = "SELECT c.chatmId FROM chatM AS c WHERE c.chatmId = ? && visible = 'TRUE' && EXISTS (SELECT 1 FROM chatmUsers AS cmu WHERE cmu.chatmId = c.chatmId && cmu.userId = ?) LIMIT 1";
		$chat = $this->db->query($sql, array($id, $this->session->UserId))->row();	
		$messages = NULL;
		if ($chat != NULL) {
			$sql = "SELECT cm.chatmmessageId, cm.userId, cm.message, cm.timestamp, up.name, up.lastName, cm.chatmId
			FROM chatmMessagesTemp AS cm
			INNER JOIN (SELECT name, lastName, userId FROM userPersonalData) AS up ON cm.userId = up.UserId
			WHERE cm.chatmId = ? && cm.chatmmessageId > ? AND cm.visible = 'TRUE' ORDER BY cm.chatmmessageId DESC";
			$messages = $this->db->query($sql, array($chat->chatmId, $last))->result();
			foreach ($messages as $message) {
				$message->date = date('d/m/Y H:i', $message->timestamp);
				if ($message->userId != $this->session->UserId) {
					$objectInsert = array(
						'timestamp' 		=> time(),
						'userId' 			=> $this->session->UserId,
						'chatmmessageId' 	=> $message->chatmmessageId
						);
					$this->db->insert('chatmmessageViews', $objectInsert);
				}
			}
		}
		return $messages;
	}

	function NewMessageM($id, $newmessage){
		$sql = "SELECT cmu.chatmId FROM chatmUsers AS cmu WHERE cmu.chatmId = ? && cmu.userId = ? LIMIT 1";
		$chat = $this->db->query($sql, array($id, $this->session->UserId))->row();	
		$message = NULL;
		if ($chat != NULL) {

			$hash_id = genereteRandomString();
			$objectInsert = array(
				'hash_id'	=> $hash_id,
				'message' 	=> $newmessage,
				'timestamp' => time(),
				'userId' 	=> $this->session->UserId,
				'chatmId' 	=> $chat->chatmId,
				'visible'	=> 'TRUE'
				);
			$this->db->insert('chatmMessagesTemp', $objectInsert);
			
			$messageId = $this->db->insert_id();
			$sql = "SELECT cm.chatmmessageId, cm.hash_id, cm.userId, cm.message, cm.timestamp, up.name, up.lastName, cm.chatmId
			FROM chatmMessagesTemp AS cm
			INNER JOIN (SELECT name, lastName, userId FROM userPersonalData WHERE userId = ?) AS up ON cm.userId = up.UserId
			WHERE cm.chatmmessageId = ? AND cm.visible = 'TRUE'";
			$message = $this->db->query($sql, array($this->session->UserId, $messageId))->row();
			
			if ($message) {
				$message->date = date('d/m/Y H:i', $message->timestamp);
			}
		}
		return $message;
	}

	function DeleteUser($id, $user){
		$sql="SELECT c.chatmId FROM chatM AS c WHERE c.chatmId= ? && c.userId= ? && visible = 'TRUE' && EXISTS (SELECT 1 FROM chatmUsers AS cmu WHERE c.chatmId = cmu.chatmId && cmu.userId = ?)";
		$chat=$this->db->query($sql, array($id, $this->session->UserId, $user))->row();
		if ($chat != NULL) {
			$objectDelete = array(
				'chatmId' => $id,
				'userId' => $user
				);

			$this->db->delete('chatmUsers',$objectDelete);
			return 'ok';
		}
		else
		{
			return 'invalid';
		}
	}

	function NewMulti(){
		
		if (!$this->input->post('userGroups[]') && !$this->input->post('sites[]')&& !$this->input->post('users[]')) {
			$this->form_validation->set_rules('usersId[]', 'users', 'required');
		}

		$this->form_validation->set_rules('message', 'lang:chat_message', 'required');
		$this->form_validation->set_rules('subject', 'lang:chat_subject', 'required');

		if ($this->form_validation->run() == FALSE)
		{	
			$data = new StdClass();
			$sql = "SELECT * FROM userGroups WHERE userId = ? ORDER BY name";
			$data->userGroups = $this->db->query($sql, $this->session->UserId)->result();
			$sql = "SELECT * FROM sites ORDER BY name";
			$data->sites = $this->db->query($sql)->result();
			$data->navbar = $this->load->view('chat/_navbar', array('title' => $this->lang->line('chat_new')), TRUE);
			$this->load->view('chat/newm', $data);
		}
		else
		{
			$timestamp = time();
			
			$validRole = " AND (userId IN(SELECT userId FROM teamLeaders) OR roleId = 3) AND roleId != ".$this->session->roleId;
			if ($this->Identity->Validate('home/inbox/new/torole')) {
				$validRole = '';
			}
			$objectInsert = array(
				'timestamp' => $timestamp,
				'userId' => $this->session->UserId,
				'label' => $this->input->post('subject'),
				);
			$this->db->insert('chatM', $objectInsert);

			$sql = "SELECT * FROM chatM WHERE userId = ? && timestamp = ? && label = ? && visible = 'TRUE' ";
			$query = $this->db->query($sql, array($this->session->UserId, $timestamp, $this->input->post('subject') ))->row();
			if ($query != NULL) {

				$hash_id = genereteRandomString();
				$objectInsert = array(
					'hash_id'	=> $hash_id,
					'message' 	=> $this->input->post('message'),
					'timestamp' => $timestamp,
					'userId' 	=> $this->session->UserId,
					'chatmId' 	=> $query->chatmId,
					'visible'	=> 'TRUE'
					);
				$this->db->insert('chatmMessagesTemp', $objectInsert);

				$objectInsert = array(
					'timestamp' => $timestamp,
					'userId' => $this->session->UserId,
					'chatmId' => $query->chatmId,
					'firstmessageId' => 0,
					);
				$this->db->insert('chatmUsers', $objectInsert);

				//busqueda userId de team leader
				$this->db->select('tl.userId');
				$this->db->join('teamLeaderOperators tlo', 'tl.teamLeaderId = tlo.teamLeaderId');
				$this->db->where('tlo.userId', $this->session->UserId);
				$tl = $this->db->get('teamLeaders tl',1)->row();

				$isInArray = false;
				if ($this->input->post('users[]')) {
					foreach ($this->input->post('users[]') as $user) {
						$sql = "SELECT userId FROM users WHERE userId != ? && userId = ? ".$validRole;
						$userExist = $this->db->query($sql, array($this->session->UserId, $user))->row();
						if (isset($userExist) && $userExist != NULL) {
							if(!is_null($tl)){
								if($tl->userId == $user){
									$isInArray = true;
								}
							}
							$objectInsert = array(
								'timestamp' => $timestamp,
								'userId' => $user,
								'chatmId' => $query->chatmId,
								'firstmessageId' => 0,
								);
							$this->db->insert('chatmUsers', $objectInsert);
						}
					}
				}
				//agrego el teamLeader si no esta en la lista de usuarios
				if(!is_null($tl)){
					if(!$isInArray){
						$objectInsert = array(
							'timestamp' => $timestamp,
							'userId' => $tl->userId,
							'chatmId' => $query->chatmId,
							'firstmessageId' => 0,
							);
						$this->db->insert('chatmUsers', $objectInsert);
					}
				}

				if ($this->input->post('userGroups[]') && $this->Identity->Validate('usergroups/index') ) {
					foreach ($this->input->post('userGroups[]') as $usergroupKey => $usergroup) {
						$sql = "SELECT * FROM userGroups WHERE usergroupId = ? && userId = ?";
						$groupExist = $this->db->query($sql, array($usergroup, $this->session->UserId))->row();
						if ($groupExist != NULL) {
							$sql = "SELECT * FROM usergroupUsers WHERE usergroupId = ?";
							$groupUsers = $this->db->query($sql,array($groupExist->usergroupId))->result();
							foreach ($groupUsers as $key => $user) {
								$validRole = " AND (userId IN(SELECT userId FROM teamLeaders) OR roleId = 3) AND roleId != ".$this->session->roleId;

								if ($this->Identity->Validate('home/inbox/new/torole')) {
									$validRole = '';
								}
								$sql = "SELECT userId FROM users WHERE userId != ? && userId = ? ".$validRole;
								$userExist = $this->db->query($sql, array($this->session->UserId, $user->userId))->row();
								if (isset($userExist) && $userExist != NULL) {

									$sql = "SELECT userId FROM chatmUsers WHERE chatmId = ? && userId = ?";
									$usermExist = $this->db->query($sql, array($query->chatmId, $user->userId))->row();
									if($usermExist == NULL)
									{
										$objectInsert = array(
											'timestamp' => $timestamp,
											'userId' => $user->userId,
											'chatmId' => $query->chatmId,
											'firstmessageId' => 0,
											);
										$this->db->insert('chatmUsers', $objectInsert);
									}
								}
							}
						}
					}
				}

				if ($this->input->post('sites[]') && $this->Identity->Validate('home/inbox/new/tosites') ) {
					foreach ($this->input->post('sites[]') as $site) {

						$sql = "SELECT * FROM sites WHERE siteId = ?";
						$siteExist = $this->db->query($sql, array($site))->row();
						if ($siteExist != NULL) {

							$validRole = " AND (userId IN(SELECT userId FROM teamLeaders) OR roleId = 3) AND roleId != ".$this->session->roleId;

							if ($this->Identity->Validate('home/inbox/new/torole')) {
								$validRole = '';
							}

							$sql = "SELECT userId FROM users WHERE siteId = ? && userId != ? ".$validRole;
							$siteUsers = $this->db->query($sql,array($siteExist->siteId, $this->session->UserId))->result();
							foreach ($siteUsers as $key => $user) {

								$sql = "SELECT userId FROM chatmUsers WHERE chatmId = ? && userId = ?";
								$usermExist = $this->db->query($sql, array($query->chatmId, $user->userId))->row();
								if($usermExist == NULL)
								{
									$objectInsert = array(
										'timestamp' => $timestamp,
										'userId' => $user->userId,
										'chatmId' => $query->chatmId,
										'firstmessageId' => 0,
										);
									$this->db->insert('chatmUsers', $objectInsert);
								}
							}
						}
					}
				}

				$data = new StdClass();
				$data->status = 'ok';
				$data->chat = $query->chatmId;
				$sql = "SELECT cmu.userId FROM chatmUsers AS cmu WHERE cmu.chatmId = ?";
				$data->to = $this->db->query($sql, $query->chatmId)->result();	
				
				echo escapeJsonString($data, FALSE);
			}
			else
			{
				echo '{"status":"invalid"}';
			}
		}
	}

	function AddMessageMView($id){
		$sql = "SELECT cm.hash_id
		FROM chatmMessagesTemp AS cm
		WHERE cm.hash_id = ? AND cm.visible = 'TRUE' && EXISTS (SELECT 1 FROM chatmUsers AS cu WHERE cm.chatmId = cu.chatmId && cu.userId = ?)" ;
		$message = $this->db->query($sql, array($id, $this->session->UserId))->row();

		if ($message) {
			$objectInsert = array(
				'timestamp' => time(),
				'userId' => $this->session->UserId,
				'chatmmessageId' => $message->hash_id
				);
			$this->db->insert('chatmmessageViews', $objectInsert);
			return 'ok';
		}
		else
		{
			return 'invalid';
		}
	}

	public function SearchUsers($likeuser)
	{
		$sql = "SELECT concat_ws(' ', up.name, up.lastName) AS completeName, u.userId, u.userName 
		FROM ((SELECT userName, roleId, userId FROM users) AS u	INNER JOIN (SELECT name, lastName,userId FROM userPersonalData) AS up ON u.userId = up.userId)
		WHERE (concat_ws(' ', name, lastName) LIKE '%$likeuser%' || userName LIKE '%$likeuser%') && u.userId != ?";
		if(!$this->Identity->Validate('home/inbox/new/torole')){
			$sql.= " AND (u.userId IN(SELECT userId FROM teamLeaders) OR roleId = 3) AND roleId != ".$this->session->roleId;
		}
		$sql.=" ORDER BY completeName LIMIT 20";
		$users = $this->db->query($sql, array($this->session->UserId))->result();
		return $users;
	}

	public function GetUnreadU()
	{
		$unread = 0;
		// $sql = 'SELECT count(*) c FROM chatuMessages WHERE fromuserId != ? && 
		// (SELECT count(*) FROM chatU WHERE chatuId = chatuMessages.chatuId && (fromuserId = ? || touserId = ?)) > 0 && 
		// (SELECT count(*) FROM chatumessageViews WHERE chatumessageId = chatuMessages.chatumessageId && userId = ?) = 0 && 
		// (SELECT count(*) FROM chatuDeletes WHERE userId = ? && chatuId = chatuMessages.chatuId && currentlyRemoved = "FALSE" && lastmessageIdDelete >= chatuMessages.chatumessageId ORDER BY chatudeleteId DESC LIMIT 1) = 0'; 
		// $unread = $this->db->query($sql, array($this->session->UserId, $this->session->UserId, $this->session->UserId, $this->session->UserId, $this->session->UserId))->row()->c; 

		$sql = 'SELECT 	count(*) AS c
		FROM chatuMessagesTemp 
		INNER JOIN (SELECT chatuId FROM chatU WHERE  (fromuserId = ? || touserId = ?))as chatsToUser ON chatsToUser.chatuId = chatuMessagesTemp.chatuId
		LEFT JOIN (SELECT * FROM chatumessageViews WHERE userId = ?) as sawMessages ON chatuMessagesTemp.hash_id = sawMessages.chatumessageId 
		WHERE fromuserId != ?
		AND chatumessageViewsId IS NULL
		AND chatuMessagesTemp.visible = "TRUE"
		AND (SELECT count(*) FROM chatuDeletes WHERE userId = ? && chatuId = chatuMessagesTemp.chatuId && currentlyRemoved = "TRUE") = 0';
		$unread = $this->db->query($sql, array($this->session->UserId,$this->session->UserId, $this->session->UserId, $this->session->UserId, $this->session->UserId))->row()->c;
		return $unread;
	}
	public function GetUnreadM()
	{
		$unread = 0;
		$sql = 'SELECT
		COUNT(cm.chatmmessageId) as c
		FROM
		(SELECT cm.chatmId, cm.chatmmessageId, cm.hash_id FROM chatmMessagesTemp AS cm 
			LEFT JOIN (SELECT chatmUsers.chatmId FROM chatmUsers WHERE userId = ?) cu ON cu.chatmId = cm.chatmId WHERE userId != ? AND cu.chatmId IS NOT NULL AND cm.visible = "TRUE") AS cm
		LEFT JOIN (SELECT chatmmessageViews.chatmmessageId FROM chatmmessageViews WHERE userId = ?) AS cv ON cv.chatmmessageId = cm.hash_id
		WHERE
		cv.chatmmessageId IS NULL';
		$unread = $this->db->query($sql, array($this->session->UserId, $this->session->UserId, $this->session->UserId))->row()->c;
		return $unread;
	}

	function addNewMember($userId,$chatId)
	{
		$objectInsert = array(
			'timestamp' => time(),
			'userId' => $userId,
			'chatmId' => $chatId,
			'firstmessageId' => 0,
			);
		
		$this->db->insert('chatmUsers', $objectInsert);
		return "success";
	}

	function getChatUsers($chatId)
	{
		$sql = "SELECT cmu.userId, up.name, up.lastName FROM chatmUsers AS cmu  INNER JOIN (SELECT name, lastName, userId FROM userPersonalData) AS up ON cmu.userId = up.userId WHERE cmu.chatmId = ?";
		$res = $this->db->query($sql, $chatId)->result();
		return $res;
	}

	function searchChatUsers($chatId,$like)
	{
		$res = 'empty';
		if($like != ''){
			$roleId = "'null'";
			if(!$this->Identity->Validate('home/inbox/new/torole')){
				$this->db->select('roleId');
				$roleId = $this->db->get_where('users',array('userId' => $this->session->UserId))->row();
				$roleId = $roleId->roleId;
			}
			$sql = 
			"SELECT * 
			FROM (SELECT concat_ws(' ', up.name, up.lastName) as completeName, u.userId, u.userName 
						FROM users AS u 
						INNER JOIN (SELECT name, lastName, userId FROM userPersonalData) AS up 
						ON u.userId = up.userId 
						WHERE u.userId NOT IN (SELECT cmu.userId from chatmUsers as cmu WHERE cmu.chatmId = $chatId)
						AND roleId <> $roleId) as `all`
            WHERE completeName LIKE '%$like%' || userName LIKE '%$like%' ORDER BY completeName LIMIT 20";
			
			$query = $this->db->query($sql)->result();
			if (count($query) > 0) {
				$res = new stdClass();
				$res->status = 'ok';
				$res->data = $query;
			}
		}
		return $res;
	}

	function updateSubject($chatData)
	{	
		$res = new stdClass();
		$res->status = 'invalid';
		$query = null;

		if(strlen($chatData->newSubject)>250){
			$res->status = 'error';
		}
		else{
			$sql =
			"UPDATE chatM 
			SET label= ? 
			WHERE chatmId = ?";
			$query = $this->db->query($sql,array("label"=>$chatData->newSubject,"chatId"=>$chatData->chatId));
			if(isset($query)){
				$res->status = 'ok';
				$sql =
				"SELECT userId 
				FROM  chatmUsers
				WHERE chatmId = ?";
				$query = $this->db->query($sql,array("chatId"=>$chatData->chatId))->result();
				if (isset($query)){
					$res->unread = $query;
				}
			}
		}
		return $res;
	}

	function isChatDeleted($chatId)
	{
		$sql = "SELECT 1 FROM chatuDeletes AS cd WHERE cd.chatuId = ?  && cd.currentlyRemoved = 'TRUE'";
		$chat = $this->db->query($sql, array($chatId))->row();
		if(isset($chat))
		{
			return TRUE;
		}
		else
			return FALSE;
	}

	function SearchChats($filters)
	{
		$chats = new StdClass();
		$chats->status = 'error';
		$chatsU = array();
		$chatsM = array();
		
		//obtener chats unicos
		if($this->validateFilter($filters,'users')){
			$chatsUchatIds = array();
			for ($i=0; $i < sizeof($filters['users']) ; $i++) { 
				$this->db->select('c.chatuId,c.fromuserId,c.touserId');
				$this->db->where(" NOT EXISTS (SELECT 1 FROM chatuDeletes AS cd WHERE c.chatuId = cd.chatuId && cd.currentlyRemoved = 'TRUE' )");
				$this->db->where("(c.fromuserId = ".$filters['users'][$i]." OR "."c.touserId = ".$filters['users'][$i].")");
				if (sizeof($chatsUchatIds)>0){
					$this->db->where_not_in('c.chatuId',$chatsUchatIds);
				}
				$chatsUres = $this->db->get('chatU as c')->result();

				for ($k=0; $k < sizeof($chatsUres); $k++) { 
					array_push($chatsU, $chatsUres[$k]);
					array_push($chatsUchatIds, $chatsUres[$k]->chatuId);
				}
			}	
		}
		//if($this->validateFilter($filters,'teamLeader')){ TODO: agregar distincion de teamleaders
		//	$this->db->where("(c.fromuserId = ".$filters['teamLeader']." OR "."c.touserId = ".$filters['teamLeader'].")");
		//}
		
		//esta validacion puede servir si se quiere optimizar la busqueda
		// if($this->validateFilter($filters,'keyword') || ($this->validateFilter($filters,'dateStart') && $this->validateFilter($filters,'dateFinish')) ){
		
		foreach ($chatsU as $chat) {
			//solamente usamos el nombre del que creo el chat, total los mensajes los podemos ver adentro no?
			$sql = 'SELECT concat_ws(" ",name, lastName) as label FROM userPersonalData WHERE userId = ?';
			$res = $this->db->query($sql, $chat->fromuserId)->row();
			$chat->label = $res->label;

			$this->db->select('cm.fromuserid,cm.message,DATE_FORMAT(FROM_UNIXTIME(cm.timestamp),"%e-%m-%Y") as date');
			$this->db->where(array('cm.chatuId' => $chat->chatuId,'visible =' => 'TRUE'));

			if($this->validateFilter($filters,'keyword')){
				$this->db->like('cm.message', $filters['keyword']);
			}
			if($this->validateFilter($filters,'dateStart') && $this->validateFilter($filters,'dateFinish')){
				$this->db->where('timestamp >=', strtotime($filters['dateStart']));
				$this->db->or_where('timestamp <=', strtotime($filters['dateFinish']));
			}
			$this->db->order_by('cm.timestamp', 'desc');
			$chat->message = $this->db->get('chatuMessagesTemp cm', 1)->row();

			if(!is_null($chat->message)){
				$chat->type="unique";
				$chats->status = "success";
				$chats->chats[] = $chat;
			}

		}
		
		//buscamos los mensajes de los chats backup
		if($this->Identity->Validate('chat/searchAdmin')){
			foreach ($chatsU as $chat) {
				$sql = 'SELECT concat_ws(" ",name, lastName) as label FROM userPersonalData WHERE userId = ?';
				$res = $this->db->query($sql, $chat->fromuserId)->row();
				$chat->label = $res->label;
	
				$this->db->select('cm.fromuserid,cm.message,DATE_FORMAT(FROM_UNIXTIME(cm.timestamp),"%e-%m-%Y") as date');
				$this->db->where(array('cm.chatuId' => $chat->chatuId));
	
				if($this->validateFilter($filters,'keyword')){
					$this->db->like('cm.message', $filters['keyword']);
				}
				if($this->validateFilter($filters,'dateStart') && $this->validateFilter($filters,'dateFinish')){
					$this->db->where('timestamp >=', strtotime($filters['dateStart']));
					$this->db->or_where('timestamp <=', strtotime($filters['dateFinish']));
				}
				$this->db->order_by('cm.timestamp', 'desc');
				$chat->message = $this->db->get('chatuMessages cm', 1)->row();
	
				if(!is_null($chat->message)){
					$chat->type="unique";
					$chats->status = "success";
					$chats->chats[] = $chat;
				}
	
			}
		}

		
		//obtener chats multiples
		if($this->validateFilter($filters,'users')){
			$chatsMchatIds = array();
			for ($i=0; $i < sizeof($filters['users']); $i++) { 
				$this->db->select('cm.chatmId,label');
				$this->db->join('chatmUsers cmu', 'cm.chatmId = cmu.chatmId','left');
				$this->db->where('cmu.userId', $filters['users'][$i]);
				if(sizeof($chatsMchatIds) > 0) {
					$this->db->where_not_in('cm.chatmId',$chatsMchatIds);
				}
				$chatsMres = $this->db->get('chatM as cm')->result();
				
				for ($j=0; $j < sizeof($chatsMres); $j++) { 
					array_push($chatsM, $chatsMres[$j]);
					array_push($chatsMchatIds, $chatsMres[$j]->chatmId);
				}
			}
		}
		
		//if($this->validateFilter($filters,'teamLeader')){
		//	$this->db->where('cmu.userId', $filters['teamLeader']);
		//}

		
		foreach ($chatsM as $chat) {
			
			$this->db->select('cm.message,DATE_FORMAT(FROM_UNIXTIME(cm.timestamp),"%e-%m-%Y") as date');
			$this->db->where(array('cm.chatmId' => $chat->chatmId,'visible = '=> 'TRUE'));
			if($this->validateFilter($filters,'keyword')){
				$this->db->like('cm.message', $filters['keyword']);
			}
			if($this->validateFilter($filters,'dateStart') && $this->validateFilter($filters,'dateFinish')){
				$this->db->where('timestamp >=', strtotime($filters['dateStart']));
				$this->db->or_where('timestamp <=', strtotime($filters['dateFinish']));
			}
			$this->db->order_by('cm.timestamp', 'desc');
			$chat->message = $this->db->get('chatmMessagesTemp cm', 1)->row();
			
			if(!is_null($chat->message)){
				$chat->type = "multi";
				$chats->status = "success";
				$chats->chats[] = $chat;
			}
		}

		//buscamos los mensajes de back
		if($this->Identity->Validate('chat/searchAdmin')){
			foreach ($chatsM as $chat) {
		
				$this->db->select('cm.message,DATE_FORMAT(FROM_UNIXTIME(cm.timestamp),"%e-%m-%Y") as date');
				$this->db->where(array('cm.chatmId' => $chat->chatmId));
				if($this->validateFilter($filters,'keyword')){
					$this->db->like('cm.message', $filters['keyword']);
				}
				if($this->validateFilter($filters,'dateStart') && $this->validateFilter($filters,'dateFinish')){
					$this->db->where('timestamp >=', strtotime($filters['dateStart']));
					$this->db->or_where('timestamp <=', strtotime($filters['dateFinish']));
				}
				$this->db->order_by('cm.timestamp', 'desc');
				$chat->message = $this->db->get('chatmMessages cm', 1)->row();
				
				if(!is_null($chat->message)){
					$chat->type = "multi";
					$chats->status = "success";
					$chats->chats[] = $chat;
				}
			}
		}
		
		insert_audit_logs('SEARCH','SEARCH',$filters);
		return $chats;
	}

	function validateFilter($filters,$filter)
	{
		if(array_key_exists($filter,$filters) && !empty($filters[$filter])) return true;
		return false;
	}

	function GetMessageInfo($messageId,$type)
	{
		$data = new StdClass();
		$data->status ="empty";

		$this->db->select("concat_ws(' ', up.name, up.lastName) AS completeName, FROM_UNIXTIME(cm.timestamp) as date");
		$this->db->join('userPersonalData up', 'up.userId = cm.userId');
		if($type == "unique"){
			$this->db->where('cm.chatumessageId', $messageId);
			$message = $this->db->get('chatumessageViews cm')->result();
		}
		else{
			$this->db->where('cm.chatmmessageId', $messageId);
			$message = $this->db->get('chatmmessageViews cm')->result();
		}
		
		if(!is_null($message)){
			$data->status = "success";
			$data->data = $message;
		}
		return $data;
	}

}