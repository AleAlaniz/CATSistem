<?php
Class Theme_model extends CI_Model {

	function GetThemes(){

		if ($this->input->post('themeId')) {
			$sql = "SELECT * FROM themes WHERE themeId = ?";
			$query = $this->db->query($sql,array($this->input->post('themeId')))->row();
			if (isset($query))
			{
				$objectEdit = array(
					'isselected' => 'FALSE',
					);
				$this->db->where('isselected', 'TRUE');
				$this->db->update('themes', $objectEdit);
				
				$objectEdit = array(
					'isselected' => 'TRUE',
					);
				$this->db->where('themeId', $query->themeId);
				$this->db->update('themes', $objectEdit);
				$this->session->set_flashdata('flashMessage', 'save');
			}
		}

		$query = new stdClass();
		$sql = "SELECT * FROM themes ORDER BY name";
		$query->model = $this->db->query($sql)->result();

		$this->load->view('_shared/_administrationlayoutheader');
		$this->load->view('themes/themes', $query);
		$this->load->view(PRELAYOUTFOOTER);

	}

	function CreateTheme(){

		$this->form_validation->set_rules('name', 'lang:administration_themes_name', 'required|is_unique[themes.name]',
			array(
				'is_unique'     => $this->lang->line('administration_themes_nameexist')
				));


		if ($this->form_validation->run() == FALSE)
		{
			$data['error'] = NULL;
			$this->load->view('_shared/_administrationlayoutheader');
			$this->load->view('themes/create', $data);
			$this->load->view(PRELAYOUTFOOTER);
		}
		else
		{
			$this->do_upload();	
		}
	}

	function do_upload()
	{	   
		$timestamp = time();
		$objecttoInsert = array(
			'name' => $this->input->post('name'),
			'timestamp' => $timestamp,
			'userId' => $this->session->UserId,
			'isselected' => 'FALSE',  
			);
		$this->db->insert('themes', $objecttoInsert);

		$sql = "SELECT * FROM themes WHERE name = ? && timestamp = ?";
		$query = $this->db->query($sql, array($this->input->post('name'), $timestamp))->row();
		if (isset($query))
		{
			$this->load->library('upload');
			$files = $_FILES;
			$cpt = count($_FILES['userfile']['name']);
			for($i=0; $i<$cpt; $i++)
			{
				$_FILES['userfile']['name']= $files['userfile']['name'][$i];
				$_FILES['userfile']['type']= $files['userfile']['type'][$i];
				$_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$i];
				$_FILES['userfile']['error']= $files['userfile']['error'][$i];
				$_FILES['userfile']['size']= $files['userfile']['size'][$i];	

				$this->upload->initialize($this->set_upload_options());
				if (!$this->upload->do_upload())
				{
					$objectDelete = array(
						'themeId' => $query->themeId
						);
					$this->db->delete('themes', $objectDelete);
					$data['error'] = $this->upload->display_errors();
					$this->load->view('_shared/_administrationlayoutheader');
					$this->load->view('themes/create', $data);
					$this->load->view(PRELAYOUTFOOTER);
					return;
				}
				else
				{
					$objecttoInsert = array(
						'image' => $this->upload->data('file_name'),
						'timestamp' => $timestamp,
						'userId' => $this->session->UserId,
						'themeId' => $query->themeId,  
						);
					$this->db->insert('themeImages', $objecttoInsert);

				}
			}
			$this->session->set_flashdata('flashMessage', 'create');
			header('Location:/'.FOLDERADD.'/themes');
		}
		else
		{
			show_404();
		}
	}

	function do_edit($query)
	{	   
		$timestamp = time();
		$this->load->library('upload');
		$files = $_FILES;
		$cpt = count($_FILES['userfile']['name']);
		for($i=0; $i<$cpt; $i++)
		{
			$_FILES['userfile']['name']= $files['userfile']['name'][$i];
			$_FILES['userfile']['type']= $files['userfile']['type'][$i];
			$_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$i];
			$_FILES['userfile']['error']= $files['userfile']['error'][$i];
			$_FILES['userfile']['size']= $files['userfile']['size'][$i];	

			$this->upload->initialize($this->set_upload_options());
			if (!$this->upload->do_upload())
			{
				$query->error = $this->upload->display_errors();
				$sql = "SELECT * FROM themeImages WHERE themeId = ?";
				$query->images = $this->db->query($sql,array($query->themeId))->result();
				$query->navBar = $this->load->view('themes/_navbar', $query, TRUE);
				$this->load->view('_shared/_administrationlayoutheader');
				$this->load->view('themes/edit',$query);
				$this->load->view(PRELAYOUTFOOTER);
			}
			else
			{
				$objecttoInsert = array(
					'image' => $this->upload->data('file_name'),
					'timestamp' => $timestamp,
					'userId' => $this->session->UserId,
					'themeId' => $query->themeId,  
					);
				$this->db->insert('themeImages', $objecttoInsert);

			}
		}
		$this->session->set_flashdata('flashMessage', 'edit');
		header('Location:/'.FOLDERADD.'/themes/details/'.$query->themeId);
	}


	private function set_upload_options()
	{   
    //upload an image options
		$config = array();
		$config['upload_path'] = './catapp/libraries/themes/images';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']             = 10240;
		$config['file_ext_tolower']     = TRUE;
		$config['remove_spaces']     	= FALSE;
		$config['encrypt_name']     	= TRUE;


		return $config;
	}

	function DetailsTheme(){
		if ($this->uri->segment(3)) {
			$sql = "SELECT * FROM themes WHERE themeId = ?";
			$query = $this->db->query($sql,array($this->uri->segment(3)))->row();
			if (isset($query))
			{
				$sql = "SELECT * FROM themeImages WHERE themeId = ?";
				$query->images = $this->db->query($sql,array($query->themeId))->result();

				$query->navBar = $this->load->view('themes/_navbar', $query, TRUE);
				$this->load->view('_shared/_administrationlayoutheader');
				$this->load->view('themes/details', $query);
				$this->load->view(PRELAYOUTFOOTER);
			}
			else
			{
				show_404();
			}
		}
		else{
			show_404();
		}
	}

	function DeleteTheme(){
		if ($this->uri->segment(3)) {
			$sql = "SELECT * FROM themes WHERE themeId = ?";
			$query = $this->db->query($sql,array($this->uri->segment(3)))->row();
			if (isset($query))
			{
				if ($this->input->post('themeId') && $this->input->post('themeId') == $query->themeId ) {
					$sectionDelete = array(
						'themeId' => $this->input->post('themeId')
						);
					$this->db->delete('themes', $sectionDelete);
					$this->session->set_flashdata('flashMessage', 'delete');
					header('Location:/'.FOLDERADD.'/themes');
				}
				else
				{
					$sql = "SELECT * FROM themeImages WHERE themeId = ?";
					$query->images = $this->db->query($sql,array($query->themeId))->result();
					$query->navBar = $this->load->view('themes/_navbar', $query, TRUE);
					$this->load->view('_shared/_administrationlayoutheader');
					$this->load->view('themes/delete', $query);
					$this->load->view(PRELAYOUTFOOTER);
				}
			}
			else
			{
				show_404();
			}
		}
		else{
			show_404();
		}
	}

	function Edit(){
		if ($this->uri->segment(3)) {
			$sql = "SELECT * FROM themes WHERE themeId = ?";
			$query = $this->db->query($sql,array($this->uri->segment(3)))->row();
			if (isset($query))
			{
				$nameRules = 'required|is_unique[themes.name]';
				$nameRulesMessages = array('is_unique' => $this->lang->line('administration_themes_nameexist'));

				if ($this->input->post('name') && $query->name == $this->input->post('name')) {
					$nameRules = 'required';
					$nameRulesMessages = NULL;
				}

				$this->form_validation->set_rules('name', 'lang:administration_sections_name', $nameRules ,$nameRulesMessages);

				if ($this->form_validation->run() == FALSE)
				{
					$query->error = NULL;
					$sql = "SELECT * FROM themeImages WHERE themeId = ?";
					$query->images = $this->db->query($sql,array($query->themeId))->result();
					$query->navBar = $this->load->view('themes/_navbar', $query, TRUE);
					$this->load->view('_shared/_administrationlayoutheader');
					$this->load->view('themes/edit',$query);
					$this->load->view(PRELAYOUTFOOTER);
				}
				else
				{
					$objectEdit = array(
						'name' => $this->input->post('name')
						);
					$this->db->where('themeId', $query->themeId);
					$this->db->update('themes', $objectEdit);
					$this->do_edit($query);	
				}
				
			}
			else
			{
				show_404();
			}
		}
		else{
			show_404();
		}
	}

	function DeleteImage(){
		if ($this->uri->segment(3)) {
			$sql = "SELECT * FROM themeImages WHERE themeimageId = ?";
			$query = $this->db->query($sql,array($this->uri->segment(3)))->row();
			if (isset($query))
			{
				$objecttoDetele = array(
					'themeimageId' => $query->themeimageId
					);
				$this->db->delete('themeImages', $objecttoDetele);
				unlink('./catapp/libraries/themes/images/'.$query->image); 
				header('Location:/'.FOLDERADD.'/themes/edit/'.$query->themeId);
				
			}
			else
			{
				show_404();
			}
		}
		else{
			show_404();
		}
	}


}
