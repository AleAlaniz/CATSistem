<?php

Class Corporatedocs_model extends CI_Model {


    public function GetCategories()
    {
        $sql="SELECT cdcat.name,cdcat.corporatedoccatfather,cdcat.corporatedoccategoryId 
                FROM corporatedoccatlinks dlcat 
                INNER JOIN corporatedoccategories cdcat ON dlcat.corporatedoccatId = cdcat.corporatedoccategoryId 
                WHERE (cdcat.userId = ? || dlcat.userId = ? || dlcat.siteId = ? || (case when dlcat.sectionId is null then dlcat.roleId = ? when dlcat.roleId is null then dlcat.sectionId = ? else dlcat.sectionId = ? && dlcat.roleId = ? end)) 
                GROUP BY dlcat.corporatedoccatId ORDER BY cdcat.name ASC";

        return  $this->db->query($sql,
            array(
                $this->session->UserId,
                $this->session->UserId,
                $this->session->siteId,
                $this->session->Role,
                $this->session->sectionId,
                $this->session->sectionId,
                $this->session->Role))->result();
    }

	public function GetDocs()
	{
		$sql="SELECT cd.name,cd.description,cd.corporatedoccategoryId,cd.corporatedocId,
		(SELECT 1 FROM corporatedocviews AS dv WHERE dv.corporatedocId = cd.corporatedocId && dv.userId = ? LIMIT 1) AS isView
		FROM corporatedoclinks dl INNER JOIN corporatedocs cd ON dl.corporatedocId = cd.corporatedocId
		WHERE (cd.userId = ? || dl.userId = ? || dl.siteId = ? || (case when dl.sectionId is null then dl.roleId = ? when dl.roleId is null then dl.sectionId = ? else dl.sectionId = ? && dl.roleId = ? end))
		GROUP BY dl.corporatedocId
		ORDER BY cd.timestamp DESC";

		return  $this->db->query($sql, 
			array(
				$this->session->UserId,
				$this->session->UserId,
				$this->session->UserId,
				$this->session->siteId,
				$this->session->Role, 
				$this->session->sectionId,
				$this->session->sectionId,
				$this->session->Role))->result();
	}

	public function GetNoViewDoc($id)
	{
		
		$sql = "SELECT cd.name, cd.description, cd.corporatedoccategoryId 
		FROM corporatedoclinks AS dl 
		INNER JOIN corporatedocs AS cd ON dl.corporatedocId = cd.corporatedocId 
		WHERE (cd.userId = ? || dl.userId = ? || dl.siteId = ? || (case when dl.sectionId is null then dl.roleId = ? when dl.roleId is null then dl.sectionId = ? else dl.sectionId = ? && dl.roleId = ? end))
		&& NOT EXISTS (SELECT 1 FROM corporatedocviews AS dv WHERE dv.corporatedocId = dl.corporatedocId && dv.userId = ? LIMIT 1) 
		&& dl.corporatedocId = ?
		LIMIT 1";
		return  $this->db->query($sql,
			array(
				$this->session->UserId,
				$this->session->UserId,
				$this->session->siteId,
				$this->session->Role,
				$this->session->sectionId,
				$this->session->sectionId,
				$this->session->Role,
				$this->session->UserId,
				$id))->row();
	}

	public function CheckView($id)
	{
		$objectInsert 		= array(
			'corporatedocId' => $id,
			'userId' 		 => $this->session->UserId,
			'timestamp' 	 => time(),
			);
		$this->db->insert('corporatedocviews', $objectInsert);

		return $this->db->affected_rows() > 0;
	}

	public function GetViewDoc($id)
	{

		$sql = "SELECT cd.name, cd.description, cd.corporatedoccategoryId , cd.file
		FROM corporatedoclinks AS dl 
		INNER JOIN corporatedocs AS cd ON dl.corporatedocId = cd.corporatedocId 
		WHERE (cd.userId = ? || dl.userId = ? || dl.siteId = ? || (case when dl.sectionId is null then dl.roleId = ? when dl.roleId is null then dl.sectionId = ? else dl.sectionId = ? && dl.roleId = ? end))
		&& EXISTS (SELECT 1 FROM corporatedocviews AS dv WHERE dv.corporatedocId = dl.corporatedocId && dv.userId = ? LIMIT 1) 
		&& dl.corporatedocId = ?
		LIMIT 1";

		return  $this->db->query($sql,
			array(
				$this->session->UserId,
				$this->session->UserId,
				$this->session->siteId,
				$this->session->Role,
				$this->session->sectionId,
				$this->session->sectionId,
				$this->session->Role,
				$this->session->UserId,
				$id))->row();
	}

	public function GetReport($id,$type)
	{
		$sql = "";

		if($type == 'read')
		{
			$sql = "SELECT up.name, up.lastName, s.name AS section 
			FROM corporatedocviews AS dv
			INNER JOIN (SELECT sectionId, userId FROM users) AS u ON dv.userId = u.userId 
			INNER JOIN (SELECT name, lastName, userId FROM userPersonalData) AS up ON dv.userId = up.userId 
			INNER JOIN sections AS s ON s.sectionId = u.sectionId 
			WHERE dv.corporatedocId = ?
			ORDER BY up.name";

		}
		else
		{
			$sql = "SELECT up.name, up.lastName, s.name AS section
			FROM users u
			JOIN userPersonalData up ON u.userId = up.userId
			JOIN sections s ON u.sectionId = s.sectionId
			WHERE exists (SELECT 1 FROM corporatedoclinks cl WHERE cl.corporatedocId = ? AND (cl.userId = u.userId OR cl.roleId = u.roleId OR cl.sectionId = u.sectionId OR cl.siteId = u.siteId))  
			AND NOT EXISTS (SELECT 1 FROM corporatedocviews cv WHERE cv.corporatedocId = ? AND cv.userId = u.userId)
			AND active = 1
			ORDER BY up.name";
		
			$id = array($id,$id);
		}

		return  $this->db->query($sql, $id)->result();
	}

    public function CreateCategory(){
        $this->form_validation->set_rules('name', 'lang:corporatedocs_name', 'required');
        if (!$this->input->post('roles[]') && !$this->input->post('sections[]') && !$this->input->post('sites[]') && !$this->input->post('userGroups[]') && !$this->input->post('users[]')) {
            $this->form_validation->set_rules('roles[]', 'lang:corporatedocs_link', 'required');
        }
        //== falso primera carga de la pagina.
        if ($this->form_validation->run() == FALSE)
        {
            $data = new stdClass();
            $sql = "SELECT name,sectionId FROM sections ORDER BY name";
            $data->sections = $this->db->query($sql)->result();
            $sql = "SELECT name,siteId FROM sites ORDER BY name";
            $data->sites = $this->db->query($sql)->result();
            $sql = "SELECT name,roleId FROM roles ORDER BY name";
            $data->roles = $this->db->query($sql)->result();
            $sql = "SELECT * FROM userGroups WHERE userId = ? ORDER BY name";
            $data->userGroups = $this->db->query($sql, $this->session->UserId)->result();
            $data->error = '';
            $this->load->view('corporatedocs/createcategory', $data);
        }else{
        //empieza la creacion
            $objectInsert = array(
                'name' => $this->input->post('name'),
                'timestamp' => time(),
                'userId' => $this->session->UserId
            );
        //si seleccionó una categoria
            if( $this->input->post('category') && $this->input->post('category') != -1 )
                $objectInsert['corporatedoccatfather'] = $this->input->post('category');

            $this->db->insert('corporatedoccategories', $objectInsert);
        //se creo la categoria, ahora vemos quienes son los viewers
            $query=new stdClass();
            $query->corporatedoccatId=$this->db->insert_id();
            //esto obtiene el ultimo id insertado y en el if comprobamos que se obtuvo posta.
            if (isset($query) && $query != NULL && $query != '')
            {
                //obtenemos la lista de checks
                $sections 	= $this->input->post('sections[]');
                $roles 		= $this->input->post('roles[]');
                $sites 		= $this->input->post('sites[]');
                $userGroups = $this->input->post('userGroups[]');
                $users 		= $this->input->post('users[]');

                if(count($roles) == 0) // caso solo se insertan secciones
                {
                    if($sections)
                    {
                        foreach ($sections as $section) {
                            $sql 	= "SELECT name FROM sections WHERE sectionId = ?";
                            $exist 	= $this->db->query($sql, $section)->row();
                            if (isset($exist)) {
                                $sql 	= "SELECT corporatedoccatId FROM corporatedoccatlinks WHERE corporatedoccatId = ? && sectionId = ?";
                                $exist 	= $this->db->query($sql, array($query->corporatedoccatId, $section))->row();
                                if (!isset($exist)) {
                                    $objectInsert = array(
                                        'corporatedoccatId' 	=> $query->corporatedoccatId,
                                        'sectionId' 	=> $section,
                                    );
                                    $this->db->insert('corporatedoccatlinks', $objectInsert);
                                }
                            }
                        }
                    }

                }
                else if(count($sections) == 0) // caso solo se insertan roles
                {
                    if($roles)
                    {
                        foreach ($roles as $role) {
                            $sql 	= "SELECT name FROM roles WHERE roleId = ?";
                            $exist 	= $this->db->query($sql, $role)->row();
                            if (isset($exist)) {
                                $sql 	= "SELECT corporatedoccatId FROM corporatedoccatlinks WHERE corporatedoccatId = ? && roleId = ?";
                                $exist 	= $this->db->query($sql, array($query->corporatedoccatId, $role))->row();
                                if (!isset($exist)) {
                                    $objectInsert 		= array(
                                        'corporatedoccatId' 	=> $query->corporatedoccatId,
                                        'roleId' 			=> $role,
                                    );
                                    $this->db->insert('corporatedoccatlinks', $objectInsert);
                                }
                            }
                        }
                    }

                }
                else // caso hay ambos para insertar
                {
                    foreach($sections as $section)
                    {
                        foreach($roles as $role)
                        {
                            $sqlSection 	= "SELECT name FROM sections WHERE sectionId = ?";
                            $existSection 	= $this->db->query($sqlSection, $section)->row();

                            $sqlRole 	= "SELECT name FROM roles WHERE roleId = ?";
                            $existRole 	= $this->db->query($sqlRole, $role)->row();

                            if (isset($existSection) && isset($existRole))
                            {
                                $sql 	= "SELECT corporatedoccatId FROM corporatedoccatlinks WHERE corporatedoccatId = ? && sectionId = ? && roleId = ?";
                                $exist 	= $this->db->query($sql, array($query->corporatedoccatId, $section, $role))->row();

                                if (!isset($exist))
                                {
                                    $objectInsert = array(
                                        'corporatedoccatId' => $query->corporatedoccatId,
                                        'sectionId' 	 => $section,
                                        'roleId' 		 => $role,
                                    );
                                    $this->db->insert('corporatedoccatlinks', $objectInsert);
                                }
                            }
                        }
                    }
                }


                if($sites)
                {
                    foreach ($sites as $site) {
                        $sql 	= "SELECT name FROM sites WHERE siteId = ?";
                        $exist 	= $this->db->query($sql, $site)->row();
                        if (isset($exist)) {
                            $sql 	= "SELECT corporatedoccatId FROM corporatedoccatlinks WHERE corporatedoccatId = ? && siteId = ?";
                            $exist 	= $this->db->query($sql, array($query->corporatedoccatId, $site))->row();
                            if (!isset($exist)) {
                                $objectInsert 		= array(
                                    'corporatedoccatId' 	=> $query->corporatedoccatId,
                                    'siteId' 			=> $site,
                                );
                                $this->db->insert('corporatedoccatlinks', $objectInsert);
                            }
                        }
                    }
                }

                if($userGroups && $this->Identity->Validate('usergroups/create'))
                {
                    foreach ($userGroups as $userGroup) {
                        $sql 	= "SELECT * FROM userGroups WHERE usergroupId = ? && userId = ?";
                        $exist 	= $this->db->query($sql, array($userGroup, $this->session->UserId))->row();
                        if (isset($exist)) {
                            $sql 	= "SELECT * FROM usergroupUsers WHERE usergroupId = ?";
                            $group 	= $this->db->query($sql, $userGroup)->result();
                            if (isset($group) && count($group) > 0) {
                                foreach ($group as $user) {
                                    $sql 	= "SELECT corporatedoccatId FROM corporatedoccatlinks WHERE corporatedoccatId = ? && userId = ?";
                                    $exist 	= $this->db->query($sql, array($query->corporatedoccatId, $user->userId))->row();
                                    if (!isset($exist)) {
                                        $objectInsert = array(
                                            'corporatedoccatId'	=> $query->corporatedoccatId,
                                            'userId' 			=> $user->userId,
                                        );
                                        $this->db->insert('corporatedoccatlinks', $objectInsert);
                                    }
                                }
                            }
                        }
                    }
                }

                if ($users) {
                    foreach ($users as $user) {
                        $sql 	= "SELECT userId FROM users WHERE userId = ?";
                        $exist 	= $this->db->query($sql, $user)->row();
                        if (isset($exist)) {
                            $sql 	= "SELECT corporatedoccatId FROM corporatedoccatlinks WHERE corporatedoccatId = ? && userId = ?";
                            $exist 	= $this->db->query($sql, array($query->corporatedoccatId, $user))->row();
                            if (!isset($exist)) {
                                $objectInsert 		= array(
                                    'corporatedoccatId' 	=> $query->corporatedoccatId,
                                    'userId' 			=> $user,
                                );
                                $this->db->insert('corporatedoccatlinks', $objectInsert);
                            }
                        }
                    }
                }

            }
            $response = new StdClass();
            $response->status = 'ok';
            $response->message = $this->lang->line('corporatedocs_categorysuccessmessage');
            echo escapeJsonString($response, FALSE);
        }


    }
    public function EditCategory()
    {
        //set rules
        $data = new StdClass();
        $this->form_validation->set_rules('name', 'lang:corporatedocs_name', 'required');

        if (!$this->input->post('roles[]') && !$this->input->post('sections[]') && !$this->input->post('sites[]') && !$this->input->post('userGroups[]') && !$this->input->post('users[]')) {
            $this->form_validation->set_rules('sections[]', 'lang:corporatedocs_link', 'required');
        }
        //first load
        if ($this->form_validation->run() == FALSE  && !$this->uri->segment(3))
        {
            $sql = "SELECT name,sectionId FROM sections ORDER BY name";
            $data->sections = $this->db->query($sql)->result();
            $sql = "SELECT * FROM corporatedoccategories ORDER BY name";
            $data->categories = $this->db->query($sql)->result();
            $sql = "SELECT name,siteId FROM sites ORDER BY name";
            $data->sites = $this->db->query($sql)->result();
            $sql = "SELECT name,roleId FROM roles ORDER BY name";
            $data->roles = $this->db->query($sql)->result();
            $sql = "SELECT * FROM userGroups WHERE userId = ? ORDER BY name";
            $data->userGroups = $this->db->query($sql, $this->session->UserId)->result();
            $this->load->view('corporatedocs/editcategory', $data);
        }
        else    //edit time
        {
            //obtain category data
            $sql = "SELECT * FROM corporatedoccategories WHERE corporatedoccategoryId = ?";
            $data = $this->db->query($sql,$this->uri->segment(3))->row();
            if (isset($data)) {
                $timestamp = time();
                if ($this->input->post('name')) {
                    $objectEdit = array(
                        'corporatedoccatfather' => $this->input->post('category') && $this->input->post('category') != -1 ? $this->input->post('category') : null,
                        'name' => $this->input->post('name') ? $this->input->post('name') : null,
                    );
                    $this->db->where('corporatedoccategoryId', $data->corporatedoccategoryId);
                    $this->db->update('corporatedoccategories', $objectEdit);
                }
                $sections 	= $this->input->post('sections[]');
                $roles 		= $this->input->post('roles[]');
                $sites 		= $this->input->post('sites[]');
                $userGroups = $this->input->post('userGroups[]');
                $users 		= $this->input->post('users[]');
                $objectDelete = array(
                    'corporatedoccatId' => $data->corporatedoccategoryId
                );
                $this->db->delete('corporatedoccatlinks', $objectDelete);

                if(count($roles) == 0)
                {
                    if($sections)
                    {
                        foreach ($sections as $section) {
                            $sql 	= "SELECT name FROM sections WHERE sectionId = ?";
                            $exist 	= $this->db->query($sql, $section)->row();
                            if (isset($exist)) {
                                $sql 	= "SELECT corporatedoccatId FROM corporatedoccatlinks WHERE corporatedoccatId = ? && sectionId = ?";
                                $exist 	= $this->db->query($sql, array($this->uri->segment(3), $section))->row();
                                if (!isset($exist)) {
                                    $objectInsert = array(
                                        'corporatedoccatId' 	=> $this->uri->segment(3),
                                        'sectionId' 	=> $section,
                                    );
                                    $this->db->insert('corporatedoccatlinks', $objectInsert);
                                }
                            }
                        }
                    }

                }
                else if(count($sections) == 0)
                {
                    if($roles)
                    {
                        foreach ($roles as $role) {
                            $sql 	= "SELECT name FROM roles WHERE roleId = ?";
                            $exist 	= $this->db->query($sql, $role)->row();
                            if (isset($exist)) {
                                $sql 	= "SELECT corporatedoccatId FROM corporatedoccatlinks WHERE corporatedoccatId = ? && roleId = ?";
                                $exist 	= $this->db->query($sql, array($this->uri->segment(3), $role))->row();
                                if (!isset($exist)) {
                                    $objectInsert 		= array(
                                        'corporatedoccatId' 	=> $this->uri->segment(3),
                                        'roleId' 			=> $role,
                                    );
                                    $this->db->insert('corporatedoccatlinks', $objectInsert);
                                }
                            }
                        }
                    }

                }
                else
                {
                    foreach($sections as $section)
                    {
                        foreach($roles as $role)
                        {
                            $sqlSection 	= "SELECT name FROM sections WHERE sectionId = ?";
                            $existSection 	= $this->db->query($sqlSection, $section)->row();

                            $sqlRole 	= "SELECT name FROM roles WHERE roleId = ?";
                            $existRole 	= $this->db->query($sqlRole, $role)->row();

                            if (isset($existSection) && isset($existRole))
                            {
                                $sql 	= "SELECT corporatedoccatId FROM corporatedoccatlinks WHERE corporatedoccatId = ? && sectionId = ? && roleId = ?";
                                $exist 	= $this->db->query($sql, array($this->uri->segment(3), $section, $role))->row();

                                if (!isset($exist))
                                {
                                    $objectInsert = array(
                                        'corporatedoccatId' => $this->uri->segment(3),
                                        'sectionId' 	 => $section,
                                        'roleId' 		 => $role,
                                    );
                                    $this->db->insert('corporatedoccatlinks', $objectInsert);
                                }
                            }
                        }
                    }
                }

                if($sites)
                {
                    foreach ($sites as $site) {
                        $sql 	= "SELECT name FROM sites WHERE siteId = ?";
                        $exist 	= $this->db->query($sql, $site)->row();
                        if (isset($exist)) {
                            $sql 	= "SELECT corporatedoccatId FROM corporatedoccatlinks WHERE corporatedoccatId = ? && siteId = ?";
                            $exist 	= $this->db->query($sql, array($data->corporatedoccategoryId, $site))->row();
                            if (!isset($exist)) {
                                $objectInsert 		= array(
                                    'corporatedoccatId' 	=> $data->corporatedoccategoryId,
                                    'siteId' 		=> $site,
                                );
                                $this->db->insert('corporatedoccatlinks', $objectInsert);
                            }
                        }
                    }
                }

                if($userGroups && $this->Identity->Validate('usergroups/create'))
                {
                    foreach ($userGroups as $userGroup) {
                        $sql 	= "SELECT * FROM userGroups WHERE usergroupId = ? && userId = ?";
                        $exist 	= $this->db->query($sql, array($userGroup, $this->session->UserId))->row();
                        if (isset($exist)) {
                            $sql 	= "SELECT * FROM usergroupUsers WHERE usergroupId = ?";
                            $group 	= $this->db->query($sql, $userGroup)->result();
                            if (isset($group) && count($group) > 0) {
                                foreach ($group as $user) {
                                    $sql 	= "SELECT corporatedoccatId FROM corporatedoccatlinks WHERE corporatedoccatId = ? && userId = ?";
                                    $exist 	= $this->db->query($sql, array($data->corporatedoccategoryId, $user->userId))->row();
                                    if (!isset($exist)) {
                                        $objectInsert = array(
                                            'corporatedoccatId' 	=> $data->corporatedoccategoryId,
                                            'userId' 	=> $user->userId,
                                        );
                                        $this->db->insert('corporatedoccatlinks', $objectInsert);
                                    }
                                }
                            }
                        }
                    }
                }

                if ($users) {
                    foreach ($users as $user) {
                        $sql 	= "SELECT userId FROM users WHERE userId = ?";
                        $exist 	= $this->db->query($sql, $user)->row();
                        if (isset($exist)) {
                            $sql 	= "SELECT corporatedoccatId FROM corporatedoccatlinks WHERE corporatedoccatId = ? && userId = ?";
                            $exist 	= $this->db->query($sql, array($data->corporatedoccategoryId, $user))->row();
                            if (!isset($exist)) {
                                $objectInsert 		= array(
                                    'corporatedoccatId' 	=> $data->corporatedoccategoryId,
                                    'userId' 	=> $user,
                                );
                                $this->db->insert('corporatedoccatlinks', $objectInsert);
                            }
                        }
                    }
                }
                echo $this->lang->line('corporatedocs_categoryeditmessage');
            }
        }
    }

    /**
     * Borra en cascada cada categoria dentro y los documentos que tenga.
     * afecta tablas: corporatedocs, corporatedocslink, corporatedoccatlink, corporatedoccategories
     */
    public function DeleteCategory(){
        if ($this->uri->segment(3)) {
            $sql = "SELECT corporatedoccategoryId FROM corporatedoccategories WHERE corporatedoccategoryId = ?";
            $query = $this->db->query($sql,$this->uri->segment(3))->row();
            if (isset($query))
            {
                $objectDelete = array('corporatedoccategoryId' => $query->corporatedoccategoryId);
                $this->db->delete('corporatedoccategories', $objectDelete);
                echo $this->lang->line('corporatedocs_categorydeletemessage');;
            }
            else
            {
                echo "invalid";
            }
        }
        else{
            echo "invalid";
        }
    }
    /**
     * Esta funcion se encargaria de obtener los grupos,roles,secciones y sites para los que es
     * visible una categoria.
     *  o todos los que existen en cualquier otro caso.
     * ademas en caso de que se use para editar trae un atributo checked = 0 si pertenece tanto
     * a la categoria padre como a la que se esta editando. checked = null si solo pertenece al padre.
     */
    public function getlinksForCategory(){

        if($this->input->post('category') && $this->input->post('category') != null && $this->input->post('category') != -1 ) {
            $data = new stdClass();
            $category = $this->input->post('category');
            if($this->input->post('myId') && $this->input->post('myId') != null){
                $sql = "SELECT name,sections.sectionId,checked FROM sections 
                    INNER JOIN corporatedoccatlinks ON sections.sectionId = corporatedoccatlinks.sectionId 
                    LEFT JOIN (SELECT if(sectionId!=null,true,false) as checked,sectionId FROM corporatedoccatlinks WHERE corporatedoccatId = ?) as xd ON xd.sectionId = sections.sectionId
                    WHERE corporatedoccatId = ? 
                    GROUP BY sectionId
                    ORDER BY name";
                $query = $this->db->query($sql,array($this->input->post('myId'),$category))->result();
            }else{
                $sql = "SELECT name,sections.sectionId FROM sections 
                    INNER JOIN corporatedoccatlinks ON sections.sectionId = corporatedoccatlinks.sectionId 
                    WHERE corporatedoccatId = ? 
                    GROUP BY sectionId
                    ORDER BY name";
                $query = $this->db->query($sql,$category)->result();
            }
            if(!isset($query)|| (isset($query) && count($query) <= 0)){                 //*
                $sql = "SELECT name,sections.sectionId FROM sections ORDER BY name";    //*
                $query = $this->db->query($sql)->result();                              //*
            }
            $data->sections = $query;
            if($this->input->post('myId') && $this->input->post('myId') != null){
                $sql = "SELECT name,sites.siteId,checked FROM sites 
                    INNER JOIN corporatedoccatlinks ON sites.siteId = corporatedoccatlinks.siteId 
                    LEFT JOIN (SELECT if(siteId!=null,true,false) as checked,siteId FROM corporatedoccatlinks WHERE corporatedoccatId = ?) as xd ON xd.siteId = sites.siteId
                    WHERE corporatedoccatId = ? 
                    GROUP BY siteId
                    ORDER BY name";
                $query = $this->db->query($sql,array($this->input->post('myId'),$category))->result();
            }else{
                $sql = "SELECT name,sites.siteId FROM sites 
                    INNER JOIN corporatedoccatlinks ON sites.siteId = corporatedoccatlinks.siteId 
                    WHERE corporatedoccatId = ? 
                    GROUP BY siteId 
                    ORDER BY name";
                $query = $this->db->query($sql,$category)->result();
            }
            if(!isset($query)|| (isset($query) && count($query) <= 0)){                 //*
                $sql = "SELECT name,sites.siteId FROM sites ORDER BY name";             //*
                $query = $this->db->query($sql)->result();                              //*
            }
            $data->sites = $query;

            if($this->input->post('myId') && $this->input->post('myId') != null){
                $sql = "SELECT name,roles.roleId,checked FROM roles 
                    INNER JOIN corporatedoccatlinks ON roles.roleId = corporatedoccatlinks.roleId 
                    LEFT JOIN (SELECT if(roleId!=null,true,false) as checked,roleId FROM corporatedoccatlinks WHERE corporatedoccatId = ?) as xd ON xd.roleId = roles.roleId
                    WHERE corporatedoccatId = ? 
                    GROUP BY roleId
                    ORDER BY name";
                $query = $this->db->query($sql,array($this->input->post('myId'),$category))->result();
            }else{
                $sql = "SELECT name,roles.roleId FROM roles 
                    INNER JOIN corporatedoccatlinks ON roles.roleId = corporatedoccatlinks.roleId 
                    WHERE corporatedoccatId = ? 
                    GROUP BY roleId 
                    ORDER BY name";
                $query = $this->db->query($sql,$category)->result();
            }

            if(!isset($query)|| (isset($query) && count($query) <= 0)){                 //*
                $sql = "SELECT name,roles.roleId FROM roles ORDER BY name";             //*
                $query = $this->db->query($sql)->result();                              //*
            }
            $data->roles = $query;
            //esto no funciona
            $userid = $this->session->UserId;
            $sql = "SELECT usergroups.* FROM usergroups
                    INNER JOIN
                    (SELECT usergroupId, COUNT(usergroupusers.userId) catsum 
                    FROM usergroupusers 
                    INNER JOIN corporatedoccatlinks on usergroupusers.userId = corporatedoccatlinks.userId 
                    where corporatedoccatlinks.corporatedoccatId = ? 
                    GROUP BY usergroupusers.usergroupId) AS countcat ON countcat.usergroupId = usergroups.usergroupId
                    INNER JOIN
                    (SELECT usergroupId, COUNT(usergroupusers.userId) as totalsum 
                    FROM usergroupusers GROUP BY usergroupId) as countug ON countug.usergroupId = countcat.usergroupId AND countcat.catsum = countug.totalsum 
                    WHERE usergroups.userId = ? ORDER BY usergroups.name";
            //$query = $this->db->query($sql,$category,$userid)->result();
            //-----------------------------
            $sql = "SELECT userGroups.* FROM userGroups WHERE userId = ? ORDER BY name";             //*
            $query = $this->db->query($sql,$userid)->result();                                                    //*
            $data->userGroups = $query;
            //users
            $sql = "SELECT concat(up.name,' ', up.lastName) as name, u.userName,u.userId 
                    FROM users AS u 
                    INNER JOIN (SELECT name, lastName, userId FROM userPersonalData) AS up ON u.userId = up.userId 
                    INNER JOIN corporatedoccatlinks ON u.userId = corporatedoccatlinks.userId WHERE corporatedoccatId = ?";
            if($this->input->post('myId') && $this->input->post('myId') != null){
                $query = $this->db->query($sql,$this->input->post('myId'))->result();
            }else{
                $query = $this->db->query($sql,$category)->result();
            }

            $data->users = $query;
            if ($this->input->post('category') == -1){
                $data->error = "new";
            }else{
                $data->error = "old";
            }
        }else{
            $data = new stdClass();
            if($this->input->post('myId') && $this->input->post('myId') != null){
                $sql = "SELECT name,sections.sectionId, xd.checked FROM sections 
                    LEFT JOIN (SELECT if(sectionId!=null,true,false) as checked,sectionId FROM corporatedoccatlinks WHERE corporatedoccatId = ?) as xd ON xd.sectionId = sections.sectionId
                    group by name 
                    ORDER BY name";
                $data->sections = $this->db->query($sql,$this->input->post('myId'))->result();
                $sql = "SELECT name,sites.siteId, xd.checked FROM sites 
                    LEFT JOIN (SELECT if(siteId!=null,true,false) as checked,siteId FROM corporatedoccatlinks WHERE corporatedoccatId = ?) as xd ON xd.siteId = sites.siteId
                    group by name 
                    ORDER BY name";
                $data->sites = $this->db->query($sql,$this->input->post('myId'))->result();
                $sql = "SELECT name,roles.roleId, xd.checked FROM roles 
                    LEFT JOIN (SELECT if(roleId!=null,true,false) as checked,roleId FROM corporatedoccatlinks WHERE corporatedoccatId = ?) as xd ON xd.roleId = roles.roleId
                    group by name 
                    ORDER BY name";
                $data->roles = $this->db->query($sql,$this->input->post('myId'))->result();
                $sql = "SELECT concat(up.name,' ', up.lastName) as name, u.userName,u.userId 
                    FROM users AS u 
                    INNER JOIN (SELECT name, lastName, userId FROM userPersonalData) AS up ON u.userId = up.userId 
                    INNER JOIN corporatedoccatlinks ON u.userId = corporatedoccatlinks.userId WHERE corporatedoccatId = ? 
                    group by u.userName";
                $query = $this->db->query($sql,$this->input->post('myId'))->result();
            }else{
                $sql = "SELECT name,sectionId FROM sections ORDER BY name";
                $data->sections = $this->db->query($sql)->result();
                $sql = "SELECT name,siteId FROM sites ORDER BY name";
                $data->sites = $this->db->query($sql)->result();
                $sql = "SELECT name,roleId FROM roles ORDER BY name";
                $data->roles = $this->db->query($sql)->result();
            }
            $sql = "SELECT * FROM userGroups WHERE userId = ? ORDER BY name";
            $data->userGroups = $this->db->query($sql, $this->session->UserId)->result();
            $data->error = "new";
        }
        $sql = "SELECT * FROM corporatedoccategories ORDER BY name";
        $data->categories = $this->db->query($sql)->result();
        echo escapeJsonString($data,false);
    }
    public function getLinksForDocument(){
        if($this->input->post('category') && $this->input->post('category') != null && $this->input->post('myId') && $this->input->post('myId') != null) {
            $data = new stdClass();
            $category = $this->input->post('category');
            $sql = "SELECT name,sections.sectionId,checked FROM sections 
                INNER JOIN corporatedoccatlinks ON sections.sectionId = corporatedoccatlinks.sectionId 
                LEFT JOIN (SELECT if(sectionId!=null,true,false) as checked,sectionId FROM corporatedoclinks WHERE corporatedocId = ?) as xd ON xd.sectionId = sections.sectionId
                WHERE corporatedoccatId = ? 
                GROUP BY sectionId
                ORDER BY name";
            $query = $this->db->query($sql,array($this->input->post('myId'),$category))->result();
            if(!isset($query)|| (isset($query) && count($query) <= 0)){                 //*
                $sql = "SELECT name,sections.sectionId,xd.checked FROM sections 
                        LEFT JOIN (SELECT if(sectionId!=null,true,false) as checked,sectionId FROM corporatedoclinks WHERE corporatedocId = 35) as xd ON xd.sectionId = sections.sectionId 
                        ORDER BY name";    //*
                $query = $this->db->query($sql, $this->input->post('myId'))->result();                              //*
            }
            $data->sections = $query;
            $sql = "SELECT name,sites.siteId,checked FROM sites 
                INNER JOIN corporatedoccatlinks ON sites.siteId = corporatedoccatlinks.siteId 
                LEFT JOIN (SELECT if(siteId!=null,true,false) as checked,siteId FROM corporatedoclinks WHERE corporatedocId = ?) as xd ON xd.siteId = sites.siteId
                WHERE corporatedoccatId = ? 
                GROUP BY siteId
                ORDER BY name";
            $query = $this->db->query($sql,array($this->input->post('myId'),$category))->result();
            if(!isset($query)|| (isset($query) && count($query) <= 0)){                 //*
                $sql = "SELECT name,sites.siteId,xd.checked FROM sites 
                        LEFT JOIN (SELECT if(siteId!=null,true,false) as checked,siteId FROM corporatedoclinks WHERE corporatedocId = ?) as xd ON xd.siteId = sites.siteId
                ORDER BY name";             //*
                $query = $this->db->query($sql,$this->input->post('myId'))->result();                              //*
            }
            $data->sites = $query;
            $sql = "SELECT name,roles.roleId,xd.checked FROM roles 
                INNER JOIN corporatedoccatlinks ON roles.roleId = corporatedoccatlinks.roleId 
                LEFT JOIN (SELECT if(roleId!=null,true,false) as checked,roleId FROM corporatedoclinks WHERE corporatedocId = ?) as xd ON xd.roleId = roles.roleId
                WHERE corporatedoccatId = ? 
                GROUP BY roleId
                ORDER BY name";
            $query = $this->db->query($sql,array($this->input->post('myId'),$category))->result();
            if(!isset($query)|| (isset($query) && count($query) <= 0)){                 //*
                $sql = "SELECT name,roles.roleId,xd.checked FROM roles 
                        LEFT JOIN (SELECT if(roleId!=null,true,false) as checked,roleId FROM corporatedoclinks WHERE corporatedocId = ?) as xd ON xd.roleId = roles.roleId
                        ORDER BY name";             //*
                $query = $this->db->query($sql,$this->input->post('myId'))->result();                              //*
            }
            $data->roles = $query;
            //esto no funciona
            $userid = $this->session->UserId;
            $sql = "SELECT usergroups.* FROM usergroups
                    INNER JOIN
                    (SELECT usergroupId, COUNT(usergroupusers.userId) catsum 
                    FROM usergroupusers 
                    INNER JOIN corporatedoccatlinks on usergroupusers.userId = corporatedoccatlinks.userId 
                    where corporatedoccatlinks.corporatedoccatId = ? 
                    GROUP BY usergroupusers.usergroupId) AS countcat ON countcat.usergroupId = usergroups.usergroupId
                    INNER JOIN
                    (SELECT usergroupId, COUNT(usergroupusers.userId) as totalsum 
                    FROM usergroupusers GROUP BY usergroupId) as countug ON countug.usergroupId = countcat.usergroupId AND countcat.catsum = countug.totalsum 
                    WHERE usergroups.userId = ? ORDER BY usergroups.name";
            //$query = $this->db->query($sql,$category,$userid)->result();
            //-----------------------------
            $sql = "SELECT userGroups.* FROM userGroups WHERE userId = ? ORDER BY name";             //*
            $query = $this->db->query($sql,$userid)->result();                                                    //*
            $data->userGroups = $query;
            //users
            $sql = "SELECT concat(up.name,' ', up.lastName) as name, u.userName,u.userId 
                    FROM users AS u 
                    INNER JOIN (SELECT name, lastName, userId FROM userPersonalData) AS up ON u.userId = up.userId 
                    INNER JOIN corporatedoccatlinks ON u.userId = corporatedoccatlinks.userId WHERE corporatedoccatId = ?";
            $query = $this->db->query($sql,$this->input->post('myId'))->result();
            $data->users = $query;
            if ($this->input->post('category') == -1){
                $data->error = "new";
            }else{
                $data->error = "old";
            }
        }else{
            $data = new stdClass();
            $sql = "SELECT name,sectionId FROM sections ORDER BY name";
            $data->sections = $this->db->query($sql)->result();
            $sql = "SELECT name,siteId FROM sites ORDER BY name";
            $data->sites = $this->db->query($sql)->result();
            $sql = "SELECT name,roleId FROM roles ORDER BY name";
            $data->roles = $this->db->query($sql)->result();
            $sql = "SELECT * FROM userGroups WHERE userId = ? ORDER BY name";
            $data->userGroups = $this->db->query($sql, $this->session->UserId)->result();
            $data->error = "new";
        }

        $sql = "SELECT * FROM corporatedoccategories ORDER BY name";
        $data->categories = $this->db->query($sql)->result();
        echo escapeJsonString($data,false);
    }

	//Metodos ABM DOCS
	public function Create(){
		if($this->input->post('category') && $this->input->post('category') != null)
		{
			$Sql = "SELECT * FROM corporatedoccategories";
			$Query = $this->db->query($Sql)->result();
			$Ids = '';
			foreach ($Query as $value) {
				$Ids .= $value->corporatedoccategoryId.','; 
			}
            $this->form_validation->set_rules('category', 'lang:corporatedocs_category','required|in_list['.$Ids.']',
                array('in_list' => $this->lang->line('corporatedocs_categoryexist')));
        }

		$this->form_validation->set_rules('name', 'lang:corporatedocs_name', 'required');
		$this->form_validation->set_rules('description', 'lang:corporatedocs_description', 'required');

		if (!$this->input->post('roles[]') && !$this->input->post('sections[]') && !$this->input->post('sites[]') && !$this->input->post('userGroups[]') && !$this->input->post('users[]')) {
			$this->form_validation->set_rules('sections[]', 'lang:corporatedocs_link', 'required');
		}

		if ($this->form_validation->run() == FALSE)
		{	
			$data = new stdClass();
			$sql = "SELECT name,sectionId FROM sections ORDER BY name";
			$data->sections = $this->db->query($sql)->result();
			$sql = "SELECT name,siteId FROM sites ORDER BY name";
			$data->sites = $this->db->query($sql)->result();
			$sql = "SELECT name,roleId FROM roles ORDER BY name";
			$data->roles = $this->db->query($sql)->result();
			$sql = "SELECT * FROM userGroups WHERE userId = ? ORDER BY name";
			$data->userGroups = $this->db->query($sql, $this->session->UserId)->result();
			$data->error = '';
			$this->load->view('corporatedocs/create', $data);
		}
		else
		{
			$timestamp = time();

			$config['upload_path']          = './catapp/_corporate_docs/';
			$config['allowed_types']        = 'gif|jpg|png|pdf|doc|docx|pptx|ppt|xlsx|xls|txt|html';
			$config['max_size']             = 50480;
			if (isset($_FILES['userfile']) && $_FILES['userfile'] != NULL) {
				$config['file_name']			= $timestamp.'-'. $_FILES['userfile']['name'];
			}
			$this->load->library('upload', $config);

			if ( !$this->upload->do_upload('userfile'))
			{
				$response = new StdClass();
				$response->status = 'fail';
				$response->message = $this->lang->line('corporatedocs_completeall');
				echo escapeJsonString($response,false);
				return;
			}
			else
			{
				$objectInsert = array(
					'name' => $this->input->post('name'),
					'description' => $this->input->post('description'),
					'userId' => $this->session->UserId,
					'timestamp' => $timestamp,
					'file' => $this->upload->data('file_name'),
					'required' => ($this->input->post('required')) ? 1 : 0
					);

				if( $this->input->post('category'))
					$objectInsert['corporatedoccategoryId'] = $this->input->post('category');

				$this->db->insert('corporatedocs', $objectInsert);
			}
			$query=new stdClass();
			$query->corporatedocId=$this->db->insert_id();

			if (isset($query) && $query != NULL && $query != '')
			{
				$sections 	= $this->input->post('sections[]');
				$roles 		= $this->input->post('roles[]');
				$sites 		= $this->input->post('sites[]');
				$userGroups = $this->input->post('userGroups[]');
				$users 		= $this->input->post('users[]');

				if(count($roles) == 0)
				{
					if($sections)
					{
						foreach ($sections as $section) {
							$sql 	= "SELECT name FROM sections WHERE sectionId = ?";
							$exist 	= $this->db->query($sql, $section)->row();
							if (isset($exist)) {
								$sql 	= "SELECT corporatedocId FROM corporatedoclinks WHERE corporatedocId = ? && sectionId = ?";
								$exist 	= $this->db->query($sql, array($query->corporatedocId, $section))->row();
								if (!isset($exist)) {
									$objectInsert = array(
										'corporatedocId' 	=> $query->corporatedocId,
										'sectionId' 	=> $section,
										);
									$this->db->insert('corporatedoclinks', $objectInsert);
								}
							}
						}
					}

				}
				else if(count($sections) == 0)
				{
					if($roles)
					{
						foreach ($roles as $role) {
							$sql 	= "SELECT name FROM roles WHERE roleId = ?";
							$exist 	= $this->db->query($sql, $role)->row();
							if (isset($exist)) {
								$sql 	= "SELECT corporatedocId FROM corporatedoclinks WHERE corporatedocId = ? && roleId = ?";
								$exist 	= $this->db->query($sql, array($query->corporatedocId, $role))->row();
								if (!isset($exist)) {
									$objectInsert 		= array(
										'corporatedocId' 	=> $query->corporatedocId,
										'roleId' 			=> $role,
										);
									$this->db->insert('corporatedoclinks', $objectInsert);
								}
							}
						}
					}

				}
				else
				{
					foreach($sections as $section)
					{
						foreach($roles as $role)
						{
							$sqlSection 	= "SELECT name FROM sections WHERE sectionId = ?";
							$existSection 	= $this->db->query($sqlSection, $section)->row();

							$sqlRole 	= "SELECT name FROM roles WHERE roleId = ?";
							$existRole 	= $this->db->query($sqlRole, $role)->row();

							if (isset($existSection) && isset($existRole)) 
							{
								$sql 	= "SELECT corporatedocId FROM corporatedoclinks WHERE corporatedocId = ? && sectionId = ? && roleId = ?";
								$exist 	= $this->db->query($sql, array($query->corporatedocId, $section, $role))->row();

								if (!isset($exist))
								{
									$objectInsert = array(
										'corporatedocId' => $query->corporatedocId,
										'sectionId' 	 => $section,
										'roleId' 		 => $role,
										);
									$this->db->insert('corporatedoclinks', $objectInsert);
								}
							}
						}
					}
				}


				if($sites)
				{
					foreach ($sites as $site) {
						$sql 	= "SELECT name FROM sites WHERE siteId = ?";
						$exist 	= $this->db->query($sql, $site)->row();
						if (isset($exist)) {
							$sql 	= "SELECT corporatedocId FROM corporatedoclinks WHERE corporatedocId = ? && siteId = ?";
							$exist 	= $this->db->query($sql, array($query->corporatedocId, $site))->row();
							if (!isset($exist)) {
								$objectInsert 		= array(
									'corporatedocId' 	=> $query->corporatedocId,
									'siteId' 			=> $site,
									);
								$this->db->insert('corporatedoclinks', $objectInsert);
							}
						}
					}
				}

				if($userGroups && $this->Identity->Validate('usergroups/create'))
				{
					foreach ($userGroups as $userGroup) {
						$sql 	= "SELECT * FROM userGroups WHERE usergroupId = ? && userId = ?";
						$exist 	= $this->db->query($sql, array($userGroup, $this->session->UserId))->row();
						if (isset($exist)) {
							$sql 	= "SELECT * FROM usergroupUsers WHERE usergroupId = ?";
							$group 	= $this->db->query($sql, $userGroup)->result();
							if (isset($group) && count($group) > 0) {
								foreach ($group as $user) {
									$sql 	= "SELECT corporatedocId FROM corporatedoclinks WHERE corporatedocId = ? && userId = ?";
									$exist 	= $this->db->query($sql, array($query->corporatedocId, $user->userId))->row();
									if (!isset($exist)) {
										$objectInsert = array(
											'corporatedocId'	=> $query->corporatedocId,
											'userId' 			=> $user->userId,
											);
										$this->db->insert('corporatedoclinks', $objectInsert);
									}
								}
							}
						}
					}
				}

				if ($users) {
					foreach ($users as $user) {
						$sql 	= "SELECT userId FROM users WHERE userId = ?";
						$exist 	= $this->db->query($sql, $user)->row();
						if (isset($exist)) {
							$sql 	= "SELECT corporatedocId FROM corporatedoclinks WHERE corporatedocId = ? && userId = ?";
							$exist 	= $this->db->query($sql, array($query->corporatedocId, $user))->row();
							if (!isset($exist)) {
								$objectInsert 		= array(
									'corporatedocId' 	=> $query->corporatedocId,
									'userId' 			=> $user,
									);
								$this->db->insert('corporatedoclinks', $objectInsert);
							}
						}
					}
				}

			}

			$response = new StdClass();
			$response->status = 'ok';
			$response->message = $this->lang->line('corporatedocs_createmessage');

			echo escapeJsonString($response, FALSE);
		}
	}

	public function Delete(){
		if ($this->uri->segment(3)) {
			$sql = "SELECT * FROM corporatedocs WHERE corporatedocId = ?";
			$query = $this->db->query($sql,$this->uri->segment(3))->row();
			if (isset($query))
			{

                insert_audit_logs('corporatedocs','DELETE',$query);

                $objectDelete = array(
                    'corporatedocId' => $query->corporatedocId
                );

                $this->db->delete('corporatedocs', $objectDelete);
                if ($query->file != NULL) {
                    unlink('./catapp/_corporate_docs/'.$query->file);
                }
				echo $this->lang->line('corporatedocs_deletemessage');;
			}
			else
			{
				echo "invalid";
			}
		}
		else{
			echo "invalid";
		}
	}


	public function SearchUsers(){
		$likeuser = $this->db->escape_like_str($this->input->post('like'));
		if ($likeuser != '') {
            $Sql = "SELECT concat_ws(' ', up.name, up.lastName) as completeName, u.userId, u.userName FROM users AS u INNER JOIN (SELECT name, lastName, userId FROM userPersonalData) AS up ON u.userId = up.userId WHERE concat_ws(' ', up.name, up.lastName) LIKE '%$likeuser%' || userName LIKE '%$likeuser%' ORDER BY completeName LIMIT 20";
		    $query = $this->db->query($Sql)->result();
			if (count($query) > 0) {
				echo escapeJsonString(json_encode($query));
			}
			else
			{
				echo "empty";
			}
		}
		else
		{
			echo "empty";
		}
	}


	function Edit()
	{

		$data = new StdClass();

		$Sql = "SELECT * FROM corporatedoccategories";
		$Query = $this->db->query($Sql)->result();
		$Ids = '';
		foreach ($Query as $value) {
			$Ids .= $value->corporatedoccategoryId.','; 
		}
		$this->form_validation->set_rules('category', 'lang:corporatedocs_category','required|in_list['.$Ids.']',
			array('in_list' => $this->lang->line('corporatedocs_categoryexist')));	

		$this->form_validation->set_rules('name', 'lang:corporatedocs_name', 'required');
		$this->form_validation->set_rules('description', 'lang:corporatedocs_description', 'required');

		if (!$this->input->post('roles[]') && !$this->input->post('sections[]') && !$this->input->post('sites[]') && !$this->input->post('userGroups[]') && !$this->input->post('users[]')) {
			$this->form_validation->set_rules('sections[]', 'lang:corporatedocs_link', 'required');
		}

		if ($this->form_validation->run() == FALSE  && !$this->uri->segment(3))
		{
			$sql = "SELECT name,sectionId FROM sections ORDER BY name";
			$data->sections = $this->db->query($sql)->result();
			$sql = "SELECT * FROM corporatedoccategories ORDER BY name";
			$data->categories = $this->db->query($sql)->result();
			$sql = "SELECT name,siteId FROM sites ORDER BY name";
			$data->sites = $this->db->query($sql)->result();
			$sql = "SELECT name,roleId FROM roles ORDER BY name";
			$data->roles = $this->db->query($sql)->result();
			$sql = "SELECT * FROM userGroups WHERE userId = ? ORDER BY name";
			$data->userGroups = $this->db->query($sql, $this->session->UserId)->result();
			$this->load->view('corporatedocs/edit', $data);
		}
		else
		{
			$sql = "SELECT * FROM corporatedocs WHERE corporatedocId = ?";
			$data = $this->db->query($sql,$this->uri->segment(3))->row();
			if (isset($data))
			{
				$timestamp = time();

				$objectEdit = array(
					'description' 				=> $this->input->post('description'),
					'corporatedoccategoryId' 	=> $this->input->post('category') && $this->input->post('category') != -1 ? $this->input->post('category') : null,
					'name' 						=> $this->input->post('name'),
					'required'					=> ($this->input->post('required')) ? 1 : 0
					);

				$this->db->where('corporatedocId', $data->corporatedocId);
				$this->db->update('corporatedocs', $objectEdit);


				$sections 	= $this->input->post('sections[]');
				$roles 		= $this->input->post('roles[]');
				$sites 		= $this->input->post('sites[]');
				$userGroups = $this->input->post('userGroups[]');
				$users 		= $this->input->post('users[]');

				$objectDelete = array(
					'corporatedocId' => $data->corporatedocId
					);
				$this->db->delete('corporatedoclinks', $objectDelete);

				if(count($roles) == 0)
				{
					if($sections)
					{
						foreach ($sections as $section) {
							$sql 	= "SELECT name FROM sections WHERE sectionId = ?";
							$exist 	= $this->db->query($sql, $section)->row();
							if (isset($exist)) {
								$sql 	= "SELECT corporatedocId FROM corporatedoclinks WHERE corporatedocId = ? && sectionId = ?";
								$exist 	= $this->db->query($sql, array($this->uri->segment(3), $section))->row();
								if (!isset($exist)) {
									$objectInsert = array(
										'corporatedocId' 	=> $this->uri->segment(3),
										'sectionId' 	=> $section,
										);
									$this->db->insert('corporatedoclinks', $objectInsert);
								}
							}
						}
					}

				}
				else if(count($sections) == 0)
				{
					if($roles)
					{
						foreach ($roles as $role) {
							$sql 	= "SELECT name FROM roles WHERE roleId = ?";
							$exist 	= $this->db->query($sql, $role)->row();
							if (isset($exist)) {
								$sql 	= "SELECT corporatedocId FROM corporatedoclinks WHERE corporatedocId = ? && roleId = ?";
								$exist 	= $this->db->query($sql, array($this->uri->segment(3), $role))->row();
								if (!isset($exist)) {
									$objectInsert 		= array(
										'corporatedocId' 	=> $this->uri->segment(3),
										'roleId' 			=> $role,
										);
									$this->db->insert('corporatedoclinks', $objectInsert);
								}
							}
						}
					}

				}
				else
				{
					foreach($sections as $section)
					{
						foreach($roles as $role)
						{
							$sqlSection 	= "SELECT name FROM sections WHERE sectionId = ?";
							$existSection 	= $this->db->query($sqlSection, $section)->row();

							$sqlRole 	= "SELECT name FROM roles WHERE roleId = ?";
							$existRole 	= $this->db->query($sqlRole, $role)->row();

							if (isset($existSection) && isset($existRole)) 
							{
								$sql 	= "SELECT corporatedocId FROM corporatedoclinks WHERE corporatedocId = ? && sectionId = ? && roleId = ?";
								$exist 	= $this->db->query($sql, array($this->uri->segment(3), $section, $role))->row();

								if (!isset($exist))
								{
									$objectInsert = array(
										'corporatedocId' => $this->uri->segment(3),
										'sectionId' 	 => $section,
										'roleId' 		 => $role,
										);
									$this->db->insert('corporatedoclinks', $objectInsert);
								}
							}
						}
					}
				}

				if($sites)
				{
					foreach ($sites as $site) {
						$sql 	= "SELECT name FROM sites WHERE siteId = ?";
						$exist 	= $this->db->query($sql, $site)->row();
						if (isset($exist)) {
							$sql 	= "SELECT corporatedocId FROM corporatedoclinks WHERE corporatedocId = ? && siteId = ?";
							$exist 	= $this->db->query($sql, array($data->corporatedocId, $site))->row();
							if (!isset($exist)) {
								$objectInsert 		= array(
									'corporatedocId' 	=> $data->corporatedocId,
									'siteId' 		=> $site,
									);
								$this->db->insert('corporatedoclinks', $objectInsert);
							}
						}
					}
				}

				if($userGroups && $this->Identity->Validate('usergroups/create'))
				{
					foreach ($userGroups as $userGroup) {
						$sql 	= "SELECT * FROM userGroups WHERE usergroupId = ? && userId = ?";
						$exist 	= $this->db->query($sql, array($userGroup, $this->session->UserId))->row();
						if (isset($exist)) {
							$sql 	= "SELECT * FROM usergroupUsers WHERE usergroupId = ?";
							$group 	= $this->db->query($sql, $userGroup)->result();
							if (isset($group) && count($group) > 0) {
								foreach ($group as $user) {
									$sql 	= "SELECT corporatedocId FROM corporatedoclinks WHERE corporatedocId = ? && userId = ?";
									$exist 	= $this->db->query($sql, array($data->corporatedocId, $user->userId))->row();
									if (!isset($exist)) {
										$objectInsert = array(
											'corporatedocId' 	=> $data->corporatedocId,
											'userId' 	=> $user->userId,
											);
										$this->db->insert('corporatedoclinks', $objectInsert);
									}
								}
							}
						}
					}
				}

				if ($users) {
					foreach ($users as $user) {
						$sql 	= "SELECT userId FROM users WHERE userId = ?";
						$exist 	= $this->db->query($sql, $user)->row();
						if (isset($exist)) {
							$sql 	= "SELECT corporatedocId FROM corporatedoclinks WHERE corporatedocId = ? && userId = ?";
							$exist 	= $this->db->query($sql, array($data->corporatedocId, $user))->row();
							if (!isset($exist)) {
								$objectInsert 		= array(
									'corporatedocId' 	=> $data->corporatedocId,
									'userId' 	=> $user,
									);
								$this->db->insert('corporatedoclinks', $objectInsert);
							}
						}
					}
				}

				echo $this->lang->line('corporatedocs_editmessage');
			}
			else{
				echo "invalid";
			}
		}
	}

    function GetCorporateDocsById(){
        if ($this->uri->segment(3)) {
            $sql = "SELECT * FROM corporatedocs WHERE corporatedocId = ?";
            $data = $this->db->query($sql,$this->uri->segment(3))->row();
            if (isset($data))
            {
                $sql = "SELECT * FROM corporatedoclinks WHERE corporatedocId = ?";
                $data->links = $this->db->query($sql, $data->corporatedocId)->result();
                $response=array();

                foreach ($data->links as $key=>$link)
                {

                    if ($link->siteId != NULL)
                    {
                        $sql = "SELECT name FROM sites WHERE siteId = ?";
                        $link->name = $this->db->query($sql, $link->siteId)->row()->name;

                    }
                    elseif ($link->userId != NULL) {
                        $sql = "SELECT up.name, up.lastName, u.userName FROM users AS u INNER JOIN (SELECT name, lastName, userId FROM userPersonalData WHERE userId = ?) AS up ON u.userId = up.userId WHERE u.userId = ?";
                        $tempVar = $this->db->query($sql, array($link->userId, $link->userId))->row();
                        $tempVar->name .= ' '. $tempVar->lastName;
                        $link->userName = $tempVar->userName;
                    }
                    else if ($link->sectionId != NULL && $link->roleId != NULL)
                    {
                        $sql = "SELECT name FROM sections WHERE sectionId = ?";

                        $section=clone $link;
                        $section->roleId=null;
                        $section->name=$this->db->query($sql, $link->sectionId)->row()->name;
                        array_push($response, $section);

                        $sql = "SELECT name FROM roles WHERE roleId = ?";

                        $role=clone $link;
                        $role->sectionId=null;
                        $role->name = $this->db->query($sql, $link->roleId)->row()->name;
                        array_push($response, $role);

                        unset($data->links[$key]);
                    }
                    else if ($link->sectionId != NULL)
                    {
                        $sql = "SELECT name FROM sections WHERE sectionId = ?";
                        $link->name = $this->db->query($sql, $link->sectionId)->row()->name;
                    }
                    else if ($link->roleId != NULL)
                    {
                        $sql = "SELECT name FROM roles WHERE roleId = ?";
                        $link->name = $this->db->query($sql, $link->roleId)->row()->name;
                    }
                    else
                    {
                        $link->name= '';
                    }
                }

                if(count($response) > 0)
                {
                    $data->links=array_merge($data->links,$response);
                }

                echo escapeJsonString(json_encode($data));
            }
            else
            {
                echo "invalid";
            }
        }
        else{
            echo "invalid";
        }
    }
    function GetCategoryById(){
        if ($this->uri->segment(3)) {
            $sql = "SELECT * FROM corporatedoccategories WHERE corporatedoccategoryId = ?";
            $data = $this->db->query($sql,$this->uri->segment(3))->row();
            if (isset($data))
            {
                $sql = "SELECT * FROM corporatedoccatlinks WHERE corporatedoccatId = ?";
                $data->links = $this->db->query($sql, $data->corporatedoccategoryId)->result();
                $response=array();

                foreach ($data->links as $key=>$link)
                {

                    if ($link->siteId != NULL)
                    {
                        $sql = "SELECT name FROM sites WHERE siteId = ?";
                        $link->name = $this->db->query($sql, $link->siteId)->row()->name;

                    }
                    else if ($link->userId != NULL) {
                        $sql = "SELECT up.name, up.lastName, u.userName FROM users AS u INNER JOIN (SELECT name, lastName, userId FROM userPersonalData WHERE userId = ?) AS up ON u.userId = up.userId WHERE u.userId = ?";
                        $tempVar = $this->db->query($sql, array($link->userId, $link->userId))->row();
                        $tempVar->name .= ' '. $tempVar->lastName;
                        $link->userName = $tempVar->userName;
                        $link->name = $tempVar->name;
                    }
                    else if ($link->sectionId != NULL && $link->roleId != NULL)
                    {
                        $sql = "SELECT name FROM sections WHERE sectionId = ?";

                        $section=clone $link;
                        $section->roleId=null;
                        $section->name=$this->db->query($sql, $link->sectionId)->row()->name;
                        array_push($response, $section);

                        $sql = "SELECT name FROM roles WHERE roleId = ?";

                        $role=clone $link;
                        $role->sectionId=null;
                        $role->name = $this->db->query($sql, $link->roleId)->row()->name;
                        array_push($response, $role);

                        unset($data->links[$key]);
                    }
                    else if ($link->sectionId != NULL)
                    {
                        $sql = "SELECT name FROM sections WHERE sectionId = ?";
                        $link->name = $this->db->query($sql, $link->sectionId)->row()->name;
                    }
                    else if ($link->roleId != NULL)
                    {
                        $sql = "SELECT name FROM roles WHERE roleId = ?";
                        $link->name = $this->db->query($sql, $link->roleId)->row()->name;
                    }
                    else
                    {
                        $link->name= '';
                    }
                }

                if(count($response) > 0)
                {
                    $data->links=array_merge($data->links,$response);
                }

                echo escapeJsonString(json_encode($data));
            }
            else
            {
                echo "invalid";
            }
        }
        else{
            echo "invalid";
        }
    }

    public function ReadDoc()
	{
		$sql=
		"SELECT corporatedocs.corporatedocId,corporatedocs.name,corporatedocs.description
		FROM corporatedoclinks links INNER JOIN corporatedocs ON links.corporatedocId = corporatedocs.corporatedocId
		WHERE corporatedocs.required = 1 && (links.userId = ? || links.siteId = ? || (case when links.sectionId is null then links.roleId = ? when links.roleId is null then links.sectionId = ? else links.sectionId = ? && links.roleId = ? end))
		&& (SELECT count(*) FROM corporatedocviews views WHERE views.corporatedocId = corporatedocs.corporatedocId && views.userId = ?) = 0
		GROUP BY links.corporatedocId ORDER BY corporatedocs.corporatedocId ASC";

		$doc = $this->db->query($sql,
			array($this->session->UserId,
				$this->session->siteId,
				$this->session->Role,
				$this->session->sectionId,
				$this->session->sectionId,
				$this->session->Role,
				$this->session->UserId))->row();

		if (isset($doc))
		{
			$this->form_validation->set_rules('corporatedoc', 'corporatedoc', 'required');

			if ($this->form_validation->run() === FALSE)
			{
				$data = array('title' => $this->lang->line('corporatedocs_corporatedoc'),'icon'=>'fa-file-text-o');

				$this->load->view("_shared/_header",$data);
				$this->load->view("corporatedocs/read_doc",$doc);
			}
			else
			{
				if ($this->input->post('corporatedoc') == $doc->corporatedocId)
				{
					echo (($this->CheckView($this->input->post('corporatedoc')) > 0)? 'ok' : 'error');
				}
				else
				{
					show_404();
				}
			}

		}
		else
		{
			header('Location:/'.FOLDERADD);
		}

	}

}
