
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Background_model extends CI_Model {
    public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
    }
    
    function Get()
    {
        $this->db->select('userBackgroundId,name');
        $backs = $this->db->get('userBackground')->result();
        return $backs;
    }

    function Create($objectInsert)
    {
        $data = new StdClass();
        $this->db->insert('userBackground', $objectInsert);
        $data->status = "success";
        return json_encode($data);
    }

    function Delete($id)
    {
        $image = $this->db->select('name')->get_where('userBackground',array('userBackgroundId' => $id))->row();
        if ($image->name != NULL) {
            unlink('./catapp/_users_backgrounds/'.$image->name); 
        }
        $res = new StdClass();       
        
        $this->SetNullImage($image->name);

        $this->db->where('userBackgroundId', $id);
        $this->db->delete('userBackground');

        $res->status = "success";
		return json_encode($res);
    }

    function SetNullImage($image)
    {
        $this->db->set('background',NULL);
		$this->db->where('background', $image);
        $this->db->update('userComplementaryData');
    }
}

/* End of file Background_model.php */

?>