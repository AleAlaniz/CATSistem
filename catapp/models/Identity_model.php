<?php
Class Identity_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->library('Login_tries');
		$this->load->model('User_model', 'User');
	}

	function Validate($permissionName, $parameter = NULL)
	{
		if ($this->session->has_userdata('Loged') && $this->session->has_userdata('UserId'))
		{
			$validateRole = FALSE;

			$validateRole = in_array($permissionName,$this->session->permissions);

			return $validateRole;
		}
		else
		{
			return FALSE;
		}
	}

	function Autenticate()
	{	
		if ($this->input->post('UserName') && $this->input->post('Password')) {

			$sql 		= "SELECT u.userId, u.userName, u.password, u.roleId, u.sectionId, u.siteId FROM users AS u WHERE u.userName = ? && u.active = 1 LIMIT 1";
			$query_user = $this->db->query($sql, $this->input->post('UserName'))->row();

			if (isset($query_user)) {

				//como primer paso, luego de obtener solamente el nbre usuario, validara los intentos por IP, los busca en la tabla
				
				$ip_address = $this->input->ip_address();

				$sql 	  = "SELECT * FROM login_tries lt WHERE lt.IP = ? LIMIT 1";
				$query_IP = $this->db->query($sql, $ip_address)->row();

				//si no lo encuentra, crea un objeto nuevo
				$ip_login_row = (isset($query_IP))?  $query_IP : (object) array(
					'IP' => $ip_address,
					'nAttempts' => 0,
					'timestamp' => date('y-m-d h:i:s', strtotime("+10 minutes")),
				);

				$ip_new = (isset($query_IP))?  false : true;


				$is_blocked = false;
				

				//el usuario esta bloqueado? Es decir, paso todos los intentos y respeto el cooldown?
				if ($this->login_tries->is_blocked($ip_login_row)) {
					
					$is_blocked = true;

					if ($this->login_tries->cooldown_over($ip_login_row)){

						$ip_login_row 	= $this->login_tries->clean_tries($ip_login_row);

						$is_blocked 	= false;
					}
				}

				if (!$is_blocked){
					//una vez se ha comprobado que no esta bloqueado, se va a la prueba de la contraseña

					//en caso de que el test haya pasado, todo funciona como deberia funcionar actualmente
					if (hash_equals($query_user->password,crypt($this->input->post('Password'), $query_user->password))){

						//primero borra la entrada de la IP, para prevenir que no funke despues
						$this->db->where('IP', $ip_address);
						$this->db->delete('login_tries');

						$sql = "SELECT name FROM rolePermissions rp
						JOIN permissons p
						ON (rp.permissionId = p.permisonId)
						WHERE p.active = 'TRUE'
						AND rp.value = 'TRUE'
						AND rp.roleId = ?";

						$data = $this->db->query($sql,$query_user->roleId)->result();
						$permissions = array();
						foreach ($data as $permis) {
							$permissions [] = $permis->name;
						}

						$sessiondata = array(
							'Loged'  		=> TRUE,
							'UserId' 		=> $query_user->userId,
							'roleId' 		=> $query_user->roleId,
							'sectionId' 	=> $query_user->sectionId,
							'siteId' 		=> $query_user->siteId,
							'permissions' 	=> $permissions
						);

						$this->session->set_userdata($sessiondata);

						$access_log = array(
							'userId' 	=> $query_user->userId,
							'timestamp' => time()
						);
						$this->db->insert('access_logs', $access_log);

						return 'success';

					}
					//fallo! 
					else{

						$ip_login_row = $this->login_tries->add_try($ip_login_row);

						if($ip_new){
							$this->db->insert('login_tries', $ip_login_row);
						}
						else{
							
							$this->db->where('IP', $ip_address);
							$this->db->update('login_tries', $ip_login_row);
						}
						
						return $this->lang->line("login_error_message");

					}
				}
				else{
					return 'Has excedido el número de intentos disponible. Por favor intentá más tarde.';
				}
			}
			else {
				return 'El nombre de usuario no existe.';
			}
		}
		else {
			show_404();
		}
	}

	function divisionAvailable(){

		$sql 	=  "SELECT * FROM teamingsectionavailable WHERE sectionId = ?";
		$query 	=	$this->db->query($sql,$this->session->sectionId)->row();

		if ($this->Identity->validate('teaming/divisions') || ($query!=NULL && $query->isAvailable == 1)){
			return TRUE;
		}

		return FALSE;
	}

	function sawPopup(){

		$sql 	=  "SELECT * FROM usershowpopupteaming WHERE userId = ?";
		$query 	=	$this->db->query($sql,$this->session->UserId)->row();
		
		if($query!=NULL){
			$this->db->delete('usershowpopupteaming', array("userId" => $this->session->UserId));
			return TRUE;
		}

		return FALSE;
	}

	function Logout()
	{
		$log = array(
			'userId' 	=> $this->session->UserId,
			'timestamp' => time()
		);
		$this->db->insert('logout_logs', $log);
		$this->session->sess_destroy();
		header('Location:/'.FOLDERADD);
	}
}
