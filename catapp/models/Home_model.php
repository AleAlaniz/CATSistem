<?php

Class Home_model extends CI_Model {

	public function GetAllAlerts()
	{
		$response = new StdClass();
		$response->status = 'ok';
		$sql = "SELECT * FROM alerts WHERE sectionId = ".$this->session->sectionId." ORDER BY timestamp DESC LIMIT 10";
		$response->alerts = $this->db->query($sql)->result();
		$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
		foreach ($response->alerts as $alert) {
			$alert->timestamp = date('d', $alert->timestamp).' '.$meses[date('n', $alert->timestamp)-1].' '.date('G:i', $alert->timestamp);
		}

		return escapeJsonString($response, FALSE);
	}

	public function DeleteAlertById($alertId)
	{
		$sql = "SELECT * FROM alerts WHERE alertId = ?";
		$query = $this->db->query($sql, $alertId)->row();

		if (isset($query))
		{
			$this->load->helper('utils_helper');
			insert_audit_logs('alerts','DELETE',$query);

			$objectDelete = array(
				'alertId' => $query->alertId
				);
			$this->db->delete('alerts', $objectDelete);
			$response = new StdClass();
			$response->status = 'ok';
			$response->message = $this->lang->line('panel_deletealertmessage');
			return escapeJsonString($response, FALSE);
		}
		else
		{
			return '{"status":"invalid"}';
		}
	}

	public function GetUnreadNewsletter()
	{
		$unread = 0;
		if ($this->Identity->Validate('newsletter/edit')) {
			$sql = "SELECT count(newsletterId) AS c FROM newsletters WHERE NOT EXISTS (SELECT 1 FROM newsletterViews WHERE newsletterViews.newsletterId = newsletters.newsletterId && newsletterViews.userId = ?)";
			$unread = $this->db->query($sql, $this->session->UserId)->row();
			$unread = $unread->c;
		}
		else
		{
			$sql = "SELECT count(newsletterId) AS c FROM newsletterLinks WHERE (sectionId = ? || roleId = ? || siteId = ? || userlinkId = ?) &&
			NOT EXISTS (SELECT 1 FROM newsletterViews WHERE newsletterViews.newsletterId = newsletterLinks.newsletterId && newsletterViews.userId = ?)
			GROUP BY newsletterId";
			$unread = $this->db->query($sql, array($this->session->sectionId,$this->session->roleId, $this->session->siteId, $this->session->UserId, $this->session->UserId))->row();
			$unread = $unread->c;
		}

		return $unread;
	}

	public function GetUnreadPress()
	{
		$unread = 0;
		if ($this->Identity->Validate('press/note/view')){ 
			$sql = "SELECT count(pn.pressnoteId) c FROM pressNotes AS pn
			WHERE NOT EXISTS (SELECT 1 FROM pressnoteViews AS pnv WHERE pn.pressnoteId = pnv.pressnoteId && pnv.userId = ?)";
			$unread = $this->db->query($sql, $this->session->UserId)->row()->c;
		}
		if ($this->Identity->Validate('press/clipping/view')){ 
			$sql = "SELECT count(pc.pressclippingId) c FROM pressClippings AS pc 
			WHERE NOT EXISTS (SELECT 1 FROM pressclippingViews AS pcv WHERE pc.pressclippingId = pcv.pressclippingId && pcv.userId = ?)";
			$unread += $this->db->query($sql, $this->session->UserId)->row()->c;
		}
		
		return $unread;
	}

	public function GetUnreadSuggestbox()
	{
		$unread = 0;
		$roles = ' && sectionId = '.$this->session->sectionId;
		if ($this->Identity->Validate('sections/viewall')) {
			$roles = ' ';
		}
		$sql = 'SELECT count(sb.suggestboxId) AS c 
		FROM suggestBox AS sb
		INNER JOIN (SELECT sectionId, userId FROM users) AS u ON sb.userId = u.userId 
		INNER JOIN (SELECT sectionId FROM sections WHERE true '.$roles.') AS s ON u.sectionId = s.sectionId 
		WHERE NOT EXISTS (SELECT 1 FROM suggestboxViews WHERE suggestboxViews.suggestboxId = sb.suggestboxId && userId = ?)';
		$suggests = $this->db->query($sql, $this->session->UserId)->row();
		$unread = $suggests->c;
		return $unread;
	}

	public function GetUnreadChatU()
	{
		$unread = 0;
		// $sql = 'SELECT count(*) c FROM chatuMessages WHERE fromuserId != ? && 
		// (SELECT count(*) FROM chatU WHERE chatuId = chatuMessages.chatuId && (fromuserId = ? || touserId = ?)) > 0 && 
		// (SELECT count(*) FROM chatumessageViews WHERE chatumessageId = chatuMessages.chatumessageId && userId = ?) = 0 && 
		// (SELECT count(*) FROM chatuDeletes WHERE userId = ? && chatuId = chatuMessages.chatuId && currentlyRemoved = "FALSE" && lastmessageIdDelete >= chatuMessages.chatumessageId ORDER BY chatudeleteId DESC LIMIT 1) = 0';
		// $unread = $this->db->query($sql, array($this->session->UserId, $this->session->UserId, $this->session->UserId, $this->session->UserId))->row()->c;


		$sql = 'SELECT 	count(*) AS c
		FROM chatuMessagesTemp 
		INNER JOIN (SELECT chatuId FROM chatU WHERE  (fromuserId = ? || touserId = ?))as chatsToUser ON chatsToUser.chatuId = chatuMessagesTemp.chatuId
		LEFT JOIN (SELECT * FROM chatumessageViews WHERE userId = ?) as sawMessages ON chatuMessagesTemp.chatumessageId = sawMessages.chatumessageId 
		WHERE fromuserId != ?
		AND chatumessageViewsId IS NULL
		AND chatuMessagesTemp.visible = "TRUE"
		AND (SELECT count(*) FROM chatuDeletes WHERE userId = ? && chatuId = chatuMessagesTemp.chatuId && currentlyRemoved = "TRUE") = 0';
		$unread = $this->db->query($sql, array($this->session->UserId,$this->session->UserId, $this->session->UserId, $this->session->UserId, $this->session->UserId))->row()->c;
		return $unread;
		

	}
	public function GetUnreadChatM()
	{
		$unread = 0;
		$sql = 'SELECT
		COUNT(cm.chatmmessageId) as c
		FROM
		(SELECT cm.chatmId, cm.chatmmessageId FROM chatmMessagesTemp AS cm 
			LEFT JOIN (SELECT chatmUsers.chatmId FROM chatmUsers WHERE userId = ?) cu ON cu.chatmId = cm.chatmId WHERE userId != ? AND cu.chatmId IS NOT NULL AND cm.visible = "TRUE") AS cm
		LEFT JOIN (SELECT chatmmessageViews.chatmmessageId FROM chatmmessageViews WHERE userId = ?) AS cv ON cv.chatmmessageId = cm.chatmmessageId
		WHERE
		cv.chatmmessageId IS NULL';
		$unread = $this->db->query($sql, array($this->session->UserId, $this->session->UserId, $this->session->UserId))->row()->c;
		return $unread;
	}

	public function GetUnreadForms(){

		$unreadJoin 	= 0;
		$unreadUpdate   = 0;

		$sql = 
		'SELECT COUNT(userId) AS agreeUnread
		FROM teamingforms
		WHERE agreementDate is NULL
		AND isApproved = 0';
		$unreadJoin = $this->db->query($sql)->row()->agreeUnread;

		$sql = 
		'SELECT COUNT(userId) AS agreeUpdateUnread
		FROM teamingupdateforms
		WHERE updateDate is NULL
		AND isUpdateApproved = 0';
		$unreadUpdate = $this->db->query($sql)->row()->agreeUpdateUnread;

		return  $unreadJoin + $unreadUpdate;
	}

	public function GetUnreadSections()
	{
		$this->load->model('Section_model', 'Section');
		$this->load->model('Notice_model', 'Notice');
		$this->load->model('Doc_model', 'Doc');

		$data = new StdClass();
		$sections = $this->Section->GetSections();

		foreach ($sections as $section) {
			$totalunread = 0;
			$subsectionUnread = $this->Notice->GetUnread($section->sectionId);
			$docUnread = $this->Doc->GetUnread($section->sectionId);
			
			$c_doc = count($docUnread);
			
			for ($i=0; $i < $c_doc; $i++) { 
				$subsectionUnread[] = $docUnread[$i];
			}	
			
			for ($i=0; $i < count($subsectionUnread); $i++) { 
				
				for ($j=0; $j < count($subsectionUnread); $j++) { 
					if($subsectionUnread[$i]->itemId == $subsectionUnread[$j]->itemId){
						if($i <> $j){
							$subsectionUnread[$i]->c += $subsectionUnread[$j]->c;
							array_splice($subsectionUnread,$j,1);
						}
					}
				}
				$totalunread+= $subsectionUnread[$i]->c;	
			}

			$object = new StdClass();
			$object->sectionId	= $section->sectionId;
			$object->totalunread = $totalunread;
			$object->subsectionUnread = $subsectionUnread;
			
			$data->unreads[] = $object;	
		}
		
		return $data->unreads;
	}

	public function resizeImages($text,$width,$height)
	{

		$this->load->library("Simple_html_dom");
		$this->load->library('image_lib');

		$config['image_library'] 	= 'gd2';
		$config['create_thumb'] 	= TRUE;
		$config['maintain_ratio'] 	= TRUE;
		$config['width']         	= $width;
		$config['height']       	= $height;
		$config['quality']       	= 75;

		$html 	 = new simple_html_dom();
		$html->load($text);
		$imgSrcs = array();

		foreach($html->find('img') as $element)
		{
       		$currentSrc  = $element->src;

       		if (file_exists($_SERVER['DOCUMENT_ROOT'].$currentSrc))
			{

				$config['source_image'] 	= $_SERVER['DOCUMENT_ROOT'].$currentSrc;
				$this->image_lib->initialize($config);

				if ( ! $this->image_lib->resize())
				{
					echo $this->image_lib->display_errors();
				}
				else{

					$element->src = str_replace(".jpg","_thumb.jpg",$currentSrc);
				}
			}
		}

		$result  = $html->save();
		return $result;
	}
}