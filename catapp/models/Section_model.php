<?php
Class Section_model extends CI_Model {
	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		$this->lang->load('login_lang');
		$this->load->model('Notice_model', 'Notice');
		$this->load->model('Doc_model', 'Doc');
	}

	var $sectionId;
	var $name;
	var $userId;
	var $icon;

	function GetSections(){
		$sql = "SELECT * FROM sections ORDER BY name";
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function GetSectionsForComponent()
	{
		$sql = 
		"SELECT sectionId as objectId, name
		FROM sections";
		return $this->db->query($sql)->result();
	}

	function CreateSection(){

		$this->form_validation->set_rules('name', 'lang:administration_sections_name', 'required|is_unique[sections.name]',
			array(
				'is_unique'     => $this->lang->line('administration_sections_nameexist')
				));

		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('_shared/_administrationlayoutheader');
			$this->load->view('sections/create');
			$this->load->view(PRELAYOUTFOOTER);
		}
		else
		{
			if (!$this->input->post('icon') || $this->input->post('icon') == '') {
				$iconSet = 'fa-th-list';
			}
			else{
				$iconSet = $this->input->post('icon');
			}
			
			$sectionInsert = array(
				'name' => $this->input->post('name'),
				'icon' => $iconSet,
				'userId' => $this->session->UserId
			);

			insert_audit_logs('sections','INSERT',$sectionInsert);
			
			$this->db->insert('sections', $sectionInsert);
			$this->session->set_flashdata('sectionMessage', 'create');
			header('Location:/'.FOLDERADD.'/sections');
		}
	}

	function DeleteSection(){

		if ($this->uri->segment(3)) {
			$sql = "SELECT * FROM sections WHERE sectionId = ?";
			$section = array($this->uri->segment(3));
			$query = $this->db->query($sql,$section)->row();
			if (isset($query))
			{
				if ($this->input->post('sectionId') && $this->input->post('sectionId') == $query->sectionId ) {

					$sectionDelete = array(
						'sectionId' => $this->input->post('sectionId')
						);
					$this->db->delete('sections', $sectionDelete);

					insert_audit_logs('sections','DELETE',$query);

					$this->session->set_flashdata('sectionMessage', 'delete');
					header('Location:/'.FOLDERADD.'/sections');
				}
				else{
					$query->navBar = $this->load->view('sections/_navbar', $query, TRUE);
					$this->load->view('_shared/_administrationlayoutheader');
					$this->load->view('sections/delete', $query);
					$this->load->view(PRELAYOUTFOOTER);
				}
			}
			else
			{
				show_404();
			}
		}
		else{
			show_404();
		}
	}

	function DetailsSection(){
		if ($this->uri->segment(3)) {
			$sql = "SELECT * FROM sections WHERE sectionId = ?";
			$section = array($this->uri->segment(3));
			$query = $this->db->query($sql,$section)->row();
			if (isset($query))
			{
				$query->navBar = $this->load->view('sections/_navbar', $query, TRUE);
				$this->load->view('_shared/_administrationlayoutheader');
				$this->load->view('sections/details', $query);
				$this->load->view(PRELAYOUTFOOTER);
			}
			else
			{
				show_404();
			}
		}
		else{
			show_404();
		}
	}

	function EditSection(){
		if ($this->uri->segment(3)) {
			$sql = "SELECT * FROM sections WHERE sectionId = ?";
			$section = array($this->uri->segment(3));
			$query = $this->db->query($sql,$section)->row();
			if (isset($query))
			{
				if ($this->input->post('sectionId') && $this->input->post('sectionId') == $query->sectionId ) {

					$nameRules = 'required|is_unique[sections.name]';
					$nameRulesMessages = array('is_unique' => $this->lang->line('administration_sections_nameexist'));

					if ($this->input->post('name') && $query->name == $this->input->post('name')) {
						$nameRules = 'required';
						$nameRulesMessages = NULL;
					}

					$this->form_validation->set_rules('name', 'lang:administration_sections_name', $nameRules ,$nameRulesMessages);

					if ($this->form_validation->run() == FALSE)
					{
						$query->navBar = $this->load->view('sections/_navbar', $query, TRUE);
						$this->load->view('_shared/_administrationlayoutheader');
						$this->load->view('sections/edit',$query);
						$this->load->view(PRELAYOUTFOOTER);
					}
					else
					{
						if (!$this->input->post('icon') || $this->input->post('icon') == '') {
							$iconSet = 'fa-th-list';
						}
						else{
							$iconSet = $this->input->post('icon');
						}

						$sectionEdit = array(
							'name' => $this->input->post('name'),
							'icon' => $iconSet
							);
						$this->db->where('sectionId', $query->sectionId);
						$this->db->update('sections', $sectionEdit);

						insert_audit_logs('sections','UPDATE',$query);

						$this->session->set_flashdata('sectionMessage', 'edit');
						header('Location:/'.FOLDERADD.'/sections/details/'.$query->sectionId);
					}
				}
				else{
					$query->navBar = $this->load->view('sections/_navbar', $query, TRUE);
					$this->load->view('_shared/_administrationlayoutheader');
					$this->load->view('sections/edit', $query);
					$this->load->view(PRELAYOUTFOOTER);
				}
			}
			else
			{
				show_404();
			}
		}
		else{
			show_404();
		}
	}


	function GetSection(){

		if ($this->input->post('sectionId'))
		{
			$validRole = '&& sectionId = '.$this->session->sectionId;

			if ($this->Identity->Validate('sections/viewall'))
			{
				$validRole = '';
			}
			
			$sql = "SELECT * FROM sections WHERE sectionId = ? ".$validRole;

			$query = $this->db->query($sql,array($this->input->post('sectionId')))->row();

			if (isset($query))
			{
				$sql = "SELECT * FROM subSections WHERE sectionId = ? ORDER BY name";
				$section = array($query->sectionId);

				$query->subSections = $this->db->query($sql,array($this->input->post('sectionId')))->result();

				$query->favorites = array();

				foreach ($query->subSections as $sskey => $ssvalue)
				{
					$ssvalue->noViews = FALSE;
					
					$sql = 'SELECT * FROM subSections WHERE fereingsubsectionId = ? ORDER BY name';
					$ssvalue->sssections = $this->db->query($sql, array($ssvalue->subsectionId))->result();

					foreach ($ssvalue->sssections as $sssection)
					{
						$sssection->noViews = FALSE;
						$sql = 'SELECT * FROM items WHERE subsectionId = ? ORDER BY name';
						$sssection->items = $this->db->query($sql, array($sssection->subsectionId))->result();

						foreach ($sssection->items as $ikey => $ivalue)
						{

							$sql = "SELECT itemfavoriteId FROM itemFavorites WHERE itemId = ? && userId = ?";
							$ivalue->isFavorite =  ($this->db->query($sql, array($ivalue->itemId, $this->session->UserId))->row() != NULL) ? TRUE : FALSE; 						

							if($ivalue->isFavorite)
							array_push($query->favorites, $ivalue);


							$ivalue->noViews = FALSE;


							if (!$this->Identity->Validate('items/index'))
							{
								if ($ivalue->toall == 'FALSE')
								{
									$sql = 'SELECT * FROM itemUsers WHERE itemId = ? && userId = ?';
									$userExist = $this->db->query($sql, array($ivalue->itemId, $this->session->UserId))->row();
									
									if (!isset($userExist) || $userExist == NULL)
									{
										unset($sssection->items[$ikey]);
									}
								}
							}

						}
					}

					$sql = 'SELECT * FROM items WHERE subsectionId = ? ORDER BY name';
					$ssvalue->items = $this->db->query($sql, array($ssvalue->subsectionId))->result();

					foreach ($ssvalue->items as $ikey => $ivalue)
					{
						$sql = "SELECT itemfavoriteId FROM itemFavorites WHERE itemId = ? && userId = ?";
						$ivalue->isFavorite =  ($this->db->query($sql, array($ivalue->itemId, $this->session->UserId))->row() != NULL) ? TRUE : FALSE; 

						if($ivalue->isFavorite)
							array_push($query->favorites, $ivalue);

						$ivalue->noViews = FALSE;

						if (!$this->Identity->Validate('items/index')) 
						{
							if ($ivalue->toall == 'FALSE')
							{
								$sql = 'SELECT * FROM itemUsers WHERE itemId = ? && userId = ?';
								$userExist = $this->db->query($sql, array($ivalue->itemId, $this->session->UserId))->row();

								if (!isset($userExist) || $userExist == NULL) 
								{
									unset($ssvalue->items[$ikey]);
								}
							}
						}

					}
				}

				echo json_encode($query);
			}
			else
			{
				show_404();
			}
		}
		else
		{
			show_404();
		}

	}


	function GetMoreItems()
	{
		if($this->input->post('subSectionId'))
		{
			$sql="SELECT *
			FROM items
			WHERE subsectionId=?";
		}
		else
			echo "[]";
	}

	/*function GetSection()
	{

		if($this->input->post('sectionId'))
		{
			$validRole = '&& sectionId = '.$this->session->sectionId;

			if ($this->Identity->Validate('sections/viewall'))
			{
				$validRole = '';
			}

			$sql = "SELECT * FROM sections WHERE sectionId = ? ".$validRole;
			$section = array($this->input->post('sectionId'));
			$query = $this->db->query($sql,$section)->row();

			if(isset($query))
			{
				$sql = "SELECT * FROM subSections WHERE sectionId = ? ORDER BY name";

				$section = array($this->input->post('sectionId'));

				$data=new stdClass();
				$data->sections= $this->db->query($sql,$section)->result();


				$sql="SELECT s.icon,s.name as 'subsectionName'
				,i.itemId,i.name
				FROM items i join subSections s
				on(i.subsectionId=s.subsectionId)
				join itemFavorites f
				on(i.itemId=f.itemId)
				where f.userId=?";

				$data->favorites=$this->db->query($sql,$this->session->UserId)->result();

				echo json_encode($data);
			}
			else
				echo "fail";

		}
		else
			echo "no sectionId";

		}



		function GetSections_Public($attributes = "*"){
			if ($this->Identity->Validate("sections/viewall")) {
				$Sql_where = "";
			}
			else
			{
				$Sql_where = "WHERE sectionId = ".$this->session->sectionId;
			}
			$sql = "SELECT $attributes FROM sections $Sql_where ORDER BY name";
			$query = $this->db->query($sql);
			return $query->result();
		}*/

	}
