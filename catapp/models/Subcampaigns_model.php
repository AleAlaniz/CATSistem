<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SubCampaigns_model extends CI_Model {

    public function GetSubCampaigns()
    {
        $sql = "SELECT * FROM subCampaigns ORDER BY name";
        $data = $this->db->query($sql)->result();
        return $data;
    }

    public function Create($object)
    {
        if(isset($object))
        {
            $this->db->insert('subCampaigns', $object);
            return "success";
        }
        else{
            return "error";
        }
        
    }

    public function GetSubCampaignById($id)
    {
        $sql = "SELECT * FROM subCampaigns WHERE subCampaignId = ?";
        $res = $this->db->query($sql,$id)->row();
        return $res;
    }

    public function Edit($object)
    {
        if(isset($object))
        {
            $this->db->where('subCampaignId', $object['subCampaignId']);
            $this->db->update('subCampaigns',$object);
            return "success";
        }
    }

    public function Delete($id)
    {
        if(isset($id))
        {
            $sql = "DELETE FROM subCampaigns WHERE subCampaignId = ?";
            $this->db->query($sql,$id);
            return "success";
        }
        return "error";
    }
    

}

/* End of file SubCampaigns_model.php */
?>