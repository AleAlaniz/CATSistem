<?php
Class Campaigns_model extends CI_Model {

	function GetCampaigns(){
		$data = new StdClass();
		$sql = "SELECT c.name, s.name section, c.campaignId, cl.name client
		FROM campaigns AS c 
		LEFT JOIN sections AS s ON c.sectionId = s.sectionId 
		LEFT JOIN clients AS cl ON c.clientId = cl.clientId
		ORDER BY c.name";
		$data->model = $this->db->query($sql)->result();
		$this->load->view('_shared/_administrationlayoutheader');
		$this->load->view('campaigns/campaigns', $data);
		$this->load->view(PRELAYOUTFOOTER);
	}


	function Create(){

		$this->form_validation->set_rules('name', 'lang:campaign_name', 'required|is_unique[campaigns.name]',
			array(
				'is_unique'     => $this->lang->line('campaign_error_exist')
				));

		$sectionsSql = "SELECT * FROM sections";
		$sectionsQuery = $this->db->query($sectionsSql)->result();
		$sectionsIds = '';
		foreach ($sectionsQuery as $value) {
			$sectionsIds .= $value->sectionId.',';
		}
		$this->form_validation->set_rules('section', 'lang:general_section','in_list['.$sectionsIds.'0]',
			array('in_list' => $this->lang->line('administration_users_sectionerror')));

		$clientsSQL = "SELECT * FROM clients";
		$clientsQuery = $this->db->query($clientsSQL)->result();
		$clientsIds = '';
		foreach ($clientsQuery as $value) {
			$clientsIds .= $value->clientId.',';
		}
		$this->form_validation->set_rules('client', 'lang:general_client','in_list['.$clientsIds.'0]',
			array('in_list' => $this->lang->line('clients_no_exist')));

		if ($this->form_validation->run() == FALSE)
		{
			$data = array();
			$sql = "SELECT * FROM sections ORDER BY name";
			$data['sections'] = $this->db->query($sql)->result();
			$sql = "SELECT * FROM clients ORDER BY name";
			$data['clients'] = $this->db->query($sql)->result();

			$this->load->view('_shared/_administrationlayoutheader');
			$this->load->view('campaigns/create', $data);
			$this->load->view(PRELAYOUTFOOTER);
		}
		else
		{
			$section = ($this->input->post('section') == 0 ? NUll : $this->input->post('section'));
			$client = ($this->input->post('client') == 0 ? NUll : $this->input->post('client'));

			$rowInsert = array(
				'name' => $this->input->post('name'),
				'timestamp' => time(),
				'userId' => $this->session->UserId,
				'sectionId' => $section,
				'clientId' => $client
				);

			$this->db->insert('campaigns', $rowInsert);
			$this->session->set_flashdata('flashMessage', 'create');
			header('Location:/'.FOLDERADD.'/campaigns');
		}
	}



	function Delete(){
		if ($this->uri->segment(3)) {
			$sql = "SELECT c.name, s.name section, cl.name client, c.campaignId 
			FROM campaigns AS c 
			LEFT JOIN sections AS s ON c.sectionId = s.sectionId
			LEFT JOIN clients AS cl ON c.clientId = cl.clientId
			WHERE c.campaignId = ?";

			$query = $this->db->query($sql,$this->uri->segment(3))->row();

			if (isset($query))
			{
				if ($this->input->post('campaignId') && $this->input->post('campaignId') == $query->campaignId)
				{
					$rowDelete = array(
						'campaignId' => $this->input->post('campaignId')
						);

					insert_audit_logs('campaigns','DELETE',$query);

					$this->db->delete('campaigns', $rowDelete);
					$this->session->set_flashdata('flashMessage', 'delete');
					header('Location:/'.FOLDERADD.'/campaigns');
				}
				else
				{
					$query->navBar = $this->load->view('campaigns/_navbar', $query, TRUE);
					$this->load->view('_shared/_administrationlayoutheader');
					$this->load->view('campaigns/delete', $query);
					$this->load->view(PRELAYOUTFOOTER);
				}
			}
			else
			{
				show_404();
			}
		}
		else{
			show_404();
		}
	}


	function Edit(){
		if ($this->uri->segment(3))
		{
			$sql = "SELECT * FROM campaigns WHERE campaignId = ?";
			$query = $this->db->query($sql,$this->uri->segment(3))->row();

			if (isset($query))
			{
				$nameRules = 'required|is_unique[campaigns.name]';
				$nameRulesMessages = array('is_unique' => $this->lang->line('campaign_error_exist'));

				if ($this->input->post('name') && $query->name == $this->input->post('name')) {
					$nameRules = 'required';
					$nameRulesMessages = NULL;
				}

				$this->form_validation->set_rules('name', 'lang:campaign_name', $nameRules ,$nameRulesMessages);


				$sectionsSql = "SELECT * FROM sections";
				$sectionsQuery = $this->db->query($sectionsSql)->result();
				$sectionsIds = '';
				foreach ($sectionsQuery as $value) {
					$sectionsIds .= $value->sectionId.',';
				}
				$this->form_validation->set_rules('section', 'lang:general_section','in_list['.$sectionsIds.'0]',
					array('in_list' => $this->lang->line('administration_users_sectionerror')));


				$clientsSQL = "SELECT * FROM clients";
				$clientsQuery = $this->db->query($clientsSQL)->result();
				$clientsIds = '';
				foreach ($clientsQuery as $value) {
					$clientsIds .= $value->clientId.',';
				}
				$this->form_validation->set_rules('client', 'lang:general_client','in_list['.$clientsIds.'0]',
					array('in_list' => $this->lang->line('clients_no_exist')));

				if ($this->form_validation->run() == FALSE)
				{
					$query->navBar = $this->load->view('campaigns/_navbar', $query, TRUE);
					$sql = "SELECT * FROM sections ORDER BY name";
					$query->sections = $this->db->query($sql)->result();
					$sql = "SELECT * FROM clients ORDER BY name";
					$query->clients = $this->db->query($sql)->result();

					$this->load->view('_shared/_administrationlayoutheader');
					$this->load->view('campaigns/edit',$query);
					$this->load->view(PRELAYOUTFOOTER);
				}
				else
				{
					$section = ($this->input->post('section') == 0 ? NUll : $this->input->post('section'));
					$client = ($this->input->post('client') == 0 ? NUll : $this->input->post('client'));

					$objectEdit = array(
						'name' => $this->input->post('name'),
						'sectionId' => $section,
						'clientId' => $client
						);

					insert_audit_logs('campaigns','UPDATE',$query);

					$this->db->where('campaignId', $query->campaignId);
					$this->db->update('campaigns', $objectEdit);
					$this->session->set_flashdata('flashMessage', 'edit');
					header('Location:/'.FOLDERADD.'/campaigns');
				}
			}
			else
			{
				show_404();
			}
		}
		else{
			show_404();
		}
	}
}
