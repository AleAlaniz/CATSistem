<?php
Class Teamingaudits_model extends CI_Model {

	function sendCommunications($communicationData){

		$table  		= 	htmlEntities($communicationData["type"]);
		$recipients 	= 	htmlEntities($communicationData["user"]);
		$data 			= 	htmlEntities($communicationData["data"]);
		$colSearch		=	htmlEntities($communicationData["colSearch"]);
		$colResult 		=	htmlEntities($communicationData["colResult"]);
		$action 		=	htmlEntities("admin_".$table);
		$res 			= 	new StdClass();
		$res->status	= 	'ok';
		$res->message 	= 	array();
		$data 			= 	"";

		$sql 	= 
		"SELECT userPersonalData.userId 
		FROM userPersonalData
		INNER JOIN users ON userPersonalData.userId = users.userId
		INNER JOIN roles ON users.roleId = roles.roleId 
		WHERE roles.roleId = 25";
		$admins	= $this->db->query($sql)->result();

		$last = array_pop($admins);
		$adminList = $last->userId; 

		foreach ($admins as $admin) {

			$adminList =	$adminList." , ".$admin->userId;
		}

		$sql 	= "SELECT " . $colResult . " AS communicationId FROM " . $table . " WHERE " . $colSearch . " IN (".$adminList.") ORDER BY " . $colResult . " DESC";
		$query 	= $this->db->query($sql)->row();
		$data 	= $query->communicationId;

		foreach ($recipients as $recipient) {
			$data =	$data." - ".$recipient["userId"];
		}

		$timestamp 		= time() - (3600*3);
		$communication 	= array(

			'date'	=> gmdate("Y-m-d H:i:s",$timestamp),
			'userId'=> $this->session->UserId,
			'action'=> $action,
			'data'	=> $data
			);
		$this->db->insert('teamingaudits', $communication);

	}

	function sendSuggestion($communicationData){

		$res 		  = new StdClass();
		$res->status  = 'invalid';
		$res->message = array();
		$data 		  = "";

		if (isset($communicationData)){

			$res->status = 'ok';
			$timestamp 	 = time() - (3600*3);

			if (! isset($communicationData->userId)){

				$communicationData->userId = $this->session->UserId;
			}
			
			$communicationData->completeAdress = '';

			if (isset($communicationData->chosenState) && isset($communicationData->chosenCity)){

				$communicationData->completeAdress = "Provincia:".$communicationData->chosenState."-Localidad:".$communicationData->chosenCity;
			}
			
			$communicationData->completeAdress = (strlen($communicationData->address) > 0)?  $communicationData->completeAdress."-Dirección:".$communicationData->address : $communicationData->completeAdress;
			$communicationData->body 		   = $communicationData->completeAdress."-".$communicationData->email."-".$communicationData->number."-".$communicationData->referrer."-".$communicationData->description;

			$communication 	= array(

				'date'	=> gmdate("Y-m-d H:i:s",$timestamp),
				'userId'=> $communicationData->userId,
				'action'=> "suggest",
				'data'	=> $communicationData->name." - ".$communicationData->body,
			);

			$this->db->insert('teamingaudits', $communication);

			$institutionEntry =  array(

				'name'		 	 => $communicationData->name,
				'address' 		 => (strlen($communicationData->address) > 0)? 	   $communicationData->address  	: null,
				'email' 		 => (strlen($communicationData->email) > 0)? 	   $communicationData->email  		: null,
				'number' 		 => (strlen($communicationData->number) > 0)? 	   $communicationData->number  		: null,
				'referrer' 		 => (strlen($communicationData->referrer) > 0)?    $communicationData->referrer  	: null,
				'description' 	 => (strlen($communicationData->description) > 0)? $communicationData->description 	: null,
				'province'		 => (isset($communicationData->chosenState))?	   $communicationData->chosenState  : null,
				'locality' 		 => (isset($communicationData->chosenCity))? 	   $communicationData->chosenCity   : null,
				'userId'	  	 => $communicationData->userId,
				'suggestionDate' => time() - (3600*3),
				);

			$this->db->insert('teamingsuggestedinstitutions', $institutionEntry);

			$res->status  = 'ok';
			$res->message = $communication;
		}
		return $res;
	}

	function getSuggestions($suggestedIds){

		$query = null;

		if (isset($suggestedIds)){

			$sql = '
			SELECT teamingsuggestedinstitutions.*,
			teamingsuggestedinstitutions.province as idProvince,
			teamingsuggestedinstitutions.locality as idCity,
			CONCAT_WS(" ",userPersonalData.name,userPersonalData.lastName) AS suggestedFrom,
			sites.name as siteName,
			sites.siteId as siteId,
			sections.name as division,
			userComplementaryData.turn as turn,
			states.state as province,
			cities.city as locality,
			from_unixtime(teamingsuggestedinstitutions.suggestionDate,"%d-%m-%Y") as date
			FROM teamingsuggestedinstitutions
			INNER JOIN userPersonalData ON teamingsuggestedinstitutions.userId = userPersonalData.userId
			INNER JOIN users ON teamingsuggestedinstitutions.userId = users.userId
			INNER JOIN sites ON users.siteId = sites.siteId
			INNER JOIN sections ON users.sectionId = sections.sectionId
			INNER JOIN userComplementaryData ON userComplementaryData.userId = teamingsuggestedinstitutions.userId
			INNER JOIN states ON teamingsuggestedinstitutions.province = states.stateId
			INNER JOIN cities ON teamingsuggestedinstitutions.locality = cities.cityId';
			$sql = $sql.' WHERE suggestedId IN ('.htmlspecialchars($suggestedIds).')';
		}
		else{
			
			$sql = '
			SELECT teamingsuggestedinstitutions.suggestedId,
			teamingsuggestedinstitutions.name,
			teamingsuggestedinstitutions.status,
			CONCAT_WS(" ",userPersonalData.name,userPersonalData.lastName) AS suggestedFrom,
			sections.name as division,
			sites.name as siteName,
			sites.siteId as siteId,
			userComplementaryData.turn as turn
			FROM teamingsuggestedinstitutions
			INNER JOIN userPersonalData ON teamingsuggestedinstitutions.userId = userPersonalData.userId
			INNER JOIN users ON teamingsuggestedinstitutions.userId = users.userId
			INNER JOIN sites ON users.siteId = sites.siteId
			INNER JOIN sections ON users.sectionId = sections.sectionId
			INNER JOIN userComplementaryData ON userComplementaryData.userId = teamingsuggestedinstitutions.userId';

			if(!$this->Identity->Validate('teaming/suggestions/actions')){
				$sql.= ' WHERE teamingsuggestedinstitutions.userId = '.$this->session->UserId;
			}
		}

		$query = $this->db->query($sql)->result();

		for ($i=0; $i < sizeof($query) ; $i++) { 

			$query[$i]->title   = $this->lang->line('teaming_sign_title_section_'.$query[$i]->siteId);
		}

		return $query;
	}

	function updateSuggestion($suggestion)
	{
		$res 		  = new StdClass();

		$res->status  = 'invalid';
		$res->message = '';

		if (isset($suggestion)){


			$suggestToUpdate	  		= $suggestion->suggestedId;
			$suggestion->province 		= $suggestion->chosenState;
			$suggestion->locality		= $suggestion->chosenCity;

			unset($suggestion->suggestedFrom);
			unset($suggestion->division);
			unset($suggestion->turn);
			unset($suggestion->selected);
			unset($suggestion->chosenState);
			unset($suggestion->chosenCity);
			unset($suggestion->idProvince);
			unset($suggestion->idCity);
			unset($suggestion->date);

			$this->db->where('suggestedId', $suggestToUpdate);
			$this->db->update('teamingsuggestedinstitutions', $suggestion);

			$res->status  = 'ok';
			$res->message  = 'La actualizacion de la sugerencia fue exitosa';
		}	
		
		return $res;
	}

	function acceptSuggestion($suggestionId)
	{

		$sql = 
		'UPDATE teamingsuggestedinstitutions
		SET status = 1
		WHERE suggestedId = ?';
		$query = $this->db->query($sql,$suggestionId);

		$sql = 
		'SELECT name,
		userId
		FROM teamingsuggestedinstitutions
		WHERE suggestedId = ?';
		$query = $this->db->query($sql,$suggestionId)->row();

		return $query;
	}
}
