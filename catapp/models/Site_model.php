<?php
Class Site_model extends CI_Model {

	function GetSites(){
		$data = new StdClass();
		$sql = "SELECT * FROM sites ORDER BY name";
		$data->model = $this->db->query($sql)->result();
		$this->load->view('_shared/_administrationlayoutheader');
		$this->load->view('sites/sites', $data);
		$this->load->view(PRELAYOUTFOOTER);
	}

	public function GetSitesForComponent()
	{
		$sql = 
		"SELECT siteId as objectId, name
		FROM sites";
		return $this->db->query($sql)->result();
	}

	function Create(){

		$this->form_validation->set_rules('name', 'lang:sites_name', 'required|is_unique[sites.name]',
			array(
				'is_unique'     => $this->lang->line('sites_nameexist')
				));

		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('_shared/_administrationlayoutheader');
			$this->load->view('sites/create');
			$this->load->view(PRELAYOUTFOOTER);
		}
		else
		{
			$sectionInsert = array(
				'name' => $this->input->post('name'),
				'timestamp' => time(),
				'userId' => $this->session->UserId
				);

			insert_audit_logs('sites','INSERT',$sectionInsert);

			$this->db->insert('sites', $sectionInsert);
			$this->session->set_flashdata('flashMessage', 'create');
			header('Location:/'.FOLDERADD.'/sites');
		}
	}

	function Delete(){
		if ($this->uri->segment(3)) {
			$sql = "SELECT * FROM sites WHERE siteId = ?";
			$query = $this->db->query($sql,$this->uri->segment(3))->row();
			if (isset($query))
			{
				if ($this->input->post('siteId') && $this->input->post('siteId') == $query->siteId ) {
					$sectionDelete = array(
						'siteId' => $this->input->post('siteId')
						);
					
					insert_audit_logs('sites','DELETE',$query);

					$this->db->delete('sites', $sectionDelete);
					$this->session->set_flashdata('flashMessage', 'delete');
					header('Location:/'.FOLDERADD.'/sites');
				}
				else{
					$query->navBar = $this->load->view('sites/_navbar', $query, TRUE);
					$this->load->view('_shared/_administrationlayoutheader');
					$this->load->view('sites/delete', $query);
					$this->load->view(PRELAYOUTFOOTER);
				}
			}
			else
			{
				show_404();
			}
		}
		else{
			show_404();
		}
	}

	function Details(){
		if ($this->uri->segment(3)) {
			$sql = "SELECT * FROM sites WHERE siteId = ?";
			$query = $this->db->query($sql,$this->uri->segment(3))->row();
			if (isset($query))
			{
				$query->navBar = $this->load->view('sites/_navbar', $query, TRUE);
				$this->load->view('_shared/_administrationlayoutheader');
				$this->load->view('sites/details', $query);
				$this->load->view(PRELAYOUTFOOTER);
			}
			else
			{
				show_404();
			}
		}
		else{
			show_404();
		}
	}

	function Edit(){
		if ($this->uri->segment(3)) {
			$sql = "SELECT * FROM sites WHERE siteId = ?";
			$query = $this->db->query($sql,$this->uri->segment(3))->row();
			if (isset($query))
			{
				$nameRules = 'required|is_unique[sites.name]';
				$nameRulesMessages = array('is_unique' => $this->lang->line('sites_nameexist'));

				if ($this->input->post('name') && $query->name == $this->input->post('name')) {
					$nameRules = 'required';
					$nameRulesMessages = NULL;
				}

				$this->form_validation->set_rules('name', 'lang:sites_name', $nameRules ,$nameRulesMessages);

				if ($this->form_validation->run() == FALSE)
				{
					$query->navBar = $this->load->view('sites/_navbar', $query, TRUE);
					$this->load->view('_shared/_administrationlayoutheader');
					$this->load->view('sites/edit',$query);
					$this->load->view(PRELAYOUTFOOTER);
				}
				else
				{
					$objectEdit = array(
						'name' => $this->input->post('name'),
						);

					insert_audit_logs('sites','UPDATE',$query);

					$this->db->where('siteId', $query->siteId);
					$this->db->update('sites', $objectEdit);
					$this->session->set_flashdata('flashMessage', 'edit');
					header('Location:/'.FOLDERADD.'/sites/details/'.$query->siteId);
				}
			}
			else
			{
				show_404();
			}
		}
		else{
			show_404();
		}
	}
}
