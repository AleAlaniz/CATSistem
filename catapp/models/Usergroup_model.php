<?php
Class Usergroup_model extends CI_Model {
	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		$this->lang->load('login_lang');
	}

	function ViewUserGroups(){
		$data = new stdClass();

		$sql = "SELECT * FROM userGroups WHERE userId = ? ORDER BY name";
		$data->userGroups = $this->db->query($sql, array($this->session->UserId))->result();
		
		$this->load->view('_shared/_administrationlayoutheader');
		$this->load->view('usergroups/usergroups', $data);
		$this->load->view(PRELAYOUTFOOTER);
	}

	function CreateUserGroup(){

		$this->form_validation->set_rules('name', 'lang:administration_usergroups_name', 'required');
		$this->form_validation->set_rules('usersId[]', 'lang:administration_usergroups_users', 'required');

		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('_shared/_administrationlayoutheader');
			$this->load->view('usergroups/create');
			$this->load->view(PRELAYOUTFOOTER);
		}
		else
		{
			$timestamp = time();
			$validRole = '';
			//if ($this->session->Role == 4) {
			if (!$this->Identity->Validate('home/inbox/new/torole')) {
				$validRole = ' && roleId != '.$this->session->Role;
			}

			$shared = 'FALSE';

			if ($this->input->post('shared')) {
				$shared = 'TRUE';
			}

			$userInsert = array(
				'name' => $this->input->post('name'),
				'userId' => $this->session->UserId,
				'shared' => $shared,
				'timestamp' => $timestamp
				);

			$this->db->insert('userGroups', $userInsert);
			$usergroupId = $this->db->insert_id();

			$this->session->set_flashdata('usergroupMessage', 'create');

			foreach ($this->input->post('usersId[]') as $key => $user) {
				$sql = "SELECT userId FROM users WHERE userId != ? && userId = ? ".$validRole;
				$userExist = $this->db->query($sql ,array($this->session->UserId, $user))->row();
				if (isset($userExist) && $userExist != NULL) {
					$objectInsert = array(
						'timestamp' 	=> $timestamp,
						'userId' 		=> $user,
						'usergroupId' 	=> $usergroupId
						);
					$this->db->insert('usergroupUsers', $objectInsert);
				}
			}

			header('Location:/'.FOLDERADD.'/usergroups');
		}
	}

	function DeleteUserGroup(){
		if ($this->uri->segment(3)) {
			$sql = "SELECT * FROM userGroups WHERE usergroupId = ? && userId = ?";
			$query = $this->db->query($sql, array($this->uri->segment(3), $this->session->UserId))->row();
			if (isset($query))
			{
				if ($this->input->post('usergroupId') && $this->input->post('usergroupId') == $query->usergroupId ) {
					$objectDelete = array(
						'usergroupId' => $query->usergroupId
						);
					$this->db->delete('userGroups', $objectDelete);
					$this->session->set_flashdata('usergroupMessage', 'delete');
					header('Location:/'.FOLDERADD.'/usergroups');
				}
				else{
					$sql = "SELECT up.name, up.lastName, up.userId FROM usergroupUsers AS uu INNER JOIN (SELECT name, lastName, userId FROM userPersonalData) AS up ON uu.userId = up.userId WHERE usergroupId = ?";
					$query->users = $this->db->query($sql, $query->usergroupId)->result();
					$query->navBar = $this->load->view('usergroups/_navbar', $query, TRUE);
					$this->load->view('_shared/_administrationlayoutheader');
					$this->load->view('usergroups/delete', $query);
					$this->load->view(PRELAYOUTFOOTER);
				}
			}
			else
			{
				show_404();
			}
		}
		else{
			show_404();
		}
	}

	function DetailsUserGroup(){
		if ($this->uri->segment(3)) {
			$sql = "SELECT * FROM userGroups WHERE usergroupId = ? && userId = ?";
			$query = $this->db->query($sql, array($this->uri->segment(3), $this->session->UserId))->row();
			if (isset($query) && $query != NULL)
			{
				$sql = "SELECT up.name, up.lastName, up.userId FROM usergroupUsers AS uu INNER JOIN (SELECT name, lastName, userId FROM userPersonalData) AS up ON uu.userId = up.userId WHERE usergroupId = ?";
				$query->users = $this->db->query($sql, $query->usergroupId)->result();
				$query->navBar = $this->load->view('usergroups/_navbar', $query, TRUE);
				$this->load->view('_shared/_administrationlayoutheader');
				$this->load->view('usergroups/details', $query);
				$this->load->view(PRELAYOUTFOOTER);
			}
			else
			{
				show_404();
			}
		}
		else{
			show_404();
		}
	}

	function EditUserGroup(){
		if ($this->uri->segment(3)) {
			$sql = "SELECT * FROM userGroups WHERE usergroupId = ? && userId = ?";
			$query = $this->db->query($sql, array($this->uri->segment(3), $this->session->UserId))->row();
			if (isset($query))
			{

				$this->form_validation->set_rules('name', 'lang:administration_usergroups_name', 'required');
				$this->form_validation->set_rules('usersId[]', 'lang:administration_usergroups_users', 'required');

				if ($this->form_validation->run() == FALSE)
				{
					$sql = "SELECT up.name, up.lastName, up.userId FROM usergroupUsers AS uu INNER JOIN (SELECT name, lastName, userId FROM userPersonalData) AS up ON uu.userId = up.userId WHERE usergroupId = ?";
					$query->users = $this->db->query($sql, $query->usergroupId)->result();
					$query->navBar = $this->load->view('usergroups/_navbar', $query, TRUE);
					$this->load->view('_shared/_administrationlayoutheader');
					$this->load->view('usergroups/edit', $query);
					$this->load->view(PRELAYOUTFOOTER);
				}
				else
				{
					$timestamp = time();
					$validRole = '';
					//if ($this->session->Role == 4) {
					if (!$this->Identity->Validate('home/inbox/new/torole')) {
						$validRole = ' && roleId != '.$this->session->Role;
					}

					$shared = 'FALSE';
					if ($this->input->post('shared')) {
						$shared = 'TRUE';
					}
					$objectEdit = array(
						'name' => $this->input->post('name'),
						'shared' => $shared
						);
					$this->db->where('usergroupId', $query->usergroupId);
					$this->db->update('userGroups', $objectEdit);

					$usersPost = $this->input->post('usersId[]');

					$sql = "SELECT * FROM usergroupUsers WHERE usergroupId = ?";
					$query->users = $this->db->query($sql,array($query->usergroupId))->result();
					foreach ($query->users as $userkey => $user) {
						if ( $findUser = array_search($user->userId, $this->input->post('usersId[]') )) {
							unset($usersPost[$findUser]);
						}
						else
						{
							$objectDelete = array(
								'usergroupuserId' => $user->usergroupuserId
								);
							$this->db->delete('usergroupUsers', $objectDelete);
						}
					}

					foreach ($usersPost as $key => $user) {
						$sql = "SELECT userId FROM users WHERE userId != ? && userId = ? ".$validRole;
						$userExist = $this->db->query($sql, array($this->session->UserId, $user ))->row();
						if (isset($userExist) && $userExist != NULL) {
							$objectInsert = array(
								'timestamp' 	=> $timestamp,
								'userId' 		=> $user,
								'usergroupId' 	=> $query->usergroupId
								);
							$this->db->insert('usergroupUsers', $objectInsert);
						}
					}
					
					$this->session->set_flashdata('usergroupMessage', 'edit');

					header('Location:/'.FOLDERADD.'/usergroups/details/'.$query->usergroupId);
				}
			}
			else
			{
				show_404();
			}
		}
		else{
			show_404();
		}
	}
}
