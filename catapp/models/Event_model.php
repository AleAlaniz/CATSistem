<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Event_model extends CI_Model {
    
        public function __construct()
        {
            parent::__construct();
            $this->load->model('Notification_model','Notification');
        }
        public function getEventsForUser($userId)
        {
            $today = date('Y-m-d');
            $sql =
            "SELECT 
            er.eventReminderId as eventReminderId,
            image
            FROM eventReminders er
            INNER JOIN events e
            ON e.eventId = er.eventId
            INNER JOIN eventLinks el
            ON er.eventId = el.eventId
            WHERE FROM_UNIXTIME(dateReminder,'%Y-%m-%d') = '$today'
            AND el.userId = $userId
            AND eventReminderId NOT IN (select eventReminderId from eventViews WHERE FROM_UNIXTIME(timestamp,'%Y-%m-%d') = '$today' AND userId = $userId)";
            $event = $this->db->query($sql)->row();
            return $event;
        }

        public function getEvents($notificationId)
        {
            $events = $this->db->get_where('events',array('notificationId' => $notificationId))->result();
            foreach ($events as $e) {
                $e->startDate = date('d-m-Y',$e->startDate);
                $e->endDate   = date('d-m-Y',$e->endDate);
                $eventReminders = $this->db->order_by('dateReminder','ASC')->get_where('eventReminders',array('eventId' => $e->eventId))->result();
                if(count($eventReminders)>0)
                {
                    foreach ($eventReminders as $r) {
                        $e->eventReminders[] = date('d-m-Y',$r->dateReminder);
                    }
                }
            }
            return $events;
        }

        public function getEventById($eventId)
        {
            $result = $this->db->get_where('events',array('eventId' => $eventId))->row();
            $usersInEvent = $this->getUsersInEvent($eventId);
            if(isset($result))
            {
                $result->startDate = date('d-m-Y',$result->startDate);
                $result->endDate   = date('d-m-Y',$result->endDate);
                $eventReminders = $this->getEventReminders($eventId,time(date('d-m-Y')));
                if(count($eventReminders)>0)
                {
                    foreach ($eventReminders as $r) {

                        $result->eventReminders[] = date('d-m-Y',$r["dateReminder"]);
                    }
                }
                if (count($usersInEvent)>0) {
                    foreach ($usersInEvent as $user) {
                        $result->users[] = $user;
                    }
                }

                echo escapeJsonString(json_encode($result));
            }
            else
            {
                echo "invalid";
            }
        }

        public function getUsers($sections,$sites,$roles,$teaming,$civilState,$genre,$turn,$startDate,$endDate)
        {
            $this->db->select('users.userId,CONCAT(name ," ", lastName) AS completeName,userName');
            $this->db->join('userPersonalData', 'userPersonalData.userId = users.userId');
            $this->db->join('userComplementaryData', 'userComplementaryData.userId = users.userId');
            $this->db->join('teamingforms', 'teamingforms.userId = users.userId','left');
            if($sections)
            {
                $this->db->where_in('sectionId', $sections);
            }
            if($sites)
            {
                $this->db->where_in('siteId', $sites);
            }
            if($roles)
            {
                $this->db->where_in('roleId', $roles);
            }
            if(!is_null($teaming))
            {
                if ($teaming) {
                    $this->db->where('isApproved', '1');
                    $this->db->where("(agreementDate IS NOT NULL)");    
                } 
                else {
                    $this->db->where("(teamingforms.userId IS NULL)");
                }
            }
            if($civilState)
            {
                $this->db->where('civilState', $civilState);
            }
            if($genre)
            {
                $this->db->where('gender', $genre);
            }
            if($turn)
            {
                $this->db->where('turn', $turn);
            }
            if($startDate && $endDate)
            {
                $this->db->where("(DAY(birthDate) >= DAY(STR_TO_DATE('$startDate','%d-%m')) AND MONTH(birthDate) >= MONTH(STR_TO_DATE('$startDate','%d-%m')))");
                $this->db->where("(DAY(birthDate) <= DAY(STR_TO_DATE('$endDate','%d-%m')) AND MONTH(birthDate) <= MONTH(STR_TO_DATE('$endDate','%d-%m')))");
            }
            $result = $this->db->get('users')->result();
            
            echo json_encode($result);
        }

        public function getUsersInEvent($eventId)
        {
            
            $this->db->select('users.userId,CONCAT(name ," ", lastName) AS completeName,userName');
            $this->db->join('userPersonalData', 'userPersonalData.userId = eventLinks.userId');
            $this->db->join('users', 'users.userId = eventLinks.userId');
            $result = $this->db->get_where('eventLinks',array('eventId' => $eventId))->result();
            if(isset($result))
            {
                return $result;
            }
        }

        public function getEventTables($tableNames,$eventId)
        {
                //esta funcion trae los resultados de las tablas anexas a events por ej:(eventLinks,etc)
            $cTableNames = count($tableNames);
            for ($i=0; $i < $cTableNames; $i++) { 
                $eTables[] = $this->db->get_where($tableName[$i],array('eventId' => $eventId))->result();    
            }
            return $eTables;
        }

        function GetReportData($eventId,$option)
        {
            $result = new StdClass();
            if ($option == 'viewed') {
                $this->db->distinct();
                $this->db->select('users.userId,userPersonalData.name,lastName,sections.name as section,turn');
                $this->db->join('userPersonalData', 'userPersonalData.userId = users.userId');
                $this->db->join('userComplementaryData', 'userComplementaryData.userId = users.userId');
                $this->db->join('sections', 'sections.sectionId = users.sectionId');
                $this->db->join('eventLinks', 'eventLinks.userId = users.userId');
                $this->db->join('eventViews', 'eventViews.userId = users.userId');
                $this->db->join('eventReminders', 'eventReminders.eventReminderId = eventViews.eventReminderId');
                $result = $this->db->get_where('users',array('eventLinks.eventId' => $eventId,'eventReminders.eventId' => $eventId))->result();
            }
            else 
            {
                $query= 
                "SELECT up.name,lastName,s.name as section,turn FROM eventLinks el
                JOIN users u ON u.userId = el.userId
                JOIN userPersonalData up ON up.userId = u.userId
                JOIN userComplementaryData uc ON uc.userId = u.userId
                JOIN sections s ON s.sectionId = u.sectionId
                WHERE el.eventId = '$eventId'
                AND el.userId NOT IN (SELECT distinct ev.userId FROM eventViews ev JOIN eventLinks er ON er.userId = ev.userId WHERE eventId = $eventId)
                AND active = 1";
                $result = $this->db->query($query)->result();
            }
            echo json_encode($result);            
        }

        public function checkView($eventReminderId)
        {    
            $objectInsert 		= array(
                'timestamp' 		=> time(),
                'userId' 			=> $this->session->UserId,
                'eventReminderId' 	=> $eventReminderId,
                );
            $this->db->insert('eventViews', $objectInsert);

            return $this->db->affected_rows();
        }

        public function Create($event,$users,$eventReminders)
        {
            $this->db->insert('events', $event);
            $eventId = $this->db->insert_id();
            if($this->InsertEventLinks($eventId , $users))
            {
                $cEventR = count($eventReminders);
                for ($i=0; $i < $cEventR; $i++) {
                    $objectInsert = array(
                        'eventId'       => $eventId,
                        'dateReminder'  => $eventReminders[$i]
                        );
                    if($i==0)
                    {
                        $objectInsert['main'] = true;
                    } 
                    $this->db->insert('eventReminders', $objectInsert);
                }
                $this->Notification->funcMessage('event_created');
            }

        }

        public function Edit($event,$users,$eventReminders)
        {   $mainEdited = 0;
            if(isset($event))
            {   
                $this->db->delete('eventLinks',array('eventId' => $event['eventId']));
                if($this->InsertEventLinks($event['eventId'] , $users))
                {
                    $actualReminders = $this->getEventReminders($event['eventId'],time(date('d-m-Y')));

                    if (count($actualReminders)>0){
                        $this->db->where_in("eventReminderId",array_column($actualReminders,'eventReminderId'))->delete('eventReminders');
                        $mainStatus = array_column($actualReminders,'main');
                        foreach ($mainStatus as $main) {
                            if ($main == 1){
                                $mainEdited = 1;
                            }
                        }
                    }

                    $cEventR = count($eventReminders);
                    for ($i=0; $i < $cEventR; $i++) {
                        $data = array(
                            'eventId'         => $event['eventId'],
                            'main'            => $mainEdited,
                            'dateReminder'    => $eventReminders[$i]
                            );
                        $this->db->insert('eventReminders',$data);
                        $mainEdited = 0;

                    }   

                }

                $this->db->where('eventid', $event['eventId']);
                $this->db->update('events', $event);
                $this->Notification->funcMessage('event_edited');
            }

        }

        public function getEventReminders($eventId,$startDate)
        {   
            $eventReminders = $this->db->select('*')
            ->where(array('eventId'=>$eventId,'dateReminder >' => $startDate))
            ->get('eventReminders');

            return $eventReminders->result_array();
        }


        public function InsertEventLinks($eventId,$users)
        {
            foreach ($users as $user) {
                $exist = $this->db->get_where('users',array('userId' => $user))->row();
                if(isset($exist))
                {
                    $exist = $this->db->get_where('eventLinks',array('userId' => $user,'eventId'=>$eventId))->row();
                    if(!isset($exist))
                    {
                        $objectInsert = array(
                            'userId'    => $user,
                            'eventId'   => $eventId
                            );
                        $this->db->insert('eventLinks', $objectInsert);
                    }
                }
            }
            return true;
        }

        public function Delete($eventId,$notification=FALSE)
        {
            $eventReminders = $this->db->select('eventReminderId')->get_where('eventReminders',array('eventId' => $eventId))->result();
            $eventLinks     = $this->db->select('eventLinkId')->get_where('eventLinks',array('eventId' => $eventId))->result();

            if (isset($eventReminders)) 
            {
                foreach ($eventReminders as $reminderId) {
                    $eventViews = $this->db->select('eventViewId')->get_where('eventViews',array('eventReminderId' => $reminderId->eventReminderId))->result();
                    if (isset($eventViews)) 
                    {
                        foreach ($eventViews as $eventViewid) {
                            $this->db->delete('eventViews',array('eventViewId' => $eventViewid->eventViewId));
                        }
                    }
                    else {
                        return 'invalid';
                    }
                    $this->db->delete('eventReminders',array('eventReminderId' => $reminderId->eventReminderId));
                }
            }
            else {
                return 'invalid';
            }

            if (isset($eventLinks)) 
            {
                foreach ($eventLinks as $id) {
                    $this->db->delete('eventLinks',array('eventLinkId' => $id->eventLinkId));
                }
            }
            else {
                return 'invalid';
            }
            $image = $this->db->select('image')->get_where('events',array('eventId' => $eventId))->row();

            if ($image->image != NULL) {
                unlink('./catapp/_notifications_images/'.$image->image); 
            }
            $this->db->delete('events',array('eventId' => $eventId));
            if(!$notification)
                $this->Notification->funcMessage('event_deletemessage');
        }     
}
?>