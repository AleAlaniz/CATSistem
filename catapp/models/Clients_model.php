<?php
Class Clients_model extends CI_Model {

	function Getclients(){
		$data = new StdClass();
		$sql = "SELECT * FROM clients ORDER BY name";
		$data->model = $this->db->query($sql)->result();
		$this->load->view('_shared/_administrationlayoutheader');
		$this->load->view('clients/clients', $data);
		$this->load->view(PRELAYOUTFOOTER);
	}

	function Create(){

		$this->form_validation->set_rules('name', 'lang:clients_name', 'required|is_unique[clients.name]',
			array(
				'is_unique'     => $this->lang->line('clients_error_exist')
				));

		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('_shared/_administrationlayoutheader');
			$this->load->view('clients/create');
			$this->load->view(PRELAYOUTFOOTER);
		}
		else
		{
			$rowInsert = array(
				'name' => $this->input->post('name'),
				'timestamp' => time(),
				'userId' => $this->session->UserId,
				);

			$this->db->insert('clients', $rowInsert);
			$this->session->set_flashdata('flashMessage', 'create');
			header('Location:/'.FOLDERADD.'/clients');
		}
	}

	function Delete(){
		if ($this->uri->segment(3)) {
			$sql = "SELECT * FROM clients WHERE clientId = ?";

			$query = $this->db->query($sql,$this->uri->segment(3))->row();

			if (isset($query))
			{
				if ($this->input->post('clientId') && $this->input->post('clientId') == $query->clientId)
				{
					$rowDelete = array(
						'clientId' => $this->input->post('clientId')
						);

					insert_audit_logs('clients','DELETE',$query);

					$this->db->delete('clients', $rowDelete);
					$this->session->set_flashdata('flashMessage', 'delete');
					header('Location:/'.FOLDERADD.'/clients');
				}
				else
				{
					$query->navBar = $this->load->view('clients/_navbar', $query, TRUE);
					$this->load->view('_shared/_administrationlayoutheader');
					$this->load->view('clients/delete', $query);
					$this->load->view(PRELAYOUTFOOTER);
				}
			}
			else
			{
				show_404();
			}
		}
		else{
			show_404();
		}
	}


	function Edit(){
		if ($this->uri->segment(3))
		{
			$sql = "SELECT * FROM clients WHERE clientId = ?";
			$query = $this->db->query($sql,$this->uri->segment(3))->row();
			
			if (isset($query))
			{
				$nameRules = 'required|is_unique[clients.name]';
				$nameRulesMessages = array('is_unique' => $this->lang->line('client_error_exist'));

				if ($this->input->post('name') && $query->name == $this->input->post('name')) {
					$nameRules = 'required';
					$nameRulesMessages = NULL;
				}

				$this->form_validation->set_rules('name', 'lang:client_name', $nameRules ,$nameRulesMessages);

				if ($this->form_validation->run() == FALSE)
				{
					$query->navBar = $this->load->view('clients/_navbar', $query, TRUE);
					$data = array();
				
					$this->load->view('_shared/_administrationlayoutheader');
					$this->load->view('clients/edit',$query);
					$this->load->view(PRELAYOUTFOOTER);
				}
				else
				{
					$objectEdit = array(
						'name' => $this->input->post('name')
						);

					insert_audit_logs('items','UPDATE',$query);
					
					$this->db->where('clientId', $query->clientId);
					$this->db->update('clients', $objectEdit);
					$this->session->set_flashdata('flashMessage', 'edit');
					header('Location:/'.FOLDERADD.'/clients');
				}
			}
			else
			{
				show_404();
			}
		}
		else{
			show_404();
		}
	}
}
