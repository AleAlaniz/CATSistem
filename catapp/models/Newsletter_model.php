<?php

Class Newsletter_model extends CI_Model {


	function GetNewsletter(){
		$sql = "SELECT * FROM newsletterCategories ORDER BY name";
		$categories = $this->db->query($sql)->result();
		$permission_edit = $this->Identity->Validate('newsletter/edit');
		$objects = array();
		foreach ($categories as $category) {
			$category->noRead = 0;
			if ($permission_edit) {
				$sql = "SELECT n.title,n.color, n.newsletterId, n.newsletter, n.file, (SELECT count(*) FROM newsletterViews AS nv WHERE nv.newsletterId = n.newsletterId && userId = ? ) AS isView
				FROM newsletters AS n WHERE n.newslettercategoryId = ? ORDER BY n.timestamp DESC";
				$category->newsletters = $this->db->query($sql, array($this->session->UserId,$category->newslettercategoryId))->result();

				foreach ($category->newsletters as $nkey => $new) {
					$sql = "SELECT * FROM newsletterLinks WHERE newsletterId = ?";
					$links = $this->db->query($sql, $new->newsletterId)->result();
					$new->linksName = '';
					foreach ($links as $link) {
						if ($link->sectionId != NULL) {
							$sql = "SELECT name FROM sections WHERE sectionId = ?";
							$tempVar = $this->db->query($sql, $link->sectionId)->row();
						}
						elseif ($link->roleId != NULL) {
							$sql = "SELECT name FROM roles WHERE roleId = ?";
							$tempVar = $this->db->query($sql, $link->roleId)->row();
						}
						elseif ($link->siteId != NULL) {
							$sql = "SELECT name FROM sites WHERE siteId = ?";
							$tempVar = $this->db->query($sql, $link->siteId)->row();
						}
						elseif ($link->userlinkId != NULL) {
							$sql = "SELECT name,lastName FROM userPersonalData WHERE userId = ?";
							$tempVar = $this->db->query($sql, $link->userlinkId)->row();
							$tempVar->name .= ' '. $tempVar->lastName; 
						}
						if (!isset($tempVar))
						{
							$tempVar = new stdClass();
							$tempVar->name = '';
						}

						if ($new->linksName == '' ) {
							$new->linksName = $tempVar->name;
						}
						else
						{
							$new->linksName .= ' - '.$tempVar->name;
						}
					}
					if ($new->linksName == '') {
						$new->linksName = $this->lang->line('newsletter_emptyLinks');
					}
					if ($new->isView <= 0) {
						$category->noRead++;
						$objectInsert = array(
							'timestamp' => time(),
							'userId' => $this->session->UserId,
							'newsletterId' => $new->newsletterId
						);
						$objects[] = $objectInsert;
					}
				}
				if(count($objects)>0)
				{
					$this->db->insert_batch('newsletterViews', $objects);
				}
			}
			else
			{
				$sql = "SELECT n.title,n.color,  n.newsletterId, n.newsletter, n.file, (SELECT count(*) FROM newsletterViews AS nv WHERE nv.newsletterId = n.newsletterId && userId = ? ) AS isView
				FROM newsletterLinks AS nl INNER JOIN newsletters AS n ON nl.newsletterId = n.newsletterId 
				WHERE (nl.sectionId = ? || nl.roleId = ? || nl.siteId = ? || nl.userlinkId = ?) && newslettercategoryId = ? 
				ORDER BY n.timestamp DESC";
				$category->newsletters = $this->db->query($sql, array($this->session->UserId, $this->session->sectionId,$this->session->roleId, $this->session->siteId, $this->session->UserId, $category->newslettercategoryId))->result();
				foreach ($category->newsletters as $new) {
					if ($new->isView <= 0) {
						
						$category->noRead++;
						$objectInsert = array(
							'timestamp' => time(),
							'userId' => $this->session->UserId,
							'newsletterId' => $new->newsletterId
						);
						$objects[] = $objectInsert;
					}
				}
				if(count($objects)>0)
				{
					$this->db->insert_batch('newsletterViews', $objects);
				}
			}
		}
		echo escapeJsonString(json_encode($categories));
	}

	function CreateCategory(){
		$this->form_validation->set_rules('name', 'lang:newsletter_name', 'required');
		$this->form_validation->set_rules('color', 'lang:newsletter_color', 'required|in_list[info,primary,warning,danger,success]');

		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('newsletter/createcategory');
		}
		else
		{
			$objectInsert = array(
				'name' => $this->input->post('name'),
				'color' => $this->input->post('color'),
				'userId' => $this->session->UserId,
				'timestamp' => time()
			);
			$this->db->insert('newsletterCategories', $objectInsert);
			echo $this->lang->line('newsletter_categorysuccessmessage');
		}
	}

	function DeleteCategory(){
		if ($this->uri->segment(3)) {
			$sql = "SELECT newslettercategoryId FROM newsletterCategories WHERE newslettercategoryId = ?";
			$query = $this->db->query($sql,$this->uri->segment(3))->row();
			if (isset($query))
			{
				$objectDelete = array(
					'newslettercategoryId' => $query->newslettercategoryId
				);
				$this->db->delete('newsletterCategories', $objectDelete);
				echo $this->lang->line('newsletter_categorydeletemessage');;
			}
			else
			{
				echo "invalid";
			}
		}
		else{
			echo "invalid";
		}
	}

	function EditCategory(){
		if ($this->uri->segment(3)) {
			$sql = "SELECT newslettercategoryId FROM newsletterCategories WHERE newslettercategoryId = ?";
			$query = $this->db->query($sql,$this->uri->segment(3))->row();
			if (isset($query))
			{
				$this->form_validation->set_rules('name', 'lang:newsletter_name', 'required');
				$this->form_validation->set_rules('color', 'lang:newsletter_color', 'required|in_list[info,primary,warning,danger,success]');
				if ($this->form_validation->run() == FALSE)
				{
					$this->load->view('newsletter/editcategory');
				}
				else
				{
					$objectEdit = array(
						'name' => $this->input->post('name'),
						'color' => $this->input->post('color')
					);
					$this->db->where('newslettercategoryId', $query->newslettercategoryId);
					$this->db->update('newsletterCategories', $objectEdit);
					echo $this->lang->line('newsletter_categoryeditmessage');;
				}
			}
			else
			{
				echo "invalid";
			}
		}
		else{
			echo "invalid";
		}
	}

	function GetCategory(){
		if ($this->uri->segment(3)) {
			$sql = "SELECT * FROM newsletterCategories WHERE newslettercategoryId = ?";
			$query = $this->db->query($sql,$this->uri->segment(3))->row();
			if (isset($query))
			{
				echo escapeJsonString(json_encode($query));
			}
			else
			{
				echo "invalid";
			}
		}
		else{
			echo "invalid";
		}
	}

	function Create(){
		$Sql = "SELECT * FROM newsletterCategories";
		$Query = $this->db->query($Sql)->result();
		$Ids = '';
		foreach ($Query as $value) {
			$Ids .= $value->newslettercategoryId.','; 
		}
		$this->form_validation->set_rules('category', 'lang:newsletter_category','required|in_list['.$Ids.']',
			array('in_list' => $this->lang->line('newsletter_categoryexist')));	
		
		$this->form_validation->set_rules('title', 'lang:newsletter_ntitle', 'required');
		$this->form_validation->set_rules('newsletter', 'lang:newsletter_newsletter', 'required');
		$this->form_validation->set_rules('color', 'lang:newsletter_color', 'in_list[info,primary,warning,danger,success,bordo]');


		if (!$this->input->post('roles[]') && !$this->input->post('sections[]') && !$this->input->post('sites[]') && !$this->input->post('userGroups[]') && !$this->input->post('users[]')) {
			$this->form_validation->set_rules('sections[]', 'lang:newsletter_link', 'required');
		}

		if ($this->form_validation->run() == FALSE)
		{
			$data = new stdClass();
			$sql = "SELECT name,sectionId FROM sections ORDER BY name";
			$data->sections = $this->db->query($sql)->result();
			$sql = "SELECT * FROM newsletterCategories ORDER BY name";
			$data->categories = $this->db->query($sql)->result();
			$sql = "SELECT name,siteId FROM sites ORDER BY name";
			$data->sites = $this->db->query($sql)->result();
			$sql = "SELECT name,roleId FROM roles ORDER BY name";
			$data->roles = $this->db->query($sql)->result();
			$sql = "SELECT * FROM userGroups WHERE userId = ? ORDER BY name";
			$data->userGroups = $this->db->query($sql, $this->session->UserId)->result();
			$data->error = '';

			$this->load->view('newsletter/create', $data);
		}
		else
		{
			$timestamp = time();

			$config['upload_path']          = './catapp/_docs/';
			$config['allowed_types']        = 'gif|jpg|png|pdf|doc|docx|pptx|ppt|xlsx|xls|txt|html';
			$config['max_size']             = 50480;

			if (isset($_FILES['userfile']) && $_FILES['userfile'] != NULL) {
				$config['file_name']			= $timestamp.'-'. $_FILES['userfile']['name'];
			}

			$this->load->library('upload', $config);

			$this->load->model('Home_model','Home');

			$resizedImagesNewsletter = $this->Home->resizeImages($this->input->post('newsletter'),1024,768);
			
			$objectInsert = array(
				'title' => $this->input->post('title'),
				'color' => $this->input->post('color'),
				'newsletter' => $resizedImagesNewsletter,
				'newslettercategoryId' => $this->input->post('category'),
				'userId' => $this->session->UserId,
				'timestamp' => $timestamp,
				'file' 		=> ( ( !$this->upload->do_upload('userfile')) ? NULL : $this->upload->data('file_name') )
			);

			$this->db->insert('newsletters', $objectInsert);
		
			$sql = "SELECT * FROM newsletters WHERE title = ? && newsletter = ? && newslettercategoryId = ? && userId = ? && timestamp = ? ";
			$query = $this->db->query($sql, array($this->input->post('title'),$resizedImagesNewsletter,$this->input->post('category'), $this->session->UserId, $timestamp))->row();

			if (isset($query) && $query != NULL && $query != '')
			{

				$sections 	= $this->input->post('sections[]');
				$roles 		= $this->input->post('roles[]');
				$sites 		= $this->input->post('sites[]');
				$userGroups = $this->input->post('userGroups[]');
				$users 		= $this->input->post('users[]');

				if($sections)
				{
					foreach ($sections as $section) {
						$sql 	= "SELECT name FROM sections WHERE sectionId = ?";
						$exist 	= $this->db->query($sql, $section)->row();
						if (isset($exist)) {
							$sql 	= "SELECT newsletterId FROM newsletterLinks WHERE newsletterId = ? && sectionId = ?";
							$exist 	= $this->db->query($sql, array($query->newsletterId, $section))->row();
							if (!isset($exist)) {
								$objectInsert = array(
									'newsletterId' 	=> $query->newsletterId,
									'sectionId' 	=> $section,
									'userId' 		=> $this->session->UserId,
									'timestamp' 	=> $timestamp,
								);
								$this->db->insert('newsletterLinks', $objectInsert);
							}
						}
					}
				}

				if($roles)
				{
					foreach ($roles as $role) {
						$sql 	= "SELECT name FROM roles WHERE roleId = ?";
						$exist 	= $this->db->query($sql, $role)->row();
						if (isset($exist)) {
							$sql 	= "SELECT newsletterId FROM newsletterLinks WHERE newsletterId = ? && roleId = ?";
							$exist 	= $this->db->query($sql, array($query->newsletterId, $role))->row();
							if (!isset($exist)) {
								$objectInsert 		= array(
									'newsletterId' 	=> $query->newsletterId,
									'roleId' 		=> $role,
									'userId' 		=> $this->session->UserId,
									'timestamp' 	=> $timestamp,
								);
								$this->db->insert('newsletterLinks', $objectInsert);
							}
						}
					}
				}

				if($sites)
				{
					foreach ($sites as $site) {
						$sql 	= "SELECT name FROM sites WHERE siteId = ?";
						$exist 	= $this->db->query($sql, $site)->row();
						if (isset($exist)) {
							$sql 	= "SELECT newsletterId FROM newsletterLinks WHERE newsletterId = ? && siteId = ?";
							$exist 	= $this->db->query($sql, array($query->newsletterId, $site))->row();
							if (!isset($exist)) {
								$objectInsert 		= array(
									'newsletterId' 	=> $query->newsletterId,
									'siteId' 		=> $site,
									'userId' 		=> $this->session->UserId,
									'timestamp' 	=> $timestamp,
								);
								$this->db->insert('newsletterLinks', $objectInsert);
							}
						}
					}
				}

				if($userGroups && $this->Identity->Validate('usergroups/create'))
				{
					foreach ($userGroups as $userGroup) {
						$sql 	= "SELECT * FROM userGroups WHERE usergroupId = ? && userId = ?";
						$exist 	= $this->db->query($sql, array($userGroup, $this->session->UserId))->row();
						if (isset($exist)) {
							$sql 	= "SELECT * FROM usergroupUsers WHERE usergroupId = ?";
							$group 	= $this->db->query($sql, $userGroup)->result();
							if (isset($group) && count($group) > 0) {
								foreach ($group as $user) {
									$sql 	= "SELECT newsletterId FROM newsletterLinks WHERE newsletterId = ? && userId = ?";
									$exist 	= $this->db->query($sql, array($query->newsletterId, $user->userId))->row();
									if (!isset($exist)) {
										$objectInsert = array(
											'newsletterId' 	=> $query->newsletterId,
											'userlinkId' 	=> $user->userId,
											'userId' 		=> $this->session->UserId,
											'timestamp' 	=> $timestamp,
										);
										$this->db->insert('newsletterLinks', $objectInsert);
									}
								}
							}
						}
					}
				}

				if ($users) {
					foreach ($users as $user) {
						$sql 	= "SELECT userId FROM users WHERE userId = ?";
						$exist 	= $this->db->query($sql, $user)->row();
						if (isset($exist)) {
							$sql 	= "SELECT newsletterId FROM newsletterLinks WHERE newsletterId = ? && userId = ?";
							$exist 	= $this->db->query($sql, array($query->newsletterId, $user))->row();
							if (!isset($exist)) {
								$objectInsert 		= array(
									'newsletterId' 	=> $query->newsletterId,
									'userlinkId' 	=> $user,
									'userId' 		=> $this->session->UserId,
									'timestamp' 	=> $timestamp,
								);
								$this->db->insert('newsletterLinks', $objectInsert);
							}
						}
					}
				}

			}

			$response = new StdClass();
			$response->status = 'ok';
			$response->message = $this->lang->line('newsletter_newscreatemessage');
			$response->to 	= array(
				'sections' 	=> array(),
				'sites' 	=> array(),
				'roles' 	=> array(),
				'users' 	=> array(),
			);
			$sql 	= "SELECT * FROM newsletterLinks WHERE newsletterId = ?";
			$to 	= $this->db->query($sql, $query->newsletterId)->result();
			foreach ($to as $link) {
				if ($link->sectionId != NULL) {
					$response->to['sections'][] = $link->sectionId; 
				}
				if ($link->roleId != NULL) {
					$response->to['roles'][] = $link->roleId; 
				}
				if ($link->siteId != NULL) {
					$response->to['sites'][] = $link->siteId; 
				}
				if ($link->userId != NULL) {
					$response->to['users'][] = $link->userlinkId; 
				}
			}

			echo escapeJsonString($response, FALSE);
		}
	}

	function Delete(){
		if ($this->uri->segment(3)) {
			$sql = "SELECT newsletterId, file FROM newsletters WHERE newsletterId = ?";
			$query = $this->db->query($sql,$this->uri->segment(3))->row();
			if (isset($query))
			{
				$objectDelete = array(
					'newsletterId' => $query->newsletterId
				);

				$this->db->delete('newsletters', $objectDelete);
				if ($query->file != NULL) {
					unlink('./catapp/_docs/'.$query->file); 
				}
				echo $this->lang->line('newsletter_newsdeletemessage');;
				
			}
			else
			{
				echo "invalid";
			}
		}
		else{
			echo "invalid";
		}
	}

	function Edit()
	{
		if ($this->uri->segment(3)) {
			$sql = "SELECT * FROM newsletters WHERE newsletterId = ?";
			$data = $this->db->query($sql,$this->uri->segment(3))->row();
			if (isset($data))
			{
				$Sql = "SELECT * FROM newsletterCategories";
				$Query = $this->db->query($Sql)->result();
				$Ids = '';
				foreach ($Query as $value) {
					$Ids .= $value->newslettercategoryId.','; 
				}
				$this->form_validation->set_rules('category', 'lang:newsletter_category','required|in_list['.$Ids.']',
					array('in_list' => $this->lang->line('newsletter_categoryexist')));	

				$this->form_validation->set_rules('title', 'lang:newsletter_ntitle', 'required');
				$this->form_validation->set_rules('newsletter', 'lang:newsletter_newsletter', 'required');
				$this->form_validation->set_rules('color', 'lang:newsletter_color', 'in_list[info,primary,warning,danger,success]');

				if (!$this->input->post('roles[]') && !$this->input->post('sections[]') && !$this->input->post('sites[]') && !$this->input->post('userGroups[]') && !$this->input->post('users[]')) {
					$this->form_validation->set_rules('sections[]', 'lang:newsletter_link', 'required');
				}

				if ($this->form_validation->run() == FALSE)
				{
					$sql = "SELECT name,sectionId FROM sections ORDER BY name";
					$data->sections = $this->db->query($sql)->result();
					$sql = "SELECT * FROM newsletterCategories ORDER BY name";
					$data->categories = $this->db->query($sql)->result();
					$sql = "SELECT name,siteId FROM sites ORDER BY name";
					$data->sites = $this->db->query($sql)->result();
					$sql = "SELECT name,roleId FROM roles ORDER BY name";
					$data->roles = $this->db->query($sql)->result();
					$sql = "SELECT * FROM userGroups WHERE userId = ? ORDER BY name";
					$data->userGroups = $this->db->query($sql, $this->session->UserId)->result();
					$this->load->view('newsletter/edit', $data);
				}
				else
				{
					$timestamp = time();

					if ($data->file != NULL) {
						unlink('./catapp/_docs/'.$data->file); 
					}

					$config['upload_path']          = './catapp/_docs/';
					$config['allowed_types']        = 'gif|jpg|png|pdf|doc|docx|pptx|ppt|xlsx|xls|txt|html';
					$config['max_size']             = 50480;
					if (isset($_FILES['userfile']) && $_FILES['userfile'] != NULL) {
						$config['file_name']			= $timestamp.'-'. $_FILES['userfile']['name'];
					}
					$this->load->library('upload', $config);

					$objectEdit = array(
						'newsletter' => $this->input->post('newsletter'),
						'newslettercategoryId' => $this->input->post('category'),
						'title' => $this->input->post('title'),
						'color' => $this->input->post('color'),
						'file'  => ( (!$this->upload->do_upload('userfile')) ? NULL : $this->upload->data('file_name') )
					);

					$this->db->where('newsletterId', $data->newsletterId);
					$this->db->update('newsletters', $objectEdit);

					$sections 	= $this->input->post('sections[]');
					$roles 		= $this->input->post('roles[]');
					$sites 		= $this->input->post('sites[]');
					$userGroups = $this->input->post('userGroups[]');
					$users 		= $this->input->post('users[]');

					$objectDelete = array(
						'newsletterId' => $data->newsletterId
					);
					$this->db->delete('newsletterLinks', $objectDelete);

					if($sections)
					{
						foreach ($sections as $section) {
							$sql 	= "SELECT name FROM sections WHERE sectionId = ?";
							$exist 	= $this->db->query($sql, $section)->row();
							if (isset($exist)) {
								$sql 	= "SELECT newsletterId FROM newsletterLinks WHERE newsletterId = ? && sectionId = ?";
								$exist 	= $this->db->query($sql, array($data->newsletterId, $section))->row();
								if (!isset($exist)) {
									$objectInsert = array(
										'newsletterId' 	=> $data->newsletterId,
										'sectionId' 	=> $section,
										'userId' 		=> $this->session->UserId,
										'timestamp' 	=> $timestamp,
									);
									$this->db->insert('newsletterLinks', $objectInsert);
								}
							}
						}
					}

					if($roles)
					{
						foreach ($roles as $role) {
							$sql 	= "SELECT name FROM roles WHERE roleId = ?";
							$exist 	= $this->db->query($sql, $role)->row();
							if (isset($exist)) {
								$sql 	= "SELECT newsletterId FROM newsletterLinks WHERE newsletterId = ? && roleId = ?";
								$exist 	= $this->db->query($sql, array($data->newsletterId, $role))->row();
								if (!isset($exist)) {
									$objectInsert 		= array(
										'newsletterId' 	=> $data->newsletterId,
										'roleId' 		=> $role,
										'userId' 		=> $this->session->UserId,
										'timestamp' 	=> $timestamp,
									);
									$this->db->insert('newsletterLinks', $objectInsert);
								}
							}
						}
					}

					if($sites)
					{
						foreach ($sites as $site) {
							$sql 	= "SELECT name FROM sites WHERE siteId = ?";
							$exist 	= $this->db->query($sql, $site)->row();
							if (isset($exist)) {
								$sql 	= "SELECT newsletterId FROM newsletterLinks WHERE newsletterId = ? && siteId = ?";
								$exist 	= $this->db->query($sql, array($data->newsletterId, $site))->row();
								if (!isset($exist)) {
									$objectInsert 		= array(
										'newsletterId' 	=> $data->newsletterId,
										'siteId' 		=> $site,
										'userId' 		=> $this->session->UserId,
										'timestamp' 	=> $timestamp,
									);
									$this->db->insert('newsletterLinks', $objectInsert);
								}
							}
						}
					}

					if($userGroups && $this->Identity->Validate('usergroups/create'))
					{
						foreach ($userGroups as $userGroup) {
							$sql 	= "SELECT * FROM userGroups WHERE usergroupId = ? && userId = ?";
							$exist 	= $this->db->query($sql, array($userGroup, $this->session->UserId))->row();
							if (isset($exist)) {
								$sql 	= "SELECT * FROM usergroupUsers WHERE usergroupId = ?";
								$group 	= $this->db->query($sql, $userGroup)->result();
								if (isset($group) && count($group) > 0) {
									foreach ($group as $user) {
										$sql 	= "SELECT newsletterId FROM newsletterLinks WHERE newsletterId = ? && userId = ?";
										$exist 	= $this->db->query($sql, array($data->newsletterId, $user->userId))->row();
										if (!isset($exist)) {
											$objectInsert = array(
												'newsletterId' 	=> $data->newsletterId,
												'userlinkId' 	=> $user->userId,
												'userId' 		=> $this->session->UserId,
												'timestamp' 	=> $timestamp,
											);
											$this->db->insert('newsletterLinks', $objectInsert);
										}
									}
								}
							}
						}
					}

					if ($users) {
						foreach ($users as $user) {
							$sql 	= "SELECT userId FROM users WHERE userId = ?";
							$exist 	= $this->db->query($sql, $user)->row();
							if (isset($exist)) {
								$sql 	= "SELECT newsletterId FROM newsletterLinks WHERE newsletterId = ? && userId = ?";
								$exist 	= $this->db->query($sql, array($data->newsletterId, $user))->row();
								if (!isset($exist)) {
									$objectInsert 		= array(
										'newsletterId' 	=> $data->newsletterId,
										'userlinkId' 	=> $user,
										'userId' 		=> $this->session->UserId,
										'timestamp' 	=> $timestamp,
									);
									$this->db->insert('newsletterLinks', $objectInsert);
								}
							}
						}
					}
					echo $this->lang->line('newsletter_newseditmessage');
				}
			}
			else
			{
				echo "invalid";
			}
		}
		else
		{
			echo "invalid";
		}
	}

	function GetNewsletterById(){
		if ($this->uri->segment(3)) {
			$sql = "SELECT * FROM newsletters WHERE newsletterId = ?";
			$data = $this->db->query($sql,$this->uri->segment(3))->row();
			if (isset($data))
			{
				$sql = "SELECT * FROM newsletterLinks WHERE newsletterId = ?";
				$data->links = $this->db->query($sql, $data->newsletterId)->result();

				foreach ($data->links as $link) {
					if ($link->sectionId != NULL) {
						$sql = "SELECT name FROM sections WHERE sectionId = ?";
						$tempVar = $this->db->query($sql, $link->sectionId)->row();
					}
					elseif ($link->roleId != NULL) {
						$sql = "SELECT name FROM roles WHERE roleId = ?";
						$tempVar = $this->db->query($sql, $link->roleId)->row();
					}
					elseif ($link->siteId != NULL) {
						$sql = "SELECT name FROM sites WHERE siteId = ?";
						$tempVar = $this->db->query($sql, $link->siteId)->row();
					}
					elseif ($link->userlinkId != NULL) {
						$sql = "SELECT up.name, up.lastName, u.userName FROM users AS u INNER JOIN (SELECT name, lastName, userId FROM userPersonalData WHERE userId = ?) AS up ON u.userId = up.userId WHERE u.userId = ?";
						$tempVar = $this->db->query($sql, array($link->userlinkId, $link->userlinkId))->row();
						$tempVar->name .= ' '. $tempVar->lastName; 
						$link->userName = $tempVar->userName;
					}
					if (!isset($tempVar))
					{
						$tempVar = new stdClass();
						$tempVar->name = '';
					}
					$link->name = $tempVar->name;
				}
				echo escapeJsonString(json_encode($data));;
			}
			else
			{
				echo "invalid";
			}
		}
		else{
			echo "invalid";
		}
	}

	function Download(){
		if ($this->uri->segment(3)) {

			if ($this->Identity->Validate('newsletter/edit')) {
				$sql = "SELECT * FROM newsletters WHERE newsletterId = ?";
				$query = $this->db->query($sql,$this->uri->segment(3))->row();
			}
			else
			{
				$sql = "SELECT n.*
				FROM newsletterLinks nl
				INNER JOIN newsletters n ON nl.newsletterId = n.newsletterId   
				WHERE (nl.sectionId = ? || nl.roleId = ? || nl.siteId = ? || nl.userlinkId = ?) && nl.newsletterId = ? 
				LIMIT 1";
				$query = $this->db->query($sql, array($this->session->sectionId,$this->session->roleId, $this->session->siteId, $this->session->UserId, $this->uri->segment(3)))->row();
			}

			if (isset($query) && $query->file != NULL)
			{
				$this->load->helper('download');
				$file = file_get_contents('./catapp/_docs/'.$query->file);
				$trozos = explode(".", $query->file); 
				$extension = end($trozos);

				force_download(addslashes($query->file), $file);  
			}
			else
			{
				echo json_encode($query);
				show_404();
			}
		}
		else
		{
			show_404();
		}
	}


	public function SearchUsers()
	{
		$likeuser = $this->db->escape_like_str($this->input->post('like'));

		if ($likeuser != '') {
			$sql = "SELECT concat_ws(' ', up.name, up.lastName) as completeName, u.userId, u.userName FROM users AS u INNER JOIN (SELECT name, lastName, userId FROM userPersonalData) AS up ON u.userId = up.userId WHERE concat_ws(' ', up.name, up.lastName) LIKE '%$likeuser%' || u.userName LIKE '%$likeuser%' ORDER BY completeName LIMIT 20";
			$query = $this->db->query($sql)->result();
			if (count($query) > 0) {
				echo escapeJsonString(json_encode($query));
			}
			else
			{
				echo "empty";
			}
		}
		else
		{
			echo "empty";
		}
	}

	public function GetUnread()
	{
		$unread = 0;
		if ($this->Identity->Validate('newsletter/edit')) {
			$sql = "SELECT count(newsletterId) AS c FROM newsletters WHERE NOT EXISTS (SELECT 1 FROM newsletterViews WHERE newsletterViews.newsletterId = newsletters.newsletterId && newsletterViews.userId = ?)";
			$unread = $this->db->query($sql, $this->session->UserId)->row();
			$unread = $unread->c;
		}
		else
		{
			$sql = "SELECT count(newsletterId) AS c FROM newsletterLinks WHERE (sectionId = ? || roleId = ? || siteId = ? || userlinkId = ?) &&
			NOT EXISTS (SELECT 1 FROM newsletterViews WHERE newsletterViews.newsletterId = newsletterLinks.newsletterId && newsletterViews.userId = ?)
			GROUP BY newsletterId";
			$unread = $this->db->query($sql, array($this->session->sectionId,$this->session->roleId, $this->session->siteId, $this->session->UserId, $this->session->UserId))->row();
			$unread = $unread->c;
		}

		return $unread;
	}
	
	public function GetNewsletterReport($id,$type)
	{
		$sql = "";

		if($type == 'read')
		{
			$sql = "SELECT DISTINCT up.userId, up.name, up.lastName, s.name AS section 
			FROM newsletterViews AS dv
			INNER JOIN (SELECT sectionId, userId FROM users) AS u ON dv.userId = u.userId 
			INNER JOIN (SELECT name, lastName, userId FROM userPersonalData) AS up ON dv.userId = up.userId 
			INNER JOIN sections AS s ON s.sectionId = u.sectionId 
			WHERE dv.newsletterId = ?
			ORDER BY up.name";

		}
		//else
		//{
		//	$sql = "SELECT up.name, up.lastName, s.name AS section
		//	FROM users u
		//	JOIN userPersonalData up ON u.userId = up.userId
		//	JOIN sections s ON u.sectionId = s.sectionId
		//	WHERE exists (SELECT 1 FROM corporatedoclinks cl WHERE cl.corporatedocId = ? AND (cl.userId = u.userId OR cl.roleId = u.roleId OR cl.sectionId = u.sectionId OR //cl.siteId = u.siteId))  
		//	AND NOT EXISTS (SELECT 1 FROM corporatedocviews cv WHERE cv.corporatedocId = ? AND cv.userId = u.userId)
		//	AND active = 1
		//	ORDER BY up.name";
		//
		//	$id = array($id,$id);
		//}

		return  $this->db->query($sql, $id)->result();
	}

}