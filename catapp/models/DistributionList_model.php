<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DistributionList extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    
    public function getDistributionLists()
    {
        $data = new stdClass();
        $sql = "SELECT name,sectionId FROM sections ORDER BY name";
        $data->sections = $this->db->query($sql)->result();
        $sql = "SELECT * FROM corporatedoccategories ORDER BY name";
        $data->categories = $this->db->query($sql)->result();
        $sql = "SELECT name,siteId FROM sites ORDER BY name";
        $data->sites = $this->db->query($sql)->result();
        $sql = "SELECT name,roleId FROM roles ORDER BY name";
        $data->roles = $this->db->query($sql)->result();
        $sql = "SELECT * FROM userGroups WHERE userId = ? ORDER BY name";
        $data->userGroups = $this->db->query($sql, $this->session->UserId)->result();
        $data->error = '';

        return $data;
    }
}
?>