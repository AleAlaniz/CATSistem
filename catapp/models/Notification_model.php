<?php
Class Notification_Model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Event_model','Event');
    }
    
    public function getNotifications()
    {   
        if ($this->Identity->Validate('notifications/manage')) 
        {
            $notifications = $this->db->get('notifications')->result();
        }
        return $notifications;
    }

    public function getNotification($id)
    {
        if ($this->Identity->Validate('notifications/manage')) 
        {
            $notification = $this->db->get_where('notifications',array('notificationId' =>$id))->row();
        }
        echo escapeJsonString(json_encode($notification));
    }

    public function Create($title)
    {
        $userId = $this->session->UserId;
        $todayDate = date('y-m-d');
        $notification = array(
            'title'         => $title,
            'creation_date' => $todayDate,
            'userId'        => $userId
            );
        $this->db->insert('notifications',$notification);
        
        $this->funcMessage('notifications_createmessage');
    }

    public function Edit($notification)
    {
        if(isset($notification))
        {
            $this->db->set('title', "$notification->title");
            $this->db->where('notificationId', $notification->notificationId);
            $this->db->update('notifications');
        }
        $this->funcMessage('notifications_editmessage');
    }

    public function Delete($notificationId)
    {
        if(isset($notificationId))
        {
            
            $eventIds = $this->db->select('eventId')->get_where('events',array('notificationId' => $notificationId))->result();
            
            if(isset($eventIds))
            {
                foreach ($eventIds as $eventId) 
                {
                    $this->Event->Delete($eventId->eventId,TRUE);
                }
                
            }

            $this->db->delete('notifications',array('notificationId' => $notificationId));
            $this->funcMessage('notifications_deletemessage');
        }
        else{
            echo "invalid";
        }
    }

    public function funcMessage($message)
    {
        $response = new StdClass();
        $response->status = 'ok';
        $response->message = $this->lang->line($message);

        echo escapeJsonString($response, FALSE);
    }
}
?>