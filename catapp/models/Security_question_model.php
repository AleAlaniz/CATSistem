<?php

class Security_question_model extends CI_Model
{
	function create($question)
	{
		$data = array
		(
			'question' 	=> $question['question'],
			'userId' 	=> $this->session->UserId,
			'timestamp' => time()
		);

		$query = 
		"SELECT count(securityquestionId) as quantity
		FROM securityQuestions
		WHERE question = ? and active = 1";

		$quantity = $this->db->query($query, $question['question'])->row();

		if(isset($quantity) && $quantity->quantity > 0)
		{
			return array('state' => 'error', 'message' => $this->lang->line('question_question_exist'));
		}

		$this->db->insert('securityQuestions' ,$data);

		$state 	 = ($this->db->affected_rows() > 0) ? 'success' : 'error';
		$message = ($state == 'success') ? $this->lang->line('question_create_success') : $this->lang->line('question_error_general') ;

		return array('state' => $state, 'message' => $message);
	}

	function edit($question, $desactivate = FALSE)
	{
		$response=array('state'=>'error', 'message'=>$this->lang->line('question_error_general'));

		if(!$desactivate)
		{
			$query = 
			"SELECT count(securityquestionId) as quantity
			FROM securityQuestions
			WHERE question = ? and active = 1";

			$quantity = $this->db->query($query, $question['question'])->row();

			if(isset($quantity) && $quantity->quantity > 0)
			{
				$response['message'] = $this->lang->line('question_question_exist');
				return $response;
			}
		}

		$is_available = $this->is_modification_available($question['securityquestionId']);

		if($is_available['state'])
		{
			$this->db->where('securityquestionId', $question['securityquestionId']);

			$attributesToEdit = array
			(
				'timestamp' => time()
			);

			($desactivate) ? $attributesToEdit['active'] = 0 : $attributesToEdit['question'] = $question['question'] ;

			$this->db->update('securityQuestions', $attributesToEdit);

			if($this->db->affected_rows() != -1)
			{
				$response['state'] 	 = 'success';
				$response['message'] = ($desactivate) ? $this->lang->line('question_delete_success') : $this->lang->line('question_edit_success');
			}	
		}
		else
		{
			$response['message'] = $is_available['message'];
		}

		return $response;
	}

	private function is_modification_available($securityQuestionId)
	{	
		$response=array('state'=>false);

		$sql =
		'SELECT count(securityquestionId) as quantity
		FROM securityQuestions
		WHERE securityquestionId = ? AND active = 1';

		$result = $this->db->query($sql, $securityQuestionId)->row();

		if(isset($result) && $result->quantity > 0)
		{
			$sql = 
			'SELECT count(answersecurityquestionId) as quantity
			FROM  answersecurityQuestions
			WHERE securityquestionId = ?';

			$result = $this->db->query($sql, $securityQuestionId)->row();

			if(isset($result) && $result->quantity == 0)
			{
				$response['state'] = true;
			}
			else
			{
				$response['message'] = $this->lang->line('question_error_in_use');
			}
		}
		else
		{
			$response['message'] = $this->lang->line('question_error_no_available');

		}

		return $response;
	}

	function get_security_questions($withDetails = FALSE)
	{
		$columns = '';

		$sql = 
		'SELECT s.securityquestionId, s.question'.(($withDetails) ? ', u.name, u.lastName, (case when a.securityquestionId is NULL then 0 else 1 end) as inUse ' : ' ')
		.'FROM securityQuestions s join (SELECT userId, name, lastName FROM userPersonalData) u
		ON (s.userId = u.userId)'
		.(($withDetails) ? 'left JOIN answersecurityQuestions a
			ON (s.securityquestionId = a.answersecurityquestionId)' : ' ').
		'WHERE s.active = 1';

		return $this->db->query($sql)->result();
	}

	function valid_answer($answer)
	{
		$response = false;

		$sql = 
		'SELECT a.answer
		FROM answersecurityQuestions a JOIN securityQuestions s
		ON (a.securityquestionId = s.securityquestionId)
		JOIN users u
		ON (a.userId = u.userId)
		WHERE s.securityquestionId IS NOT NULL AND s.active = 1 AND s.securityquestionId = ? AND u.userName = ?';

		$result = $this->db->query($sql, array($answer['securityquestionId'], $answer['userName']))->row();

		return (isset($result) && $answer['answer'] == $result->answer);
	}

	function save_security_questions($securityQuestions)
	{
		$response=array('state'=>'error', 'message'=>$this->lang->line('question_error_general'));
		
		for($i = 0; $i < count($securityQuestions); $i++)
		{
			if($securityQuestions[$i]['securityquestionId'] > 0)
			{
				$sql =
				'SELECT COUNT(securityquestionId) as quantity
				FROM securityQuestions
				WHERE securityquestionId = ? AND active = 1';

				$result = $this->db->query($sql, $securityQuestions[$i]['securityquestionId'])->row();

				if(!isset($result) || $result->quantity == 0)
				{
					$response['message'] = $this->lang->line('question_question_no_exist');
					return $response;
				}
			}
		}

		for($i = 0; $i < count($securityQuestions); $i++)			
		{
			if(!isset($securityQuestions[$i]['answersecurityquestionId']))
			{
				$sql =
				'SELECT count(answersecurityquestionId) as quantity
				FROM answersecurityQuestions
				WHERE userId = ?';

				$result = $this->db->query($sql, $this->session->UserId)->row();

				if(isset($result) && $result->quantity < 3)
				{
					$data_to_insert =
					array
					(
						'timestamp'			=> time(),
						'securityquestionId'=> $securityQuestions[$i]['securityquestionId'],
						'userId' 			=> $this->session->UserId,
						'answer' 			=> $securityQuestions[$i]['answer']
					);

					$this->db->insert('answersecurityQuestions' , $data_to_insert);

					if($this->db->affected_rows() < 1)
					{
						return $response;
					}
				}
			}
			else
			{
				$sql =
				'SELECT COUNT(answersecurityquestionId) as quantity
				FROM answersecurityQuestions
				WHERE answersecurityquestionId = ? AND userId = ?';

				$result = $this->db->query($sql, array($securityQuestions[$i]['answersecurityquestionId'], $this->session->UserId))->row();

				if(!isset($result) || $result->quantity == 0)
				{
					$response['message'] = $this->lang->line('question_error_no_access');
					return $response;
				}

				$attributes_to_edit = array
				(
					'timestamp'			 => time(),
					'securityquestionId' => $securityQuestions[$i]['securityquestionId'],
					'answer' 			 => $securityQuestions[$i]['answer']
				);

				$this->db->where('answersecurityquestionId', $securityQuestions[$i]['answersecurityquestionId']);
				$this->db->update('answersecurityQuestions', $attributes_to_edit);

				if($this->db->affected_rows() == -1)
				{
					return $response;
				}
			}
		}

		$response['state'] = 'success';
		$response['message'] = $this->lang->line('questions_user_edit_success');

		return $response;
	}

	function get_security_questions_by_user()
	{
		$sql = 
		'SELECT a.answersecurityquestionId, s.securityquestionId, a.answer
		FROM securityQuestions s RIGHT JOIN answersecurityQuestions a
		ON (s.securityquestionId = a.securityquestionId)
		WHERE a.userId = ?';

		return $this->db->query($sql, $this->session->UserId)->result();
	}

	function get_security_question_by_id($securityQuestionId)
	{
		$sql = 
		'SELECT securityquestionId, question
		FROM securityQuestions
		WHERE securityquestionId = ?';

		return $this->db->query($sql, $securityQuestionId)->row();
	}

}

?>