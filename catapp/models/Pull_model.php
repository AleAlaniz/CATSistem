<?php

Class Pull_model extends CI_Model {

	var $maxpressnotes = 5;
	function ViewPress(){

		$data = new stdClass();

		$sql = "SELECT * FROM pressNotes ORDER BY pressnoteId DESC LIMIT ".$this->maxpressnotes;
		$data->notes = $this->GetNotes($sql);

		$sql = "SELECT * FROM pressClippings ORDER BY pressclippingId DESC LIMIT ".$this->maxpressnotes;
		$data->clipping = $this->GetClipping($sql);

		$this->load->view(PRELAYOUTHEADER);
		$this->load->view('press/press', $data);
		$this->load->view(PRELAYOUTFOOTER);
	}

	function Create(){

		$this->form_validation->set_rules('pressnote', 'lang:press_pressnote', 'required');

		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view(PRELAYOUTHEADER);
			$this->load->view('press/create');
			$this->load->view(PRELAYOUTFOOTER);
		}
		else
		{
			$objectInsert = array(
				'pressnote' => $this->input->post('pressnote'),
				'userId' => $this->session->UserId,
				'timestamp' => time()
				);
			$this->db->insert('pressNotes', $objectInsert);
			$this->session->set_flashdata('flashMessage', 'create');
			header('Location:/'.FOLDERADD.'/press');
		}
	}

	function CreateClipping(){

		$this->form_validation->set_rules('clipping', 'lang:press_clippingpress', 'required');
		$this->form_validation->set_rules('title', 'lang:press_notetitle', 'required');

		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view(PRELAYOUTHEADER);
			$this->load->view('press/createclipping');
			$this->load->view(PRELAYOUTFOOTER);
		}
		else
		{
			$objectInsert = array(
				'clipping' => $this->input->post('clipping'),
				'title' => $this->input->post('title'),
				'userId' => $this->session->UserId,
				'timestamp' => time()
				);
			$this->db->insert('pressClippings', $objectInsert);
			$this->session->set_flashdata('flashMessage', 'createclipping');
			header('Location:/'.FOLDERADD.'/press');
		}
	}

	function EditClipping(){
		if ($this->uri->segment(3)) {
			$sql = "SELECT * FROM pressClippings WHERE pressclippingId  = ?";
			$query = $this->db->query($sql,$this->uri->segment(3))->row();
			if (isset($query))
			{
				$this->form_validation->set_rules('clipping', 'lang:press_clippingpress', 'required');
				$this->form_validation->set_rules('title', 'lang:press_notetitle', 'required');

				if ($this->form_validation->run() == FALSE)
				{
					$this->load->view(PRELAYOUTHEADER);
					$this->load->view('press/editclipping', $query);
					$this->load->view(PRELAYOUTFOOTER);
				}
				else
				{
					$objectEdit = array(
						'clipping' => $this->input->post('clipping'),
						'title' => $this->input->post('title'),
						);
					$this->db->where('pressclippingId', $query->pressclippingId);
					$this->db->update('pressClippings', $objectEdit);
					$this->session->set_flashdata('flashMessage', 'editclipping');
					header('Location:/'.FOLDERADD.'/press');
				}
			}
			else
			{
				show_404();
			}
		}
		else{
			show_404();
		}
	}

	function DeleteClipping(){
		if ($this->uri->segment(3)) {
			$sql = "SELECT * FROM pressClippings WHERE pressclippingId  = ?";
			$query = $this->db->query($sql,$this->uri->segment(3))->row();
			if (isset($query))
			{
				$objectDelete = array(
					'pressclippingId' => $query->pressclippingId
					);
				$this->db->delete('pressClippings', $objectDelete);
				$this->session->set_flashdata('flashMessage', 'deleteclipping');
				header('Location:/'.FOLDERADD.'/press');
			}
			else
			{
				show_404();
			}
		}
		else{
			show_404();
		}
	}

	function Delete(){
		if ($this->uri->segment(3)) {
			$sql = "SELECT * FROM pressNotes WHERE pressnoteId  = ?";
			$query = $this->db->query($sql,$this->uri->segment(3))->row();
			if (isset($query))
			{
				$objectDelete = array(
					'pressnoteId' => $query->pressnoteId
					);
				$this->db->delete('pressNotes', $objectDelete);
				$this->session->set_flashdata('flashMessage', 'delete');
				header('Location:/'.FOLDERADD.'/press');
			}
			else
			{
				show_404();
			}
		}
		else{
			show_404();
		}
	}

	function Edit(){

		if ($this->uri->segment(3)) {
			$sql = "SELECT * FROM pressNotes WHERE pressnoteId  = ?";
			$query = $this->db->query($sql,$this->uri->segment(3))->row();
			if (isset($query))
			{

				$this->form_validation->set_rules('pressnote', 'lang:press_pressnote', 'required');

				if ($this->form_validation->run() == FALSE)
				{
					$this->load->view(PRELAYOUTHEADER);
					$this->load->view('press/edit', $query);
					$this->load->view(PRELAYOUTFOOTER);
				}
				else
				{
					$objectEdit = array(
						'pressnote' => $this->input->post('pressnote'),
						);

					$this->db->where('pressnoteId', $query->pressnoteId);
					$this->db->update('pressNotes', $objectEdit);
					$this->session->set_flashdata('flashMessage', 'edit');
					header('Location:/'.FOLDERADD.'/press');
				}
			}
			else
			{
				show_404();
			}
		}
		else{
			show_404();
		}
	}

	function GetOldPressNotes(){
		if ($this->input->post('lastpressNote')) {
			$sql = "SELECT * FROM pressNotes WHERE pressnoteId < ? ORDER BY pressnoteId DESC LIMIT 0,".$this->maxpressnotes;
			$query = $this->GetNotes($sql, array($this->input->post('lastpressNote')));
			if (count($query) > 0) {
				return json_encode($query);
			}
			else
			{	
				return 'empty';
			}
		}
		else
		{
			return 'invalid';
		}
	}

	function GetNotes($sql, $object = NULL)
	{
		$notes = $this->db->query($sql, $object)->result();

		foreach ($notes as $key => $note) {
			$sqlView = "SELECT * FROM pressnoteViews WHERE userId = ? && pressnoteId = ?" ;
			$isView = $this->db->query($sqlView, array($this->session->UserId, $note->pressnoteId))->row();	
			if ($isView == NULL) {
				$objectInsert = array(
					'timestamp' => time(),
					'userId' => $this->session->UserId,
					'pressnoteId' => $note->pressnoteId
					);
				$this->db->insert('pressnoteViews', $objectInsert);
			}
		}

		return $notes;
	}

	function GetClipping($sql, $object = NULL)
	{
		$clipping = $this->db->query($sql, $object)->result();

		foreach ($clipping as $key => $clip) {
			$sqlView = "SELECT * FROM pressclippingViews WHERE userId = ? && pressclippingId = ?" ;
			$isView = $this->db->query($sqlView, array($this->session->UserId, $clip->pressclippingId))->row();	
			if ($isView == NULL) {
				$objectInsert = array(
					'timestamp' => time(),
					'userId' => $this->session->UserId,
					'pressclippingId' => $clip->pressclippingId
					);
				$this->db->insert('pressclippingViews', $objectInsert);
			}
		}
		return $clipping;
	}

	function GetOldPressClipping(){
		if ($this->input->post('lastpressClipping')) {
			$sql = "SELECT * FROM pressClippings WHERE pressclippingId < ? ORDER BY pressclippingId DESC LIMIT 0,".$this->maxpressnotes;
			$query = $this->GetClipping($sql, array($this->input->post('lastpressClipping')));
			if (count($query) > 0) {
				return json_encode($query);
			}
			else
			{	
				return 'empty';
			}
		}
		else
		{
			return 'invalid';
		}
	}
}