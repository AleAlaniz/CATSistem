<?php
Class Doc_model extends CI_Model {
	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		$this->lang->load('login_lang');
	}

	var $itemdocId;
	var $name;
	var $userId;
	var $timestamp;
	var $itemId;
	var $comment;


	function GetDocs($query)
	{
		if($this->input->post('itemId'))
		{
			$sql = "SELECT * 
			FROM itemDocs
			WHERE itemId = ? && itemdocfolderId IS NULL ORDER BY timestamp DESC";

			$query->docs = $this->db->query($sql,$query->itemId)->result();

			$sql = "SELECT *
			FROM itemdocFolders
			WHERE itemId = ? ORDER BY name";

			$query->folders = $this->db->query($sql,$query->itemId)->result();


			for($i=0; count($query->folders) > $i; $i++)
			{ 
				$sqlFolder = "SELECT *
				FROM itemDocs
				WHERE itemId = ? && itemdocfolderId = ? ORDER BY timestamp DESC";

				$folderVar = array($query->itemId,$query->folders[$i]->itemdocfolderId);
				$query->folders[$i]->docs = $this->db->query($sqlFolder,$folderVar)->result();
			}

			$query->ActionView = $this->load->view('items/docs/_viewdocs_get', $query, TRUE);
			$query->partialView = $this->load->view('items/docs/_viewdocs', $query, TRUE);
			
			echo json_encode($query);
		}
		else
			echo "fail";
	}


	public function GetUploadDataView($query)
	{
		$sql="SELECT *
		FROM itemdocFolders
		WHERE itemId=?";

		$query->folders=$this->db->query($sql,$query->itemId)->result();

		$query->partialView = $this->load->view('items/docs/_viewdocs_create', $query, TRUE);

		echo json_encode($query);
	}

	public function GetEditDocView($query)
	{

		$sql="SELECT *
		FROM itemDocs
		where itemdocId=? and itemId=?";

		$params=array($this->input->post('itemdocId'),$query->itemId);

		$query->itemToEdit=$this->db->query($sql,$params)->row();

		$sql="SELECT *
		FROM itemdocFolders
		WHERE itemId=?";

		$query->folders=$this->db->query($sql,$query->itemId)->result();

		$query->partialView = $this->load->view('items/docs/_viewdocs_edit', $query, TRUE);

		echo json_encode($query);
	}

	public function GetCreateFolderView($query)
	{
		$query->partialView = $this->load->view('items/docs/_viewdocs_createfolder', $query, TRUE);

		echo json_encode($query);
	}

	public function GetEditFolderView($query)
	{	
		$sql="SELECT *
		FROM itemdocFolders
		where itemdocfolderId=?";

		$params=array($this->input->post('itemfolderId'));

		$query->itemToEdit=$this->db->query($sql,$params)->row();

		$query->partialView = $this->load->view('items/docs/_viewdocs_editfolder', $query, TRUE);

		echo json_encode($query);
	}

	function CreateDoc()
	{

		if($this->input->post('itemId'))
		{
			$res = new StdClass();
			$sql="SELECT *
			FROM items
			WHERE itemId=?";

			$query=$this->db->query($sql,$this->input->post('itemId'))->row();

			if(isset($query))
			{
				if ($this->input->post('folder'))
				{
					$foldersSql = "SELECT * FROM itemdocFolders WHERE itemId = ? ";

					$foldersQuery = $this->db->query($foldersSql,$query->itemId )->result();
					$folderIds = '';

				foreach ($foldersQuery as $value) //Este foreach no me convence
				{
					$folderIds .= $value->itemdocfolderId.','; 
				}

				$this->form_validation->set_rules('folder', 'lang:administration_items_doc_folder','in_list['.$folderIds.']',
					array('in_list' => $this->lang->line('administration_items_doc_folderexist')));	
			}

			$this->form_validation->set_rules('name', 'lang:administration_items_doc_name', 'required');
			if ($this->form_validation->run() == FALSE)
			{
				$res->status = 'fail';
				$res->errorMessage ="Falta agregar el nombre";
			}
			else
			{
				$config['upload_path']          = './catapp/_docs/';
				$config['allowed_types']        = 'gif|jpg|png|pdf|doc|docx|pptx|ppt|xlsx|xls|txt|ods|odt|html';
				$config['max_size']             = 50480;
				if (isset($_FILES['userfile']) && $_FILES['userfile'] != NULL) {
					$timestampFile = time();
					$config['file_name']			= $query->subsectionId.'-'.$query->itemId.'-'.$timestampFile.'-'. $_FILES['userfile']['name'];
				}
				$this->load->library('upload', $config);
				if ( ! $this->upload->do_upload('userfile'))
				{
					$res->status = 'fail';
					$res->errorMessage = $this->upload->display_errors();
				}
				else
				{
					if ($this->input->post('folder')) {
						$folderObject = $this->input->post('folder');
					}
					else{
						$folderObject = NULL;
					}
					$objecttoInsert = array(
						'name' => $this->input->post('name'),
						'timestamp' => $timestampFile,
						'userId' => $this->session->UserId,
						'itemId' => $query->itemId,
						'comment' => $this->input->post('comment'),
						'filename' => $this->upload->data('file_name'),
						'size' => $this->upload->data('file_size'), 
						'itemdocfolderId' => $folderObject,  
						);
					$this->db->insert('itemDocs', $objecttoInsert);

					$res->status = "success";
				}
			}
		}
		else
			$res->status = "fail";
		}
		else
			$res->status = "fail";
		return $res;
	}

	function EditDoc($query)
	{

		if($this->input->post('itemdocId'))
		{

			$sql="SELECT *
			FROM itemDocs
			WHERE itemdocId=?";

			$query=$this->db->query($sql,$this->input->post('itemdocId'))->row();

			if($query)
			{
				if ($this->input->post('folder'))
				{
					$foldersSql = "SELECT * FROM itemdocFolders WHERE itemId = ? ";
					$foldersQuery = $this->db->query($foldersSql, array($query->itemId))->result();
					$folderIds = '';
					foreach ($foldersQuery as $value) {
						$folderIds .= $value->itemdocfolderId.','; 
					}
					$this->form_validation->set_rules('folder', 'lang:administration_items_doc_folder','in_list['.$folderIds.']',
						array('in_list' => $this->lang->line('administration_items_doc_folderexist')));	
				}

				$this->form_validation->set_rules('name', 'lang:administration_items_doc_name', 'required');
				if ($this->form_validation->run() == FALSE)
				{
					echo "fail";
				}
				else
				{
					if ($this->input->post('folder')) {
						$folderObject = $this->input->post('folder');
					}
					else
					{
						$folderObject = NULL;
					}

					$objecttoEdit = array(
						'name' => $this->input->post('name'),
						'comment' => $this->input->post('comment'),
						'timestamp' => time(),
						'itemdocfolderId' => $folderObject, 
						);

					$this->db->where('itemdocId', $query->itemdocId);
					$this->db->update('itemDocs', $objecttoEdit);

					echo "success";

				}

			}
			else
				echo "fail";
		}
		else
			echo "fail";

	}

	function DeleteDoc()
	{
		if ($this->input->post('itemdocId') && $this->input->post('itemId'))
		{
			$sqltoDelete = "SELECT *
			FROM itemDocs
			WHERE itemdocId = ? and itemId = ?";

			$objecttoDetele = array($this->input->post('itemdocId') , $this->input->post('itemId'));
			
			$querydoc = $this->db->query($sqltoDelete,$objecttoDetele)->row();
			
			if (isset($querydoc))
			{
				$objecttoDetele = array(
					'itemdocId' => $querydoc->itemdocId
					);

				$this->db->delete('itemDocs', $objecttoDetele);
				unlink('./catapp/_docs/'.$querydoc->filename); 

				echo "success";
			}
			else
			{
				echo "fail";
			}
		}
		else
		{
			echo "fail";
		}
	}

	function DownloadDoc($query){
		if ($this->input->get('action') && $this->input->get('action') == 'download' && $this->input->get('itemdocId')) {
			$sqltoDelete = "SELECT * FROM itemDocs WHERE itemdocId = ? and itemId = ?";
			$objecttoDetele = array($this->input->get('itemdocId') , $query->itemId);
			$querydoc = $this->db->query($sqltoDelete,$objecttoDetele)->row();
			if (isset($querydoc))
			{
				$this->load->helper('download');
				$file = file_get_contents('./catapp/_docs/'.$querydoc->filename);
				$trozos = explode(".", $querydoc->filename); 
				$extension = end($trozos);

				force_download(addslashes($querydoc->name).'.'.$extension, $file);  
			}
			else{
				show_404();
			}
		}
	}

	function GetImage($query)
	{
		if ($this->input->get('action') && $this->input->get('action') == 'getImg' && $this->input->get('itemdocId')) {
			$sqltoDelete = "SELECT * FROM itemDocs WHERE itemdocId = ? and itemId = ?";
			$objecttoDetele = array($this->input->get('itemdocId') , $query->itemId);
			$querydoc = $this->db->query($sqltoDelete,$objecttoDetele)->row();
			if (isset($querydoc))
			{
				$this->load->helper('download');
				$file = file_get_contents('./catapp/_docs/'.$querydoc->filename);
				$trozos = explode(".", $querydoc->filename); 
				$extension = end($trozos);

				force_download(addslashes($querydoc->name).'.'.$extension, $file);  
			}
			else{
				show_404();
			}
		}
	}


	function DownloadDocument()
	{
		if ($this->input->get('itemdocid') && $this->input->get('itemid'))
		{	
			$sqltoDelete = "SELECT * FROM itemDocs WHERE itemdocId = ? and itemId = ?";

			$objecttoDetele = array($this->input->get('itemdocid') , $this->input->get('itemid'));
			
			$querydoc = $this->db->query($sqltoDelete,$objecttoDetele)->row();
			
			if (isset($querydoc))
			{
				$this->load->helper('download');
				$file = file_get_contents('./catapp/_docs/'.$querydoc->filename);
				$trozos = explode(".", $querydoc->filename); 
				$extension = end($trozos);

				force_download(addslashes($querydoc->name).'.'.$extension, $file);  
			}
			else
			{
				show_404();
			}
		}
	}



	function GetImageDoc()
	{

		if ($this->input->get('itemdocid') && $this->input->get('itemid'))
		{
			$sqltoDelete = "SELECT * FROM itemDocs WHERE itemdocId = ? and itemId = ?";

			$objecttoDetele = array( $this->input->get('itemdocid'), $this->input->get('itemid'));
			
			$querydoc = $this->db->query($sqltoDelete,$objecttoDetele)->row();
			
			if (isset($querydoc))
			{
				$this->load->helper('download');
				$file = file_get_contents('./catapp/_docs/'.$querydoc->filename);
				$trozos = explode(".", $querydoc->filename); 
				$extension = end($trozos);

				force_download(addslashes($querydoc->name).'.'.$extension, $file);  
			}
			else
			{
				show_404();
			}
		}
		else
			show_404();
	}

	function CreateFolder()
	{
		if($this->input->post('itemId'))
		{
			$this->form_validation->set_rules('name', 'lang:administration_items_doc_name', 'required');

			if ($this->form_validation->run() == FALSE)
			{
				echo "fail";
			}
			else
			{
				$objecttoInsert = array(
					'name' => $this->input->post('name'),
					'userId' => $this->session->UserId,
					'itemId' => $this->input->post('itemId'), 
					);

				$this->db->insert('itemdocFolders', $objecttoInsert);

				echo "success";
			}

		}
		else
			echo "fail";

	}

	function DeleteFolder()
	{

		if($this->input->post('itemdocfolderId') && $this->input->post('itemId'))
		{
			$sqltoDelete = "SELECT *
			FROM itemdocFolders
			WHERE itemdocfolderId = ? and itemId = ?";

			$objecttoDetele = array($this->input->post('itemdocfolderId') , $this->input->post('itemId'));
			$querydocfolder = $this->db->query($sqltoDelete,$objecttoDetele)->row();

			if (isset($querydocfolder))
			{
				$objecttoDetele = array(
					'itemdocfolderId' => $querydocfolder->itemdocfolderId
					);

				$this->db->delete('itemdocFolders', $objecttoDetele);

				echo "success";
			}
			else
			{
				echo "fail";
			}
		}
		else
		{
			echo "fail";
		}

	}

	function EditFolder()
	{

		if($this->input->post('itemdocfolderId'))  
		{
			$this->form_validation->set_rules('name', 'lang:administration_items_doc_name', 'required');

			if ($this->form_validation->run() == FALSE)
			{
				echo "fail";
			}
			else
			{

				$objecttoEdit = array(
					'name' => $this->input->post('name'),
					);

				$this->db->where('itemdocfolderId', $this->input->post('itemdocfolderId'));
				$this->db->update('itemdocFolders', $objecttoEdit);

				echo "success";
			}
		}
		else
			echo "fail";
	}

	public function GetUnread($sectionId)
	{
		$unread = 0;
		$sql = "SELECT ss.subsectionId,i.itemId,count(itemdocId) AS c 
		FROM itemDocs id
		LEFT JOIN items i
		ON i.itemId = id.itemId
		LEFT JOIN subSections ss
		ON ss.subsectionId = i.subsectionId
		WHERE NOT EXISTS
			(SELECT 1 FROM itemDocsView iv WHERE iv.itemdocId = id.itemdocId && iv.userId = ?)
		AND ss.sectionId = ?
		GROUP BY i.itemId";
		$unread = $this->db->query($sql,array($this->session->UserId,$sectionId))->result();
		return $unread;
	}

	public function AddToView($itemId)
	{
		$sql = "SELECT id.itemdocId,id.userId
		FROM itemDocs id
		JOIN items i
		ON (i.itemId = id.itemId)
		JOIN subSections ss
		ON (ss.subsectionId = i.subsectionId)
		WHERE i.itemId = ?";

		$docs = $this->db->query($sql,$itemId)->result();
		foreach ($docs as $doc) {
			$sql = "SELECT * FROM itemDocsView WHERE itemDocId = ? && userId = ?";
			$isView = $this->db->query($sql,array($doc->itemdocId,$this->session->UserId))->row();
			if($isView == NULL){
				$objecttoInsert = array(
					'dateView' 	=> date('Y-m-d'), 
					'userId' 	=> $this->session->UserId,
					'itemdocId'	=> $doc->itemdocId
				);
				$this->db->insert('itemDocsView', $objecttoInsert);
			}
		}
	}
}
