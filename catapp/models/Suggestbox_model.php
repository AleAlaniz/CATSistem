<?php

Class Suggestbox_model extends CI_Model {

	public function PostSuggest(){
		$this->form_validation->set_rules('suggest', 'lang:suggestbox_suggest', 'required');
		$this->form_validation->set_rules('site', 'lang:suggestbox_site', 'required');
		$this->form_validation->set_rules('campaign', 'lang:suggestbox_campaign', 'required');
		if ($this->form_validation->run() == FALSE)
		{
			return '{"status":"invalid"}';
		}
		else
		{
			$objectInsert = array(
				'timestamp' => time(),
				'userId' => $this->session->UserId,
				'suggest' => $this->input->post('suggest'),
				'site' => $this->input->post('site'),
				'campaign' => $this->input->post('campaign'),
				);
			$this->db->insert('suggestBox', $objectInsert);
			$response = new StdClass();
			$response->status = 'ok';
			$response->message = $this->lang->line('suggestbox_successmessage');
			return escapeJsonString($response, FALSE);
		}
	}

	public function GetMySuggest()
	{
		$response = new StdClass();
		$response->status = 'ok';
		$sql = "SELECT sb.suggest, sb.timestamp, sb.site, sb.campaign, concat(up.name, ' ', up.lastName) takenBy FROM suggestBox AS sb LEFT JOIN (SELECT name, lastName, userId FROM userPersonalData) AS up ON up.userId = sb.takenBy WHERE sb.userId = ? ORDER BY sb.suggestboxId DESC";
		$response->suggests = $this->db->query($sql, $this->session->UserId)->result();
		$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
		foreach ($response->suggests as $suggest) {
			$suggest->timestamp = date('d', $suggest->timestamp).' '.$meses[date('n', $suggest->timestamp)-1].' '.date('G:i', $suggest->timestamp);
		}
		return escapeJsonString($response, FALSE);
	}

	public function GetSections(){
		$data = new stdClass();
		$data->status = 'ok';
		if ($this->Identity->Validate('sections/viewall')) {
			$sql = 'SELECT sectionId, name, (SELECT count(*) FROM suggestBox INNER JOIN (SELECT sectionId, userId FROM users) AS u ON suggestBox.userId = u.userId WHERE u.sectionId = sections.sectionId && NOT EXISTS (SELECT 1 FROM suggestboxViews WHERE suggestboxId = suggestBox.suggestboxId && userId = ?)) noRead FROM sections ORDER BY name';
			$data->sections = $this->db->query($sql, $this->session->UserId)->result();
		}
		else
		{
			$sql = 'SELECT sectionId, name, (SELECT count(*) FROM suggestBox INNER JOIN (SELECT sectionId, userId FROM users) AS u ON suggestBox.userId = u.userId WHERE u.sectionId = sections.sectionId && NOT EXISTS (SELECT 1 FROM suggestboxViews WHERE suggestboxId = suggestBox.suggestboxId && userId = ?)) noRead FROM sections WHERE sectionId = ?';
			$data->sections = $this->db->query($sql, array($this->session->UserId, $this->session->sectionId))->result();
		}
		return escapeJsonString($data, FALSE);
	}

	public function GetSuggests(){
		$data = new stdClass();
		$data->status = 'ok';
		if ($this->uri->segment(3)) {
			if($this->uri->segment(3) != $this->session->sectionId && !$this->Identity->Validate('sections/viewall')){
				$data->suggests = array();
			}
			$sql = "SELECT sectionId, name FROM sections WHERE sectionId = ?";
			$data->sectionSelect = $this->db->query($sql, $this->uri->segment(3))->row();
			if ($data->sectionSelect) {
				$sql = "SELECT suggestBox.suggestboxId,  suggestBox.suggest, suggestBox.userId , suggestBox.timestamp,  suggestBox.site, suggestBox.campaign, concat(ut.name, ' ', ut.lastName) takenBy, concat(up.name, ' ', up.lastName) completeName, u.sectionId sectionId FROM suggestBox LEFT JOIN (SELECT name, lastName,userId FROM userPersonalData) AS ut ON ut.userId = suggestBox.takenBy  INNER JOIN (SELECT sectionId,userId FROM users) AS u ON u.userId = suggestBox.userId LEFT JOIN (SELECT name, lastName, userId FROM userPersonalData) AS up ON up.userId = suggestBox.userId WHERE u.sectionId = ? ORDER BY suggestboxId DESC";
				$data->suggests = $this->db->query($sql, $this->uri->segment(3))->result();
			}
			else
			{
				$data->suggests = array();
			}
		}
		elseif ($this->Identity->Validate('sections/viewall')) {
			$sql = "SELECT suggestBox.suggestboxId,  suggestBox.suggest, suggestBox.userId , suggestBox.timestamp,  suggestBox.site, suggestBox.campaign, concat(ut.name, ' ', ut.lastName) takenBy, concat(up.name, ' ', up.lastName) completeName, u.sectionId sectionId FROM suggestBox LEFT JOIN (SELECT name, lastName,userId FROM userPersonalData) AS ut ON ut.userId = suggestBox.takenBy  INNER JOIN (SELECT sectionId,userId FROM users) AS u ON u.userId = suggestBox.userId LEFT JOIN (SELECT name, lastName, userId FROM userPersonalData) AS up ON up.userId = suggestBox.userId  ORDER BY suggestboxId DESC";
			$data->suggests = $this->db->query($sql)->result();
		}
		else
		{
			$data->suggests = array();
		}

		$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
		foreach ($data->suggests as $suggest) {
			$suggest->timestamp = date('d', $suggest->timestamp).' '.$meses[date('n', $suggest->timestamp)-1].' '.date('G:i', $suggest->timestamp);
			$sql = "SELECT * FROM suggestboxViews WHERE suggestboxId = ? && userId = ?";
			$isView = $this->db->query($sql, array($suggest->suggestboxId, $this->session->UserId))->row();
			if ($isView == NULL) {
				$objectInsert = array(
					'timestamp' => time(),
					'userId' => $this->session->UserId,
					'suggestboxId' => $suggest->suggestboxId
					);
				$this->db->insert('suggestboxViews', $objectInsert);
			}
		}
		return escapeJsonString($data, FALSE);
	}

	public function Take(){

		if ($this->uri->segment(3)) {
			if ($this->Identity->Validate('sections/viewall')) {
				$sql = 'SELECT suggestboxId FROM suggestBox WHERE suggestboxId = ? && takenBy IS null';
				$suggest = $this->db->query($sql, $this->uri->segment(3))->row();
			}
			else
			{
				$sql = 'SELECT suggestboxId FROM suggestBox WHERE suggestboxId = ? && (SELECT count(*) FROM users WHERE userId = suggestBox.userId && sectionId = ?) = 1 && takenBy IS null';
				$suggest = $this->db->query($sql, array($this->uri->segment(3), $this->session->sectionId))->row();
			}

			if ($suggest) {
				$this->db->where('suggestboxId', $suggest->suggestboxId);
				$this->db->update('suggestBox', array('takenBy' => $this->session->UserId, 'takenTime' => time()));
				$response 			= new StdClass();
				$response->status 	= 'ok';
				$response->message 	= $this->lang->line('suggestbox_takenok');

				return escapeJsonString($response, FALSE);;
			}
		}
		return '{"status":"invalid"}';
	}

	public function GetUnread()
	{
		$unread = 0;
		$roles = ' && sectionId = '.$this->session->sectionId;
		if ($this->Identity->Validate('sections/viewall')) {
			$roles = ' ';
		}
		$sql = 'SELECT count(sb.suggestboxId) AS c 
		FROM suggestBox AS sb
		INNER JOIN (SELECT sectionId, userId FROM users) AS u ON sb.userId = u.userId 
		INNER JOIN (SELECT sectionId FROM sections WHERE true '.$roles.') AS s ON u.sectionId = s.sectionId 
		WHERE NOT EXISTS (SELECT 1 FROM suggestboxViews WHERE suggestboxViews.suggestboxId = sb.suggestboxId && userId = ?)';
		$suggests = $this->db->query($sql, $this->session->UserId)->row();
		$unread = $suggests->c;
		return $unread;
	}

}