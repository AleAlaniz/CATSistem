<?php
Class Event_model extends CI_Model {
	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		$this->lang->load('login_lang');
	}

	var $itemeventsId;
	var $title;
	var $userId;
	var $timestamp;
	var $itemId;
	var $location;
	var $duration;


	function GetEvents($query)
	{
		if ($this->Identity->Validate('items/item/event/actions')) {

			/***********Delete event**********/
			if ($this->Identity->Validate('items/item/event/actions/delete')){
				$this->DeleteEvent($query);
			}

			/***********Delete all events**********/
			if ($this->Identity->Validate('items/item/event/actions/deleteall')){
				$this->DeleteAllEvents($query);
			}

			/***********Get event to edit**********/
			if ($this->input->get('action') && $this->input->get('action') == 'edit' && $this->input->get('itemeventsId') && $this->Identity->Validate('items/item/event/actions/edit')) {
				$sqlevent = "SELECT * FROM itemEvents WHERE itemeventsId = ? and itemId = ?";
				$itemevent = array($this->input->get('itemeventsId') , $query->itemId);
				$query->eventEdit = $this->db->query($sqlevent,$itemevent)->row();
			}
			/***********Get event to edit**********/

			$sql = "SELECT * FROM itemEvents WHERE itemId = ? ORDER BY timestamp";
			$diary = array($query->itemId);
			$query->diary = $this->db->query($sql,$diary)->result();

			if ($this->input->get('action') && $this->input->get('action') == 'create' && $this->Identity->Validate('items/item/event/actions/create')) {
				$this->CreateEvent($query);
			}
			elseif ($this->input->get('action') && $this->input->get('action') == 'edit' && isset($query->eventEdit) && $query->eventEdit != NULL && $this->Identity->Validate('items/item/event/actions/edit')) {
				$this->EditEvent($query);
			}
			else
			{
				$query->ActionView = $this->load->view('items/events/_viewevents_get', $query, TRUE);
				$query->partialView = $this->load->view('items/events/_viewevents', $query, TRUE);
				$this->load->view('_shared/_layoutheader');
				$this->load->view('items/item', $query);
				$this->load->view(PRELAYOUTFOOTER);
			}
		}
		else{
			$sql = "SELECT * FROM itemEvents WHERE itemId = ? ORDER BY timestamp";
			$diary = array($query->itemId);
			$query->diary = $this->db->query($sql,$diary)->result();

			$query->ActionView = $this->load->view('items/events/_viewevents_get', $query, TRUE);
			$query->partialView = $this->load->view('items/events/_viewevents', $query, TRUE);
			$this->load->view('_shared/_layoutheader');
			$this->load->view('items/item', $query);
			$this->load->view(PRELAYOUTFOOTER);
		}
	}

	function CreateEvent($query)
	{
		$this->form_validation->set_rules('title', 'lang:administration_items_event_title', 'required');

		if ($this->form_validation->run() == FALSE)
		{
			$query->ActionView = $this->load->view('items/events/_viewevents_create', $query, TRUE);
			$query->partialView = $this->load->view('items/events/_viewevents', $query, TRUE);
			$this->load->view('_shared/_layoutheader');
			$this->load->view('items/item', $query);
			$this->load->view(PRELAYOUTFOOTER);
		}
		else
		{
			$validDate = validateDate($this->input->post('fyear').'-'.$this->input->post('fmonth').'-'.$this->input->post('fday').' '.$this->input->post('fhour').':'.$this->input->post('fminute'));
			if ($validDate) {
				$timestamp = strtotime($this->input->post('fyear').'-'.$this->input->post('fmonth').'-'.$this->input->post('fday').' '.$this->input->post('fhour').':'.$this->input->post('fminute'));

				$itemEvent = array(
					'title' => $this->input->post('title'),
					'timestamp' => $timestamp,
					'userId' => $this->session->UserId,
					'itemId' => $query->itemId,
					'details' => $this->input->post('details'),
					'duration' => $this->input->post('duration'),
					'location' => $this->input->post('location')
					);
				$this->db->insert('itemEvents', $itemEvent);
				$this->session->set_flashdata('itemdiaryMessage', 'create');
				header('Location:/'.FOLDERADD.'/items/item/'.$query->itemId.'?cmd=diary');
			}
			else{
				header('Location:/'.FOLDERADD.'/items/item/'.$query->itemId.'?cmd=diary');
			}
		}
	}

	function EditEvent($query)
	{
		$this->form_validation->set_rules('title', 'lang:administration_items_event_title', 'required');
		if ($this->form_validation->run() == FALSE)
		{
			$query->ActionView = $this->load->view('items/events/_viewevents_edit', $query, TRUE);
			$query->partialView = $this->load->view('items/events/_viewevents', $query, TRUE);
			$this->load->view('_shared/_layoutheader');
			$this->load->view('items/item', $query);
			$this->load->view(PRELAYOUTFOOTER);
		}
		else
		{
			$itemEvent = array(
				'title' => $this->input->post('title'),
				'details' => $this->input->post('details'),
				'duration' => $this->input->post('duration'),
				'location' => $this->input->post('location')
				);
			$this->db->where('itemeventsId', $query->eventEdit->itemeventsId);
			$this->db->update('itemEvents', $itemEvent);
			$this->session->set_flashdata('itemdiaryMessage', 'edit');
			header('Location:/'.FOLDERADD.'/items/item/'.$query->itemId.'?cmd=diary');

		}
	}


	function DeleteEvent($query)
	{
		if ($this->input->get('action') && $this->input->get('action') == 'deleteevent' && $this->input->get('itemeventsId')) {
			$sqlevent = "SELECT * FROM itemEvents WHERE itemeventsId = ? and itemId = ?";
			$itemevent = array($this->input->get('itemeventsId') , $query->itemId);
			$queryevent = $this->db->query($sqlevent,$itemevent)->row();
			if (isset($queryevent))
			{
				$eventDelete = array(
					'itemeventsId' => $queryevent->itemeventsId
					);
				$this->db->delete('itemEvents', $eventDelete);
				$this->session->set_flashdata('itemdiaryMessage', 'delete');
			}
			else{
				show_404();
			}
		}
	}

	function DeleteAllEvents($query)
	{
		if ($this->input->get('action') && $this->input->get('action') == 'deleteallevent') {						
			$sqlevent = "SELECT * FROM itemEvents WHERE itemId = ?";
			$itemevent = array($query->itemId);
			$queryevent = $this->db->query($sqlevent,$itemevent)->row();
			if (isset($queryevent))
			{
				$eventDelete = array(
					'itemId' => $query->itemId
					);
				$this->db->delete('itemEvents', $eventDelete);
				$this->session->set_flashdata('itemdiaryMessage', 'deleteall');
			}
			else{
				show_404();
			}
		}
	}

}
