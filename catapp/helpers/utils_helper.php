<?php
if(!defined('BASEPATH')) exit('No direct scrript access allowed');


function insert_audit_logs($table,$action,$content)
{
    $CI =& get_instance();
    $messageInsert = array(
        'userId'    => $CI->session->UserId,
        'table'     => $table,
        'action'    => $action,
        'content'   => json_encode($content),
        'timestamp' => time()
    );
    
    $CI->db->insert('logs', $messageInsert);
}

function genereteRandomString()
{
    $str=rand(); 
    $result = md5($str); 
    return $result;
}