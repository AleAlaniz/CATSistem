<?php
if (!defined( 'BASEPATH')) exit('No direct script access allowed'); 
class Surveys_hook
{
    private $ci;
    public function __construct()
    {
        $this->ci =& get_instance();
        !$this->ci->load->library('session') ? $this->ci->load->library('session') : false;
        !$this->ci->load->helper('url') ? $this->ci->load->helper('url') : false;
    }    

    public function complete_surveys()
    {
        if($this->ci->session->Loged && (($this->ci->uri->segment(1) == NULL && $this->ci->uri->segment(2) == NULL) || ($this->ci->uri->segment(1) == 'home' && $this->ci->uri->segment(2) == 'index')) && $this->ci->Identity->Validate('surveys/view') && !($this->ci->uri->segment(1) == 'surveys' && $this->ci->uri->segment(2) == 'completesurvey') && !($this->ci->uri->segment(1) == 'users' && $this->ci->uri->segment(2) == 'profilephoto')  && !($this->ci->uri->segment(1) == 'users' && $this->ci->uri->segment(2) == 'config') )
        {
            $timestamp = time();
            $sql = 
            "SELECT surveys.surveyId, surveys.name, surveys.description
            FROM surveyLinks INNER JOIN surveys ON surveyLinks.surveyId = surveys.surveyId
            WHERE surveys.required = 'TRUE' && (surveyLinks.userId = ? || surveyLinks.sectionId = ? || surveyLinks.siteId = ? || surveyLinks.roleId = ?)
            && surveys.active = 'TRUE'
            && (surveys.startDate <= ? || surveys.startDate IS NULL) 
            && (surveys.endDate >= ? || surveys.endDate IS NULL)
            && (SELECT count(*) FROM surveyCompletes WHERE surveyCompletes.surveyId = surveys.surveyId && surveyCompletes.userId = ?) = 0
            GROUP BY surveyLinks.surveyId ORDER BY surveys.surveyId ASC";

            $survey = $this->ci->db->query($sql, 
                array($this->ci->session->UserId, 
                    $this->ci->session->sectionId, 
                    $this->ci->session->siteId, 
                    $this->ci->session->roleId,
                    $timestamp, 
                    $timestamp,
                    $this->ci->session->UserId))->row();

            if (isset($survey)) 
            {
                header('Location:/'.FOLDERADD.'/surveys/completesurvey');
            }
        }


    }
}
