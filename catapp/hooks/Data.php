<?php
if (!defined( 'BASEPATH')) exit('No direct script access allowed'); 
class Data
{
	private $ci;
	public function __construct()
	{
		$this->ci =& get_instance();
		!$this->ci->load->library('session') ? $this->ci->load->library('session') : false;
		!$this->ci->load->helper('url') ? $this->ci->load->helper('url') : false;
	}    

	public function check_data_complete()
	{

		if($this->ci->uri->segment(1) != 'users' && $this->ci->uri->segment(2) != 'config' && $this->ci->session->Loged && (($this->ci->uri->segment(1) == NULL && $this->ci->uri->segment(2) == NULL) || ($this->ci->uri->segment(1) == 'home' && $this->ci->uri->segment(2) == 'index') || ($this->ci->uri->segment(1) == 'home' && $this->ci->uri->segment(2) == NULL))) {

			$sql 	= 'SELECT u.password FROM users AS u WHERE u.userId = ? && u.active = 1';
			$query 	= $this->ci->db->query($sql, $this->ci->session->UserId)->row();
			if ($query && crypt('cat*123', $query->password) == $query->password) {
				header('Location:/'.FOLDERADD.'/users/config?cmd=alert');
			}
			else{
				$sql = 'SELECT userId FROM completeDataUsers WHERE userId = ? AND completeDataUsers.timestamp <= UNIX_TIMESTAMP() LIMIT 1';
				$notify =  $this->ci->db->query($sql, $this->ci->session->UserId)->row();
				if ($notify) {
					header('Location:/'.FOLDERADD.'/users/config?cmd=data&msg=notify');

				}
			}
		}
	}
}