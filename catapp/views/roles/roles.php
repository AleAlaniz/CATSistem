<ol class="breadcrumb">
	<li><a href="/<?=FOLDERADD?>/administration"><?=$this->lang->line('general_administration');?></a></li>
	<li class="active"><?=$this->lang->line('general_roles');?></li>
</ol>

<?php if($this->Identity->Validate('roles/create')) { ?>
<p>
	<a href="/<?=FOLDERADD?>/roles/create" class="btn btn-sm btn-success "><i class="fa fa-plus"></i><strong> <?=$this->lang->line('administration_create');?></strong></a>
</p>
<?php }
if (isset($_SESSION['flashMessage'])){ ?>
<div class="alert alert-success alert-dismissible" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	<strong><i class="fa fa-check"></i></strong> 
	<?php if ($_SESSION['flashMessage'] == 'create'){
		echo $this->lang->line('roles_successmessage');
	}
	elseif ($_SESSION['flashMessage'] == 'delete'){
		echo $this->lang->line('roles_deletemessage');
	}
	?>
</div>
<?php } ?>
<div class="panel panel-default">
	<div class="panel-heading">
		<strong><?=$this->lang->line('general_roles');?></strong>
		<span class="badge pull-right bg-success"><?=count($model)?></span>
	</div>
	<table class="table table-hover">
		<thead>
			<tr class="active">
				<th><?=$this->lang->line('roles_name');?></th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			<?php if (count($model) > 0) 
			{
				?>

				<?php foreach ($model as $role)
				{
					?>
					<tr class="optionsUser">
						<td><?=encodeQuery($role->name)?></td>
						<td>
							<?php if($this->Identity->Validate('roles/details')) { ?>
							<a href="/<?=FOLDERADD?>/roles/details/<?=$role->roleId?>"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></a>&nbsp; 
							<?php } ?>
							<?php if($this->Identity->Validate('roles/edit')) { ?>
							<a href="/<?=FOLDERADD?>/roles/edit/<?=$role->roleId?>"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>&nbsp; 
							<?php } ?>
							<?php if($this->Identity->Validate('roles/delete')) { ?>
							<a href="/<?=FOLDERADD?>/roles/delete/<?=$role->roleId?>"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
							<?php } ?>
						</td>
					</tr>
					<?php
				} 
			}
			else 
			{
				?>
				<tr class="text-center">
					<td colspan="3"><i class="fa fa-user-plus"></i> <?=$this->lang->line('roles_empty');?></td>
				</tr>

				<?php
			}?>
		</tbody>
	</table>
</div>
<script type="text/javascript">
$('#nav_roles').addClass('active');
$('.optionsUser').hover(function(){
	$(this).find('.glyphicon').css('visibility','visible');

},function(){
	$(this).find('.glyphicon').css('visibility','hidden');

});
</script>