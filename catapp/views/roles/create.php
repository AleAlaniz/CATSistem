<ol class="breadcrumb">
	<li><a href="/<?=FOLDERADD?>/administration"><?=$this->lang->line('general_administration');?></a></li>
	<li><a href="/<?=FOLDERADD?>/roles"><?=$this->lang->line('general_roles');?></a></li>
	<li class="active"><?=$this->lang->line('administration_create');?></li>
</ol>
<div class="panel panel-default">
	<div class="panel-heading">
		<strong><?=$this->lang->line('roles_create');?></strong>
	</div>
	<div class="panel-body">
		<form class="form-horizontal" method="POST" novalidate >
			<div class="form-group">
				<label for="name" class="col-sm-2 control-label"><?=$this->lang->line('roles_name');?><span class="text-danger"><strong> *</strong></span></label>
				<div class="col-sm-10">
					<input type="text" class="form-control input-sm" id="name" name="name" value="<?php echo set_value('name');?>" placeholder="<?=$this->lang->line('roles_name');?>" required>
					<?php echo form_error('name'); ?>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label"><?=$this->lang->line('roles_permissions');?></label>
				<div class="col-sm-10">
					<input type="text" id="likeName" class="form-control input-sm input-heading-find" placeholder="<?=$this->lang->line('general_find')?>">
					<button type="button" role="checkall" class="btn btn-default btn-xs"><?=$this->lang->line('roles_selectall');?></button>
					<button type="button" role="uncheckall" class="btn btn-default btn-xs"><?=$this->lang->line('roles_unselectall');?></button>
					<?php
					foreach ($permissions as $key => $permission) {
						$permission->isSelected = FALSE;
						if ($permission->default == 'TRUE') {
							$permission->isSelected = TRUE;
						}
						?>
						<div class="checkbox optionsUser" findwith="<?=encodeQuery($permission->description)?>">
							<label >
								<input type="checkbox" name="permissions[]" value="<?=$permission->permisonId?>" <?php echo set_checkbox('permissions[]', '<?=$permission->permisonId?>', $permission->isSelected); ?> >
								<?=encodeQuery($permission->description)?>
							</label>
						</div>
						<?php
					}
					?>
					<?php echo form_error('permissions[]'); ?>
				</div>
			</div>
			<hr>
			<div class="form-group text-center">
				<button type="submit" class="btn btn-success btn-sm"><?=$this->lang->line('general_save');?></button>
				<a href="/<?=FOLDERADD?>/roles" class="btn btn-danger btn-sm"><?=$this->lang->line('general_cancel');?></a>
			</div>
		</form>
	</div>
</div>
<script type="text/javascript">
$('#nav_roles').addClass('active');
$('[role=checkall]').click(function() {
	$('[name="permissions[]"]').each(function(){ this.checked = true; });
});
$('[role=uncheckall]').click(function() {
	$('[name="permissions[]"]').each(function(){ this.checked = false; });
});

$('#likeName').keydown(function(e){
	var code = e.keyCode || e.which;
	if (code == 13) {
		e.preventDefault();
		return false;
	}
});

$('#likeName').on('keyup paste', function() {

	var datosForm = {
		userLike: $(this).val(),
		optionsUser : $('.optionsUser')
	};

	if (datosForm.userLike == '') {
		datosForm.optionsUser.css('display', 'block');
	}
	else
	{
		datosForm.optionsUser.css('display', 'none');
		var users = $('.optionsUser').filter(function() {
			return $(this).attr('findwith').toLowerCase().indexOf(datosForm.userLike) > -1;
		});
		users.css('display', 'block');
	}
	
});
</script>