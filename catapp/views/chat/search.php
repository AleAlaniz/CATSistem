<link href="/<?php echo APPFOLDERADD; ?>/libraries/css/bootstrap-datepicker.css" rel="stylesheet">
<div class="page-title">
    <h5 class="title"><?php echo $title; ?></h5>
    <a href="#/chat/unique" class="btn btn-white pull-right"><i class="fa fa-chevron-left"></i> <?php echo $this->lang->line('general_goback');?></a>
</div>

<div class="col-md-12 flat-style">
    <div class="panel panel-default">
        <div class="panel-body">

            <form class="form-horizontal" method="POST" class="bg-white" ng-submit="submitSearch()" ng-show="!loadingAll">
                <h3><?php echo $this->lang->line('chat_search_2');?></h3>
                <hr style="margin-top: 10px; "/>

                <div class="form-group">
                    <label for="keyword" class="col-sm-2 control-label"><?php echo $this->lang->line('chat_search_keyword');?></label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control input-sm" id="keyword" name="keyword" ng-model="keyword" value="<?php echo set_value('keyword')?>" minlength="3" placeholder="<?php echo $this->lang->line('chat_search_keyword');?>">
                      <?php echo form_error('keyword'); ?>
                  </div>
              </div>

              <div class="form-group">
                <label for="" class="col-sm-2 control-label"><?php echo $this->lang->line('chat_search_daterange');?></label>
                <div class="col-sm-10">
                    <div class=" input-group input-daterange">
                        <input type="text" data-date-format="mm/dd/yyyy" class="form-control" name="dateStart" ng-model="dateStart" placeholder="<?=$this->lang->line('tools_analitycs_datestart');?>">
                        <span class="input-group-addon"><?=$this->lang->line('tools_analitycs_dateto');?></span>
                        <input type="text" data-date-format="mm/dd/yyyy" class="form-control" name="dateFinish" ng-model="dateFinish" placeholder="<?=$this->lang->line('tools_analitycs_datefinish');?>">
                    </div>
                </div>
            </div>
            <div class="form-group" style="position: relative;margin-left: -13px !important;margin-right: -1px;">
                <searchUsers></searchUsers>
                <p id="no_users" class="text-danger" style="display: none;">Debe ingresar al menos un usuario</p>
            </div>

            <!-- busqueda por team leaders -->
                <!-- <div class="form-group">
                    <label for="" class="col-sm-2 control-label"><?php echo $this->lang->line('chat_search_tl');?></label>
                    <div class="col-sm-10">
                        <select name="teamLeaders" id="teamLeaders" class="form-control" ng-model="teamLeader">
                            <option ng-repeat="tl in teamLeaders" value="{{tl.userId}}">{{tl.completeName}}</option>
                        </select>
                    </div>
                </div> -->

                <div class="form-group text-center">
                    <button class="btn btn-green"><?php echo $this->lang->line('chat_search');?></button>
                    <button class="btn btn-green" disabled ng-show="loading"><i class="fa fa-refresh fa-spin fa-lg fa-fw"></i></button>
                </div>
            </form>
            <div class="col-md-12-well" ng-show="loadingAll">
                <i class="fa fa-refresh fa-spin fa-5" aria-hidden="true"></i>
            </div>

            <div ng-show="chats.length > 0">
                <hr>
                <h3><?php echo $this->lang->line('chats_search_results');?></h3>
                <a ng-href="{{link}}" ng-click="validateLink(c)" class="col-md-12 well pointer" ng-repeat="c in chats" style="text-decoration: none;" target="_blank">
                    <h4 ng-bind="c.label +' ('+c.message.date+')'"></h4>
                    <p ng-bind-html="c.message.message"></p>
                </a>
            </div>
            <div ng-show="noResults">
                <h1><?php echo $this->lang->line('chats_search_no_results');?></h1>
            </div>
        </div>
    </div>
</div>
<script src="/<?php echo APPFOLDERADD; ?>/libraries/script/bootstrap-datepicker.min.js"></script>
<script src="/<?php echo APPFOLDERADD; ?>/libraries/script/locales/bootstrap-datepicker.es.min.js"></script>
