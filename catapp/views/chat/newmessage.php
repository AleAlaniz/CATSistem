<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="shortcut icon" href="/<?php echo APPFOLDERADD;?>/libraries/images/favicon.png" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" type="text/css" href="/<?php echo APPFOLDERADD;?>/libraries/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="/<?php echo APPFOLDERADD;?>/libraries/css/jquery-ui.min.css">
	<title>CAT - Technologies</title>
</head>
<body>
	<div class="alert alert-success text-center">
		<strong><?=$this->lang->line('chat_newmessage')?></strong>
	</div>
	<script src="/<?php echo APPFOLDERADD;?>/libraries/script/jquery-1.11.3.min.js"></script>
</body>
</html>