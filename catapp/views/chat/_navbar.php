<div class="page-title">
	<h5 class="title"><?php echo $title; ?></h5>
	<?php
	if($this->Identity->Validate('chat/unique')) 
	{
		?>
		<a href="#/chat/unique" class="btn btn-white">
			<i class="fa fa-comment-o fa-lg"></i> <span class="hidden-xs">&nbsp; <?php echo $this->lang->line('chat_unique');?></span>
			<span class="chat-noread-dot " ng-show="unreadu > 0" ng-bind="unreadu | number"></span>
		</a>
		<?php 
	}
	if($this->Identity->Validate('chat/multi')) 
	{
		?>
		<a href="#/chat/multi" class="btn btn-white">
			<i class="fa fa-comments-o fa-lg"></i> <span class="hidden-xs">&nbsp; <?php echo $this->lang->line('chat_multi');?></span>
			<span class="chat-noread-dot " ng-show="unreadm > 0" ng-bind="unreadm | number"></span>
		</a>
		<?php 
	}
	if($this->Identity->Validate('chat/unique')) 
	{
		?>
		<a href="#/chat/search" class="btn btn-white">
			<i class="fa fa-search fa-lg"></i> <span class="hidden-xs">&nbsp; <?php echo $this->lang->line('chat_search_advanced');?></span>
		</a>
		<?php 
	}
	?>
</div>