<?php echo $navbar; ?>
<div class="col-xs-12 flat-style" style="margin-bottom:15px" ng-init="userId = '<?php echo $this->session->UserId; ?>';getUserId(userId)">
	<div class="panel panel-default">
		<div class="panel-body row chat-container-body">
			<div class="col-sm-4 col-md-3 chats-container">
				<?php
				if($this->Identity->Validate('chat/multi/new'))
				{
					?>
					<a class="btn btn-white btn-block text-ligthgreen b_r-0 p_20 btn-noshadow" style="" href="#/chat/newmulti" ><?php echo $this->lang->line('chat_new');?> &nbsp;<i class="fa fa-plus fa-lg"></i></a>
					<?php 
				}
				?>
				<div class="form-inline">
					<div class="form-group" style="width: 100%; height:41px">
						<div class="input-group" style="width: 100%;border-bottom: 1px solid #DBDBDB ;border-top: 1px solid #DBDBDB ;">
							<div class="input-group-addon" style="border: 0;background:transparent"><i class="fa fa-search"></i></div>
							<input type="text" class="form-control" ng-model="chatfilter" style="border: 0" placeholder="<?php echo $this->lang->line('chat_searchchat');?>">
						</div>
					</div>
				</div>
				<div class="list-group chats" id="chat_chats">
					<a href="#/chat/multi/{{chat.chatmId}}" ng-class="{'active': chatIdSelected == chat.chatmId}" class="list-group-item hoverable" ng-repeat="chat in chats | filter : {label : chatfilter} | orderBy : '-timestamp'">
						<div class="media">
							<div class="media-left text-center">
								<div cn-square-image cls="profileImage profileImageChat pointer" url="/<?php echo FOLDERADD; ?>/users/profilephoto?userId={{chat.userId}}&wah=200"></div>
							</div>
							<div class="media-body">
								<h5 class="list-group-item-heading" ng-bind="chat.label"></h5>
								<p class="lastMessage" ng-class="{'text-muted': chat.noRead <= 0}" ng-bind-html="chat.message.message"></p>
								<div class="chat-date-container">
									<span class="chat-time time" ng-class="{'text-muted': chat.noRead <= 0}"  ng-bind="chat.date"></span>
								</div>
								<div class="chat-noread-container">
									<span class="chat-noread-dot " ng-show="chat.noRead > 0" ng-bind="chat.noRead"></span>

									<span class="btn btn-xs btn-options text-dark chats-delete" ng-show="chat.userId == userId" data-target="#confirm-deleteall" data-toggle="modal" data-id="{{chat.chatmId}}" title="<?php echo $this->lang->line('chat_deletechatm');?>" aria-label="<?php echo $this->lang->line('chat_deletechatm');?>">
										<i class="fa fa-trash"></i>
									</span>

									<span class="btn btn-xs btn-options text-dark chats-delete" ng-if="chat.userId != userId"  data-target="#confirm-delete" data-toggle="modal" data-id="{{chat.chatmId}}" title="<?php echo $this->lang->line('chat_exitchatm');?>" aria-label="<?php echo $this->lang->line('chat_exitchatm');?>">
										<i class="fa fa-sign-out"></i>
									</span>
								</div>
							</div>
						</div>
					</a>
					<div class="list-group-item" ng-hide="(chats | filter : {label : chatfilter}).length > 0">
						<h5 class="text-muted text-center"><?php echo $this->lang->line('chat_empty'); ?></h5>
						<?php if($this->Identity->Validate('chat/multi/new')){?>
						<h5 class="text-muted text-center"><?php echo $this->lang->line('chat_press'); ?></h5>
						<?php }?>
					</div>
				</div>
			</div>
			<div class="col-sm-8 col-md-9 text-center text-dark" ng-show="!chat">
				<h2 style="margin-top:115px"> 
					<i class="fa fa-comment-o fa-4x"></i>
					<br>
					<br>
					<?php echo $this->lang->line('chat_selectchat');?> 
				</h2>
			</div>
			<div class="chat-container " ng-class="{'col-sm-8 col-md-9' : !infoOpen, 'col-sm-5 col-md-6' : infoOpen }" ng-show="chat">
				<div class="media chat-header">
					<div class="media-left">
						<div cn-square-image cls="profileImage profileImageChat" url="/<?php echo FOLDERADD; ?>/users/profilephoto?userId={{chat.userId}}&wah=200"></div>
					</div>
					<div class="media-body">
						<h5>
							<span id="el" ng-bind="chat.label" ng-show="subjectEdition==false" ng-init="addDoubleClickAngular(userId)">
							</span>

							<input type="text" ng-model="chat.label" ng-hide="subjectEdition==false" maxlength="250">

							<a class="btn text-success" ng-hide="subjectEdition==false" ng-click="changeSubject()" aria-label="<?php echo $this->lang->line('general_accept');?>" title="<?php echo $this->lang->line('general_accept');?>">
								<i class="fa fa-check fa-2x"></i>
							</a>

							<a class="btn text-danger" ng-hide="subjectEdition==false" ng-click="restoreSubject()" aria-label="<?php echo $this->lang->line('general_cancel');?>" title="<?php echo $this->lang->line('general_cancel');?>">
								<i class="fa fa-times fa-2x"></i>
							</a>

							<?php if($this->Identity->Validate('chat/multi/new')){?>

								<a class="btn" ng-show="chat.userId == userId && subjectEdition==false" ng-click="showAdd = true" title="<?php echo $this->lang->line('add_users');?>">
									<i class="fa fa-plus-square fa-2x"></i>
								</a>

							<?php }?>
						</h5>
						<div ng-bind-html="error | HtmlSanitize" style="text-align: left;"></div>
						<div class="btn-group  btn-noshadow pull-right btn-options">
							<span aria-expanded="false" class="btn btn-sm btn-noshadow text-dark dropdown-toggle" data-toggle="dropdown">
								<i class="fa fa-caret-down"></i>
							</span>
							<ul class="dropdown-menu dropdown-white dropdown-menu-scale dropdown-right">
								<li class="cursor-pointer">
									<a data-target="#confirm-delete" data-toggle="modal" data-id="{{chat.chatmId}}">
										<?php echo $this->lang->line('chat_exitchatm'); ?>
									</a>
								</li>
								<li class="cursor-pointer" ng-show="chat.userId == userId">
									<a data-target="#confirm-deleteall" data-toggle="modal" data-id="{{chat.chatmId}}">
										<?php echo $this->lang->line('chat_deletechatm'); ?>
									</a>
								</li>
								<li class="cursor-pointer" ng-show="chat.userId == userId" ng-click="subjectEdition = true">
									<a >
										<?php echo $this->lang->line('chat_modify_subject'); ?>
									</a>
								</li>
								<li class="cursor-pointer" ng-click="infoOpen = true">
									<a >
										<?php echo $this->lang->line('chat_info'); ?>
									</a>
								</li>
							</ul>
						</div>
						
						<?php if($this->Identity->Validate('chat/multi/new')){?>
						<div class="well" ng-show="showAdd;">
							<span class="btn btn-red btn-xs" ng-click="closePanel()"><i class="fa fa-times pull left"></i></span>
							<div>
								<searchUsers></searchUsers>
							</div>
							
						<div class="col-md-offset-2" style="padding-left: 2em;padding-top: 1em;">
							<button class="btn btn-success" ng-show="users.length > 0" class="btn bg-lightblue" ng-click="addNewMember()" title="<?php echo $this->lang->line('add_to_chat');?>"><?php echo $this->lang->line('general_accept');?></button>
							<span ng-show="loading"><i class="fa fa-spinner fa-spin fa-2x"></i></span>
							<span ng-show="!loading && success" title="<?php echo $this->lang->line('add_success');?>"<i class="fa fa-check fa-2x text-success"></i></span>
						</div>
						</div>
						<?php }?>
					</div>
				</div>
				<div class="list-group chat-messages" schroll-bottom="chat.messages">
					<span class="btn btn-white btn-block b_r-0 text-red btn-noshadow" ng-hide="gettingOldMessages || fullOldMessages || chat.messages.length < 1" ng-click="getOldMessages()"><?=$this->lang->line('chat_loadmessages');?></span>
					<span class="btn-block center-block text-center text-red" ng-show="gettingOldMessages"><i class="fa fa-refresh fa-spin fa-3x"></i></span>
					<div class="list-group-item" ng-repeat="message in chat.messages  | orderBy : 'timestamp'">
						<div class="media"> 
							<div class="media-left" ng-show="message.userId == userId">
								<div cn-square-image cls="profileImage profileImageChat" url="/<?php echo FOLDERADD; ?>/users/profilephoto?userId={{message.userId}}&wah=200"></div>
								<span class="btn btn-sm btn-options btn-info" data-target="#messageInfo" data-toggle="modal" ng-click="getMessageInfo(message.hash_id)">
									<i class="fa fa-exclamation"></i>
								</span>
							</div>
							<div class="media-body" ng-class="{textRight: message.userId != userId}">
								<h5 class="list-group-item-heading" ng-bind="message.name+' '+message.lastName"></h5>
								<span ng-bind-html="message.message"></span> 
								<div ng-bind="message.date" class="text-muted small"></div>
							</div>
							<div class="media-right"  ng-show="message.userId != userId">
								<div cn-square-image cls="profileImage profileImageChat" url="/<?php echo FOLDERADD; ?>/users/profilephoto?userId={{message.userId}}&wah=200"></div>
								<!-- <span class="btn btn-sm btn-options btn-info" data-target="#messageInfo" data-toggle="modal" ng-click="getMessageInfo(message.hash_id)">
									<i class="fa fa-exclamation"></i>
								</span> -->
							</div>
						</div>
					</div>
				</div>
				<?php 
				if ($this->Identity->Validate('chat/multi/sendmessage'))
				{
					?>
					<form class="form-inline chat-newmessage" ng-submit="sendMessage()" >
						<div class="form-group" >
							<div class="input-group">
								<div class="input-group-addon btn btn-white btn-newmessage-send btn-noshadow b_r-0" id="emojiPicker"><i class="fa fa-smile-o"></i></div>
								<div id="inputText" class="form-control" contenteditable="true"></div>
								<!-- <input type="text" class="form-control" ng-model="chat.newmessage" placeholder="<?php echo $this->lang->line('chat_writemessage');?>"> -->
								<div ng-click="sendMessage()" class="input-group-addon btn btn-white btn-newmessage-send btn-noshadow b_r-0"><i class="fa fa-paper-plane"></i></div>
							</div>
						</div>
					</form>
					<?php
				}
				?>
			</div>

			<div class="col-sm-3 col-md-3 chats-container" ng-show="chat && infoOpen" style="border-left: 1px solid #DBDBDB;border-right:0">
				<div class="media chat-header" >
					<div class="media-body" style="height:40px">
						<h5 class="list-group-item-heading "><?php echo $this->lang->line('chat_info'); ?></h5>
						<span class="btn btn-sm pull-right btn-options text-dark" style="right:25px" ng-click="infoOpen = false">
							<i class="fa fa-times"></i>
						</span>
					</div>
				</div>
				<div class="list-group chats" style="height:500px">
					<div class="list-group-item">
						<h5 class="text-muted text-center"><?php echo $this->lang->line('chat_users'); ?></h5>
					</div>
					<div class="list-group-item hoverable" ng-repeat="user in chat.users">
						<div class="media">
							<div class="media-left text-center">
								<div cn-square-image cls="profileImage profileImageChat" url="/<?php echo FOLDERADD; ?>/users/profilephoto?userId={{user.userId}}&wah=200"></div>
							</div>
							<div class="media-body">
								<h5>
									<span ng-bind="user.name+' '+user.lastName"></span> 
									<span class="btn btn-xs btn-options text-red btn-deleteuser" ng-if="chat.userId == userId && chat.userId != user.userId" data-target="#confirm-deleteuser" data-toggle="modal" data-id="{{user.userId}}">
										<i class="fa fa-times"></i>
									</span>
								</h5>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="modal animated shake" id="confirm-delete" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-sm ">
			<div class="modal-content">
				<div class="modal-body">
					<p><?php echo $this->lang->line('chat_exitareyousure'); ?></p>
					<div class="text-right">
						<span class="btn btn-white" data-dismiss="modal" aria-label="Close"><?php echo $this->lang->line('general_no'); ?></span>
						<span class="btn btn-lightgreen" id="delete-yes"><?php echo $this->lang->line('general_yes'); ?></span>
						<span class="btn btn-lightgreen disabled" id="delete-loading" style="display:none"><i class="fa fa-refresh fa-spin fa-lg"></i></span>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="modal animated shake" id="confirm-deleteall" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-sm ">
			<div class="modal-content">
				<div class="modal-body">
					<p><?php echo $this->lang->line('chat_deleteareyousure'); ?></p>
					<div class="text-right">
						<span class="btn btn-white" data-dismiss="modal" aria-label="Close"><?php echo $this->lang->line('general_no'); ?></span>
						<span class="btn btn-lightgreen" id="deleteall-yes"><?php echo $this->lang->line('general_yes'); ?></span>
						<span class="btn btn-lightgreen disabled" id="deleteall-loading" style="display:none"><i class="fa fa-refresh fa-spin fa-lg"></i></span>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="modal animated shake" id="confirm-deleteuser" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-sm ">
			<div class="modal-content">
				<div class="modal-body">
					<p><?php echo $this->lang->line('user_deleteareyousure'); ?></p>
					<div class="text-right">
						<span class="btn btn-white" data-dismiss="modal" aria-label="Close"><?php echo $this->lang->line('general_no'); ?></span>
						<span class="btn btn-lightgreen" id="deleteuser-yes"><?php echo $this->lang->line('general_yes'); ?></span>
						<span class="btn btn-lightgreen disabled" id="deleteuser-loading" style="display:none"><i class="fa fa-refresh fa-spin fa-lg"></i></span>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- modal informacion mensaje -->
	<div class="modal" id="messageInfo" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-sm ">
			<div class="modal-content">
				<div class="modal-body">
					<div ng-show="infoUsers.length > 0">
						<h3><?php echo $this->lang->line('chat_infoMessage');?></h3>
						<p ng-repeat="info in infoUsers" ng-bind="info.completeName +': '+ info.date"></p>
					</div>
					<div ng-show="infoUsers.length == 0">
						<h3><?php echo $this->lang->line('chat_noViewed');?></h3>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

