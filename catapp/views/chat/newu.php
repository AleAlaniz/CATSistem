<?=$navbar?>
<div class="col-xs-12 flat-style">
	<div class="panel panel-default">
		<div class="panel-body">
			<form class="form-horizontal" method="POST" novalidate ng-submit="submitChat()">
				<div class="form-group">
					<input type="hidden" ng-repeat="user in users track by $index" name="user" value="{{user.userId}}" />
					<label class="col-sm-2 control-label"><?=$this->lang->line('general_user');?></label>
					<div class="col-sm-10 form-group">
						<div class="select-container" ng-click="selectClick($event);">
							<i class="fa fa-spin fa-refresh " style="position:absolute; top:13px;right:25px" ng-show="finding"></i>
							<span class="select-selected">
								<span class="select-selected-item" ng-repeat="user in users track by $index" ng-init="i=$index">
									<span cn-square-image cls="selected-item-image" url="/<?php echo FOLDERADD; ?>/users/profilephoto?userId={{user.userId}}&wah=200"></span>
									<span class="selected-item-label" ng-bind="user.completeName+' <'+user.userName+'>'" ></span>
									<span class="selected-item-delete no-selectable" ng-click="removeUser(i)">&times;</span>
								</span>
							</span>
							<input type="text" autocomplete="off" class="input-sm form-control select-input" ng-trim="true" ng-model="findLike" ng-change="searchUsers()" id="findInput" placeholder="<?php echo $this->lang->line('general_find');?>" />
						</div>
						<div class="select-options-container-super" ng-show="userOptions.length > 0">
							<div class="select-options-container">
								<div class="select-option no-selectable" ng-repeat="user in userOptions track by $index" ng-init="i=$index" ng-click="addUser(i)">
									<span cn-square-image cls="selected-option-image" url="/<?php echo FOLDERADD; ?>/users/profilephoto?userId={{user.userId}}&wah=200"></span>
									<span class="selected-option-label" ng-bind="user.completeName+' <'+user.userName+'>'"></span>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label for="message" class="col-sm-2 control-label"><?=$this->lang->line('chat_message');?> <span class="text-danger"> *</span></label>
					<div class="col-sm-10">
						<input autocomplete="off" type="text" class="form-control input-sm" id="message" name="message" ng-model="message" placeholder="<?=$this->lang->line('chat_message');?>" required>
					</div>
				</div>
				<div class="form-group text-center">
					<button type="submit" class="btn btn-green" id="chatu_create"><?=$this->lang->line('chat_start');?></button>
				</div>
			</form>

			<div class="modal fade" id="no-complete" tabindex="-1" role="dialog">
				<div class="modal-dialog modal-sm">
					<div class="modal-content">
						<div class="modal-body">
							<p><?php echo $this->lang->line('general_completeall')?> <strong class="text-danger">*</strong></p>
							<div class="text-right">
								<span class="btn btn-white" data-dismiss="modal" aria-label="Close"><?php echo $this->lang->line('general_accept'); ?></span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>