<?php echo $navbar;?>
<div class="col-xs-12 flat-style" style="margin-bottom:15px" ng-init="userId = '<?php echo $this->session->UserId; ?>'">
	<div class="panel panel-default">
		<div class="panel-body row chat-container-body">
			<div class="col-sm-4 col-md-3 chats-container">
				<?php
				if($this->Identity->Validate('chat/unique/new'))
				{
					?>
					<a class="btn btn-white btn-block text-ligthgreen b_r-0 p_20 btn-noshadow" style="" href="#/chat/newunique" ><?php echo $this->lang->line('chat_new');?> &nbsp;<i class="fa fa-plus fa-lg"></i></a>
					<?php 
				}
				?>
				<div class="form-inline">
					<div class="form-group" style="width: 100%; height:41px">
						<div class="input-group" style="width: 100%;border-bottom: 1px solid #DBDBDB ;border-top: 1px solid #DBDBDB ;">
							<div class="input-group-addon" style="border: 0;background:transparent"><i class="fa fa-search"></i></div>
							<input type="text" class="form-control" ng-model="chatfilter" style="border: 0" placeholder="<?php echo $this->lang->line('chat_searchchat');?>">
						</div>
					</div>
				</div>
				<div class="list-group chats" id="chat_chats">
					<a href="#/chat/unique/{{chat.chatuId}}" ng-class="{'active': chatIdSelected == chat.chatuId}" class="list-group-item hoverable" ng-repeat="chat in chats | filter : {user : {name: chatfilter, lastName : chatfilter}} | orderBy : '-timestamp'">
						<div class="media">
							<div class="media-left text-center">
								<div cn-square-image cls="profileImage profileImageChat pointer" url="/<?php echo FOLDERADD; ?>/users/profilephoto?userId={{chat.user.userId}}&wah=200"></div>
							</div>
							<div class="media-body">
								<h5 class="list-group-item-heading" ng-bind="chat.user.name+' '+chat.user.lastName"></h5>
								<span class="lastMessage" ng-class="{'text-muted': chat.noRead <= 0}" ng-bind-html="chat.message.message"></span>
								<div class="chat-date-container">
									<span class="chat-time time" ng-class="{'text-muted': chat.noRead <= 0}"  ng-bind="chat.date"></span>
								</div>
								<div class="chat-noread-container">
									<span class="chat-noread-dot " ng-show="chat.noRead > 0" ng-bind="chat.noRead"></span>
									<span class="btn btn-xs btn-options text-dark chats-delete"  data-target="#confirm-delete" data-toggle="modal" data-id="{{chat.chatuId}}" title="<?php echo $this->lang->line('chat_deletechatm');?>">
										<i class="fa fa-trash"></i>
									</span>
								</div>
							</div>
						</div>
					</a>
					<div class="list-group-item" ng-hide="(chats | filter : {user : {name: chatfilter, lastName : chatfilter}}).length > 0">
						<h5 class="text-muted text-center"><?php echo $this->lang->line('chat_empty'); ?></h5>
					</div>
				</div>
			</div>
			<div class="col-sm-8 col-md-9 text-center text-dark" ng-show="!chat">
				<h2 style="margin-top:115px"> 
					<i class="fa fa-comment-o fa-4x"></i>
					<br>
					<br>
					<?php echo $this->lang->line('chat_selectchat');?> 
				</h2>
			</div>
			<div class="col-sm-8 col-md-9 chat-container" ng-show="chat">
				<div class="media chat-header">
					<div class="media-left">
						<div cn-square-image cls="profileImage profileImageChat" url="/<?php echo FOLDERADD; ?>/users/profilephoto?userId={{chat.user.userId}}&wah=200"></div>
					</div>
					<div class="media-body">
						<h5 class="list-group-item-heading ">
							<span ng-bind="chat.user.name+' '+chat.user.lastName"></span>
						</h5>

						<span class="btn btn-sm pull-right btn-options text-dark" data-target="#confirm-delete" data-toggle="modal" data-id="{{chat.chatuId}}" >
							<i class="fa fa-trash"></i>
						</span>
					</div>
				</div>

				<div class="list-group chat-messages" schroll-bottom="chat.messages">
					<span class="btn btn-white btn-block b_r-0 text-red btn-noshadow" ng-hide="gettingOldMessages || fullOldMessages || chat.messages.length < 1" ng-click="getOldMessages()"><?=$this->lang->line('chat_loadmessages');?></span>
					<span class="btn-block center-block text-center text-red" ng-show="gettingOldMessages"><i class="fa fa-refresh fa-spin fa-3x"></i></span>
					<div class="list-group-item" ng-repeat="message in chat.messages  | orderBy : 'timestamp'">
						<div class="media"> 
							<div class="media-left" ng-show="message.userId == userId">
								<div cn-square-image cls="profileImage profileImageChat" url="/<?php echo FOLDERADD; ?>/users/profilephoto?userId={{message.userId}}&wah=200"></div>
								<span class="btn btn-sm btn-options btn-info" data-target="#messageInfo" data-toggle="modal" ng-click="getMessageInfo(message.hash_id)">
									<i class="fa fa-exclamation"></i>
								</span>
							</div>
							<div class="media-body" ng-class="{textRight: message.userId != userId}">
								<h5 class="list-group-item-heading" ng-bind="message.name+' '+message.lastName"></h5>
								<p ng-bind-html="message.message"></p> 
								<div ng-bind="message.date" class="text-muted small"></div>
							</div>
							<div class="media-right"  ng-show="message.userId != userId">
								<div cn-square-image cls="profileImage profileImageChat" url="/<?php echo FOLDERADD; ?>/users/profilephoto?userId={{message.userId}}&wah=200"></div>
								<!-- <span class="btn btn-sm btn-options btn-info" data-target="#messageInfo" data-toggle="modal" ng-click="getMessageInfo(message.hash_id)">
									<i class="fa fa-exclamation"></i>
								</span> -->
							</div>
						</div>
					</div>
				</div>
				<?php if ($this->Identity->Validate('chat/unique/sendmessage')){ ?>
					<form ng-if="!chat.deleted" class="form-inline chat-newmessage" ng-submit="sendMessage()" >
						<div class="form-group" >
							<div class="input-group">
								<div class="input-group-addon btn btn-white btn-newmessage-send btn-noshadow b_r-0" id="emojiPicker"><i class="fa fa-smile-o"></i></div>
								<!-- <input id="inputText" type="text" class="form-control" ng-model="chat.newmessage" placeholder="<?php echo $this->lang->line('chat_writemessage');?>"> -->
								<div id="inputText" class="form-control" contenteditable="true"></div>
								<div ng-click="sendMessage()" class="input-group-addon btn btn-white btn-newmessage-send btn-noshadow b_r-0"><i class="fa fa-paper-plane"></i></div>
							</div>
						</div>
					</form>
					<div ng-if="chat.deleted" class="bg-yelow">
						<h4 class="text-center"><?php echo $this->lang->line('chat_user_out');?></h4>
					</div>
				<?php } ?>
			</div>
		</div>
	</div>
	<div class="modal animated shake" id="confirm-delete" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-sm ">
			<div class="modal-content">
				<div class="modal-body">
					<p><?php echo $this->lang->line('chat_deleteuareyousure'); ?></p>
					<div class="text-right">
						<span class="btn btn-white" data-dismiss="modal" aria-label="Close"><?php echo $this->lang->line('general_no'); ?></span>
						<span class="btn btn-lightgreen" id="delete-yes"><?php echo $this->lang->line('general_yes'); ?></span>
						<span class="btn btn-lightgreen disabled" id="delete-loading" style="display:none"><i class="fa fa-refresh fa-spin fa-lg"></i></span>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- modal informacion mensaje -->
	<div class="modal" id="messageInfo" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-sm ">
			<div class="modal-content">
				<div class="modal-body">
					<div ng-show="infoUsers.length > 0">
						<h3><?php echo $this->lang->line('chat_infoMessage');?></h3>
						<p ng-repeat="info in infoUsers" ng-bind="info.completeName +': '+ info.date"></p>
					</div>
					<div ng-show="infoUsers.length == 0">
						<h3><?php echo $this->lang->line('chat_noViewed');?></h3>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>