<ul class="nav nav-tabs">
	<?php if($this->Identity->Validate('sites/details')) { ?>
	<li role="presentation" id="NavDetails"><a href="/<?=FOLDERADD?>/sites/details/<?=$siteId;?>"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> <span class="hidden-xs"><?=$this->lang->line('administration_details');?></span></a></li>
	<?php } ?>
	<?php if($this->Identity->Validate('sites/edit')) { ?>
	<li role="presentation" id="NavEdit"><a href="/<?=FOLDERADD?>/sites/edit/<?=$siteId;?>"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> <span class="hidden-xs"><?=$this->lang->line('administration_edit');?></span></a></li>
	<?php } ?>
	<?php if($this->Identity->Validate('sites/delete')) { ?>
	<li role="presentation" id="NavDelete"><a href="/<?=FOLDERADD?>/sites/delete/<?=$siteId;?>"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> <span class="hidden-xs"><?=$this->lang->line('administration_delete');?></span></a></li>
	<?php } ?>
</ul>