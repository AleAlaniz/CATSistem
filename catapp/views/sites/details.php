<ol class="breadcrumb">
	<li><a href="/<?=FOLDERADD?>/administration"><?=$this->lang->line('general_administration');?></a></li>
	<li><a href="/<?=FOLDERADD?>/sites"><?=$this->lang->line('general_sites');?></a></li>
	<li><a href="/<?=FOLDERADD?>/sites/details/<?=$siteId;?>"><?=encodeQuery($name);?></a></li>
	<li class="active"><?=$this->lang->line('administration_details');?></li>
</ol>
	<?php if (isset($_SESSION['flashMessage'])): ?>
	<div class="alert alert-success alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<strong><i class="fa fa-check"></i></strong> 
		<?php if ($_SESSION['flashMessage'] == 'edit'){
			echo $this->lang->line('sites_editmessage');
		}
		?>
	</div>
<?php endif; ?>
<div class="col-xs-12" style="margin-bottom:15px">
	<?=$navBar?>
</div>
<div class="col-xs-12">
<div class="panel panel-default">
	<div class="panel-heading">
		<strong><?=$this->lang->line('sites_details');?></strong>
	</div>
	<div class="panel-body">
		<dl class="dl-horizontal col-sm-6">
			<dt><?=$this->lang->line('sites_name');?></dt>
			<dd><?=encodeQuery($name)?></dd>
		</dl>
	</div>
</div>
</div>
<script type="text/javascript">
$('#nav_sites').addClass('active');
$('#NavDetails').addClass('active');
</script>