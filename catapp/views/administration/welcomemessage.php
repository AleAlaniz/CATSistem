<ol class="breadcrumb">
	<li><a href="/<?=FOLDERADD?>/administration"><?=$this->lang->line('general_administration');?></a></li>
	<li class="active"><?=$this->lang->line('general_welcomemessage');?></li>
</ol>
<?php if (isset($_SESSION['welcomeMessage'])): ?>
	<div class="alert alert-success alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<strong><i class="fa fa-check"></i></strong> 
		<?php if ($_SESSION['welcomeMessage'] == 'edit'){
			echo $this->lang->line('administration_welcomemessage_editmessage');
		}
		?>
	</div>
<?php endif; ?>
<div class="panel panel-default">
	<div class="panel-heading">
		<strong><?=$this->lang->line('general_welcomemessage');?> <?php if(isset($section)){echo $section->name;}?></strong>
	</div>
	<?php
	if (!isset($section)) {
		?>
		<table class="table table-hover">
			<thead>
				<tr class="active">
					<th><?=$this->lang->line('administration_sections_name');?></th>
				</tr>
			</thead>
			<tbody>
				<?php 
				if (count($sections) > 0) 
				{
					?>

					<?php 
					foreach ($sections as $section){
						?>
						<tr class="optionsUser">
							<td><a href="/<?=FOLDERADD?>/administration/welcomemessage/<?=$section->sectionId?>"><?=encodeQuery($section->name)?></a></td>
						</tr>
						<?php 
					}
				} 
				else 
				{ 
					?>
					<tr class="text-center">
						<td colspan="3"><i class="fa fa-list-alt"></i> <?=$this->lang->line('administration_sections_empty');?></td>
					</tr>
					<?php
				}?>
			</tbody>
		</table>
		<?php
	}
	else
	{
		?>
		<div class="panel-body">
			<form class="form-horizontal" method="POST" novalidate >
				<input type="hidden" name="sectionId" value="<?php echo $section->sectionId; ?>">
				<div class="form-group">
					<div class="col-sm-12">
						<textarea class="form-control" id="message" name="message" rows="10"><?php echo set_value('message', $message);?></textarea>
						<?php echo form_error('message'); ?>
					</div>
				</div>
				<hr>
				<div class="form-group text-center">
					<button type="submit" class="btn btn-success btn-sm"><?=$this->lang->line('general_save');?></button>
					<a href="/<?=FOLDERADD?>/administration/welcomemessage" class="btn btn-danger btn-sm"><?=$this->lang->line('general_cancel');?></a>
				</div>
			</form>
		</div>
		<?php
	}
	?>
</div>
<script type="text/javascript" src="/<?=APPFOLDERADD?>/libraries/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
	$('#nav_administration').addClass('active');
	<?php
	if (isset($section)) {
		?>
		tinymce.init({
			toolbar: "cut copy paste | alignleft aligncenter alignright alignjustify | bullist numlist  | outdent indent | forecolor backcolor fontsizeselect fontselect | bold italic underline | link unlink styleselect  image jbimages media | table",
			plugins: " image link media textcolor table jbimages ",
			selector: "#message",
			language: 'es',
			image_advtab: true,
			relative_urls: false,
			style_formats: [
			{
				title: 'Ajustar imagen',
				selector: 'img',
				styles: {
					'max-width': '100%', 
					'height': 'auto'
				}
			},
			{
				title: 'Ajustar imagen sin margen',
				selector: 'img',
				styles: {
					'max-width': 'calc(100% + 30px)', 
					'height': 'auto', 
					'margin': '-15px',
					'margin-bottom': '-25px'
				}
			}
			]
		});
		<?php 
	} 
	?>
</script>