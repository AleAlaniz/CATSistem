<ol class="breadcrumb">
	<li><a href="/<?=FOLDERADD?>/administration"><?=$this->lang->line('general_administration');?></a></li>
	<li class="active"><?=$this->lang->line('users_notify_complete_data');?></li>
</ol>
<?php if (isset($_SESSION['flashMessage'])): ?>
	<div class="alert alert-success alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<strong><i class="fa fa-check"></i></strong> 
		<?php if ($_SESSION['flashMessage'] == 'edit'){
			echo $this->lang->line('users_notify_complete_data_success');
		}
		?>
	</div>
<?php endif; ?>
<div class="panel panel-default">
	<div class="panel-heading">
		<strong><?=$this->lang->line('users_notify_complete_data');?></strong>
	</div>
	<div class="panel-body">
		<form class="form-horizontal" method="POST" novalidate >
			<div class="form-group">
				<label class="col-sm-2 control-label"><?php echo $this->lang->line('general_roles'); ?></label>
				<div class="col-sm-10">
					<?php
					for ($i=0; $i < count($roles); $i++) { 
						?>
						<div class="checkbox">
							<label>
								<input type="checkbox" name="roles[]" value="<?php echo $roles[$i]->roleId;?>">
								<?php echo encodeQuery($roles[$i]->name);?>
							</label>
						</div>
						<?php
					}
					?>
				</div>
			</div>
			<hr>
			<div class="form-group text-center">
				<button type="submit" class="btn btn-success"><?=$this->lang->line('general_send');?></button>
				<input type="submit"  class="btn btn-danger" value="<?=$this->lang->line('general_delete');?>" name="delete" >
			</div>
		</form>
	</div>
</div>
<script type="text/javascript">
	$('#nav_administration').addClass('active');
</script>