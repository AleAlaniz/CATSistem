<ol class="breadcrumb">
	<li class="active"><?=$this->lang->line('general_administration');?></li>
</ol>
<div class="panel panel-default">
	<div class="panel-heading"><strong><?=$this->lang->line('administration_title');?></strong></div>
	<ul class="list-group">
		<?php if($this->Identity->Validate('administration/welcomemessage')) { ?>
		<li class="list-group-item">
			<a class="pull-right btn btn-info btn-xs btn-linear-info" href="/<?=FOLDERADD?>/administration/welcomemessage"><i class="fa fa-cog "></i> <?=$this->lang->line('administration_config');?></a>
			<strong class="text-primary"><?=$this->lang->line('general_welcomemessage');?></strong> - <?=$this->lang->line('administration_welcomemessage_message');?>
		</li>
		<?php } ?>
		<?php if($this->Identity->Validate('administration/panelmessage')) { ?>
		<li class="list-group-item">
			<a class="pull-right btn btn-info btn-xs btn-linear-info" href="/<?=FOLDERADD?>/administration/panelmessage"><i class="fa fa-cog "></i> <?=$this->lang->line('administration_config');?></a>
			<strong class="text-primary"><?=$this->lang->line('administration_panelmessage_title');?></strong> - <?=$this->lang->line('administration_panelmessage_message');?>
		</li>
		<?php } ?>
		<?php if($this->Identity->Validate('administration/alert')) { ?>
		<li class="list-group-item">
			<a class="pull-right btn btn-info btn-xs btn-linear-info" href="/<?=FOLDERADD?>/administration/alert"><i class="fa fa-cog "></i> <?=$this->lang->line('administration_config');?></a>
			<strong class="text-primary"><?=$this->lang->line('general_alert');?></strong> - <?=$this->lang->line('administration_alert_message');?>
		</li>
		<?php } ?>
		<?php if($this->Identity->Validate('theme/index')) { ?>
		<li class="list-group-item">
			<a class="pull-right btn btn-info btn-xs btn-linear-info" href="/<?=FOLDERADD?>/themes"><i class="fa fa-cog"></i> <?=$this->lang->line('administration_config');?></a>
			<strong class="text-primary"><?=$this->lang->line('general_themes');?></strong> - <?=$this->lang->line('administration_themes_message');?>
		</li>
		<?php } ?>
		<?php if($this->Identity->Validate('background/manage')) { ?>
		<li class="list-group-item">
			<a class="pull-right btn btn-info btn-xs btn-linear-info" href="/<?=FOLDERADD?>/background"><i class="fa fa-cog"></i> <?=$this->lang->line('administration_config');?></a>
			<strong class="text-primary"><?=$this->lang->line('general_backgrounds');?></strong> - <?=$this->lang->line('administration_backgrounds_message');?>
		</li>
		<?php } ?>
		<?php if($this->Identity->Validate('users/index')) { ?>
		<li class="list-group-item">
			<a class="pull-right btn btn-info btn-xs btn-linear-info" href="/<?=FOLDERADD?>/users"><i class="fa fa-cog"></i> <?=$this->lang->line('administration_config');?></a>
			<strong class="text-primary"><?=$this->lang->line('general_users');?></strong> - <?=$this->lang->line('administration_users_message');?>
		</li>
		<?php } ?>
		<?php if($this->Identity->Validate('usergroups/index')) { ?>
		<li class="list-group-item">
			<a class="pull-right btn btn-info btn-xs btn-linear-info" href="/<?=FOLDERADD?>/usergroups"><i class="fa fa-cog"></i> <?=$this->lang->line('administration_config');?></a>
			<strong class="text-primary"><?=$this->lang->line('administration_usergroups');?></strong> - <?=$this->lang->line('administration_usergroups_message');?>
		</li>
		<?php } ?>
		<?php if($this->Identity->Validate('roles/index')) { ?>
		<li class="list-group-item">
			<a class="pull-right btn btn-info btn-xs btn-linear-info" href="/<?=FOLDERADD?>/roles"><i class="fa fa-cog"></i> <?=$this->lang->line('administration_config');?></a>
			<strong class="text-primary"><?=$this->lang->line('general_roles');?></strong> - <?=$this->lang->line('roles_message');?>
		</li>
		<?php } ?>
		<?php if($this->Identity->Validate('sections/index')) { ?>
		<li class="list-group-item">
			<a class="pull-right btn btn-info btn-xs btn-linear-info" href="/<?=FOLDERADD?>/sections"><i class="fa fa-cog"></i> <?=$this->lang->line('administration_config');?></a>
			<strong class="text-primary"><?=$this->lang->line('general_sections');?></strong> - <?=$this->lang->line('administration_sections_message');?>
		</li>
		<?php } ?>
		<?php if($this->Identity->Validate('sites/index')) { ?>
		<li class="list-group-item">
			<a class="pull-right btn btn-info btn-xs btn-linear-info" href="/<?=FOLDERADD?>/sites"><i class="fa fa-cog"></i> <?=$this->lang->line('administration_config');?></a>
			<strong class="text-primary"><?=$this->lang->line('general_sites');?></strong> - <?=$this->lang->line('sites_message');?>
		</li>
		<?php } ?>
		<?php if($this->Identity->Validate('subsections/index')) { ?>
		<li class="list-group-item">
			<a class="pull-right btn btn-info btn-xs btn-linear-info" href="/<?=FOLDERADD?>/subsections"><i class="fa fa-cog"></i> <?=$this->lang->line('administration_config');?></a>
			<strong class="text-primary"><?=$this->lang->line('general_subsections');?></strong> - <?=$this->lang->line('administration_subsections_message');?>
		</li>
		<?php } ?>
		<?php if($this->Identity->Validate('items/index')) { ?>
		<li class="list-group-item">
			<a class="pull-right btn btn-info btn-xs btn-linear-info" href="/<?=FOLDERADD?>/items"><i class="fa fa-cog"></i> <?=$this->lang->line('administration_config');?></a>
			<strong class="text-primary"><?=$this->lang->line('general_items');?></strong> - <?=$this->lang->line('administration_items_message');?>
		</li>
		<?php } ?>
		<?php if($this->Identity->Validate('users/resetpassword')) { ?>
		<li class="list-group-item">
			<a class="pull-right btn btn-info btn-xs btn-linear-info" href="/<?=FOLDERADD?>/users/resetpassword"><i class="fa fa-cog"></i> <?=$this->lang->line('administration_config');?></a>
			<strong class="text-primary"><?=$this->lang->line('administration_users_resetpassword');?></strong> - <?=$this->lang->line('administration_users_resetpasswordmessage');?>
		</li>
		<?php } ?>
		<?php if($this->Identity->Validate('administration/completedata')) { ?>
		<li class="list-group-item">
			<a class="pull-right btn btn-info btn-xs btn-linear-info" href="/<?=FOLDERADD?>/administration/completedata"><i class="fa fa-cog"></i> <?=$this->lang->line('administration_config');?></a>
			<strong class="text-primary"><?=$this->lang->line('users_notify_complete_data');?></strong> - <?=$this->lang->line('users_notify_complete_data_config');?>
		</li>
		<?php } ?>
	</ul>
</div>
<script type="text/javascript">
	$('#nav_administration').addClass('active');
</script>