<div class="page-title">
	<h3 class="title"><?=$this->lang->line('events_create');?></h3>
	<a href="#/notifications" class="btn btn-white pull-right"><i class="fa fa-chevron-left"></i> <?=$this->lang->line('general_goback');?></a>
</div>
<div class="col-xs-12 flat-style">
	<div class="panel panel-default">
		<div class="panel-body">
			<form class="form-horizontal" method="POST" novalidate enctype="multipart/form-data" ng-submit="submitEvent()" id="eventForm">
				<h3 style="margin-top: 0; margin-bottom: 0; padding-left: 10px"><?php echo $this->lang->line('general_info'); ?></h3>
				<hr style="margin-top: 10px; "/>

				<div class="form-group">
					<label for="title" class="col-sm-2 control-label"><?=$this->lang->line('administration_items_event_title');?><span class="text-danger"><strong> *</strong></span></label>
					<div class="col-sm-10">
						<input type="text" class="form-control input-sm" ng-model="event.title" id="title" name="title" placeholder="<?=$this->lang->line('administration_items_event_title');?>">
					</div>
				</div>

				<div class="form-group">
					<label for="userfile" class="col-sm-2 control-label"><?=$this->lang->line('attached_file');?><span class="text-danger strong"> *</span></label>
					<div class="col-sm-10">
						<input type="file" id="userfile" name="userfile" accept="image/gif, image/jpeg, image/png">
						<small><?=$this->lang->line('events_file_allowtypes')?></small><br>
						<small><?=$this->lang->line('events_file_allowsizes')?></small>
						<h5 class="text-warning"><?=$this->lang->line('event_size')?></h5>
					</div>
				</div>
				<hr style="margin-top: 10px; "/>
				
				<div class="col-md-12 col-md-offset-2">
					<filters></filters>
				</div>

				<div ng-repeat="f in addedFilters track by $index">
					<span class=" btn btn-xs btn-red m-r_5 btn-noshadow" ng-click="deleteElement($index); refresh()" title="<?=$this->lang->line('delete_filter')?>">
						<i class="fa fa-trash"></i>
					</span>
					<div class="col-md-12 well" ng-if="f.type == 'group'">
						<group division="divisions[0]"></group>
						<group division="divisions[1]"></group>
						<group division="divisions[2]"></group>
					</div>

					<div class="col-md-12 well" ng-if="f.type == 'civil-state'">
						<civil-states></civil-states>
					</div>

					<!-- sucrito teaming -->
					<div class="form-group col-md-12 well" ng-if="f.type == 'teaming'">
						<div class="checkbox checkbox-angular col-md-offset-2">
							<label ng-class="{check: checked}">
								<input type="checkbox" name="fromTeaming" ng-model="checked" ng-change="changed(checked)"><?=$this->lang->line('suscribed_teaming')?>
							</label>
						</div>
					</div>
					<!-- fin de sucrito teaming -->

					<!-- Por género -->
					<div class="form-group col-md-12 well" ng-if="f.type == 'genre'">
						<label class="col-md-1 col-md-offset-1" for="genre"><?=$this->lang->line('general_select_genre')?></label>
						<div class="col-md-10">
							<select class="form-control" id="genre" ng-model="gender">
								<option value="m"><?=$this->lang->line('gender_value_m')?></option>
								<option value="f"><?=$this->lang->line('gender_value_f')?></option>
								<option value="o"><?=$this->lang->line('gender_value_o')?></option>
							</select>	
						</div>	
					</div>
					<!-- fin de por género -->

					<!-- por turno -->
					<div class="form-group col-md-12 well" ng-if="f.type == 'turn'">
						<label class="col-md-1 col-md-offset-1" for="turn"><?=$this->lang->line('general_select_turn')?></label>
						<div class="col-md-10">
							<select class="form-control" id="turn" ng-model="turn">
								<option value="m"><?=$this->lang->line('general_turn_m')?></option>
								<option value="t"><?=$this->lang->line('general_turn_t')?></option>
								<option value="n"><?=$this->lang->line('general_turn_n')?></option>
								<option value="f"><?=$this->lang->line('general_turn_f')?></option>
							</select>	
						</div>	
					</div>
					<!-- fin de por turno -->

					<div class="form-group col-md-12 well" ng-if="f.type == 'birthDate'">
						<date-picker-search></date-picker-search>
					</div>
				</div>
				
				
				<div class="form-group text-center">
					<button type="button" class="btn btn-green " id="apply_filter" ng-if='addedFilters.length > 0' ng-click="applyFilters()"><?=$this->lang->line('apply_filter');?></button>
					<button type="button" class="btn btn-green " disabled='disabled' ng-if='loadingUsers'><i class='fa fa-refresh fa-spin fa-lg fa-fw'></i></button>
				</div>
				
				<searchUsers></searchUsers>
				<hr>

				<!-- Datepicker de fecha de inicio y fin -->
				<div class="form-group">
					<label for="startDate" class="col-sm-2 control-label"><?php echo $this->lang->line('general_start_date');?><span class="text-danger"><strong> *</strong></span></label>
					<div class="col-xs-10">
						<input type="text" class="form-control input-sm" role="date" id="startDate" name="startDate" value="<?php echo set_value('startDate')?>" placeholder="<?php echo $this->lang->line('general_start_date');?>">
					</div>
				</div>
				<div class="form-group">
					<label for="endDate" class="col-sm-2 control-label"><?php echo $this->lang->line('general_end_date');?><span class="text-danger"><strong> *</strong></span></label>
					<div class="col-xs-10">
						<input type="text"  class="form-control input-sm" role="date" id="endDate" name="endDate" value="<?php echo set_value('endDate')?>" placeholder="<?php echo $this->lang->line('general_end_date');?>">
					</div>
				</div>
				<!-- Fin de datepickers -->
				<hr>

				<!-- Datepicker de recordatorios -->
				<div class="form-group" ng-show="showReminder">
					<label for="reminders" class="col-sm-2 control-label"><?php echo $this->lang->line('event_reminders');?><span class="text-danger"></span></label>
					<i class="fa fa-question-circle text-indigo fa-lg" style="cursor: pointer; margin-left: 5px;" id="tooltip" title="<?php echo $this->lang->line('reminders_help');?>"></i>
					<div class="col-xs-10">
						<input type="text" class="form-control input-sm" id="reminders" name="reminders" placeholder="<?php echo $this->lang->line('event_reminders');?>">
					</div>
				</div>
				<!-- fin de datepicker de recordatorios -->

				<div class="form-group text-center">
					<button type="submit" class="btn btn-green " id="event_create" ng-show='!saving'><?=$this->lang->line('general_create');?></button>
					<button type="submit" class="btn btn-green " disabled='disabled' ng-show='saving'><i class='fa fa-refresh fa-spin fa-lg fa-fw'></i></button>
				</div>
			</form>

			<div class="modal fade" id="no-complete" tabindex="-1" role="dialog">
				<div class="modal-dialog modal-sm">
					<div class="modal-content">
						<div class="modal-body">
							<p><?php echo $this->lang->line('general_completeall')?> <strong class="text-danger">*</strong></p>
							<div class="text-right">
								<span class="btn btn-white" data-dismiss="modal" aria-label="Close"><?php echo $this->lang->line('general_accept'); ?></span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="/<?php echo APPFOLDERADD; ?>/libraries/script/jquery.form.js"></script>
<script src="/<?php echo APPFOLDERADD; ?>/libraries/script/bootstrap-datepicker.min.js"></script>
<script src="/<?php echo APPFOLDERADD; ?>/libraries/script/locales/bootstrap-datepicker.es.min.js"></script>