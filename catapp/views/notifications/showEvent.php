
<div style="margin-top:20px" ng-app="notifications" ng-controller="myApp" class="row">
	<br>
	<div class="col-xs-12 flat-style">
		<div class="panel panel-default">
			<div class="well well-sm text-center">
				<div class="text-center form-group">
					<img id="eventImg" src="../catapp/_notifications_images/{{event.image}}" alt="" style="display:none;">
				</div>	
				<div class="text-center form-group">
					<div class="form-group" ng-if="show">
						<label>
							<input type="checkbox" ng-if="!isCheckPressed" ng-click="pressed()">
							<?php echo $this->lang->line('notifications_confirmation')?>
						</label>
					</div>
					<div class="form-group">
						<a class="btn btn-default" ng-if="isCheckPressed" href="/<?php echo FOLDERADD; ?>/#/">
							<?php echo $this->lang->line('notifications_continue')?>
						</a>
					</div>
				</div>
				<div class="form-group" ng-if="!show && !loading">
					<h2 class="text-danger"><?php echo $this->lang->line('notifications_user_empty')?></h2>
					<a class="btn btn-default" href="/<?php echo FOLDERADD; ?>/#/">
						<?php echo $this->lang->line('notifications_continue')?>
					</a>
				</div>	
			</div>
		</div>
	</div>
</div>
<script src="/<?php echo APPFOLDERADD; ?>/libraries/script/angular.min.js"></script>
<script type="text/javascript">
	angular
	.module("notifications",[])
	.controller("myApp",["$scope","$http","$location",function($scope,$http,$location) {
		var url="<?php echo FOLDERADD ?>";
		$scope.event = {};
		$scope.isCheckPressed = false;
		$scope.show 		  = false; 
		$scope.loading        = true;
		getEventsData();

		function getEventsData() {
			$http({
				method 	: 'GET',
				url 	: '/'+url+'/event/geteventsforuser'
			}).success(function(data){
				$scope.event = data;
				$scope.loading = false;
				if ($scope.event == "null") {
					$scope.show    = false;
				} 
				else {
					$scope.show = true;
				}
			});
		}

		$scope.pressed = function(){
			$scope.isCheckPressed = true;
			$http({
				method 	: 'POST',
				url 	: '/'+url+'/event/checkview',
				data 	: {"eventReminderId": $scope.event.eventReminderId}
			}).success(function(data){
				if (data.status!='ok'){
					console.log("error");
				}
			});
		}
	}]);

	$(document).ready(function() {
		$('#eventImg').fadeIn(1500);
	});
</script>