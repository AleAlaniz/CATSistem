<div class="page-title">
	<h3 class="title"><?=$this->lang->line('notifications_create');?></h3>
	<a href="#/notifications" class="btn btn-white pull-right"><i class="fa fa-chevron-left"></i> <?=$this->lang->line('general_goback');?></a>
</div>
<div class="col-xs-12 flat-style">
	<div class="panel panel-default">
		<div class="panel-body">
			<form class="form-horizontal" method="POST" ng-submit="createNotification()">
				<h3 style="margin-top: 0; margin-bottom: 0; padding-left: 10px"><?php echo $this->lang->line('general_info'); ?></h3>
				<hr style="margin-top: 10px; "/>
				<div class="form-group">
					<label for="name" class="col-sm-2 control-label"><?=$this->lang->line('administration_users_name');?><span class="text-danger"><strong> *</strong></span></label>
					<div class="col-sm-10">
						<input type="text" class="form-control input-sm" id="title" name="title" ng-model="notification.title" placeholder="<?=$this->lang->line('administration_users_name');?>" >
					</div>
				</div>
				<hr>
				<div class="form-group text-center">
					<button type="submit" class="btn btn-green" id="notification_create"><?=$this->lang->line('general_create');?></button>
				</div>
			</form>
			<div class="modal fade" id="no-complete" tabindex="-1" role="dialog">
				<div class="modal-dialog modal-sm">
					<div class="modal-content">
						<div class="modal-body">
							<p><?php echo $this->lang->line('general_completeall')?> <strong class="text-danger">*</strong></p>
							<div class="text-right">
								<span class="btn btn-white" data-dismiss="modal" aria-label="Close"><?php echo $this->lang->line('general_accept'); ?></span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>