<link href="/<?php echo APPFOLDERADD; ?>/libraries/css/animate.min.css" rel="stylesheet">
<div class="page-title ">
	<h5 class="title"><?php echo $this->lang->line('suggestbox_title'); ?></h5>
	<?php
	if ($this->Identity->Validate('suggestbox/view')) {
		?>
		<div class="btn-group">
			<span aria-expanded="false" class="btn btn-white dropdown-toggle" data-toggle="dropdown">
				<?php echo $this->lang->line('suggestbox_view');?> 
				<span class="badge bg-lightgreen" ng-bind="suggestbox.noRead | number" ng-show="suggestbox.noRead > 0"></span> 	
			</span>
			<ul class="dropdown-menu dropdown-white dropdown-menu-scale">
				<li ng-repeat="section in sections">
					<a href="#/suggestbox/view/{{section.sectionId}}">
						<span ng-bind="section.name"></span>
						<span class="badge bg-lightgreen" ng-bind="section.noRead" ng-show="section.noRead > 0"></span>
					</a>
				</li>
				<li ng-show="sections.length > 1">
					<a href="#/suggestbox/view">
						<?php echo $this->lang->line('suggestbox_viewall'); ?> 
						<span class="badge bg-lightgreen" ng-bind="suggestbox.noRead | number" ng-show="suggestbox.noRead > 0"></span> 	
					</a>
				</li>
			</ul>
		</div>
		<?php
	}
	?>
	<a href="#/suggestbox/" class="btn btn-white pull-right"><i class="fa fa-chevron-left"></i> <?=$this->lang->line('general_goback');?></a>
</div>

<div class="col-xs-12 flat-style">
	<div class="alert bg-green" role="alert" ng-show="message != ''">
		<button type="button" class="close"  ng-click="message = ''"><span>&times;</span></button>
		<strong><i class="fa fa-check"></i> <span ng-bind="message"></span></strong> 
	</div>

	<div class="modal " id="confirm-take" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-sm animated flipInY ">
			<div class="modal-content">
				<div class="modal-body">
					<p><?php echo $this->lang->line('suggestbox_takeconfirm'); ?></p>
					<div class="text-right">
						<span class="btn btn-white" data-dismiss="modal" aria-label="Close"><?php echo $this->lang->line('general_no'); ?></span>
						<span class="btn btn-lightgreen" id="takeok"><?php echo $this->lang->line('general_yes'); ?></span>
						<span class="btn btn-lightgreen" disabled="true" id="suggestbox_taking" style="display:none"><i class='fa fa-refresh fa-spin fa-lg fa-fw'></i></span>

					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="panel ">
		<div class="panel-custom-heading">
			<h4 class="strong"><?php echo $this->lang->line('suggestbox_suggests'); ?> <span ng-bind="sectionName"></span></h4>
		</div>
		<div id="suggestbox_suggests_container" style="display:none">
			<div class="list-group">
				<div class="list-group-item" ng-repeat="suggest in suggests" style="word-break: normal;">
					<div class="media">
						<div class="media-body ">
							<h5 class="list-group-item-heading">
								<span ng-bind="suggest.completeName"></span>
								<span class="badge bg-pink pull-right" ng-show="suggest.takenBy"><i class="fa fa-check"></i> <?php echo $this->lang->line('suggestbox_takedby'); ?> <strong ng-bind="suggest.takenBy"></strong></span>
								<?php
								if ($this->Identity->Validate('suggestbox/take')) {
									?>
									<span class="btn btn-green btn-xs pull-right" role="take" ng-hide="suggest.takenBy" data-suggest="{{suggest.suggestboxId}}" data-target="#confirm-take" data-toggle="modal"><?php echo $this->lang->line('suggestbox_take')?></span>
									<?php
								}
								?>
							</h5>
							<strong><?php echo $this->lang->line('suggestbox_site'); ?></strong>: <span ng-bind="suggest.site"></span> -
							<strong><?php echo $this->lang->line('suggestbox_campaign'); ?></strong>: <span ng-bind="suggest.campaign"></span>
							<div>
								<strong><?php echo $this->lang->line('suggestbox_suggest'); ?></strong>:
								<span ng-bind="suggest.suggest"></span>
							</div>
							<div><small class="text-muted"><i class="fa fa-clock-o" style="width:auto !important"></i> <span ng-bind="suggest.timestamp"></span></small></div>
						</div>
					</div>
				</div>
			</div>
			<div class="panel-body text-center" ng-hide="suggests.length > 0"><?php echo $this->lang->line('suggestbox_empty'); ?></div>
		</div>
		<div class="text-center panel-body" id="suggestbox_suggests_loading">
			<i class='fa fa-refresh fa-spin fa-5x fa-fw' ></i>
		</div>
	</div>
</div>