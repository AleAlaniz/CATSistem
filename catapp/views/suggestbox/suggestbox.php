<div class="page-title ">
	<h5 class="title"><?php echo $this->lang->line('suggestbox_title'); ?></h5>
	<?php
	if ($this->Identity->Validate('suggestbox/view')) {
		?>
		<div class="btn-group">
			<span aria-expanded="false" class="btn btn-white dropdown-toggle" data-toggle="dropdown">
				<?php echo $this->lang->line('suggestbox_view');?> 
				<span class="badge bg-lightgreen" ng-bind="suggestbox.noRead | number" ng-show="suggestbox.noRead > 0"></span> 	
			</span>
			<ul class="dropdown-menu dropdown-white dropdown-menu-scale">
				<li ng-repeat="section in sections">
					<a href="#/suggestbox/view/{{section.sectionId}}">
						<span ng-bind="section.name"></span>
						<span class="badge bg-lightgreen" ng-bind="section.noRead" ng-show="section.noRead > 0"></span>
					</a>
				</li>
				<li ng-show="sections.length > 1">
					<a href="#/suggestbox/view">
						<?php echo $this->lang->line('suggestbox_viewall'); ?> 
						<span class="badge bg-lightgreen" ng-bind="suggestbox.noRead | number" ng-show="suggestbox.noRead > 0"></span> 	
					</a>
				</li>
			</ul>
		</div>
		<?php
	}
	?>
</div>
<div class="col-xs-12 flat-style">
	<div class="alert bg-green" role="alert" ng-show="message != ''">
		<button type="button" class="close"  ng-click="message = ''"><span>&times;</span></button>
		<strong><i class="fa fa-check"></i> <span ng-bind="message"></span></strong> 
	</div>
	<div class="panel">
		<img class="image-fix"  src="/<?php echo APPFOLDERADD; ?>/libraries/images/sugerencias.png">
		<div class="panel-body">
			<div class="modal fade" id="no-complete" tabindex="-1" role="dialog">
				<div class="modal-dialog modal-sm">
					<div class="modal-content">
						<div class="modal-body">
							<p><?php echo $this->lang->line('general_completeall')?> <strong class="text-danger">*</strong></p>
							<div class="text-right">
								<span class="btn btn-white" data-dismiss="modal" aria-label="Close"><?php echo $this->lang->line('general_accept'); ?></span>
							</div>
						</div>
					</div>
				</div>
			</div>
			<form method="POST" class="form-horizontal" ng-submit="sendSuggest()">
				<div class="form-group">
					<label for="site" class="col-sm-2 control-label"><?php echo $this->lang->line('suggestbox_site'); ?> <strong class="text-danger">*</strong></label>
					<div class="col-sm-10">
						<input class="form-control input-sm" name="site" ng-model="suggestdata.site" placeholder="<?php echo $this->lang->line('suggestbox_site'); ?>">
					</div>
				</div>
				<div class="form-group">
					<label for="campaign" class="col-sm-2 control-label"><?php echo $this->lang->line('suggestbox_campaign'); ?> <strong class="text-danger">*</strong></label>
					<div class="col-sm-10">
						<input class="form-control input-sm" name="campaign" ng-model="suggestdata.campaign" placeholder="<?php echo $this->lang->line('suggestbox_campaign'); ?>">
					</div>
				</div>
				<div class="form-group" style="padding-bottom:14px;">
					<label for="suggest" class="col-sm-2 control-label"><?=$this->lang->line('suggestbox_suggest')?> <strong class="text-danger">*</strong></label>
					<div class="col-sm-10">
						<textarea class="form-control input-sm" name="suggest" ng-model="suggestdata.suggest" placeholder="<?php echo $this->lang->line('suggestbox_suggest'); ?>"></textarea>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-12">
						<button class="btn btn-lightgreen pull-right" type="submit" id="suggestbox_sendsuggest"><?php echo $this->lang->line('general_send'); ?></button>
						<button class="btn btn-lightgreen pull-right" disabled="true" type="submit" id="suggestbox_sendingsuggest" style="display:none"><i class='fa fa-refresh fa-spin fa-lg fa-fw'></i></button>
					</div>
				</div>
			</form>
		</div>
	</div>
	<div class="panel">
		<div class="panel-custom-heading">
			<h4 class="strong"><?php echo $this->lang->line('suggestbox_mysuggest'); ?></h4>
		</div>
		<div id="suggestbox_mysuggest_container" style="display:none">
			<div class="panel-body text-center" ng-show="mysuggest == null"><?php echo $this->lang->line('suggestbox_empty'); ?></div>
			<ul class="list-group" ng-show="mysuggest != null">
				<li class="list-group-item" ng-repeat="suggest in mysuggest" style="word-break: normal;">
					<strong><?php echo $this->lang->line('suggestbox_site'); ?></strong>: <span ng-bind="suggest.site"></span> -
					<strong><?php echo $this->lang->line('suggestbox_campaign'); ?></strong>: <span ng-bind="suggest.campaign"></span>
					<span class="badge bg-pink pull-right" ng-show="suggest.takenBy"><i class="fa fa-check"></i> <?php echo $this->lang->line('suggestbox_takedby'); ?> <strong ng-bind="suggest.takenBy"></strong></span>
					<div>
						<strong><?php echo $this->lang->line('suggestbox_suggest'); ?></strong>:
						<span ng-bind="suggest.suggest"></span>
					</div>
					<div>
						<small class="text-muted">
							<i class="fa fa-clock-o" style="width:auto !important"></i>
							<span ng-bind="suggest.timestamp"></span>
						</small>
					</div>
				</li>
				<li class="list-group-item text-center" ng-show="mysuggest.length == 0"><?=$this->lang->line('suggestbox_empty')?></li>
			</ul>
		</div>
		<div class="text-center panel-body" id="suggestbox_mysuggest_loading">
			<i class='fa fa-refresh fa-spin fa-5x fa-fw' ></i>
		</div>
	</div>
</div>