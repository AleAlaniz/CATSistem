<ol class="breadcrumb">
	<li><a href="/<?=FOLDERADD?>/administration"><?=$this->lang->line('general_administration');?></a></li>
	<li><a href="/<?=FOLDERADD?>/subcampaigns"><?=$this->lang->line('subcampaigns_index');?></a></li>
	<li class="active"><?=$this->lang->line('administration_delete');?></li>
</ol>
<div class="col-xs-12" style="margin-bottom:15px">
	<?=$navBar?>
</div>
<div class="col-xs-12">
<div class="panel panel-default">
	<div class="panel-heading">
		<strong><?=$this->lang->line('subCampaign_delete');?></strong>
	</div>
	<div class="panel-body">
		<h4><?=$this->lang->line('subcampaign_delete_areyousure');?></h4>
		<hr>
		<dl class="dl-horizontal col-sm-6">
			<dt><?=$this->lang->line('subcampaign_name');?>:</dt>
			<dd><?=encodeQuery($name)?></dd>
		</dl>
		<form method="POST"  >
			<input type="hidden" name="subCampaignId" value="<?=$subCampaignId?>">
			<div class="form-group text-center col-xs-12">
				<button type="submit" class="btn btn-success btn-sm"><?=$this->lang->line('general_delete');?></button>
				<a href="/<?=FOLDERADD?>/subcampaigns" class="btn btn-danger btn-sm"><?=$this->lang->line('general_cancel');?></a>
			</div>
		</form>
	</div>
</div>
</div>
<script type="text/javascript">
$('#nav_subCampaigns').addClass('active');
$('#NavDelete').addClass('active');
</script>