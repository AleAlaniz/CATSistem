<ol class="breadcrumb">
	<li><a href="/<?=FOLDERADD?>/administration"><?=$this->lang->line('general_administration');?></a></li>
	<li><a href="/<?=FOLDERADD?>/subcampaigns"><?=$this->lang->line('subcampaigns_index');?></a></li>
	<li class="active"><?=$this->lang->line('administration_edit');?></li>
</ol>
<div class="col-xs-12" style="margin-bottom:15px">
	<?=$navBar?>
</div>
<div class="col-xs-12">
	<div class="panel panel-default">
		<div class="panel-heading">
			<strong><?=$this->lang->line('campaign_edit');?></strong>
		</div>
		<div class="panel-body">
			<form class="form-horizontal" method="POST" novalidate >
				<div class="form-group">
					<label for="name" class="col-sm-2 control-label"><?=$this->lang->line('campaign_name');?><span class="text-danger"><strong> *</strong></span></label>
					<div class="col-sm-10">
						<input type="text" class="form-control input-sm" id="name" name="name" value="<?php echo set_value('name', $name);?>" placeholder="<?=$this->lang->line('campaign_name');?>" required>
						<?php echo form_error('name'); ?>
					</div>
				</div>

				<hr>
				<div class="form-group text-center">
					<button type="submit" class="btn btn-success btn-sm"><?=$this->lang->line('general_save');?></button>
					<a href="/<?=FOLDERADD?>/subcampaigns" class="btn btn-danger btn-sm"><?=$this->lang->line('general_cancel');?></a>
				</div>
			</form>
		</div>
	</div>
</div>
<script type="text/javascript">
$('#nav_subCampaign').addClass('active');
$('#NavEdit').addClass('active');
</script>