<div class="page-title">
	<h3 class="title"><?php echo $this->lang->line('newsletter_title'); ?></h3>
	<?php if($this->Identity->Validate('newsletter/create') || $this->Identity->Validate('newsletter/createcategory')) 
	{ 
		?>
		<div class="btn-group">
			<span aria-expanded="false" class="btn btn-white dropdown-toggle" data-toggle="dropdown"><i class="fa fa-plus text-muted"></i> <?php echo $this->lang->line('general_create')?></span>
			<ul class="dropdown-menu dropdown-white dropdown-menu-scale">
				<?php if($this->Identity->Validate('newsletter/create')) 
				{ 
					?>
					<li><a href="#/newsletter/create"><i class="fa fa-newspaper-o"></i>&nbsp; <?php echo $this->lang->line('newsletter_create')?></a></li>
					<?php 
				}
				?>
				<?php if($this->Identity->Validate('newsletter/createcategory')) 
				{ 
					?>
					<li><a href="#/newsletter/createcategory"><i class="fa fa-sitemap"></i>&nbsp; <?php echo $this->lang->line('newsletter_createcategory')?></a></li>
					<?php 
				}
				?>
			</ul>
		</div>
		<?php 
	}
	?>
</div>
<div class="col-xs-12 flat-style" ng-init="deletecategory = <?php echo "'".$this->lang->line('newsletter_categorydeleteareyousure')."'"; ?>; deletenewsletter = <?php echo "'".$this->lang->line('newsletter_newsdeleteareyousure')."'"; ?>">
	<div class="alert bg-green" role="alert" ng-show="message.message != '' && message.message != null">
		<button type="button" class="close"  ng-click="message.setMessage('', true)"><span>&times;</span></button>
		<strong><i class="fa fa-check"></i> <span ng-bind="message.message"></span>
		</strong> 
	</div>
	<div class="modal animated shake" id="confirm-delete" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-sm ">
			<div class="modal-content">
				<div class="modal-body">
					<p role="message"></p>
					<div class="text-right">
						<span class="btn btn-white" data-dismiss="modal" aria-label="Close"><?php echo $this->lang->line('general_no'); ?></span>
						<span class="btn btn-lightgreen" role="delete"><?php echo $this->lang->line('general_yes'); ?></span>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="panel">
		<div class="panel-body" style="padding:0">
			<img class="image-fix"  src="/<?=APPFOLDERADD;?>/libraries/images/e-news_header.png" style="margin-bottom: 15px;">
			<div role="containt" style="display : none">
				<div ng-repeat="category in categories" class="panel" ng-class="'panel-'+category.color" style="margin-bottom:0">
					<div class="panel-heading shadow-hover pointer" role="toggle-body">
						<span class="badge pull-right" ng-bind="category.noRead" ng-show="category.noRead > 0"></span>
						<?php
						if($this->Identity->Validate('newsletter/deletecategory')) 
						{ 
							?>
							<span class="pull-right btn btn-xs white" style="margin-right:10px" role="deletebutton" data-target="#confirm-delete" data-toggle="modal" data-id="{{category.newslettercategoryId}}" data-type="category"><i class="fa fa-times  fa-lg"></i></span>
							<?php
						}
						?>
						<?php
						if($this->Identity->Validate('newsletter/editcategory')) 
						{ 
							?>
							<a href="#/newsletter/editcategory/{{category.newslettercategoryId}}" class="pull-right btn btn-xs  m-r_10 white"><i class="fa fa-pencil fa-lg"></i></a>
							<?php
						}
						?>
						<strong ng-bind="category.name"></strong>
					</div>
					<div class="panel-body" style="display:none">
						<div class="panel" ng-class="'panel-'+(newsletter.color || category.color)" ng-repeat="newsletter in category.newsletters">
							<div class="panel-heading pointer shadow-hover" role="toggle-body">
								<strong>
									<span ng-bind="newsletter.title"></span>
									<?php
									if($this->Identity->Validate('newsletter/edit')) 
									{ 
										?>
										<span ng-bind="' ( '+newsletter.linksName+' ) '"></span>
										<?php
									}
									?>
								</strong>

								<?php
								if($this->Identity->Validate('newsletter/delete')) 
								{ 
									?>
									<span class="pull-right btn btn-xs" style="margin-right:10px" role="deletebutton" data-target="#confirm-delete" data-toggle="modal" data-id="{{newsletter.newsletterId}}" data-type="newsletter"><i class="fa fa-times fa-lg"></i></span>
									<?php
								}
								?>
								<?php
								if($this->Identity->Validate('newsletter/edit')) 
								{ 
									?>
									<a href="#/newsletter/edit/{{newsletter.newsletterId}}" class="pull-right btn btn-xs white m-r_10"><i class="fa fa-pencil fa-lg"></i></a>
									<?php
								}
								?>
								<?php
								if($this->Identity->Validate('newsletter/edit')) 
								{ 
									?>
									<a class="pull-right btn btn-xs white m-r_10" ng-click="viewNewsletterReport(newsletter)"><i class="fa fa-eye fa-lg" aria-hidden="true"></i></a>
									<?php
								}
								?>
							</div>
							<div class="panel-body" style="display:none">
								<div class="text-center">
								<a href="/<?=FOLDERADD?>/newsletter/download/{{newsletter.newsletterId}}" class="btn btn-dark btn-sm" target="_blank" ng-show="newsletter.file != null" style="margin-bottom:10px">
										<i class="fa fa-download fa-lg"></i> <?php echo $this->lang->line('newsletter_downloadfile'); ?>
									</a>
								</div>
								<div ng-bind-html="newsletter.newsletter | HtmlSanitize"></div>

							</div>
						</div>
						<div class="alert bg-dark" role="alert" ng-hide="category.newsletters.length > 0">
							<i class="fa fa-info-circle fa-lg"></i>
							<?=$this->lang->line('newsletter_empty')?>
						</div>
					</div>
				</div>
				<div class="alert bg-dark" role="alert" ng-hide="categories.length > 0" style="margin: 0 15px">
					<i class="fa fa-info-circle fa-lg"></i>
					<?php echo $this->lang->line('newsletter_empty');?>
				</div>
			</div>
			<div class="text-center" role="loading">
				<i class='fa fa-refresh fa-spin fa-4x fa-fw dark' ></i>
			</div>
			<img class="image-fix"  src="/<?=APPFOLDERADD;?>/libraries/images/e-news_pie.png" style="margin-top: 15px;">
		</div>
	</div>
	<div class="modal animated" tabindex="-1" id="doc-details">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<dl class="dl-horizontal">
						<dt>Título</dt>
						<dd ng-bind="selectednewsletter.title"></dd>
					</dl>
					
						<p class="text-center" ng-show="loadingReport && !report" style="margin-top: 10px;"><i class="fa fa-refresh fa-spin fa-3x"></i></p>
						
						<span class="btn btn-block btn-dark"  style="margin-top: 10px;" ng-show="!report && !loadingReport" ng-click="getReport(report,'read')">
							Ver reporte
						</span>

					<div ng-if="report != null" style="margin-top: 15px">
						
						<ul class="nav nav-tabs">
							<li role="presentation" ng-class="{active : reportOption == 'read'}"><a href="" ng-click="getReport(report,'read')">Leyeron el documento</a></li>
						</ul>

						<p class="text-center" ng-show="loadingReport" style="margin-top: 10px;"><i class="fa fa-refresh fa-spin fa-3x"></i></p>
						<div ng-show="!loadingReport" style="margin-top:15px">
							<strong>
								<?php echo $this->lang->line('corporatedocs_total'); ?> 
								<span ng-bind="report.length"></span>
							</strong>
							<table class="table">
								<thead>
									<tr>
										<th><?php echo $this->lang->line('general_user'); ?> </th>
										<th><?php echo $this->lang->line('general_section'); ?> </th>
									</tr>
								</thead>
								<tbody>

									<tr ng-repeat="user in report track by $index">
										<td ng-bind="user.name + ' ' + user.lastName"></td>
										<td ng-bind="user.section"></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>