<ol class="breadcrumb">
    <?php if($this->Identity->Validate('referred/index')  && $this->session->sectionId == 6) { ?>
    <li id="referred" class="active"><a href="#/referred"><?php echo $this->lang->line('referred_title') ?></a></li>
    <?php } ?>

    <?php if($this->Identity->Validate('referred/admin')  && $this->session->sectionId == 6){ ?>
        <li id="referredList" class="active"><a href="#/referred/referredList"><?php echo $this->lang->line('referred_sended') ?></a></li>
    <?php } ?>
</ol>