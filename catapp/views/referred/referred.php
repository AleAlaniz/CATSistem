<div class="container-fluid">
	<div class="panel panel-default center block">
		<div class="panel-body" id="referred_description">
			<?php echo $this->lang->line('referred_desc') ?>
		</div>
	</div>
	<div class="buttons">
		<a href="#/referred/sendReferred" class="btn btn-white bg-lightblue">
			<?=$this->lang->line('referred_send');?>
		</a>
	</div>	
</div>
<script> $('#referred').replaceWith("<li><?php echo $this->lang->line('referred_title') ?></li>");</script>
