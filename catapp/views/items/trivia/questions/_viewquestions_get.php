<?php 
if (count($questions) <= 0) {
	echo $this->lang->line('administration_items_trivia_question_empty');

}
else{ 
	foreach ($questions as $obectFor) 
	{
		?>	
		<div class="panel panel-default">
			<div class="panel-heading">
				<h6 class="panel-title-nomargin">
					<i class="fa fa-question-circle fa-fw fa-lg"></i>
					<strong>
						<?=encodeQuery($obectFor->question)?> | <?=$this->lang->line('administration_items_trivia_question_type');?>: 
						<?php
						switch ($obectFor->type) {
							case 1:
							echo $this->lang->line('administration_items_trivia_question_optionunique');
							break;
							case 2:
							echo $this->lang->line('administration_items_trivia_question_severaloptions');
							break;
							case 3:
							echo $this->lang->line('administration_items_trivia_question_input');
							break;
							case 4:
							echo $this->lang->line('administration_items_trivia_question_trueorfalse');
							break;
							default:
							break;
						}
						?>
					</strong>
					<?php
					if (($obectFor->type == 1 || $obectFor->type == 2) && $this->Identity->Validate('items/item/trivia/actions/addanswer')) {
						?>
						<a href="/<?=FOLDERADD?>/items/item/<?=$itemId?>?cmd=trivia&action=addanswer&itemquestionId=<?=$obectFor->itemtriviaquestionId?>" class="btn btn-warning btn-xs"><i class="fa fa-plus-circle"></i> <?=$this->lang->line('administration_items_trivia_question_addanswer')?></a>
						<?php
					}
					if ($this->Identity->Validate('items/item/trivia/actions/deletequestion')) {
						?>
						<button id="deleteId-<?=$obectFor->itemtriviaquestionId?>" class="btn btn-xs pull-right"><i class="text-danger fa fa-times fa-lg"></i></button>		
						<?php } ?>
					</h6>
				</div>
				<div class="panel-body">
					<?php if ($this->Identity->Validate('items/item/trivia/actions/deletequestion')) { ?>
					<div class="alert alert-danger text-center" role="alert" id="deletealertId-<?=$obectFor->itemtriviaquestionId?>" style="display:none;" 	>
						<div><strong><?=$this->lang->line('administration_items_trivia_question_deleteareyousure')?></strong></div>
						<a href="/<?=FOLDERADD?>/items/item/<?=$itemId?>?cmd=trivia&action=deletequestion&itemquestionId=<?=$obectFor->itemtriviaquestionId?>" class="btn btn-default btn-sm"><?=$this->lang->line('general_delete')?></a>
						<button id="deletecancelId-<?=$obectFor->itemtriviaquestionId?>" href="#" class="btn btn-default btn-sm"><?=$this->lang->line('general_cancel')?></button>
					</div>
					<?php } ?>
					<?php if($obectFor->description != NULL){ echo $obectFor->description;} else { echo $this->lang->line('administration_items_trivia_question_nodescription');}?>
				</div>
				<?php

				if ($obectFor->type == 4)
				{
					?>
					<div class="list-group">
						<div class="list-group-item">
							<form class="form-inline row" method="POST">
								<input type="hidden" value="<?=$obectFor->itemtriviaquestionId?>" name="itemtriviaquestionId">
								<div class="radio col-sm-2">
									<?=$this->lang->line('administration_items_trivia_question_correctanswer')?>
								</div>
								<div class="radio col-sm-2">
									<label>
										<input type="radio" name="istrue" value="TRUE" <?php if($obectFor->trueorfalse == 'TRUE'){echo 'checked';} ?>>
										<?=$this->lang->line('general_true')?>
									</label>
								</div>
								<div class="radio col-sm-2">
									<label>
										<input type="radio" name="istrue" value="FALSE" <?php if($obectFor->trueorfalse == 'FALSE'){echo 'checked';} ?> >
										<?=$this->lang->line('general_false')?>
									</label>
								</div>
								<div class="form-group col-sm-4">
									<label for="value"><?=$this->lang->line('administration_items_trivia_question_value')?></label> 
									<input type="number" class="form-control input-sm" id="value" name="value" value="<?=$obectFor->value?>" placeholder="<?=$this->lang->line('administration_items_trivia_question_value')?>">
								</div>
								<div class="form-group col-sm-2 text-right">
									<input type="submit" class="btn btn-primary btn-sm" value="<?=$this->lang->line('general_save')?>" name="actioneditquestion">
								</div>
							</form>
						</div>
					</div>
					<?php
				}
				elseif (count($obectFor->answers) > 0) { 
					?>
					<div class="list-group">
						<?php
						
						foreach ($obectFor->answers as $answer) 
						{
							?>
							<div class="list-group-item">
								<form class="form-inline row" method="POST">
									<input type="hidden" value="<?=$answer->itemtriviaanswerId?>" name="itemtriviaanswerId">
									<input type="hidden" value="<?=$obectFor->itemtriviaquestionId?>" name="itemtriviaquestionId">
									<input type="hidden" value="<?=$obectFor->itemtriviaId?>" name="itemtriviaId">
									<div class="form-group col-sm-4">
										<label for="answer"><?=$this->lang->line('administration_items_trivia_question_answer')?></label>
										<input type="text" class="form-control input-sm" id="answer" name="answer" value="<?=$answer->answer?>" placeholder="<?=$this->lang->line('administration_items_trivia_question_answer')?>">
									</div>
									<div class="form-group col-sm-3">
										<label for="value"><?=$this->lang->line('administration_items_trivia_question_value')?></label> 
										<input type="number" class="form-control input-sm" id="value" name="value" value="<?=$answer->value?>" placeholder="<?=$this->lang->line('administration_items_trivia_question_value')?>">
									</div>
									<div class="checkbox col-sm-3">
										<label>
											<input type="checkbox" id="istrue" name="istrue" <?php if($answer->istrue == 'TRUE'){ echo 'checked';}?> > <?=$this->lang->line('administration_items_trivia_question_correctanswer')?>
										</label>
									</div>
									<div class="form-group col-sm-2 text-right">
										<input type="submit" class="btn btn-primary btn-sm" value="<?=$this->lang->line('general_save')?>" name="actioneditanswer">
										<input type="submit" class="btn btn-danger btn-sm" value="<?=$this->lang->line('general_delete')?>" name="actiondeleteanswer">
									</div>
								</form>
							</div>
							<?php
						}
						
						?>
					</div>
					<?php
				}
				?>
			</div>

			<?php	
		} 
		?>
		<script type="text/javascript">
		<?php
		if ($this->Identity->Validate('items/item/trivia/actions/deletequestion')){
			foreach ($questions as $obectFor) 
			{
				?>
				$('#deletecancelId-<?=$obectFor->itemtriviaquestionId?>').click(
					function(){
						$('#deletealertId-<?=$obectFor->itemtriviaquestionId?>').slideUp();
					}
					);
				$('#deleteId-<?=$obectFor->itemtriviaquestionId?>').click(
					function(){
						$('#deletealertId-<?=$obectFor->itemtriviaquestionId?>').slideDown();
					}
					);
				<?php	
			}
		} 
		?>
		</script>
		<?php
	}	
	?>