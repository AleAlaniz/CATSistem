<form class="form-horizontal" method="POST" novalidate>
	<div class="form-group">
		<label for="question" class="col-sm-2 control-label"><?=$this->lang->line('administration_items_trivia_question_question');?> <strong class="text-danger">*</strong></label>
		<div class="col-sm-10">
			<input type="text" class="form-control input-sm" id="question" name="question" value="<?=set_value('question')?>" placeholder="<?=$this->lang->line('administration_items_trivia_question_question');?>" required>
			<?php echo form_error('question'); ?>
		</div>
	</div>
	<div class="form-group">
		<label for="type" class="col-sm-2 control-label"><?=$this->lang->line('administration_items_trivia_question_type');?><span class="text-danger"><strong> *</strong></span></label>
		<div class="col-sm-10">
			<select class="form-control input-sm" name="type" id="type" >
				<option value="1" <?php echo  set_select('type', '1'); ?> ><?=$this->lang->line('administration_items_trivia_question_optionunique');?></option>
				<option value="2" <?php echo  set_select('type', '2'); ?> ><?=$this->lang->line('administration_items_trivia_question_severaloptions');?></option>
				<option value="3" <?php echo  set_select('type', '3'); ?> ><?=$this->lang->line('administration_items_trivia_question_input');?></option>
				<option value="4" <?php echo  set_select('type', '4'); ?> ><?=$this->lang->line('administration_items_trivia_question_trueorfalse');?></option>
			</select>
			<?php echo form_error('type'); ?>
		</div>
	</div>
	<div class="form-group">
		<label for="description" class="col-sm-2 control-label"><?=$this->lang->line('administration_items_trivia_description');?></label>
		<div class="col-sm-10">
			<textarea class="form-control" id="description" name="description" rows="3"><?php echo set_value('description');?></textarea>
			<?php echo form_error('description'); ?>
		</div>
	</div>
	<div class="form-group text-center">
		<input type="submit" class="btn btn-success btn-sm" value="<?=$this->lang->line('administration_items_trivia_question_addquestion');?>"></input>
		<a href="/<?=FOLDERADD?>/items/item/<?=$itemId?>?cmd=trivia&action=editquestions&itemtriviaId=<?=$this->input->get('itemtriviaId')?>" class="btn btn-danger btn-sm"><?=$this->lang->line('general_cancel');?></a>
	</div>
</form>			
<script type="text/javascript" src="/<?=APPFOLDERADD?>/libraries/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
tinymce.init({
	toolbar: "cut copy paste | alignleft aligncenter alignright alignjustify | bullist numlist  | outdent indent | forecolor backcolor fontsizeselect  | bold italic underline | link unlink image media | table",
	plugins: "image link media textcolor table",
	language: 'es',
	selector: "#description"
});
</script>