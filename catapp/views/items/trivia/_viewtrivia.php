<div class="item-header">
	<h3>
		<span><?=$this->lang->line('administration_items_trivia')?></span> 
		<?php if($this->Identity->Validate('items/item/trivia/actions/create')){?>
		<a class="btn btn-success btn-xs" href="/<?=FOLDERADD?>/items/item/<?=$itemId?>?cmd=trivia&action=create"><i class="fa fa-plus-square-o"></i> <?=$this->lang->line('administration_items_trivia_create');?></a>
		<?php 
		}
		if ($this->input->get('action') && $this->input->get('action') == 'editquestions') { ?>
		<a class="btn btn-default btn-xs" href="/<?=FOLDERADD?>/items/item/<?=$itemId?>?cmd=trivia&action=createquestion&itemtriviaId=<?=$this->input->get('itemtriviaId')?>"><i class="fa fa-plus-square-o"></i> <?=$this->lang->line('administration_items_trivia_question_addquestion');?></a>
		<?php	
		}
		?>
	</h3>
	<hr>
</div>
<?=$ActionView?>
<script type="text/javascript">
$('#triviarightBar').addClass('active');
</script>