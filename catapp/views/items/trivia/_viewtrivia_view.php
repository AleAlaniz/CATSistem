<?php
echo '<h2>'.$viewtrivia->name.'</h2>';
if($viewtrivia->description != NULL) {echo '<p>'.$viewtrivia->description.'</p>';} 
if (count($viewtrivia->questions) <= 0) {
	echo '<p>'.$this->lang->line('administration_items_trivia_question_empty').'</p>';
	?>
	<p>
		<a href="/<?=FOLDERADD?>/items/item/<?=$itemId?>?cmd=trivia" class="btn btn-primary btn-sm">
			<i class="fa fa-chevron-left fa-lg"></i> <?=$this->lang->line('general_back')?>
		</a>
	</p>
	<?php
}elseif ($viewtrivia->starttime  > time()) {
	echo '<p>'.$this->lang->line('administration_items_trivia_question_nostart').'</p>';
	?>
	<p>
		<a href="/<?=FOLDERADD?>/items/item/<?=$itemId?>?cmd=trivia" class="btn btn-primary btn-sm">
			<i class="fa fa-chevron-left fa-lg"></i> <?=$this->lang->line('general_back')?>
		</a>
	</p>
	<?php
}elseif ($viewtrivia->endtime  < time()) {
	echo '<p>'.$this->lang->line('administration_items_trivia_question_endtrivia').'</p>';
	?>
	<p>
		<a href="/<?=FOLDERADD?>/items/item/<?=$itemId?>?cmd=trivia" class="btn btn-primary btn-sm">
			<i class="fa fa-chevron-left fa-lg"></i> <?=$this->lang->line('general_back')?>
		</a>
	</p>
	<?php
}
else
{
	?>
	<form class="form-horizontal" method="POST" novalidate >
		<input type="hidden" name="attempt" value="TRUE">
		<?php 
		foreach ($viewtrivia->questions as $question) {
			?>

			<div class="panel panel-default">
				<div class="panel-heading ">
					<h6 class="panel-title-nomargin">
						<i class="fa fa-question-circle fa-fw fa-lg"></i>
						<strong>
							<?=encodeQuery($question->question)?>
						</strong>
					</h6>
				</div>
				<div class="panel-body">
					<?php
					if($question->description != NULL) {echo '<p>'.$question->description.'</p>';} 
					if ($question->type == 1) {
						foreach ($question->answers as $answer) {
							
							?>
							<div class="radio">
								<label>
									<input type="radio" name="questionId-<?=$question->itemtriviaquestionId?>" value="<?=$answer->itemtriviaanswerId?>">
									<?=encodeQuery($answer->answer)?>
								</label>
							</div>
							<?php
						}
					}
					elseif ($question->type == 2) {
						foreach ($question->answers as $answer) {
							
							?>
							<div class="checkbox">
								<label>
									<input type="checkbox" name="answerId-<?=$answer->itemtriviaanswerId?>" value="<?=$answer->itemtriviaanswerId?>">
									<?=encodeQuery($answer->answer)?>
								</label>
							</div>
							<?php
						}
					}
					elseif ($question->type == 3) {
						?>
								<input type="text" class="form-control input-sm" name="questionId-<?=$question->itemtriviaquestionId?>" placeholder="<?=encodeQuery($question->question);?>">
						<?php
					}
					elseif ($question->type == 4) {
						?>
						<div class="radio">
							<label>
								<input type="radio" name="questionId-<?=$question->itemtriviaquestionId?>" value="TRUE">
								<?=$this->lang->line('general_true')?>
							</label>
						</div>
						<div class="radio">
							<label>
								<input type="radio" name="questionId-<?=$question->itemtriviaquestionId?>" value="FALSE">
								<?=$this->lang->line('general_false')?>
							</label>
						</div>
						<?php
					}
					?>
				</div> 
			</div>

			<?php
		}
		?>
		<div class="form-group text-center">
			<input type="submit" class="btn btn-info btn-sm" value="<?=$this->lang->line('general_end');?>"></input>
		</div>
	</form>
	<?php
}
?>