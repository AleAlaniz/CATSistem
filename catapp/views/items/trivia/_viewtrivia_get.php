<?php if (isset($_SESSION['itemtriviaMessage']) && $this->Identity->Validate('items/item/trivia/actions')){ ?>
<div class="alert alert-success alert-dismissible" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	<strong><i class="fa fa-check"></i></strong> 
	<?php if ($_SESSION['itemtriviaMessage'] == 'create'){
		echo $this->lang->line('administration_items_trivia_createmessage');
	} elseif ($_SESSION['itemtriviaMessage'] == 'delete'){
		echo $this->lang->line('administration_items_trivia_deletemessage');
	}elseif ($_SESSION['itemtriviaMessage'] == 'edit'){
		echo $this->lang->line('administration_items_trivia_editmessage');
	}
	?>
</div>
<?php 
}

if (count($trivias) <= 0) {
	echo $this->lang->line('administration_items_trivia_empty');
}
else{ 
	foreach ($trivias as $trivia) {?>

	<div class="panel panel-default">
		<div class="panel-heading">
			<h6 class="panel-title-nomargin">

				<i class="fa fa-check-square-o fa-fw fa-lg"></i>
				<strong>
					<?=encodeQuery($trivia->name)?>
				</strong>
				<?php if($this->Identity->Validate('items/item/trivia/actions')){
					if ($trivia->active == 'TRUE')
					{
						$context = 'danger';
						$icon = 'eye-slash';
						$text = $this->lang->line('administration_items_trivia_deactivetrivia');
					}
					else
					{
						$context = 'default';
						$icon = 'eye';
						$text = $this->lang->line('administration_items_trivia_activetrivia');
					}
					if($this->Identity->Validate('items/item/trivia/actions/editquestions')){
					?>
					<a href="/<?=FOLDERADD?>/items/item/<?=$itemId?>?cmd=trivia&action=editquestions&itemtriviaId=<?=$trivia->itemtriviaId?>" class="btn btn-default btn-xs">
						<i class="fa fa-question-circle"></i> <?=$this->lang->line('administration_items_trivia_editquestions')?>
					</a>
					<?php }
					if($this->Identity->Validate('items/item/trivia/actions/togglestate')){
					?>
					<a href="/<?=FOLDERADD?>/items/item/<?=$itemId?>?cmd=trivia&action=togglestate&itemtriviaId=<?=$trivia->itemtriviaId?>" class="btn btn-<?=$context?> btn-xs">
						<i class="fa fa-<?=$icon?>"></i> <?=$text?>
					</a>
					<?php }
					if($this->Identity->Validate('items/item/trivia/actions/delete')){
					?>
					<button id="deleteId-<?=$trivia->itemtriviaId?>" class="btn btn-xs pull-right"><i class="text-danger fa fa-times fa-lg"></i></button>	
					<?php }
					if($this->Identity->Validate('items/item/trivia/actions/edit')){
					?>	
					<a href="/<?=FOLDERADD?>/items/item/<?=$itemId?>?cmd=trivia&action=edit&itemtriviaId=<?=$trivia->itemtriviaId?>" class="btn btn-xs pull-right"><i class="text-warning fa fa-pencil fa-lg"></i></a>
					<?php }
				} ?>
				</h6>
			</div>
			<div class="panel-body">
				<?php if($this->Identity->Validate('items/item/trivia/actions/delete')){?>
				<div class="alert alert-danger text-center" role="alert" id="deletealertId-<?=$trivia->itemtriviaId?>" style="display:none;" 	>
					<div><strong><?=$this->lang->line('administration_items_trivia_deleteareyousure')?></strong></div>
					<a href="/<?=FOLDERADD?>/items/item/<?=$itemId?>?cmd=trivia&action=delete&itemtriviaId=<?=$trivia->itemtriviaId?>" class="btn btn-default btn-sm"><?=$this->lang->line('general_delete')?></a>
					<button id="deletecancelId-<?=$trivia->itemtriviaId?>" href="#" class="btn btn-default btn-sm"><?=$this->lang->line('general_cancel')?></button>
				</div>
				<?php }?>
				<?php
				if ($trivia->description != NULL) 
				{
					echo $trivia->description;	
				}
				else
				{
					echo '<p>'.$this->lang->line('administration_items_trivia_nodescription').'</p>';
				}
				?>

				<?php
				if ($trivia->active == 'TRUE') 
				{
					?>
					<p>
						<a href="/<?=FOLDERADD?>/items/item/<?=$itemId?>?cmd=trivia&action=viewtrivia&itemtriviaId=<?=$trivia->itemtriviaId?>" class="btn btn-primary btn-sm">
							<i class="fa fa-chevron-right fa-lg"></i> <?=$this->lang->line('administration_items_trivia_starttrivia')?>
						</a>
					</p>
					<?php
				}
				?>
			</div>
		</div>

		<?php	} ?>
		<script type="text/javascript">
		<?php
		if($this->Identity->Validate('items/item/trivia/actions/delete')){
		foreach ($trivias as $trivia) {?>
			$('#deletecancelId-<?=$trivia->itemtriviaId?>').click(function(){
				$('#deletealertId-<?=$trivia->itemtriviaId?>').slideUp();
			});
			$('#deleteId-<?=$trivia->itemtriviaId?>').click(function(){
				$('#deletealertId-<?=$trivia->itemtriviaId?>').slideDown();
			});
			<?php	}
		} ?>
			</script>

			<?php	}

			?>