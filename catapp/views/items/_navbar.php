<ul class="nav nav-tabs">
	<?php if($this->Identity->Validate('items/details')) { ?>
	<li role="presentation" id="itemNavDetails"><a href="/<?=FOLDERADD?>/items/details/<?=$itemId;?>"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> <span class="hidden-xs"><?=$this->lang->line('administration_details');?></span></a></li>
	<?php } ?> 
	<?php if($this->Identity->Validate('items/edit')) { ?>
	<li role="presentation" id="itemNavEdit"><a href="/<?=FOLDERADD?>/items/edit/<?=$itemId;?>"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> <span class="hidden-xs"><?=$this->lang->line('administration_edit');?></span></a></li>
	<?php } ?> 
	<?php if($this->Identity->Validate('items/details')) { ?>
	<li role="presentation" id="itemNavDelete"><a href="/<?=FOLDERADD?>/items/delete/<?=$itemId;?>"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> <span class="hidden-xs"><?=$this->lang->line('administration_delete');?></span></a></li>
	<?php } ?> 
	<?php if($this->Identity->Validate('items/item')) { ?>
	<li role="presentation"><a href="/<?=FOLDERADD?>/#/items/item/<?=$itemId;?>"><i class="fa fa-chevron-right"></i>
		<?=$this->lang->line('administration_items_comeitem');?></a></li>
		<?php } ?> 
	</ul>