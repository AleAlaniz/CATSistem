<link rel="stylesheet" type="text/css" href="/<?php echo APPFOLDERADD; ?>/libraries/css/dataTables.bootstrap.min.css">
<ol class="breadcrumb">
	<li><a href="/<?=FOLDERADD?>/administration"><?=$this->lang->line('general_administration');?></a></li>
	<li class="active"><?=$this->lang->line('general_items');?></li>
</ol>
<?php if($this->Identity->Validate('items/create')) { ?>
<p>
	<a href="/<?=FOLDERADD?>/items/create" class="btn btn-sm btn-success "><i class="fa fa-plus"></i><strong> <?=$this->lang->line('administration_create');?></strong></a>
</p>
<?php
}
if (isset($_SESSION['itemMessage'])): ?>
<div class="alert alert-success alert-dismissible" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	<strong><i class="fa fa-check"></i></strong> 
	<?php if ($_SESSION['itemMessage'] == 'create'){
		echo $this->lang->line('administration_items_successmessage');
	}
	elseif ($_SESSION['itemMessage'] == 'delete'){
		echo $this->lang->line('administration_items_deletemessage');
	}
	?>
</div>
<?php endif; ?>
<div class="panel panel-default">
	<div class="panel-heading">
		<strong><?=$this->lang->line('general_items');?></strong>
		<span class="badge pull-right"><?=count($model)?></span>
	</div>
	<div class="panel-body">
		<table class="table table-hover">
			<thead>
				<tr class="active">
					<th><?=$this->lang->line('administration_items_name');?></th>
					<th><?=$this->lang->line('administration_items_subsection');?></th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<?php if (count($model) > 0) {?>

				<?php 
				foreach ($model as $item)
				{ 
					?>
					<tr class="optionsUser">
						<td><?=encodeQuery($item->name)?></td>
						<td><?=encodeQuery($item->subsection->name)?></td>
						<td>
							<?php if($this->Identity->Validate('items/details')) { ?>
							<a href="/<?=FOLDERADD?>/items/details/<?=$item->itemId?>"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></a>&nbsp; 
							<?php } ?>
							<?php if($this->Identity->Validate('items/edit')) { ?>
							<a href="/<?=FOLDERADD?>/items/edit/<?=$item->itemId?>"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>&nbsp; 
							<?php } ?>
							<?php if($this->Identity->Validate('items/delete')) { ?>
							<a href="/<?=FOLDERADD?>/items/delete/<?=$item->itemId?>"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
							<?php } ?>
						</td>
					</tr>
					<?php 
				}
			}
			else 
			{
				?>
				<tr class="text-center">
					<td colspan="3"><i class="fa fa-circle"></i> <?=$this->lang->line('administration_items_empty');?></td>
				</tr>

				<?php
			}?>
		</tbody>
	</table>
</div>
</div>

<script type="text/javascript" src="/<?php echo APPFOLDERADD; ?>/libraries/script/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="/<?php echo APPFOLDERADD; ?>/libraries/script/dataTables.bootstrap.min.js"></script>
<script type="text/javascript">

$('.table').DataTable( {
	language: {
		"sProcessing":     "Procesando...",
		"sLengthMenu":     "Mostrar _MENU_ items",
		"sZeroRecords":    "<i class='fa fa-circle'></i>  No se encontraron resultados",
		"sEmptyTable":     "Ningún dato disponible en esta tabla",
		"sInfo":           "Mostrando items del _START_ al _END_ de un total de _TOTAL_ items",
		"sInfoEmpty":      "Mostrando items del 0 al 0 de un total de 0 items",
		"sInfoFiltered":   "(filtrado de un total de _MAX_ usuarios)",
		"sInfoPostFix":    "",
		"sSearch":         "Buscar:",
		"sUrl":            "",
		"sInfoThousands":  ",",
		"sLoadingRecords": "Cargando...",
		"oPaginate": {
			"sFirst":    "Primero",
			"sLast":     "Último",
			"sNext":     "Siguiente",
			"sPrevious": "Anterior"
		},
		"oAria": {
			"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
			"sSortDescending": ": Activar para ordenar la columna de manera descendente"
		}
	}
} );


$('#nav_items').addClass('active');
</script>