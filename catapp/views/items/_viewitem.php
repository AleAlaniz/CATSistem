<div class="item-header"><h3><?=$this->lang->line('administration_items_information')?> 
	<?php if($this->Identity->Validate('items/edit')){?>
	<a class="btn btn-warning btn-xs" href="/<?=FOLDERADD?>/items/edit/<?=$itemId?>"><i class="fa fa-pencil"></i> <?=$this->lang->line('general_edit');?></a>
	<?php }?></h3>
	<hr>
</div>
<?php
if ($description == NULL) {
	echo $this->lang->line('administration_items_nodescription');
}
else{
	echo $description;
}?>
