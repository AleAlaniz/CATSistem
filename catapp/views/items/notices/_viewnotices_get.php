<?php if (isset($_SESSION['itemnoticeMessage']) && $this->Identity->Validate('items/item/notices/actions')){ ?>
<div class="alert alert-success alert-dismissible" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	<strong><i class="fa fa-check"></i></strong> 
	<?php if ($_SESSION['itemnoticeMessage'] == 'create'){
		echo $this->lang->line('administration_items_notice_createmessage');
	} elseif ($_SESSION['itemnoticeMessage'] == 'delete'){
		echo $this->lang->line('administration_items_notice_deletemessage');
	}elseif ($_SESSION['itemnoticeMessage'] == 'deleteall'){
		echo $this->lang->line('administration_items_notice_deleteallmessage');
	}elseif ($_SESSION['itemnoticeMessage'] == 'edit'){
		echo $this->lang->line('administration_items_notice_editmessage');
	}
	?>
</div>
<?php 
} ?>

<div id="noticesContainer">
<?php
if (count($notices) <= 0) {
	echo $this->lang->line('administration_items_notice_empty');
}
else{ 
	foreach ($notices as $notice) {?>

	<div class="panel panel-default">
		<div class="panel-heading">
			<h6 class="panel-title-nomargin">
				<?php if($this->Identity->Validate('items/item/notices/actions')){
					if($this->Identity->Validate('items/item/notices/actions/delete')) { ?>
					<button  class="btn btn-xs pull-right deleteButton"><i class="text-danger fa fa-times fa-lg"></i></button>		
					<?php } 
					if($this->Identity->Validate('items/item/notices/actions/edit')) { ?>
					<a href="#/items/item/<?= $itemId ?>/notice/<?= $notice->itemnoticeId ?>/edit" class="btn btn-xs pull-right"><i class="text-warning fa fa-pencil fa-lg"></i></a>
					<?php 
				}?>
				<?php }?>
				<span style="display:none;"><i class="text-danger fa fa-refresh fa-spin fa-lg pull-right"></i></span>

				<i class="fa fa-newspaper-o fa-fw fa-lg"></i>
				<strong>
					<?php
					$dias = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sábado");
					$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
					echo $this->lang->line('administration_items_notice_createdday').': '.$dias[date('w', $notice->timestamp)].' '.$meses[date('n', $notice->timestamp)-1].' '.date('d ,Y', $notice->timestamp);
					?>
				</strong>
			</h6>
		</div>
		<div id="panelAlert" class="panel-body">
			<?php if($this->Identity->Validate('items/item/notices/actions/delete')){?>
			<div  id="alertDelete" class="alert alert-danger text-center" role="alert" style="display:none;">
				<div><strong><?=$this->lang->line('administration_items_notice_deleteareyousure')?></strong></div>
				<button id="<?=$notice->itemnoticeId?>" class="btn btn-default btn-sm confirmDeleteNotice"><?=$this->lang->line('general_delete')?></button>
				<button class="btn btn-default btn-sm cancelDeleteButton"><?=$this->lang->line('general_cancel')?></button>
			</div>
			<?php }?>
			<p><strong><?=encodeQuery($notice->title)?></p></strong>
			<?=$notice->details?>
		</div>
	</div>



	<?php	} ?>
	<?php	}	?>
</div>