
<form class="form-horizontal" method="POST" novalidate >
	<div class="form-group">
		<label for="title" class="col-sm-2 control-label"><?=$this->lang->line('administration_items_notice_title');?> <strong class="text-danger">*</strong></label>
		<div class="col-sm-10">
			<input type="text" class="form-control input-sm" id="title" name="title" value="<?php echo set_value('title');?>" placeholder="<?=$this->lang->line('administration_items_notice_title');?>" required>
			<?php echo form_error('title'); ?>
		</div>
	</div>
	<div class="form-group">
		<label for="details" class="col-sm-2 control-label"><?=$this->lang->line('administration_items_notice_details');?></label>
		<div class="col-sm-10">
			<textarea class="form-control" id="details" name="details" rows="10"><?php echo set_value('details');?></textarea>
			<?php echo form_error('details'); ?>
		</div>
	</div>
	<div class="form-group text-center" id="actionsButtons">
		<button type="submit" id="createNotice" class="btn btn-success btn-sm"><?=$this->lang->line('general_save');?></button>
		<a href="#/items/item/<?=$itemId?>/notices" class="btn btn-danger btn-sm"><?=$this->lang->line('general_back');?></a>
	</div>
	<div class="form-group text-center" style="display:none" id="loadingCreateNotice">
		<p><i class="fa fa-refresh fa-2x fa-spin"></i></p>
	</div>
</form>

<div class="modal flat-style animated bounce" id="incomplete" role="dialog">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-body">
				<p><?php echo $this->lang->line('general_completeall_error') ?></p>
				<div class="text-right">
					<button  class="btn btn-white" data-dismiss="modal"><?= $this->lang->line('general_ok') ?></button>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Modal de no se permite -->
<div class="modal flat-style animated bounceIn" id="NotAvailableMessage" role="dialog">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-body">
			<p><?php echo $this->lang->line('items_notice_notavailable') ?></p>
				<div class="text-right">
					<button id="accept" data-dismiss="modal" class="btn btn-white" data-dismiss="modal"><?= $this->lang->line('general_accept') ?></button>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- fin del modal -->
<div class="modal flat-style animated bounceIn" id="create" role="dialog">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-body">
				<p><?php echo $this->lang->line('administration_items_notice_createmessage') ?></p>
				<div class="text-right">
					<button  class="btn btn-white" data-dismiss="modal"><?= $this->lang->line('general_ok') ?></button>
				</div>
			</div>

		</div>

	</div>

</div>

<script type="text/javascript" src="/<?php echo APPFOLDERADD; ?>/libraries/tinymce/tinymce.min.js"></script>

<script>
//crear noticia
$('#createNotice').on('click',function(event)
{	
	event.preventDefault();
	
	if($('#title').val().trim() === '')
	{
		$('#incomplete').modal('show');
	}
	else
	{
		$.ajax(
		{
			method:'POST',
			url:'/'+FOLDERADD+'/items/createnotices',
			data:{'itemId':<?= $itemId ?>,'title':$('#title').val(), 'details':$('#details_ifr').contents().find('#tinymce').html()},
			beforeSend:function()
			{
				$('#actionsButtons').hide();
				$('#loadingCreateNotice').show();
			}
		})
		.done(function(res)
		{
			if(res != "fail" && res!= "Not available to create")
			{	
				$('#actionsButtons').show();
				$('#loadingCreateNotice').hide();
				$('#title').val('')
				$('#details_ifr').contents().find('#tinymce').html('<p><br data-mce-bogus="1"></p>');
				$('#create').modal('show');
			}
			//validar si se puede crear
			else if (res== "Not available to create"){
				$('#NotAvailableMessage').modal('show');
			}
			else
			{
				$('#incomplete').modal('show');
			}
		});
	}

});

$('#accept').on('click',function () {
	$('#NotAvailableMessage').modal('hide');
	window.location.href = "#/items/item/<?=$itemId?>/notices";
})

</script>