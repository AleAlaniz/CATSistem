<?php if (isset($_SESSION['itemforumMessage']) && $this->Identity->Validate('items/item/forums/actions')){ ?>
<div class="alert alert-success alert-dismissible" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	<strong><i class="fa fa-check"></i></strong> 
	<?php if ($_SESSION['itemforumMessage'] == 'createtopic'){
		echo $this->lang->line('administration_items_forum_createtopicmessage');
	} elseif ($_SESSION['itemforumMessage'] == 'deletetopic'){
		echo $this->lang->line('administration_items_forum_deletetopicmessage');
	}
	?>
</div>
<?php 
}
?>
<div class="panel panel-default">
	<div class="panel-heading">
		<h6 class="panel-title-nomargin">
			<i class="fa fa-comments fa-fw fa-lg"></i>
			<strong>
				<?=encodeQuery($viewforum->name)?>
			</strong>
		</h6>
	</div>
	<table class="table table-hover table-centertext">
		<thead>
			<tr>
				<th><?=$this->lang->line('administration_items_forum_topic')?></th>
				<th><?=$this->lang->line('administration_items_forum_messages')?></th>
				<th><?=$this->lang->line('administration_items_forum_initiator')?></th>
				<th><?=$this->lang->line('administration_items_forum_views')?></th>
				<th><?=$this->lang->line('administration_items_forum_lastmessage')?></th>
				<th colspan="2"><?=$this->lang->line('administration_items_forum_edit')?> / <?=$this->lang->line('administration_items_forum_delete')?></th>
			</tr>
		</thead>
		<tbody>
			<?php 
			foreach ($viewforum->topics as $topic) {
				if (count($topic->messages) > 0) {
					$lastMessage = end($topic->messages);
					$lastMessage = date('Y-m-d G:i',$lastMessage->timestamp);
				}
				else{
					$lastMessage = $this->lang->line('administration_items_forum_nomessage') ;
				}

				$tnoview = '';
				if ($topic->noViews) {
					$tnoview = '<i class="fa fa-circle noreadDotFT"></i> ';
				}
				echo "<tr>". 
				"<th><i class='fa fa-commenting-o'></i> <a href='/".FOLDERADD."/items/item/".$itemId."?cmd=forums&action=viewTopic&itemforumtopicId=".$topic->itemforumtopicId."' class='table-link'>".$tnoview.encodeQuery($topic->topic)."</a></th>".
				"<td>".count($topic->messages)."</td>".
				"<td>".encodeQuery($topic->initiator->lastName)." ".encodeQuery($topic->initiator->name)."</td>".
				"<td>".$topic->views."</td>".
				"<td>".$lastMessage."</td>";

				if ($this->Identity->Validate('items/item/forums/actions/edittopic', $topic->userId)) {
					echo '<td><a href="/'.FOLDERADD.'/items/item/'.$itemId.'?cmd=forums&action=editTopic&itemforumtopicId='.$topic->itemforumtopicId.'"><i class="text-warning fa fa-pencil fa-lg"></i></a></td>';
				}
				if ($this->Identity->Validate('items/item/forums/actions/deletetopic', $topic->userId)) {
					echo '<td><span id="deleteId-'.$topic->itemforumtopicId.'" class="btn btn-xs"><i class="text-danger fa fa-times fa-lg"></i></span></td>';
				} 
				echo "</tr>";

				if($this->Identity->Validate('items/item/forums/actions/deletetopic', $topic->userId)){ ?>
				<tr>
					<td class="alert alert-danger text-center" role="alert" id="deletealertId-<?=$topic->itemforumtopicId?>" style="display:none;" colspan="7" 	>
						<div><strong><?=$this->lang->line('administration_items_forum_deletetopicareyousure')?></strong></div>
						<a href="/<?=FOLDERADD?>/items/item/<?=$itemId?>?cmd=forums&action=deleteTopic&itemforumtopicId=<?=$topic->itemforumtopicId?>" class="btn btn-default btn-sm"><?=$this->lang->line('general_delete')?></a>
						<button id="deletecancelId-<?=$topic->itemforumtopicId?>" class="btn btn-default btn-sm"><?=$this->lang->line('general_cancel')?></button>
					</td>
				</tr>
				<?php }
			} 
			if (count($viewforum->topics) <= 0) { 
				echo "<tr>". 
				"<td colspan='8' class='text-center'>".$this->lang->line('administration_items_forum_notopics')."</td>".
				"</tr>";
			}

			?>
		</tbody>
	</table>
</div>

<script type="text/javascript">
<?php
if($this->Identity->Validate('items/item/forums/actions/delete')){
	foreach ($viewforum->topics as $topic) { 
		?>
		$('#deletecancelId-<?=$topic->itemforumtopicId?>').click(function(){
			$('#deletealertId-<?=$topic->itemforumtopicId?>').fadeOut();
		});
		$('#deleteId-<?=$topic->itemforumtopicId?>').click(function(){
			$('#deletealertId-<?=$topic->itemforumtopicId?>').fadeIn();
		});
		<?php
	}
}
?>
</script>