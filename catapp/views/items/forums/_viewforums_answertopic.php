<form class="form-horizontal" method="POST" novalidate>
	<div class="form-group">
		<label for="message" class="col-sm-2 control-label"><?=$this->lang->line('administration_items_forum_topicmessage');?><strong class="text-danger"> *</strong></label>
		<div class="col-sm-10">
			<textarea class="form-control" id="message" name="message" rows="10"><?php echo set_value('message', $messageQuote);?></textarea>
			<?php echo form_error('message'); ?>
		</div>
	</div>
	<div class="form-group text-center">
		<input type="submit" class="btn btn-success btn-sm" value="<?=$this->lang->line('general_answer');?>"></input>
		<a href="/<?=FOLDERADD?>/items/item/<?=$itemId?>?cmd=forums&action=viewTopic&itemforumtopicId=<?=$viewtopic->itemforumtopicId?>" class="btn btn-danger btn-sm"><?=$this->lang->line('general_cancel');?></a>
	</div>
</form>
<script type="text/javascript" src="/<?=APPFOLDERADD?>/libraries/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
tinymce.init({
	toolbar: "cut copy paste | alignleft aligncenter alignright alignjustify | bullist numlist  | outdent indent | forecolor backcolor fontsizeselect  | bold italic underline | link unlink image media | table",
	plugins: "image link media textcolor table",
	language: 'es',
	selector: "#message"
});
</script>