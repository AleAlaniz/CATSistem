<form class="form-horizontal" method="POST" enctype="multipart/form-data" novalidate >
	<div class="form-group">
		<label for="name" class="col-sm-2 control-label"><?=$this->lang->line('administration_items_forum_category_name');?> <strong class="text-danger">*</strong></label>
		<div class="col-sm-10">
			<input type="text" class="form-control input-sm" id="name" name="name" value="" placeholder="<?=$this->lang->line('administration_items_forum_category_name');?>" required>
			<?php echo form_error('name'); ?>
		</div>
	</div>
	<div class="form-group text-center">
		<button type="submit" class="btn btn-success btn-sm"><?=$this->lang->line('general_save');?></button>
		<a href="/<?=FOLDERADD?>/items/item/<?=$itemId?>?cmd=forums" class="btn btn-danger btn-sm"><?=$this->lang->line('general_cancel');?></a>
	</div>
</form>