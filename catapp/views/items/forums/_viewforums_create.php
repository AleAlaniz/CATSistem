<form class="form-horizontal" method="POST" novalidate>
	<div class="form-group">
		<label for="name" class="col-sm-2 control-label"><?=$this->lang->line('administration_items_forum_name');?> <strong class="text-danger">*</strong></label>
		<div class="col-sm-10">
			<input type="text" class="form-control input-sm" id="name" name="name" value="<?=set_value('name')?>" placeholder="<?=$this->lang->line('administration_items_forum_name');?>" required>
			<?php echo form_error('name'); ?>
		</div>
	</div>
	<div class="form-group">
		<label for="description" class="col-sm-2 control-label"><?=$this->lang->line('administration_items_forum_description');?></label>
		<div class="col-sm-10">
			<textarea class="form-control" id="description" name="description" rows="2"><?php echo set_value('description');?></textarea>
			<?php echo form_error('description'); ?>
		</div>
	</div>
	<div class="form-group">
		<label for="category" class="col-sm-2 control-label"><?=$this->lang->line('administration_items_forum_category');?></label>
		<div class="col-sm-10">
			<select class="form-control input-sm" id="category" name="category">
				<option value=""><?=$this->lang->line('administration_items_forum_nocategory')?></option>
				<?php foreach($categories as $category): ?>
				<option value="<?=$category->itemforumcategoryId?>" <?php echo  set_select('category', $category->itemforumcategoryId); ?> ><?=$category->name?></option>
				<?php endforeach; ?>
			</select>
			<?php echo form_error('category'); ?>
		</div>
	</div>
	<div class="form-group text-center">
		<input type="submit" class="btn btn-success btn-sm" value="<?=$this->lang->line('general_save');?>"></input>
		<a href="/<?=FOLDERADD?>/items/item/<?=$itemId?>?cmd=forums" class="btn btn-danger btn-sm"><?=$this->lang->line('general_cancel');?></a>
	</div>
</form>