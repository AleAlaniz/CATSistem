<?php if (isset($_SESSION['itemforumMessage']) && $this->Identity->Validate('items/item/forums/actions')){ ?>
<div class="alert alert-success alert-dismissible" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	<strong><i class="fa fa-check"></i></strong> 
	<?php if ($_SESSION['itemforumMessage'] == 'create'){
		echo $this->lang->line('administration_items_forum_createmessage');
	} elseif ($_SESSION['itemforumMessage'] == 'delete'){
		echo $this->lang->line('administration_items_forum_deletemessage');
	}elseif ($_SESSION['itemforumMessage'] == 'edit'){
		echo $this->lang->line('administration_items_forum_editmessage');
	}elseif ($_SESSION['itemforumMessage'] == 'clear'){
		echo $this->lang->line('administration_items_forum_clearmessage');
	}elseif ($_SESSION['itemforumMessage'] == 'createCategory'){
		echo $this->lang->line('administration_items_forum_createcategorymessage');
	}elseif ($_SESSION['itemforumMessage'] == 'editCategory'){
		echo $this->lang->line('administration_items_forum_editcategorymessage');
	}elseif ($_SESSION['itemforumMessage'] == 'deleteCategory'){
		echo $this->lang->line('administration_items_forum_deletecategorymessage');
	}
	?>
</div>
<?php 
}

if (count($forums) <= 0 && count($categories) <= 0) {
	echo $this->lang->line('administration_items_forum_empty');
}
else{ 
	$allForums = $forums;

	foreach ($categories as $category) {
		?>
		<div class="panel panel-default">
			<div class="panel-heading">
				<h6 class="panel-title-nomargin">
					<i class="fa fa-list-alt fa-fw fa-lg"></i>
					<strong>
						<?=encodeQuery($category->name)?>
					</strong>
					<?php if($this->Identity->Validate('items/item/forums/actions')){
						if($this->Identity->Validate('items/item/forums/actions/deletecategory')){
							?>
							<button id="deletecategoryId-<?=$category->itemforumcategoryId?>" class="btn btn-xs pull-right"><i class="text-danger fa fa-times fa-lg"></i></button>	
							<?php }
							if($this->Identity->Validate('items/item/forums/actions/editcategory')){
								?>	
								<a href="/<?=FOLDERADD?>/items/item/<?=$itemId?>?cmd=forums&action=editCategory&itemforumcategoryId=<?=$category->itemforumcategoryId?>" class="btn btn-xs pull-right"><i class="text-warning fa fa-pencil fa-lg"></i></a>
								<?php }
							} ?>
						</h6>
					</div>
					<?php if($this->Identity->Validate('items/item/forums/actions/deletecategory')){?>
					<div class="alert alert-danger text-center" role="alert" id="deletecategoryalertId-<?=$category->itemforumcategoryId?>" style="display:none;" 	>
						<div><strong><?=$this->lang->line('administration_items_forum_deletecategoryareyousure')?></strong></div>
						<a href="/<?=FOLDERADD?>/items/item/<?=$itemId?>?cmd=forums&action=deleteCategory&itemforumcategoryId=<?=$category->itemforumcategoryId?>" class="btn btn-default btn-sm"><?=$this->lang->line('general_delete')?></a>
						<button id="deletecategorycancelId-<?=$category->itemforumcategoryId?>" href="#" class="btn btn-default btn-sm"><?=$this->lang->line('general_cancel')?></button>
					</div>
					<?php } ?>
					<table class="table table-hover table-centertext">
						<thead>
							<tr>
								<th><?=$this->lang->line('administration_items_forum_forum')?></th>
								<th><?=$this->lang->line('administration_items_forum_topics')?></th>
								<th><?=$this->lang->line('administration_items_forum_messages')?></th>
								<th><?=$this->lang->line('administration_items_forum_lastmessage')?></th>
								<?php
								if ($this->Identity->Validate('items/item/forums/actions/edit')) {
									echo '<th>'.$this->lang->line('administration_items_forum_edit').'</th>';
								}
								if ($this->Identity->Validate('items/item/forums/actions/clear')) {
									echo '<th>'.$this->lang->line('administration_items_forum_clear').'</th>';
								} 
								if ($this->Identity->Validate('items/item/forums/actions/delete')) {
									echo '<th>'.$this->lang->line('administration_items_forum_delete').'</th>';
								} 
								?>
							</tr>
						</thead>
						<tbody>
							<?php 
							foreach ($category->forums as $forum) {
								$forum->noViews = FALSE;
								$sql = 'SELECT itemforumtopicId, userId FROM itemforumTopics WHERE itemforumId = ?';
								$forum->topics = $this->db->query($sql, array($forum->itemforumId))->result();
								foreach ($forum->topics as $tkey => $tvalue) {
									$sql = 'SELECT itemforummessageId, userId FROM itemforumMessages WHERE itemforumtopicId = ?';
									$tvalue->messages = $this->db->query($sql, array($tvalue->itemforumtopicId))->result();
									foreach ($tvalue->messages as $mkey => $mvalue) {
										if ($mvalue->userId != $this->session->UserId) {
											$sql = 'SELECT itemforummessageviewsId FROM itemforummessageViews WHERE itemforummessageId = ? && userId = ?';
											$mviewexist = $this->db->query($sql, array($mvalue->itemforummessageId, $this->session->UserId))->result();
											if ($mviewexist == NULL) {
												$forum->noViews = TRUE;
											}
										}
									}
									if ($tvalue->userId != $this->session->UserId) {
										$sql = 'SELECT itemforumtopicviewId FROM itemforumtopicViews WHERE itemforumtopicId = ? && userId = ?';
										$tviewexist = $this->db->query($sql, array($tvalue->itemforumtopicId, $this->session->UserId))->result();
										if ($tviewexist == NULL) {
											$forum->noViews = TRUE;
										}
									}
								}
								$fnoview = '';
								if ($forum->noViews) {
									$fnoview = '<i class="fa fa-circle noreadDotFT"></i> ';
								}

								array_push($allForums, $forum);
								$lastMessage = ($forum->lastMessage == NULL) ? $this->lang->line('administration_items_forum_nomessage') : date('Y-m-d G:i',$forum->lastMessage->timestamp);
								echo "<tr>". 
								"<th><i class='fa fa-comments-o'></i> <a href='/".FOLDERADD."/items/item/".$itemId."?cmd=forums&action=viewforum&itemforumId=".$forum->itemforumId."' class='table-link'>".$fnoview.encodeQuery($forum->name)."</a><span class='table-comment'>".encodeQuery($forum->description)."</span></th>".
								"<td>".count($forum->topics)."</td>".
								"<td>".$forum->totalMessages."</td>".
								"<td>".$lastMessage."</td>";

								if ($this->Identity->Validate('items/item/forums/actions/edit')) {
									echo '<td><a href="/'.FOLDERADD.'/items/item/'.$itemId.'?cmd=forums&action=edit&itemforumId='.$forum->itemforumId.'"><i class="text-warning fa fa-pencil fa-lg"></i></a></td>';
								}
								if ($this->Identity->Validate('items/item/forums/actions/clear')) {
									echo '<td><span id="clearId-'.$forum->itemforumId.'" class="btn btn-xs"><i class="text-info fa fa-trash fa-lg"></i></span></td>';
								} 
								if ($this->Identity->Validate('items/item/forums/actions/delete')) {
									echo '<td><span id="deleteId-'.$forum->itemforumId.'" class="btn btn-xs"><i class="text-danger fa fa-times fa-lg"></i></span></td>';
								} 
								echo "</tr>";

								if($this->Identity->Validate('items/item/forums/actions/delete')){?>
								<tr>
									<td class="alert alert-danger text-center" role="alert" id="deletealertId-<?=$forum->itemforumId?>" style="display:none;" colspan="7" 	>
										<div><strong><?=$this->lang->line('administration_items_forum_deleteareyousure')?></strong></div>
										<a href="/<?=FOLDERADD?>/items/item/<?=$itemId?>?cmd=forums&action=delete&itemforumId=<?=$forum->itemforumId?>" class="btn btn-default btn-sm"><?=$this->lang->line('general_delete')?></a>
										<button id="deletecancelId-<?=$forum->itemforumId?>" class="btn btn-default btn-sm"><?=$this->lang->line('general_cancel')?></button>
									</td>
								</tr>
								<?php }
								if($this->Identity->Validate('items/item/forums/actions/clear')){?>
								<tr>
									<td class="alert alert-info text-center" role="alert" id="clearalertId-<?=$forum->itemforumId?>" style="display:none;" colspan="7" 	>
										<div><strong><?=$this->lang->line('administration_items_forum_clearareyousure')?></strong></div>
										<a href="/<?=FOLDERADD?>/items/item/<?=$itemId?>?cmd=forums&action=clear&itemforumId=<?=$forum->itemforumId?>" class="btn btn-default btn-sm"><?=$this->lang->line('general_clear')?></a>
										<button id="clearcancelId-<?=$forum->itemforumId?>" class="btn btn-default btn-sm"><?=$this->lang->line('general_cancel')?></button>
									</td>
								</tr>
								<?php }
							} 
							if (count($category->forums) <= 0) { 
								echo "<tr>". 
								"<td colspan='8' class='text-center'>".$this->lang->line('administration_items_forum_noforums')."</td>".
								"</tr>";
							}

							?>
						</tbody>
					</table>

				</div>
				<?php	
			} 

			?>
			<div class="panel panel-default">
				<div class="panel-heading">
					<h6 class="panel-title-nomargin">
						<i class="fa fa-comment-o fa-fw fa-lg"></i>
						<strong>
							<?=$this->lang->line('administration_items_forum_nocategory')?>
						</strong>
					</h6>
				</div>
				<table class="table table-hover table-centertext">
					<thead>
						<tr>
							<th><?=$this->lang->line('administration_items_forum_forum')?></th>
							<th><?=$this->lang->line('administration_items_forum_topics')?></th>
							<th><?=$this->lang->line('administration_items_forum_messages')?></th>
							<th><?=$this->lang->line('administration_items_forum_lastmessage')?></th>
							<?php
							if ($this->Identity->Validate('items/item/forums/actions/edit')) {
								echo '<th>'.$this->lang->line('administration_items_forum_edit').'</th>';
							}
							if ($this->Identity->Validate('items/item/forums/actions/clear')) {
								echo '<th>'.$this->lang->line('administration_items_forum_clear').'</th>';
							} 
							if ($this->Identity->Validate('items/item/forums/actions/delete')) {
								echo '<th>'.$this->lang->line('administration_items_forum_delete').'</th>';
							} 
							?>
						</tr>
					</thead>
					<tbody>
						<?php 
						foreach ($forums as $forum) {

							$forum->noViews = FALSE;
							$sql = 'SELECT itemforumtopicId, userId FROM itemforumTopics WHERE itemforumId = ?';
							$forum->topics = $this->db->query($sql, array($forum->itemforumId))->result();
							foreach ($forum->topics as $tkey => $tvalue) {
								$sql = 'SELECT itemforummessageId, userId FROM itemforumMessages WHERE itemforumtopicId = ?';
								$tvalue->messages = $this->db->query($sql, array($tvalue->itemforumtopicId))->result();
								foreach ($tvalue->messages as $mkey => $mvalue) {
									if ($mvalue->userId != $this->session->UserId) {
										$sql = 'SELECT itemforummessageviewsId FROM itemforummessageViews WHERE itemforummessageId = ? && userId = ?';
										$mviewexist = $this->db->query($sql, array($mvalue->itemforummessageId, $this->session->UserId))->result();
										if ($mviewexist == NULL) {
											$forum->noViews = TRUE;
										}
									}
								}
								if ($tvalue->userId != $this->session->UserId) {
									$sql = 'SELECT itemforumtopicviewId FROM itemforumtopicViews WHERE itemforumtopicId = ? && userId = ?';
									$tviewexist = $this->db->query($sql, array($tvalue->itemforumtopicId, $this->session->UserId))->result();
									if ($tviewexist == NULL) {
										$forum->noViews = TRUE;
									}
								}
							}
							$fnoview = '';
							if ($forum->noViews) {
								$fnoview = '<i class="fa fa-circle noreadDotFT"></i> ';
							}

							$lastMessage = ($forum->lastMessage == NULL) ? $this->lang->line('administration_items_forum_nomessage') : date('Y-m-d G:i', $forum->lastMessage->timestamp);
							echo "<tr>". 
							"<th><i class='fa fa-comments-o'></i> <a href='/".FOLDERADD."/items/item/".$itemId."?cmd=forums&action=viewforum&itemforumId=".$forum->itemforumId."' class='table-link'>".$fnoview.encodeQuery($forum->name)."</a><span class='table-comment'>".encodeQuery($forum->description)."</span></th>".
							"<td>".count($forum->topics)."</td>".
							"<td>".$forum->totalMessages."</td>".
							"<td>".$lastMessage."</td>";

							if ($this->Identity->Validate('items/item/forums/actions/edit')) {
								echo '<td><a href="/'.FOLDERADD.'/items/item/'.$itemId.'?cmd=forums&action=edit&itemforumId='.$forum->itemforumId.'"><i class="text-warning fa fa-pencil fa-lg"></i></a></td>';
							}
							if ($this->Identity->Validate('items/item/forums/actions/clear')) {
								echo '<td><span id="clearId-'.$forum->itemforumId.'" class="btn btn-xs"><i class="text-info fa fa-trash fa-lg"></i></span></td>';
							} 
							if ($this->Identity->Validate('items/item/forums/actions/delete')) {
								echo '<td><span id="deleteId-'.$forum->itemforumId.'" class="btn btn-xs"><i class="text-danger fa fa-times fa-lg"></i></span></td>';
							} 
							echo "</tr>";

							if($this->Identity->Validate('items/item/forums/actions/delete')){?>
							<tr>
								<td class="alert alert-danger text-center" role="alert" id="deletealertId-<?=$forum->itemforumId?>" style="display:none;" colspan="7" 	>
									<div><strong><?=$this->lang->line('administration_items_forum_deleteareyousure')?></strong></div>
									<a href="/<?=FOLDERADD?>/items/item/<?=$itemId?>?cmd=forums&action=delete&itemforumId=<?=$forum->itemforumId?>" class="btn btn-default btn-sm"><?=$this->lang->line('general_delete')?></a>
									<button id="deletecancelId-<?=$forum->itemforumId?>" class="btn btn-default btn-sm"><?=$this->lang->line('general_cancel')?></button>
								</td>
							</tr>
							<?php }
							if($this->Identity->Validate('items/item/forums/actions/clear')){?>
							<tr>
								<td class="alert alert-info text-center" role="alert" id="clearalertId-<?=$forum->itemforumId?>" style="display:none;" colspan="7" 	>
									<div><strong><?=$this->lang->line('administration_items_forum_clearareyousure')?></strong></div>
									<a href="/<?=FOLDERADD?>/items/item/<?=$itemId?>?cmd=forums&action=clear&itemforumId=<?=$forum->itemforumId?>" class="btn btn-default btn-sm"><?=$this->lang->line('general_clear')?></a>
									<button id="clearcancelId-<?=$forum->itemforumId?>" class="btn btn-default btn-sm"><?=$this->lang->line('general_cancel')?></button>
								</td>
							</tr>
							<?php }
						} 
						if (count($forums) <= 0) { 
							echo "<tr>". 
							"<td colspan='8' class='text-center'>".$this->lang->line('administration_items_forum_noforums')."</td>".
							"</tr>";
						}
						?>
					</tbody>
				</table>
			</div>

			<script type="text/javascript">
			<?php
			if($this->Identity->Validate('items/item/forums/actions/deletecategory')){
				foreach ($categories as $category) { ?>
					$('#deletecategorycancelId-<?=$category->itemforumcategoryId?>').click(function(){
						$('#deletecategoryalertId-<?=$category->itemforumcategoryId?>').slideUp();
					});
					$('#deletecategoryId-<?=$category->itemforumcategoryId?>').click(function(){
						$('#deletecategoryalertId-<?=$category->itemforumcategoryId?>').slideDown();
					});
					<?php	}
				}
				if($this->Identity->Validate('items/item/forums/actions/delete')){
					foreach ($allForums as $forum) { ?>
						$('#deletecancelId-<?=$forum->itemforumId?>').click(function(){
							$('#deletealertId-<?=$forum->itemforumId?>').fadeOut();
						});
						$('#deleteId-<?=$forum->itemforumId?>').click(function(){
							$('#deletealertId-<?=$forum->itemforumId?>').fadeIn();
						});
						<?php	}
					}
					if($this->Identity->Validate('items/item/forums/actions/clear')){
						foreach ($allForums as $forum) { ?>
							$('#clearcancelId-<?=$forum->itemforumId?>').click(function(){
								$('#clearalertId-<?=$forum->itemforumId?>').fadeOut();
							});
							$('#clearId-<?=$forum->itemforumId?>').click(function(){
								$('#clearalertId-<?=$forum->itemforumId?>').fadeIn();
							});
							<?php	}
						}
						?>
						</script>
						<?php
					}
					?>