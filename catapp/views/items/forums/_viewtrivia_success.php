<?php
echo '<h2>'.$viewtrivia->name.'</h2>';
if($viewtrivia->description != NULL) {echo '<p>'.$viewtrivia->description.'</p>';} 
$triviaValue = 0;
$totalanswervalue = 0;
foreach ($viewtrivia->questions as $question) {
	$totalValue = 0; 
	$answerValue = 0; 
	?>
	<div class="panel panel-default">
		<div class="panel-heading ">
			<h6 class="panel-title-nomargin">
				<i class="fa fa-question-circle fa-fw fa-lg"></i>
				<strong>
					<?=encodeQuery($question->question)?>
				</strong>
			</h6>
		</div>
		<div class="panel-body">
			<?php
			if($question->description != NULL) {echo '<p>'.$question->description.'</p>';} 
			if ($question->type == 1) {
				$type = $this->lang->line('administration_items_trivia_question_optionunique');
				?>
				<table class="table">
					<thead>
						<tr>
							<th class="text-center"><?=$this->lang->line('administration_items_trivia_youchoice')?></th>
							<th class="text-center"><?=$this->lang->line('administration_items_trivia_expectedchoice')?></th>
							<th class="text-center"><?=$this->lang->line('administration_items_trivia_answer')?></th>
						</tr>
					</thead>
					<tbody>
						<?php
						foreach ($question->answers as $answer) {
							if ($answer->value > 0) {
								$totalValue += $answer->value;
							}
							$sql = "SELECT * FROM itemtriviaAttempts WHERE itemtriviaId = ? && userId = ?";
							$objecttoGet = array($viewtrivia->itemtriviaId, $this->session->UserId);
							$attempt = $this->db->query($sql,$objecttoGet)->last_row();
							if (isset($attempt)) {
								$sql = "SELECT * FROM itemtriviaSuccess WHERE itemtriviaId = ? && userId = ? && itemtriviaquestionId = ? && itemtriviaanswerId = ? && attempt = ?";
								$objecttoGet = array($viewtrivia->itemtriviaId, $this->session->UserId, $question->itemtriviaquestionId, $answer->itemtriviaanswerId, $attempt->attempt);
								$answerAttempt = $this->db->query($sql,$objecttoGet)->row();
								if (isset($answerAttempt)) {
									$icon = 'dot-circle-o';
									$answerValue += $answer->value;
								}
								else
								{
									$icon = 'circle-o';
								}

								if ($answer->istrue == 'TRUE') {
									$iconExpected = 'dot-circle-o';
								}
								else
								{
									$iconExpected = 'circle-o';
								}

								?>
								<tr>
									<th class="text-center"><i class="fa fa-<?=$icon?> fa-lg"></i></th>
									<td class="text-center"><i class="fa fa-<?=$iconExpected?> fa-lg"></i></td>
									<td class="text-center"><?=encodeQuery($answer->answer)?></td>
								</tr>
								<?php 
							}
							if ($answerValue < 0) {
								$answerValue = 0;
							}
						} 
						?>
					</tbody>
				</table>
				<?php 
			}
			elseif ($question->type == 2) {
				$type = $this->lang->line('administration_items_trivia_question_severaloptions');
				?>
				<table class="table">
					<thead>
						<tr>
							<th class="text-center"><?=$this->lang->line('administration_items_trivia_youchoice')?></th>
							<th class="text-center"><?=$this->lang->line('administration_items_trivia_expectedchoice')?></th>
							<th class="text-center"><?=$this->lang->line('administration_items_trivia_answer')?></th>
						</tr>
					</thead>
					<tbody>
						<?php
						foreach ($question->answers as $answer) {
							if ($answer->value > 0) {
								$totalValue += $answer->value;
							}
							$sql = "SELECT * FROM itemtriviaAttempts WHERE itemtriviaId = ? && userId = ?";
							$objecttoGet = array($viewtrivia->itemtriviaId, $this->session->UserId);
							$attempt = $this->db->query($sql,$objecttoGet)->last_row();
							if (isset($attempt)) {
								$sql = "SELECT * FROM itemtriviaSuccess WHERE itemtriviaId = ? && userId = ? && itemtriviaquestionId = ? && itemtriviaanswerId = ? && attempt = ?";
								$objecttoGet = array($viewtrivia->itemtriviaId, $this->session->UserId, $question->itemtriviaquestionId, $answer->itemtriviaanswerId, $attempt->attempt);
								$answerAttempt = $this->db->query($sql,$objecttoGet)->row();
								if (isset($answerAttempt)) {
									$icon = 'check-square-o';
									$answerValue += $answer->value;
								}
								else
								{
									$icon = 'square-o';
								}

								if ($answer->istrue == 'TRUE') {
									$iconExpected = 'check-square-o';
								}
								else
								{
									$iconExpected = 'square-o';
								}

								?>
								<tr>
									<th class="text-center"><i class="fa fa-<?=$icon?> fa-lg"></i></th>
									<td class="text-center"><i class="fa fa-<?=$iconExpected?> fa-lg"></i></td>
									<td class="text-center"><?=encodeQuery($answer->answer)?></td>
								</tr>
								<?php 
							}
							if ($answerValue < 0) {
								$answerValue = 0;
							}
						} 
						?>
					</tbody>
				</table>
				<?php 
				
			}
			elseif ($question->type == 3) {
				$type = $this->lang->line('administration_items_trivia_question_input');
				$sql = "SELECT * FROM itemtriviaAttempts WHERE itemtriviaId = ? && userId = ?";
				$objecttoGet = array($viewtrivia->itemtriviaId, $this->session->UserId);
				$attempt = $this->db->query($sql,$objecttoGet)->last_row();
				if (isset($attempt)) {
					$sql = "SELECT * FROM itemtriviaSuccess WHERE itemtriviaId = ? && userId = ? && itemtriviaquestionId = ? && attempt = ?";
					$objecttoGet = array($viewtrivia->itemtriviaId, $this->session->UserId, $question->itemtriviaquestionId, $attempt->attempt);
					$answerAttempt = $this->db->query($sql,$objecttoGet)->row();
					if (isset($answerAttempt)) {
						echo '<span>'.encodeQuery($answerAttempt->answer).'</span>';
					}
					else
					{
						echo '<span>'.$this->lang->line('administration_items_trivia_question_noanswer').'</span>';
					}
				}

			}
			elseif ($question->type == 4) {
				$type = $this->lang->line('administration_items_trivia_question_trueorfalse');
				$sql = "SELECT * FROM itemtriviaAttempts WHERE itemtriviaId = ? && userId = ?";
				$objecttoGet = array($viewtrivia->itemtriviaId, $this->session->UserId);
				$attempt = $this->db->query($sql,$objecttoGet)->last_row();
				if (isset($attempt)) {
					$sql = "SELECT * FROM itemtriviaSuccess WHERE itemtriviaId = ? && userId = ? && itemtriviaquestionId = ? && attempt = ?";
					$objecttoGet = array($viewtrivia->itemtriviaId, $this->session->UserId, $question->itemtriviaquestionId, $attempt->attempt);
					$answerAttempt = $this->db->query($sql,$objecttoGet)->row();
					if (isset($answerAttempt)) {
						?>
						<table class="table">
							<thead>
								<tr>
									<th class="text-center"><?=$this->lang->line('administration_items_trivia_youchoice')?></th>
									<th class="text-center"><?=$this->lang->line('administration_items_trivia_expectedchoice')?></th>
									<th class="text-center"><?=$this->lang->line('administration_items_trivia_answer')?></th>
								</tr>
							</thead>
							<tbody>
								<?php
								if ($answerAttempt->answer == 'TRUE') {
									$iconTRUE = 'dot-circle-o';
								}
								else
								{
									$iconTRUE = 'circle-o';
								}

								if ($question->trueorfalse == 'TRUE') {
									$iconExpectedTRUE = 'dot-circle-o';
								}
								else
								{
									$iconExpectedTRUE = 'circle-o';
								}

								if ($answerAttempt->answer == 'FALSE') {
									$iconFALSE = 'dot-circle-o';
								}
								else
								{
									$iconFALSE = 'circle-o';
								}
								
								if ($question->trueorfalse == 'FALSE') {
									$iconExpectedFALSE = 'dot-circle-o';
								}
								else
								{
									$iconExpectedFALSE = 'circle-o';
								}

								?>
								<tr>
									<th class="text-center"><i class="fa fa-<?=$iconTRUE?> fa-lg"></i></th>
									<td class="text-center"><i class="fa fa-<?=$iconExpectedTRUE?> fa-lg"></i></td>
									<td class="text-center"><?=$this->lang->line('general_true')?></td>
								</tr>
								<tr>
									<th class="text-center"><i class="fa fa-<?=$iconFALSE?> fa-lg"></i></th>
									<td class="text-center"><i class="fa fa-<?=$iconExpectedFALSE?> fa-lg"></i></td>
									<td class="text-center"><?=$this->lang->line('general_false')?></td>
								</tr>
							</tbody>
						</table>
						<?php 
						if ($answerAttempt->answer == $question->trueorfalse) {
							$answerValue = $question->value;	
						}
					}
					else
					{
						echo '<span>'.$this->lang->line('administration_items_trivia_question_noanswer').'</span>';
					}
					$totalValue = $question->value;
				}
			}
			?>
		</div> 
		<ul class="list-group">
			<li class="list-group-item"><?=$type?><span class="badge"><strong><?=$this->lang->line('administration_items_trivia_question_points');?>: <?=$answerValue?>/<?=$totalValue?></strong></span></li>
		</ul>
	</div>

	<?php
	$triviaValue += $totalValue;
	$totalanswervalue += $answerValue;
}
?>
<div class="alert alert-warning" role="alert"><i class="fa fa-info-circle fa-lg fa-fw"></i> <strong><?=$this->lang->line('administration_items_trivia_question_totalvaluetrivia')?> <?=$totalanswervalue?>/<?=$triviaValue?><strong></div>
<div class="form-group text-center">
	<a href="/<?=FOLDERADD?>/items/item/<?=$itemId?>?cmd=trivia" class="btn btn-success btn-sm"><?=$this->lang->line('general_end');?></a>
</div>