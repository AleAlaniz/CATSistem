<form class="form-horizontal" method="POST" novalidate>
	<div class="form-group">
		<label for="topic" class="col-sm-2 control-label"><?=$this->lang->line('administration_items_forum_topictopic');?> <strong class="text-danger">*</strong></label>
		<div class="col-sm-10">
			<input type="text" class="form-control input-sm" id="topic" name="topic" value="<?=set_value('topic', $topicEdit->topic)?>" placeholder="<?=$this->lang->line('administration_items_forum_topictopic');?>" required>
			<?php echo form_error('topic'); ?>
		</div>
	</div>
	<div class="form-group">
		<label for="description" class="col-sm-2 control-label"><?=$this->lang->line('administration_items_forum_topicbody');?></label>
		<div class="col-sm-10">
			<textarea class="form-control" id="description" name="description" rows="10"><?php echo set_value('description', $topicEdit->description);?></textarea>
			<?php echo form_error('description'); ?>
		</div>
	</div>
	<div class="form-group text-center">
		<input type="submit" class="btn btn-success btn-sm" value="<?=$this->lang->line('general_save');?>"></input>
		<a href="/<?=FOLDERADD?>/items/item/<?=$itemId?>?cmd=forums" class="btn btn-danger btn-sm"><?=$this->lang->line('general_cancel');?></a>
	</div>
</form>
<script type="text/javascript" src="/<?=APPFOLDERADD?>/libraries/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
tinymce.init({
	toolbar: "cut copy paste | alignleft aligncenter alignright alignjustify | bullist numlist  | outdent indent | forecolor backcolor fontsizeselect  | bold italic underline | link unlink image media | table",
	plugins: "image link media textcolor table",
	language: 'es',
	selector: "#description"
});
</script>