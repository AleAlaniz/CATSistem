<ol class="breadcrumb">
	<li><a href="/<?=FOLDERADD?>/administration"><?=$this->lang->line('general_administration');?></a></li>
	<li><a href="/<?=FOLDERADD?>/items"><?=$this->lang->line('general_items');?></a></li>
	<li><a href="/<?=FOLDERADD?>/items/details/<?=$itemId;?>"><?=encodeQuery($name);?></a></li>
	<li class="active"><?=$this->lang->line('administration_delete');?></li>
</ol>
<div class="col-xs-12" style="margin-bottom:15px">
	<?=$navBar?>
</div>
<div class="col-xs-12">
<div class="panel panel-default">
	<div class="panel-heading">
		<strong><?=$this->lang->line('administration_items_delete');?></strong>
	</div>
	<div class="panel-body">
		<h4><?=$this->lang->line('administration_items_delete_areyousure');?></h4>
		<hr>
		<dl class="dl-horizontal col-sm-6">
			<dt><?=$this->lang->line('administration_items_name');?></dt>
			<dd><?=encodeQuery($name)?></dd>
		</dl>
		<dl class="dl-horizontal col-sm-6">
			<?php 
			$sql = "SELECT * FROM subSections WHERE subsectionId = ?";
			$subsection = array($subsectionId);
			$query = $this->db->query($sql,$subsection)->row();
			?>
			<dt><?=$this->lang->line('administration_items_subsection');?></dt>
			<dd><?=encodeQuery($query->name)?></dd>
		</dl>
		<dl class="dl-horizontal col-sm-12">
			<dt><?=$this->lang->line('administration_items_description');?></dt>
			<dd><?=$description?></dd>
		</dl>
		<form method="POST"  >
			<input type="hidden" name="itemId" value="<?=$itemId?>">
			<div class="form-group text-center col-xs-12">
				<button type="submit" class="btn btn-success btn-sm"><?=$this->lang->line('general_delete');?></button>
				<a href="/<?=FOLDERADD?>/items" class="btn btn-danger btn-sm"><?=$this->lang->line('general_cancel');?></a>
			</div>
		</form>
	</div>
</div>
</div>
<script type="text/javascript">
$('#nav_items').addClass('active');
$('#itemNavDelete').addClass('active');
</script>