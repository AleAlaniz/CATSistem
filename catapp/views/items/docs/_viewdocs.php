<div class="item-header">
	<h3>
		<span><?=$this->lang->line('administration_items_doc')?></span> 
		<?php if($this->Identity->Validate('items/item/docs/actions/create')) {?>
		<a class="btn btn-success btn-xs" href="#/items/item/<?= $itemId ?>/docs/upload"><i class="fa fa-upload"></i> <?=$this->lang->line('administration_items_doc_upload');?></a>
		<?php }?>
		<?php if($this->Identity->Validate('items/item/docs/actions/createfolder')) {?>
		<a class="btn btn-success btn-xs" href="#/items/item/<?= $itemId ?>/docs/folder/create"><i class="fa fa-folder-open-o"></i> <?=$this->lang->line('administration_items_doc_createfolder');?></a>
		<?php }?>
	</h3>
	<hr>
</div>
<?=$ActionView?>
