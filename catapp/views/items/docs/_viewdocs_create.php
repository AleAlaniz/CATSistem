<form class="form-horizontal" method="POST" enctype="multipart/form-data" id="uploadDoc" novalidate >
	<input type="hidden" name="itemId" value="<?= $itemId ?>" />
	<div class="form-group">
		<label for="userfile" class="col-sm-2 control-label"><?=$this->lang->line('administration_items_doc_document');?> <strong class="text-danger">*</strong></label>
		<div class="col-sm-10">
			<input type="file" class="" id="userfile" name="userfile" value="<?php echo set_value('userfile');?>"><small><?=$this->lang->line('administration_items_doc_allowtypes')?></small>
		</div>
	</div>
	<div class="form-group">
		<label for="name" class="col-sm-2 control-label"><?=$this->lang->line('administration_items_doc_name');?> <strong class="text-danger">*</strong></label>
		<div class="col-sm-10">
			<input type="text" class="form-control input-sm" id="name" name="name" value="" placeholder="<?=$this->lang->line('administration_items_doc_name');?>" required>
		</div>
	</div>
	<div class="form-group">
		<label for="folder" class="col-sm-2 control-label"><?=$this->lang->line('administration_items_doc_folder');?></label>
		<div class="col-sm-10">
			<select class="form-control input-sm" id="folder" name="folder">
				<option value=""><?=$this->lang->line('administration_items_doc_nofolder')?></option>
				<?php foreach($folders as $folder): ?>
				<option value="<?=$folder->itemdocfolderId?>" <?php echo  set_select('folder', $folder->itemdocfolderId); ?> ><?=$folder->name?></option>
			<?php endforeach; ?>
		</select>
	</div>
</div>
<div class="form-group">
	<label for="comment" class="col-sm-2 control-label"><?=$this->lang->line('administration_items_doc_comment');?></label>
	<div class="col-sm-10">
		<textarea class="form-control" id="comment" name="comment" rows="3"><?php echo set_value('comment');?></textarea>
	</div>
</div>
<div class="form-group text-center" id="actionsButtons">
	<button id="upload" type="submit" class="btn btn-success btn-sm"><?=$this->lang->line('general_upload');?></button>
	<a href="#/items/item/<?= $itemId ?>/docs" class="btn btn-danger btn-sm"><?=$this->lang->line('general_back');?></a>
</div>
</form>
<div class="form-group text-center" style="display:none" id="loadingUpload">
	<p><i class="fa fa-refresh fa-2x fa-spin"></i></p>
</div>
<div class="modal flat-style animated bounce" id="incomplete" role="dialog">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-body">
				<p><?php echo $this->lang->line('general_completeall_error') ?></p>
				<div class="text-right">
					<button  class="btn btn-white" data-dismiss="modal"><?= $this->lang->line('general_ok') ?></button>
				</div>
			</div>

		</div>

	</div>

</div>

<div class="modal flat-style animated bounceIn" id="create" role="dialog">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-body">
				<p><?php echo $this->lang->line('administration_items_doc_createmessage') ?></p>
				<div class="text-right">
					<button  class="btn btn-white" data-dismiss="modal"><?= $this->lang->line('general_ok') ?></button>
				</div>
			</div>

		</div> 

	</div>

</div>

<script type="text/javascript" src="/<?php echo APPFOLDERADD; ?>/libraries/script/jquery.form.js"></script>

<script type="text/javascript">
$('#userfile').on('change', function() {
	$('#name').val($('#userfile').val());
});

$('#upload').on('click',function(event)
{
	event.preventDefault();

	if($('#userfile').val() === '' || $('#name').val().trim() === '')
	{
		$('#incomplete').modal('show');
		$('#actionsButtons').show();
		$('#loadingUpload').hide();
	}
	else
	{		
		$('#actionsButtons').hide();
		$('#loadingUpload').show();

		$('#uploadDoc').ajaxSubmit({
			url : '/'+FOLDERADD+'/items/uploaddoc',
			success: function(data)
			{
				data = JSON.parse(data);
				$('#loadingUpload').hide();
				$('#actionsButtons').show();
				if (data.status != "fail")
				{
					$('#userfile').val('');
					$('#name').val('');
					$('#folder').val('');
					$('#comment').val('');


					$('#create').modal('show');
				}
				else
				{
					$('#incomplete').modal('show');
				}
			}
		});
	}

});
</script>