<form class="form-horizontal" method="POST" enctype="multipart/form-data" novalidate >
	<div class="form-group">
		<label for="name" class="col-sm-2 control-label"><?=$this->lang->line('administration_items_doc_name');?> <strong class="text-danger">*</strong></label>
		<div class="col-sm-10">
			<input type="text" class="form-control input-sm" id="name" name="name" value="<?php echo set_value('name', $itemToEdit->name);?>" placeholder="<?=$this->lang->line('administration_items_doc_name');?>" required>
			<?php echo form_error('name'); ?>
		</div>
	</div>
	<div id="actionsButtons" class="form-group text-center">
		<button type="submit" id="btnEdit" class="btn btn-success btn-sm"><?=$this->lang->line('general_save');?></button>
		<a href="#/items/item/<?= $itemId ?>/docs" class="btn btn-danger btn-sm"><?=$this->lang->line('general_back');?></a>
	</div>
</form>


<div class="form-group text-center" style="display:none" id="loadingEdit">
	<p><i class="fa fa-refresh fa-2x fa-spin"></i></p>
</div>

<div class="modal flat-style animated bounce" id="incomplete" role="dialog">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-body">
				<p><?php echo $this->lang->line('general_completeall_error') ?></p>
				<div class="text-right">
					<button  class="btn btn-white" data-dismiss="modal"><?= $this->lang->line('general_ok') ?></button>
				</div>
			</div>

		</div>

	</div>

</div>

<div class="modal flat-style animated bounceIn" id="editModal" role="dialog">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-body">
				<p><?php echo $this->lang->line('administration_items_doc_createfoldermessage') ?></p>
				<div class="text-right">
					<button  class="btn btn-white" data-dismiss="modal"><?= $this->lang->line('general_ok') ?></button>
				</div>
			</div>

		</div>

	</div>


	<script>

	$('#btnEdit').on('click',function(event)
	{

		event.preventDefault();

		if($('#name').val().trim() === '')
		{
			$('#incomplete').modal('show');
			$('#actionsButtons').show();
			$('#loadingEdit').hide();
		}
		else
		{
			$.ajax(
			{
				method:'POST',
				url:'/'+FOLDERADD+'/items/editfolder',
				data:{'itemdocfolderId':<?= $itemToEdit->itemdocfolderId ?>, 'name': $('#name').val()},
				beforeSend:function()
				{
					$('#actionsButtons').hide();
					$('#loadingEdit').show();
				}
			})
			.done(function(res)
			{
				if(res != "fail")
				{	
					$('#editModal').modal('show');
				}
				else
				{
					$('#incomplete').modal('show');

				}

				$('#actionsButtons').show();
				$('#loadingEdit').hide();
			});
		}
	});
	</script>