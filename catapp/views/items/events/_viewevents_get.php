<?php if (isset($_SESSION['itemdiaryMessage']) && $this->Identity->Validate('items/item/event/actions')){ ?>
<div class="alert alert-success alert-dismissible" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	<strong><i class="fa fa-check"></i></strong> 
	<?php if ($_SESSION['itemdiaryMessage'] == 'create'){
		echo $this->lang->line('administration_items_event_createmessage');
	} elseif ($_SESSION['itemdiaryMessage'] == 'delete'){
		echo $this->lang->line('administration_items_event_deletemessage');
	}elseif ($_SESSION['itemdiaryMessage'] == 'deleteall'){
		echo $this->lang->line('administration_items_event_deleteallmessage');
	}elseif ($_SESSION['itemdiaryMessage'] == 'edit'){
		echo $this->lang->line('administration_items_event_editmessage');
	}
	?>
</div>
<?php 
}

if (count($diary) <= 0) {
	echo $this->lang->line('administration_items_nodiary');
}
else{ 
	foreach ($diary as $event) {?>

	<div class="panel panel-default">
		<div class="panel-heading">
			<h6 class="panel-title-nomargin">
				<?php if($this->Identity->Validate('items/item/event/actions')){?>
				<?php if($this->Identity->Validate('items/item/event/actions/delete')){?>
				<button id="deleteeventId-<?=$event->itemeventsId?>" class="btn btn-xs pull-right"><i class="text-danger fa fa-calendar-times-o fa-lg"></i></button>
				<?php }
				if($this->Identity->Validate('items/item/event/actions/edit')){ ?>		
				<a href="/<?=FOLDERADD?>/items/item/<?=$itemId?>?cmd=diary&action=edit&itemeventsId=<?=$event->itemeventsId?>" class="btn btn-xs pull-right"><i class="text-warning fa fa-pencil fa-lg"></i></a>
				<?php }
				}?>
				<i class="fa fa-calendar fa-fw fa-lg"></i>
				<strong>
					<?php
					echo date('Y-m-d H:i',$event->timestamp); 
					if ($event->duration != NULL) {echo ' | '.$this->lang->line('administration_items_event_duration').': '.encodeQuery($event->duration);}
					if ($event->location != NULL) {echo ' | '.$this->lang->line('administration_items_event_location').': '.encodeQuery($event->location);}
					?>
				</strong>
			</h6>
		</div>
		<div class="panel-body">
			<?php if($this->Identity->Validate('items/item/event/actions/delete')){?>
			<div class="alert alert-danger text-center" role="alert" id="deletealerteventId-<?=$event->itemeventsId?>" style="display:none;" 	>
				<div><strong><?=$this->lang->line('administration_items_event_deleteareyousure')?></strong></div>
				<a href="/<?=FOLDERADD?>/items/item/<?=$itemId?>?cmd=diary&action=deleteevent&itemeventsId=<?=$event->itemeventsId?>" class="btn btn-default btn-sm"><?=$this->lang->line('general_delete')?></a>
				<button id="deletecanceleventId-<?=$event->itemeventsId?>" href="#" class="btn btn-default btn-sm"><?=$this->lang->line('general_cancel')?></button>
			</div>
			<?php }?>
			<p><strong><?=encodeQuery($event->title)?></p></strong>
			<?=$event->details?>
		</div>
	</div>

	<?php	} ?>
	<script type="text/javascript">
	<?php if($this->Identity->Validate('items/item/event/actions/delete')){ 
		foreach ($diary as $event) {?>
			$('#deletecanceleventId-<?=$event->itemeventsId?>').click(function(){
				$('#deletealerteventId-<?=$event->itemeventsId?>').slideUp();
			});
			$('#deleteeventId-<?=$event->itemeventsId?>').click(function(){
				$('#deletealerteventId-<?=$event->itemeventsId?>').slideDown();
			});
			<?php	}} ?>
			</script>

			<?php	}
			
			?>