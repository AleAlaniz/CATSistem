<form class="form-horizontal" method="POST" novalidate >
	<div class="form-group">
		<label for="title" class="col-sm-2 control-label"><?=$this->lang->line('administration_items_event_title');?> <strong class="text-danger">*</strong></label>
		<div class="col-sm-10">
			<input type="text" class="form-control input-sm" id="title" name="title" value="<?php echo set_value('title');?>" placeholder="<?=$this->lang->line('administration_items_event_title');?>" required>
			<?php echo form_error('title'); ?>
		</div>
	</div>

	<div class="form-group">
		<label for="date" class="col-sm-2 control-label"><?=$this->lang->line('administration_items_event_date');?></label>

		<div class="col-xs-10">
			<select  name="fday" id="fday">
				<?php 
				$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
					
				for ($i=1; $i <= 31; $i++) { 
					$selected = FALSE;
					if ($i == date('d')) {
						$selected = TRUE;
					}
					$zero = NULL;
					if ($i < 10) {
						$zero = 0;
					}
					echo '<option value="'.$zero.$i.'"'.set_select('fday',$zero.$i, $selected).'>'.$zero.$i.'</option>';
				}
				?>
			</select> /
			<select name="fmonth" id="fmonth">
				<?php 
				for ($i=1; $i <= 12; $i++) { 
					$selected = FALSE;
					if ($i == date('m')) {
						$selected = TRUE;
					}
					$zero = NULL;
					if ($i < 10) {
						$zero = 0;
					}
					echo '<option value="'.$zero.$i.'"'.set_select('fmonth',$zero.$i, $selected).'>'.$meses[$i-1].'</option>';
				}
				?>
			</select> /
			<select   name="fyear" id="fyear">
				<?php 
				for ($i=2000; $i <= 2020; $i++) { 
					$selected = FALSE;
					if ($i == date('Y')) {
						$selected = TRUE;
					}
					echo '<option value="'.$i.'"'.set_select('fyear',$i, $selected).'>'.$i.'</option>';
				}
				?>
			</select>
			&nbsp;-&nbsp;
			<select  name="fhour" id="fhour">
				<?php 
				for ($i=0; $i <= 23; $i++) { 
					$selected = FALSE;
					if ($i == date('G')) {
						$selected = TRUE;
					}
					$zero = NULL;
					if ($i < 10) {
						$zero = 0;
					}
					echo '<option value="'.$i.'"'.set_select('fhour',$i, $selected).'>'.$zero.$i.'</option>';
				}
				?>
			</select> : 
			<select name="fminute" id="fminute">
				<?php 
				for ($i=0; $i <= 59; $i++) { 
					$selected = FALSE;
					if ($i == date('i')) {
						$selected = TRUE;
					}
					$zero = NULL;
					if ($i < 10) {
						$zero = 0;
					}
					echo '<option value="'.$zero.$i.'"'.set_select('fminute',$zero.$i, $selected).'>'.$zero.$i.'</option>';
				}
				?>
			</select> (d/m/a hh:mm)
		</div>

		<?php echo form_error('date'); ?>

	</div>

	<div class="form-group">
		<label for="duration" class="col-sm-2 control-label"><?=$this->lang->line('administration_items_event_duration');?></label>
		<div class="col-sm-10">
			<input type="text" class="form-control input-sm" id="duration" name="duration" value="<?php echo set_value('duration');?>" placeholder="<?=$this->lang->line('administration_items_event_duration');?>" required>
			<?php echo form_error('duration'); ?>
		</div>
	</div>

	<div class="form-group">
		<label for="location" class="col-sm-2 control-label"><?=$this->lang->line('administration_items_event_location');?></label>
		<div class="col-sm-10">
			<input type="text" class="form-control input-sm" id="location" name="location" value="<?php echo set_value('location');?>" placeholder="<?=$this->lang->line('administration_items_event_location');?>" required>
			<?php echo form_error('location'); ?>
		</div>
	</div>


	<div class="form-group">
		<label for="details" class="col-sm-2 control-label"><?=$this->lang->line('administration_items_event_details');?></label>
		<div class="col-sm-10">
			<textarea class="form-control" id="details" name="details" rows="10"><?php echo set_value('details');?></textarea>
			<?php echo form_error('details'); ?>
		</div>
	</div>
	<div class="form-group text-center">
		<button type="submit" class="btn btn-success btn-sm"><?=$this->lang->line('general_save');?></button>
		<a href="/<?=FOLDERADD?>/items/item/<?=$itemId?>?cmd=diary" class="btn btn-danger btn-sm"><?=$this->lang->line('general_cancel');?></a>
	</div>
</form>
	<script type="text/javascript" src="/<?=APPFOLDERADD?>/libraries/tinymce/tinymce.min.js"></script>
	<script type="text/javascript">
	tinymce.init({
		toolbar: "cut copy paste | alignleft aligncenter alignright alignjustify | bullist numlist  | outdent indent | forecolor backcolor fontsizeselect  | bold italic underline | link unlink image media | table",
		plugins: "image link media textcolor table",
		selector: "#details",
		language: 'es'
	});
	</script>