<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->lang->load('login_lang');
?><!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
	<link rel="shortcut icon" href="/<?php echo APPFOLDERADD;?>/libraries/images/favicon.png" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" type="text/css" href="/<?php echo APPFOLDERADD;?>/libraries/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="/<?php echo APPFOLDERADD;?>/libraries/css/style.min.css">
	<title>CAT - Technologies</title>
</head>
<body style="background-color: #FFF">
	<div class="container-fluid logincont">
		<div class="row formlogin">
			<div class="col-sm-offset-1 col-sm-5" id="logogroup">
				<img src="/<?php echo APPFOLDERADD;?>/libraries/images/logo.png" id="logoimg" >
			</div>
			<div>
				<div class="col-sm-2" id="logingroup">
					<?php if (isset($_SESSION['auth_status'])){?>
						<div class="alert alert-danger loginmessage" role="alert" id="ErrorMessage">
							<?php echo $_SESSION['auth_status']; ?>
						</div>
					<?php } ?>
					<form action="/<?=FOLDERADD?>/home/autenticate" id="FormLogin" method="post">
						<div class="form-horizontal">
							<div class="form-group">
								<label class="control-label" for="UserName" style="color:#000"><?=$this->lang->line('login_username');?></label>
								<input class="form-control block text-box single-line input-sm" required="required" id="UserName" name="UserName" value="" type="text">
							</div>
							<div class="form-group" >
								<label class="control-label" for="Password" style="color:#000"><?=$this->lang->line('login_password');?></label>
								<input class="form-control block text-box single-line password input-sm" required id="Password" name="Password" value="" type="password">
							</div>
							<div class="form-group">
								<a id="dont_remember_pass" href="#"><?= $this->lang->line('login_dont_remember_pass') ?></a>
							</div>
							<div class="form-group">
								<input id="Enter" value="<?=$this->lang->line('login_signin')?>" class="btn btn-info" type="submit">
								<img id="ajax-load-login" src="/<?php echo APPFOLDERADD;?>/libraries/images/ajax-loader.gif" style="display:none;">
							</div>
						</div>
					</form>
				</div>
				<div class="col-sm-5" id="questiongroup">
					<h4 class="" style="margin-bottom:25px">Reestablecer contraseña.</h4>
					<form action="#" id="send_answer" method="post">
						<div class="form-horizontal">
							<div class="form-group">
								<label class="col-sm-2 control-label"><?=$this->lang->line('login_username');?></label>
								<div class="col-sm-10">
									<input type="text" class="form-control input-sm" id="user_name" name="user_name">
									<?php echo form_error('user_name'); ?>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label"><?=$this->lang->line('question_question')?></label>
								<div class="col-sm-10">
									<select class="form-control input-sm" id="questions" name="question">
										<option value="0"><?php echo $this->lang->line('question_select_one'); ?></option>
									</select>
									<?php echo form_error('question'); ?>
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-2 control-label"><?=$this->lang->line('question_answer');?></label>
								<div class="col-sm-10">
									<input type="password" class="form-control input-sm" id="answer" name="answer">
									<?php echo form_error('answer'); ?>
								</div>
							</div>
							<div class="form-group text-center">
								<div class="col-sm-11 col-sm-offset-1">
									<div id="questions_buttons">
										<input id="send_answer_button" value="<?=$this->lang->line('general_send')?>" class="btn btn-info" type="submit">
										<input id="back_to_login" value="<?=$this->lang->line('general_back')?>" class="btn btn-warning" type="button">
									</div>
									<div id="ajax-load-question" style="display:none;">
										<img src="/<?php echo APPFOLDERADD;?>/libraries/images/ajax-loader.gif">
									</div>
								</div>
							</div>
							<div class="form-group">
								<div style="display: none;" class="alert alert-danger loginmessage" role="alert" id="ErrorMessageQuestion"></div>
							</div>
						</div>
					</form>
				</div>
				<div class="col-sm-5" id="question_loading" style="display:none;height: 250px">
					<img class="center-block" src="/<?php echo APPFOLDERADD;?>/libraries/images/ajax-loader.gif" style="margin-top: 110px">
				</div>
				

				
				<div class="pull-right" style="" id="campusBannerRight">
					<img src="/<?php echo APPFOLDERADD;?>/libraries/images/catnet.png"  style="height:50px" >
				</div>
			</div>
		</div>
		<div id="loginfooter">
			<img src="/<?php echo APPFOLDERADD;?>/libraries/images/favicon.png">
			<span>Copyright © CAT - Technologies., <?php echo date("Y");?>. All rights reserved.</span>
		</div>
		<script src="/<?php echo APPFOLDERADD;?>/libraries/script/jquery-1.11.3.min.js"></script>
		<script src="/<?php echo APPFOLDERADD;?>/libraries/script/bootstrap.min.js"></script>
		<script>

			$(function () {
				
				$("#FormLogin").submit(function () {
					$("#Enter").attr("disabled", true);
					return true;
				});

				$("#dont_remember_pass").click(show_security_questions);

				function show_security_questions()
				{
					$("#logingroup").animate({opacity:0}, 280, hide_login_form_success);

					function hide_login_form_success()
					{
						$("#logingroup").css({"display":"none"});
						$("#question_loading").css({"display":"block"});
						
						$("option[name=question_option]").remove();
						$("#user_name").val($("#UserName").val());
						$.ajax({method: "POST", url: 'securityquestions/get_security_questions'}).done(get_questions_success);

						function get_questions_success(data)
						{
							data = JSON.parse(data);

							for(var i = 0; i < data.length; i++)
							{
								var option = $('<option>', {value : data[i].securityquestionId, text : data[i].question, name : 'question_option'});
								$("#questions").append(option);
							}

							$("#question_loading").css({"display":"none"});
							
							$("#questiongroup").css({"display":"block"});
							$("#questiongroup").animate({opacity:1}, 250);
						}
					}
				}

				$("#back_to_login").click(show_login_form);

				function show_login_form()
				{
					$("#questiongroup").animate({opacity:0}, 280, show_login_form);

					function show_login_form()
					{
						$("#questiongroup").css({"display":"none"});
						$("#logingroup").css({"display":"block"});
						$("#logingroup").animate({opacity:1}, 250);
					}
				}

				$("#send_answer").submit(send_answer);

				function send_answer(e)
				{
					e.preventDefault();

					var datosForm = 
					{
						user_name: 	 $("#user_name").val(),
						question_id: $("#questions").find(":selected").val(),
						answer: 	 $("#answer").val()
					};

					$("#ErrorMessageQuestion").hide();
					$("#questions_buttons").hide();
					$("#ajax-load-question").show();

					$.ajax({method: "POST", url: 'securityquestions/valid_answer',data: datosForm}).done(send_answer_success);

					function send_answer_success(data)
					{
						console.log(data);

						if (data == "success")
						{
							//$(location).attr('href', '/<?php echo FOLDERADD;?>');
						}
						else 
						{
							$("#ajax-load-question").hide();
							$("#ErrorMessageQuestion").html(data);
							$("#ErrorMessageQuestion").slideDown();
							$("#questions_buttons").show();
						}
					}
				}
			});

		</script>
		<?php
		if (!isset($fileContent) || !isset($fileType)) {

			$sql = "SELECT * FROM themes WHERE isselected = 'TRUE'";
			$query = $this->db->query($sql,array($this->uri->segment(3)))->row();
			if (isset($query))
			{
				$sql = "SELECT * FROM themeImages WHERE themeId = ? ORDER BY rand()";
				$query->images = $this->db->query($sql,array($query->themeId))->result();
				if (count($query->images) > 0) {
					$bgImage = '/'.APPFOLDERADD.'/libraries/themes/images/'.$query->images[0]->image;
					?>
					<script>

						$(".logincont").css('background-image', 'url("<?=$bgImage?>")');

					</script>
					<?php
				}
				else
				{
					?>
					<script>
						var aleatorio = Math.round(Math.random()*4) + 1;
						$(".logincont").addClass('background-login-' + aleatorio);
					</script>
					<?php
				}
			}
			else
			{
				?>
				<script>
					var aleatorio = Math.round(Math.random()*4) + 1;
					$(".logincont").addClass('background-login-' + aleatorio);
				</script>
				<?php
			}
		}
		else
		{
			?>
			<script>
				$(".logincont").css('background-image', 'url(data:<?php echo $fileType; ?>;base64,<?php echo $fileContent; ?>)');
				$("input").attr('disabled', true);
			</script>
			<?php
		}
		?>

		<script src="/<?php echo APPFOLDERADD;?>/libraries/script/html5shiv.min.js"></script>
		<script src="/<?php echo APPFOLDERADD;?>/libraries/script/respond.min.js"></script>
	</body>
	</html>