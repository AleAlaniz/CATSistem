
<div style="margin-top:20px" ng-app="corporatedocs" ng-controller="form" class="row">

	<input type="hidden" id="corporatedoc" value="<?php echo $corporatedocId; ?>" />
	<div class="modal fade" id="message" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-sm ">
			<div class="modal-content">
				<div class="modal-body">
					<p><?php echo $this->lang->line('corporatedocs_complete_message')?></p>
					<div class="text-right">
						<span class="btn btn-white" data-dismiss="modal" aria-label="Close"><?php echo $this->lang->line('general_accept'); ?></span>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="col-md-10 col-md-offset-1">
		<div class="text-center">
			<h3><?= $name ?></h3>
			<p><?= $description ?></p>
			<div style="margin-top:15px" ng-show="!loading">
				<form id="form-check" ng-show="!readed">
					<label  style="cursor:pointer">
						<input type="checkbox" ng-click="acceptCondition()" value="true">
						<?php echo $this->lang->line('corporatedocs_checkmessage'); ?>
					</label>
				</form>
			</div>

			<p ng-show="loading"><i class="fa fa-refresh fa-spin fa-3x"></i></p>
			<div/>

			<div>
				<a class="btn btn-green" ng-class="{disabled : !readed}" href="/<?php echo FOLDERADD; ?>/corporatedocs/open/<?=$corporatedocId?>" target="_blank">
					<?php echo $this->lang->line('corporatedocs_downloadfile'); ?>
				</a>
			</div>
			<br>
			<div>
				<a class="btn btn-info" ng-if="readed" href="/<?php echo FOLDERADD ?>">
					<?php echo $this->lang->line('corporatedocs_gotocatnet'); ?>
				</a>
			</div>
		</div>
	</div>

	<script src="/<?php echo APPFOLDERADD; ?>/libraries/script/angular.min.js"></script>
	<script type="text/javascript">

	angular
	.module("corporatedocs", [])
	.controller("form", ["$scope","$http","$location",function($scope,$http,$location) {

		var url="/<?php echo FOLDERADD ?>/";
		$scope.loading=false;
		$scope.readed=false;

		$scope.acceptCondition=function()
		{
			$scope.loading=true;

			$.post(url+"corporatedocs/readdoc",
			{
				'corporatedoc':document.getElementById('corporatedoc').value
			},
			function(data, status)
			{
				if(data == 'ok')
				{
					$scope.readed=true;
					$scope.loading=false;
				}
				else if(data != 'error')
				{
					window.location.href=url;
				}
				$scope.$apply();
			});
		}

	}]);


	$(document).ready(function() {
		$('#message').modal('show');
	});
	</script>
