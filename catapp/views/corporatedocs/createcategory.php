<div class="page-title">
    <h3 class="title"><?=$this->lang->line('corporatedocs_ccategory');?></h3>
    <a href="#/corporatedocs" class="btn btn-white pull-right"><i class="fa fa-chevron-left"></i> <?=$this->lang->line('general_goback');?></a>
</div>
<div class="col-xs-12 flat-style">
    <div class="panel panel-default">
        <div class="panel-body">
            <form class="form-horizontal" method="POST" novalidate ng-submit="createCategory()" id="corporatecatForm">
                <h3 style="margin-top: 0; margin-bottom: 0; padding-left: 10px"><?php echo $this->lang->line('corporatedocs_info'); ?></h3>
                <hr style="margin-top: 10px; "/>
                <!--nombre-->
                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label"><?=$this->lang->line('corporatedoccategory_name');?><span class="text-danger"><strong> *</strong></span></label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control input-sm" id="name" name="name" ng-model="categorydata.name" placeholder="<?=$this->lang->line('corporatedoccategory_name');?>" >
                    </div>
                </div>
                <hr>
                <!-- categorias-->
                <div class="form-group">
                    <label for="section" class="col-sm-2 control-label"><?=$this->lang->line('corporatedocs_category');?><span class="text-danger"><strong> *</strong></span></label>
                    <div class="col-sm-10">
                        <select ng-change="cargarCampos()" class="form-control input-sm" ng-model="category" id="category" name="category">
                            <option value=""><?= $this->lang->line('corporatedocs_no_category');?></option>
                            <option ng-repeat="cat in categories" value="{{cat.corporatedoccategoryId}}">{{cat.name}}</option>
                        </select>
                    </div>
                </div>
                <!--(Quien lo puede ver)-->
                <h3 style="margin-top: 0; margin-bottom: 0; padding-left: 10px"><?php echo $this->lang->line('corporatedocs_link'); ?> <strong class="text-danger">*</strong></h3>
                <hr style="margin-top: 10px; "/>
                <!--sections-->
                <div class="form-group" ng-show="secciones.length > 0">
                    <label class="col-sm-2 control-label"><?=$this->lang->line('general_sections');?></label>
                    <div class="col-sm-10">
                        <div class="checkbox checkbox-angular" ng-repeat="section in secciones track by $index" ng-init="i=$index">
                            <label ng-class="{check: sections[i]}">
                                <input type="checkbox" name="sections[]" ng-model="sections[i]"  value={{section.sectionId}}>
                                <?php echo encodeQuery("{{section.name}}");?>
                            </label>
                        </div>
                    </div>
                </div>
                <!--sites-->
                <div class="form-group" ng-show="sitios.length > 0">
                    <label class="col-sm-2 control-label"><?=$this->lang->line('general_sites');?></label>
                    <div class="col-sm-10">
                        <div class="checkbox checkbox-angular" ng-repeat="site in sitios track by $index" ng-init="i=$index">
                            <label ng-class="{check: sites[i]}">
                                <input type="checkbox" name="sites[]" ng-model="sites[i]"  value={{site.siteId}}>
                                <?php echo encodeQuery("{{site.name}}");?>
                            </label>
                        </div>
                    </div>
                </div>
                <!-- perfiles-->
                <div class="form-group" ng-show="perfiles.length > 0">
                    <label class="col-sm-2 control-label"><?=$this->lang->line('general_roles');?></label>
                    <div class="col-sm-10">
                        <div class="checkbox checkbox-angular" ng-repeat="role in perfiles track by $index" ng-init="i=$index">
                            <label ng-class="{check: roles[i]}">
                                <input type="checkbox" name="roles[]" ng-model="roles[i]"  value={{role.roleId}}>
                                <?php echo encodeQuery("{{role.name}}");?>
                            </label>
                        </div>
                    </div>
                </div>
                <!--usergroups-->
                <?php
                if ($this->Identity->Validate('usergroups/create')) {
                    ?>
                    <div class="form-group" ng-show="grupouser.length > 0">
                        <label class="col-sm-2 control-label"><?=$this->lang->line('general_usergroups');?></label>
                        <div class="col-sm-10">
                            <div class="checkbox checkbox-style" ng-repeat="usergroup in grupouser track by $index" ng-init="i=$index">
                                <label ng-class="{check: userGroups[i]}">
                                    <input class="bind" type="checkbox" ng-model="userGroups[i]" name="userGroups[]" value={{usergroup.usergroupId}}>
                                    <?php echo encodeQuery("{{usergroup.name}}");?>
                                </label>
                            </div>
                        </div>
                    </div>
                    <?php
                }
                ?>
                <!--Este es el buscador de usuarios-->
                <div class="form-group">
                    <input type="hidden" ng-repeat="user in users track by $index" name="users[]" value="{{user.userId}}" />
                    <label class="col-sm-2 control-label"><?=$this->lang->line('general_users');?></label>
                    <div class="col-sm-10 form-group">
                        <div class="select-container" ng-click="selectClick($event);">
                            <i class="fa fa-spin fa-refresh " style="position:absolute; top:13px;right:25px" ng-show="finding"></i>
                            <span class="select-selected">
								<span class="select-selected-item" ng-repeat="user in users track by $index" ng-init="i=$index">
									<span class="selected-item-image" style="background-image: url('/<?php echo FOLDERADD; ?>/users/profilephoto?userId={{user.userId}}&amp;amp;wah=200');filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='/<?php echo FOLDERADD; ?>/users/profilephoto?userId={{user.userId}}&wah=200',sizingMethod='scale');"></span>
									<span class="selected-item-label" ng-bind="user.completeName+' <'+user.userName+'>'" ></span>
									<span class="selected-item-delete no-selectable" ng-click="removeUser(i)">&times;</span>
								</span>
							</span>
                            <input type="text" autocomplete="off" class="input-sm form-control select-input" ng-trim="true" ng-model="findLike" ng-change="searchUsers()" id="findInput" placeholder="<?=$this->lang->line('survey_writename');?>" />
                        </div>
                        <div class="select-options-container-super" ng-show="userOptions.length > 0">
                            <div class="select-options-container">
                                <div class="select-option no-selectable" ng-repeat="user in userOptions track by $index" ng-init="i=$index" ng-click="addUser(i)">
                                    <span class="selected-option-image" style="background-image: url('/<?php echo FOLDERADD; ?>/users/profilephoto?userId={{user.userId}}&amp;amp;wah=200');filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='/<?php echo FOLDERADD; ?>/users/profilephoto?userId={{user.userId}}&wah=200',sizingMethod='scale');"></span>
                                    <span class="selected-option-label" ng-bind="user.completeName+' <'+user.userName+'>'"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="form-group text-center">
                    <button type="submit" class="btn btn-green " id="corporatedocs_createcategory" ng-show='!saving'><?=$this->lang->line('general_create');?></button>
                    <button type="submit" class="btn btn-green " disabled='disabled' ng-show='saving'><i class='fa fa-refresh fa-spin fa-lg fa-fw'></i></button>
                </div>

            </form>



            <!--Modal para errores-->
            <div class="modal fade" id="no-complete" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-body">
                            <p><?php echo $this->lang->line('corporatedoccategory_completeall')?> <strong class="text-danger">*</strong></p>
                            <div class="text-right">
                                <span class="btn btn-white" data-dismiss="modal" aria-label="Close"><?php echo $this->lang->line('general_accept'); ?></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>