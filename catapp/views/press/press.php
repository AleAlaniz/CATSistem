<div class="page-title">
	<h3 class="title"><?php echo $this->lang->line('press_title'); ?></h3>
	<?php if($this->Identity->Validate('press/create') || $this->Identity->Validate('press/clipping/create')) 
	{ 
		?>
		<div class="btn-group">
			<span aria-expanded="false" class="btn btn-white dropdown-toggle" data-toggle="dropdown"><i class="fa fa-plus text-muted"></i> <?php echo $this->lang->line('general_create')?></span>
			<ul class="dropdown-menu dropdown-white dropdown-menu-scale">
				<?php if($this->Identity->Validate('press/create')) 
				{ 
					?>
					<li><a href="#/press/create"><?php echo $this->lang->line('press_create')?></a></li>
					<?php 
				}
				?>
				<?php if($this->Identity->Validate('press/clipping/create')) 
				{ 
					?>
					<li><a href="#/press/createclipping"><?php echo $this->lang->line('press_createclipping')?></a></li>
					<?php 
				}
				?>
			</ul>
		</div>
		<?php 
	}
	?>
</div>
<div class="col-xs-12 flat-style">
	<div class="alert bg-green" role="alert" ng-show="message.message != '' && message.message != null">
		<button type="button" class="close"  ng-click="message.setMessage('', true)"><span>&times;</span></button>
		<strong><i class="fa fa-check"></i> <span ng-bind="message.message"></span>
		</strong> 
	</div>
	<div class="modal animated shake" id="confirm-delete" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-sm ">
			<div class="modal-content">
				<div class="modal-body">
					<p id="delete_message"></p>
					<div class="text-right">
						<span class="btn btn-white" data-dismiss="modal" aria-label="Close"><?php echo $this->lang->line('general_no'); ?></span>
						<span class="btn btn-lightgreen" id="delete_button_yes"><?php echo $this->lang->line('general_yes'); ?></span>
						<span class="btn btn-lightgreen" disabled="true" id="delete_button_loading" style="display:none"><i class='fa fa-refresh fa-spin fa-lg fa-fw'></i></span>
					</div>
				</div>
			</div>
		</div>
	</div>
	<span ng-init="deletenoteconfirm = <?php echo "'".$this->lang->line('press_newsdeleteareyousure')."'"; ?>"></span>
	<span ng-init="deleteclippingconfirm = <?php echo "'".$this->lang->line('press_clippingdeleteareyousure')."'"; ?>;"></span>
	<?php
	$gridClass = '12';
	if ($this->Identity->Validate('press/note/view') && $this->Identity->Validate('press/clipping/view')) {
		$gridClass = '6';
	}
	if ($this->Identity->Validate('press/clipping/view')) {
		?>
		<div class="col-sm-<?php echo $gridClass; ?>">
			<div class="panel panel-default" >
				<img class="image-fix"  src="/<?php echo APPFOLDERADD;?>/libraries/images/prensa_header_clipping_2018.png" style="margin-bottom: 15px;">
				<div class="panel-body"> 
					<div id="press_clipping_container" style="display:none">
						<div ng-repeat="clipping in clippings" style="padding-bottom: 20px;">
							<p style="padding-bottom: 20px;">
								<?php
								if($this->Identity->Validate('press/clipping/delete')) 
								{ 
									?>
									<span class="pull-right btn btn-xs" style="margin-right:10px" data-target="#confirm-delete" data-toggle="modal" data-id="{{clipping.pressclippingId}}" data-type="clipping"><i class="fa fa-times fa-lg text-danger"></i></span>
									<?php
								}
								if($this->Identity->Validate('press/clipping/edit')) 
								{ 
									?>
									<a href="#/press/editclipping/{{clipping.pressclippingId}}" class="pull-right btn btn-xs  m-r_10"><i class="fa fa-pencil fa-lg text-warning"></i></a>
									<?php
								}
								?>
							</p>
							<h4 class="strong" ng-bind-html="clipping.title"></h4>
							<div ng-bind-html="clipping.clipping | HtmlSanitize"></div>
							<hr ng-hide="$index == clippings.length-1">
						</div>
						<div class="alert bg-red strong text-center" style="margin-bottom:0" ng-hide="clippings.length > 0">
							<?php echo $this->lang->line('press_clippingempty');?>
						</div>
						<div class="alert bg-red strong text-center" style="margin-bottom:0; margin-top:10px" ng-show="clippingfull">
							<?php echo $this->lang->line('press_clippingfull');?>
						</div>
						<span class="btn btn-red center-block" id="press_clipping_loadold" ng-click="loadOldClipping()" ng-hide="clippings.length == 0"><?=$this->lang->line('press_loadclippingnotes');?></span>
						<span id="press_clipping_loadingold" style="display:none" class="btn btn-block center-block btn-red text-center"> <i class="fa fa-refresh fa-spin fa-lg fa-lg"></i></span>
					</div>
					<div class="text-center" id="press_clipping_loading">
						<i class='fa fa-refresh fa-spin fa-5x'></i>
					</div>
				</div>
				<img class="image-fix"  src="/<?php echo APPFOLDERADD;?>/libraries/images/prensa_pie_2018.png" style="margin-top: 15px;">
			</div>
		</div>
		<?php
	}
	if ($this->Identity->Validate('press/note/view')) {
		?>
		<div class="col-sm-<?php echo $gridClass; ?>">
			<div class="panel panel-default">
				<img class="image-fix"  src="/<?php echo APPFOLDERADD;?>/libraries/images/prensa_header_2018.png" style="margin-bottom: 15px;">
				<div class="panel-body">
					<div id="press_note_container" style="display:none">
						<div ng-repeat="note in notes" style="padding-bottom: 20px;">
							<p style="padding-bottom: 20px;">
								<?php
								if($this->Identity->Validate('press/delete')) 
								{ 
									?>
									<span class="pull-right btn btn-xs" style="margin-right:10px" data-target="#confirm-delete" data-toggle="modal" data-id="{{note.pressnoteId}}" data-type="note"><i class="fa fa-times fa-lg text-danger"></i></span>
									<?php
								}
								if($this->Identity->Validate('press/edit')) 
								{ 
									?>
									<a href="#/press/edit/{{note.pressnoteId}}" class="pull-right btn btn-xs  m-r_10"><i class="fa fa-pencil fa-lg text-warning"></i></a>
									<?php
								}
								?>
							</p>
							<div ng-bind-html="note.pressnote | HtmlSanitize"></div>
							<hr ng-hide="$index == note.length-1">
						</div>
						<div class="alert bg-red strong text-center" style="margin-bottom:0" ng-hide="notes.length > 0">
							<?php echo $this->lang->line('press_empty');?>
						</div>
						<div class="alert bg-red strong text-center" style="margin-bottom:0; margin-top:10px" ng-show="notefull">
							<?php echo $this->lang->line('press_full');?>
						</div>
						<span class="btn btn-red center-block" id="press_note_loadold" ng-click="loadOldNote()" ng-hide="notes.length == 0"><?=$this->lang->line('press_loadpressnotes');?></span>
						<span id="press_note_loadingold" style="display:none" class="btn btn-block center-block btn-red text-center"> <i class="fa fa-refresh fa-spin fa-lg fa-lg"></i></span>
					</div>
					<div class="text-center" id="press_note_loading">
						<i class='fa fa-refresh fa-spin fa-5x'></i>
					</div>

				</div>
				<img class="image-fix"  src="/<?php echo APPFOLDERADD;?>/libraries/images/prensa_pie_2018.png" style="margin-top: 15px;">
			</div>
		</div>
		<?php
	}
	?>
</div>