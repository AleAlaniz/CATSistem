<div class="page-title">
	<h3 class="title"><?php echo $this->lang->line('press_createnote');?></h3>
	<a href="#/press" class="btn btn-white pull-right"><i class="fa fa-chevron-left"></i> <?php echo $this->lang->line('general_goback');?></a>
</div>
<div class="col-xs-12 flat-style">
	
	<div class="panel panel-default">
		<div class="panel-body">
			<form  method="POST" novalidate ng-submit="create()" >
			<h3 style="margin-top: 0; margin-bottom: 0; padding-left: 10px"><?php echo $this->lang->line('press_info'); ?></h3>
				<hr style="margin-top: 10px; "/>
				<div class="form-group" style="overflow:auto">
					<label for="pressnote" class="col-sm-2 control-label"><?php echo $this->lang->line('press_pressnote');?><span class="text-danger"><strong> *</strong></span></label>
					<div class="col-sm-10">
						<textarea class="form-control" id="pressnote" name="pressnote" rows="20"></textarea>
					</div>
				</div>
				<hr>
				<div class="form-group text-center">
					<button type="submit" class="btn btn-green" id="press_note_create"><?php echo $this->lang->line('general_save');?></button>
				</div>
			</form>
			<div class="modal fade" id="no-complete" tabindex="-1" role="dialog">
				<div class="modal-dialog modal-sm">
					<div class="modal-content">
						<div class="modal-body">
							<p><?php echo $this->lang->line('general_completeall'); ?> <strong class="text-danger">*</strong></p>
							<div class="text-right">
								<span class="btn btn-white" data-dismiss="modal" aria-label="Close"><?php echo $this->lang->line('general_accept'); ?></span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="/<?php echo APPFOLDERADD; ?>/libraries/tinymce/tinymce.min.js"></script>