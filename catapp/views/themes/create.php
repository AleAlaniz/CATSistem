<ol class="breadcrumb">
	<li><a href="/<?=FOLDERADD?>/administration"><?=$this->lang->line('general_administration');?></a></li>
	<li><a href="/<?=FOLDERADD?>/themes"><?=$this->lang->line('general_themes');?></a></li>
	<li class="active"><?=$this->lang->line('administration_create');?></li>
</ol>
<div class="panel panel-default">
	<div class="panel-heading">
		<strong><?=$this->lang->line('administration_themes_create');?></strong>
	</div>
	<div class="panel-body">
		<form class="form-horizontal" method="POST" enctype="multipart/form-data" novalidate id="myform" >
			<div class="form-group">
				<label for="name" class="col-sm-2 control-label"><?=$this->lang->line('administration_themes_name');?><span class="text-danger"><strong> *</strong></span></label>
				<div class="col-sm-10">
					<input type="text" class="form-control input-sm" id="name" name="name" value="<?php echo set_value('name');?>" placeholder="<?=$this->lang->line('administration_themes_name');?>" required>
					<?php echo form_error('name'); ?>
					<p role="nameError" class="text-danger" style="display:none"><?=$this->lang->line('administration_themes_nameerror');?></p>
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-2 control-label"><?=$this->lang->line('administration_themes_images');?><strong class="text-danger"> *</strong></label>
				<div class="col-sm-10" id="userfiles">
					<input accept="image/x-png, image/gif, image/jpeg"  type="file" name="userfile[]" value="<?php echo set_value('userfile[]');?>">
					<small><?=$this->lang->line('administration_themes_imagesallowtypes')?></small>
					<?php echo $error;?>
					<?php echo form_error('userfile[]'); ?>
				</div>
			</div>
			<hr>
			<div class="form-group text-center">
				<button type="submit" class="btn btn-success btn-sm"><?=$this->lang->line('general_save');?></button>
				<a href="/<?=FOLDERADD?>/themes" class="btn btn-danger btn-sm"><?=$this->lang->line('general_cancel');?></a>
			</div>
		</form>
	</div>
</div>
<form class="form-horizontal" enctype="multipart/form-data" target="_blank" method="POST" action="/<?=FOLDERADD?>/" novalidate id="formPreview">

</form>

<script type="text/javascript">
$(document).ready(function() {
	
	$('#nav_theme').addClass('active');
	function readURL(input) {


		var newFile = input, form = $('#formPreview');
		newFile.attr('name', 'prev');
		$( "<span id='tempPlace' style='display:none'>Place</span>" ).insertBefore(newFile);
		form.append(newFile);
		form.submit();
		newFile.attr('name', 'userfile[]');
		$("#tempPlace").replaceWith(newFile);

	}

	$('input[name^="userfile"]').on('change',function() {
		var allUsed = true;
		jQuery.each($('input[name^="userfile"]'), function(i, userfile) {
			var value = $(this).val();
			if (value == '') {
				allUsed = false;
			}
		});
		if (allUsed) {
			var newFile = $(this).clone(true);
			newFile.val('');
			$('#userfiles').prepend(newFile)
		}
		readURL($(this));
	});

	$('#myform').submit(function(e) {

		var $name = $('#name');
		jQuery.each($('input[name^="userfile"]'), function(i, userfile) {
			var value = $(this).val();
			if (value == '' && $('input[name^="userfile"]').length > 1) {
				console.log($(this));
				$(this).remove();
			}
		});

		if ($name.val() == '') {
			e.preventDefault();
			$('[role="nameError"]').show();
		}
		else
		{
			$('[role="nameError"]').hide();
		}
	});
});
</script>