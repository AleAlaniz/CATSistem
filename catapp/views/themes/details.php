<ol class="breadcrumb">
	<li><a href="/<?=FOLDERADD?>/administration"><?=$this->lang->line('general_administration');?></a></li>
	<li><a href="/<?=FOLDERADD?>/themes"><?=$this->lang->line('general_themes');?></a></li>
	<li><a href="/<?=FOLDERADD?>/themes/details/<?=$themeId;?>"><?=encodeQuery($name);?></a></li>
	<li class="active"><?=$this->lang->line('administration_details');?></li>
</ol>
	<?php if (isset($_SESSION['flashMessage'])): ?>
	<div class="alert alert-success alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<strong><i class="fa fa-check"></i></strong> 
		<?php if ($_SESSION['flashMessage'] == 'edit'){
			echo $this->lang->line('administration_themes_editmessage');
		}
		?>
	</div>
<?php endif; ?>
<div class="col-xs-12" style="margin-bottom:15px">
	<?=$navBar?>
</div>
<div class="col-xs-12">
<div class="panel panel-default">
	<div class="panel-heading">
		<strong><?=$this->lang->line('administration_themes_details');?></strong>
	</div>
	<div class="panel-body">
		<dl class="dl-horizontal col-sm-6">
			<dt><?=$this->lang->line('administration_themes_name');?></dt>
			<dd><?=encodeQuery($name)?></dd>
		</dl>
		<dl class="dl-horizontal col-sm-6">
			<dt><?=$this->lang->line('administration_themes_images');?></dt>
			<?php
			foreach ($images as $key => $image) {
				?>
					<dd style="margin-top:5px"><img src="/<?=APPFOLDERADD?>/libraries/themes/images/<?=$image->image?>" alt="" class="img-thumbnail"></dd>
				<?php
			}
			?>
		</dl>
	</div>
</div>
</div>
<script type="text/javascript">
$('#nav_theme').addClass('active');
$('#NavDetails').addClass('active');
</script>