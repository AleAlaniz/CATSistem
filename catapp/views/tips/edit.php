<div class="page-title">
	<h3 class="title"><?=$this->lang->line('tips_edit');?></h3>
	<a href="#/tips/view" class="btn btn-white pull-right"><i class="fa fa-chevron-left"></i> <?=$this->lang->line('general_goback');?></a>
</div>
<div class="col-xs-12 flat-style">
	<div class="panel panel-default">
		<div class="panel-body">
			<div class="text-center" ng-hide="tip">
				<i class='fa fa-refresh fa-spin fa-5x fa-fw' ></i>
			</div>

			<form class="form-horizontal ng-hide" method="POST" novalidate ng-submit="editTip()" id="createForm" ng-show="tip">
				<h3 style="margin-top: 0; margin-bottom: 0; padding-left: 10px"><?php echo $this->lang->line('general_info'); ?></h3>
				<hr style="margin-top: 10px; "/>
				<div class="form-group" >
					<label for="description" class="col-sm-2 control-label"><?=$this->lang->line('tips_tip');?><span class="text-danger strong"> *</span></label>
					<div class="col-sm-10">
						<textarea class="form-control" id="description" name="description" rows="10" ng-model="tip.description"></textarea>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label"><?=$this->lang->line('tips_feedback');?></label>
					<div class="col-xs-10">
						<div class="checkbox checkbox-angular">
							<label ng-class="{check: feedback}">
								<input type="checkbox" name="feedback" ng-model="feedback" value="TRUE">
							</label>
						</div>
					</div>
				</div>
				<h3 style="margin-top: 0; margin-bottom: 0; padding-left: 10px"><?php echo $this->lang->line('general_to'); ?></h3>
				<hr style="margin-top: 10px; "/>
				<div class="form-group">
					<label class="col-sm-2 control-label"><?=$this->lang->line('general_sections');?></label>
					<div class="col-sm-10">
						<?php
						foreach ($sections as $section) {
							?>
							<div class="checkbox checkbox-angular">
								<label ng-class="{check: sections['id-<?php echo $section->sectionId;?>']}">
									<input type="checkbox" name="sections[]" ng-model="sections['id-<?php echo $section->sectionId;?>']"  value="<?=$section->sectionId?>">
									<?php echo encodeQuery($section->name);?>
								</label>
							</div>
							<?php
						}
						?>

					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label"><?=$this->lang->line('general_sites');?></label>
					<div class="col-sm-10">
						<?php
						foreach ($sites as $site) {
							?>
							<div class="checkbox checkbox-angular">
								<label ng-class="{check: sites['id-<?php echo $site->siteId;?>']}">
									<input type="checkbox" name="sites[]" ng-model="sites['id-<?php echo $site->siteId;?>']"  value="<?=$site->siteId?>">
									<?php echo encodeQuery($site->name);?>
								</label>
							</div>
							<?php
						}
						?>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label"><?=$this->lang->line('general_roles');?></label>
					<div class="col-sm-10">
						<?php
						foreach ($roles as $role) {
							?>
							<div class="checkbox checkbox-angular">
								<label ng-class="{check: roles['id-<?php echo $role->roleId;?>']}">
									<input type="checkbox" name="roles[]" ng-model="roles['id-<?php echo $role->roleId;?>']"  value="<?=$role->roleId?>">
									<?php echo encodeQuery($role->name);?>
								</label>
							</div>
							<?php
						}
						?>

					</div>
				</div>

				<div class="form-group">
					<label class="col-sm-2 control-label"><?=$this->lang->line('general_campaigns');?></label>
					<div class="col-sm-10">
						<?php
						foreach ($campaigns as $campaign) {
							?>
							<div class="checkbox checkbox-angular">
								<label ng-class="{check: campaigns['id-<?php echo $campaign->campaignId;?>']}">
									<input type="checkbox" name="campaigns[]" ng-model="campaigns['id-<?php echo $campaign->campaignId;?>']"  value="<?=$campaign->campaignId?>">
									<?php echo encodeQuery($campaign->name);?>
								</label>
							</div>
							<?php
						}
						?>

					</div>
				</div>

				<?php
				if ($this->Identity->Validate('usergroups/create')) {
					?>
					<div class="form-group">
						<label class="col-sm-2 control-label"><?=$this->lang->line('general_usergroups');?></label>
						<div class="col-sm-10">
							<?php
							foreach ($userGroups as $userGroup) {
								?>
								<div class="checkbox checkbox-angular">
									<label ng-class="{check: userGroups['id-<?php echo $userGroup->userGroupId;?>']}">
										<input type="checkbox" name="userGroups[]" ng-model="userGroups['id-<?php echo $userGroup->userGroupId;?>']"  value="<?=$userGroup->userGroupId?>">
										<?php echo encodeQuery($userGroup->name);?>
									</label>
								</div>
								<?php
							}
							?>

						</div>
					</div>
					<?php
				}
				?>

				<div class="form-group" >
					<label class="col-sm-2 control-label"><?=$this->lang->line('general_users');?></label>
					<div class="col-sm-10 form-group" cn-search-users users="users" url="/<?php echo FOLDERADD;?>/tips/searchusers" placeholder="<?php echo $this->lang->line('general_find');?>"></div>
				</div>
				<hr>
				<div class="form-group text-center">
					<button type="submit" class="btn btn-green" ng-hide="editing"><?=$this->lang->line('general_edit');?></button>
					<button type="button" disabled="disabled" class="btn btn-green nh-hide" ng-show="editing"><i class="fa fa-refresh fa-spin fa-lg"></i></button>
				</div>
			</form>
			<div class="modal fade" id="no-complete" tabindex="-1" role="dialog">
				<div class="modal-dialog modal-sm">
					<div class="modal-content">
						<div class="modal-body">
							<p><?php echo $this->lang->line('general_completeall');?> <strong class="text-danger">*</strong></p>
							<div class="text-right">
								<span class="btn btn-white" data-dismiss="modal" aria-label="Close"><?php echo $this->lang->line('general_accept'); ?></span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="/<?php echo APPFOLDERADD; ?>/libraries/tinymce/tinymce.min.js"></script>
