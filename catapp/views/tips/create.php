<div class="page-title">
	<h3 class="title"><?=$this->lang->line('tips_create');?></h3>
	<a href="#/tips/view" class="btn btn-white pull-right"><i class="fa fa-chevron-left"></i> <?=$this->lang->line('general_goback');?></a>
</div>
<div class="col-xs-12 flat-style">
	<div class="panel panel-default">
		<div class="panel-body">
			<form class="form-horizontal" method="POST" novalidate ng-submit="create()" id="createForm">
				<h3 style="margin-top: 0; margin-bottom: 0; padding-left: 10px"><?php echo $this->lang->line('general_info'); ?></h3>
				<hr style="margin-top: 10px; "/>
				<div class="form-group" >
					<label for="description" class="col-sm-2 control-label"><?=$this->lang->line('tips_tip');?><span class="text-danger strong"> *</span></label>
					<div class="col-sm-10">
						<textarea class="form-control" id="description" name="description" rows="10"></textarea>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label"><?=$this->lang->line('tips_feedback');?></label>
					<div class="col-xs-10">
						<div class="checkbox checkbox-angular">
							<label ng-class="{check: feedback}">
								<input type="checkbox" name="feedback" ng-model="feedback" value="TRUE">
							</label>
						</div>
					</div>
				</div>
				<h3 style="margin-top: 0; margin-bottom: 0; padding-left: 10px"><?php echo $this->lang->line('general_to'); ?></h3>
				<hr style="margin-top: 10px; "/>
				<div class="form-group">
					<label class="col-sm-2 control-label"><?=$this->lang->line('general_sections');?></label>
					<div class="col-sm-10">
						<?php
						foreach ($sections as $key => $section) {
							?>
							<div class="checkbox checkbox-angular">
								<label ng-class="{check: sections[<?php echo $key;?>]}">
									<input type="checkbox" name="sections[]" ng-model="sections[<?php echo $key;?>]"  value="<?=$section->sectionId?>">
									<?php echo encodeQuery($section->name);?>
								</label>
							</div>
							<?php
						}
						?>

					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label"><?=$this->lang->line('general_sites');?></label>
					<div class="col-sm-10">
						<?php
						foreach ($sites as $key => $site) {
							?>
							<div class="checkbox checkbox-angular">
								<label ng-class="{check: sites[<?php echo $key;?>]}">
									<input type="checkbox" name="sites[]" ng-model="sites[<?php echo $key;?>]"  value="<?=$site->siteId?>">
									<?php echo encodeQuery($site->name);?>
								</label>
							</div>
							<?php
						}
						?>

					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label"><?=$this->lang->line('general_roles');?></label>
					<div class="col-sm-10">
						<?php
						foreach ($roles as $key => $role) {
							?>
							<div class="checkbox checkbox-angular">
								<label ng-class="{check: roles[<?php echo $key;?>]}">
									<input type="checkbox" name="roles[]" ng-model="roles[<?php echo $key;?>]"  value="<?=$role->roleId?>">
									<?php echo encodeQuery($role->name);?>
								</label>
							</div>
							<?php
						}
						?>

					</div>
				</div>

				<div class="form-group">
					<label class="col-sm-2 control-label"><?=$this->lang->line('general_campaigns');?></label>
					<div class="col-sm-10">
						<?php
						foreach ($campaigns as $key => $campaign) {
							?>
							<div class="checkbox checkbox-angular">
								<label ng-class="{check: campaigns[<?php echo $key;?>]}">
									<input type="checkbox" name="campaigns[]" ng-model="campaigns[<?php echo $key;?>]"  value="<?=$campaign->campaignId?>">
									<?php echo encodeQuery($campaign->name);?>
								</label>
							</div>
							<?php
						}
						?>

					</div>
				</div>

				<?php
				if ($this->Identity->Validate('usergroups/create')) {
					?>
					<div class="form-group">
						<label class="col-sm-2 control-label"><?=$this->lang->line('general_usergroups');?></label>
						<div class="col-sm-10">
							<?php
							foreach ($userGroups as $key => $userGroup) {
								?>
								<div class="checkbox checkbox-angular">
									<label ng-class="{check: userGroups[<?php echo $key;?>]}">
										<input type="checkbox" name="userGroups[]" ng-model="userGroups[<?php echo $key;?>]"  value="<?=$userGroup->userGroupId?>">
										<?php echo encodeQuery($userGroup->name);?>
									</label>
								</div>
								<?php
							}
							?>

						</div>
					</div>
					<?php
				}
				?>

				<div class="form-group" >
					<label class="col-sm-2 control-label"><?=$this->lang->line('general_users');?></label>
					<div class="col-sm-10 form-group" cn-search-users url="/<?php echo FOLDERADD;?>/tips/searchusers" placeholder="<?php echo $this->lang->line('general_find');?>"></div>
				</div>
				<hr>
				<div class="form-group text-center">
					<button type="submit" class="btn btn-green" ng-hide="creating"><?=$this->lang->line('general_create');?></button>
					<button type="button" disabled="disabled" class="btn btn-green nh-hide" ng-show="creating"><i class="fa fa-refresh fa-spin fa-lg"></i></button>
				</div>
			</form>
			<div class="modal fade" id="no-complete" tabindex="-1" role="dialog">
				<div class="modal-dialog modal-sm">
					<div class="modal-content">
						<div class="modal-body">
							<p><?php echo $this->lang->line('general_completeall');?> <strong class="text-danger">*</strong></p>
							<div class="text-right">
								<span class="btn btn-white" data-dismiss="modal" aria-label="Close"><?php echo $this->lang->line('general_accept'); ?></span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="/<?php echo APPFOLDERADD; ?>/libraries/tinymce/tinymce.min.js"></script>
