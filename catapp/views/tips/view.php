<div class="page-title">
	<h5 class="title"><?php echo $this->lang->line('general_tips'); ?></h5>
	<?php if($this->Identity->Validate('tips/create')) 
	{ 
		?>
		<a href="#/tips/create" class="btn  btn-white "><i class="fa fa-plus text-muted"></i> <?=$this->lang->line('general_create');?></a>
		<?php 
	}
	?>
	<a href="#/tools" class="btn btn-white pull-right hidden-xs"><i class="fa fa-chevron-left"></i> <?=$this->lang->line('general_goback');?></a>

</div>

<div class="col-xs-12 flat-style">
	<?php
	if ($this->Identity->Validate('tips/delete')) {
		?>
		<div class="modal animated shake" id="confirm-delete" tabindex="-1" role="dialog">
			<div class="modal-dialog modal-sm ">
				<div class="modal-content">
					<div class="modal-body">
						<p><?php echo $this->lang->line('tips_deletemessage')?></p>
						<div class="text-right">
							<span class="btn btn-white" data-dismiss="modal" aria-label="Close"><?php echo $this->lang->line('general_no'); ?></span>
							<button class="btn btn-lightgreen" ng-hide="deleting" ng-click="deleteTip()"><?php echo $this->lang->line('general_yes'); ?></button>
							<button class="btn btn-lightgreen disabled" ng-show="deleting"><i class="fa fa-refresh fa-spin fa-lg"></i></button>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php
	}
	?>

	<div class="modal animated fade" id="tip-details" tabindex="-1" role="dialog">
		<div class="modal-dialog  ">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title"><?php echo $this->lang->line('tips_details');?></h4>
				</div>

				<div class="modal-body">
					<dl class="dl-horizontal">
						<dt><?php echo $this->lang->line('tips_tip'); ?></dt>
						<dd ng-bind-html="tipDetails.description | HtmlSanitize"></dd>
						<dt><?php echo $this->lang->line('tips_createdby'); ?></dt>
						<dd ng-bind="tipDetails.name +' '+tipDetails.lastName"></dd>
						<dt><?php echo $this->lang->line('tips_date'); ?></dt>
						<dd ng-bind="tipDetails.date"></dd>
						<dt><?php echo $this->lang->line('tips_feedback'); ?></dt>
						<dd ng-if="tipDetails.feedback == 'TRUE'"><?php echo $this->lang->line('general_yes');?></dd>
						<dd ng-if="tipDetails.feedback != 'TRUE'"><?php echo $this->lang->line('general_no');?></dd>
					</dl>
					<h5 class="fw-500"><?php echo $this->lang->line('general_to');?></h5>

					<dl class="dl-horizontal ng-hide" ng-hide="loadingTo">
						<dt ng-if="tipDetails.to.sections.length > 0"><?php echo $this->lang->line('general_sections'); ?></dt>
						<dd ng-repeat="section in tipDetails.to.sections" ng-bind="section.name"></dd>
						
						<dt ng-if="tipDetails.to.sites.length > 0"><?php echo $this->lang->line('general_sites'); ?></dt>
						<dd ng-repeat="site in tipDetails.to.sites" ng-bind="site.name"></dd>
						
						<dt ng-if="tipDetails.to.roles.length > 0"><?php echo $this->lang->line('general_roles'); ?></dt>
						<dd ng-repeat="role in tipDetails.to.roles" ng-bind="role.name"></dd>
						
						<dt ng-if="tipDetails.to.campaigns.length > 0"><?php echo $this->lang->line('general_campaigns'); ?></dt>
						<dd ng-repeat="campaign in tipDetails.to.campaigns" ng-bind="campaign.name"></dd>
						
						<dt ng-if="tipDetails.to.users.length > 0"><?php echo $this->lang->line('general_users'); ?></dt>
						<dd ng-repeat="user in tipDetails.to.users" ng-bind="user.name"></dd>
					</dl>

					<h5 class="fw-500" ng-if="tipDetails.feedback == 'TRUE'"><?php echo $this->lang->line('tips_feedback_w');?></h5>

					<dl class="dl-horizontal ng-hide" ng-hide="loadingTo" ng-if="tipDetails.feedback == 'TRUE'">
						<dt><?php echo $this->lang->line('general_good'); ?></dt>
						<dd>
							<span ng-bind="tipDetails.feedbacks.good.length"></span> 
							<?php echo $this->lang->line('general_users')?> 
							<span ng-bind="' ('+(tipDetails.feedbacks.goodPercent | number:2)+'%)'" ></span> 
						</dd>
						<dd ng-repeat="feedback in tipDetails.feedbacks.good" ng-bind="feedback.name+' '+feedback.lastName"></dd>
					</dl>
					<dl class="dl-horizontal ng-hide" ng-hide="loadingTo" ng-if="tipDetails.feedback == 'TRUE'">
						<dt><?php echo $this->lang->line('general_bad'); ?></dt>
						<dd>
							<span ng-bind="tipDetails.feedbacks.bad.length"></span> 
							<?php echo $this->lang->line('general_users')?>
							<span ng-bind="' ('+(tipDetails.feedbacks.badPercent | number:2)+'%)'" ></span> 
						</dd>
						<dd ng-repeat="feedback in tipDetails.feedbacks.bad" ng-bind="feedback.name+' '+feedback.lastName"></dd>
					</dl>

					<p class="text-center" ng-show="loadingTo"><i class="fa fa-refresh fa-spin fa-3x"></i></p>
				</div>
			</div>
		</div>
	</div>


	<div class="panel panel-default">
		<div class="panel-body text-center" ng-show="loading">
			<i class='fa fa-refresh fa-spin fa-5x fa-fw dark' ></i>
		</div>
		<div class="ng-hide" ng-hide="loading">
			<div class="panel-body"  ng-show="tips.length == 0">
				<div class="alert bg-pink" style="margin-bottom:0" role="alert"><?php echo $this->lang->line('tips_empty'); ?></div>
			</div>

			<table class="table table-hover" role="dataTable" ng-if="tips.length > 0" role="dataTable">
				<thead>
					<tr class="active  form-inline">
						<th><?=$this->lang->line('tips_tip');?></th>
						<th><?=$this->lang->line('tips_createdby');?></th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					<tr ng-repeat="tip in tips | orderBy : '-timestamp'" >
						<td ng-bind-html="tip.description | HtmlSanitize"></td>
						<td ng-bind="tip.name +' '+tip.lastName"></td>
						<td class="text-center">
							<span class="btn btn-green btn-xs" ng-click="showDetails(tip)" ><i class="fa fa-search"></i></span>&nbsp; 
							<?php 
							if($this->Identity->Validate('tips/edit')) { 
								?>
								<a href="#/tips/edit/{{tip.tipId}}" class="btn btn-yelow btn-xs"><i class="fa fa-pencil"></i></a>&nbsp; 
								<?php 
							} 
							if($this->Identity->Validate('tips/delete')) { 
								?>
								<span class="btn btn-red btn-xs" ng-click="showDelete(tip)"><i class="fa fa-trash"></i></span>
								<?php 
							}
							?>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>