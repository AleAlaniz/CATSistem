<ol class="breadcrumb">
	<li><a href="/<?=FOLDERADD?>/administration"><?=$this->lang->line('general_administration');?></a></li>
	<li><a href="/<?=FOLDERADD?>/sections"><?=$this->lang->line('general_sections');?></a></li>
	<li><a href="/<?=FOLDERADD?>/sections/details/<?=$sectionId;?>"><?=encodeQuery($name);?></a></li>
	<li class="active"><?=$this->lang->line('administration_edit');?></li>
</ol>
<div class="col-xs-12" style="margin-bottom:15px">
	<?=$navBar?>
</div>
<div class="col-xs-12">
	<div class="panel panel-default">
		<div class="panel-heading">
			<strong><?=$this->lang->line('administration_sections_edit');?></strong>
		</div>
		<div class="panel-body">
			<form class="form-horizontal" method="POST" novalidate >
				<input type="hidden" name="sectionId" value="<?=$sectionId?>">
				<div class="form-group">
					<label for="name" class="col-sm-2 control-label"><?=$this->lang->line('administration_sections_name');?><span class="text-danger"><strong> *</strong></span></label>
					<div class="col-sm-5">
						<input type="text" class="form-control input-sm" id="name" name="name" value="<?php echo set_value('name', $name);?>" placeholder="<?=$this->lang->line('administration_sections_name');?>" required>
						<?php echo form_error('name'); ?>
					</div>
				</div>
				<div class="form-group">
					<label for="icon" class="col-sm-2 control-label"><?=$this->lang->line('administration_sections_icon');?></label>
					<div class="col-sm-10">
						<div class="input-group">
							<div class="input-group-addon"><i class="fa <?php echo set_value('icon', $icon);?>"></i></div>
							<input type="text" class="form-control input-sm" id="icon" name="icon" value="<?php echo set_value('icon', $icon);?>" placeholder="<?=$this->lang->line('administration_sections_icon');?>" required>
							
						</div>
						<a href="http://fortawesome.github.io/Font-Awesome/icons/" target="_blank">Ver iconos disponibles</a>
						<?php echo form_error('icon'); ?>
					</div>
				</div>
				<hr>
				<div class="form-group text-center">
					<button type="submit" class="btn btn-success btn-sm"><?=$this->lang->line('general_save');?></button>
					<a href="/<?=FOLDERADD?>/sections" class="btn btn-danger btn-sm"><?=$this->lang->line('general_cancel');?></a>
				</div>
			</form>
		</div>
	</div>
</div>
<script type="text/javascript">
$('#nav_sections').addClass('active');
$('#sectionNavEdit').addClass('active');
$( "#icon" ).on('change keyup paste click',function() {
 	$(this).prev().find('.fa').attr('class','fa '+ $( "#icon" ).val() )
});
</script>