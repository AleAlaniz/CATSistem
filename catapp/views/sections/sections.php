	<ol class="breadcrumb">
		<li><a href="/<?=FOLDERADD?>/administration"><?=$this->lang->line('general_administration');?></a></li>
		<li class="active"><?=$this->lang->line('general_sections');?></li>
	</ol>

	<?php if($this->Identity->Validate('sections/create')) { ?>
	<p>
		<a href="/<?=FOLDERADD?>/sections/create" class="btn btn-sm btn-success "><i class="fa fa-plus"></i><strong> <?=$this->lang->line('administration_create');?></strong></a>
	</p>
	<?php }
	 if (isset($_SESSION['sectionMessage'])): ?>
	<div class="alert alert-success alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<strong><i class="fa fa-check"></i></strong> 
		<?php if ($_SESSION['sectionMessage'] == 'create'){
			echo $this->lang->line('administration_sections_successmessage');
		}
		elseif ($_SESSION['sectionMessage'] == 'delete'){
			echo $this->lang->line('administration_sections_deletemessage');
		}
		?>
	</div>
	<?php endif; ?>
<div class="panel panel-default">
	<div class="panel-heading">
		<strong><?=$this->lang->line('general_sections');?></strong>
		<span class="badge pull-right bg-success"><?=count($model)?></span>
	</div>
	<table class="table table-hover">
		<thead>
			<tr class="active">
				<th><?=$this->lang->line('administration_sections_name');?></th>
				<th><?=$this->lang->line('administration_sections_icon');?></th>
				<th></th>
			</tr>
		</thead>
		<tbody>
		<?php if (count($model) > 0) {?>
		
			<?php foreach ($model as $section): ?>
			<tr class="optionsUser">
				<td><?=encodeQuery($section->name)?></td>
				<td><i class="fa <?=$section->icon?>"></i></td>
				<td>
					<?php if($this->Identity->Validate('sections/details')) { ?>
					<a href="/<?=FOLDERADD?>/sections/details/<?=$section->sectionId?>"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></a>&nbsp; 
					<?php } ?>
					<?php if($this->Identity->Validate('sections/edit')) { ?>
					<a href="/<?=FOLDERADD?>/sections/edit/<?=$section->sectionId?>"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>&nbsp; 
					<?php } ?>
					<?php if($this->Identity->Validate('sections/delete')) { ?>
					<a href="/<?=FOLDERADD?>/sections/delete/<?=$section->sectionId?>"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
					<?php } ?>
				</td>
			</tr>

		<?php endforeach; ?>
		
		<?php } else { ?>
			<tr class="text-center">
				<td colspan="3"><i class="fa fa-list-alt"></i> <?=$this->lang->line('administration_sections_empty');?></td>
			</tr>

		<?php
		}?>
		</tbody>
</table>
</div>
<script type="text/javascript">
$('#nav_sections').addClass('active');
$('.optionsUser').hover(function(){
	$(this).find('.glyphicon').css('visibility','visible');

},function(){
	$(this).find('.glyphicon').css('visibility','hidden');

});
</script>