<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>404 Page Not Found</title>
	<style type="text/css">

	body {
		font: 13px/20px "Open Sans",sans-serif;
		margin: 0;
	}

	h1 {
		margin-top: 150px;
		color: #FAFAFA;
		font-size: 180px;
		font-weight: normal;
	}

	p {
		font-size: 50px;
		color: rgb(255, 255, 255);
		font-weight: 500;
		margin-bottom: 50px;
	}

	#container {
		margin: 0px;
		text-align: center; 
		background-color: #D32F2F;
		overflow: auto;
	}

	span{
		cursor: pointer;
	}

	a{
		width: 24px;
		height: 24px;
	}

	svg{
		margin-top: 40px;
		margin-bottom: 20px;
		padding: 15px;
		border-radius: 50px;
		box-shadow: 0px 2px 5px 0px rgba(0, 0, 0, 0.26);
		transition: background-color 0.2s linear;
	}

	a:focus svg{
		background-color: #EC4A4A;
	}
	</style>
</head>
<body>
	<div id="container">
		<h1>404</h1>
		<p>Page Not Found</p>
		<a href="/CATSistem">
			<svg style="height:24px;width:24px" preserveAspectRatio="xMidYMid meet" fill="#FFF" xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" viewBox="0 0 24 24">
				<path d="M10 20v-6h4v6h5v-8h3L12 3 2 12h3v8z"></path>
			</svg>
		</a>
	</div>
</body>
</html>