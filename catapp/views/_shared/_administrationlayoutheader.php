<?php
$sql 	= "SELECT name, lastName FROM userPersonalData WHERE userId = ?";
$user 	= array($this->session->UserId);
$query 	= $this->db->query($sql,$user)->row();

$sql = $this->Layout->GetUser();
$background 	= ($sql->background == NULL) ? '' : 'style="filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src=\'/'.APPFOLDERADD.'/_users_backgrounds/'.$sql->background.'\',sizingMethod=\'scale\');background-image:url(/'.APPFOLDERADD.'/_users_backgrounds/'.$sql->background.')"';
?>
<!DOCTYPE HTML>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="shortcut icon" href="/<?php echo APPFOLDERADD;?>/libraries/images/favicon.png" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" type="text/css" href="/<?php echo APPFOLDERADD;?>/libraries/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="/<?php echo APPFOLDERADD;?>/libraries/css/style.min.css">
	<link rel="stylesheet" type="text/css" href="/<?php echo APPFOLDERADD;?>/libraries/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="/<?php echo APPFOLDERADD;?>/libraries/css/animate.min.css">
	<title>CAT - Technologies</title>
</head>
<body>
	<script src="/<?php echo APPFOLDERADD;?>/libraries/script/jquery-1.11.3.min.js"></script>
	<script src="/<?php echo APPFOLDERADD;?>/libraries/script/bootstrap.min.js"></script>
	<nav class="navbar navbar-default contain-fixed navbar-fixed-top" style="z-index:999">
		<div class="container-fluid">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="/<?=FOLDERADD;?>">
					<img alt="cat" src="/<?=APPFOLDERADD;?>/libraries/images/logo-mini.png">
				</a>
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav navbar-right">
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
							<div class="profileImage profileImage25" style="filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='/<?php echo FOLDERADD; ?>/users/profilephoto?userId=<?php echo $this->session->UserId;?>&wah=200',sizingMethod='scale');background-image:url(/<?=FOLDERADD?>/users/profilephoto?userId=<?=$this->session->UserId?>&wah=200)"></div>
							<?=encodeQuery($query->name)?> <?=encodeQuery($query->lastName)?>  <span class="caret"></span></span>
						</a>
						<ul class="dropdown-menu">
							<?php
							if ($this->Identity->Validate('users/config')){
								?>
								<li>
									<a href="/<?=FOLDERADD?>/users/config">
										<i class="fa fa-user"></i> &nbsp;&nbsp;<?=$this->lang->line('general_miprofile');?>
									</a>
								</li>
								<?php
							}
							?>
							<li role="separator" class="divider"></li>
							<li><a href="/<?=FOLDERADD?>/home/logout"><i class="fa fa-sign-out"></i> &nbsp;&nbsp;Salir</a></li>
						</ul>
					</li>

					<?php
					if ($this->Identity->Validate('_layoutheader/diary')){
						?>
						<li><a href="#"><i class="fa fa-calendar"></i></a></li>
						<?php
					}
					?>
					<?php
					if ($this->Identity->Validate('_layoutheader/dashboard')){
						?>
						<li><a href="#"><span class="glyphicon glyphicon-gift" aria-hidden="true"></span></a></li>
						<?php
					}
					?>

					<?php if ($this->Identity->Validate('administration/index')){ ?>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
							<i class="fa fa-cog"></i> <span class="visible-xs-inline"><?=$this->lang->line('general_administration');?></span>
						</a>
						<ul class="dropdown-menu">	
							<li><a href="/<?=FOLDERADD?>/administration"><i class="fa fa-lock"></i> &nbsp;&nbsp;&nbsp;<?=$this->lang->line('general_administration');?></a></li>
							<?php if ($this->Identity->Validate('theme/index')){ ?>
							<li><a href="/<?=FOLDERADD?>/themes"><i class="fa fa-image"></i> &nbsp;&nbsp;<?=$this->lang->line('general_themes');?></a></li>
							<?php } ?>
							<?php if ($this->Identity->Validate('users/index')){ ?>
							<li><a href="/<?=FOLDERADD?>/users"><i class="fa fa-users"></i> &nbsp;&nbsp;<?=$this->lang->line('general_users');?></a></li>
							<?php } ?>
							<?php if ($this->Identity->Validate('usergroups/index')){ ?>
							<li><a href="/<?=FOLDERADD?>/usergroups"><i class="fa fa-slideshare"></i> &nbsp;&nbsp;<?=$this->lang->line('administration_usergroups');?></a></li>
							<?php } ?>
							<?php if ($this->Identity->Validate('roles/index')){ ?>
							<li><a href="/<?=FOLDERADD?>/roles"><i class="fa fa-user-plus"></i> &nbsp;&nbsp;<?=$this->lang->line('general_roles');?></a></li>
							<?php } ?>
							<?php if ($this->Identity->Validate('sections/index')){ ?>
							<li><a href="/<?=FOLDERADD?>/sections"><i class="fa fa-list-alt"></i> &nbsp;&nbsp;<?=$this->lang->line('general_sections');?></a></li>
							<?php } ?>
							<?php if ($this->Identity->Validate('sites/index')){ ?>
							<li><a href="/<?=FOLDERADD?>/sites"><i class="fa fa-building"></i> &nbsp;&nbsp;<?=$this->lang->line('general_sites');?></a></li>
							<?php } ?>
							<?php if ($this->Identity->Validate('subsections/index')){ ?>
							<li><a href="/<?=FOLDERADD?>/subsections"><i class="fa fa-sitemap"></i> &nbsp;&nbsp;<?=$this->lang->line('general_subsections');?></a></li>
							<?php } ?>
							<?php if ($this->Identity->Validate('items/index')){ ?>
							<li><a href="/<?=FOLDERADD?>/items"><i class="fa fa-circle"></i> &nbsp;&nbsp;<?=$this->lang->line('general_items');?></a></li>
							<?php } ?>
						</ul>
					</li>
					<?php } ?>
				</ul>
			</div><!-- /.navbar-collapse -->
		</div><!-- /.container-fluid -->
	</nav>
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-2 col-sm-2 fixed">
				<ul class="nav nav-pills nav-stacked">
					<?php if ($this->Identity->Validate('home/index')): ?>
						<li role="presentation">
							<a href="/<?=FOLDERADD?>">
								<i class="fa fa-home"></i><span class="hidden-xs"><?=$this->lang->line('administration_comeback');?></span>
							</a>
						</li>
					<?php endif; ?>

					<?php if ($this->Identity->Validate('administration/index')): ?>
						<li role="presentation" id="nav_administration">
							<a href="/<?=FOLDERADD?>/administration">
								<i class="fa fa-cogs"></i> <span class="hidden-xs"><?=$this->lang->line('general_administration');?></span>
							</a>
						</li>
					<?php endif; ?>

					<?php if ($this->Identity->Validate('theme/index')): ?>
						<li role="presentation" id="nav_theme">
							<a href="/<?=FOLDERADD?>/themes">
								<i class="fa fa-image"></i> <span class="hidden-xs"><?=$this->lang->line('general_themes');?></span>
							</a>
						</li>
					<?php endif; ?>
					<?php if ($this->Identity->Validate('users/index')): ?>
						<li role="presentation" id="nav_users">
							<a href="/<?=FOLDERADD?>/users">
								<i class="fa fa-users"></i> <span class="hidden-xs"><?=$this->lang->line('general_users');?></span>
							</a>
						</li>
					<?php endif; ?>
					<?php if ($this->Identity->Validate('usergroups/index')): ?>
						<li role="presentation" id="nav_usergroups">
							<a href="/<?=FOLDERADD?>/usergroups">
								<i class="fa fa-slideshare"></i> <span class="hidden-xs"><?=$this->lang->line('administration_usergroups');?></span>
							</a>
						</li>
					<?php endif; ?>
					<?php if ($this->Identity->Validate('roles/index')): ?>
						<li role="presentation" id="nav_roles">
							<a href="/<?=FOLDERADD?>/roles">
								<i class="fa fa-user-plus"></i> <span class="hidden-xs"><?=$this->lang->line('general_roles');?></span>
							</a>
						</li>
					<?php endif; ?>
					<?php if ($this->Identity->Validate('sections/index')): ?>
						<li role="presentation" id="nav_sections">
							<a href="/<?=FOLDERADD?>/sections">
								<i class="fa fa-list-alt"></i> <span class="hidden-xs"><?=$this->lang->line('general_sections');?></span>
							</a>
						</li>
					<?php endif; ?>
					<?php if ($this->Identity->Validate('sites/index')): ?>
						<li role="presentation" id="nav_sites">
							<a href="/<?=FOLDERADD?>/sites">
								<i class="fa fa-building"></i> <span class="hidden-xs"><?=$this->lang->line('general_sites');?></span>
							</a>
						</li>
					<?php endif; ?>
					<?php if ($this->Identity->Validate('subsections/index')): ?>
						<li role="presentation" id="nav_subsections">
							<a href="/<?=FOLDERADD?>/subsections">
								<i class="fa fa-sitemap"></i> <span class="hidden-xs"><?=$this->lang->line('general_subsections');?></span>
							</a>
						</li>
					<?php endif; ?>
					<?php if ($this->Identity->Validate('items/index')): ?>
						<li role="presentation" id="nav_items">
							<a href="/<?=FOLDERADD?>/items">
								<i class="fa fa-circle"></i> <span class="hidden-xs"><?=$this->lang->line('general_items');?></span>
							</a>
						</li>
					<?php endif; ?>
					<?php if ($this->Identity->Validate('clients/index')): ?>
						<li role="presentation" id="nav_clients">
							<a href="/<?=FOLDERADD?>/clients">
								<i class="fa fa-arrow-up"></i> <span class="hidden-xs"><?=$this->lang->line('general_clients');?></span>
							</a>
						</li>
					<?php endif; ?>
					<?php if ($this->Identity->Validate('campaigns/index')): ?>
						<li role="presentation" id="nav_campaigns">
							<a href="/<?=FOLDERADD?>/campaigns">
								<i class="fa fa-bullhorn"></i> <span class="hidden-xs"><?=$this->lang->line('general_campaigns');?></span>
							</a>
						</li>
					<?php endif; ?>
					<?php if ($this->Identity->Validate('subcampaigns/index')): ?>
						<li role="presentation" id="nav_subCampaigns">
							<a href="/<?=FOLDERADD?>/subcampaigns">
								<i class="fa fa-bullhorn"></i> <span class="hidden-xs"><?=$this->lang->line('subcampaigns_index');?></span>
							</a>
						</li>
					<?php endif; ?>
					<?php if ($this->Identity->Validate('users/security_questions/adm_index')): ?>
						<li role="presentation" id="nav_security_questions">
							<a href="/<?=FOLDERADD?>/securityquestions/config">
								<i class="fa fa-shield"></i> <span class="hidden-xs"><?=$this->lang->line('question_security_questions');?></span>
							</a>
						</li>
					<?php endif; ?>
				</ul>
			</div>
			<div class="col-md-offset-2 col-sm-offset-2 col-xs-offset-1 col-xs-11 col-md-10 col-sm-10 scrollit" <?php echo $background; ?>>
