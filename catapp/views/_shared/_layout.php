<!DOCTYPE HTML>
<html ng-app="catnet">
<head>
	<link rel="icon" type="image/png" href="/<?php echo APPFOLDERADD;?>/libraries/images/favicon.png" />
	<script src="/<?=APPFOLDERADD?>/libraries/script/excanvas.min.js"></script>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" type="text/css" href="/<?php echo APPFOLDERADD;?>/libraries/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="/<?php echo APPFOLDERADD;?>/libraries/css/style.css">
	<link rel="stylesheet" type="text/css" href="/<?php echo APPFOLDERADD;?>/libraries/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="/<?php echo APPFOLDERADD;?>/libraries/css/jquery-ui.min.css">
	<link rel="stylesheet" type="text/css" href="/<?php echo APPFOLDERADD;?>/libraries/css/animate.min.css">
	<link rel="stylesheet" type="text/css" href="/<?php echo APPFOLDERADD;?>/libraries/css/bootstrap-datepicker.css">
	<link rel="stylesheet" type="text/css" href="/<?php echo APPFOLDERADD;?>/libraries/emoji/css/jquery.emoji.css">
	<title>CAT - Technologies</title>
</head>
<body>
	<?php
	$query 			= $this->Layout->GetUser();
	$sectionsQuery 	= $this->Layout->GetSections();
	$background 	= ($query->background == NULL) ? '' : 'style="filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src=\'/'.APPFOLDERADD.'/_users_backgrounds/'.$query->background.'\',sizingMethod=\'scale\');background-image:url(/'.APPFOLDERADD.'/_users_backgrounds/'.$query->background.')"';
	?>
	<script>
		var APPFOLDERADD 	= 'CATSistem/catapp',
		FOLDERADD 			= 'CATSistem', 
		POLLPATH	 		= '<?php echo POLLPATH;?>',
		permissions 		= {},
		userData 			= {};

		userData.userId 	= "<?php echo $this->session->UserId; 		?>";
		userData.sectionId 	= "<?php echo $this->session->sectionId; 	?>";
		userData.siteId 	= "<?php echo $this->session->siteId; 		?>";
		userData.roleId 	= "<?php echo $this->session->Role; 		?>";
		userData.campaignId	= "<?php echo $query->campaignId; 			?>";

		permissions['suggestbox/view'] 			= "<?php if($this->Identity->Validate('suggestbox/view'))			{ echo 'true'; }else{ echo 'false'; } ?>";
		permissions['newsletter/index'] 		= "<?php if($this->Identity->Validate('newsletter/index'))			{ echo 'true'; }else{ echo 'false'; } ?>";
		permissions['newsletter/create'] 		= "<?php if($this->Identity->Validate('newsletter/create'))			{ echo 'true'; }else{ echo 'false'; } ?>";
		permissions['press/index'] 				= "<?php if($this->Identity->Validate('press/index'))				{ echo 'true'; }else{ echo 'false'; } ?>";
		permissions['chat/index'] 				= "<?php if($this->Identity->Validate('chat/index'))				{ echo 'true'; }else{ echo 'false'; } ?>";
		permissions['chat/multi'] 				= "<?php if($this->Identity->Validate('chat/multi'))				{ echo 'true'; }else{ echo 'false'; } ?>";
		permissions['analitycs/index'] 			= "<?php if($this->Identity->Validate('analitycs/index'))			{ echo 'true'; }else{ echo 'false'; } ?>";
		permissions['surveys/manage'] 			= "<?php if($this->Identity->Validate('surveys/manage'))			{ echo 'true'; }else{ echo 'false'; } ?>";
		permissions['teaming/pendingforms']		= "<?php if($this->Identity->Validate('teaming/pendingforms'))		{ echo 'true'; }else{ echo 'false'; } ?>";
		permissions['sections/index']			= "<?php if($this->Identity->Validate('sections/index'))			{ echo 'true'; }else{ echo 'false'; } ?>";
	</script>

	<!--Dependencies-->
	<script src="/<?php echo APPFOLDERADD;?>/libraries/script/jquery-1.11.3.min.js"></script>
	<script src="/<?php echo APPFOLDERADD;?>/libraries/script/bootstrap.min.js"></script>
	<script src="/<?php echo APPFOLDERADD;?>/libraries/script/jquery-ui.min.js"></script>
	<script src="/<?php echo APPFOLDERADD;?>/libraries/script/angular.min.js"></script>
	<script src="/<?php echo APPFOLDERADD;?>/libraries/script/angular-route.min.js"></script>
	<script src="/<?php echo APPFOLDERADD;?>/libraries/script/angular-sanitize.min.js"></script>
	<script src="/<?php echo APPFOLDERADD;?>/libraries/script/socket.io/node_modules/socket.io-client/socket.io.js"></script>
	<script src="/<?php echo APPFOLDERADD; ?>/libraries/script/Chart.min.js"></script>
	<script src="/<?php echo APPFOLDERADD; ?>/libraries/script/jquery.dataTables.min.js"></script>
	<script src="/<?php echo APPFOLDERADD; ?>/libraries/script/jquery.form.js"></script>
	<script src="/<?php echo APPFOLDERADD;?>/libraries/emoji/js/jquery.emoji.js"></script>
	<script src="/<?php echo APPFOLDERADD;?>/libraries/emoji/js/jquery.mCustomScrollbar.min.js"></script>
	<script src="/<?php echo APPFOLDERADD;?>/libraries/emoji/js/jquery.mousewheel-3.0.6.min.js"></script>
	<!--End Dependencies-->

	<!--App-->
	<script type="text/javascript" src="/<?php echo APPFOLDERADD; ?>/libraries/script/app.min.js"></script>
	<!--End App-->

	<!--Services-->
	<script type="text/javascript" src="/<?php echo APPFOLDERADD; ?>/libraries/script/services/service-nav.js"></script>
	<script type="text/javascript" src="/<?php echo APPFOLDERADD; ?>/libraries/script/services/service-tinymce.min.js"></script>
	<script type="text/javascript" src="/<?php echo APPFOLDERADD; ?>/libraries/script/services/service-realtime.js"></script>
	<!--End Services-->

	<!--Filters-->
	<script src="/<?php echo APPFOLDERADD;?>/libraries/script/filters/filter-htmlsanitize.min.js"></script>
	<script src="/<?php echo APPFOLDERADD;?>/libraries/script/filters/filter-orderobjectby.min.js"></script>
	<!--End Filters-->

	<!--Directives-->
	<script src="/<?php echo APPFOLDERADD;?>/libraries/script/directives/directive-cnSquareImage.min.js"></script>
	<script src="/<?php echo APPFOLDERADD;?>/libraries/script/directives/directive-cnSearchUsers.min.js"></script>
	<script src="/<?php echo APPFOLDERADD;?>/libraries/script/directives/group/group.js"></script>
	<script src="/<?php echo APPFOLDERADD;?>/libraries/script/directives/civilStates/civilStates.js"></script>
	<script src="/<?php echo APPFOLDERADD;?>/libraries/script/directives/searchUsers/searchUsers.js"></script>
	<script src="/<?php echo APPFOLDERADD;?>/libraries/script/directives/filters/filters.js"></script>
	<script src="/<?php echo APPFOLDERADD;?>/libraries/script/directives/datePickerSearch/datePickerSearch.js"></script>

    <script src="/<?php echo APPFOLDERADD;?>/libraries/script/directives/visibilityCheck/roleCheck.js"></script>
    <!--End Directives-->

	<!--Controllers-->
	<script src="/<?php echo APPFOLDERADD;?>/libraries/script/controllers/nav.js"></script>
	<script src="/<?php echo APPFOLDERADD;?>/libraries/script/controllers/section.js"></script>
	<script src="/<?php echo APPFOLDERADD;?>/libraries/script/controllers/newsletter.js"></script>
	<script src="/<?php echo APPFOLDERADD;?>/libraries/script/controllers/panel.min.js"></script>
	<script src="/<?php echo APPFOLDERADD;?>/libraries/script/controllers/suggestbox.min.js"></script>
	<script src="/<?php echo APPFOLDERADD;?>/libraries/script/controllers/press.min.js"></script>
	<script src="/<?php echo APPFOLDERADD;?>/libraries/script/controllers/myprizes.min.js"></script>
	<script src="/<?php echo APPFOLDERADD;?>/libraries/script/controllers/ranking.min.js"></script>
	<script src="/<?php echo APPFOLDERADD;?>/libraries/script/controllers/tools.min.js"></script>
	<script src="/<?php echo APPFOLDERADD;?>/libraries/script/controllers/wecat.min.js"></script>
	<script src="/<?php echo APPFOLDERADD;?>/libraries/script/controllers/chatu.js"></script>
	<script src="/<?php echo APPFOLDERADD;?>/libraries/script/controllers/chatm.js"></script>
	<script src="/<?php echo APPFOLDERADD;?>/libraries/script/controllers/surveys.min.js"></script>
    //1752021
	<script src="/<?php echo APPFOLDERADD;?>/libraries/script/controllers/corporatedocs.js"></script>
	<script src="/<?php echo APPFOLDERADD;?>/libraries/script/controllers/notifications.min.js"></script>
	<script src="/<?php echo APPFOLDERADD;?>/libraries/script/controllers/teaming.js"></script>
	<script src="/<?php echo APPFOLDERADD;?>/libraries/script/controllers/referred.min.js"></script>
	<script src="/<?php echo APPFOLDERADD;?>/libraries/script/controllers/activities.js"></script>
	<!--End Controllers-->

	<nav class="navbar navbar-default contain-fixed navbar-fixed-top" style="z-index:1000" id="siteNavBar">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
					<span class="sr-only"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#/">
					<img alt="cat" src="/<?=APPFOLDERADD;?>/libraries/images/logo-mini.png">
				</a>
				<span class="navbar-brand" id="layout_loading" style="display:none">
					<img alt="cargando" src="/<?=APPFOLDERADD;?>/libraries/images/loading.gif" style="width:20px;height:20px; margin-top:6px">
				</span>
			</div>
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav navbar-right">
					<li class="dropdown">
						<a href="" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
							<div class="profileImage profileImage25" style="filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='/<?php echo FOLDERADD; ?>/users/profilephoto?userId=<?php echo $this->session->UserId;?>&wah=200',sizingMethod='scale');background-image:url(/<?=FOLDERADD?>/users/profilephoto?userId=<?=$this->session->UserId?>&wah=200)"></div>
							<?=encodeQuery($query->name)?> <?=encodeQuery($query->lastName)?> 
							<span class="caret"></span></span>
						</a>
						<ul class="dropdown-menu">
							<?php
							if ($this->Identity->Validate('users/config')){
								?>
								<li>
									<a href="/<?=FOLDERADD?>/users/config">
										<i class="fa fa-user"></i> &nbsp;&nbsp;<?=$this->lang->line('general_miprofile');?>
									</a>
								</li>
								<?php 
							}
							?>
							<li role="separator" class="divider"></li>
							<li><a href="/<?=FOLDERADD?>/home/logout"><i class="fa fa-sign-out"></i> &nbsp;&nbsp;Salir</a></li>
						</ul>
					</li>
					<?php 
					if ($this->Identity->Validate('administration/index')){ ?>
						<li class="dropdown">
							<a href="" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
								<i class="fa fa-cog"></i> <span class="visible-xs-inline"><?=$this->lang->line('general_administration');?></span>
							</a>
							<ul class="dropdown-menu">
								<li><a href="/<?=FOLDERADD?>/administration"><i class="fa fa-lock"></i> &nbsp;&nbsp;&nbsp;<?=$this->lang->line('general_administration');?></a></li>
								<?php 
								if ($this->Identity->Validate('theme/index')){ ?>
									<li><a href="/<?=FOLDERADD?>/themes"><i class="fa fa-image"></i> &nbsp;&nbsp;<?=$this->lang->line('general_themes');?></a></li>
									<?php 
								} 
								if ($this->Identity->Validate('users/index')){ ?>
									<li><a href="/<?=FOLDERADD?>/users"><i class="fa fa-users"></i> &nbsp;&nbsp;<?=$this->lang->line('general_users');?></a></li>
									<?php 
								} 
								if ($this->Identity->Validate('usergroups/index')){ ?>
									<li><a href="/<?=FOLDERADD?>/usergroups"><i class="fa fa-slideshare"></i> &nbsp;&nbsp;<?=$this->lang->line('administration_usergroups');?></a></li>
									<?php 
								} 
								if ($this->Identity->Validate('roles/index')){ ?>
									<li><a href="/<?=FOLDERADD?>/roles"><i class="fa fa-user-plus"></i> &nbsp;&nbsp;<?=$this->lang->line('general_roles');?></a></li>
									<?php 
								} 
								if ($this->Identity->Validate('sections/index')){ ?>
									<li><a href="/<?=FOLDERADD?>/sections"><i class="fa fa-list-alt"></i> &nbsp;&nbsp;<?=$this->lang->line('general_sections');?></a></li>
									<?php 
								} 
								if ($this->Identity->Validate('sites/index')){ ?>
									<li><a href="/<?=FOLDERADD?>/sites"><i class="fa fa-building"></i> &nbsp;&nbsp;<?=$this->lang->line('general_sites');?></a></li>
									<?php 
								}  
								if ($this->Identity->Validate('subsections/index')){ ?>
									<li><a href="/<?=FOLDERADD?>/subsections"><i class="fa fa-sitemap"></i> &nbsp;&nbsp;<?=$this->lang->line('general_subsections');?></a></li>
									<?php 
								} 
								if ($this->Identity->Validate('items/index')){ ?>
									<li><a href="/<?=FOLDERADD?>/items"><i class="fa fa-circle"></i> &nbsp;&nbsp;<?=$this->lang->line('general_items');?></a></li>
									<?php 
								} 
								?>
							</ul>
						</li>
						<?php 
					} 
					?>
				</ul>
			</div>
		</div>
	</nav>
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-2 col-sm-2 fixed" ng-controller="Cnav">

				<ul class="nav nav-pills nav-stacked">

					<?php 
					if ($this->Identity->Validate('home/index')){ ?>
						<li role="presentation" id="nav_home" ng-class="{active: navSelected == 'home'}"><a href="#/"><i class="fa fa-home"></i><span class="hidden-xs"><?=$this->lang->line('general_home');?></span></a></li>
						<hr> 
						<?php 
					} 
					if ($this->Identity->Validate('home/wecat')){ ?>
						<li title="<?=$this->lang->line('help_wecat');?>" role="presentation"><a class="specialhover" ng-class="{active: navSelected == 'wecat'}" href="#/home/wecat" id="nav_wecat"><i class="fa" style="text-align:left"><img class="img-wecat" src="/<?php echo APPFOLDERADD; ?>/libraries/images/we-cat.png"></i><span class="hidden-xs">&nbsp;<?=$this->lang->line('general_wecat');?></span></a></li>
						<?php 
					} 
					?>
					<?php if($this->Identity->divisionAvailable()){ ?> 
						<li title="<?=$this->lang->line('help_teaming');?>" role="presentation"> 
							<a class="specialhover" ng-class="{active: navSelected == 'teaming'}" href="#/teaming" id="nav_teaming"> 
								<i class="fa" style="text-align:left"> <img class="img-wecat" src="/<?php echo APPFOLDERADD; ?>/libraries/images/corazon_teaming.png"> 
									<?php if($this->Identity->Validate('teaming/pendingforms')){ ?> 
										<span class="messagesnumber" ng-bind="unread.teamingforms" ng-show="unread.teamingforms > 0"></span> <?php } ?> 
									</i>
									<span class="hidden-xs" style="font-family: Segoe Print ; letter-spacing: 0px;font-size: 14px;">&nbsp;<?=$this->lang->line('general_teaming');?></span> 
								</a> 
								</li> <?php } ?>
								<hr>
								<?php if($this->Identity->Validate('tools/faq')){?>
									<li title="<?=$this->lang->line('help_faq');?>" ng-class="{active: navSelected == 'faqs'}">
										<a href="#/tools/faq">
											<i class="fa fa-question-circle"></i> 
											<span class="hidden-xs"><?=$this->lang->line('general_faq');?></span>
										</a>
									</li>
								<?php }?>
								<?php 
								if ($this->Identity->Validate('newsletter/index')){ ?>
									<li title="<?=$this->lang->line('help_newsletter');?>" role="presentation" id="nav_newsletter" ng-class="{active: navSelected == 'newsletter'}">
										<a href="#/newsletter">
											<i class="fa fa-newspaper-o">
												<span class="messagesnumber" ng-bind="unread.newsletter" ng-show="unread.newsletter > 0"></span>
											</i>
											<span class="hidden-xs"><?php echo $this->lang->line('newsletter_title');?></span>
										</a>
									</li>
									<?php 
								}
								if ($this->Identity->Validate('referred/index') && $this->session->sectionId == 6){ ?>
									<li title="<?=$this->lang->line('help_referred');?>" role="presentation" id="nav_referred" ng-class="{active: navSelected == 'referred'}" >
										<a href="#/referred">
											<i class="fa fa-users"></i>
											<span class="hidden-xs"><?php echo $this->lang->line('referred_title');?></span>
										</a>
									</li>
									<?php 
								}  
								if ($this->Identity->Validate('suggestbox/index')){ ?>
									<li title="<?=$this->lang->line('help_suggestbox');?>" role="presentation" id="nav_suggestbox" ng-class="{active: navSelected == 'suggestbox'}">
										<a href="#/suggestbox">
											<i class="fa fa-archive">
												<span class="messagesnumber" ng-bind="unread.suggestbox" ng-show="unread.suggestbox > 0"></span>
											</i>
											<span class="hidden-xs"><?php echo $this->lang->line('suggestbox_title');?></span>
										</a>
									</li>
									<?php 
								}  
								if ($this->Identity->Validate('press/index')){ ?>
									<li title="<?=$this->lang->line('help_press');?>" role="presentation" id="nav_press" ng-class="{active: navSelected == 'press'}" >
										<a href="#/press">
											<i class="fa fa-camera">
												<span class="messagesnumber" ng-bind="unread.press" ng-show="unread.press > 0"></span>
											</i>
											<span class="hidden-xs"><?php echo $this->lang->line('press_title');?></span>
										</a>
									</li>
									<?php 
								}  
								if ($this->Identity->Validate('chat/index')){ ?>
									<li title="<?=$this->lang->line('help_chat');?>" role="presentation" id="nav_chat" ng-class="{active: navSelected == 'chat'}" >
										<a href="#/chat/unique">
											<i class="fa fa-comment-o">
												<span class="messagesnumber" ng-bind="unread.chats" ng-show="unread.chats > 0"></span>
											</i>
											<span class="hidden-xs"><?=$this->lang->line('chat_title');?></span>
										</a>
									</li>
									<?php 
								} 
								if ($this->Identity->Validate('sections/section')){ ?>
									<?php
									foreach ($sectionsQuery as $section){ 
										?>
										<li title="<?=$this->lang->line('help_section').' '.$section->name;?> " role="presentation" id="nav_sectionId-<?=$section->sectionId?>" ng-class="{active: navSelected == 'sectionId-<?=$section->sectionId?>'}" >
											<a href="#/sections/section/<?= $section->sectionId ?>">
												<i class="fa <?=encodeQuery($section->icon)?>">
													<?php for ($i=0; $i < count($sectionsQuery); $i++) { ?>
														<span class="messagesnumber" ng-if="unread.section[<?=$i?>].sectionId == <?=$section->sectionId?>" ng-bind="unread.section[<?=$i?>].totalunread" ng-show="unread.section[<?=$i?>].totalunread > 0"></span>
													<?php } ?>
												</i>
												<span class="hidden-xs"><?=encodeQuery($section->name)?></span>
											</a>
										</li>
										<?php 
									}
								} 
								if ($this->Identity->Validate('tools/index'))
								{ 
									?>
									<li title="<?=$this->lang->line('help_tools');?>" role="presentation" id="nav_tools" ng-class="{active: navSelected == 'tools'}"><a href="#/tools"><i class="fa fa-wrench"></i><span class="hidden-xs"><?=$this->lang->line('tools_title');?></span></a></li>
									<?php 
								} 
								if ($this->Identity->Validate('surveys/view'))
								{ 
									?>
									<li title="<?=$this->lang->line('help_surveys');?>" role="presentation" id="nav_surveys" ng-class="{active: navSelected == 'surveys'}"><a href="#/surveys"><i class="fa fa-pie-chart "></i><span class="hidden-xs"><?=$this->lang->line('survey_surveys');?></span></a></li>
									<?php 
								}
								if ($this->Identity->Validate('activities/view'))
								{ 
									?>
									<li title="<?=$this->lang->line('help_activities');?>" role="presentation" id="nav_activities" ng-class="{active: navSelected == 'activities'}"><a href="#/activities"><i class="fa fa-calendar "></i><span class="hidden-xs"><?=$this->lang->line('activity_activities');?></span></a></li>
									<?php 
								}   
								if ($this->Identity->Validate('corporatedocs/index'))
								{ 
									?>
									<li title="<?=$this->lang->line('help_corporatedocs');?>" role="presentation" id="nav_corporate_docs" ng-class="{active: navSelected == 'corporatedocs'}"><a href="#/corporatedocs"><i class="fa fa-file-text-o "></i><span class="hidden-xs"><?=$this->lang->line('general_corporate_docs');?></span></a></li>
									<?php 
								}
								if ($this->Identity->Validate('notifications/manage'))
								{ 
									?>
									<li title="<?=$this->lang->line('help_notifications');?>" role="presentation" id="nav_notifications" ng-class="{active: navSelected == 'notifications'}"><a href="#/notifications"><i class="fa fa-bell-o "></i><span class="hidden-xs"><?=$this->lang->line('general_notifications');?></span></a></li>
									<?php 
								}
								?>
							</ul>

						</div>
						<div class="col-md-offset-2 col-sm-offset-2 col-xs-offset-1 col-xs-11 col-md-10 col-sm-10 scrollit" <?php echo $background; ?>>
							<div ng-view id="viewContainer">
							</div>
							<div class="col-xs-12" id="socialContainer">
								<?php
								if ($this->Identity->Validate('_layoutfooter/socialbutons'))
								{ 
									?>
									<p class="text-center">
										Seguinos en:
									</p>
									<ul class="social">
										<li> 
											<img class="social-image" src="/<?php echo APPFOLDERADD;?>/libraries/images/social-icons/facebook.png"/>					
										</li>
										<li> 
											<img class="social-image" src="/<?php echo APPFOLDERADD;?>/libraries/images/social-icons/linkedin.png"/>					
										</li>
										<li> 
											<img class="social-image" src="/<?php echo APPFOLDERADD;?>/libraries/images/social-icons/twitter.png"/>					
										</li>
										<li> 
											<img class="social-image" src="/<?php echo APPFOLDERADD;?>/libraries/images/social-icons/youtube.png"/>					
										</li>
										<li> 
											<img class="social-image" src="/<?php echo APPFOLDERADD;?>/libraries/images/social-icons/instagram.png"/>					
										</li>
										<li> 
											<img class="social-image" src="/<?php echo APPFOLDERADD;?>/libraries/images/social-icons/web.png"/>					
										</li>
									</ul>


									<?php 
								} 
								?>
							</div>
						</div>
					</div>
				</div>
				<div id="suggestionModal" class="modal fade modal-sg" tabindex="-1" role="dialog">
					<div class="modal-dialogue">
						<div class="modal-content">
							<div class="modal-body">
								<button id="exit-modal" data-dismiss="modal">&times;</button>
								<p class="popup-welcome"><?php echo $this->lang->line('teaming_popup_accepted'); ?></p>
							</div>
							<div class="modal-button">
								<button class="btn btn-default" id="go-suggestion"><?php echo $this->lang->line('teaming_popup_go_my_institutions'); ?></button>	
							</div>
						</div>
					</div>
				</div>
				<div id="teaming-modal" class="modal fade modal-sg" tabindex="-1" role="dialog">
					<div class="modal-dialogue">
						<div class="modal-content">
							<div class="modal-body">
								<button id="exit-modal" data-dismiss="modal">&times;</button>
								<p class="popup-welcome"><?php echo $this->lang->line('teaming_popup_welcome'); ?></p>
								<div class="center-teaming-heart"><img src="/<?=APPFOLDERADD;?>/libraries/images/corazon_teaming.png" class="teaming-heart"></div>
							</div>
							<div class="modal-button">
								<button class="btn btn-default" id="go-teaming" ><?php echo $this->lang->line('teaming_popup_go_teaming'); ?></button>	
							</div>
						</div>
					</div>
				</div> 
				<script src="/<?php echo APPFOLDERADD;?>/libraries/script/html5shiv.min.js"></script>
				<script src="/<?php echo APPFOLDERADD;?>/libraries/script/respond.min.js"></script>
				<script>
					$(function () {

						$('#go-suggestion').on('click', function (event) {

							$('#suggestionModal').modal('hide');
							window.open('#/teaming/institutions');
						});

						$('#go-teaming').on('click', function (event) {

							$('#teaming-modal').modal('hide');
							window.open('#/teaming');
						});
					})
				</script>
			</body>
			</html>

