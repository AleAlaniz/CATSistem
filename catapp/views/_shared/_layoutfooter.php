
				<div class="col-xs-12" id="socialContainer">
					<?php
					if ($this->Identity->Validate('_layoutfooter/socialbutons'))
					{ 
						?>
						<p class="text-center">
							Seguinos en:
						</p>
						<ul class="social">
							<li> 
								<img class="social-image" src="/<?php echo APPFOLDERADD;?>/libraries/images/social-icons/facebook.png"/>					
							</li>
							<li> 
								<img class="social-image" src="/<?php echo APPFOLDERADD;?>/libraries/images/social-icons/linkedin.png"/>					
							</li>
							<li> 
								<img class="social-image" src="/<?php echo APPFOLDERADD;?>/libraries/images/social-icons/twitter.png"/>					
							</li>
							<li> 
								<img class="social-image" src="/<?php echo APPFOLDERADD;?>/libraries/images/social-icons/youtube.png"/>					
							</li>
							<li> 
								<img class="social-image" src="/<?php echo APPFOLDERADD;?>/libraries/images/social-icons/instagram.png"/>					
							</li>
							<li> 
								<img class="social-image" src="/<?php echo APPFOLDERADD;?>/libraries/images/social-icons/web.png"/>					
							</li>
						</ul>
						<?php 
					} 
					?>
				</div>
			</div>
		</div>
	</div>
	<script src="/<?php echo APPFOLDERADD;?>/libraries/script/html5shiv.min.js"></script>
	<script src="/<?php echo APPFOLDERADD;?>/libraries/script/respond.min.js"></script>
</body>
</html>