<ol class="breadcrumb">
	<li><a href="/<?=FOLDERADD?>/administration"><?=$this->lang->line('general_administration');?></a></li>
	<li><a href="/<?=FOLDERADD?>/campaigns"><?=$this->lang->line('general_campaigns');?></a></li>
	<li class="active"><?=$this->lang->line('administration_delete');?></li>
</ol>
<div class="col-xs-12" style="margin-bottom:15px">
	<?=$navBar?>
</div>
<div class="col-xs-12">
<div class="panel panel-default">
	<div class="panel-heading">
		<strong><?=$this->lang->line('campaign_delete');?></strong>
	</div>
	<div class="panel-body">
		<h4><?=$this->lang->line('campaign_deleteareyousure');?></h4>
		<hr>
		<dl class="dl-horizontal col-sm-6">
			<dt><?=$this->lang->line('campaign_name');?></dt>
			<dd><?=encodeQuery($name)?></dd>
		</dl>
		<dl class="dl-horizontal col-sm-6">
			<dt><?=$this->lang->line('general_section');?></dt>
			<dd><?php echo ($section == NULL ? $this->lang->line('campaign_nosection'): encodeQuery($section));?></dd>
		</dl>
		<dl class="dl-horizontal col-sm-6">
			<dt><?=$this->lang->line('general_client');?></dt>
			<dd><?php echo ($client == NULL ? $this->lang->line('campaign_noclient'): encodeQuery($client));?></dd>
		</dl>
		<form method="POST"  >
			<input type="hidden" name="campaignId" value="<?=$campaignId?>">
			<div class="form-group text-center col-xs-12">
				<button type="submit" class="btn btn-success btn-sm"><?=$this->lang->line('general_delete');?></button>
				<a href="/<?=FOLDERADD?>/campaigns" class="btn btn-danger btn-sm"><?=$this->lang->line('general_cancel');?></a>
			</div>
		</form>
	</div>
</div>
</div>
<script type="text/javascript">
$('#nav_campaigns').addClass('active');
$('#NavDelete').addClass('active');
</script>