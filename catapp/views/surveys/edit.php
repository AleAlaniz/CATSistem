<div class="page-title">
	<h3 class="title"><?=$this->lang->line('survey_edit');?></h3>
	<a href="#/surveys" class="btn btn-white pull-right"><i class="fa fa-chevron-left"></i> <?=$this->lang->line('general_goback');?></a>
</div>
<div class="col-xs-12 flat-style">

	<div class="panel panel-default">	
		
		<div class="panel-body">
			<form class="form-horizontal" method="POST" novalidate class="bg-white" name="formSurvey" ng-submit="submitSurvey()" id="form-survey" >
				<h3 style="margin-top: 0; margin-bottom: 0; padding-left: 10px"><?php echo $this->lang->line('survey_info'); ?></h3>
				<hr style="margin-top: 10px; "/>
				<div class="form-group" ng-class="{inputError:name == null}" >
					<label for="name" class="col-sm-2 control-label"><?=$this->lang->line('survey_name');?> <strong class="text-danger" role="required">*</strong></label>
					<div class="col-sm-10">
						<input type="text" class="form-control input-sm" id="name" name="name" ng-model="survey.name" placeholder="<?=$this->lang->line('survey_name');?>" required>
						<?php echo form_error('name'); ?>
					</div>
				</div>
				<div class="form-group">
					<label for="descriptionS" class="col-sm-2 control-label"><?=$this->lang->line('survey_description');?></label>
					<div class="col-sm-10">
						<textarea class="form-control input-sm" ng-model="survey.description" id="descriptionS" name="descriptionS" rows="3" placeholder="<?=$this->lang->line('survey_description');?>"></textarea>
						<?php echo form_error('descriptionS'); ?>
					</div>
				</div>
				<div class="form-group">
					<label for="closeMessage" class="col-sm-2 control-label"><?=$this->lang->line('survey_closemessage');?></label>
					<div class="col-sm-10">
						<textarea class="form-control input-sm" ng-model="survey.closeMessage" id="closeMessage" name="closeMessage" rows="3" placeholder="<?=$this->lang->line('survey_closemessage');?>"></textarea>
						<?php echo form_error('closeMessage'); ?>
					</div>
				</div>
				<div class="form-group">
					<label for="startDate" class="col-sm-2 control-label"><?=$this->lang->line('survey_start_date');?></label>
					<div class="col-xs-10">
						<input type="text" class="form-control input-sm"  data-date-format="mm/dd/yyyy" role="date" id="startDate" name="startDate"  placeholder="<?=$this->lang->line('survey_start_date');?>">

						<?php echo form_error('startDate'); ?>
					</div>
				</div>
				<div class="form-group">
					<label for="endDate" class="col-sm-2 control-label"><?=$this->lang->line('survey_end_date');?></label>
					<div class="col-xs-10">
						<input type="text" data-date-format="mm/dd/yyyy"  class="form-control input-sm" role="date" id="endDate" name="endDate" placeholder="<?=$this->lang->line('survey_end_date');?>">
						<?php echo form_error('endDate'); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label"><?=$this->lang->line('survey_required');?></label>
					<div class="col-xs-10">
						<div class="checkbox checkbox-angular">
							<label ng-class="{check: required}">
								<input type="checkbox" name="required" ng-model="required"  value="TRUE">
							</label>
						</div>
					</div>
				</div>
				<h3 style="margin-top: 0; margin-bottom: 0; padding-left: 10px"><?php echo $this->lang->line('survey_whose'); ?> <strong class="text-danger">*</strong></h3>
				<hr style="margin-top: 10px; "/>

				<div class="form-group">
					<label class="col-sm-2 control-label"><?=$this->lang->line('general_sections');?></label>
					<div class="col-sm-10">

						<div ng-repeat="section in survey.sections" class="checkbox checkbox-angular">
							<label ng-class="{check: section.active}">
								<input type="checkbox" name="sections[]" ng-model="section.active" value="{{section.sectionId}}">
								{{section.name}}
							</label>
						</div>

					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label"><?=$this->lang->line('general_sites');?></label>
					<div class="col-sm-10">

						<div ng-repeat="site in survey.sites" class="checkbox checkbox-angular">
							<label ng-class="{check: site.active}">
								<input type="checkbox" name="sites[]" ng-model="site.active"  value="{{site.siteId}}">
								{{site.name}}
							</label>
						</div>

					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label"><?=$this->lang->line('general_roles');?></label>
					<div class="col-sm-10">

						<div ng-repeat="role in survey.roles" class="checkbox checkbox-angular">
							<label ng-class="{check: role.active}">
								<input type="checkbox" name="roles[]" ng-model="role.active" value="{{role.roleId}}">
								{{role.name}}
							</label>
						</div>

					</div>
				</div>
				<?php
				if ($this->Identity->Validate('usergroups/create')) {
					?>
					<div class="form-group">
						<label class="col-sm-2 control-label"><?=$this->lang->line('general_usergroups');?></label>
						<div class="col-sm-10">

							<div ng-repeat="userGroup in survey.userGroups" class="checkbox checkbox-angular">
								<label ng-class="{check: userGroup.active}">
									<input type="checkbox" name="userGroups[]" ng-model="userGroup.active" value="{{userGroup.userGroupId}}">
									{{userGroup.name}}
								</label>
							</div>

						</div>
					</div>
					<?php
				}
				?>
				<div class="form-group">
					<input type="hidden" name="surveyId" value="{{survey.surveyId}}"/>
					<input type="hidden" ng-repeat="user in users track by $index" name="users[]" value="{{user.userId}}" />

					<label class="col-sm-2 control-label"><?=$this->lang->line('general_users');?></label>
					<div class="col-sm-10 form-group">
						<div class="select-container" ng-click="selectClick($event);">
							<span class="select-selected">
								<span class="select-selected-item" ng-repeat="user in users track by $index" ng-init="i=$index">
									<span class="selected-item-image" style="background-image: url('/<?php echo FOLDERADD; ?>/users/profilephoto?userId={{user.userId}}&amp;amp;wah=200');filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='/<?php echo FOLDERADD; ?>/users/profilephoto?userId={{user.userId}}&wah=200',sizingMethod='scale');"></span>
									<span class="selected-item-label" ng-bind="user.completeName+' <'+user.userName+'>'" ></span>
									<span class="selected-item-delete no-selectable" ng-click="removeUser(i)">&times;</span>
								</span>
							</span>
							<input type="text" autocomplete="off" class="input-sm form-control select-input" ng-trim="true" ng-model="findLike" ng-change="searchUsers()" id="findInput" placeholder="<?=$this->lang->line('survey_writename');?>" />
						</div>
						<div class="select-options-container-super" ng-show="userOptions.length > 0">
							<div class="select-options-container">
								<div class="select-option no-selectable" ng-repeat="user in userOptions track by $index" ng-init="i=$index" ng-click="addUser(i)">
									<span class="selected-option-image" style="background-image: url('/<?php echo FOLDERADD; ?>/users/profilephoto?userId={{user.userId}}&amp;amp;wah=200');filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='/<?php echo FOLDERADD; ?>/users/profilephoto?userId={{user.userId}}&wah=200',sizingMethod='scale');"></span>
									<span class="selected-option-label" ng-bind="user.completeName+' <'+user.userName+'>'"></span>
								</div>
							</div>
						</div>
					</div>
				</div>

				<h3 style="margin-top: 0; margin-bottom: 0; padding-left: 10px"><?php echo $this->lang->line('survey_questions'); ?> <strong class="text-danger" role="required">*</strong></h3>
				<hr style="margin-top: 10px; "/>
				<div id="questions-group">
					<div class="question-item"  ng-repeat="question in questions track by $index" ng-init="i=$index; question.ind=i"  ng-class="question.classTag">
						<div class="form-group">
							<label class="col-sm-2 control-label"><?=$this->lang->line('survey_question');?> <strong class="text-danger">*</strong></label>
							<div class="col-sm-10">
								<input type="text" class="form-control input-sm" name="question[{{i}}]" role="question" ng-model="question.question" placeholder="<?=$this->lang->line('survey_question');?>">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label"><?=$this->lang->line('survey_question_type');?> <strong class="text-danger">*</strong></label>
							<div class="col-sm-10">
								<select class="form-control input-sm" name="type[{{i}}]" ng-model="question.type">
									<option value="1"><?=$this->lang->line('survey_question_optionunique');?></option>
									<option value="2"><?=$this->lang->line('survey_question_severaloptions');?></option>
									<option value="3"><?=$this->lang->line('survey_question_input');?></option>
									<option value="4"><?=$this->lang->line('survey_question_trueorfalse');?></option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label"><?=$this->lang->line('survey_description');?></label>
							<div class="col-sm-10">
								<textarea class="form-control input-sm" name="description[{{i}}]" ng-model="question.description" placeholder="<?=$this->lang->line('survey_description');?>"></textarea>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label"><?php echo $this->lang->line('survey_required_question');?></label>
							<div class="col-xs-10">
								<div class="checkbox checkbox-angular">
									<label ng-class="{check: question.required}">
										<input type="checkbox" name="requiredQ[{{i}}]" ng-model="question.required" value="TRUE">
									</label>
								</div>
							</div>
						</div>
						<div class="answers-group tof" ng-class="{block: question.type == 4}">
							<label class="col-sm-2 control-label"><?=$this->lang->line('survey_answer_correct');?> <strong class="text-danger" role="required">*</strong></label>
							<div class="col-sm-10">
								<div class="radio radio-angular">
									<label ng-class="{check: question.isTrue == 'TRUE'}">
										<input type="radio" name="isTrue[{{i}}]" value="TRUE" ng-model="question.isTrue">
										<?=$this->lang->line('general_true')?>
									</label>
								</div>
								<div class="radio radio-angular">
									<label ng-class="{check: question.isTrue == 'FALSE'}">
										<input type="radio" name="isTrue[{{i}}]" value="FALSE" ng-model="question.isTrue">
										<?=$this->lang->line('general_false')?>
									</label>
								</div>
							</div>
						</div>
						<div class="answers-group" ng-class="{block: question.type == 1 || question.type == 2}">
							<h4 class="col-sm-2"><?=$this->lang->line('survey_answers');?> <strong class="text-danger" role="required">*</strong></h4>
							<div class="answers" >
								<div class="answer-item row"  ng-repeat="answer in question.answers track by $index" ng-init="j=$index;" ng-class="answer.classTag">
									<div class="col-sm-offset-2 col-sm-10 ">
										<div class="form-group col-sm-4" ng-class="{inputError:answer.answer == ''}">
											<input type="text" class="form-control input-sm" name="answer[{{i}}][{{j}}]" ng-model="answer.answer" placeholder="<?=$this->lang->line('survey_answer')?>">
										</div>
										<div class="checkbox col-sm-4 checkbox-angular">
											<label ng-class="{check: answer.isTrue}">
												<input type="checkbox" name="isTrueA[{{i}}][{{j}}]" value="TRUE" ng-model="answer.isTrue"> <?=$this->lang->line('survey_answer_iscorrect')?>
											</label>
											<i class="fa fa-question-circle text-indigo fa-lg" style="cursor: pointer; margin-left: 5px;" data-toggle="tooltip" data-trigger="hover" data-placement="top" title="<?php echo $this->lang->line('survey_answer_iscorrect_help');?>" ></i>
										</div>
										<div class="form-group col-sm-4">
											<div class="text-center checkbox">
												<i class="btn btn-red btn-xs answer-remove" ng-click="answer.remove(i,j)" ><i class="fa fa-trash"></i></i>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="form-group text-center" style="margin-top: 10px;">
								<div class="col-sm-12 text-right">
									<span class="btn btn-lightgreen btn-sm add-answer" ng-click="question.addAnswer()"><?=$this->lang->line('survey_question_addanswer');?></span>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-12 text-right">
								<i class="btn btn-red btn-sm remove" ng-click="question.remove(i)" ><i class="fa fa-trash"></i></i>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group text-center" style="margin-top: 10px;">
					<div class="col-sm-12 text-right">
						<span class="btn btn-lightgreen" id="add-question" ng-click="addQuestion()"><?=$this->lang->line('survey_question_addquestion');?></span>
					</div>
				</div>
				<div class="form-group text-center">
					
					<button type="submit" class="btn btn-green btn-lg" ng-hide="sending"><?php echo $this->lang->line('general_edit'); ?></button>
					<button class="btn btn-green disabled" ng-show="sending"><i class="fa fa-refresh fa-spin fa-lg"></i></button>
					
				</div>

			</form>

			<div class="modal fade" id="no-complete" tabindex="-1" role="dialog">
				<div class="modal-dialog modal-sm">
					<div class="modal-content">
						<div class="modal-body">
							<p><?php echo $this->lang->line('survey_completeall')?> <strong class="text-danger">*</strong></p>
							<div class="text-right">
								<span class="btn btn-white" data-dismiss="modal" aria-label="Close"><?php echo $this->lang->line('general_accept'); ?></span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript" src="/<?php echo APPFOLDERADD; ?>/libraries/tinymce/tinymce.min.js"></script>
<script src="/<?php echo APPFOLDERADD; ?>/libraries/script/bootstrap-datepicker.min.js"></script>
<script src="/<?php echo APPFOLDERADD; ?>/libraries/script/locales/bootstrap-datepicker.es.min.js"></script>
<script type="text/javascript" src="/<?php echo APPFOLDERADD; ?>/libraries/script/jquery.form.js"></script>