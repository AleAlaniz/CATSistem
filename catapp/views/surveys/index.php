<link href="/<?php echo APPFOLDERADD; ?>/libraries/css/animate.min.css" rel="stylesheet">
<div class="page-title">
	<h5 class="title"><?php echo $this->lang->line('survey_surveys'); ?></h5>
	<?php if($this->Identity->Validate('surveys/create')) 
	{ 
		?>
		<a href="#/surveys/create" class="btn  btn-white "><i class="fa fa-plus text-muted"></i> <?=$this->lang->line('survey_new');?></a>
		<?php 
	}
	?>
</div>

<div class="col-xs-12 flat-style">
	<?php
	if ($this->Identity->Validate('surveys/manage')) {
		?>
		<div class="alert bg-lightgreen alert-dismissible" role="alert" ng-show="message != null && message != ''">
			<button type="button" class="close" ng-click="message = ''"><span aria-hidden="true">&times;</span></button>
			<strong><i class="fa fa-check"></i></strong> <span ng-bind="message"></span>
		</div>
		
		<div class="modal animated shake" id="confirm-delete" tabindex="-1" role="dialog">
			<div class="modal-dialog modal-sm ">
				<div class="modal-content">
					<div class="modal-body">
						<p><?php echo $this->lang->line('survey_delete_confirm_message')?><span class="" role="survey-title"></span>?</p>
						<div class="text-right">
							<span class="btn btn-white" data-dismiss="modal" aria-label="Close"><?php echo $this->lang->line('general_no'); ?></span>
							<button class="btn btn-lightgreen" role="delete"><?php echo $this->lang->line('general_yes'); ?></button>
							<button class="btn btn-lightgreen disabled" role="deleteLoading" ><i class="fa fa-refresh fa-spin fa-lg"></i></button>
						</div>
					</div>
				</div>
			</div>
		</div>


		<div class="modal fade" id="confirm-activate" tabindex="-1" role="dialog">
			<div class="modal-dialog modal-sm">
				<div class="modal-content">
					<div class="modal-body">
						<p><?php echo $this->lang->line('survey_activate_confirm_message')?></p>
						<div class="text-right">
							<span class="btn btn-white" data-dismiss="modal" aria-label="Close"><?php echo $this->lang->line('general_no'); ?></span>
							<button type="submit" role="activate" class="btn btn-green" ng-hide="sending"><?=$this->lang->line('general_yes');?></button>
							<button class="btn btn-green disabled" role="activateLoading"><i class="fa fa-refresh fa-spin fa-lg"></i></button>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php
	}
	?>

	<div class="panel" role="loading" ng-show="loading">
		<div class="panel-body text-center">
			<i class='fa fa-refresh fa-spin fa-5x fa-fw dark' ></i>
		</div>
	</div>
	<div  ng-hide="loading">
		<div class="alert bg-pink" role="alert" ng-show="surveys.length == 0"><?php echo $this->lang->line('survey_empty'); ?></div>

		<div class="jumbotron" ng-repeat="survey in surveys">
			<h2 ng-bind="survey.name"></h2>
			<hr/>
			<p ng-bind="survey.description"></p>

			<div class="small-lg" style="margin-bottom: 10px" ng-show="survey.startDate || survey.endDate">
				<div ng-show="survey.startDate"><?php echo $this->lang->line('survey_start_date'); ?>: <b ng-bind="survey.startDate"></b></div> 
				<div ng-show="survey.endDate"><?php echo $this->lang->line('survey_end_date'); ?>: <b ng-bind="survey.endDate"></b>
				<?php if ($this->Identity->Validate('surveys/manage')) {?>
					<i class="fa fa-pencil fa-lg btn" ng-show = "survey.active == 'TRUE'" ng-click="editDateSurvey = true; showEdit();" title="<?php echo $this->lang->line('survey_edit_enddate')?>"></i>
				<?php } ?>
				</div>
				<?php if ($this->Identity->Validate('surveys/manage')) {?>
				<div class="">

					<input ng-show="editDateSurvey" ng-model="survey.editDate" type="text" class="form-control input-sm" role="endDate" style="width:42em;margin-bottom: 0.5em;" placeholder="<?php echo $this->lang->line('survey_end_date');?>">
					<h6 id="{{survey.surveyId}}" style="display: none;" class="text-danger"><?php echo $this->lang->line('survey_empty_date'); ?></h6>

					<i class="fa fa-check btn btn-green" ng-show="editDateSurvey" ng-click="updateDate(survey.surveyId,survey.editDate,endDateEmpty);" title="<?php echo $this->lang->line('survey_confirm_edit');?>"></i>
					<i class="fa fa-times btn btn-red" ng-show="editDateSurvey" ng-click="editDateSurvey = false; hideEmpty(survey.surveyId);" title="<?php echo $this->lang->line('general_cancel');?>"></i>
				</div>
				<?php } ?>
			</div>

			<p style="margin-top: 10px;" ng-show="survey.active == 'TRUE'">
				<?php
				if ($this->Identity->Validate('surveys/manage')) {
					?>
					<a class="btn btn-green" href="#/surveys/report/{{survey.surveyId}}">
						<i class="fa fa-bar-chart"></i> <?php echo $this->lang->line('survey_report'); ?>
					</a>
					<a class="btn btn-lightgreen" href="#/surveys/details/{{survey.surveyId}}">
						<i class="fa fa-search"></i> <?php echo $this->lang->line('survey_details'); ?>
					</a>
					<span class="btn btn-red" data-survey="{{survey.surveyId}}" data-title="{{survey.name}}" data-target="#confirm-delete" data-toggle="modal">
						<i class="fa fa-trash"></i> <?php echo $this->lang->line('general_delete'); ?>
					</span>
					<a class="btn btn-green" href="/<?php echo FOLDERADD; ?>/surveys/showManualCharge/{{survey.surveyId}}">
						<i class="fa fa-file-text"> </i> <?php echo $this->lang->line('survey_manual_charge'); ?>
					</a>
					<?php
				}
				?>

				<a class="btn btn-dark" ng-if="survey.complete >= 1" href="#/surveys/myanswers/{{survey.surveyId}}">
					<?php echo $this->lang->line('survey_myanswers'); ?> <i class="fa fa-arrow-right"></i>
				</a>
				<a class="btn btn-dark" ng-if="survey.complete == 0 && survey.isTime" href="/<?php echo FOLDERADD; ?>/surveys/completesurvey/{{survey.surveyId}}">
					<?php echo $this->lang->line('survey_completesurvey'); ?> <i class="fa fa-arrow-right"></i>
				</a>
				
			</p>
			<p style="margin-top: 10px;" ng-show="survey.active == 'FALSE'">
				<?php
				if ($this->Identity->Validate('surveys/manage')) {
					?>

					<a class="btn btn-lightgreen" href="/<?php echo FOLDERADD; ?>/surveys/showPreview/{{survey.surveyId}}">
						<i class="fa fa-search"></i> <?php echo $this->lang->line('survey_view_preview'); ?>
					</a>
					
					<a class="btn btn-yelow" href="#/surveys/edit/{{survey.surveyId}}">
						<i class="fa fa-pencil-square-o"></i> <?php echo $this->lang->line('general_edit'); ?>
					</a>

					<span class="btn btn-red" data-survey="{{survey.surveyId}}" data-title="{{survey.name}}" data-target="#confirm-delete" data-toggle="modal">
						<i class="fa fa-trash"></i> <?php echo $this->lang->line('general_delete'); ?>
					</span>

					<span class="btn btn-green" data-survey="{{survey.surveyId}}" data-title="{{survey.name}}" data-target="#confirm-activate" data-toggle="modal">
						<i class="fa fa-bolt"></i> <?php echo $this->lang->line('survey_activate'); ?>
					</span>
					<?php
				}
				?>
			</p>
		</div>

	</div>
</div>
<script src="/<?php echo APPFOLDERADD; ?>/libraries/script/bootstrap-datepicker.min.js"></script>
<script src="/<?php echo APPFOLDERADD; ?>/libraries/script/locales/bootstrap-datepicker.es.min.js"></script>