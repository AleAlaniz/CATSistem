<link rel="stylesheet" type="text/css" href="/<?php echo APPFOLDERADD; ?>/libraries/css/dataTables.bootstrap.min.css">
<style>

.info
{
	position:relative;
	background-color:#252926;
	color:white;
	font-size: 12px;
	padding: 10px 20px;

	border-radius: 10px 10px 10px 10px;
	-moz-border-radius: 10px 10px 10px 10px;
	-webkit-border-radius: 10px 10px 10px 10px;
	display: none;

	z-index: 300;
}

.details-item:hover .countUser + .info{

	display: block;
}

.countUser
{
	cursor:default;
}

</style>
<div class="page-title">
	<h3 class="title"><?php echo $this->lang->line('survey_report'); ?></h3>
	<a href="#/surveys" class="btn btn-white pull-right"><i class="fa fa-chevron-left"></i> <?=$this->lang->line('general_goback');?></a>
</div>
<div class="col-xs-12 flat-style">
	<div class="panel" role="loading" ng-show="loading">
		<div class="panel-body text-center">

			<i class='fa fa-refresh fa-spin fa-5x fa-fw dark' ></i>
		</div>
	</div>
	<div class="panel ng-hide" ng-show="survey && !loading" ng-init="trueLang = '<?php echo $this->lang->line('general_true'); ?>'; falseLang = '<?php echo $this->lang->line('general_false'); ?>'">
		<div class="panel-custom-heading" >
			<h4  class="f-w-500" ng-bind="survey.name"></h4>
		</div>
		<div class="panel-body">
			<!-- form para exportar tabla a excel -->
			<form ng-if="survey.completeTimes.length > 0 || survey.incompleteTimes.length > 0" action="/<?=FOLDERADD?>/surveys/exportReportSurvey" method="post" target="_blank" id="exportForm">
				<button class="btn bg-lightgreen" ng-click="exportData()"><i class="fa fa-file-excel-o"></i> <?php echo $this->lang->line('survey_export_report');?></button>			
				<input type="hidden" id="surveyName" name="surveyName"/>
				<input type="hidden" id="completesSend" name="completesSend"/>
				<input type="hidden" id="incompletesSend" name="incompletesSend"/>
			</form>
			<!-- fin de form de exportar -->

			<div class="details-list">
				<div class="details-item">
					<div class="details-label">
						<?php echo $this->lang->line('survey_report_totalcompletes'); ?>
					</div>	
					<div class="details-detail" ng-bind="survey.completeTimes.length"></div>
					<div ng-if="survey.completeTimes.length > 0">
						<button class="btn" ng-click="displayTables('cTable')" title="<?=$this->lang->line('general_showhide_content');?>"><i ng-bind="openCtable ? 'Ocultar detalles' : 'Mostrar detalles'"></i></button>
						<div id="cTable">
							<table id="completesTable" class="table table-hover" role="dataTable">
								<thead>
									<tr class="active  form-inline" style="border: 1px solid;background-color:#ABCECE;">
										<th><?=$this->lang->line('survey_user_name');?></th>
										<th><?=$this->lang->line('survey_user_lastname');?></th>
										<th><?=$this->lang->line('survey_user_campaing');?></th>
										<th><?=$this->lang->line('general_site');?></th>
										<th><?=$this->lang->line('survey_user_sex');?></th>
										<th><?=$this->lang->line('survey_user_birthday');?></th>
										<th><?=$this->lang->line('survey_user_date');?></th>
										<th ng-show="showCorrect"><?=$this->lang->line('survey_correctresponses');?></th>

									</tr>
								</thead>
								<tbody>
									<tr ng-repeat="user in survey.completeTimes" style="border: 1px solid">
										<td ng-bind="user.name"></td>
										<td ng-bind="user.lastName"></td>
										<td ng-bind="user.campaign"></td>
										<td ng-bind="user.site"></td>
										<td ng-bind="user.gender"></td>
										<td ng-bind="user.birthDate"></td>
										<td ng-bind="user.date"></td>
										<td ng-show="showCorrect" ng-bind="user.correctresponses.count"></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>

				</div> 
				<div class="details-item" style="margin:40px 0">
					<div class="details-label">
						<?php echo $this->lang->line('survey_report_totalincompletes'); ?>
					</div>	
					<div class="details-detail" ng-bind="survey.incompleteTimes.length"></div>
					<div ng-if="survey.incompleteTimes.length > 0">
						<button class="btn" ng-click="displayTables('iTable')" title="<?=$this->lang->line('general_showhide_content');?>"><i ng-bind="openItable ? 'Ocultar detalles' : 'Mostrar detalles'"></i></button>
						<div id="iTable">
							<table id="incompletesTable" class="table table-hover">
								<thead>
									<tr class="active  form-inline" style="border: 1px solid;background-color:#ABCECE;">
										<th><?=$this->lang->line('survey_user_name');?></th>
										<th><?=$this->lang->line('survey_user_lastname');?></th>
										<th><?=$this->lang->line('survey_user_campaing');?></th>
										<th><?=$this->lang->line('general_site');?></th>
										<th><?=$this->lang->line('survey_user_sex');?></th>
										<th><?=$this->lang->line('survey_user_birthday');?></th>
									</tr>
								</thead>
								<tbody>
									<tr ng-repeat="user in survey.incompleteTimes" style="border: 1px solid">
										<td ng-bind="user.name"></td>
										<td ng-bind="user.lastName"></td>
										<td ng-bind="user.campaign"></td>
										<td ng-bind="user.site"></td>
										<td ng-bind="user.gender"></td>
										<td ng-bind="user.birthDate"></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div> 
			</div>

			<!-- Inicio de tabla de preguntas y respuestas -->
			<form ng-if="survey.completeTimes.length > 0" action="/<?=FOLDERADD?>/surveys/collectData" method="post" target="_blank" id="surveyResults">
			<button type ="button" class="btn bg-green" ng-click="exportSurveyResults()" ><i class="fa fa-file-excel-o"></i> <?php echo $this->lang->line('survey_survey_results');?></button>			
				<input type="hidden" id="surveyId" name="surveyId"/>
			</form>
			<h3><?php echo $this->lang->line('survey_useranswers'); ?></h3>

			<!-- fin de  tabla de preguntas y respuestas -->
			<div ng-repeat="question in survey.questions track by $index">
				<button class="accordion" ng-bind-html="question.question | HtmlSanitize" ng-click="displayResponses($index);question.open = !question.open" title="<?=$this->lang->line('general_showhide_content');?>"></button><i id="icon{{$index}}" class="fa-3x pull-right" style="position:absolute;right:45px;" ng-class="question.open ? 'fa fa-angle-down' : 'fa fa-angle-up'"></i>
				<div class="panel panel-default border-dark" id="response{{$index}}">
					<div class="panel-custom-heading bg-white" style="padding-top: 20px;padding-bottom: 20px;">
					</div>
					<div class="panel-body">
						<div ng-if="question.type == 1 ||question.type == 2">
							<div class="col-sm-6 col-md-4">
								<canvas question="{{question.surveyquestionId}}" width="250" height="250"></canvas>
							</div>
							<div class="col-sm-5 col-md-7 col-sm-offset-1 col-md-offset-1">
								<div ng-repeat="answer in question.answers" ng-init="color = (colors[$index]) ? colors[$index] : color" style="overflow: auto;">
									<div class="legend">
										<div class="legend-color" ng-style="{'background-color':color}"></div>
										<div class="legend-label">
											<span ng-bind="answer.answer"></span>
											<span ng-show="answer.isTrue == 'TRUE'">(<?php echo $this->lang->line('survey_answer_correct'); ?>)</span>
										</div>
									</div>
									<div class="details-list" style="margin-left: 20px;">
										<div class="details-item col-sm-6">
											<div class="details-label">
												<?php echo $this->lang->line('survey_peoplecount'); ?>
											</div>	
											<div class="details-detail countUser" ng-bind="answer.responses.length" ></div>
											<ul ng-if="answer.users.length > 0" class="info">
												<li ng-repeat="user in answer.users">{{user.completeName}}</li>
											</ul>
										</div> 
										<div class="details-item col-sm-6" style="border-top: 0;">
											<div class="details-label">
												<?php echo $this->lang->line('survey_percent'); ?>
											</div>	
											<div class="details-detail">
												<span ng-show="answer.responses.length > 0" ng-bind="((question.type == 1 ? (answer.responses.length * 100) / survey.completeTimes.length  : (answer.responses.length * 100) / question.totalResponses) | number:2) + '%'"></span>
												<span ng-hide="answer.responses.length > 0">0</span>
											</div>
										</div> 
									</div>

								</div>
							</div>
						</div>

						<div ng-if="question.type == 4">
							<div class="col-sm-6 col-md-4">
								<canvas question="{{question.surveyquestionId}}" width="250" height="250"></canvas>
							</div>
							<div class="col-sm-5 col-md-7 col-sm-offset-1 col-md-offset-1">
								<div class="legend">
									<div class="legend-color" style="background-color: #0cc2aa"></div>
									<div class="legend-label">
										<?php 
										echo $this->lang->line('general_true'); 
										?>
										<span ng-show="question.isTrue == 'TRUE'"> <?php echo " (".$this->lang->line('survey_answer_correct').")"; ?></span>
									</div>
								</div>
								<div class="details-list" style="margin-left: 20px;">
									<div class="details-item col-sm-6">
										<div class="details-label">
											<?php echo $this->lang->line('survey_peoplecount'); ?>
										</div>	
										<div class="details-detail" ng-bind="question.answers.true.length"></div>
									</div> 
									<div class="details-item col-sm-6" style="border-top: 0;">
										<div class="details-label">
											<?php echo $this->lang->line('survey_percent'); ?>
										</div>	
										<div class="details-detail">
											<span ng-show="question.answers.true.length > 0" ng-bind="(((question.answers.true.length * 100) / survey.completeTimes.length ) | number:2) + '%'"></span>
											<span ng-hide="question.answers.true.length > 0">0</span>
										</div>
									</div> 
								</div>
								<div class="legend">
									<div class="legend-color" style="background-color: #f55060"></div>
									<div class="legend-label">
										<?php 
										echo $this->lang->line('general_false'); 
										?>
										<span ng-show="question.isTrue == 'FALSE'"> <?php echo " (".$this->lang->line('survey_answer_correct').")"; ?></span>
									</div>
								</div>
								<div class="details-list" style="margin-left: 20px;">
									<div class="details-item col-sm-6">
										<div class="details-label">
											<?php echo $this->lang->line('survey_peoplecount'); ?>
										</div>	
										<div class="details-detail">
											<div class="details-detail" ng-bind="question.answers.false.length"></div>
										</div>
									</div> 
									<div class="details-item col-sm-6" style="border-top: 0;">
										<div class="details-label">
											<?php echo $this->lang->line('survey_percent'); ?>
										</div>	
										<div class="details-detail">
											<span ng-show="question.answers.false.length > 0" ng-bind="(((question.answers.false.length * 100) / survey.completeTimes.length ) | number:2) + '%'"></span>
											<span ng-hide="question.answers.false.length > 0">0</span>
										</div>
									</div> 
								</div>
							</div>
						</div>

						<div ng-if="question.type == 3">
							<h4><?php echo $this->lang->line('survey_useranswers'); ?></h4>
							<ul>
								<li ng-repeat="answer in question.answers" ng-bind="answer.answer"></li>
							</ul>
						</div>
					</div>
				</div>
				<hr>	
			</div>
		</div>

	</div>
</div>
