<!DOCTYPE HTML>
<html>
<head>
	<script src="/<?=APPFOLDERADD?>/libraries/script/excanvas.min.js"></script>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="shortcut icon" href="/<?php echo APPFOLDERADD;?>/libraries/images/favicon.png" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" type="text/css" href="/<?php echo APPFOLDERADD;?>/libraries/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="/<?php echo APPFOLDERADD;?>/libraries/css/style.min.css">
	<link rel="stylesheet" type="text/css" href="/<?php echo APPFOLDERADD;?>/libraries/css/font-awesome.min.css">
	<title>CAT - Technologies</title>
	<style type="text/css">
		
	</style>
</head>
<body class="flat-style">

	<script src="/<?php echo APPFOLDERADD;?>/libraries/script/jquery-1.11.3.min.js"></script>
	<script src="/<?php echo APPFOLDERADD;?>/libraries/script/bootstrap.min.js"></script>
	
	<nav class="navbar-fixed-top page-title text-center">
		<img alt="cat" src="/<?=APPFOLDERADD;?>/libraries/images/catnet.png" class="brand-image">
		<h3 class="title hidden-xs"><i class="fa fa-pie-chart"></i> <?php echo $this->lang->line('survey_surveys'); ?></h3>
		<?php
			if (isset($isManual) && $isManual) {?>
				<a href="/<?=FOLDERADD?>/#/surveys" class="btn btn-white pull-right f-w-400"><i class="fa fa-chevron-left"></i>&nbsp;Atrás</a>
		<?php	}
		else{
		?>
			<a href="/<?=FOLDERADD?>/home/logout" class="btn btn-white pull-right f-w-400"><i class="fa fa-sign-out"></i>&nbsp;Salir</a>
		<?php }?>
		<div class="pull-right" style="margin-top: 16px;">
			<div class="profileImage profileImage25" style="filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='/<?php echo FOLDERADD; ?>/users/profilephoto?userId=<?php echo $this->session->UserId;?>&wah=200',sizingMethod='scale');background-image:url(/<?=FOLDERADD?>/users/profilephoto?userId=<?=$this->session->UserId?>&wah=200); width: 30px; height: 30px; margin-right: 15px" ></div>
		</div>
	</nav>
	<div class="container-fluid">
		<div class="row">

			<div class="col-xs-12">
