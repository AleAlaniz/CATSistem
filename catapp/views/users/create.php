<ol class="breadcrumb">
	<li><a href="/<?=FOLDERADD?>/administration"><?=$this->lang->line('general_administration');?></a></li>
	<li><a href="/<?=FOLDERADD?>/users"><?=$this->lang->line('general_users');?></a></li>
	<li class="active"><?=$this->lang->line('administration_create');?></li>
</ol>
<div class="panel panel-default">
	<div class="panel-heading">
		<strong><?=$this->lang->line('administration_create_user');?></strong>
	</div>
	<div class="panel-body">
		<form class="form-horizontal" method="POST" enctype="multipart/form-data" novalidate >
			<div class="form-group" id="uniquePhoto">
				<label for="userfile" class="col-sm-2 control-label"><?=$this->lang->line('administration_users_photo');?></label>
				<div class="col-sm-10">
					<input accept="image/x-png, image/gif, image/jpeg"  type="file"  id="userfile" name="userfile" value="<?php echo set_value('userfile');?>">
					<small>Tipos de archivos permitidos: gif, jpg, png</small>
					<div>
						<img style="max-height: 200px; max-width: 200px;" id="imgprev" src="/<?php echo FOLDERADD;?>/users/profilephoto?userId=0">
					</div>
				</div>
			</div>
			<div class="form-group">
				<label for="active" class="col-sm-2 control-label"><?=$this->lang->line('administration_users_active');?></label>
				<div class="col-sm-10">
					<div class="checkbox" style="padding-left:21px">
						<input type="checkbox" name="active" value="active" checked >
					</div>
					<?php echo form_error('active'); ?>

				</div>
			</div>
			<div class="form-group">
				<label for="name" class="col-sm-2 control-label"><?=$this->lang->line('administration_users_name');?><span class="text-danger"><strong> *</strong></span></label>
				<div class="col-sm-5">
					<input type="text" class="form-control input-sm" id="name" name="name" value="<?php echo set_value('name');?>" placeholder="<?=$this->lang->line('administration_users_name');?>" required>
					<?php echo form_error('name'); ?>
				</div>
				<div class="col-sm-5">
					<input type="text" class="form-control input-sm" id="lastName" name="lastName" value="<?php echo set_value('lastName');?>" placeholder="<?=$this->lang->line('administration_users_lastname');?>" required>
					<?php echo form_error('lastName'); ?>
				</div>
			</div>
			<div class="form-group">
				<label for="userName" class="col-sm-2 control-label"><?=$this->lang->line('login_username');?><span class="text-danger"><strong> *</strong></span></label>
				<div class="col-sm-10">
					<input type="text" class="form-control input-sm" id="userName" name="userName" value="<?php echo set_value('userName');?>" placeholder="<?=$this->lang->line('login_username');?>" required>
					<?php echo form_error('userName'); ?>
				</div>
			</div>
			<div class="form-group">
				<label for="password" class="col-sm-2 control-label"><?=$this->lang->line('login_password');?><span class="text-danger"><strong> *</strong></span></label>
				<div class="col-sm-10">
					<input type="password" class="form-control input-sm" id="password" name="password" value="<?php echo set_value('password');?>" placeholder="<?=$this->lang->line('login_password');?>" required>
					<?php echo form_error('password'); ?>
				</div>
			</div>
			<div class="form-group">
				<label for="passwordConfirm" class="col-sm-2 control-label"><?=$this->lang->line('administration_users_passwordconfirm');?><span class="text-danger"><strong> *</strong></span></label>
				<div class="col-sm-10">
					<input type="password" class="form-control input-sm" id="passwordConfirm" name="passwordConfirm" value="<?php echo set_value('passwordConfirm');?>" placeholder="<?=$this->lang->line('administration_users_passwordconfirm');?>" required>
					<?php echo form_error('passwordConfirm'); ?>
				</div>
			</div>
			<div class="form-group">
				<label for="role" class="col-sm-2 control-label"><?=$this->lang->line('administration_users_role');?><span class="text-danger"><strong> *</strong></span></label>
				<div class="col-sm-10">
					<select class="form-control input-sm" name="role" id="role" >
						<?php
						foreach($roles as $role){
							?>
							<option value="<?=$role->roleId?>" <?php echo  set_select('role', $role->roleId); ?> ><?=$role->name?></option>
							<?php
						}
						?>
					</select>
					<?php echo form_error('role'); ?>
				</div>
			</div>
			<div class="form-group">
				<label for="section" class="col-sm-2 control-label"><?=$this->lang->line('general_section');?><span class="text-danger"><strong> *</strong></span></label>
				<div class="col-sm-10">
					<select class="form-control input-sm" id="section" name="section">
						<?php
						foreach($sections as $section){
							?>
							<option value="<?=$section->sectionId?>" <?php echo  set_select('section', $section->sectionId); ?> ><?=$section->name?></option>
							<?php
						}
						?>
					</select>
					<?php echo form_error('section'); ?>

				</div>
			</div>
			<div class="form-group">
				<label for="site" class="col-sm-2 control-label"><?=$this->lang->line('general_site');?><span class="text-danger"><strong> *</strong></span></label>
				<div class="col-sm-10">
					<select class="form-control input-sm" id="site" name="site">
						<?php
						foreach($sites as $site){
							?>
							<option value="<?=$site->siteId?>" <?php echo  set_select('site', $site->siteId); ?> ><?=$site->name?></option>
							<?php
						}
						?>
					</select>
					<?php echo form_error('site'); ?>

				</div>
			</div>
			<div class="form-group">
				<label for="userNumber" class="col-sm-2 control-label"><?=$this->lang->line('users_usernumber');?></label>
				<div class="col-sm-10">
					<input type="userNumber" class="form-control input-sm" id="userNumber" name="userNumber" value="<?php echo set_value('userNumber');?>" placeholder="<?=$this->lang->line('users_usernumber');?>" required>
					<?php echo form_error('userNumber'); ?>
				</div>
			</div>

			<hr>
			<div class="form-group text-center">
				<button type="submit" class="btn btn-success btn-sm"><?=$this->lang->line('general_save');?></button>
				<a href="/<?=FOLDERADD?>/users" class="btn btn-danger btn-sm"><?=$this->lang->line('general_cancel');?></a>
			</div>
		</form>
	</div>
</div>
<script type="text/javascript">function readURL(e){if(e.files&&e.files[0]){var a=new FileReader;a.onload=function(e){$("#imgprev").attr("src",e.target.result)},a.readAsDataURL(e.files[0])}}$("#nav_users").addClass("active"),$("#userfile").change(function(){readURL(this)});</script>

