<?php 
if (isset($_SESSION['userconfigMessage'])){ 
	?>
	<div class="alert alert-success" role="alert">
		<strong><i class="fa fa-check"></i></strong> 
		<?php if ($_SESSION['userconfigMessage'] == 'editphoto'){
			echo $this->lang->line('administration_users_editphotomessage');
		}
		?>
	</div>
	<?php 
}
?>
<div class="panel panel-default">
	<div class="panel-heading">
		<strong><?=$this->lang->line('administration_select_photo_message');?></strong>
	</div>
	<div class="panel-body">
		<h3><?php echo $this->lang->line('administration_select_photo_selected'); ?> <i class="fa fa-check"></i></h3><br>
			<img style="max-height:200px; max-width:200px;" src="/<?=FOLDERADD;?>/users/profilephoto?userId=<?=$UserId?>&wah=200">
			<a class="pull-left btn" style="position: absolute;margin-left:-2.5em;"  role="deleteAlert" href="/<?=FOLDERADD?>/users/resetphoto/<?=$UserId?>" title="<?= $this->lang->line("administration_delete_image_text")?>" ><i class="fa fa-times text-danger fa-lg"></i></a>
		<hr>
		<h3><?php echo $this->lang->line('administration_select_photo_enable'); ?></h3><br>
		<div>
			<?php foreach ($images as $value) {?>
			<a   href="/<?php echo FOLDERADD;?>/users/selectimage?imageId=<?php echo $value->imageId;?>">
			<img  class="image" style="margin-right:25px; max-height:200px; max-width:200px;"  id="imgprev" src="/<?php echo FOLDERADD;?>/users/getprofilephotos?imageId=<?php echo $value->imageId;?>&wah=200">
			</a>
			<?php } ?>
		</div>
	</div>
</div>

<script type="text/javascript">
$('#nav_selectPhoto').addClass('active');
	
$('[role=deleteAlert]').click(function(e) {
		var goToF = confirm('<?=$this->lang->line("administration_themes_resetimage_areyousure")?>');
		if(!goToF){
			e.preventDefault();
		}
});

$('.image').mouseover(function(){
    $(this).animate({opacity: '0.7'});
}); 

$('.image').mouseout(function(){
    $(this).animate({opacity: '1'});
}); 

</script>