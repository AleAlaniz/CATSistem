<ol class="breadcrumb">
	<li><a href="/<?=FOLDERADD?>/administration"><?=$this->lang->line('general_administration');?></a></li>
	<li><a href="/<?=FOLDERADD?>/users"><?=$this->lang->line('general_users');?></a></li>
	<li><a href="/<?=FOLDERADD?>/users/details/<?=$userId;?>"><?=encodeQuery($lastName);?> <?=encodeQuery($name);?></a></li>
	<li class="active"><?=$this->lang->line('administration_configImages');?></li>
</ol>
<?php if (isset($_SESSION['userMessage'])): ?>
	<div class="alert alert-success alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<strong><i class="fa fa-check"></i></strong> 
		<?php if ($_SESSION['userMessage'] == 'configImages'){
			echo $this->lang->line('administration_users_configimages');
		}
		?>
	</div>
<?php endif; ?>
<div class="col-xs-12" style="margin-bottom:15px">
	<?=$navBar?>
</div>
<div class="col-xs-12">
	<div class="panel panel-default">
		<div class="panel-heading">
			<strong><?=$this->lang->line('administration_configImages');?></strong>
		</div>
		<div class="panel-body">
			<form class="form-horizontal" method="POST" enctype="multipart/form-data" novalidate >
				<div class="form-group" id="morePhoto">
					
					
					<?php if(is_null($error))
						{
					?><label class="col-sm-2 control-label"><?=$this->lang->line('administration_users_photos');?></label>
						<div class="col-sm-10" id="userfiles">
							<input id="1" accept="image/x-png, image/gif, image/jpeg"  type="file" name="userfiles[]" class="file" value="<?php echo set_value('userfile[]');?>">
							<small>Tipos de archivos permitidos: gif, jpg, png</small>
							<small>(Máximo 5 imágenes.)</small>
							<br>
							
							<?php echo form_error('userfile[]'); ?>
						</div>
					<?php } ?>
					
					<p id="error" style="color:red; text-align:center"><?php echo $error;?></p>
				</div>
				<?php if(is_null($error))
						{ ?>
				<div  class="form-group text-center">
					<button type="submit" class="btn btn-success btn-sm"><?=$this->lang->line('general_save');?></button>
					<a href="/<?=FOLDERADD?>/users" class="btn btn-danger btn-sm"><?=$this->lang->line('general_cancel');?></a>
				</div>
				<?php } ?>
			</form>
			<div id="previews">

			</div>
			<hr>
			
			<h4><?php echo $this->lang->line('administration_select_photo_enable'); ?></h4><br>
			<div>
				<?php foreach ($images as $value) {?>
				<div style="float:left; margin-right:25px;">
					<p style="position: relative;" class="text-right">
						<img  class="img-thumbnail"  id="imgprev" src="/<?php echo FOLDERADD;?>/users/getprofilephotos?imageId=<?php echo $value->imageId;?>&wah=200">
						<a class="pull-right btn" style="position: absolute;top:0;right:0;"  role="deleteAlert" href="/<?=FOLDERADD?>/users/deletephoto/<?=$value->imageId?>"><i class="fa fa-times text-danger fa-lg"></i></a>
					</p>
				</div>
				<?php } ?>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
$('#nav_users').addClass('active');
$('#userNavconfigImages').addClass('active');
</script>


<script>
var count = <?php echo $count; ?>;

$('form').submit(function(){

	var files=document.getElementsByClassName("file");
	var total=count;

	for(i=0; i < files.length;i++)
	{
		if(files[i].value != '')
		{
			total++;
		}
	}

	if(total > 5)
	{		

		$('#error').text('No puedes subir más de 5 imagenes.');
		return false;
	}
});

</script>

<script type="text/javascript">
$(document).ready(function() {

	$('[role=deleteAlert]').click(function(e) {
		var goToF = confirm('<?=$this->lang->line("administration_themes_deleteimage_areyousure")?>');
		if(!goToF){
			e.preventDefault();
		}
	});


	function readURL(input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();
			
			var image;

			reader.onload = function (e) {
				image = $('<img/>', {
					'src': e.target.result,
					'style':'margin-right:25px ;max-height: 200px; max-width: 200px;',
					'id':$(input).attr('id'),
					'class':'fileImg'
				});

				var existe=0;

				$('img[class^="fileImg"]').each(function()
					{  if($(input).attr('id') == $(this).attr('id'))
						existe=1;  
						return;
					});

				if(existe==1)
				{
					$("img[id^="+$(input).attr('id')+"]").replaceWith(image);
				}
				else
				{
					$('#previews').append(image);
				}
				
			}

			reader.readAsDataURL(input.files[0]);
		}
		else
		{
			$("img[id^="+$(input).attr('id')+"]").remove();
		}
	}


	var idInput=2;

	$('input[name^="userfiles"]').on('change',function() {
		var allUsed = true;

		jQuery.each($('input[name^="userfiles"]'), function(i, userfile) {
			var value = $(this).val();
			if (value == '') {
				allUsed = false;
			}
		});
		if (allUsed) {
			var newFile = $(this).clone(true);
			newFile.attr({id:idInput++});
			newFile.val('');
			$('#userfiles').prepend(newFile)

		}
		readURL(this);

	});

});
</script>


