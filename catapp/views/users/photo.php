<?php 
if (isset($_SESSION['userconfigMessage'])){ 
	?>
	<div class="alert alert-success" role="alert">
		<strong><i class="fa fa-check"></i></strong> 
		<?php if ($_SESSION['userconfigMessage'] == 'editphoto'){
			echo $this->lang->line('administration_users_editphotomessage');
		}
		?>
	</div>
	<?php 
}
?>
<div class="panel panel-default">
	<div class="panel-heading">
		<strong><?=$this->lang->line('administration_users_configphoto');?></strong>
	</div>
	<div class="panel-body">
		<form class="form-horizontal" method="POST" enctype="multipart/form-data" novalidate >
			<input type="hidden" name="request" value="post">
			<div class="form-group">
				<label class="col-sm-2 control-label"><?=$this->lang->line('administration_users_actualphoto');?></label>
				<div class="col-sm-10">
					<img style="max-height: 200px; max-width: 200px;" id="imgprev" src="/<?php echo FOLDERADD; ?>/users/profilephoto?userId=<?=$userId?>&wah=200">
				</div>
			</div>
			<hr>
			<div class="form-group">
				<label for="userfile" class="col-sm-2 control-label"><?=$this->lang->line('administration_users_newphoto');?><strong class="text-danger"> *</strong></label>
				<div class="col-sm-10">
					<input accept="image/x-png, image/gif, image/jpeg"  type="file"  id="userfile" name="userfile" value="<?php echo set_value('userfile');?>"><small>Tipos de archivos permitidos: gif, jpg, png</small>
					<?php echo $error;?>
					<?php echo form_error('userfile'); ?>
				</div>
			</div>
			<div class="form-group text-center">
				<button type="submit" class="btn btn-success btn-sm" ><?=$this->lang->line('general_save');?></button>
			</div>
		</form>
	</div>
</div>

<script type="text/javascript">function readURL(e){if(e.files&&e.files[0]){var a=new FileReader;a.onload=function(e){$("#imgprev").attr("src",e.target.result)},a.readAsDataURL(e.files[0])}}$("#nav_configPhoto").addClass("active"),$("#userfile").change(function(){readURL(this)});</script>