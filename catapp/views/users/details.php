<ol class="breadcrumb">
	<li><a href="/<?=FOLDERADD?>/administration"><?=$this->lang->line('general_administration');?></a></li>
	<li><a href="/<?=FOLDERADD?>/users"><?=$this->lang->line('general_users');?></a></li>
	<li><a href="/<?=FOLDERADD?>/users/details/<?=$userId;?>"><?=encodeQuery($lastName);?> <?=encodeQuery($name);?></a></li>
	<li class="active"><?=$this->lang->line('administration_details');?></li>
</ol>
<?php if (isset($_SESSION['userMessage'])): ?>
	<div class="alert alert-success alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<strong><i class="fa fa-check"></i></strong> 
		<?php if ($_SESSION['userMessage'] == 'edit'){
			echo $this->lang->line('administration_users_editmessage');
		}
		?>
	</div>
<?php endif; ?>
<div class="col-xs-12" style="margin-bottom:15px">
	<?=$navBar?>
</div>
<div class="col-xs-12">
	<div class="panel panel-default">
		<div class="panel-heading">
			<strong><?=$this->lang->line('administration_details_user');?></strong>
		</div>
		<div class="panel-body">
			<div class="col-sm-2">
				<img class="img-thumbnail userImg" src="/<?=FOLDERADD;?>/users/profilephoto?userId=<?=$userId?>&wah=200">
			</div>
			<dl class="dl-horizontal col-sm-5">
				<dt><?=$this->lang->line('administration_users_name');?></dt>
				<dd><?=encodeQuery($name)?></dd>
				<dt><?=$this->lang->line('administration_users_lastname');?></dt>
				<dd><?=encodeQuery($lastName)?></dd>
				<dt><?=$this->lang->line('administration_users_role');?></dt>
				<dd><?=$role;?></dd>
				<dt><?=$this->lang->line('general_site');?></dt>
				<dd><?=encodeQuery($site)?></dd>
			</dl>
			<dl class="dl-horizontal col-sm-5">
				<dt><?=$this->lang->line('general_section');?></dt>
				<dd><?=encodeQuery($section)?></dd>
				<dt><?=$this->lang->line('administration_users_username');?></dt>
				<dd><?=encodeQuery($userName)?></dd>
				<dt><?=$this->lang->line('administration_users_active');?></dt>
				<dd><?php echo ($active == 1) ? "Si" : "No" ; ?></dd>
			</dl>
		</div>
	</div>
</div>
<script type="text/javascript">
	$('#nav_users').addClass('active');
	$('#userNavDetails').addClass('active');
</script>