<link href="/<?php echo APPFOLDERADD; ?>/libraries/css/bootstrap-datepicker.css" rel="stylesheet">
<?php 
if (isset($_SESSION['userconfigMessage'])){ 
	?>
	<div class="alert alert-success" role="alert">
		<strong><i class="fa fa-check"></i></strong> 
		<?php if ($_SESSION['userconfigMessage'] == 'edit'){
			echo $this->lang->line('administration_users_editconfigmessage');
		}
		?>
	</div>
	<?php 
}
if ($this->input->get('msg') == 'notify') {
	?>
	<div class="modal fade" id="message" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-body text-center">
					<img src="/<?php echo APPFOLDERADD; ?>/libraries/images/pop_up_3.jpg">
					<div>
						<span class="btn btn-white" data-dismiss="modal" aria-label="Close"><?php echo $this->lang->line('general_accept'); ?></span>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php
}
?>

<div class="panel panel-default" ng-app="catnetUserConfig" ng-controller="configCtrl">
	<div class="panel-heading">
		<strong><?=$this->lang->line('administration_users_config');?></strong>
	</div>
	<div class="panel-body">
		<form class="form-horizontal" method="POST" novalidate action="/<?php echo FOLDERADD; ?>/users/config?cmd=data">
			<input type="hidden" name="request" value="post">
			<div class="form-group">
				<label for="email" class="col-sm-2 control-label"><?=$this->lang->line('administration_users_email');?><strong class="text-danger"> *</strong></label>
				<div class="col-sm-10">
					<input type="email" class="form-control input-sm" id="email" name="email"  value="<?php echo set_value('email', $email);?>" placeholder="<?=$this->lang->line('administration_users_email');?>">
					<?php echo form_error('email'); ?>
				</div>
			</div>
			<div class="form-group">
				<label for="address" class="col-sm-2 control-label"><?=$this->lang->line('administration_users_address');?><strong class="text-danger"> *</strong></label>
				<div class="col-sm-10">
					<input type="text" class="form-control input-sm" id="address" name="address"  value="<?php echo set_value('address', $address);?>" placeholder="<?=$this->lang->line('administration_users_address');?>">
					<?php echo form_error('address'); ?>
					<small>La actualización de datos por cambio de domicilio en CATnet no implica que la información esté notificada. Tenés que presentar la documentación correspondiente al área de Administración de Personal. ¡Gracias!</small>
				</div>
			</div>
			<div class="form-group">
				<label for="state" class="col-sm-2 control-label"><?=$this->lang->line('administration_users_state');?><strong class="text-danger"> *</strong></label>
				<div class="col-sm-10">
					<select class="form-control input-sm" id="state" name="state">
						<option value="-1"></option>
						<?php
						foreach ($states as $state) {
							?>
							<option value="<?php echo $state->stateId; ?>"<?php echo set_select('state', $state->stateId, $stateId == $state->stateId) ?> ><?php echo $state->state; ?></option>
							<?php
						}
						?>
					</select>
					<?php echo form_error('state'); ?>
				</div>
			</div>
			<div class="form-group">
				<label for="city" class="col-sm-2 control-label"><?=$this->lang->line('administration_users_city');?><strong class="text-danger"> *</strong></label>
				<div class="col-sm-10">
					<select class="form-control input-sm" id="city" name="city" data-default="<?php echo $cityId | $this->input->post('city'); ?>">
					</select>
					<?php echo form_error('city'); ?>
				</div>
			</div>
			<div class="form-group">
				<label for="zipCode" class="col-sm-2 control-label"><?=$this->lang->line('users_zipcode');?><strong class="text-danger"></strong></label>
				<div class="col-sm-10">
					<input type="text" class="form-control input-sm" id="zipCode" name="zipCode" ng-model="data.zipCode"  value="<?php echo set_value('zipCode', $zipCode);?>" placeholder="<?=$this->lang->line('users_zipcode');?>">
					<?php echo form_error('zipCode'); ?>
				</div>
			</div>
			<div class="form-group">
				<label for="birthDate" class="col-sm-2 control-label"><?=$this->lang->line('administration_users_birthday');?><strong class="text-danger"> *</strong></label>
				<div class="col-sm-10">
					<input type="text" class="form-control input-sm" data-date-format="dd/mm/yyyy" role="date" id="birthDate" name="birthDate" ng-model="data.birthDate" value="<?php echo set_value('birthDate', $birthDate);?>" placeholder="<?=$this->lang->line('administration_users_birthdayplaceholder');?>">
					<?php echo form_error('birthDate'); 
					if(isset($dateError))
					{
						echo('<p style="color:red">'.$dateError.'</p>');
					}

					?>

				</div>
			</div>
			<div class="form-group">
				<label for="gender" class="col-sm-2 control-label"><?=$this->lang->line('users_gender');?><strong class="text-danger"> *</strong></label>
				<div class="col-sm-10">
					<select class="form-control input-sm" id="gender" name="gender">
						<?php
						foreach ($genders as $k => $v) {
							?>
							<option value="<?php echo $k; ?>"<?php echo set_select('gender', $k, $gender == $k) ?> ><?php echo $v; ?></option>
							<?php
						}
						?>
					</select>
					<?php echo form_error('gender'); ?>
				</div>
			</div>
			<div class="form-group">
				<label for="civilState" class="col-sm-2 control-label"><?=$this->lang->line('users_civilstate');?><strong class="text-danger"> *</strong></label>
				<div class="col-sm-10">
					<select class="form-control input-sm" id="civilState" name="civilState">
						<?php
						foreach ($civilStates as $k => $v) {
							?>
							<option value="<?php echo $k; ?>"<?php echo set_select('civilState', $k, $civilState == $k) ?> ><?php echo $v; ?></option>
							<?php
						}
						?>
					</select>
					<?php echo form_error('civilState'); ?>
				</div>
			</div>

			<div class="form-group">
				<label for="campaign" class="col-sm-2 control-label"><?=$this->lang->line('administration_users_campaign');?><strong class="text-danger"> *</strong></label>
				<div class="col-sm-10">
				<?php if($this->Identity->Validate('users/isagent')) { ?>
					<select class="form-control input-sm" id="campaign" name="campaign">
						<option value="21">Operaciones</option>
					</select>
				<?php } 
				else{ ?>
					<select class="form-control input-sm" id="campaign" name="campaign">
						<option value="-1"> </option>
						<?php
						foreach($campaigns as $campaign){
							?>
							<option value="<?=$campaign->campaignId?>" <?php echo  set_select('campaign', $campaign->campaignId, $campaignId == $campaign->campaignId); ?> ><?php echo (!isset($campaign->client) ? $campaign->name : $campaign->client.' - '.$campaign->name);?></option>
							<?php
						}
						?>
					</select>
				<?php } ?>
					<?php echo form_error('campaign'); ?>
				</div>
			</div>

			<div class="form-group">
				<label for="dni" class="col-sm-2 control-label"><?=$this->lang->line('users_dni');?><strong class="text-danger"> *</strong></label>
				<div class="col-sm-10">
					<input type="text" class="form-control input-sm" id="dni" name="dni" ng-model="data.dni" value="<?php echo set_value('dni', $dni);?>" placeholder="<?=$this->lang->line('users_dni');?>">
					<?php echo form_error('dni'); ?>
				</div>
			</div>

			<div class="form-group">
				<label for="nationality" class="col-sm-2 control-label"><?=$this->lang->line('administration_users_nationality');?><strong class="text-danger"> *</strong></label>
				<div class="col-sm-10">
					<select class="form-control input-sm" id="nationality" name="nationality">
						<option value="-1"> </option>
						<?php
						
						foreach($nationalities as $nationality){
							?>
							<option value="<?=$nationality->nationalityId?>" <?php echo  set_select('nationality', $nationality->nationalityId, $nationalityId == $nationality->nationalityId); ?> ><?php echo ($nationality->value);?></option>
							<?php
						}
						?>
					</select>
					<?php echo form_error('nationality'); ?>
				</div>
			</div>

			<div class="form-group hidden" id='dsubcampaign'>
				<label for="subcampaign" class="col-sm-2 control-label"><?=$this->lang->line('subcampaign');?><strong class="text-danger"> *</strong></label>
				<div class="col-sm-10">
					<select class="form-control input-sm" id="subcampaign" name="subcampaign">
						<option value="-1"> </option>
						<?php
						foreach($subCampaigns as $subCampaign){
							?>
							<option value="<?=$subCampaign->subCampaignId?>" <?php echo  set_select('subcampaign', $subCampaign->subCampaignId, $subCampaignId == $subCampaign->subCampaignId); ?> ><?php echo (!isset($subCampaign->client) ? $subCampaign->name : $subCampaign->client.' - '.$subCampaign->name);?></option>
							<?php
						}
						?>
					</select>
					<?php echo form_error('subcampaign'); ?>
				</div>
			</div>

			<div class="form-group">
				<label for="site" class="col-sm-2 control-label"><?=$this->lang->line('general_site');?><span class="text-danger"><strong> *</strong></span></label>
				<div class="col-sm-10">
					<select class="form-control input-sm" id="site" name="site">
						<?php 
						foreach($sites as $site){
							?>
							<option value="<?=$site->siteId?>" <?php echo  set_select('site', $site->siteId, $siteId == $site->siteId); ?> ><?=$site->name?></option>
							<?php 
						}
						?>
					</select>
					<?php echo form_error('site'); ?>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label"><?php echo $this->lang->line('users_rent'); ?><span class="text-danger"><strong> *</strong></span></label>
				<div class="col-sm-10">
					<div class="radio">
						<label><input type="radio" name="rent" value="1" <?php echo set_checkbox('rent', 1, $rent == 1); ?> ><?php echo $this->lang->line('general_yes'); ?></label>
					</div>
					<div class="radio">
						<label><input type="radio" name="rent" value="0" <?php echo set_checkbox('rent', 0, $rent == 0); ?> ><?php echo $this->lang->line('general_no'); ?></label>
					</div>
					<?php echo form_error('rent'); ?>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label"><?php echo $this->lang->line('users_has_child'); ?><span class="text-danger"><strong> *</strong></span></label>
				<div class="col-sm-10">
					<div class="radio">
						<label><input type="radio" name="hasChild" value="1" <?php echo set_checkbox('hasChild', 1, $hasChild == 1); ?> ><?php echo $this->lang->line('general_yes'); ?></label>
					</div>
					<div class="radio">
						<label><input type="radio" name="hasChild" value="0" <?php echo set_checkbox('hasChild', 0, $hasChild == 0); ?> ><?php echo $this->lang->line('general_no'); ?></label>
					</div>
					<?php echo form_error('hasChild'); ?>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label"><?php echo $this->lang->line('users_personsincharge'); ?><span class="text-danger"><strong> *</strong></span></label>
				<div class="col-sm-10">
					<div class="radio">
						<label><input type="radio" name="personsInCharge" value="1" <?php echo set_checkbox('personsInCharge', 1, $personsInCharge == 1); ?> ><?php echo $this->lang->line('general_yes'); ?></label>
					</div>
					<div class="radio">
						<label><input type="radio" name="personsInCharge" value="0" <?php echo set_checkbox('personsInCharge', 0, $personsInCharge == 0); ?> ><?php echo $this->lang->line('general_no'); ?></label>
					</div>
					<?php echo form_error('personsInCharge'); ?>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label"><?php echo $this->lang->line('users_family_members'); ?></label>
				<div class="col-sm-10 table-responsive">
					<input type="hidden" name="deletedFamilyMembers[]" ng-repeat="member in deletedFamilyMembers" ng-value="member.familyMemberId">
					<table class="table table-bordered ">
						<thead ng-show="familyMembers.length > 0" class="ng-hide">
							<tr>
								<th class="col-sm-4"><?php echo $this->lang->line('users_gender'); ?></th>
								<th class="col-sm-4"><?php echo $this->lang->line('users_relation'); ?></th>
								<th class="col-sm-4"><?php echo $this->lang->line('users_birthdate'); ?></th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							<tr ng-repeat="member in familyMembers" ng-init="member.oldBirthDate = member.birthDate">
								<input type="hidden" name="familyMembers[]" ng-value="member | json : 0">
								<td>
									<select class="form-control input-sm" ng-model="member.gender">
										<option ng-repeat="(k,v) in genders" ng-value="k" ng-bind="v" ng-selected="k == member.gender"></option>
									</select>
								</td>
								<td>
									<select class="form-control input-sm" ng-model="member.familyRelationId">
										<option ng-repeat="relation in familyRelations | filter : {gender : member.gender}" ng-value="relation.familyRelationId" ng-bind="relation.relation" ng-selected="relation.familyRelationId == member.familyRelationId"></option>
									</select>
								</td>
								<td><input type="text" class="form-control input-sm" ng-model="member.birthDate" ng-change="((validate(member, 'birthDate', 10, 'dd/mm/yyyy')(member.birthDate, member.oldBirthDate)) || (member.oldBirthDate = member.birthDate))" role="date" placeholder="<?php echo $this->lang->line('users_birthdate'); ?>" ></td>
								<td><button class="btn btn-danger btn-sm" type="button" ng-click="removeFamilyMember($index)"><i class="fa fa-trash"></i></button></td>
							</tr>
							<tr><td colspan="6" class="text-center ng-hide" ng-show="familyMembers.length < 1"><?php echo $this->lang->line('users_nomembers');?></td></tr>
							<tr><td colspan="6" class="text-center"><button class="btn btn-warning btn-sm" type="button" ng-click="addFamilyMember()"><?php echo $this->lang->line('general_add'); ?></button></td></tr>
						</tbody>
					</table>
				</div>
			</div>
			<div class="form-group">
				<label for="turn" class="col-sm-2 control-label"><?php echo $this->lang->line('users_turn'); ?><strong class="text-danger"> *</strong></label>
				<div class="col-sm-10">
					<select class="form-control input-sm" id="turn" name="turn">
						<?php
						foreach ($turns as $k => $v) {
							?>
							<option value="<?php echo $k; ?>"<?php echo set_select('turn', $k, $turn == $k) ?> ><?php echo $v; ?></option>
							<?php
						}
						?>
					</select>
					<?php echo form_error('turn'); ?>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label"><?php echo $this->lang->line('users_phone'); ?></label>
				<div class="col-sm-10 form-inline">
					<div class="form-group col-sm-4">
						<label class="control-label"><?php echo $this->lang->line('users_area_code'); ?></label>
						<input type="text" class="form-control input-sm" id="phoneAreaCode" name="phoneAreaCode" ng-model="data.phoneAreaCode" value="<?php echo set_value('phoneAreaCode', $phoneAreaCode);?>" placeholder="<?=$this->lang->line('users_area_code');?>">
						<?php echo form_error('phoneAreaCode'); ?>
					</div>
					<div class="form-group col-sm-8">
						<label class="control-label"><?php echo $this->lang->line('users_number'); ?></label>
						<input type="text" class="form-control input-sm" id="phone" name="phone" ng-model="data.phone" value="<?php echo set_value('phone', $phone);?>" placeholder="<?=$this->lang->line('users_number');?>">
						<?php echo form_error('phone'); ?>
					</div>
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-2 control-label"><?php echo $this->lang->line('users_cell_phone'); ?><strong class="text-danger"> *</strong></label>
				<div class="col-sm-10 form-inline">
					<div class="form-group col-sm-4">
						<label class="control-label"><?php echo $this->lang->line('users_area_code'); ?></label>
						<input type="text" class="form-control input-sm" id="cellPhoneAreaCode" name="cellPhoneAreaCode" ng-model="data.cellPhoneAreaCode" value="<?php echo set_value('cellPhoneAreaCode', $cellPhoneAreaCode);?>" placeholder="<?=$this->lang->line('users_area_code');?>">
						<?php echo form_error('cellPhoneAreaCode'); ?>
					</div>
					<div class="form-group col-sm-8">
						<label class="control-label"><?php echo $this->lang->line('users_number_phone'); ?></label>
						<input type="text" class="form-control input-sm" id="cellPhone" name="cellPhone" ng-model="data.cellPhone" value="<?php echo set_value('cellPhone', $cellPhone);?>" placeholder="<?=$this->lang->line('users_number_phone');?>">
						<?php echo form_error('cellPhone'); ?>
					</div>
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-2 control-label"><?php echo $this->lang->line('users_hobbies'); ?></label>
				<div class="col-sm-10">
					<div search search-value="hobby" search-id="hobbyId" search-selected="userHobbies" search-all="hobbies" search-plural="Hobbies" search-placeholder="<?php echo $this->lang->line('users_add_hobby'); ?>"></div>
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-2 control-label"><?php echo $this->lang->line('users_transports'); ?></label>
				<div class="col-sm-10">
					<div search search-value="transport" search-id="transportId" search-selected="userTransports" search-all="transports" search-plural="Transports" search-placeholder="<?php echo $this->lang->line('users_add_transport'); ?>"></div>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label"><?php echo $this->lang->line('users_before_works'); ?></label>
				<div class="col-sm-10">
					<input type="hidden" name="deletedUserWorks[]" ng-repeat="work in deletedUserWorks" ng-value="work.userWorkId">
					<table class="table table-bordered">
						<tbody>
							<tr ng-repeat="work in userWorks">
								<input type="hidden" name="userWorks[]" ng-value="work | json : 0">
								<td class="col-xs-12">
									<select class="form-control input-sm" ng-model="work.workTaskId" ng-change="null">
										<option ng-repeat="task in workTasks" ng-value="task.workTaskId" ng-bind="task.task" ng-selected="task.workTaskId == work.workTaskId"></option>
									</select>
									<input type="text" class="form-control input-sm" ng-show="(workTasks | filter : { workTaskId : work.workTaskId})[0].haveDetails" ng-model="work.workDetail" placeholder="<?php echo $this->lang->line('users_details'); ?>" >
								</td>
								<td><button class="btn btn-danger btn-sm" type="button" ng-click="removeUserWork($index)"><i class="fa fa-trash"></i></button></td>
							</tr>
							<tr><td colspan="6" class="text-center ng-hide" ng-show="userWorks.length < 1"><?php echo $this->lang->line('users_no_before_works');?></td></tr>
							<tr><td colspan="6" class="text-center"><button class="btn btn-warning btn-sm" type="button" ng-click="addUserWork()"><?php echo $this->lang->line('general_add'); ?></button></td></tr>
						</tbody>
					</table>
				</div>
			</div>



			<div class="form-group">
				<label class="col-sm-2 control-label"><?php echo $this->lang->line('users_studies'); ?></label>
				<div class="col-sm-10">
					<input type="hidden" name="deletedUserStudies[]" ng-repeat="study in deletedUserStudies" ng-value="study.userStudyId">
					<table class="table table-bordered ">
						<thead ng-show="userStudies.length > 0" class="ng-hide">
							<tr>
								<th class="col-sm-3"><?php echo $this->lang->line('users_level'); ?></th>
								<th class="col-sm-3"><?php echo $this->lang->line('users_status'); ?></th>
								<th class="col-sm-3"><?php echo $this->lang->line('users_degree'); ?></th>
								<th class="col-sm-3"><?php echo $this->lang->line('users_institution'); ?></th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							<tr ng-repeat="study in userStudies">
								<input type="hidden" name="userStudies[]" ng-value="study | json : 0">
								<td>
									<span ng-bind="(studyLevels | filter : { k : study.level })[0].v" ng-if="study.required"></span>
									<select class="form-control input-sm" ng-model="study.level" ng-if="!study.required">
										<option ng-repeat="level in studyLevels track by $index" ng-value="level.k" ng-bind="level.v" ng-selected="level.k == study.level"></option>
									</select>
								</td>
								<td>
									<select class="form-control input-sm" ng-model="study.status">
										<option ng-repeat="(k,v) in studyStatues" ng-value="k" ng-bind="v" ng-selected="k == study.status"></option>
									</select>
								</td>
								<td>
									<select class="form-control input-sm" ng-model="study.careerId">
										<option ng-repeat="career in careers | filter : { level : study.level }" ng-value="career.careerId" ng-bind="career.career" ng-selected="career.careerId == study.careerId"></option>
										<option value="new" ><?php echo $this->lang->line("general_other"); ?></option>
									</select>
									<input type="text" class="form-control input-sm" ng-show="study.careerId == 'new'" ng-model="study.career" placeholder="<?php echo $this->lang->line('users_degree'); ?>" >
								</td>
								<td>
									<select class="form-control input-sm" ng-model="study.institutionId">
										<option ng-repeat="institution in institutions" ng-if="(studyLevels | filter : { k : study.level })[0].has_institution" ng-value="institution.institutionId" ng-bind="institution.institution" ng-selected="institution.institutionId == study.institutionId"></option>
									</select>
									<input type="text" class="form-control input-sm" ng-show="study.institutionId == 'new'" ng-model="study.institution" placeholder="<?php echo $this->lang->line('users_institution'); ?>" >
								</td>
								<td><button class="btn btn-danger btn-sm" type="button" ng-click="removeUserStudy($index)" ng-if="!study.required"><i class="fa fa-trash"></i></button></td>
							</tr>
							<tr><td colspan="6" class="text-center ng-hide" ng-show="userStudies.length < 1"><?php echo $this->lang->line('users_no_studies');?></td></tr>
							<tr><td colspan="6" class="text-center"><button class="btn btn-warning btn-sm" type="button" ng-click="addUserStudy()"><?php echo $this->lang->line('general_add'); ?></button></td></tr>
						</tbody>
					</table>
				</div>
			</div>



			<hr>
			<div class="form-group text-center">
				<button type="submit" class="btn btn-success" name=""><?=$this->lang->line('general_save');?></button>
			</div>
		</form>
	</div>

	<script src="/<?php echo APPFOLDERADD; ?>/libraries/script/bootstrap-datepicker.min.js"></script>
	<script src="/<?php echo APPFOLDERADD; ?>/libraries/script/locales/bootstrap-datepicker.es.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('#nav_configData').addClass('active');
			$('#message').modal('show');

			$('[role="date"]').datepicker({ language : 'es', autoclose : true });

			var a = [], zip_codes = {};
			function t() {
				$.ajax({
					method: "GET",
					url: "/<?php echo FOLDERADD; ?>/users/getcitiesbystate",
					data: {
						state: $("[name=state]").val()
					},
					dataType: "json"
				}).success(function(t) {
					zip_codes = {};
					if (t.state = "success") {
						a =  t.data;					
						if(a.length == 0){
							$('[name=city]').prop('disabled', 'disabled');
						}
						else{
							$('[name=city]').prop('disabled', false);
						}
						for (var i = 0; i < a.length; i++) {
							zip_codes[a[i].cityId] = a[i].zipCode;
						}
					}
					else{
						a = [];
					}
				}).error(function() {
					a = []
				}).done(function() {
					e()
				})
			}

			function e() {
				for (var t = $("[name=city]"), e = t.data("default"), n = "<option value='-1'></option>", o = 0; o < a.length; o++) n += a[o].city != null ? "<option value='" + a[o].cityId + "' " + (a[o].cityId == e ? ($('[name=zipCode]').val(a[o].zipCode), "selected") : "") + ">" + a[o].city + "</option>" : '';
					t.html(n)
			}
			$("[name=state]").on("change", t);
			t();

			$("[name=city]").on('change', function(e) {
				$('[name=zipCode]').val(''); 
			});

			if($('#campaign option:selected').text() == 'Operaciones'){
					$('#dsubcampaign').removeClass('hidden');
				}
			$('#campaign').click(function () {
				if($('#campaign option:selected').text() == 'Operaciones'){
					$('#dsubcampaign').removeClass('hidden');
				}
				else{
					document.getElementById('subcampaign').selectedIndex = 0;
					$('#dsubcampaign').addClass('hidden');
					
				}
			})
	// $('[name=zipCode]').on('change paste keyup', function() {
		// 	$("[name=city]").val('-1');
		// }); 
	});
	//Proto trim()
	"function"!=typeof String.prototype.trim&&(String.prototype.trim=function(){return this.replace(/^\s+|\s+$/g,"")});

	!function() {
		'use strict';

		Array.prototype.remove = function(index) {
			var ret = this[index];
			for (var i = index; i < this.length; i++) {
				this[i] = this[i+1];
			}
			this.length--;
			return ret;
		};

		angular
		.module('catnetUserConfig', [])
		.controller('configCtrl', configCtrl)
		.directive('search', searchDirective);

		searchDirective.$inject = ['$filter'];
		function searchDirective($filter) {
			return {
				restrict : 'A',
				scope : {
					value 		: '@searchValue',
					id 			: '@searchId',
					plural 		: '@searchPlural',
					placeholder : '@searchPlaceholder',
					selected 	: '=searchSelected',
					all 		: '=searchAll'
				},
				template : 
				'<div class="list-group">'+
				'	<input ng-repeat="v in deleted" type="hidden" name="deleted{{plural}}[]" ng-value="v[id]"/>'+
				'	<div ng-repeat="v in selected" class="list-group-item">'+
				'		<span ng-bind="v[value]"></span> '+
				'		<button type="button" ng-click="removeValue($index)" class="btn btn-danger btn-xs pull-right" role="remove"><i class="fa fa-trash"></i></button>'+
				'		<input type="hidden" name="user{{plural}}[]" ng-value="v[id]" />'+
				'		<input type="hidden" name="user{{plural+value.charAt(0).toUpperCase()+value.slice(1)}}[]" ng-value="v[value]"/>'+
				'	</div>'+
				'	<div class="list-group-item">'+
				'		<input type="text" class="form-control input-sm" ng-model="search" ng-change="filterValue()" placeholder="{{placeholder}}">'+
				'		<div class="list-group ng-hide" ng-show="search.length > 2">'+
				'			<button type="button" ng-click="addValue(\'new\', new_value)" class="list-group-item ng-hide" ng-show="(all | filter : value_filter : true).length < 1 && (selected | filter : value_filter : true).length < 1" ng-bind="new_value"></button>'+
				'			<button type="button" ng-click="addValue(v[id], v[value])" class="list-group-item" ng-repeat="v in all | filter : value_filter" ng-bind="v[value]"></button>'+
				'		</div>'+
				'	</div>'+
				'</div>',
				link : function($scope) {

					var initialValues 	= JSON.parse(JSON.stringify($scope.selected));
					$scope.deleted 		= [];
					$scope.search 		= '';
					$scope.new_value 	= null;
					$scope.value_filter = {};
					$scope.value_filter[$scope.value] = $scope.new_value;

					$scope.removeValue 	= removeValue;
					$scope.filterValue 	= filterValue;
					$scope.addValue 	= addValue;

					function removeValue(index) {
						if ($scope.selected[index][$scope.id] != 'new') {
							$scope.all.push($scope.selected[index]);

							var filter_object = {};
							filter_object[$scope.id] = $scope.selected[index][$scope.id];

							if ($filter('filter')(initialValues, filter_object, true).length > 0) {
								$scope.deleted.push($scope.selected[index]);
							}
						}
						$scope.selected.remove(index)
					}
					function filterValue() {
						$scope.search.length > 100 && ($scope.search = $scope.search.substr(0, 100));
						$scope.new_value = $scope.search.trim().toLowerCase();
						$scope.new_value = $scope.new_value.charAt(0).toUpperCase()+$scope.new_value.slice(1);
						$scope.value_filter[$scope.value] = $scope.new_value;
					}

					function addValue(id, v) {
						if (id == 'new') {
							var new_val 			= {};
							new_val[$scope.id] 		= 'new';
							new_val[$scope.value] 	= v;

							$scope.selected.push(new_val);
						}
						else{
							for (var i = 0; i < $scope.deleted.length; i++) {
								if ($scope.deleted[i][$scope.id] == id) {
									$scope.deleted.remove(i)
									break;
								}
							}

							var valueIndex;
							for (var i = 0; i < $scope.all.length; i++) {
								if ($scope.all[i][$scope.id] == id) {
									valueIndex = i;
									break;
								}
							}
							$scope.selected.push($scope.all[valueIndex]);
							$scope.all.remove(valueIndex);
						}
						$scope.search = '';
					}
				}
			}
		}

		configCtrl.$inject = ['$scope', '$timeout']
		function configCtrl($scope, $timeout) {
			$scope.hobbies 			= <?php echo escapeJsonString($hobbies, false); ?>;
			$scope.userHobbies 		= <?php echo escapeJsonString($userHobbies, false); ?>;
			$scope.transports 		= <?php echo escapeJsonString($transports, false); ?>;
			$scope.userTransports 	= <?php echo escapeJsonString($userTransports, false); ?>;
			$scope.careers 			= <?php echo escapeJsonString($careers, false); ?>;
			$scope.institutions 	= <?php echo escapeJsonString($institutions, false); ?>;
			$scope.institutions.push({ institution : '<?php echo $this->lang->line("general_other"); ?>', institutionId : 'new' })

			$scope.familyMembers 		= <?php echo escapeJsonString($familyMembers, false); ?>;
			$scope.familyRelations 		= <?php echo escapeJsonString($familyRelations, false); ?>;
			$scope.genders 				= <?php echo escapeJsonString($genders, false); ?>;

			$scope.deletedFamilyMembers = [];

			$scope.data = {
				zipCode 			: '<?php echo $zipCode; ?>',
				birthDate 			: '<?php echo $birthDate; ?>',
				dni 				: '<?php echo $dni; ?>',
				phoneAreaCode 		: '<?php echo $phoneAreaCode; ?>',
				phone 				: '<?php echo $phone; ?>',
				cellPhoneAreaCode 	: '<?php echo $cellPhoneAreaCode; ?>',
				cellPhone 			: '<?php echo $cellPhone; ?>'
			};

			$scope.removeFamilyMember 	= removeFamilyMember;
			$scope.addFamilyMember 		= addFamilyMember;

			function removeFamilyMember(i) {
				if ($scope.familyMembers[i].familyMemberId) {
					$scope.deletedFamilyMembers.push($scope.familyMembers[i]);
				}
				$scope.familyMembers.remove(i);
			}
			function addFamilyMember() {
				$scope.familyMembers.push({ birthDate : '', gender : null , familyRelationId : null})
				$timeout(function() {$('[role="date"]').datepicker({ language : 'es', autoclose : true });});

			}
			$timeout(function() {$('[role="date"]').datepicker({ language : 'es', autoclose : true })});



			$scope.userWorks 		= <?php echo escapeJsonString($userWorks, false); ?>;
			$scope.workTasks 		= <?php echo escapeJsonString($workTasks, false); ?>;

			$scope.deletedUserWorks = [];

			$scope.removeUserWork 	= removeUserWork;

			function removeUserWork(i) {
				if ($scope.userWorks[i].userWorkId) {
					$scope.deletedUserWorks.push($scope.userWorks[i]);
				}
				$scope.userWorks.remove(i);
			}

			$scope.addUserWork = addUserWork;
			function addUserWork() {
				$scope.userWorks.push({ workTastId : null, workDetail : null });
			}

			$scope.userStudies 		= <?php echo escapeJsonString($userStudies, false); ?>;
			$scope.studyLevels		= [];

			var studyLevels	= <?php echo escapeJsonString($studyLevels, false); ?>;
			var studyLevelsHasInstitution	= <?php echo escapeJsonString($studyLevelsHasInstitution, false); ?>;

			for(var k in studyLevels){
				$scope.studyLevels.push({ k : k, v : studyLevels[k], has_institution : studyLevelsHasInstitution[k]});
			}

			if ($scope.userStudies.length < $scope.studyLevels.length) {
				for (var i = 0; i < $scope.userStudies.length; i++) {
					if(typeof studyLevels[$scope.userStudies[i].level] != 'undefined'){
						$scope.userStudies[i].required = true;
						delete studyLevels[$scope.userStudies[i].level];
					}
				}
				for (var i in studyLevels) {
					$scope.userStudies.push({ status : null, level : i, careerId : null, institutionId : null, required : true });
				}
			}
			else{
				for (var i = 0; i < $scope.studyLevels.length; i++) {
					$scope.userStudies[i].required = true;
				}
			}

			$scope.studyStatues 	= <?php echo escapeJsonString($studyStatues, false); ?>;

			$scope.deletedUserStudies = [];

			$scope.removeUserStudy 	= removeUserStudy;

			function removeUserStudy(i) {
				if ($scope.userStudies[i].userStudyId) {
					$scope.deletedUserStudies.push($scope.userStudies[i]);
				}
				$scope.userStudies.remove(i);
			}

			$scope.addUserStudy = addUserStudy;
			function addUserStudy() {
				$scope.userStudies.push({ status : null, level : null, careerId : null, institutionId : null });
			}

			$scope.$watch('data.zipCode', validate($scope.data, 'zipCode', 4, 'number'));
			$scope.$watch('data.birthDate', validate($scope.data, 'birthDate', 10, 'dd/mm/yyyy'));
			$scope.$watch('data.dni', validate($scope.data, 'dni', 8, 'number'));
			$scope.$watch('data.phoneAreaCode', validate($scope.data, 'phoneAreaCode', 4, 'number'));
			$scope.$watch('data.phone', validate($scope.data, 'phone', 8, 'number'));
			$scope.$watch('data.cellPhoneAreaCode', validate($scope.data, 'cellPhoneAreaCode', 4, 'number'));
			$scope.$watch('data.cellPhone', validate($scope.data, 'cellPhone', 8, 'number'));

			$scope.validate = validate;
			function validate(data, field, length, type) {
				return function(n,old) {
					data[field] = data[field].length > length ? data[field].slice(0,length) : data[field];

					var regexp = /^.*$/;

					switch(type){
						case 'number' :
						regexp = /^[0-9]*$/;
						break;
						case 'dd/mm/yyyy' :
						regexp = /^[0-9/]{0,10}$/
						break;
					}
					data[field] = regexp.test(data[field]) ? data[field] : old;
				}
			}
		}
	}()

</script>