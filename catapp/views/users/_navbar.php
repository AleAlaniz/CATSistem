<ul class="nav nav-tabs">
	<?php if($this->Identity->Validate('users/details')) { ?>
	<li role="presentation" id="userNavDetails"><a href="/<?=FOLDERADD?>/users/details/<?=$userId;?>"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> <span class="hidden-xs"><?=$this->lang->line('administration_details');?></span></a></li>
	<?php } ?>
	<?php if($this->Identity->Validate('users/edit')) { ?>
	<li role="presentation" id="userNavEdit"><a href="/<?=FOLDERADD?>/users/edit/<?=$userId;?>"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> <span class="hidden-xs"><?=$this->lang->line('administration_edit');?></span></a></li>
	<?php } ?>
	<?php if($this->Identity->Validate('users/delete')) { ?>
	<li role="presentation" id="userNavDelete"><a href="/<?=FOLDERADD?>/users/delete/<?=$userId;?>"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> <span class="hidden-xs"><?=$this->lang->line('administration_delete');?></span></a></li>
	<?php } ?>
	<?php if($this->Identity->Validate('users/config/photo')) { ?>
	<li role="presentation" id="userNavconfigImages"><a href="/<?=FOLDERADD?>/users/userimages/<?=$userId?>"><span class="glyphicon glyphicon-picture" aria-hidden="true"></span> <span class="hidden-xs"><?=$this->lang->line('administration_configImages');?></span></a></li>
	<?php } ?>
</ul>
