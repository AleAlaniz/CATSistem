<div class="col-xs-12">
	<?php 
	if (isset($message)){ 
		?>
		<div class="alert alert-success" role="alert">
			<strong><i class="fa fa-check"></i></strong> 
			<?php echo $message ?>
		</div>
		<?php 
	}
	?>
	
	<!-- contenedor de los fondos -->
	<div class="panel panel-default">
		<div class="panel-heading">
			<strong><?=$this->lang->line('administration_users_selectbackground');?></strong>
		</div>
		<div class="panel-body row">
			<?php 
			foreach ($images as $image) {
				?>
				<div class="col-xs-6 col-md-4">
					<a href="/<?php echo FOLDERADD;?>/users/config?cmd=background&select=<?php echo $image->userBackgroundId;?>" class="thumbnail">
						<img src="/<?php echo APPFOLDERADD;?>/_users_backgrounds/<?php echo $image->name;?>">
					</a>

				</div>
				<?php 
			} 
			?>
		</div>
	<?php if(count($images)==0){ ?>
		<div class="bg-dark">
			<h1><?php echo $this->lang->line('background_empty')?></h1>
		</div>
	<?php } ?>
	</div>
	<script type="text/javascript">
		$('#nav_selectbackground').addClass('active');
	</script>