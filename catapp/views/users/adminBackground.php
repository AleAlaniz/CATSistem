<ol class="breadcrumb">
	<li><a href="/<?=FOLDERADD?>/administration"><?=$this->lang->line('general_administration');?></a></li>
	<li class="active"><?=$this->lang->line('general_backgrounds');?></li>
</ol>
<div class="col-xs-12" ng-app="background" ng-controller="Cbackgrounds">
	<?php 
	if (isset($message)){ 
		?>
		<div class="alert alert-success" role="alert">
			<strong><i class="fa fa-check"></i></strong> 
			<?php echo $message ?>
		</div>
		<?php 
	}
	?>
	<p>
	<?php if($this->Identity->Validate('background/manage')){?>
		<a ng-show ="showButton" ng-click="showAdd = true; showButton = false" class="btn btn-sm btn-success" ><i class="fa fa-plus"></i> <?=$this->lang->line('general_create');?></a>
	<?php }?>
	</p>
	<div class="panel panel-default">
		<div class="panel-heading">
			<strong><?=$this->lang->line('admin_background');?></strong>
				
		</div>
		<!-- contenedor de creacion de fondos -->
		<?php if($this->Identity->Validate('background/manage')){?>
		<div ng-show="!loading && showAdd" class="row ng-hide">
			<div class="col-md-8 well" style="margin-left:1em">
				<span class="btn btn-red btn-xs" ng-click="closePanel()"><i class="fa fa-times pull left"></i></span>
				<h1></h1>
				<label for="userfile"><?php echo $this->lang->line('background_add_new');?></label>
				<input class="" type="file" name="userfile" id="userfile" accept="image/gif, image/jpeg, image/png" required>

				<h2></h2>
				<a ng-click="addNewBackground()" ng-hide="adding" class="btn btn-green"><?php echo $this->lang->line('general_add');?></a>
				<span ng-show="adding"><i class="fa fa-spinner fa-spin fa-2x"></i></span>
			</div>
		</div>
		<?php }?>
		<div class="text-center" ng-show="loading">
			<i class='fa fa-refresh fa-spin fa-5x fa-fw dark'></i>
		</div>
		
		<!-- contenedor de los fondos -->
		<div ng-show="backs.length > 0" class="panel-body row ng-hide">
			<div class="col-xs-6 col-md-4" ng-repeat="b in backs">
				<?php if($this->Identity->Validate('background/manage')){?>
				<span ng-click="setDelete(b.userBackgroundId)" class="btn btn-xs btn-red m-r_10" data-target="#confirm-delete" data-toggle="modal" style="position: absolute;"><i class="fa fa-trash"></i></span><?php }?>
                <img ng-src='/<?php echo APPFOLDERADD;?>/_users_backgrounds/{{b.name}}' class="thumbnail">
			</div>
		</div>

		<div ng-show="!loading && backs.length == 0" class="bg-dark ng-hide">
			<h1><?php echo $this->lang->line('background_empty')?></h1>
		</div>

		<!-- modal de borrado -->
		<div class="modal animated shake open" tabindex="-1" id="confirm-delete">
			<div class="modal-dialog modal-sm ">
				<div class="modal-content">
					<div class="modal-body">
						<p><?php echo $this->lang->line('background_deleteareyousure'); ?></p>
						<div class="text-right">
							<span class="btn btn-white" data-dismiss="modal" aria-label="Close"><?php echo $this->lang->line('general_no'); ?></span>
							<span class="btn btn-lightgreen" ng-click="toDelete()" ng-show="!loading"><?php echo $this->lang->line('general_yes'); ?></span>
							<span class="btn btn-lightgreen disabled" ng-show="loading"><i class="fa fa-refresh fa-spin"></i></span>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- fin de modal -->
		<!-- modal de no completado -->
		<div class="modal fade" id="no-complete" tabindex="-1" role="dialog">
			<div class="modal-dialog modal-sm">
				<div class="modal-content">
					<div class="modal-body">
						<p><?php echo $this->lang->line('general_completeall')?> <strong class="text-danger">*</strong></p>
						<div class="text-right">
							<span class="btn btn-white" data-dismiss="modal" aria-label="Close"><?php echo $this->lang->line('general_accept'); ?></span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- script de angular para la vista -->
<script src="/<?php echo APPFOLDERADD; ?>/libraries/script/angular.min.js"></script>
<script type="text/javascript">
	$('#nav_background').addClass('active');
    angular
    .module('background',[])
    .controller('Cbackgrounds',["$scope","$http",($scope,$http)=> {
		const FOLDERADD = "/<?php echo FOLDERADD?>/";
		
		$scope.backs = [];
		$scope.loading = true;
		$scope.showAdd = false;
		$scope.showButton = true;

		$scope.closePanel = ()=>{
			if(document.getElementById("userfile")!= null){
				document.getElementById("userfile").value = "";			
			}
			$scope.showAdd = false;
			$scope.showButton = true;
		}

		function getBackgrounds(){
			$scope.loading = true;
			$http({
			method 	: 'POST',
			url 	: FOLDERADD+'background/get',
			}).success(function(data) {
				$scope.backs = data;
				$scope.loading = false;
			});
		}
		getBackgrounds();

		$scope.addNewBackground = ()=>{
			$scope.adding = true;
			var fd = new FormData();
			var files = document.getElementById('userfile').files[0];
			fd.append('userfile',files);

			$http.post(FOLDERADD+'background/create',fd,{
				transformRequest: angular.identity,
				headers: {'Content-Type': undefined,'Process-Data': false}
			}).success((data)=>{
				if(data.status == "success"){
					getBackgrounds();
					$scope.adding = false;
					$scope.closePanel();
					$scope.backAdded = true;
				}
				else
				{
					$scope.adding = false;
					$('#no-complete').modal('show');
				}
			});
		}

		$scope.setDelete = (id)=>{
			$scope.loading = false;
			$scope.deleteId = id;
		}

		$scope.toDelete = ()=>{
			$scope.loading = true;
			$http({
			method 	: 'POST',
			url 	: FOLDERADD+'background/delete',
			data 	: {deleteId : $scope.deleteId}
			}).success(function(data) {
				if(data.status == "success"){
					$('#confirm-delete').modal('hide');
					getBackgrounds();
					$scope.closePanel();
				}
			});
		}
	}]);
</script>