<div class="back-teaming" style="overflow: hidden;">
	<?php $header ?>
	<div class="panel panel-default" id="division-panel" style="height:auto;">
		<div class="panel-heading" id="admin-header">
			<?php echo $this->lang->line('teaming_institution_suggestions');?>
		</div>
		<div class="panel-body" id="teaming-paragraph">
			<div class="form-group">
				<span class="control-label"><?=$this->lang->line('Teaming_suggestion_message_body');?></span>
			</div>
			
				<div class="form-group">
					<label class="control-label"><?=$this->lang->line('teaming_suggest_name');?></label><span class="text-danger"><strong> *</strong></span>
					<input class="form-control" type="text" ng-model="institution.name" maxlength="150">
				</div>
				<div class="form-group">
					<label class="control-label"><?=$this->lang->line('administration_users_state');?></label><span class="text-danger"><strong> *</strong></span>
					<div>
						<select class="form-control input-sm" id="state" name="state" ng-model="institution.state" ng-options="state as state.stateName for state in states track by state.stateId">
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label"><?=$this->lang->line('administration_users_city');?></label><span class="text-danger"><strong> *</strong></span>
					<div>
						<select class="form-control input-sm" ng-model="institution.cityInState" ng-options="city as city.cityName for city in getCities(institution.state) | orderBy:'cityName'">
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label"><?=$this->lang->line('administration_users_address');?></label>
					<input class="form-control" type="text" ng-model="institution.address" maxlength="150">
				</div>
				<div class="form-group">
					<label class="control-label"><?=$this->lang->line('teaming_suggest_contact_data_social_network_web_page');?></label>
					<input class="form-control" type="text" ng-model="institution.email" maxlength="100">
				</div>
				<div class="form-group">
					<label class="control-label"><?=$this->lang->line('teaming_form_celnumber');?></label>
					<input class="form-control" type="text" ng-model="institution.number" maxlength="30" cel-number="celNumberInput()">
				</div>
				<div class="form-group">
					<label class="control-label"><?=$this->lang->line('teaming_suggest_contact_data_referrer');?></label>
					<input class="form-control" type="text" ng-model="institution.referrer" maxlength="150" alpha-input="alphaInput()">
				</div>
				<div class="form-group">
					<label class="control-label"><?=$this->lang->line('administration_items_description');?></label>
					<input class="form-control" type="text" ng-model="institution.description" maxlength="150">
				</div>
				<div ng-bind-html="errors | HtmlSanitize" style="text-align: left;"></div>
			
		</div>
		
	</div>
	<div class="buttons text-center">
		<button type="submit" ng-click="sendSuggestionData()" class="btn btn-default" id="first-try">
			<?=$this->lang->line('general_send')?>
		</button>
	</div>
	<div class="sep" style="margin-bottom:  120px">
		&nbsp;
	</div>
	<div id="noContactData" class="modal fade modal-sg" tabindex="-1" role="dialog">
		<div class="modal-dialogue">
			<div class="modal-content">
				<div class="modal-body">
					<div class="content-observation"> 
						<button id="exit-modal" data-dismiss="modal">&times;</button>
						<p><?php echo $this->lang->line('teaming_suggest_institution_no_contact_data');?></p>
					</div>
				</div>
			</div>
		</div>
	</div>   
</div>



	
	