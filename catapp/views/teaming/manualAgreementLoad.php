<link href="/<?php echo APPFOLDERADD; ?>/libraries/css/bootstrap-datepicker.css" rel="stylesheet">
<div class="back-teaming">
	<?php $header ?>
	<div class="container-fluid" id="reports-panel">
		<?php echo $navbar;?>
		<div class="panel panel-default" style="overflow:auto;">
			<div class="panel-heading" id="admin-header">
				<?=$this->lang->line('teaming_manual_join_explanation');?>
			</div>
			<div class="panel-body">
				<form>
					<table class="table table-responsive">
						<tbody>
							<tr>
								<td>
									<strong><?=$this->lang->line('teaming_form_name');?></strong>
								</td>
								<td>
									<div class="select-container" ng-click="selectClick($event);">
										<i class="fa fa-spin fa-refresh pull-right " style="position:absolute; top:13px;right:25px" ng-show="finding"></i>
										<span class="select-selected">
											<span class="select-selected-item" ng-repeat="user in users track by $index" ng-init="i=$index">
												<span cn-square-image cls="selected-item-image" url="/<?php echo FOLDERADD; ?>/users/profilephoto?userId={{user.userId}}&wah=200"></span>
												<span class="selected-item-label" ng-bind="user.completeName+' <'+user.userName+'>'" ></span>
												<span class="selected-item-delete no-selectable" ng-click="removeUser(i)">&times;</span>
											</span>
										</span>
										<input type="text" autocomplete="off" class="input-sm form-control select-input" ng-trim="true" ng-model="findLike" ng-change="searchUsers()" id="findInput" placeholder="<?php echo $this->lang->line('general_find');?>" alpha-input="alphaInput()"/>
									</div>
									<div class="select-options-container-super" ng-show="userOptions.length > 0">
										<div class="select-options-container">
											<div class="select-option no-selectable" ng-repeat="user in userOptions track by $index" ng-init="i=$index" ng-click="addUser(i)">
												<span cn-square-image cls="selected-option-image" url="/<?php echo FOLDERADD; ?>/users/profilephoto?userId={{user.userId}}&wah=200"></span>
												<span class="selected-option-label" ng-bind="user.completeName+' <'+user.userName+'>'"></span>
											</div>
										</div>
									</div>
								</td>
							</tr>
							<tr>
								<td>
									<strong><?=$this->lang->line('teaming_form_id');?></strong>
								</td>
								<td>

									<input class="form-control adhesion-dni" type="text" ng-model="agreeform.dni"  maxlength="8" only-numbers="onlyNumbers()">

								</td>     
							</tr>
							<tr>
								<td>
									<strong><?=$this->lang->line('teaming_form_company_id');?></strong>
								</td>
								<td>
									<input class="form-control adhesion-id" type="text" ng-model="agreeform.id"  maxlength="6" only-numbers="onlyNumbers()">
								</td>
							</tr>
							<tr>
								<td>
									<strong><?=$this->lang->line('teaming_form_amount');?></strong>
								</td>
								<td>
									<input class="form-control amount-qty" type="text" ng-model="agreeform.amount" maxlength="4" size="4" only-numbers="onlyNumbers()">
								</td>
							</tr>
							<tr>
								<td>
									<strong><?=$this->lang->line('teaming_form_amount_letters');?>
								</strong>
							</td>
							<td>
								<input class="form-control adhesion-amount-characters" type="text" ng-model="agreeform.amountCharacters" maxlength="46" alpha-input="alphaInput()">
							</td>
						</tr>
						<tr>
							<td>
								<strong><?=$this->lang->line('teaming_form_area_campaign');?>
							</strong>
						</td>
						<td>
							<input class="form-control" type="text" ng-model="agreeform.areaCampaign" maxlength="70" alpha-input="alphaInput()">
						</td>
					</tr>
					<tr>
						<td>
							<strong><?=$this->lang->line('teaming_form_celnumber');?>
						</strong>
					</td>
					<td>
						<input class="form-control mobile-number" type="text" ng-model="agreeform.mobileNumber" maxlength="20" cel-number="celNumberInput()">
					</td>
				</tr>
				<tr>
					<td>
						<strong><?=$this->lang->line('teaming_form_institution');?>
					</strong>
				</td>
				<td>
					<input class="form-control" type="text" ng-model="agreeform.institutionName" maxlength="70">
				</td>
			</tr>
			<tr style=" visibility: hidden;">
				<td>
					<strong>
						<?=$this->lang->line('teaming_form_load_type');?>
					</strong>
				</td>
				<td>
					<select class="form-control" ng-model="agreeform.loadType" style=" width: 102px; visibility: hidden;">
						<option><?=$this->lang->line('teaming_form_load_type_manual');?></option>
						<option><?=$this->lang->line('teaming_form_load_type_online');?></option>
					</select>
				</td>
			</tr>
		</tbody>
	</table>
	<div ng-bind-html="errorsAdd | HtmlSanitize" style="text-align: left;"></div>
</form>
</div>
</div>
<div class="div-buttons">
	<button type="submit" name="submit" ng-click="submit(agreeform)" class="btn btn-primary" id="first-try">
		<?=$this->lang->line('teaming_send');?>
	</button>
</div>
</div>


<script src="/<?php echo APPFOLDERADD; ?>/libraries/script/bootstrap-datepicker.min.js"></script>
<script src="/<?php echo APPFOLDERADD; ?>/libraries/script/locales/bootstrap-datepicker.es.min.js"></script>
<script>
	$('#manualAgreementLoad').replaceWith("<li><?php echo $this->lang->line('general_manual');?></li>");
</script>
</div>
