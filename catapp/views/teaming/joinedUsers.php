<link rel="stylesheet" type="text/css" href="/<?php echo APPFOLDERADD; ?>/libraries/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="/<?php echo APPFOLDERADD; ?>/libraries/css/buttons.dataTables.min.css">
<div class="back-teaming">
	<?php $header ?>
	<div class="container-fluid" id="discharge-panel">
		<div class="panel panel-default" style=" overflow: auto;">
			<div class="panel-heading" id="admin-header">
				<?=$this->lang->line('teaming_joined_table');?>
				<span class="badge pull-right">{{approvedAgreeSize}}</span>
			</div>
			<div class="ng-hide panel-body" ng-show="!loading">
				<table class="datatable table table-hover" id="table-discharge-users">
					<thead>
						<tr>
							<?php if ($this->Identity->Validate('teaming/joinedUsers')) {?>
							<th class="row-md-1"></th>
							<?php
						}
						?>
						<th class="row-md-1"><?=$this->lang->line('teaming_form_name');?></th>
						<th class="row-md-1"><?=$this->lang->line('teaming_form_id');?></th>
						<th class="row-md-1"><?=$this->lang->line('teaming_form_company_id');?></th>
						<th class="row-md-1"><?=$this->lang->line('administration_users_teaming_amount');?></th>
						<th class="row-md-1"><?=$this->lang->line('teaming_form_area_campaign');?></th>
						<th class="row-md-1"><?=$this->lang->line('teaming_form_celnumber');?></th>
						<th class="row-md-1"><?=$this->lang->line('teaming_form_institution');?></th>
						<th class="row-md-1"><?=$this->lang->line('administration_users_teaming_join_date');?></th>
					</tr>
				</thead>
				<tbody style="font-family: Segoe UI Symbol; word-break: break-word;">
					<tr ng-repeat="approvedAgreeForm in approvedAgreeForms" class="optionsUser">
						<?php
						if ($this->Identity->Validate('teaming/joinedUsers')) {?>
						<td class="row-md-1">
							<input type="checkbox" name="rowIdentifier" ng-model="approvedAgreeForm.value" ng-hide="enable" ng-value="true" ng-change="valueChange(approvedAgreeForm,$index)">
						</td>
						<?php
					}
					?>
					<td class="row-md-1">
						<span ng-model="approvedAgreeForm.userName">
							{{approvedAgreeForm.userName}}
						</span>
					</td>
					<td class="row-md-1">
						<span ng-model="approvedAgreeForm.dni">
							{{approvedAgreeForm.dni}}
						</span>
					</td>
					<td ng-if="approvedAgreeForm.lastId == null" class="row-md-1">
						<span ng-model="approvedAgreeForm.id">
							{{approvedAgreeForm.id}}
						</span>
					</td>
					<td ng-if="approvedAgreeForm.lastId != null" class="row-md-1">
						<span ng-model="approvedAgreeForm.id">
							{{approvedAgreeForm.lastId}}
						</span>
					</td>
					<td class="row-md-1">
						<span ng-model="approvedAgreeForm.lastAmount">
							{{approvedAgreeForm.lastAmount+' ('+approvedAgreeForm.lastAmountCharacters+')'}}
						</span>
					</td>
					<td class="row-md-1">
						<span ng-model="approvedAgreeForm.areaCampaign">
							{{approvedAgreeForm.areaCampaign}}
						</span>
					</td>
					<td class="row-md-1">
						<span ng-model="approvedAgreeForm.mobileNumber">
							{{approvedAgreeForm.mobileNumber}}
						</span>
					</td>
					<td class="row-md-1">
						<span ng-model="approvedAgreeForm.institutionName">
							{{approvedAgreeForm.institutionName}}
						</span>
					</td>
					<td class="row-md-1">
						<span ng-model="approvedAgreeForm.agreementDate">
							{{approvedAgreeForm.agreementDate}}
						</span>
					</td>
				</tbody>
			</table>
		</div>
	</div>
	<div class="text-center" ng-if="loading">
		<i class='fa fa-refresh fa-spin fa-5x fa-fw dark'></i>
	</div>
	<div ng-bind-html="errors | HtmlSanitize" style="text-align: center;"></div>
	
</div>
<div class="sep">
	&nbsp;
</div>
<script type="text/javascript" src="/<?php echo APPFOLDERADD; ?>/libraries/script/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="/<?php echo APPFOLDERADD; ?>/libraries/script/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" src="/<?php echo APPFOLDERADD; ?>/libraries/script/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="/<?php echo APPFOLDERADD; ?>/libraries/script/buttons.flash.min.js"></script>
<script type="text/javascript" src="/<?php echo APPFOLDERADD; ?>/libraries/script/buttons.html5.min.js"></script>
<script type="text/javascript" src="/<?php echo APPFOLDERADD; ?>/libraries/script/jszip.min.js"></script>
<div class="modal flat-style animated lightSpeedIn" id="removeAlert" role="dialog">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-body">
				<div class="content-observation" ng-show="show_observation"> 
					<p><?php echo $this->lang->line('teaming_modal_observation_text') ?></p> 
					*<textarea name="observation" class="form-control" rows="5" maxlength="500" ng-model="var_observation" autofocus></textarea> 
					<p ng-show="error_modal" style="color: red">El campo de la observación no debe ser vacío.</p> 
				</div> 
				<p><?php echo $this->lang->line('teaming_delete_are_you_sure') ?></p>
				<div class="text-right">
					<button  class="btn btn-white" data-dismiss="modal"><?= $this->lang->line('general_no') ?></button>
					<button ng-click="dischargeUser()" class="btn btn-lightgreen"><?= $this->lang->line('general_yes') ?></button>
				</div>
			</div>		
		</div>
	</div>
</div>
</div>
<script>
	$('#joinedUsers').replaceWith("<li><?php echo $this->lang->line('general_joined_users');?></li>");
</script>


