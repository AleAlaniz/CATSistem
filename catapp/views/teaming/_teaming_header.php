<ol class="breadcrumb">

	<li id="teaming" class="active"><a class="teaming-color" href="#/teaming"><?php echo $this->lang->line('general_teaming');?></a></li>
	
	<?php if ($this->Identity->Validate('teaming/divisions')) { ?>
		<li id="divisions" class="active"><a class="teaming-color" href="#/teaming/divisions"><?php echo $this->lang->line('general_sections');?></a></li>
	<?php }?>

	<?php if ($this->Identity->Validate('teaming/pendingforms')) { ?>
		<li id="pendingForms" class="active"><a class="teaming-color" href="#/teaming/pendingForms"><?php echo $this->lang->line('general_pending_forms');?></a></li>
	<?php }?>

	<?php if ($this->Identity->Validate('teaming/records')) { ?>
		<li id="records" class="active"><a class="teaming-color" href="#/teaming/records"><?php echo $this->lang->line('teaming_records');?></a></li>
	<?php }?>

	<?php if ($this->Identity->Validate('teaming/reports')) { ?>
		<li id="reports" class="active"><a class="teaming-color" href="#/teaming/reports"><?php echo $this->lang->line('general_comm_channel');?></a></li>
	<?php }?>

	<?php if ($this->Identity->Validate('teaming/comunnicationreports')) { ?>
		<li id="communicationReports" class="active"><a class="teaming-color" href="#/teaming/communicationReports"><?php echo $this->lang->line('general_comm_report');?></a></li>
	<?php }?>
	<?php if ($this->Identity->Validate('teaming/manual')) { ?>
		<li id="manualAgreementLoad" class="active">
			<a class="teaming-color" href="#/teaming/manualAgreementLoad">
				<?php echo $this->lang->line('general_manual');?>
			</a>
		</li>
	<?php }?>
	<?php if ($this->Identity->Validate('teaming/suggestions/viewall')) { ?>
	<li id="institutions" class="active">
		<a class="teaming-color" href="#/teaming/institutions">
			<?php echo $this->lang->line('general_institutions');?>
		</a>
	</li>
	<?php }
	else {?>
	<li id="institutions" class="active">
		<a class="teaming-color" href="#/teaming/institutions">
			<?php echo $this->lang->line('my_institutions');?>
		</a>
	</li>
	<?php }?>
</ol>

