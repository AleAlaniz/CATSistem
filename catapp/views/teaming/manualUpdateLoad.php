<link href="/<?php echo APPFOLDERADD; ?>/libraries/css/bootstrap-datepicker.css" rel="stylesheet">
<div class="back-teaming">
	<?php $header ?>
	<div class="container-fluid" id="reports-panel">
		<?php echo $navbar;?>
		<div class="panel panel-default" style=" overflow: auto;">
			<div class="panel-heading" id="admin-header">
				<?=$this->lang->line('teaming_manual_update_explanation');?>
			</div>
			<div class="panel-body">
				<form>
					<table class="table table-hover">
						<tbody>
							<tr>
								<td>
									<strong><?=$this->lang->line('teaming_form_name');?></strong>
								</td>
								<td>
									<div class="select-container" ng-click="selectClickUpdate($event);">
										<i class="fa fa-spin fa-refresh" style="position:absolute; top:13px;right:25px" ng-show="findingUpdate"></i>
										<span class="select-selected">
											<span class="select-selected-item" ng-repeat="user in usersUpdate track by $index" ng-init="i=$index">
												<span cn-square-image cls="selected-item-image" url="/<?php echo FOLDERADD; ?>/users/profilephoto?userId={{user.userId}}&wah=200"></span>
												<span class="selected-item-label" ng-bind="user.completeName+' <'+user.userName+'>'" ></span>
												<span class="selected-item-delete no-selectable" ng-click="removeUserUpdate(i)">&times;</span>
											</span>
										</span>
										<input type="text" autocomplete="off" class="input-sm form-control select-input" ng-trim="true" ng-model="findLikeUpdate" ng-change="searchUsersUpdate()" id="findInputUpdate" placeholder="<?php echo $this->lang->line('general_find');?>" alpha-input="alphaInput()"/>
									</div>
									<div class="select-options-container-super" ng-show="userOptionsUpdate.length > 0">
										<div class="select-options-container">
											<div class="select-option no-selectable" ng-repeat="user in userOptionsUpdate track by $index" ng-init="i=$index" ng-click="addUserUpdate(i)">
												<span cn-square-image cls="selected-option-image" url="/<?php echo FOLDERADD; ?>/users/profilephoto?userId={{user.userId}}&wah=200"></span>
												<span class="selected-option-label" ng-bind="user.completeName+' <'+user.userName+'>'"></span>
											</div>
										</div>
									</div>
								</td>
							</tr>
							<tr>
								<td>
									<strong><?=$this->lang->line('teaming_form_id');?></strong>
								</td>
								<td>
									<input class="form-control adhesion-dni" type="text" ng-model="updateform.dni"  maxlength="8" only-numbers="onlyNumbers()">
								</td>
							</tr>
							<tr>
								<td>
									<strong><?=$this->lang->line('teaming_form_company_id');?></strong>
								</td>
								<td>
									<input class="form-control adhesion-id" type="text" ng-model="updateform.id"  maxlength="6" only-numbers="onlyNumbers()">
								</td>
							</tr>
							<tr>
								<td>
									<strong><?=$this->lang->line('teaming_form_amount');?></strong>
								</td>
								<td>
									<input class="form-control amount-qty redify" id="amount-qty" ng-change="toBlack('#amount-qty')" type="text" ng-model="updateform.amount" maxlength="4" only-numbers="onlyNumbers()">
								</td>
							</tr>
							<tr>
								<td>
									<strong><?=$this->lang->line('teaming_form_amount_letters');?>
								</strong>
							</td>
							<td>
								<input class="form-control adhesion-amount-characters redify" id="amount-characters" ng-change="toBlack('#amount-characters')" type="text" ng-model="updateform.amountCharacters" maxlength="46" alpha-input="alphaInput()">
							</td>
						</tr>
						<tr>
							<td>
								<strong><?=$this->lang->line('teaming_form_area_campaign');?>
							</strong>
						</td>
						<td>
							<input class="form-control" type="text" ng-model="updateform.areaCampaign" maxlength="70" alpha-input="alphaInput()">
						</td>
					</tr>
					<tr>
						<td>
							<strong><?=$this->lang->line('teaming_form_celnumber');?>
						</strong>
					</td>
					<td>
						<input class="form-control mobile-number"type="text" ng-model="updateform.mobileNumber" maxlength="20" cel-number="celNumberInput()">
					</td>
				</tr>
				<tr>
					<td>
						<strong><?=$this->lang->line('teaming_form_institution');?>
					</strong>
				</td>
				<td>
					<input class="form-control" id="institution" ng-change="toBlack('#institution')" type="text" ng-model="updateform.institutionName" maxlength="70">
				</td>
			</tr>
			<tr style=" visibility: hidden;">
				<td>
					<strong>
						<?=$this->lang->line('teaming_form_load_type');?>
					</strong>
				</td>
				<td>
					<select class="form-control" ng-model="updateform.loadType" style=" width: 102px;">
						<option><?=$this->lang->line('teaming_form_load_type_manual');?></option>
						<option><?=$this->lang->line('teaming_form_load_type_online');?></option>
					</select>
				</td>
			</tr>
		</tbody>
	</table>
	<div ng-bind-html="errorsUpd | HtmlSanitize" style="text-align: left;"></div>
</form>
</div>
</div>
<div class="div-buttons">
	<button type="submit" name="submit" ng-click="submitUpdate(updateform)" class="btn btn-primary text-center" id="first-try">
		<?=$this->lang->line('teaming_send');?>
	</button>
</div>
</div>

<script>
	$('#manualAgreementLoad').replaceWith("<li><?php echo $this->lang->line('general_manual');?></li>");
</script>
</div>