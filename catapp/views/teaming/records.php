<link rel="stylesheet" type="text/css" href="/<?php echo APPFOLDERADD; ?>/libraries/css/dataTables.bootstrap.min.css">
<link href="/<?php echo APPFOLDERADD; ?>/libraries/css/bootstrap-datepicker.css" rel="stylesheet">
<div class="back-teaming">
	<?php $header ?>
	<div class="container-fluid" id="discharge-panel">
		<div class="panel panel-default" style=" overflow: auto;">
			<div class="panel-heading" id="admin-header">
				<?=$this->lang->line('teaming_joined_records');?>
				<div class="container-fluid pull-right">
					<button class="btn btn-default btn-xs" ng-click="showfilters = !showfilters">
						<span class="glyphicon glyphicon-filter"></span><span class="hidden-xs">&nbsp;<?=$this->lang->line('teaming_filters');?></span>
					</button>
					<button class="btn btn-default btn-xs" ng-click="showColumns = !showColumns">
						<span class="glyphicon glyphicon-eye-open"></span><span class="hidden-xs">&nbsp;<?=$this->lang->line('teaming_columns');?></span>
					</button>
				</div>
			</div>
			<div class="ng-hide panel-body" ng-show="!loading">
				<div class="filter-panel" ng-show="showfilters == true">
					<div class="form-group">
						<h5>
							<?=$this->lang->line('teaming_filters_selection');?>	
						</h5>
						<select class="dropdown" ng-options="filter.name for filter in filters| filter:showFilter" ng-model="selected" ng-change="addElement(selected)">
						</select>
					</div>
					<div class="form-group" ng-repeat="selectedFilter in filters" ng-show="selectedFilter.show==true">
						<div class="col-sm-1" style="width: 70px;">
							<button ng-click="selectedFilter.show=false;notSelectedFilter(selectedFilter)">&times;</button>
						</div>
						<div class="row form-group" ng-if="selectedFilter.type=='text'">
							<label class="col-sm-2 control-label" ng-bind="format(selectedFilter.name)"></label>
							<input type="text" class="col-sm-4" ng-model="activeFilter[selectedFilter.name]" ng-change="changeInput(activeFilter[selectedFilter.name],selectedFilter.column)">
						</div>
						<div class="row form-group" ng-if="selectedFilter.type=='num'">
							<label class="col-sm-2 control-label" ng-bind="format(selectedFilter.name)"></label>
							<input type="text" maxlength="{{selectedFilter.limit}}" ng-model="activeFilter[selectedFilter.name]" only-numbers="onlyNumbers()" ng-change="changeInput(activeFilter[selectedFilter.name],selectedFilter.column)">
						</div>
						<div class="row form-group" ng-if="selectedFilter.type=='date'">
							<label class="col-sm-2 control-label" ng-bind="format(selectedFilter.name)">
							</label>
							<div class="form-group">
								<input type="text" readonly="true" class="datepicker" id="{{selectedFilter.column}}init" ng-model="activeFilter[selectedFilter.name+' initDate']">
								<i class="fa fa-calendar"></i>
								<input type="text" readonly="true" class="datepicker" id="{{selectedFilter.column}}end" ng-model="activeFilter[selectedFilter.name+' endDate']">
							</div>
						</div>
						<div class="row form-group" ng-if="selectedFilter.type=='select'">
							<label class="col-sm-2 control-label" ng-bind="format(selectedFilter.name)"></label>
							<select ng-model="activeFilter[selectedFilter.name]" ng-options="filtervalue.value as filtervalue.name for filtervalue in selectedFilter.filterValues" ng-change="changeInput(activeFilter[selectedFilter.name],selectedFilter.column)">
							</select>
						</div>
						<div class="row form-group" ng-if="selectedFilter.type=='amount'">
							<label class="col-sm-2 control-label" ng-bind="format(selectedFilter.name)"></label>
							<div class="form-group">
								<input type="text"  maxlength="4" id="initAmount" ng-model="activeFilter[selectedFilter.name+' initAmount']" only-numbers="onlyNumbers()">
								<input type="text"  maxlength="4" id="endAmount" ng-model="activeFilter[selectedFilter.name+' endAmount']"  only-numbers="onlyNumbers()">
							</div>
						</div>
					</div>
				</div>
				
				<div class="filter-columns" ng-show="showColumns == true">
					<div class="form-group">
						<h5 class="text-left">
							<?=$this->lang->line('select_columns');?>	
						</h5>
						<button class="btn btn-default btn-xs" ng-repeat="filter in filters" data-toggle="button" ng-class="{active:filter.pressed}" ng-click="changeState($index)">
							{{filter.name}}
						</button>
					</div>
				</div>
				<table class="datatable table table-hover" id="table-joined-users">
					<thead>
						<tr>
							<th ng-repeat="filter in filters">			
								{{filter.name}}
							</th>
							<th class="row-md-1"></th>
						</tr>
					</thead>
					<tbody>
						<tr ng-repeat="approvedForm in approvedAgreeForms">
							<td class="row-md-1">
								<span ng-model="approvedForm.userName">
									{{approvedForm.userName}}
								</span>
							</td>
							<td class="row-md-1">
								<span ng-model="approvedForm.dni">
									{{approvedForm.dni}}
								</span>
							</td>
							<td class="row-md-1">
								<span ng-model="approvedForm.id">
									{{approvedForm.id}}
								</span>
							</td>
							<td class="row-md-1">
								<span ng-model="approvedForm.joinAmount">
									{{approvedForm.amount}}
								</span>
							</td>
							<td class="row-md-1">
								<span ng-model="approvedForm.areaCampaign">
									{{approvedForm.areaCampaign}}
								</span>
							</td>
							<td class="row-md-1">
								<span ng-model="approvedForm.mobileNumber">
									{{approvedForm.mobileNumber}}
								</span>
							</td>
							<td class="row-md-1">
								<span ng-model="approvedForm.institutionName">
									{{approvedForm.institutionName}}
								</span>
							</td>
							<td class="row-md-1">
								<span ng-model="approvedForm.agreementDate">
									{{approvedForm.agreementDate}}
								</span>
							</td>
							<td class="row-md-1">
								<span ng-model="approvedForm.email">
									{{approvedForm.email}}
								</span>
							</td>
							<td class="row-md-1">
								<span ng-model="approvedForm.birthDate">
									{{approvedForm.birthDate}}
								</span>
							</td>
							<td class="row-md-1">
								<span ng-model="approvedForm.gender">
									{{approvedForm.gender}}
								</span>
							</td>
							<td class="row-md-1">
								<span ng-model="approvedForm.civilState">
									{{approvedForm.civilState}}
								</span>
							</td>
							<td class="row-md-1">
								<span ng-model="approvedForm.site">
									{{approvedForm.site}}
								</span>
							</td>
							<td class="row-md-1">
								<span ng-model="approvedForm.turn">
									{{approvedForm.turn}}
								</span>
							</td>
							<td class="row-md-1">
								<span ng-model="approvedForm.socialReason">
									{{approvedForm.socialReason}}
								</span>
							</td>
							<td class="row-md-1">
								<span ng-model="approvedForm.actions">
									<a class="teaming-color" href="#/teaming/details/{{approvedForm.userId}}"><span class="glyphicon glyphicon-search" aria-hidden="true" ></span></a>&nbsp;
								</span>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		<div class="text-center" ng-if="loading">
			<i class='fa fa-refresh fa-spin fa-5x fa-fw dark'></i>
		</div>
	</div>
	
	<div class="sep">
		&nbsp;
	</div>
	<script src="/<?php echo APPFOLDERADD; ?>/libraries/script/bootstrap-datepicker.min.js"></script>
	<script src="/<?php echo APPFOLDERADD; ?>/libraries/script/locales/bootstrap-datepicker.es.min.js"></script>
	<script type="text/javascript" src="/<?php echo APPFOLDERADD; ?>/libraries/script/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="/<?php echo APPFOLDERADD; ?>/libraries/script/dataTables.bootstrap.min.js"></script>
	<script>
		$('#records').replaceWith("<li><?php echo $this->lang->line('teaming_records');?></li>");
	</script>

</div>


