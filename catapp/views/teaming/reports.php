<link href="/<?php echo APPFOLDERADD; ?>/libraries/css/bootstrap-datepicker.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="/<?php echo APPFOLDERADD; ?>/libraries/css/dataTables.bootstrap.min.css">
<div class="back-teaming">
	<?php $header ?>
	<div class="container-fluid" id="reports-panel">
		<?php echo $navbar;?>

		<div class="panel panel-default" style="overflow: auto;">
			<div class="panel-heading" id="admin-header">
				<?=$this->lang->line('teaming_joined_table_communication');?>
				<div class="container-fluid pull-right">
					<button class="btn btn-default btn-xs" ng-click="showfilters = !showfilters">
						<span class="glyphicon glyphicon-filter"></span><span class="hidden-xs">&nbsp;<?=$this->lang->line('teaming_filters');?></span>
					</button>
					<button class="btn btn-default btn-xs" ng-click="showColumns = !showColumns">
						<span class="glyphicon glyphicon-eye-open"></span><span class="hidden-xs">&nbsp;<?=$this->lang->line('teaming_columns');?></span>
					</button>
				</div>
			</div>
			<div class="ng-hide panel-body" ng-show="!loading">
				<div class="filter-panel" ng-show="showfilters == true">
					<div class="form-group">
						<h5>
							<?=$this->lang->line('teaming_filters_selection');?>	
						</h5>
						<select class="dropdown" ng-options="filter.name for filter in filters| filter:showFilter" ng-model="selected" ng-change="addElement(selected)">
						</select>
					</div>
					<div class="row" ng-repeat="selectedFilter in filters" ng-show="selectedFilter.show==true">
						<div class="col-sm-1 form-group" style="width: 70px;">
							<button class="btn btn-default btn-block" ng-click="selectedFilter.show=false;notSelectedFilter(selectedFilter)">&times;</button>
						</div>
						<div class="row form-group" ng-if="selectedFilter.type=='text'">
							<label class="col-sm-2 control-label" ng-bind="format(selectedFilter.name)"></label>
							<input type="text" class="col-sm-2" ng-model="activeFilter[selectedFilter.name]" ng-change="changeInput(activeFilter[selectedFilter.name],selectedFilter.column)">
						</div>
						<div class="row form-group" ng-if="selectedFilter.type=='num'">
							<label class="col-sm-2 control-label" ng-bind="format(selectedFilter.name)"></label>
							<input type="text" maxlength="{{selectedFilter.limit}}" ng-model="activeFilter[selectedFilter.name]" ng-change="changeInput(activeFilter[selectedFilter.name],selectedFilter.column)" only-numbers="onlyNumbers()">
						</div>
						<div class="row form-group" ng-if="selectedFilter.type=='date'">
							<label class="col-sm-2 control-label" ng-bind="format(selectedFilter.name)">
							</label>
							<div class="form-group">
								<input type="text" readonly="true"  class="datepicker" id="{{selectedFilter.column}}init" ng-model="activeFilter[selectedFilter.name+' initDate']">
								<i class="fa fa-calendar"></i>
								<input type="text" readonly="true" class="datepicker" id="{{selectedFilter.column}}end" ng-model="activeFilter[selectedFilter.name+' endDate']">
							</div>
						</div>
						<div class="row form-group" ng-if="selectedFilter.type=='select'">
							<label class="col-sm-2 control-label" ng-bind="format(selectedFilter.name)"></label>
							<select ng-model="activeFilter[selectedFilter.name]" ng-options="filtervalue.value as filtervalue.name for filtervalue in selectedFilter.filterValues" ng-change="changeInput(activeFilter[selectedFilter.name],selectedFilter.column)">
							</select>
						</div>
						<div class="row form-group" ng-if="selectedFilter.type=='amount'">
							<label class="col-sm-2 control-label" ng-bind="format(selectedFilter.name)"></label>
							<div class="form-group">
								<input type="text"  maxlength="4" id="initAmount" ng-model="activeFilter[selectedFilter.name+' initAmount']" only-numbers="onlyNumbers()">
								<input type="text"  maxlength="4" id="endAmount"  ng-model="activeFilter[selectedFilter.name+' endAmount']"  only-numbers="onlyNumbers()">
							</div>
						</div>
					</div>
				</div>
				<div class="filter-columns" ng-show="showColumns == true">
					<div class="form-group">
						<h5 class="text-left">
							<?=$this->lang->line('select_columns');?>	
						</h5>
						<button class="btn btn-default btn-xs" ng-repeat="filter in filters" data-toggle="button" ng-class="{active:filter.pressed}" ng-click="changeState($index)">
							{{filter.name}}
						</button>
					</div>
				</div>

				<table class="datatable table table-hover" id="table-joined-users">
					<thead>
						<tr>
							<th class="row-md-1"><input type="checkbox" name="rowIdentifier" ng-model="all" data-ng-click="checkAll()"></th>
							<th ng-repeat="filter in filters">			
								{{filter.name}}
							</th>
							<th>			
								Accion
							</th>
						</tr>
					</thead>
					<tbody>
						<tr ng-repeat="approvedForm in approvedAgreeForms">
							<td class="row-md-1">
								<input type="checkbox" name="rowIdentifier" ng-model="approvedForm.value" ng-value="true" ng-change="show(approvedForm)">
							</td>
							<td class="row-md-1">
								<span ng-model="approvedForm.userName">
									{{approvedForm.userName}}
								</span>
							</td>
							<td class="row-md-1">
								<span ng-model="approvedForm.dni">
									{{approvedForm.dni}}
								</span>
							</td>
							<td class="row-md-1">
								<span ng-model="approvedForm.id">
									{{approvedForm.id}}
								</span>
							</td>
							<td class="row-md-1">
								<span ng-model="approvedForm.joinAmount">
									{{approvedForm.amount}}
								</span>
							</td>
							<td class="row-md-1">
								<span ng-model="approvedForm.areaCampaign">
									{{approvedForm.areaCampaign}}
								</span>
							</td>
							<td class="row-md-1">
								<span ng-model="approvedForm.mobileNumber">
									{{approvedForm.mobileNumber}}
								</span>
							</td>
							<td class="row-md-1">
								<span ng-model="approvedForm.institutionName">
									{{approvedForm.institutionName}}
								</span>
							</td>
							<td class="row-md-1">
								<span ng-model="approvedForm.agreementDate">
									{{approvedForm.agreementDate}}
								</span>
							</td>
							<td class="row-md-1">
								<span ng-model="approvedForm.email">
									{{approvedForm.email}}
								</span>
							</td>
							<td class="row-md-1">
								<span ng-model="approvedForm.birthDate">
									{{approvedForm.birthDate}}
								</span>
							</td>
							<td class="row-md-1">
								<span ng-model="approvedForm.gender">
									{{approvedForm.gender}}
								</span>
							</td>
							<td class="row-md-1">
								<span ng-model="approvedForm.civilState">
									{{approvedForm.civilState}}
								</span>
							</td>
							<td class="row-md-1">
								<span ng-model="approvedForm.site">
									{{approvedForm.site}}
								</span>
							</td>
							<td class="row-md-1">
								<span ng-model="approvedForm.turn">
									{{approvedForm.turn}}
								</span>
							</td>
							<td class="row-md-1">
								<span ng-model="approvedForm.socialReason">
									{{approvedForm.socialReason}}
								</span>
							</td>
							<td class="row-md-1">
								<button class ="btn btn-success" ng-click="confirmDischarge(approvedForm,$index)" title="<?=$this->lang->line('teaming_division_discharge');?>">
									<i class="fa fa-arrow-down" aria-hidden="true"></i>

								</button>
								<button class ="btn btn-success" ng-click="print(approvedForm)" title="<?=$this->lang->line('teaming_division_print');?>">
									<i class="fa fa-print" aria-hidden="true"></i>
								</button>
							</td>

						</tr>
					</tbody>
				</table>
			</div>
		</div>
		<div class="text-center" ng-if="loading">
			<i class='fa fa-refresh fa-spin fa-5x fa-fw dark'></i>
		</div>
		<div ng-bind-html="errors | HtmlSanitize" style="text-align: left;"></div>
		<div class ="ng-hide report-buttons" ng-show="!loading">
			<button class ="btn btn-default" id="first-try" ng-click="sendCommunication('newNewsletter')">
				<?=$this->lang->line('teaming_send_newsletter');?>
			</button>
			<button class ="btn btn-default" id="first-try" ng-click="sendCommunication('newSurvey')">
				<?=$this->lang->line('teaming_send_survey');?>
			</button>
		</div>
	</div>
	<script src="/<?php echo APPFOLDERADD; ?>/libraries/script/bootstrap-datepicker.min.js"></script>
	<script src="/<?php echo APPFOLDERADD; ?>/libraries/script/locales/bootstrap-datepicker.es.min.js"></script>
	<script type="text/javascript" src="/<?php echo APPFOLDERADD; ?>/libraries/script/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="/<?php echo APPFOLDERADD; ?>/libraries/script/dataTables.bootstrap.min.js"></script>
	<script type="text/javascript" src="/<?php echo APPFOLDERADD; ?>/libraries/script/dataTables.select.min.js"></script>
	<script>
		$('#reports').replaceWith("<li><?php echo $this->lang->line('general_comm_channel');?></li>");
	</script>
	<div class="modal flat-style animated lightSpeedIn" id="removeAlert" role="dialog">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-body">
				<div class="content-observation" ng-show="show_observation"> 
					<p><?php echo $this->lang->line('teaming_modal_observation_text') ?></p> 
					*<textarea name="observation" class="form-control" rows="5" maxlength="500" ng-model="var_observation" autofocus></textarea> 
					<p ng-show="error_modal" style="color: red"><?php echo $this->lang->line('teaming_delete_not_empty')?></p> 
				</div> 
				<p><?php echo $this->lang->line('teaming_delete_are_you_sure') ?></p>
				<div class="text-right">
					<button  class="btn btn-white" data-dismiss="modal"><?= $this->lang->line('general_no') ?></button>
					<button ng-click="dischargeUser()" class="btn btn-lightgreen"><?= $this->lang->line('general_yes') ?></button>
				</div>
			</div>		
		</div>
	</div>
</div>
</div>




