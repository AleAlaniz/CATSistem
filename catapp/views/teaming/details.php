<div class="back-teaming" ng-controller='Cteaming'>
	<div class="container-fluid" style="margin-right: auto;margin-left: auto;">
		<div class="panel panel-default" id="panel-agreement-terms">
			<div class="panel-body" id="panel-agreement-terms-body">
				<div class="container-fluid">
					<img src="/<?php echo APPFOLDERADD; ?>/libraries/images/logo_teaming.png" class="teaming-small-terms pull-left"><img src="/<?php echo APPFOLDERADD; ?>/libraries/images/logo.png" class="cat-small-terms pull-right">
				</div>
				<h2 style="font-family: Segoe UI Symbol;"><?=$this->lang->line('teaming_user_details')?></h2>
				<div class="alert alert-danger" ng-if="!teamingData.isJoined">
					<span class="alert-not-joined"><?=$this->lang->line('teaming_user_not_joined');?></span>
				</div>
				<div class="container-fluid col-xs-12" style="font-family: Segoe UI Symbol;">
					<div class="col-xs-6">
						<h4  style="font-family: Segoe UI Symbol;"><?=$this->lang->line('teaming_user_details_teaming')?></h4>
						<div class="form-group">
							<span class="bold text-left">
								<?=$this->lang->line('teaming_form_name');?>
							</span>
							<span>
								{{teamingData.completeName}}
							</span>
						</div>
						<div class="form-group">
							<span class="bold text-left">
								<?=$this->lang->line('teaming_form_id');?>
							</span>
							<span>
								{{teamingData.dni}}
							</span>
						</div>
						<div class="form-group">
							<span class="bold text-left">
								<?=$this->lang->line('teaming_form_company_id');?>
							</span>
							<span>
								{{teamingData.id}}
							</span>
						</div>
						<div class="form-group">
							<span class="bold text-left">
								<?=$this->lang->line('teaming_form_area_campaign');?>
							</span>
							<span>
								{{teamingData.areaCampaign}}
							</span>
						</div>
						<div class="form-group">
							<span class="bold text-left">
								<?=$this->lang->line('teaming_form_celnumber');?>
							</span>
							<span>
								{{teamingData.mobileNumber}}
							</span>
						</div>
						<div class="form-group">
							<span class="bold text-left">
								<?=$this->lang->line('teaming_form_amount');?>
							</span>
							<span>
								{{teamingData.amount+' ('+teamingData.amountCharacters+') '}}
							</span><span class="bold text-left">

						</div>
						<div class="form-group">
							<span class="bold text-left">
								<?=$this->lang->line('administration_users_teaming_join_date');?>
							</span>
							<span>
								{{teamingData.agreementDate}}
							</span>
						</div>
					</div>
					<div class="col-xs-6">
						<h4  style="font-family: Segoe UI Symbol;"><?=$this->lang->line('teaming_user_details_personal')?></h4>
						<div class="form-group">
							<span class="bold text-left">
								<?=$this->lang->line('users_gender');?>
							</span>
							<span>
								{{personalData.gender}}
							</span>
						</div>
						<div class="form-group">
							<span class="bold text-left">
								<?=$this->lang->line('users_birthdate');?>
							</span>
							<span>
								{{personalData.birthDate}}
							</span>
						</div>
						<div class="form-group">
							<span class="bold text-left">
								<?=$this->lang->line('administration_users_email');?>
							</span>
							<span>
								{{personalData.email}}
							</span>
						</div>
						<div class="form-group">
							<span class="bold text-left">
								<?=$this->lang->line('users_civilstate');?>
							</span>
							<span>
								{{personalData.civilState}}
							</span>
						</div>
						<div class="form-group">
							<span class="bold text-left">
								<?=$this->lang->line('teaming_form_site');?>
							</span>
							<span>
								{{personalData.site}}
							</span>
						</div>
						<div class="form-group">
							<span class="bold text-left">
								<?=$this->lang->line('users_turn');?>
							</span>
							<span>
								{{personalData.turn}}
							</span>
						</div>
					</div>
				</div>
				<div class="container-fluid">
					<h2 style="font-family: Segoe UI Symbol;"><?=$this->lang->line('teaming_user_details_history')?></h2>
					<div class="row" style="font-family: Segoe UI Symbol;">
						<table class="table table responsive">
							<thead>
								<tr>
									<th></th>
									<th></th>
								</tr>
							</thead>
							<tbody>
								<tr ng-repeat="entry in teamingHistory">
									<td class="bold text-left">
										<span>{{entry.date}}</span>
									</td>
									<td class="align-left">
										<span>{{entry.action}}</span>
										<div ng-bind-html="format(entry.obtainedData) | HtmlSanitize"></div>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="buttons">
		<a class="btn btn-default" id="first-try"  style="padding-top: 30px;" href="#/teaming/records">
			<i class="fa fa-chevron-left"></i>
			<?=$this->lang->line('general_goback');?>
		</a>
		<button class="btn btn-default" id="first-try" ng-click="savePdf('/<?php echo FOLDERADD; ?>/teaming/generatePdf/')">
			<i class="fa fa-file-pdf-o"></i>
			<?=$this->lang->line('teaming_user_details_export_pdf');?>
		</button>
		<a class="btn btn-default" id="first-try" style="padding-top: 30px;" href="/<?php echo FOLDERADD; ?>/teaming/generateExcel/{{teamingData.userId}}">
			<i class="fa fa-file-excel-o"></i>
			<?=$this->lang->line('teaming_user_details_export_excel');?>
		</a>
	</div>
	<div class="sep" style="margin-bottom:  120px">
		&nbsp;
	</div>