<link rel="stylesheet" type="text/css" href="/<?php echo APPFOLDERADD; ?>/libraries/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="/<?php echo APPFOLDERADD; ?>/libraries/css/buttons.dataTables.min.css">
<!--Al parecer no se usa en ningún lado-->
<div class="back-teaming">
	<ol class="breadcrumb">
		<li class="active"><a class="teaming-color" href="#/teaming"><?=$this->lang->line('general_teaming');?></a></li>
		<li class="active"><a class="teaming-color" href="#/teaming/divisions"><?=$this->lang->line('general_sections');?></a></li>
		<li class="active"><a class="teaming-color" href="#/teaming/pendingForms"><?=$this->lang->line('general_pending_forms');?></a></li>
		<li class="active"><?=$this->lang->line('general_joined_users');?></li>
		<li class="active"><a class="teaming-color" href="#/teaming/records"><?=$this->lang->line('teaming_records');?></a></li>
		<li class="active"><a class="teaming-color" href="#/teaming/reports"><?=$this->lang->line('general_comm_channel');?></a></li>
		<li class="active"><a class="teaming-color" href="#/teaming/communicationReports"><?=$this->lang->line('general_comm_report');?></a></li>
		<li class="active"><a class="teaming-color" href="#/teaming/manualAgreementLoad"><?=$this->lang->line('general_manual');?></a></li>
	</ol>
	<div class="container-fluid" id="discharge-panel">
		<?php echo $navbar;?>
		<div class="panel panel-default" style=" overflow: auto;">
			<div class="panel-heading" id="admin-header">
				<?=$this->lang->line('teaming_joined_update_record_descrip');?>
				<span class="badge pull-right">{{approvedAgreeSize}}</span>
			</div>
			<div class="panel-body">
				<table class=" table table-hover" id="table-discharge-users">
					<thead>
						<tr>
							<th class="row-md-1"><?=$this->lang->line('teaming_form_name');?></th>
							<th class="row-md-1"><?=$this->lang->line('teaming_form_original_amount');?></th>
							<th class="row-md-1"><?=$this->lang->line('teaming_joined_update_record');?></th>
						</tr>
					</thead>
					<tbody style="font-family: Segoe UI Symbol;">
						<tr ng-repeat="updateReport in updateReports">
							<td class="row-md-1">
								<span ng-model="updateReport.userName">
									{{updateReport.userName}}
								</span>
							</td>
							<td class="row-md-1">
								<span ng-model="updateReport.userOriginalAmount">
									{{updateReport.userOriginalAmount+' ('+updateReport.userOriginalAmountCharacters+')'}}
								</span>
							</td>
							<td class="row-md-1">
								<div ng-if="hasNoStory(updateReport.userStory)">
									<span class="text"> <?=$this->lang->line('teaming_form_no_story');?></span>
								</div>
								<div ng-repeat="story in updateReport.userStory">
									<button type="button" class="btn btn-default story-button" style="margin:10px;" data-toggle="collapse" data-target="#demo{{$parent.$index+($index+1)}}">
										{{story.updateDate}}
										<div id="demo{{$parent.$index+($index+1)}}" class="collapse"  style="text-align: left;">
											<span style="color:white;">
												<?=$this->lang->line('teaming_form_id');?>
											</span>
											{{format(story.dni)}}
											<br>
											<span style="color:white;">
												<?=$this->lang->line('teaming_form_company_id');?>
											</span>
											{{format(story.id)}}
											<br>
											<span style="color:white;">
												<?=$this->lang->line('teaming_form_amount');?>
											</span>
											{{format(story.updateAmount)}}
											<br>
											<span style="color:white;">
												<?=$this->lang->line('teaming_form_amount');?>
											</span>
											{{format(story.updatedAmountCharacters)}}
											<br>
											<span style="color:white;">
												<?=$this->lang->line('teaming_form_area_campaign');?>
											</span>
											{{format(story.areaCampaign)}}
											<br>
											<span style="color:white;"> 
												<?=$this->lang->line('teaming_form_celnumber');?>
											</span>
											{{format(story.mobileNumber)}}
											<br>
											<span style="color:white;">
												<?=$this->lang->line('teaming_form_institution');?>
											</span>
											{{format(story.institutionName)}}
											<br>
										</div>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="sep">
			&nbsp;
		</div>
		<script type="text/javascript" src="/<?php echo APPFOLDERADD; ?>/libraries/script/jquery.dataTables.min.js"></script>
		<script type="text/javascript" src="/<?php echo APPFOLDERADD; ?>/libraries/script/dataTables.bootstrap.min.js"></script>
		<script type="text/javascript" src="/<?php echo APPFOLDERADD; ?>/libraries/script/dataTables.buttons.min.js"></script>
		<script type="text/javascript" src="/<?php echo APPFOLDERADD; ?>/libraries/script/buttons.flash.min.js"></script>
		<script type="text/javascript" src="/<?php echo APPFOLDERADD; ?>/libraries/script/buttons.html5.min.js"></script>
		<script type="text/javascript" src="/<?php echo APPFOLDERADD; ?>/libraries/script/jszip.min.js"></script>
	</div>


