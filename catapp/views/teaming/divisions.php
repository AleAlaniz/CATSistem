<div class="back-teaming" style="overflow: hidden;">
	<?php $header ?>
	<div class="panel panel-default" id="division-panel">
		<div class="panel-heading" id="admin-header">
			<?php echo $this->lang->line('teaming_divisions_available'); ?>
		</div>
		<div class="panel-body" id="division-body">
			<form id="form-admin">
				<div class="form-group" ng-repeat="section in sections">
					<input type="checkbox" ng-model="sections[$index].sectionValue" ng-checked="sections[$index].sectionValue == true"">
					<span class="returned-string" ng-bind-html="sections[$index].name | HtmlSanitize"></span>
				</div>
			</form>
		</div>

	</div>
	<div class="text-center form-group returned-string" ng-bind-html="updateSuccess | HtmlSanitize"></div>
	<div class="buttons text-center">
		<button type="submit" name="submitDivision" ng-click="submitDivision()" class="btn btn-default" id="first-try" ng-show="showSaveButton==true">
			<?=$this->lang->line('general_save')?>
		</button>
	</div>
	<div class="sep" style="margin-bottom:  120px">
		&nbsp;
	</div>
	<script>
		$('#divisions').replaceWith("<li><?php echo $this->lang->line('general_sections');?></li>");
	</script>
