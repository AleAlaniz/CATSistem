<div class="back-teaming" style="overflow: hidden;">
	<div class="container-fluid" id="reports-panel">
		<div class="panel panel-default" id="division-panel" style="height:auto;">
			<div class="panel-heading" id="admin-header">
				<span ng-model="suggestion.name" ng-hide="editSelected">
					{{suggestion.name}}	
				</span>
				<div class="form-horizontal">
					<input type="text" class="form-control input-sm" ng-show="editSelected" ng-model="suggestion.name" maxlength="150">
				</div>
			</div>
			<div class="panel-body" style="font-family: 'Segoe UI Symbol';">
				<div class="form-horizontal">
					<div class="form-group">
						<label class="col-sm-3">
							<?=$this->lang->line('administration_users_state');?>:<span class="text-danger" ng-show="editSelected"><strong> *</strong></span>	
						</label>
						<div class="col-sm-8">
							<span ng-model="suggestion.province" ng-hide="editSelected">
								{{suggestion.province}}
							</span>
							<select class="form-control input-sm" ng-show="editSelected" id="state" name="state" ng-model="suggestion.newState" ng-options="state as state.stateName for state in states track by state.stateId">
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3">
							<?=$this->lang->line('administration_users_city');?>:<span class="text-danger" ng-show="editSelected"><strong> *</strong></span>	
						</label>
						<div class="col-sm-8">
							<span ng-model="suggestion.locality" ng-hide="editSelected">
								{{suggestion.locality}}
							</span>
							<select class="form-control input-sm" ng-show="editSelected" ng-model="suggestion.newCity" ng-options="city as city.cityName for city in getCities(suggestion.newState) | 		orderBy:'cityName'">
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3">
							<?=$this->lang->line('administration_users_address');?>:
						</label>
						<div class="col-sm-8">
							<span ng-model="suggestion.address" ng-hide="editSelected">
								{{suggestion.address}}
							</span>
							<input type="text" class="form-control input-sm" ng-show="editSelected" ng-model="suggestion.address" maxlength="150">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3">
							<?=$this->lang->line('teaming_suggest_contact_data_social_network_web_page');?>:
						</label>
						<div class="col-sm-8">
							<span ng-model="suggestion.email" ng-hide="editSelected">
								{{suggestion.email}}
							</span>
							<input type="text" class="form-control input-sm" ng-show="editSelected" ng-model="suggestion.email" maxlength="100">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3">
							<?=$this->lang->line('teaming_form_celnumber');?>:
						</label>
						<div class="col-sm-8">
							<span ng-model="suggestion.number" ng-hide="editSelected">
								{{suggestion.number}}
							</span>
							<input type="text" class="form-control input-sm" ng-show="editSelected" ng-model="suggestion.number" maxlength="30" cel-number="celNumberInput()">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3">
							<?=$this->lang->line('teaming_suggest_contact_data_referrer');?>:
						</label>
						<div class="col-sm-8">
							<span ng-model="suggestion.referrer" ng-hide="editSelected">
								{{suggestion.referrer}}
							</span>
							<input type="text" class="form-control input-sm" ng-show="editSelected" ng-model="suggestion.referrer" maxlength="150" alpha-input="alphaInput()">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3">
							<?=$this->lang->line('administration_items_description');?>:
						</label>
						<div class="col-sm-8">
							<span ng-model="suggestion.description" ng-hide="editSelected">
								{{suggestion.description}}
							</span>
							<input type="text" class="form-control input-sm" ng-show="editSelected" ng-model="suggestion.description" maxlength="200">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3">
							<?php echo $this->lang->line('teaming_suggestion_date'); ?>:
						</label>
						<div class="col-sm-8">
							<span ng-model="suggestion.date">
								{{suggestion.date}}
							</span>
						</div>
					</div>
				</div>
				<div ng-bind-html="errors | HtmlSanitize" style="text-align: left;"></div>
				<div ng-bind-html="success | HtmlSanitize" style="text-align: left; color: green;"></div>
			</div>
		</div>
		<!--habilitar segun permiso-->
		<div class="btn-toolbar center-block" style="display: table;">
			<div class="btn-group" role="group" aria-label="First group">
				<a class="btn btn-primary" style="padding-top: 30px;" id="first-try" href="#/teaming/institutions" >
					<?php echo $this->lang->line("general_goback")?>
				</a>
			</div>
			<?php if ($this->Identity->Validate('teaming/suggestions/edit')) { ?>	
				<div class="btn-group" role="group" aria-label="First group">
					<button class="btn btn-primary" id="first-try"  ng-hide="editSelected ==  true"  ng-click="editSelected = true; ">
						<?php echo $this->lang->line("general_edit")?>
					</button>
				</div>
				<div class="btn-group" role="group" aria-label="First group">
					<button class="btn btn-primary" id="first-try"  ng-show="editSelected == true"  ng-click="updatesuggestion(); editSelected = false; ">
						<?php echo $this->lang->line("general_accept")?>
					</button>
				</div>
				<div class="btn-group" role="group" aria-label="First group">
					<button class="btn btn-primary" id="first-try"  ng-show="editSelected == true"  ng-click="restore(); editSelected = false; ">
						<?php echo $this->lang->line("general_cancel")?>
					</button>
				</div>
			<?php } ?>
		</div>
</div>
<div id="noContactData" class="modal fade modal-sg" tabindex="-1" role="dialog">
	<div class="modal-dialogue">
		<div class="modal-content">
			<div class="modal-body">
				<div class="content-observation"> 
					<button id="exit-modal" data-dismiss="modal">&times;</button>
					<p><?php echo $this->lang->line('teaming_suggest_suggestion_no_contact_data');?></p>
				</div>
			</div>
		</div>
	</div>
</div> 