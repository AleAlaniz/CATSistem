<div class="back-teaming">
	<?php $header ?>
	<div class="container-fluid" style="margin-right: auto;margin-left: auto; margin-bottom: 0;">
		<img class="img-responsive pull-left small-heart" src="/<?php echo APPFOLDERADD; ?>/libraries/images/corazon_chico.png">
		<img class="img-responsive center-block teaming-logo" src="/<?php echo APPFOLDERADD; ?>/libraries/images/logo_teaming.png">
		<div class="panel panel-default center-block" id="panel-teaming">
			<div class="panel-body" id="teaming-description">
				<div class="teaming-paragraph">
					<br>
					<?=$this->lang->line('teaming_intro_first_paragraph');?> 
					<p class="second-par">
						<?=$this->lang->line('teaming_intro_second_paragraph');?> 
					</p><br>
					<p class="third-par">
						<?=$this->lang->line('teaming_intro_third_paragraph');?>
					</p><br><br> 
					<p class="bottom-teaming">
						<?=$this->lang->line('teaming_intro_last_paragraph');?> 
					</p><br>
					<p class="shadow">&nbsp;</p>
				</div>
			</div>
		</div>

		<div class="buttons" ng-if="finishedCharging == true" style="margin-top: -72px;">
			<a href="#/teaming/agreement" class="btn btn-default" id="first-try" ng-show="!alreadyJoined" style="line-height: 62px;">
				<?=$this->lang->line('teaming_agreement');?>
			</a>
			<a href="#/teaming/update" class="btn btn-default" id="first-try" ng-show="petitionApproved && !PendingUpdate">
				<?=$this->lang->line('teaming_update');?>
			</a>
			<a type="submit" name="submit"  class="btn btn-primary" id="first-try" href="#/teaming/sendSuggestion">
				<?=$this->lang->line('teaming_suggest_institution');?>
			</a>
		</div>
		<img class="img-responsive pull-right" id="big-heart" src="/<?php echo APPFOLDERADD; ?>/libraries/images/corazon_grande.png">
		<div class="sep">
			&nbsp;
		</div>
	</div>
</div>
<script>
	$('#teaming').replaceWith("<li><?php echo $this->lang->line('general_teaming');?></li>");
</script>
