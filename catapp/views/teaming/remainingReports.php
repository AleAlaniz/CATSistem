<link href="/<?php echo APPFOLDERADD; ?>/libraries/css/bootstrap-datepicker.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="/<?php echo APPFOLDERADD; ?>/libraries/css/dataTables.bootstrap.min.css">
<div class="back-teaming">
	<?php $header ?>
	<div class="container-fluid" id="reports-panel">
		<?php echo $navbar;?>

		<div class="panel panel-default" style="overflow: auto; ">
			<div class="panel-heading" id="admin-header">
				<?=$this->lang->line('teaming_not_joined_table_communication');?>
				<div class="container-fluid pull-right">
					<button class="btn btn-default btn-xs" ng-click="showfilters = !showfilters">
						<span class="glyphicon glyphicon-filter"></span><span class="hidden-xs">&nbsp;<?=$this->lang->line('teaming_filters');?></span>
					</button>
				</div>
			</div>
			<div class="ng-hide panel-body" ng-show="!loading">
				<div class="filter-panel" ng-show="showfilters == true">
					<div class="form-group">
						<h5>
							<?=$this->lang->line('teaming_filters_selection');?>	
						</h5>
						<select class="dropdown" ng-options="filter.name for filter in filters| filter:showFilter" ng-model="selected" ng-change="addElement(selected)">
						</select>
					</div>
					<div class="row" ng-repeat="selectedFilter in filters" ng-show="selectedFilter.show==true">
						<div class="col-sm-1 form-group" style="width: 70px;">
							<button class="btn btn-default btn-block" ng-click="selectedFilter.show=false;notSelectedFilter(selectedFilter)">&times;</button>
						</div>
						<div class="row form-group" ng-if="selectedFilter.type=='text'">
							<label class="col-sm-2 control-label" ng-bind="format(selectedFilter.name)"></label>
							<input type="text" class="col-sm-4" ng-model="activeFilter[selectedFilter.name]" ng-change="changeInput(activeFilter[selectedFilter.name],selectedFilter.column)">
						</div>
						<div class="row form-group" ng-if="selectedFilter.type=='num'">
							<label class="col-sm-2 control-label" ng-bind="format(selectedFilter.name)"></label>
							<input type="text" maxlength="{{selectedFilter.limit}}" ng-model="activeFilter[selectedFilter.name]" only-numbers="onlyNumbers()" ng-change="changeInput(activeFilter[selectedFilter.name],selectedFilter.column)">
						</div>
						<div class="row form-group" ng-if="selectedFilter.type=='date'">
							<label class="col-sm-2 control-label" ng-bind="format(selectedFilter.name)">
							</label>
							<div class="form-group">
								<input type="text" readonly="true"  class="datepicker" id="{{selectedFilter.column}}init" ng-model="activeFilter[selectedFilter.name+' initDate']">
								<i class="fa fa-calendar"></i>
								<input type="text" readonly="true" class="datepicker" id="{{selectedFilter.column}}end" ng-model="activeFilter[selectedFilter.name+' endDate']">
							</div>
						</div>
						<div class="row form-group" ng-if="selectedFilter.type=='select'">
							<label class="col-sm-2 control-label" ng-bind="format(selectedFilter.name)"></label>
							<select ng-model="activeFilter[selectedFilter.name]" ng-options="filtervalue.value as filtervalue.name for filtervalue in selectedFilter.filterValues" ng-change="changeInput(activeFilter[selectedFilter.name],selectedFilter.column)">
							</select>
						</div>
					</div>
				</div>
				<table class="datatable table table-hover" id="table-joined-users">
					<thead>
						<tr>
							<th class="row-md-1"><input type="checkbox" name="rowIdentifier" ng-model="all" data-ng-click="checkAll()"></th>
							<th ng-repeat="filter in filters">			
								{{filter.name}}
							</th>
						</tr>
					</thead>
					<tbody>
						<tr ng-repeat="remainingUser in remainingUsers" class="optionsUser">
							<td class="row-md-1">
								<input type="checkbox" name="rowIdentifier" ng-model="remainingUser.value" ng-value="true" ng-change="show(remainingUser)">
							</td>
							<td class="row-md-1">
								<span ng-model="remainingUser.userName">
									{{remainingUser.userName}}
								</span>
							</td>
							<td class="row-md-1">
								<span ng-model="remainingUser.id">
									{{remainingUser.dni}}
								</span>
							</td>
							<td class="row-md-1">
								<span ng-model="remainingUser.email">
									{{remainingUser.email}}
								</span>
							</td>
							<td class="row-md-1">
								<span ng-model="remainingUser.birthDate">
									{{remainingUser.birthDate}}
								</span>
							</td>
							<td class="row-md-1">
								<span ng-model="remainingUser.gender">
									{{remainingUser.gender}}
								</span>
							</td>
							<td class="row-md-1">
								<span ng-model="remainingUser.civilState">
									{{remainingUser.civilState}}
								</span>
							</td>
							<td class="row-md-1">
								<span ng-model="remainingUser.site">
									{{remainingUser.site}}
								</span>
							</td>
							<td sclass="row-md-1">
								<span ng-model="remainingUser.turn">
									{{remainingUser.turn}}
								</span>
							</td>
							<td sclass="row-md-1">
								<span ng-model="remainingUser.socialReason">
									{{remainingUser.socialReason}}
								</span>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		<div class="text-center" ng-if="loading">
			<i class='fa fa-refresh fa-spin fa-5x fa-fw dark'></i>
		</div>
		<div ng-bind-html="errors | HtmlSanitize" style="text-align: left;"></div>
		<div class ="ng-hide report-buttons" ng-show="!loading">
			<button class ="btn btn-default" id="first-try" ng-click="sendCommunication('newNewsletter')">
				<?=$this->lang->line('teaming_send_newsletter');?>
			</button>
			<button class ="btn btn-default" id="first-try" ng-click="sendCommunication('newSurvey')">
				<?=$this->lang->line('teaming_send_survey');?>
			</button>
		</div>
	</div>
	<script src="/<?php echo APPFOLDERADD; ?>/libraries/script/bootstrap-datepicker.min.js"></script>
	<script src="/<?php echo APPFOLDERADD; ?>/libraries/script/locales/bootstrap-datepicker.es.min.js"></script>
	<script type="text/javascript" src="/<?php echo APPFOLDERADD; ?>/libraries/script/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="/<?php echo APPFOLDERADD; ?>/libraries/script/dataTables.bootstrap.min.js"></script>
	<script type="text/javascript" src="/<?php echo APPFOLDERADD; ?>/libraries/script/dataTables.select.min.js"></script>
	<script>
		$('#reports').replaceWith("<li><?php echo $this->lang->line('general_comm_channel');?></li>");
	</script>
</div>



