<link href="/<?php echo APPFOLDERADD; ?>/libraries/css/bootstrap-datepicker.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="/<?php echo APPFOLDERADD; ?>/libraries/css/dataTables.bootstrap.min.css">

<div class="back-teaming">
	<?php $header ?>
	<div class="container-fluid" style="margin-bottom: 200px;">
		<div class="adhesion-panel">
			<div class="panel panel-default">
				<div class="panel panel-heading" id="admin-header">
					<?=$this->lang->line('teaming_division_add_table');?>
				</div>									    	
				<div class="ng-hide panel panel-body" ng-show="!loading">
					<?php echo validation_errors(); ?>
					<div class="teaming-paragraph" ng-if="agreeFormsNum == 0">
						<label><?php echo $this->lang->line('teaming_join_no_forms')?></label>
					</div>
					<table class="datatable-join table table-responsive" id="adm-tbl" ng-if="agreeFormsNum > 0">
						<thead>
							<tr>
								<th class="row-md-1">
									&nbsp;
								</th>
								<th class="row-md-1">
									<?=$this->lang->line('teaming_form_name');?>
								</th>
								<th class="row-md-1">
									<?=$this->lang->line('teaming_form_id');?>
								</th>
								<th class="row-md-1">
									<?=$this->lang->line('teaming_form_company_id');?>
								</th>
								<th class="row-md-1">
									<?=$this->lang->line('teaming_form_amount');?>
								</th>
								<th class="row-md-1">
									<?=$this->lang->line('teaming_form_amount_letters');?>
								</th>
								<th class="row-md-1">
									<?=$this->lang->line('teaming_form_area_campaign');?>
								</th>
								<th class="row-md-1">
									<?=$this->lang->line('teaming_form_celnumber');?>
								</th>
								<th class="row-md-1">
									<?=$this->lang->line('teaming_form_institution');?>
								</th>
								<th class="row-md-1">
									<?=$this->lang->line('teaming_form_date');?>
								</th>
							</tr>
						</thead>
						<tbody class="table-body">
							<tr class="table-adm" ng-repeat="agreeForm in agreeForms" ng-init ="agreeForm.value = false">
								<td class="row-md-1">
									<input type="checkbox" name="rowIdentifier" ng-model="agreeForm.value" ng-show="!enable" ng-change="agreeFormValueChange(agreeForm, $index)">
								</td>
								<td class="row-md-1">
									<span ng-model="agreeForm.completeName">
										{{agreeForm.name+' '+agreeForm.lastName}}
									</span>
								</td>
								<td class="row-md-1">
									<span ng-hide="agreeForm.value && enable">
										{{agreeForm.dni}}
									</span>
									<input type="text" ng-model="agreeForm.dni" ng-show="agreeForm.value && enable"  ng-value="agreeForm.dni" maxlength="8"  only-numbers="onlyNumbers()"  style=" max-width: 70px;">
								</td>
								<td class="row-md-1">
									<span ng-hide="agreeForm.value && enable">
										{{agreeForm.id}}
									</span>
									<input type="text" ng-model="agreeForm.id" ng-show="agreeForm.value && enable"  ng-value="agreeForm.id" maxlength="6"  only-numbers="onlyNumbers()"  style=" max-width: 70px;">
								</td>
								<td class="row-md-1">
									<span ng-hide="agreeForm.value && enable">
										{{agreeForm.amount}}
									</span>
									<input type="text" ng-model="agreeForm.amount" ng-show="agreeForm.value && enable"  ng-value="agreeForm.amount" maxlength="4" only-numbers="onlyNumbers()"  style=" max-width: 40px;">
								</td>
								<td class="row-md-1">
									<span ng-hide="agreeForm.value && enable">
										{{agreeForm.amountCharacters}}
									</span>
									<input type="text" ng-model="agreeForm.amountCharacters" ng-show="agreeForm.value && enable"  ng-value="agreeForm.amountCharacters" maxlength="46" alpha-input="alphaInput()" style=" width: 270px;">
								</td>
								<td class="row-md-1">
									<span ng-hide="agreeForm.value && enable">
										{{agreeForm.areaCampaign}}
									</span>
									<input type="text" ng-model="agreeForm.areaCampaign" ng-show="agreeForm.value && enable"  ng-value="agreeForm.areaCampaign" maxlength="70" alpha-input="alphaInput()">
								</td>
								<td class="row-md-1">
									<span ng-hide="agreeForm.value && enable">
										{{agreeForm.mobileNumber}}
									</span>
									<input type="text" ng-model="agreeForm.mobileNumber" ng-show="agreeForm.value && enable"  ng-value="agreeForm.mobileNumber" maxlength="20" cel-number="celNumberInput()">
								</td>
								<td class="row-md-1">
									<span ng-hide="agreeForm.value && enable">
										{{agreeForm.institutionName}}
									</span>
									<input type="text" ng-model="agreeForm.institutionName" ng-show="agreeForm.value && enable"  ng-value="agreeForm.institutionName" maxlength="70">
								</td>
								<td class="row-md-1">
									<span ng-hide="agreeForm.value && enable">
										{{agreeForm.agreementDate}}
									</span>
									<input type="text" ng-model="agreeForm.agreementDate" ng-show="agreeForm.value && enable" ng-value="agreeForm.agreementDate" class="form-control datepicker"  style="width:100px;">
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="text-center" ng-if="loading">
			<i class='fa fa-refresh fa-spin fa-5x fa-fw dark'></i>
		</div>
		<br>

		<div class="modification-panel" >
			<div class="panel panel-default">
				<div class="panel panel-heading" id="admin-header">
					<?=$this->lang->line('teaming_division_mod_table');?>
				</div>		    	
				<div class="ng-hide panel panel-body" ng-show="!loading">
					<div class="teaming-paragraph" ng-if="updateFormsNum == 0">
						<label><?php echo $this->lang->line('teaming_update_no_forms')?></label>
					</div>
					<table class="datatable-update table table-responsive" id="adm-tbl" ng-if="updateFormsNum > 0">
						<thead>
							<tr>
								<th class="row-md-1">
									&nbsp;
								</th>
								<th class="row-md-1">
									<?=$this->lang->line('teaming_form_name');?>
								</th>
								<th class="row-md-1">
									<?=$this->lang->line('teaming_form_id');?>
								</th>
								<th class="row-md-1">
									<?=$this->lang->line('teaming_form_company_id');?>
								</th>
								<th class="row-md-1">
									<?=$this->lang->line('teaming_form_amount');?>
								</th>
								<th class="row-md-1">
									<?=$this->lang->line('teaming_form_amount_letters');?>
								</th>
								<th class="row-md-1">
									<?=$this->lang->line('teaming_form_area_campaign');?>
								</th>
								<th class="row-md-1">
									<?=$this->lang->line('teaming_form_celnumber');?>
								</th>
								<th class="row-md-1">
									<?=$this->lang->line('teaming_form_institution');?>
								</th>
								<th class="row-md-1">
									<?=$this->lang->line('teaming_form_date');?>
								</th>
							</tr>
						</thead>
						<tbody class="table-body">
							<tr class="table-adm" ng-repeat="updateForm in updateForms" ng-init="updateForm.value = false;updateForm.opened = false">
								<td class="row-md-1">
									<input type="checkbox" name="rowIdentifier" ng-model="updateForm.value" ng-hide="enable" ng-value="true" ng-change="updateFormValueChange(updateForm, $index)">
								</td>
								<td class="row-md-1">
									<span ng-model="agreeForm.completeName">
										{{updateForm.name+' '+updateForm.lastName}}
									</span>
								</td>
								<td class="row-md-1">
									<span ng-hide="enable && updateForm.value">
										{{updateForm.dni}}
									</span>
									<input type="text" ng-model="updateForm.dni" ng-show="updateForm.value && enable" ng-value="updateForm.dni" maxlength="8"  only-numbers="onlyNumbers()" style=" max-width: 70px;">
								</td>
								<td class="row-md-1">
									<span ng-hide="enable && updateForm.value">
										{{updateForm.id}}
									</span>
									<input type="text" ng-model="updateForm.id" ng-show="updateForm.value && enable" ng-value="updateForm.id" maxlength="6"  only-numbers="onlyNumbers()" style=" max-width: 70px;">
								</td>
								<td class="row-md-1">
									<span ng-hide="enable && updateForm.value">
										{{updateForm.updatedAmount}}
									</span>
									<input type="text" ng-model="updateForm.updatedAmount" ng-show="updateForm.value && enable" ng-value="updateForm.updatedAmount" maxlength="4" only-numbers="onlyNumbers()" style=" max-width: 40px;">
								</td>
								<td class="row-md-1">
									<span ng-hide="enable && updateForm.value">
										{{updateForm.updatedAmountCharacters}}
									</span>
									<input type="text" ng-model="updateForm.updatedAmountCharacters" ng-show="updateForm.value && enable" ng-value="updateForm.updatedAmountCharacters"  maxlength="46" alpha-input="alphaInput()" style=" width: 270px;">
								</td>
								<td class="row-md-1">
									<span ng-hide="enable && updateForm.value">
										{{updateForm.areaCampaign}}
									</span>
									<input type="text" ng-model="updateForm.areaCampaign" ng-show="updateForm.value && enable" ng-value="updateForm.areaCampaign" maxlength="70" alpha-input="alphaInput()">
								</td>
								<td class="row-md-1">
									<span ng-hide="enable && updateForm.value">
										{{updateForm.mobileNumber}}
									</span>
									<input type="text" ng-model="updateForm.mobileNumber" ng-show="updateForm.value && enable" ng-value="updateForm.mobileNumber" maxlength="20" cel-number="celNumberInput()">
								</td>
								<td class="row-md-1">
									<span ng-hide="enable && updateForm.value">
										{{updateForm.institutionName}}
									</span>
									<input type="text" ng-model="updateForm.institutionName" ng-show="updateForm.value && enable" ng-value="updateForm.institutionName" maxlength="70">
								</td>
								<td class="row-md-1">
									<span ng-hide="updateForm.value && enable">
										{{updateForm.updateDate}}
									</span>
									<input type="text" ng-model="updateForm.updateDate" ng-show="updateForm.value && enable" ng-value="updateForm.updateDate" class="form-control datepicker" style="width:100px;">
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		
		<div class="text-center" ng-if="loading">
			<i class='fa fa-refresh fa-spin fa-5x fa-fw dark'></i>
		</div>
		<div ng-bind-html="errors | HtmlSanitize" style="text-align: left;"></div>
		<div class="div-buttons ng-hide" ng-show="!loading">
			<button class ="btn btn-default" id="first-try" ng-click="editAvailable()" ng-hide="enable || !radioPressed">
				<?=$this->lang->line('teaming_division_edit');?>
			</button>
			<button class ="btn btn-default" id="first-try" ng-click="saveForm()" ng-show="enable">
				<?=$this->lang->line('teaming_division_save');?>
			</button>
			<button class ="btn btn-default" id="first-try" ng-click="accept()" ng-show="radioPressed">
				<?=$this->lang->line('teaming_division_accept');?>
			</button>
			<button class ="btn btn-default" id="first-try" ng-click="showModal()" ng-show="radioPressed">
				<?=$this->lang->line('teaming_division_reject');?>
			</button>
			<button class ="btn btn-default" id="first-try" ng-click="print()" ng-show="radioPressed">
				<?=$this->lang->line('teaming_division_print');?>
			</button>
		</div>
		<!-- modal -->
		<div class="modal flat-style animated lightSpeedIn" id="removeAlert" role="dialog">
			<div class="modal-dialog modal-sm">
				<div class="modal-content">
					<div class="modal-body">
						<p><?php echo $this->lang->line('teaming_modal_observation_text') ?></p> 
						*<textarea name="observation" class="form-control" rows="5" maxlength="500" ng-model="var_observation" autofocus></textarea> 
						<p ng-show="error_modal" style="color: red">El campo de la observación no debe ser vacío.</p> 
						<p><?php echo $this->lang->line('teaming_delete_are_you_sure') ?></p>
						<div class="text-right">
							<button  class="btn btn-white" data-dismiss="modal"><?= $this->lang->line('general_no') ?></button>
							<button ng-click="reject()" class="btn btn-lightgreen"><?= $this->lang->line('general_yes') ?></button>
						</div>
					</div>		
				</div>
			</div>
		</div>
	</div>
</div>
<script src="/<?php echo APPFOLDERADD; ?>/libraries/script/bootstrap-datepicker.min.js"></script>
<script src="/<?php echo APPFOLDERADD; ?>/libraries/script/locales/bootstrap-datepicker.es.min.js"></script>
<script type="text/javascript" src="/<?php echo APPFOLDERADD; ?>/libraries/script/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="/<?php echo APPFOLDERADD; ?>/libraries/script/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" src="/<?php echo APPFOLDERADD; ?>/libraries/script/dataTables.select.min.js"></script>
<script>
	$('#pendingForms').replaceWith("<li><?php echo $this->lang->line('general_pending_forms');?></li>");
</script>

