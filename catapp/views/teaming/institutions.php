<link rel="stylesheet" type="text/css" href="/<?php echo APPFOLDERADD; ?>/libraries/css/dataTables.bootstrap.min.css">
<div class="back-teaming">
	<?php $header ?>
	<div class="container-fluid" id="reports-panel">

		<div class="panel panel-default" style="overflow: auto;">
			<div class="panel-heading" id="admin-header">
				<?php if ($this->Identity->Validate('teaming/suggestions/viewall')) { ?>
				<?=$this->lang->line('teaming_suggested_institutions');?>
				<?php }
				else {?>
				<?=$this->lang->line('teaming_suggested_user');?>
				<?php }?>
				
			</div>
			<div class="panel-body text-center" ng-if="loading">
				<i class='fa fa-refresh fa-spin fa-5x fa-fw dark'></i>
			</div>
			<div class="ng-hide panel-body" ng-show="!loading">
				<table class="datatable table table-hover" id="table-joined-users">
					<thead>
						<tr>
							<?php if ($this->Identity->Validate('teaming/suggestions/export')) { ?>
								<th class="row-md-1">
									<span>
										Exportar
									</span>
								</th>
							<?php } 
							?>
							<th class="row-md-1">
								<span>
									<?=$this->lang->line('teaming_suggest_name');?><span class="text-danger" ng-show="editSelected"><strong> *</strong></span>	
								</span>
							</th>
							<th class="row-md-1">
								<span>
									<?=$this->lang->line('teaming_suggested_from');?>	
								</span>
							</th>
							<th class="row-md-1">
								<span>
									<?=$this->lang->line('general_section');?>	
								</span>
							</th>
							<th class="row-md-1">
								<span>
									<?=$this->lang->line('general_site');?>	
								</span>
							</th>
							<th class="row-md-1">
								<span>
									<?=$this->lang->line('general_social_reason');?>	
								</span>
							</th>
							<th class="row-md-1">
								<span>
									<?=$this->lang->line('administration_users_turn');?>	
								</span>
							</th>
							<th class="row-md-1">
								<span>
									<?=$this->lang->line('teaming_suggest_contact_data');?>
								</span>
							</th>
							<th class="row-md-1">
								<span>
									&nbsp;
								</span>
							</th>
						</tr>
					</thead>
					<tbody>
						<tr ng-repeat="institution in suggestedInstitutions">
							<?php if ($this->Identity->Validate('teaming/suggestions/export')) { ?>
								<td class="row-md-1">
									<input type="checkbox" name="institutions[]" ng-model="institution.selected" ng-change="select(institution)">
								</td>
							<?php } 
							?>
							<td class="row-md-1">
								<span ng-model="institution.name">
									{{institution.name}}
								</span>
							</td>
							<td class="row-md-1">
								<span ng-model="institution.suggestedFrom">
									{{institution.suggestedFrom}}
								</span>
							</td>
							<td class="row-md-1">
								<span ng-model="institution.division">
									{{institution.division}}
								</span>
							</td>
							<td class="row-md-1">
								<span ng-model="institution.siteName">
									{{institution.siteName}}
								</span>
							</td>
							<td class="row-md-1">
								<span ng-model="institution.title">
									{{institution.title}}
								</span>
							</td>
							<td class="row-md-1">
								<span ng-model="institution.turn">
									{{institution.turn}}
								</span>
							</td>
							<td class="row-md-1">
								<span>
									<a class="teaming-color" href="#/teaming/suggestionDetails/{{institution.suggestedId}}"><span class="glyphicon glyphicon-search" aria-hidden="true" ></span></a>&nbsp;
								</span>
							</td>
							<?php   if ($this->Identity->Validate('teaming/suggestions/actions')) { ?>
								<td class="row-md-1">
									<span class="btn btn-green btn-xs" role="take" ng-if="institution.status == 0" ng-click="takeSuggestion(institution.suggestedId)">Tomar sugerencia</span>
									<span class="badge bg-pink" ng-if="institution.status == 1"><i class="fa fa-check"></i> Sugerencia tomada</span>
								</td>
							<?php } else{ ?>
								<td class="row-md-1">
									<span>
										<span class="badge bg-warning" role="take" ng-if="institution.status == 0">sugerencia pendiente</span>
										<span class="badge bg-pink" ng-if="institution.status == 1"><i class="fa fa-check"></i> Sugerencia tomada</span>
									</span>
								</td>
							<?php } ?>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		<div>
			<div class="btn-toolbar center-block" style="display: table;">
				<?php if ($this->Identity->Validate('teaming/suggestions/export')) { ?>
					<div class="btn-group" role="group" aria-label="First group" ng-show="numberSelected > 0">
						<button class="btn btn-primary" id="first-try"  ng-click="exportPdf('/<?php echo FOLDERADD; ?>/teaming/institutionsPdf/')">
							<?php echo $this->lang->line("teaming_user_details_export_pdf")?>
						</button>
					</div>
					<div class="btn-group" role="group" aria-label="First group" ng-show="numberSelected > 0">
						<form action="/<?=FOLDERADD?>/teaming/exportInstitutions" method="post" target="_blank" id="exportForm">
							<button class="btn btn-primary" id="first-try" ng-click="exportExcel()">
								<?php echo $this->lang->line("teaming_user_details_export_excel")?>
							</button>
							<input type="hidden" id="columns" name="columns"/>
							<input type="hidden" id="selectedInstitutions" name="selectedInstitutions"/>
						</form>
					</div>
				<?php } 
				?>
			</div>
		</div>
		<div id="noContactData" class="modal fade modal-sg" tabindex="-1" role="dialog">
			<div class="modal-dialogue">
				<div class="modal-content">
					<div class="modal-body">
						<div class="content-observation"> 
							<button id="exit-modal" data-dismiss="modal">&times;</button>
							<p><?php echo $this->lang->line('teaming_suggest_institution_no_contact_data');?></p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="modal " id="confirm-take" tabindex="-1" role="dialog">
			<div class="modal-dialog modal-sm animated flipInY ">
				<div class="modal-content">
					<div class="modal-body">
						<p><?php echo $this->lang->line('suggestbox_takeconfirm'); ?></p>
						<div class="text-right">
							<span class="btn btn-white" data-dismiss="modal" aria-label="Close"><?php echo $this->lang->line('general_no'); ?></span>
							<span class="btn btn-lightgreen" id="takeok"><?php echo $this->lang->line('general_yes'); ?></span>
							<span class="btn btn-lightgreen" disabled="true" id="suggestbox_taking" style="display:none"><i class='fa fa-refresh fa-spin fa-lg fa-fw'></i></span>
						</div>
					</div>
				</div>
			</div>
		</div> 

	</div>
	<script>
		$('#institutions').replaceWith("<li><?php echo $this->lang->line('general_institutions');?></li>");
	</script>
	<script type="text/javascript" src="/<?php echo APPFOLDERADD; ?>/libraries/script/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="/<?php echo APPFOLDERADD; ?>/libraries/script/dataTables.bootstrap.min.js"></script>


