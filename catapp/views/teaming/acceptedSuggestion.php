<div class="back-teaming">
	<div class="container-fluid" style="margin-right: auto;margin-left: auto; margin-bottom: 0;">
		<img class="img-responsive pull-left small-heart" src="/<?php echo APPFOLDERADD; ?>/libraries/images/corazon_chico.png">
		<img class="img-responsive center-block teaming-logo" src="/<?php echo APPFOLDERADD; ?>/libraries/images/logo_teaming.png">
		<div class="panel panel-default" id="accepted-panel" style="    margin-bottom: 160px;">
			<div class="panel-body" id="accepted-panel-body">
				<p class="accept-paragraph" style="font-family: Segoe UI;">
					<?=$this->lang->line('teaming_accepted_suggestion');?>
				</p>
				<p class="shadow-panel"></p>
			</div>
		</div>
		<img class="img-responsive pull-right" id="big-heart-accepted" src="/<?php echo APPFOLDERADD; ?>/libraries/images/corazon_grande.png">
		<div class="buttons text-center">
			<a class="btn btn-default" id="first-try"  style="padding-top: 30px;" href="#/teaming">
				<i class="fa fa-chevron-left"></i><?=$this->lang->line('general_goback');?>
			</a>
		</div>
	</div>
	
	<div class="sep" style="margin-bottom:  120px">
		&nbsp;
	</div>
</div>
