<?php

if (isset($_SESSION['save_success']))
{
	$success = ($_SESSION['save_success']['state'] == 'success');

	?>
	<div class="alert alert-<?= ($success) ? 'success' : 'danger' ?> alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<strong><i class="fa fa-<?= ($success) ? 'check' : 'times' ?>"></i></strong>
		<?= $_SESSION['save_success']['message'] ?>
	</div>
	<?php 
}
?>

<div class="panel panel-default">
	<div class="panel-heading">
		<strong><?=$this->lang->line('question_security_questions');?></strong>
	</div>
	<div class="panel-body">
		<form class="form-horizontal" method="POST">
			
			<?php 
			for($i = 0; $i < 3; $i++)
			{
				?>
				<div style="margin-bottom:40px">
					<div class="form-group">
						<label for="question_<?= $i ?>" class="col-sm-2 control-label"><?=$this->lang->line('question_question').' '.($i+1);?></label>
						<div class="col-sm-10">
							<select class="form-control input-sm" id="question_<?= $i ?>" name="question_<?= $i ?>">
								<option value="0"><?php echo $this->lang->line('question_select_one'); ?></option>
								<?php
								foreach($questions as $question)
								{
									?>
									<option value="<?=$question->securityquestionId?>" <?php echo set_select('question_'.$i, $question->securityquestionId, (isset($my_questions[$i]->securityquestionId) && $question->securityquestionId == $my_questions[$i]->securityquestionId))?> ><?=$question->question?></option>
									<?php
								}?>
							</select>
							<?php echo form_error('question_'.$i); ?>
						</div>
					</div>

					<div class="form-group">
						<label for="answer<?= $i ?>" class="col-sm-2 control-label"><?=$this->lang->line('question_answer');?></label>
						<div class="col-sm-10">
							<?php if(isset($my_questions[$i]->answersecurityquestionId))
							{ ?>
								<input type="hidden" name="answer_id_<?= $i ?>" value="<?= $my_questions[$i]->answersecurityquestionId?>">
								<?php
							}?>
							
							<div class="input-group">
								<span class="input-group-btn">
									<button id="view_answer_<?= $i ?>" class="btn btn-default input-sm" type="button"><span class="fa fa-eye"></span></button>
								</span>
								<input type="password" class="form-control input-sm" id="answer<?= $i ?>" name="answer_<?= $i ?>" value="<?php echo set_value('answer_'.$i, isset($my_questions[$i]->answer) ? $my_questions[$i]->answer : '');?>" placeholder="<?=$this->lang->line('question_answer_example');?>">
							</div>

							<?php echo form_error('answer_'.$i); ?>
						</div>
					</div>
				</div>
				<?php 
			}
			?>

			<hr>
			<div class="form-group text-center">
				<button name="edit" type="submit" class="btn btn-success btn-sm"><?=$this->lang->line('general_save');?></button>
			</div>
		</form>

	</div>
</div>
<script type="text/javascript">

	$('#nav_security_questions').addClass('active');

	for(var i = 0; i < 3; i++)
	{
		$('#view_answer_'+i).on('click', set_visibility_answer);
	}

	function set_visibility_answer()
	{
		var eye_button   = $(this).children();
		var answer_input = $(this).parent().next();

		if( eye_button.attr('class') == 'fa fa-eye')
		{
			eye_button.attr('class', 'fa fa-eye-slash');
			answer_input.attr('type', 'text');
		}
		else
		{
			eye_button.attr('class', 'fa fa-eye');
			answer_input.attr('type', 'password');
		}
	}



</script>