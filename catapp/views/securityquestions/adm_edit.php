<ol class="breadcrumb">
	<li><a href="/<?=FOLDERADD?>/administration"><?=$this->lang->line('general_administration');?></a></li>
	<li><a href="/<?=FOLDERADD?>/securityquestions/config"><?=$this->lang->line('question_security_questions');?></a></li>
	<li class="active"><?=$this->lang->line('administration_edit');?></li>
</ol>
<?php
if (isset($_SESSION['save_success']))
{
	$success = ($_SESSION['save_success']['state'] == 'success');

	?>
	<div class="alert alert-<?= ($success) ? 'success' : 'danger' ?> alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<strong><i class="fa fa-<?= ($success) ? 'check' : 'times' ?>"></i></strong>
		<?= $_SESSION['save_success']['message'] ?>
	</div>
	<?php 
}?>
<div class="panel panel-default">
	<div class="panel-heading">
		<strong><?=$this->lang->line('question_edit');?></strong>
	</div>
	<div class="panel-body">
		<form class="form-horizontal" method="POST">
			<div class="form-group">
				<label for="question" class="col-sm-2 control-label"><?=$this->lang->line('question_question');?><span class="text-danger"><strong> *</strong></span></label>
				<div class="col-sm-10">
					<input type="text" class="form-control input-sm" id="question" name="question" value="<?php echo set_value('question', $question->question);?>" placeholder="<?=$this->lang->line('question_name_example');?>" required>
					<?php echo form_error('question'); ?>
				</div>
			</div>
			<hr>
			<div class="form-group text-center">
				<button type="submit" name="edit" class="btn btn-success btn-sm"><?=$this->lang->line('general_save');?></button>
				<a href="/<?=FOLDERADD?>/securityquestions/config" class="btn btn-danger btn-sm"><?=$this->lang->line('general_cancel');?></a>
			</div>
		</form>
	</div>
</div>
<script type="text/javascript">
$('#nav_security_questions').addClass('active');
</script>