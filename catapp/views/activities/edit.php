<div class="page-title">
	<h3 class="title"><?php echo $this->lang->line('activity_edit');?></h3>
	<a href="#/activities" class="btn btn-white pull-right"><i class="fa fa-chevron-left"></i> <?php echo $this->lang->line('general_goback');?></a>
</div>
<div class="col-xs-12 flat-style">

	<div class="panel panel-default">	
		
		<div class="panel-body">
			<form class="form-horizontal" method="POST" novalidate class="bg-white" name="formActivity" ng-submit="submitActivity()" id="form-activity" >
				<h3 style="margin-top: 0; margin-bottom: 0; padding-left: 10px"><?php echo $this->lang->line('survey_info'); ?></h3>
				<hr style="margin-top: 10px; "/>
				<div class="form-group" ng-class="{inputError:name == null}" >
					<label for="name" class="col-sm-2 control-label"><?php echo $this->lang->line('survey_name');?> <strong class="text-danger" role="required">*</strong></label>
					<div class="col-sm-10">
						<input type="text" class="form-control input-sm" id="name" name="name" ng-model="activity.name" value="<?php echo set_value('name')?>" placeholder="<?php echo $this->lang->line('survey_name');?>" required>
						<?php echo form_error('name'); ?>
					</div>
				</div>
				<div class="form-group">
					<label for="descriptionS" class="col-sm-2 control-label"><?php echo $this->lang->line('survey_description');?></label>
					<div class="col-sm-10">
						<textarea class="form-control input-sm" id="descriptionS" name="descriptionS" rows="3" ng-model="activity.description" placeholder="<?php echo $this->lang->line('survey_description');?>"></textarea>
						<?php echo form_error('descriptionS'); ?>
					</div>
				</div>
				<div class="form-group">
					<label for="closeMessage" class="col-sm-2 control-label"><?php echo $this->lang->line('survey_closemessage');?></label>
					<div class="col-sm-10">
						<textarea class="form-control input-sm" id="closeMessage" name="closeMessage" rows="3" ng-model="activity.closeMessage" placeholder="<?php echo $this->lang->line('survey_closemessage');?>"></textarea>
						<?php echo form_error('closeMessage'); ?>
					</div>
				</div>
				<div class="form-group">
					<label for="startDate" class="col-sm-2 control-label"><?php echo $this->lang->line('activity_start_date');?><span class="text-danger strong"> *</span></label>
					<div class="col-xs-10">
						<input type="text" class="form-control input-sm" data-date-format="mm/dd/yyyy" role="date" id="startDate" name="startDate" value="<?php echo set_value('startDate')?>" placeholder="<?php echo $this->lang->line('survey_start_date');?>">

						<?php echo form_error('startDate'); ?>
					</div>
				</div>
				<div class="form-group">
					<label for="endDate" class="col-sm-2 control-label"><?php echo $this->lang->line('activity_end_date');?><span class="text-danger strong"> *</span></label>
					<div class="col-xs-10">
						<input type="text" data-date-format="mm/dd/yyyy"  class="form-control input-sm" role="date" id="endDate" name="endDate" value="<?php echo set_value('endDate')?>" placeholder="<?php echo $this->lang->line('survey_end_date');?>">
						<?php echo form_error('endDate'); ?>
					</div>
                </div>
                <div class="form-group">
					<label for="userfile" class="col-sm-2 control-label"><?=$this->lang->line('attached_image');?><strong class="text-danger">*</strong></label>
					<div class="col-sm-10">
						<input type="file" id="userfile" name="userfile" accept="image/gif, image/jpeg, image/png">
						<small><?=$this->lang->line('events_file_allowtypes')?></small><br>
						<small><?=$this->lang->line('events_file_allowsizes')?></small><br>
						<img style="max-height: 200px; max-width: 200px;" id="imgPrev" src="/<?php echo FOLDERADD;?>/catapp/_activities_images/{{activity.image}}" alt="">
					</div>
                </div>
                <div class="form-group">
                    <div class="col-md-10 col-md-offset-2">
                        <span class="btn btn-lightgreen" id="addCoupon" ng-show="!showCoupons" ng-click="showCoupons = true"><?=$this->lang->line('activity_capacity');?></span>
                        <span class="btn btn-red" id="cancelCoupon" ng-show="showCoupons" ng-click="cancelCoupons()" title="<?=$this->lang->line('general_cancel');?>"><i class="fa fa-times"></i></span>
                    </div>
                </div>
                <div class="form-group" ng-show="showCoupons">
                    <label for="coupons" class="col-md-2 control-label"><?=$this->lang->line('activity_coupons');?></label>
                    <div class="col-sm-10">
                        <input type="number" name="coupons" id="coupons" class="form-control input-sm">
                    </div>
                </div>
				<div class="form-group">
					<label class="col-sm-2 control-label"><?php echo $this->lang->line('activity_required');?></label>
					<div class="col-xs-10">
						<div class="checkbox checkbox-angular">
							<label ng-class="{check: required}">
								<input type="checkbox" name="required" ng-model="required" ng-init="required = true" value="TRUE">
							</label>
						</div>
					</div>
				</div>
				<h3 style="margin-top: 0; margin-bottom: 0; padding-left: 10px"><?php echo $this->lang->line('survey_whose'); ?> <strong class="text-danger">*</strong></h3>
				<hr style="margin-top: 10px; "/>

				<div class="form-group">
					<label class="col-sm-2 control-label"><?=$this->lang->line('general_sections');?></label>
					<div class="col-sm-10">

						<div ng-repeat="section in activity.sections" class="checkbox checkbox-angular">
							<label ng-class="{check: section.active}">
								<input type="checkbox" name="sections[]" ng-model="section.active" value="{{section.sectionId}}">
								{{section.name}}
							</label>
						</div>

					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label"><?=$this->lang->line('general_sites');?></label>
					<div class="col-sm-10">

						<div ng-repeat="site in activity.sites" class="checkbox checkbox-angular">
							<label ng-class="{check: site.active}">
								<input type="checkbox" name="sites[]" ng-model="site.active"  value="{{site.siteId}}">
								{{site.name}}
							</label>
						</div>

					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label"><?=$this->lang->line('general_roles');?></label>
					<div class="col-sm-10">

						<div ng-repeat="role in activity.roles" class="checkbox checkbox-angular">
							<label ng-class="{check: role.active}">
								<input type="checkbox" name="roles[]" ng-model="role.active" value="{{role.roleId}}">
								{{role.name}}
							</label>
						</div>

					</div>
				</div>
				<?php
				if ($this->Identity->Validate('usergroups/create')) {
					?>
					<div class="form-group">
						<label class="col-sm-2 control-label"><?=$this->lang->line('general_usergroups');?></label>
						<div class="col-sm-10">

							<div ng-repeat="userGroup in activity.userGroups" class="checkbox checkbox-angular">
								<label ng-class="{check: userGroup.active}">
									<input type="checkbox" name="userGroups[]" ng-model="userGroup.active" value="{{userGroup.userGroupId}}">
									{{userGroup.name}}
								</label>
							</div>

						</div>
					</div>
					<?php
				}
				?>
				<div class="form-group">
					<input type="hidden" name="activityId" value="{{activity.activityId}}"/>
					<input type="hidden" ng-repeat="user in users track by $index" name="users[]" value="{{user.userId}}" />
					<label class="col-sm-2 control-label"><?php echo $this->lang->line('general_users');?></label>
					<div class="col-sm-10 form-group">
						<div class="select-container" ng-click="selectClick($event);">
							<span class="select-selected">
								<span class="select-selected-item" ng-repeat="user in users track by $index" ng-init="i=$index">
									<span class="selected-item-image" style="background-image: url('/<?php echo FOLDERADD; ?>/users/profilephoto?userId={{user.userId}}&amp;amp;wah=200');filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='/<?php echo FOLDERADD; ?>/users/profilephoto?userId={{user.userId}}&wah=200',sizingMethod='scale');"></span>
									<span class="selected-item-label" ng-bind="user.completeName+' <'+user.userName+'>'" ></span>
									<span class="selected-item-delete no-selectable" ng-click="removeUser(i)">&times;</span>
								</span>
							</span>
							<input type="text" autocomplete="off" class="input-sm form-control select-input" ng-trim="true" ng-model="findLike" ng-change="searchUsers()" id="findInput" placeholder="<?php echo $this->lang->line('survey_writename');?>" />
						</div>
						<div class="select-options-container-super" ng-show="userOptions.length > 0">
							<div class="select-options-container">
								<div class="select-option no-selectable" ng-repeat="user in userOptions track by $index" ng-init="i=$index" ng-click="addUser(i)">
									<span class="selected-option-image" style="background-image: url('/<?php echo FOLDERADD; ?>/users/profilephoto?userId={{user.userId}}&amp;amp;wah=200');filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='/<?php echo FOLDERADD; ?>/users/profilephoto?userId={{user.userId}}&wah=200',sizingMethod='scale');"></span>
									<span class="selected-option-label" ng-bind="user.completeName+' <'+user.userName+'>'"></span>
								</div>
							</div>
						</div>
					</div>
                </div>
				<hr>
				<div class="form-group">
					<label class="col-sm-2 control-label"><?=$this->lang->line('users_turn');?></label>
					<div class="col-sm-10">
						<div ng-repeat="(key,turn) in activity.turns track by $index" class="checkbox checkbox-angular">
								<label ng-class="{check: selected.value == $index}">
									<input type="checkbox" name="turns[]" ng-click="selected.value == -1 ? selected.value = $index : selected.value = -1" value="{{key}}" ng-init="selected.value = -1">
									{{turn}}
								</label>
						</div>
					</div>
				</div>
                <div class="form-group text-center">
					<button type="submit" class="btn btn-green btn-lg" ng-hide="sending"><?php echo $this->lang->line('general_edit'); ?></button>
					<button class="btn btn-green disabled" ng-show="sending"><i class="fa fa-refresh fa-spin fa-lg"></i></button>
				</div>
			</form>
			<div class="modal fade" id="no-complete" tabindex="-1" role="dialog">
				<div class="modal-dialog modal-sm">
					<div class="modal-content">
						<div class="modal-body">
							<p><?php echo $this->lang->line('survey_completeall')?> <strong class="text-danger">*</strong></p>
							<div class="text-right">
								<span class="btn btn-white" data-dismiss="modal" aria-label="Close"><?php echo $this->lang->line('general_accept'); ?></span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript" src="/<?php echo APPFOLDERADD; ?>/libraries/tinymce/tinymce.min.js"></script>
<script src="/<?php echo APPFOLDERADD; ?>/libraries/script/bootstrap-datepicker.min.js"></script>
<script src="/<?php echo APPFOLDERADD; ?>/libraries/script/locales/bootstrap-datepicker.es.min.js"></script>
<script type="text/javascript" src="/<?php echo APPFOLDERADD; ?>/libraries/script/jquery.form.js"></script>