<div class="panel" id="socialContainer" style="border-radius: 3px; margin-top: 30px;">
					<?php
					if ($this->Identity->Validate('_layoutfooter/socialbutons'))
					{ 
						?>
						<p class="text-center" style="margin-top: 10px;">
							Seguinos en:
						</p>
						<ul class="social">
							<li class="social-li">
								<a style="cursor:default">  
									<div class="social-icon">
										<span class="fa-stack">
											<i class="fa fa-circle fa-stack-2x circle-fb"></i>
											<i class="fa fa-facebook fa-stack-1x"></i>
										</span>
										<span class="social-icon-text">/SumateaCatTechnologies</span>
									</div>
								</a> 
							</li>
							<li class="social-li"> 
								<a style="cursor:default"> 
									<div class="social-icon">
										<span class="fa-stack">
											<i class="fa fa-circle fa-stack-2x circle-li"></i>
											<i class="fa fa-linkedin fa-stack-1x"></i>
										</span>
										<span class="social-icon-text"><span class="">CAT Technologies Argentina</span></span>
									</div>
								</a>

							</li>
							<li class="social-li"> 
								<a style="cursor:default">
									<div class="social-icon">
										<span class="fa-stack">
											<i class="fa fa-circle fa-stack-2x circle-tw"></i>
											<i class="fa fa-twitter fa-stack-1x"></i>
										</span>
										<span class="social-icon-text"><span class="">@CATArgentinaSA</span></span>
									</div>
								</a> 
							</li>
						</ul>
						<?php 
					} 
					?>
				</div>


			</div>
		</div>
	</div>
	<script src="/<?php echo APPFOLDERADD;?>/libraries/script/html5shiv.min.js"></script>
	<script src="/<?php echo APPFOLDERADD;?>/libraries/script/respond.min.js"></script>
</body>
</html>