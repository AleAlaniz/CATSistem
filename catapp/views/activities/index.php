<div class="page-title">
	<h5 class="title">
		<?php echo $this->lang->line('activity_activities'); ?>	
	</h5>
	<?php if($this->Identity->Validate('activities/manage')) 
	{ 
		?>
		<a href="#/activities/create" class="btn  btn-white ">
			<i class="fa fa-plus text-muted"></i> 
			<?=$this->lang->line('survey_new');?>
		</a>
		<?php 
	}
	?>
</div>

<div class="col-xs-12 flat-style">

	<div class="modal animated fade" id="confirm-create">
		<div class="modal-dialog modal-sm">
			<div class="modal-content">
				<div class="modal-body">
					<p><?php echo $this->lang->line('activity_create_confirm_message')?>?</p>
					<div class="text-right">
						<span class="btn btn-white" data-dismiss="modal" aria-label="Close"><?php echo $this->lang->line('general_no'); ?></span>
						<button class="btn btn-lightgreen" ng-click="active()"><?php echo $this->lang->line('general_yes'); ?></button>
					</div>
				</div>
			</div>
		</div>
	</div>


	<?php
	if ($this->Identity->Validate('activities/manage')) {
		?>
		<div class="alert bg-lightgreen alert-dismissible" role="alert" ng-show="message != null && message != ''">
			<button type="button" class="close" ng-click="message = ''"><span aria-hidden="true">&times;</span></button>
			<strong><i class="fa fa-check"></i></strong> <span ng-bind="message"></span>
		</div>
		
		<div class="modal animated shake" id="confirm-delete" tabindex="-1" role="dialog">
			<div class="modal-dialog modal-sm ">
				<div class="modal-content">
					<div class="modal-body">
						<p><?php echo $this->lang->line('activity_delete_confirm_message')?><span class="" role="activitiy-title"></span>?</p>
						<div class="text-right">
							<span class="btn btn-white" data-dismiss="modal" aria-label="Close"><?php echo $this->lang->line('general_no'); ?></span>
							<button class="btn btn-lightgreen" ng-click="delete()"><?php echo $this->lang->line('general_yes'); ?></button>
						</div>
					</div>
				</div>
			</div>
		</div>


		<div class="modal fade" id="confirm-activate" tabindex="-1" role="dialog">
			<div class="modal-dialog modal-sm">
				<div class="modal-content">
					<div class="modal-body">
						<p><?php echo $this->lang->line('activity_activate_confirm_message')?></p>
						<div class="text-right">
							<span class="btn btn-white" data-dismiss="modal" aria-label="Close"><?php echo $this->lang->line('general_no'); ?></span>
							<button type="submit" role="activate" class="btn btn-green" ng-hide="sending"><?=$this->lang->line('general_yes');?></button>
							<button class="btn btn-green disabled" role="activateLoading"><i class="fa fa-refresh fa-spin fa-lg"></i></button>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php
	}
	?>

	<div class="panel" role="loading" ng-show="loading">
		<div class="panel-body text-center">
			<i class='fa fa-refresh fa-spin fa-5x fa-fw dark' ></i>
		</div>
	</div>

	<div  ng-hide="loading">

		<div class="alert bg-pink" role="alert" ng-show="activities.length == 0">
			<?php echo $this->lang->line('activity_empty'); ?>
		</div>

		<div class="jumbotron" ng-repeat="activity in activities">

			<h2 ng-bind="activity.name"></h2>
			<hr/>
			<img style="max-height: 400px; max-width: 400px;" id="imgPrev" src="/<?php echo FOLDERADD;?>/catapp/_activities_images/{{activity.image}}" alt="">
			<p ng-bind="activity.description"></p>
			<div class="small-lg" style="margin-bottom: 10px;" ng-show="activity.startDate || activity.endDate">
				<div class="small-lg" style="margin-bottom: 10px;" ng-show="activity.capacity && activity.capacity > 0">
					<div ng-show="activity.capacity">
						<?php echo $this->lang->line('activity_available'); ?>: <b ng-bind="activity.capacity"></b>
					</div>	
				</div>
				<p class="text-red" ng-show="activity.capacity && activity.capacity == 0 && !activity.active"><?php echo $this->lang->line('activity_empty')?></p>
				<?php if ($this->Identity->Validate('activities/manage')) {?>
					<div ng-show="activity.startDate"><?php echo $this->lang->line('activity_start_date'); ?>: <b ng-bind="activity.startDate"></b></div>
					<div ng-show="activity.endDate"><?php echo $this->lang->line('activity_end_date'); ?>: <b ng-bind="activity.endDate"></b>
						<i class="fa fa-pencil fa-lg btn" ng-show = "activity.active == 'TRUE'" ng-click="editDateActivity = true; showEdit();" title="<?php echo $this->lang->line('survey_edit_enddate')?>"></i>
					</div>
					<div>
						<input ng-show="editDateActivity" ng-model="activity.editDate" type="text" class="form-control input-sm" role="endDate" style="width:42em;margin-bottom: 0.5em;" placeholder="<?php echo $this->lang->line('survey_end_date');?>">
						<h6 id="{{activity.activityId}}" style="display: none;" class="text-danger"><?php echo $this->lang->line('survey_empty_date'); ?></h6>

						<i class="fa fa-check btn btn-green" ng-show="editDateActivity" ng-click="updateDate(activity.activityId,activity.editDate,endDateEmpty);" title="<?php echo $this->lang->line('survey_confirm_edit');?>"></i>
						<i class="fa fa-times btn btn-red" ng-show="editDateActivity" ng-click="editDateActivity = false; hideEmpty(activity.activityId);" title="<?php echo $this->lang->line('general_cancel');?>"></i>
					</div>
				<?php } 
				?>
				<p class="text-red" ng-show="activity.capacity == 0"><?php echo $this->lang->line('activity_empty_coupon')?></p>
			</div>

			<p style="margin-top: 10px;" ng-show="activity.active == 'TRUE'">
				<?php
				if ($this->Identity->Validate('activities/manage')) {
					?>
					<a class="btn btn-green" href="#/activities/report/{{activity.activityId}}">
						<i class="fa fa-bar-chart"></i> <?php echo $this->lang->line('survey_report'); ?>
					</a>
					<span class="btn btn-red" ng-click="showDelete(activity.activityId)">
						<i class="fa fa-trash"></i> <?php echo $this->lang->line('general_delete'); ?>
					</span>
					<a class="btn btn-green" ng-show="activity.capacity > 0 || activity.capacity == null" href="/<?php echo FOLDERADD; ?>/activity/showManualCharge/{{activity.activityId}}">
						<i class="fa fa-file-text"> </i> <?php echo $this->lang->line('survey_manual_charge'); ?>
					</a>
					<?php
				}
				?>

				<a class="btn btn-dark" href="/<?php echo FOLDERADD; ?>/activity/completeActivity/{{activity.activityId}}" ng-if="activity.complete == 0 && !activity.willAssist && activity.isTime && (activity.capacity > 0 || activity.capacity == null)">
					<i class="fa fa-file-text"></i> <?php echo $this->lang->line('activity_confirm'); ?>
				</a>

				<a class="btn btn-yelow" href="/<?php echo FOLDERADD; ?>/activity/changeOpinion/{{activity.activityId}}" ng-if="activity.isTime && activity.complete != 0">
					<i class="fa fa-sign-out"></i> <?php echo $this->lang->line('activity_edit_opinion'); ?>
				</a>
				
			</p>

			<p style="margin-top: 10px;" ng-show="activity.active == 'FALSE'">
				<?php
				if ($this->Identity->Validate('activities/manage')) {
					?>
					<a class="btn btn-yelow" href="#/activities/edit/{{activity.activityId}}">
						<i class="fa fa-pencil-square-o"></i> <?php echo $this->lang->line('general_edit'); ?>
					</a>

					<span class="btn btn-red" ng-click="showDelete(activity.activityId)">
						<i class="fa fa-trash"></i> <?php echo $this->lang->line('general_delete'); ?>
					</span>

					<span class="btn btn-green" ng-click="showCreate(activity.activityId)">
						<i class="fa fa-bolt"></i> <?php echo $this->lang->line('survey_activate'); ?>
					</span>
					<?php
				}
				?>
			</p>
		</div>
<script src="/<?php echo APPFOLDERADD; ?>/libraries/script/bootstrap-datepicker.min.js"></script>
<script src="/<?php echo APPFOLDERADD; ?>/libraries/script/locales/bootstrap-datepicker.es.min.js"></script>
		
