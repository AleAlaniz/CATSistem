<ul class="nav nav-tabs">
	<?php if($this->Identity->Validate('clients/edit')) { ?>
	<li role="presentation" id="NavEdit"><a href="/<?=FOLDERADD?>/clients/edit/<?=$clientId;?>"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> <span class="hidden-xs"><?=$this->lang->line('administration_edit');?></span></a></li>
	<?php } ?>
	<?php if($this->Identity->Validate('clients/delete')) { ?>
	<li role="presentation" id="NavDelete"><a href="/<?=FOLDERADD?>/clients/delete/<?=$clientId;?>"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> <span class="hidden-xs"><?=$this->lang->line('administration_delete');?></span></a></li>
	<?php } ?>
</ul>