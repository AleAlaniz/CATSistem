<ol class="breadcrumb">
	<li><a href="/<?=FOLDERADD?>/administration"><?=$this->lang->line('general_administration');?></a></li>
	<li class="active"><?=$this->lang->line('general_clients');?></li>
</ol>

<?php if($this->Identity->Validate('clients/create')) {
	?>
	<p>
		<a href="/<?=FOLDERADD?>/clients/create" class="btn btn-sm btn-success "><i class="fa fa-plus"></i><strong> <?=$this->lang->line('administration_create');?></strong></a>
	</p>
	<?php 
}
if (isset($_SESSION['flashMessage'])){ 
	?>
	<div class="alert alert-success alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<strong><i class="fa fa-check"></i></strong> 
		<?php if ($_SESSION['flashMessage'] == 'create'){
			echo $this->lang->line('clients_successmessage');
		}
		elseif ($_SESSION['flashMessage'] == 'delete'){
			echo $this->lang->line('clients_deletemessage');
		}
		elseif($_SESSION['flashMessage']=='edit')
		{
			echo $this->lang->line('clients_editmessage');
		}
		?>
	</div>
	<?php
}
?>
<div class="panel panel-default">
	<div class="panel-heading">
		<strong><?=$this->lang->line('general_clients');?></strong>
		<span class="badge pull-right bg-success"><?=count($model)?></span>
	</div>
	<table class="table table-hover">
		<thead>
			<tr class="active">
				<th><?=$this->lang->line('clients_name');?></th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			<?php 
			if (count($model) > 0) {
				foreach ($model as $client){
					?>
					<tr class="optionsUser">
						<td><?=encodeQuery($client->name)?></td>
						<td>
							<?php 
							if($this->Identity->Validate('clients/edit')) {
								?>
								<a href="/<?=FOLDERADD?>/clients/edit/<?=$client->clientId?>"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>&nbsp; 
								<?php 
							}
							if($this->Identity->Validate('clients/delete')) { 
								?>
								<a href="/<?=FOLDERADD?>/clients/delete/<?=$client->clientId?>"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
								<?php
							} 
							?>
						</td>
					</tr>

					<?php
				}
				?>
				<?php
			} 
			else { 
				?>
				<tr class="text-center">
					<td colspan="3"><i class="fa fa-bullhorn"></i> <?=$this->lang->line('clients_empty');?></td>
				</tr>
				<?php
			}
			?>
		</tbody>
	</table>
</div>
<script type="text/javascript">
	$('#nav_clients').addClass('active');

</script>