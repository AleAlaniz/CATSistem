<ol class="breadcrumb">
	<li><a href="/<?=FOLDERADD?>/administration"><?=$this->lang->line('general_administration');?></a></li>
	<li><a href="/<?=FOLDERADD?>/clients"><?=$this->lang->line('general_clients');?></a></li>
	<li class="active"><?=$this->lang->line('administration_create');?></li>
</ol>
<div class="panel panel-default">
	<div class="panel-heading">
		<strong><?=$this->lang->line('clients_create');?></strong>
	</div>
	<div class="panel-body">
		<form class="form-horizontal" method="POST" novalidate >
			<div class="form-group">
				<label for="name" class="col-sm-2 control-label"><?=$this->lang->line('clients_name');?><span class="text-danger"><strong> *</strong></span></label>
				<div class="col-sm-10">
					<input type="text" class="form-control input-sm" id="name" name="name" value="<?php echo set_value('name');?>" placeholder="<?=$this->lang->line('clients_name');?>" required>
					<?php echo form_error('name'); ?>
				</div>
			</div>
			<hr>
			<div class="form-group text-center">
				<button type="submit" class="btn btn-success btn-sm"><?=$this->lang->line('general_save');?></button>
				<a href="/<?=FOLDERADD?>/clients" class="btn btn-danger btn-sm"><?=$this->lang->line('general_cancel');?></a>
			</div>
		</form>
	</div>
</div>
<script type="text/javascript">
	$('#nav_clients').addClass('active');
</script>