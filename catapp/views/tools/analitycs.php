<link href="/<?php echo APPFOLDERADD; ?>/libraries/css/bootstrap-datepicker.css" rel="stylesheet">
<?php echo $navBar; ?>
<div class="col-xs-12 flat-style">
	<div class="panel panel-default">
		<div class="panel-body">
			<form class="form-horizontal" method="POST" novalidate ng-submit="getData()">
				<div class="form-group">
					<label for="date" class="col-sm-2 control-label"><?php echo $this->lang->line('tools_analitycs_date');?><span class="text-danger"><strong> *</strong></span></label>
					<div class="col-sm-10">
						<input type="text" data-date-format="mm/dd/yyyy" class="form-control input-date" ng-init="date = '<?php echo date('m/d/Y');?>'" name="date" ng-model="date" placeholder="<?php echo $this->lang->line('tools_analitycs_date');?>">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label"><?php echo $this->lang->line('tools_analitycs_measures');?><span class="text-danger"><strong> *</strong></span></label>
					<div class="col-sm-10">
						<div class="input-group">
							<input type="number" class="form-control text-center" ng-model="width" name="width" placeholder="<?php echo $this->lang->line('tools_analitycs_width');?>">
							<span class="input-group-addon">&times;</span>
							<input type="number" class="form-control text-center" ng-model="height" name="height" placeholder="<?php echo $this->lang->line('tools_analitycs_height');?>">
						</div>
					</div>
					<label for="date" class="col-sm-2 control-label"><?php echo $this->lang->line('general_sites');?><span class="text-danger"><strong> *</strong></span></label>
					<group division="divisions"></group>
				</div>
				<hr>
				<div class="form-group text-center">
					<button class="btn btn-green" disabled="true" type="submit" ng-show="sending"><i class='fa fa-refresh fa-spin fa-lg fa-fw'></i></button>
					<button type="submit" class="btn btn-green" ng-hide="sending" ><?php echo $this->lang->line('tools_analitycs_viewdata');?></button>
				</div>
				<div class="modal fade" id="no-complete" tabindex="-1" role="dialog">
					<div class="modal-dialog modal-sm">
						<div class="modal-content">
							<div class="modal-body">
								<p><?php echo $this->lang->line('general_completeall')?> <strong class="text-danger">*</strong></p>
								<div class="text-right">
									<span class="btn btn-white" data-dismiss="modal" aria-label="Close"><?php echo $this->lang->line('general_accept'); ?></span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</form>
			<div style="overflow: auto; margin-bottom: 10px" id="chart">
			</div>
			<div ng-show="data">
				<dl class="dl-horizontal col-sm-6">
					<dt><?php echo $this->lang->line('tools_analitycs_users');?></dt>
					<dd ng-bind="data.totalUsers"></dd>
					<dt><?php echo $this->lang->line('tools_analitycs_logins');?></dt>
					<dd ng-bind="data.totalLogins.total"></dd>
				</dl>
				<dl class="dl-horizontal col-sm-6">
					<dt><?php echo $this->lang->line('tools_analitycs_newusers');?></dt>
					<dd ng-bind="data.newUsers"></dd>
				</dl>
			</div>

		</div>
	</div>
</div>
<script src="/<?php echo APPFOLDERADD; ?>/libraries/script/bootstrap-datepicker.min.js"></script>
<script src="/<?php echo APPFOLDERADD; ?>/libraries/script/locales/bootstrap-datepicker.es.min.js"></script>
<script src="/<?php echo APPFOLDERADD; ?>/libraries/script/Chart.min.js"></script>