<div class="page-title">
	<h3 class="title"><?php echo $this->lang->line('tools_title'); ?></h3>
</div>
<div class="col-xs-12 flat-style">
	<div class="panel panel-default" ng-app="toolsApp" ng-controller="Ctools">
		<div class="list-group">
			<?php 
			if($this->Identity->Validate('tools/notes')) 
			{
				?>
				<a class="list-group-item" href="#/tools/notes">
					<i class="fa fa-sticky-note"></i> <?=$this->lang->line('tools_notes');?>
				</a>
				<?php 
			}
			?>
			<a class="list-group-item" target="_blank" href="/<?=APPFOLDERADD?>/libraries/miscelaneo/manual_intranet.pdf">
				<i class="fa fa-book"></i> <?=$this->lang->line('tools_manual');?>
			</a>
			<?php 
			if($this->Identity->Validate('analitycs/index')) 
			{
				?>
				<a class="list-group-item"  href="#/tools/analitycs">
					<i class="fa fa-bar-chart"></i> <?=$this->lang->line('tools_analitycs');?>
				</a>
				<?php 
			}
			?>
			<?php
			if($this->Identity->Validate('gamificationadmin/input/index')) 
			{
				?>
				<a class="list-group-item" href="/<?=FOLDERADD?>/tools/gamificationinput">
					<i class="fa fa-gamepad"></i> <?=$this->lang->line('tools_input_gamification');?>
				</a>
				<?php 
			}
			if($this->Identity->Validate('tools/export')){
				?>
				<a class="list-group-item" href="/<?=FOLDERADD?>/tools/exportToExcel">
					<i class="fa fa-file-excel-o"></i> <?=$this->lang->line('tools_input_export');?>
				</a>
				<?php
			}?>
		</div>
	</div>
</div>