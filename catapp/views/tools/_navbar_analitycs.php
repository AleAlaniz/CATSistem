<div class="page-title">
	<a href="#/tools" class="btn btn-white pull-right"><i class="fa fa-chevron-left"></i><span class="hidden-xs"> <?php echo $this->lang->line('general_goback'); ?></span></a>
	<h3 class="title"><?php echo $title ?></h3>
	<a href="#/tools/analitycs" class="btn btn-white"><?php echo $this->lang->line('tools_analitycs_daily');?> </a>
	<a href="#/tools/analitycsusers" class="btn btn-white"><?php echo $this->lang->line('tools_analitycs_perusers');?> </a>
	<a href="#/tools/analitycsonline" class="btn btn-white"><?php echo $this->lang->line('tools_analitycs_online');?> </a>
	<a href="#/tools/analitycslogin" class="btn btn-white"><?php echo $this->lang->line('tools_analitycs_login');?> </a>
</div>

