<ul class="nav nav-tabs nav-inbox">
	<?php 
	if($this->Identity->Validate('chat/unique')) { 
		$sql = "SELECT * FROM chatU WHERE fromuserId = ? || touserId = ?";
		$allChats = $this->db->query($sql, array($this->session->UserId, $this->session->UserId))->result();	
		$noRead = NULL;
		foreach ($allChats as $key => $value) {

			$sql = "SELECT * FROM chatuDeletes WHERE userId = ? && chatuId = ? && currentlyRemoved = 'FALSE' ";
			$lastDelete = $this->db->query($sql, array($this->session->UserId, $value->chatuId))->last_row();	
			if ($lastDelete == NULL) {
				$lastDelete = 0;
			}
			else
			{
				$lastDelete = $lastDelete->lastmessageIdDelete;
			}

			$sql = "SELECT * FROM chatuMessages WHERE chatuId = ? && chatumessageId > ?" ;
			$allMessages = $this->db->query($sql, array($value->chatuId, $lastDelete))->result();

			$sql = "SELECT * FROM chatuDeletes WHERE userId = ? && chatuId = ? && currentlyRemoved = 'TRUE' ";
			$value->chatuDelete = $this->db->query($sql, array($this->session->UserId, $value->chatuId))->last_row();	
			if ($value->chatuDelete != NULL) {
				unset($allChats[$key]);
			}
			else
			{
				foreach ($allMessages as $messageKey => $message) {
					if ($this->session->UserId != $message->fromuserId) {
						$sql = "SELECT * FROM chatumessageViews WHERE chatumessageId = ? && userId = ?";
						$isView = $this->db->query($sql, array($message->chatumessageId, $this->session->UserId))->row();
						if ($isView == NULL) {
							$noRead++;
						}
					}
				}
			}
		}
		?>
		<li role="presentation" id="chatUnique"><a href="/<?=FOLDERADD?>/chat"><i class="fa fa-comment-o"></i> <span class="hidden-xs"><?=$this->lang->line('chat_unique');?></span> <span class="badge" role="navuBadge"><?=$noRead ?></span></a></li>
		<?php 
	}
	if($this->Identity->Validate('chat/multi')) { 
		$noRead = NULL;
		$sql = "SELECT * FROM chatmUsers WHERE userId = ?";
		$allmChats = $this->db->query($sql, array($this->session->UserId))->result();	
		foreach ($allmChats as $key => $value) {

			$sql = "SELECT * FROM chatmMessages WHERE chatmId = ?" ;
			$allMessages = $this->db->query($sql, array($value->chatmId))->result();
			foreach ($allMessages as $messageKey => $message) {
				if ($this->session->UserId != $message->userId) {
					$sql = "SELECT * FROM chatmmessageViews WHERE chatmmessageId = ? && userId = ?";
					$isView = $this->db->query($sql, array($message->chatmmessageId, $this->session->UserId))->row();
					if ($isView == NULL) {
						$noRead++;
					}
				}
			}
		}
		?>
		<li role="presentation" id="chatMulti"><a href="/<?=FOLDERADD?>/chat/multi"><i class="fa fa-comments-o"></i> <span class="hidden-xs"><?=$this->lang->line('chat_multi');?> </span><span class="badge" role="navmBadge"><?=$noRead ?></span></a>
		</li>
		<?php 
	}
	?>
</ul>
