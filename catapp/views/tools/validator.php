<link href="/<?php echo APPFOLDERADD; ?>/libraries/css/animate.min.css" rel="stylesheet">
<div class="page-title" >
	<h3 class="title"><?php echo $this->lang->line('tools_prize_validator'); ?>   </h3>
	<a href="/<?=FOLDERADD?>#/tools" class="btn btn-white pull-right"><i class="fa fa-chevron-left"></i><span class="hidden-xs"> <?php echo $this->lang->line('general_goback'); ?></span></a>
</div>
<div class="col-xs-12 flat-style">
	<?php if (isset($_SESSION['flashMessage']))
	{
		?>
		<div class="alert bg-lightgreen alert-dismissible" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<strong><i class="fa fa-check"></i></strong> 
			<?php 
			if ($_SESSION['flashMessage'] == 'redeemed')
			{
				echo $this->lang->line('tools_prize_reddemedcomplete');
			}
			?>
		</div>
		<?php 
	}
	?>
	<div class="panel panel-default" > 
		<div class="panel-body row"  ng-app="validator" ng-controller="form" >
			<form method="POST" ng-submit="findVoucher()" style="overflow: auto;" class="col-xs-12" >
				<div class="col-xs-12">				
					<div class="material-input" ng-class="{focused: code != '' || codefocus == true}" >
						<input type="text" id="code" ng-model="code" ng-focus="codefocus=true;" ng-blur="codefocus=false;" ng-trim="true" />
						<label for="code" class="no-selectable"><?php echo $this->lang->line('tools_prize_code');?></label>
					</div>
					<p class="text-danger" ng-show="codeError"><?php echo $this->lang->line('tools_prize_coderequired'); ?></p>
				</div>
				<div class="col-xs-12 text-center ">
					<button type="submit" class="btn btn-lightgreen" ng-class="{disabled:finding}">
						<span  ng-show="finding"><i class="fa fa-spinner fa-spin fa-lg"></i>&nbsp;  </span>
						<?php echo $this->lang->line('tools_prize_findprize');?>
					</button>
					<p class="text-danger" ng-show="voucherError != ''" ng-bind="voucherError"></p>
				</div>
			</form>
			<div ng-show="voucher"  class="panel col-xs-12 col-sm-8 col-sm-offset-2 border-dark" style="overflow: auto;margin-top: 20px;">
				<div class="panel-body">
					<h4><?php echo $this->lang->line('tools_prize_userdata'); ?></h4>
					<hr/>
					<div class="row">
						<div class="col-sm-6">
							<strong><?php echo $this->lang->line('tools_prize_completename'); ?></strong>
						</div>
						<div class="col-sm-6">
							<span class="details-detail" ng-bind="voucher.user.completeName"></span>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6">
							<strong><?php echo $this->lang->line('tools_prize_photo'); ?></strong>
						</div>
						<div class="col-sm-6">
							<img style="max-height: 100%; max-width: 100%;" id="imgprev" src="/<?php echo FOLDERADD; ?>/users/profilephoto?userId={{voucher.user.userId}}&wah=200">

						</div>
					</div>
					<h4><?php echo $this->lang->line('tools_prize_prizedata'); ?></h4>
					<hr/>
					<div class="row">
						<div class="col-sm-6">
							<strong><?php echo $this->lang->line('tools_prize_code'); ?></strong>
						</div>
						<div class="col-sm-6">
							<span class="details-detail" ng-bind="voucher.code"></span>
						</div>
					</div>

					<div class="row">
						<div class="col-sm-6">
							<strong><?php echo $this->lang->line('tools_prize_name'); ?></strong>
						</div>
						<div class="col-sm-6">
							<span class="details-detail" ng-bind="voucher.prize.name"></span>
						</div>
					</div>

					<div class="row">
						<div class="col-sm-6">
							<strong><?php echo $this->lang->line('tools_prize_description'); ?></strong>
						</div>
						<div class="col-sm-6">
							<span class="details-detail" ng-bind="voucher.prize.description"></span>
						</div>
					</div>

					<div class="row">
						<div class="col-sm-6">
							<strong><?php echo $this->lang->line('tools_prize_photo'); ?></strong>
						</div>
						<div class="col-sm-6">
							<img style="max-height: 100%; max-width: 100%;" id="imgprev" src="/<?php echo FOLDERADD; ?>/gamification/getprizeimage/{{voucher.prize.prizeId}}">
						</div>
					</div>

					<div ng-show="voucher.redeemed == 'TRUE'">
						
						<h4><?php echo $this->lang->line('tools_prize_reddemeddata'); ?></h4>
						<hr/>
						<div class="row">
							<div class="col-sm-6">
								<strong><?php echo $this->lang->line('tools_prize_reddemeddate'); ?></strong>
							</div>
							<div class="col-sm-6">
								<span class="details-detail" ng-bind="voucher.redeemedDate"></span>
							</div>
						</div>
						<div class="row" ng-show="voucher.redeemedUser">
							<div class="col-sm-6">
								<strong><?php echo $this->lang->line('tools_prize_reddemeduser'); ?></strong>
							</div>
							<div class="col-sm-6">
								<span class="details-detail" ng-bind="voucher.redeemedUser.completeName"></span>
							</div>
						</div>

					</div>
					<div class="col-xs-12" ng-show="voucher.redeemed =='FALSE'" style="overflow: auto;margin-top: 20px;">
						<span class="btn btn-dark btn-block btn-sm" data-target="#confirm-redeem" data-toggle="modal">
							<?php echo $this->lang->line('tools_prize_reddem');?>
						</span>
					</div>
					<div class="modal animated shake" id="confirm-redeem" tabindex="-1" role="dialog">
						<div class="modal-dialog modal-sm ">
							<div class="modal-content">
								<div class="modal-body">
									<p><?php echo $this->lang->line('tools_prize_reddemconfirm')?></p>
									<div class="text-right">
										<form method="POST" action="#"  >
											<input type="hidden" value="{{voucher.voucherId}}" name="voucherId" />
											<span class="btn btn-white" data-dismiss="modal" aria-label="Close"><?php echo $this->lang->line('general_no'); ?></span>
											<button type="submit" class="btn btn-lightgreen">
												<?php echo $this->lang->line('general_yes');?>
											</button>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
					

				</div> 
			</div>
		</div>
	</div>
</div>


<script src="/<?php echo APPFOLDERADD; ?>/libraries/script/angular.min.js"></script>


<script type="text/javascript">


	$('#nav_tools').addClass('active');

	angular
	.module("validator", [])
	.controller("form", ["$scope",function($scope)
	{
		$scope.code 		= "";
		$scope.codefocus 	= false;
		$scope.finding 		= false;
		$scope.codeError	= false;
		$scope.voucher		= null;
		$scope.voucherError = "";

		$scope.findVoucher  = function() {
			$scope.codeError = false;

			if(!$scope.finding) 
			{
				$scope.voucherError = "";
				if ($scope.code != "") {
					$scope.finding	= true;
					$scope.voucher	= null;
					$.ajax({
						method 	: "POST",
						url		: '/<?php echo FOLDERADD; ?>/tools/findvoucher',
						data 	: {'code': $scope.code}
					}).done(function (data) {
						$scope.$apply($scope.voucherResponse(data));
					});
				}
				else
				{
					$scope.codeError = true;
				}
			}



		};

		$scope.voucherResponse = function(data) {
			$scope.finding = false;

			if(data == 'invalid'){
				$(location).attr('href', '/<?php echo FOLDERADD;?>');
			}
			else if(data == 'empty')
			{
				$scope.voucherError = "<?php echo $this->lang->line('tools_prize_voucherempty'); ?>";
			}
			else if(data == 'error')
			{
				$scope.voucherError = "<?php echo $this->lang->line('tools_prize_error'); ?>";
			}
			else
			{
				try{
					$scope.voucher = $.parseJSON(data);
					$scope.voucherError = "";
				}
				catch(e){
					$scope.voucherError = '<?php echo $this->lang->line("tools_prize_error"); ?>';
				}
			}
		};



	}]);	


</script>


