<div class="page-title">
	<a href="#/tools" class="btn btn-white pull-right"><i class="fa fa-chevron-left"></i><span class="hidden-xs"> <?php echo $this->lang->line('general_goback'); ?></span></a>
	<h3 class="title"><?php echo $this->lang->line('tools_notes'); ?></h3>
	<?php if($this->Identity->Validate('tools/notes/create')) 
	{ 
		?>
		<div class="btn-group">
			<span aria-expanded="false" class="btn btn-white dropdown-toggle" data-toggle="dropdown"><i class="fa fa-plus text-muted"></i> <?=$this->lang->line('tools_notes_create')?></span>
			<ul class="dropdown-menu dropdown-white dropdown-menu-scale" id="addNote">
				<li><a class="pointer" color="yelow"><i class="fa fa-circle fa-lg" style="color: #EBEE8E"></i>&nbsp; Amarillo</a></li>
				<li><a class="pointer" color="blue"><i class="fa fa-circle fa-lg" style="color: #2962ff"></i>&nbsp; Azul</a></li>
				<li><a class="pointer" color="pink"><i class="fa fa-circle fa-lg" style="color: #FF4081"></i>&nbsp; Rosa</a></li>
				<li><a class="pointer" color="green"><i class="fa fa-circle fa-lg" style="color: #0dceb5"></i>&nbsp; Verde</a></li>
				<li><a class="pointer" color="gray"><i class="fa fa-circle fa-lg" style="color: #eee"></i>&nbsp; Gris</a></li>
			</ul>
		</div>
		<?php 
	}
	?>
	<span class="btn btn-white" id="stack"><i class="fa fa-files-o text-muted"></i> <?= $this->lang->line('tools_notes_stack'); ?></span>
	<span class="btn btn-dark btn-title-normal" style="margin-bottom: 12px; display: none;" role="revert"><i class="fa fa-undo"></i> Revertir</span>
</div>
<div class="alert bg-yelow" role="alert">
	<strong><i class="fa fa-info-circle"></i></strong> 
	<?php echo $this->lang->line('tools_message'); ?>
</div>
<?php
if ($this->Identity->Validate('tools/notes/delete'))
{
	?>
	<div class="modal animated shake" id="confirm-delete" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-sm ">
			<div class="modal-content">
				<div class="modal-body">
					<p><?php echo $this->lang->line("tools_notes_deleteareyousure"); ?></p>
					<div class="text-right">
						<span class="btn btn-white" data-dismiss="modal" aria-label="Close"><?php echo $this->lang->line('general_no'); ?></span>
						<span class="btn btn-lightgreen" role="delete"><?php echo $this->lang->line('general_yes'); ?></span>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php
}
?>
<div class="modal animated shake" id="note-error" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-sm ">
			<div class="modal-content">
				<div class="modal-body">
					<p><?php echo $this->lang->line("tools_notes_error"); ?></p>
					<div class="text-right">
						<span class="btn btn-white" data-dismiss="modal" aria-label="Close"><?php echo $this->lang->line('general_goback'); ?></span>
					</div>
				</div>
			</div>
		</div>
	</div>
<div class="col-xs-12 flat-style">
	<div class="alert bg-pink" style="padding:7px;"><i class="fa fa-info-circle"></i> <?=$this->lang->line('tools_notes_tip')?></div>
	<div class="panel panel-default" style="margin-top: 19px">
		<div class="panel-body sticky-note-box">
			<span id="saving" class="fa fa-floppy-o fa-spin fa-2x" style="color:green; margin-left:5px; display:none; position:absolute; z-index:100"></span>
		</div>
	</div>
</div>
