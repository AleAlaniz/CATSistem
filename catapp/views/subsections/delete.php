<ol class="breadcrumb">
	<li><a href="/<?=FOLDERADD?>/administration"><?=$this->lang->line('general_administration');?></a></li>
	<li><a href="/<?=FOLDERADD?>/subsections"><?=$this->lang->line('general_subsections');?></a></li>
	<li><a href="/<?=FOLDERADD?>/subsections/details/<?=$subsectionId;?>"><?=encodeQuery($name);?></a></li>
	<li class="active"><?=$this->lang->line('administration_delete');?></li>
</ol>
<div class="col-xs-12" style="margin-bottom:15px">
	<?=$navBar?>
</div>
<div class="col-xs-12">
<div class="panel panel-default">
	<div class="panel-heading">
		<strong><?=$this->lang->line('administration_subsections_delete');?></strong>
	</div>
	<div class="panel-body">
		<h4><?=$this->lang->line('administration_subsections_delete_areyousure');?></h4>
		<hr>
		<dl class="dl-horizontal col-sm-6">
			<dt><?=$this->lang->line('administration_subsections_name');?></dt>
			<dd><?=encodeQuery($name)?></dd>
			<dt><?=$this->lang->line('administration_subsections_icon');?></dt>
			<dd><i class="fa <?=encodeQuery($icon)?>"></i></dd>
		</dl>
		<dl class="dl-horizontal col-sm-6">
				<?php 
				if (isset($section)) 
				{
					?>
					<dt><?=$this->lang->line('administration_subsections_section');?></dt>
					<dd><?=encodeQuery($section->name)?></dd>
					<?php 
				} 
				elseif (isset($subsection)) {
					?>
					<dt><?=$this->lang->line('administration_subsections_subsection');?></dt>
					<dd><?=encodeQuery($subsection->name)?></dd>
					<?php 
				} ?>
			</dl>
		<form method="POST"  >
			<input type="hidden" name="subsectionId" value="<?=$subsectionId?>">
			<div class="form-group text-center col-xs-12">
				<button type="submit" class="btn btn-success btn-sm"><?=$this->lang->line('general_delete');?></button>
				<a href="/<?=FOLDERADD?>/subsections" class="btn btn-danger btn-sm"><?=$this->lang->line('general_cancel');?></a>
			</div>
		</form>
	</div>
</div>
</div>
<script type="text/javascript">
$('#nav_subsections').addClass('active');
$('#subsectionNavDelete').addClass('active');
</script>