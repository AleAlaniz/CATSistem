<ul class="nav nav-tabs">
	<?php if($this->Identity->Validate('subsections/details')) { ?>
	<li role="presentation" id="subsectionNavDetails"><a href="/<?=FOLDERADD?>/subsections/details/<?=$subsectionId;?>"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> <span class="hidden-xs"><?=$this->lang->line('administration_details');?></span></a></li>
	<?php } ?>
	<?php if($this->Identity->Validate('subsections/edit')) { ?>
	<li role="presentation" id="subsectionNavEdit"><a href="/<?=FOLDERADD?>/subsections/edit/<?=$subsectionId;?>"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> <span class="hidden-xs"><?=$this->lang->line('administration_edit');?></span></a></li>
	<?php } ?>
	<?php if($this->Identity->Validate('subsections/delete')) { ?>
	<li role="presentation" id="subsectionNavDelete"><a href="/<?=FOLDERADD?>/subsections/delete/<?=$subsectionId;?>"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> <span class="hidden-xs"><?=$this->lang->line('administration_delete');?></span></a></li>
	<?php } ?>
</ul>