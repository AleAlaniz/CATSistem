<ol class="breadcrumb">
	<li><a href="/<?=FOLDERADD?>/administration"><?=$this->lang->line('general_administration');?></a></li>
	<li><a href="/<?=FOLDERADD?>/subsections"><?=$this->lang->line('general_subsections');?></a></li>
	<li><a href="/<?=FOLDERADD?>/subsections/details/<?=$subsectionId;?>"><?=encodeQuery($name);?></a></li>
	<li class="active"><?=$this->lang->line('administration_details');?></li>
</ol>
<?php if (isset($_SESSION['subsectionMessage'])): ?>
	<div class="alert alert-success alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<strong><i class="fa fa-check"></i></strong> 
		<?php if ($_SESSION['subsectionMessage'] == 'edit'){
			echo $this->lang->line('administration_subsections_editmessage');
		}
		?>
	</div>
<?php endif; ?>
<div class="col-xs-12" style="margin-bottom:15px">
	<?=$navBar?>
</div>
<div class="col-xs-12">
	<div class="panel panel-default">
		<div class="panel-heading">
			<strong><?=$this->lang->line('administration_subsections_details');?></strong>
		</div>
		<div class="panel-body">
			<dl class="dl-horizontal col-sm-6">
				<dt><?=$this->lang->line('administration_subsections_name');?></dt>
				<dd><?=encodeQuery($name)?></dd>
				<dt><?=$this->lang->line('administration_subsections_icon');?></dt>
				<dd><i class="fa <?=encodeQuery($icon)?>"></i></dd>
			</dl>
			<dl class="dl-horizontal col-sm-6">
				<?php 
				if (isset($section)) 
				{
					?>
					<dt><?=$this->lang->line('administration_subsections_section');?></dt>
					<dd><?=encodeQuery($section->name)?></dd>
					<?php 
				} 
				elseif (isset($subsection)) {
					?>
					<dt><?=$this->lang->line('administration_subsections_subsection');?></dt>
					<dd><?=encodeQuery($subsection->name)?></dd>
					<?php 
				} ?>
			</dl>
		</div>
	</div>
</div>
<script type="text/javascript">
$('#nav_subsections').addClass('active');
$('#subsectionNavDetails').addClass('active');
</script>