	<ol class="breadcrumb">
		<li><a href="/<?=FOLDERADD?>/administration"><?=$this->lang->line('general_administration');?></a></li>
		<li class="active"><?=$this->lang->line('general_subsections');?></li>
	</ol>

	<?php if($this->Identity->Validate('subsections/create')) { ?>
	<p>
		<a href="/<?=FOLDERADD?>/subsections/create" class="btn btn-sm btn-success "><i class="fa fa-plus"></i><strong> <?=$this->lang->line('administration_create');?></strong></a>
	</p>
	<?php } ?>
	<?php if (isset($_SESSION['subsectionMessage'])): ?>
	<div class="alert alert-success alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<strong><i class="fa fa-check"></i></strong> 
		<?php if ($_SESSION['subsectionMessage'] == 'create'){
			echo $this->lang->line('administration_subsections_successmessage');
		}
		elseif ($_SESSION['subsectionMessage'] == 'delete'){
			echo $this->lang->line('administration_subsections_deletemessage');
		}
		?>
	</div>
<?php endif; ?>
<div class="panel panel-default">
	<div class="panel-heading">
		<strong><?=$this->lang->line('general_subsections');?></strong>
		<span class="badge pull-right"><?=count($model)?></span>
	</div>
	<table class="table table-hover">
		<thead>
			<tr class="active">
				<th><?=$this->lang->line('administration_subsections_name');?></th>
				<th><?=$this->lang->line('administration_subsections_icon');?></th>
				<th><?=$this->lang->line('administration_subsections_section');?> / <?=$this->lang->line('administration_subsections_subsection');?></th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			<?php if (count($model) > 0) { ?>

			<?php foreach ($model as $subsection){

				?>
				<tr class="optionsUser">
					<td><?=$subsection->name?></td>
					<td><i class="fa <?=encodeQuery($subsection->icon)?>"></i></td>
					<td><?=encodeQuery($subsection->section->name)?></td>
					<td>
						<?php if($this->Identity->Validate('subsections/details')) { ?>
						<a href="/<?=FOLDERADD?>/subsections/details/<?=$subsection->subsectionId?>"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></a>&nbsp; 
						<?php } ?>
						<?php if($this->Identity->Validate('subsections/edit')) { ?>
						<a href="/<?=FOLDERADD?>/subsections/edit/<?=$subsection->subsectionId?>"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>&nbsp; 
						<?php } ?>
						<?php if($this->Identity->Validate('subsections/delete')) { ?>
						<a href="/<?=FOLDERADD?>/subsections/delete/<?=$subsection->subsectionId?>"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
						<?php } ?>
					</td>
				</tr>

				<?php } ?>

				<?php } else { ?>
				<tr class="text-center">
					<td colspan="3"><i class="fa fa-sitemap"></i> <?=$this->lang->line('administration_subsections_empty');?></td>
				</tr>

				<?php
			}?>
		</tbody>
	</table>
</div>
<script type="text/javascript">
$('#nav_subsections').addClass('active');
</script>