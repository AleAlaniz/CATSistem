<ol class="breadcrumb">
	<li><a href="/<?=FOLDERADD?>/administration"><?=$this->lang->line('general_administration');?></a></li>
	<li><a href="/<?=FOLDERADD?>/subsections"><?=$this->lang->line('general_subsections');?></a></li>
	<li><a href="/<?=FOLDERADD?>/subsections/details/<?=$subsectionId;?>"><?=encodeQuery($name);?></a></li>
	<li class="active"><?=$this->lang->line('administration_edit');?></li>
</ol>
<div class="col-xs-12" style="margin-bottom:15px">
	<?=$navBar?>
</div>
<div class="col-xs-12">
	<div class="panel panel-default">
		<div class="panel-heading">
			<strong><?=$this->lang->line('administration_subsections_edit');?></strong>
		</div>
		<div class="panel-body">
			<form class="form-horizontal" method="POST" novalidate >
				<input type="hidden" name="subsectionId" value="<?=$subsectionId?>">
				<div class="form-group">
					<label for="name" class="col-sm-2 control-label"><?=$this->lang->line('administration_subsections_name');?><span class="text-danger"><strong> *</strong></span></label>
					<div class="col-sm-10">
						<input type="text" class="form-control input-sm" id="name" name="name" value="<?php echo set_value('name', $name);?>" placeholder="<?=$this->lang->line('administration_subsections_name');?>" required>
						<?php echo form_error('name'); ?>
					</div>
				</div>
				<div class="form-group">
					<label for="appendto" class="col-sm-2 control-label"><?=$this->lang->line('administration_subsections_appendto');?><span class="text-danger"><strong> *</strong></span></label>
					<div class="col-sm-10">
						<?php
						$appendSection = FALSE;
						$appendSubsection = FALSE;
						if (isset($sectionId)) {
							$appendSection = TRUE;
						}
						elseif (isset($fereingsubsectionId)) {
							$appendSubsection = TRUE;
						}
						?>
						<div class="radio">
							<label>
								<input type="radio" name="appendto" value="section" <?=set_radio('appendto', 'section', $appendSection)?> >
								<?=$this->lang->line('administration_subsections_section');?>
							</label>
						</div>
						<div class="radio">
							<label>
								<input type="radio" name="appendto" value="subsection" <?=set_radio('appendto', 'subsection', $appendSubsection)?> >
								<?=$this->lang->line('administration_subsections_subsection');?>
							</label>
						</div>
						<?php echo form_error('appendto'); ?>
					</div>
				</div>
				<div class="form-group" id="sections">
					<label for="section" class="col-sm-2 control-label"><?=$this->lang->line('administration_subsections_section');?><span class="text-danger"><strong> *</strong></span></label>
					<div class="col-sm-10">
						<select class="form-control input-sm" id="section" name="section">
							<?php foreach($sections as $section){ 
								$sectionSelected = FALSE;
								if (isset($sectionId) && $sectionId == $section->sectionId) {
									$sectionSelected = TRUE;
								} ?>
							<option value="<?=$section->sectionId?>" <?php echo  set_select('section', $section->sectionId, $sectionSelected); ?> ><?=$section->name?></option>
							<?php 
						} ?>
					</select>
					<?php echo form_error('section'); ?>
				</div>


			</div>
			<div class="form-group" id="subsections">
				<label for="subsection" class="col-sm-2 control-label"><?=$this->lang->line('administration_subsections_subsection');?><span class="text-danger"><strong> *</strong></span></label>
				<div class="col-sm-10">
					<select class="form-control input-sm" id="subsection" name="subsection">
						<?php foreach($subSections as $subsection){ 
							$subsectionselected = FALSE;
							if (isset($fereingsubsectionId) && $fereingsubsectionId == $subsection->subsectionId) {
								$subsectionselected= TRUE;
							}?>
						<option value="<?=$subsection->subsectionId?>" <?php echo  set_select('subsection', $subsection->subsectionId, $subsectionselected); ?> ><?=$subsection->name?></option>
					<?php } ?>
				</select>
				<?php echo form_error('subsection'); ?>
			</div>
		</div>
		<div class="form-group">
			<label for="icon" class="col-sm-2 control-label"><?=$this->lang->line('administration_subsections_icon');?></label>
			<div class="col-sm-10">
				<div class="input-group">
					<div class="input-group-addon"><i class="fa <?php echo set_value('icon', $icon);?>"></i></div>
					<input type="text" class="form-control input-sm" id="icon" name="icon" value="<?php echo set_value('icon', $icon);?>" placeholder="<?=$this->lang->line('administration_subsections_icon');?>" required>
				</div>
				<a href="http://fortawesome.github.io/Font-Awesome/icons/" target="_blank">Ver iconos disponibles</a>
				<?php echo form_error('icon'); ?>
			</div>
		</div>
		<hr>
		<div class="form-group text-center">
			<button type="submit" class="btn btn-success btn-sm"><?=$this->lang->line('general_save');?></button>
			<a href="/<?=FOLDERADD?>/subsections" class="btn btn-danger btn-sm"><?=$this->lang->line('general_cancel');?></a>
		</div>
	</form>
</div>
</div>
</div>
<script type="text/javascript">
$('#nav_subsections').addClass('active');
$('#subsectionNavEdit').addClass('active');
$( "#icon" ).on('change keyup paste click',function() {
	$(this).prev().find('.fa').attr('class','fa '+ $( "#icon" ).val() )
});
if ($("[value='section']").prop('checked'))
{
	$('#sections').show();
	$('#subsections').hide();
}
else
{
	$('#sections').hide();
	$('#subsections').show();
}

$( "[name='appendto']" ).on('change',function() {
	if ($(this).val() == 'section')
	{
		$('#sections').slideDown();
		$('#subsections').slideUp();
	}
	else if ($(this).val() == 'subsection')
	{
		$('#sections').slideUp();
		$('#subsections').slideDown();
	}
});
</script>