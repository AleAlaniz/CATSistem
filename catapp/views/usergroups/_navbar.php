<ul class="nav nav-tabs">
	<?php if($this->Identity->Validate('usergroups/details')) { ?>
	<li role="presentation" id="NavDetails"><a href="/<?=FOLDERADD?>/usergroups/details/<?=$usergroupId;?>"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> <span class="hidden-xs"><?=$this->lang->line('administration_details');?></span></a></li>
	<?php } ?>
	<?php if($this->Identity->Validate('usergroups/edit')) { ?>
	<li role="presentation" id="NavEdit"><a href="/<?=FOLDERADD?>/usergroups/edit/<?=$usergroupId;?>"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> <span class="hidden-xs"><?=$this->lang->line('administration_edit');?></span></a></li>
	<?php } ?>
	<?php if($this->Identity->Validate('usergroups/delete')) { ?>
	<li role="presentation" id="NavDelete"><a href="/<?=FOLDERADD?>/usergroups/delete/<?=$usergroupId;?>"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> <span class="hidden-xs"><?=$this->lang->line('administration_delete');?></span></a></li>
	<?php } ?>
</ul>