<script>
	function userselectToggle(argument) {
		argument.click(function() {
			if (argument.parent().attr('id') == 'userFinds') {
				var selectExist = $('[user="'+argument.attr("user")+'"]', $('#userSelects'));
				var answerUser = $('[role="answerUser"]');
				if (selectExist.length > 0 || answerUser.attr('user') == argument.attr("user")) {
					alert('<?=$this->lang->line("inbox_new_usernoselectable");?>');
				}
				else
				{
					var newUser = $('<div/>', {
						'class' : 'btn userFinds-user label label-info',
						'user'    : argument.attr('user'),
					});
					newUser.html(argument.text() + ' <i class="fa fa-times text-danger"></i>');
					$('#userSelects').append(newUser);
					userselectToggle(newUser); 

					var newUserinput = $('<input/>', {
						'type' : 'hidden',
						'name' : 'usersId[]',
						'value'    : argument.attr('user'),
					});
					$('#outForm').prepend(newUserinput);
				}
			}
			else if(argument.parent().attr('id') == 'userSelects'){
				argument.remove();
				$('#outForm [value="'+argument.attr("user")+'"]').remove()
			}
		});
}
</script>
<ol class="breadcrumb">
	<li><a href="/<?=FOLDERADD?>/administration"><?=$this->lang->line('general_administration');?></a></li>
	<li><a href="/<?=FOLDERADD?>/usergroups"><?=$this->lang->line('administration_usergroups');?></a></li>
	<li class="active"><?=$this->lang->line('administration_create');?></li>
</ol>
<div class="panel panel-default">
	<div class="panel-heading">
		<strong><?=$this->lang->line('administration_usergroups_create');?></strong>
	</div>
	<div class="panel-body">
		<form class="form-horizontal" method="POST" novalidate id="outForm" >
			<div class="form-group">
				<label for="name" class="col-sm-2 control-label"><?=$this->lang->line('administration_usergroups_name');?><span class="text-danger"><strong> *</strong></span></label>
				<div class="col-sm-10">
					<input type="text" class="form-control input-sm" id="name" name="name" value="<?php echo set_value('name');?>" placeholder="<?=$this->lang->line('administration_usergroups_name');?>" required>
					<?php echo form_error('name'); ?>
					<p role="nameError" class="text-danger" style="display:none"><?=$this->lang->line('administration_usergroups_nameerror');?></p>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label" for="userLike" ><?=$this->lang->line('administration_usergroups_users');?><span class="text-danger"> *</span></label>
				<div class="col-sm-10">
					<div  id="findUser">
						<input autocomplete="off" type="text" class="form-control input-sm" id="userLike" placeholder="<?=$this->lang->line('administration_usergroups_findusers');?>">
						<p id="userFinds"></p>
					</div>
					<p id="userSelects"><span><?=$this->lang->line('administration_usergroups_usersselects');?>: </span> </p>
					<?php echo form_error('usersId[]'); ?>
				</div>
			</div>
			<div class="form-group">
				<label for="shared" class="col-sm-2 control-label"><?=$this->lang->line('administration_usergroups_shared');?></label>
				<div class="col-sm-10">
					<div class="checkbox" style="padding-left:21px">
						<input type="checkbox" name="shared" value="shared" >
					</div>
					<?php echo form_error('shared'); ?>

				</div>
			</div>
			<hr>
			<div class="form-group text-center">
				<button type="submit" class="btn btn-success btn-sm"><?=$this->lang->line('general_save');?></button>
				<a href="/<?=FOLDERADD?>/usergroups" class="btn btn-danger btn-sm"><?=$this->lang->line('general_cancel');?></a>
			</div>
		</form>
	</div>
</div>
<script type="text/javascript">
	$('#nav_usergroups').addClass('active');

	$('#userLike').keydown(function(e){
		var code = e.keyCode || e.which;
		if (code == 13) {
			e.preventDefault();
			return false;
		}
	});

	$('form').submit(function(e) {
		var $name = $('#name');
		if ($name.val() == '') {
			e.preventDefault();
			$('[role="nameError"]').show();
		}
		else
		{
			$('[role="nameError"]').hide();
		}
	});

	$('#userLike').on('keyup paste', function() {
		var datosForm = {
			userLike: $(this).val(),
		};

		$.ajax({
			method: "POST",
			url: '/<?php echo FOLDERADD;?>/home/findusers',
			data: datosForm,
		}).done(function (data) {

			if(data == 'invalid'){
				$(location).attr('href', '/<?php echo FOLDERADD;?>');
			}
			else if (data == 'nolike')
			{
				$('#userFinds').html('');
			}
			else if (data == 'empty') {
				$('#userFinds').html('<?=$this->lang->line("inbox_noresults")?>');
			}
			else
			{
				$('#userFinds').html(data);
			}

		});
	});

</script>