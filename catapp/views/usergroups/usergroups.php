
<ol class="breadcrumb">
	<li><a href="/<?=FOLDERADD?>/administration"><?=$this->lang->line('general_administration');?></a></li>
	<li class="active"><?=$this->lang->line('administration_usergroups');?></li>
</ol>
<?php if($this->Identity->Validate('usergroups/create')) { ?>
<p>
	<a href="/<?=FOLDERADD?>/usergroups/create" class="btn btn-sm btn-success "><i class="fa fa-plus"></i><strong> <?=$this->lang->line('administration_create');?></strong></a>
</p>
<?php }
if (isset($_SESSION['usergroupMessage']))
{ 
	?>
	<div class="alert alert-success alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<strong><i class="fa fa-check"></i></strong> 
		<?php if ($_SESSION['usergroupMessage'] == 'create'){
			echo $this->lang->line('administration_usergroups_successmessage');
		}
		elseif ($_SESSION['usergroupMessage'] == 'delete'){
			echo $this->lang->line('administration_usergroups_deletemessage');
		}
		?>
	</div>
	<?php 
}
?>
<div class="panel panel-default">
	<div class="panel-heading">

		<strong><?=$this->lang->line('administration_usergroups');?></strong>
		<span class="badge pull-right"><?=count($userGroups)?></span>
	</div>
	<table class="table table-hover">
		<thead>
			<tr class="active">
				<th><?=$this->lang->line('administration_usergroups_name');?></th>
				<th><?=$this->lang->line('administration_usergroups_shared');?></th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($userGroups as $userGroup): ?>
			<tr class="optionsUser">
				<td><?=encodeQuery($userGroup->name)?></td>
				<td><?php if($userGroup->shared == 'TRUE'){$userGroup->shared = 'Si';}else{$userGroup->shared = 'No';} echo encodeQuery($userGroup->shared); ?></td>
				<td>
					<?php if($this->Identity->Validate('usergroups/details')) { ?>
					<a href="/<?=FOLDERADD?>/usergroups/details/<?=$userGroup->usergroupId?>"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></a>&nbsp; 
					<?php } ?>
					<?php if($this->Identity->Validate('usergroups/edit')) { ?>
					<a href="/<?=FOLDERADD?>/usergroups/edit/<?=$userGroup->usergroupId?>"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>&nbsp; 
					<?php } ?>
					<?php if($this->Identity->Validate('usergroups/delete')) { ?>
					<a href="/<?=FOLDERADD?>/usergroups/delete/<?=$userGroup->usergroupId?>"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
					<?php } ?>
				</td>
			</tr>

		<?php endforeach; ?>
	</tbody>
</table>
</div>
<script type="text/javascript">
$('#nav_usergroups').addClass('active');
$('.optionsUser').hover(function(){
	$(this).find('.glyphicon').css('visibility','visible');

},function(){
	$(this).find('.glyphicon').css('visibility','hidden');

});
</script>