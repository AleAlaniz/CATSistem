<ol class="breadcrumb">
	<li><a href="/<?=FOLDERADD?>/administration"><?=$this->lang->line('general_administration');?></a></li>
	<li><a href="/<?=FOLDERADD?>/usergroups"><?=$this->lang->line('administration_usergroups');?></a></li>
	<li><a href="/<?=FOLDERADD?>/usergroups/details/<?=$usergroupId;?>"><?=encodeQuery($name);?></a></li>
	<li class="active"><?=$this->lang->line('administration_details');?></li>
</ol>
<?php if (isset($_SESSION['usergroupMessage'])): ?>
	<div class="alert alert-success alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<strong><i class="fa fa-check"></i></strong> 
		<?php if ($_SESSION['usergroupMessage'] == 'edit'){
			echo $this->lang->line('administration_usergroups_editmessage');
		}
		?>
	</div>
<?php endif; ?>
<div class="col-xs-12" style="margin-bottom:15px">
	<?=$navBar?>
</div>
<div class="col-xs-12">
	<div class="panel panel-default">
		<div class="panel-heading">
			<strong><?=$this->lang->line('administration_usergroups_details');?></strong>
		</div>
		<div class="panel-body">
			<dl class="dl-horizontal col-sm-6">
				<dt><?=$this->lang->line('administration_usergroups_name');?></dt>
				<dd><?=encodeQuery($name)?></dd>
				<dt><?=$this->lang->line('administration_usergroups_shared');?></dt>
				<dd><?php if($shared == 'TRUE'){$shared = 'Si';}else{$shared = 'No';} echo encodeQuery($shared); ?></dd>
			</dl>
			<dl class="dl-horizontal col-sm-6">
				<dt><?=$this->lang->line('administration_usergroups_users');?></dt>
				<?php
				foreach ($users as $userkey => $user) {
					?>
						<dd><?=encodeQuery($user->name)?> <?=encodeQuery($user->lastName)?></dd>	
					<?php
				}
				?>
			</dl>
		</div>
	</div>
</div>
<script type="text/javascript">
$('#nav_usergroups').addClass('active');
$('#NavDetails').addClass('active');
</script>