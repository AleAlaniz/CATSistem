<ol class="breadcrumb">
	<li><a href="/<?=FOLDERADD?>/administration"><?=$this->lang->line('general_administration');?></a></li>
	<li><a href="/<?=FOLDERADD?>/usergroups"><?=$this->lang->line('administration_usergroups');?></a></li>
	<li><a href="/<?=FOLDERADD?>/usergroups/details/<?=$usergroupId;?>"><?=encodeQuery($name);?></a></li>
	<li class="active"><?=$this->lang->line('administration_delete');?></li>
</ol>
<div class="col-xs-12" style="margin-bottom:15px">
	<?=$navBar?>
</div>
<div class="col-xs-12">
	<div class="panel panel-default">
		<div class="panel-heading">
			<strong><?=$this->lang->line('administration_usergroups_delete');?></strong>
		</div>
		<div class="panel-body">
			<h4><?=$this->lang->line('administration_usergroups_delete_areyousure');?></h4>
			<hr>
			<dl class="dl-horizontal col-sm-6">
				<dt><?=$this->lang->line('administration_usergroups_name');?></dt>
				<dd><?=encodeQuery($name)?></dd>
				<dt><?=$this->lang->line('administration_usergroups_shared');?></dt>
				<dd><?php if($shared == 'TRUE'){$shared = 'Si';}else{$shared = 'No';} echo encodeQuery($shared); ?></dd>
			</dl>
			<dl class="dl-horizontal col-sm-6">
				<dt><?=$this->lang->line('administration_usergroups_users');?></dt>
				<?php
				foreach ($users as $userkey => $user) {
					?>
					<dd><?=encodeQuery($user->name)?> <?=encodeQuery($user->lastName)?></dd>	
					<?php
				}
				?>
			</dl>
			<form method="POST"  >
				<input type="hidden" name="usergroupId" value="<?=$usergroupId?>">
				<div class="form-group text-center col-xs-12">
					<button type="submit" class="btn btn-success btn-sm"><?=$this->lang->line('general_delete');?></button>
					<a href="/<?=FOLDERADD?>/usergroups" class="btn btn-danger btn-sm"><?=$this->lang->line('general_cancel');?></a>
				</div>
			</form>
		</div>
	</div>
</div>
<script type="text/javascript">
$('#nav_usergroups').addClass('active');
$('#NavDelete').addClass('active');
</script>