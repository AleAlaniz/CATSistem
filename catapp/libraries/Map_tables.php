<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Map_tables {

	public function __construct()
	{
		$this->CI =& get_instance();
		$this->CI->lang->load('map_tables_lang');
	}

	public function get_table($tableName)
	{
		$table = array();

		switch ($tableName) {
			case 'genders':
			$table = array(
				'm' => $this->CI->lang->line('map_tables_gender_m'),
				'f' => $this->CI->lang->line('map_tables_gender_f')
				);
			break;
			case 'turns':
			$table = array(
				'm' => $this->CI->lang->line('map_tables_turn_m'),
				't' => $this->CI->lang->line('map_tables_turn_t'),
				'n' => $this->CI->lang->line('map_tables_turn_n'),
				'f' => $this->CI->lang->line('map_tables_turn_f')
				);
			break;
			case 'civilStates':
			$table = array(
				's' => $this->CI->lang->line('map_tables_civil_state_s'),
				'c' => $this->CI->lang->line('map_tables_civil_state_c'),
				'd' => $this->CI->lang->line('map_tables_civil_state_d'),
				'v' => $this->CI->lang->line('map_tables_civil_state_v'),
				'o' => $this->CI->lang->line('map_tables_civil_state_o')
				);
			break;
			case 'studyLevels':
			$table = array(
				's' => $this->CI->lang->line('map_tables_study_levels_s'),
				't' => $this->CI->lang->line('map_tables_study_levels_t'),
				'u' => $this->CI->lang->line('map_tables_study_levels_u'),
				'p' => $this->CI->lang->line('map_tables_study_levels_p')
				);
			break;
			case 'studyStatues':
			$table = array(
				'c' => $this->CI->lang->line('map_tables_study_statues_c'),
				'i' => $this->CI->lang->line('map_tables_study_statues_i'),
				'e' => $this->CI->lang->line('map_tables_study_statues_e'),
				'n' => $this->CI->lang->line('map_tables_study_statues_n')
				);
			break;
			case 'teamingFirms':
			$table = array(
				'6' => $this->CI->lang->line('map_tables_teaming_SL_firm'),
				'15' => $this->CI->lang->line('map_tables_teaming_BA_firm')
				);
			break;
		}

		return $table;
	}


	public function get_gender($gender)
	{
		$genders = $this->get_table('genders');
		return isset($genders[$gender]) ? $genders[$gender] : '';
	}

	public function get_turn($turn)
	{
		$turns = $this->get_table('turns');
		return isset($turns[$turn]) ? $turns[$turn] : '';
	}

	public function get_civil_state($civilState)
	{
		$civilStates = $this->get_table('civilStates');
		return isset($civilStates[$civilState]) ? $civilStates[$civilState] : '';
	}

	public function get_study_levels($studyLevel)
	{
		$studyLevels = $this->get_table('studyLevels');
		return isset($studyLevels[$studyLevel]) ? $studyLevels[$studyLevel] : '';
	}

	public function get_study_has_institution($studyLevel)
	{
		return $studyLevel == 's' ? FALSE : TRUE;
	}

	public function get_study_statues($studyStatus)
	{
		$studyStatues = $this->get_table('studyStatues');
		return isset($studyStatues[$studyStatus]) ? $studyStatues[$studyStatus] : '';
	}
}