(function() {
	'use strict';
	angular
	.module('catnet')
	.config(['$routeProvider', function($routeProvider) {
		$routeProvider
		.when('/gamification/ranking', {
			templateUrl : '/'+FOLDERADD+'/gamification/ranking',
			controller 	: 'Cranking'
		});
	}])
	.controller('Cranking'	, Cranking);

	Cranking.$inject = ['$scope' ,'nav'];
	function Cranking($scope, nav) {
		nav.setSelected('dashboard_d');

		$scope.medals 			= [];
		$scope.challenges		= [];
		$scope.achievements		= [];
		$scope.getRanking 		= getRanking;

		$scope.getRanking();
		function getRanking() {
			$('#ranking_containt').hide();
			$('#ranking_loading').show();
			$.ajax({
				method 	: 'GET',
				url 	: '/'+FOLDERADD+'/gamification/getranking',
				dataType: 'json'
			}).done(function(data) {
				if (data.status == 'ok') {	
					$scope.$apply(function() {
						$scope.medals 		= data.usersMedals;
						$scope.challenges 	= data.usersChallenges;
						$scope.achievements = data.usersAchievements;
					});
					$('#ranking_containt').show();
					$('#ranking_loading').hide();
				}
			});
		}
	}

})();