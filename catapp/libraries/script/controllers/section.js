(function()
{
	angular
	.module('catnet')
	.config(SectionConfig)
	.factory('FsharedData',FsharedData)
	.factory('sectionService',sectionService)
	.controller('Csection',Csection)
	.controller('Citems',Citems)
	.controller('CcreateNotice',CcreateNotice)
	.controller('CeditNotice',CeditNotice)
	.controller('Cnotices',Cnotices)
	.controller('Cdocs',Cdocs)
	.controller('CuploadData',CuploadData)
	.controller('CcreateFolder',CcreateFolder)
	.controller('CeditFolder',CeditFolder)
	.controller('CeditDoc',CeditDoc);


	function FsharedData()
	{	
		var methods=
		{
			addFavorite:function(id)
			{	
				$.ajax(
				{
					method:'POST',
					url:'/'+FOLDERADD+'/items/addfavorite',
					data:{'itemId':id},
					beforeSend:function()
					{
						beforeAddFavorite();
					}
				})
				.done(function(res)
				{
					successAddFavorite(res);
					return res;
				});
			}
		}

		return methods;
	}

	sectionService.$inject = ['$http','$rootScope'];
	function sectionService($http,$rootScope) {
		var sv = {
			unread : 0,
			getUnread : function () {
				$http
				.get('/'+FOLDERADD+'/sections/getunread')
				.success(function (data) {
					if(data.status == 'ok'){
						sv.unread = data.unreads;
						$rootScope.$broadcast('update_unread_sections');
					}
				})
			}
		}
		return sv;
	}

	SectionConfig.$inject=['$routeProvider'];
	function SectionConfig($routeProvider)
	{
		$routeProvider.
		when('/sections/section/:sectionId',
		{
			templateUrl:'/'+FOLDERADD+'/sections/section',
			controller:'Csection'
		})
		.when('/items/item/:itemId',{

			templateUrl:'/'+FOLDERADD+'/items/item',
			controller:'Citems'
		})
		.when('/items/item/:itemId/notices',{

			templateUrl:'/'+FOLDERADD+'/items/item',
			controller:'Cnotices'
		})
		.when('/items/item/:itemId/notice/create',{

			templateUrl:'/'+FOLDERADD+'/items/item',
			controller:'CcreateNotice'
		})
		.when('/items/item/:itemId/notice/:itemnoticeId/edit',{

			templateUrl:'/'+FOLDERADD+'/items/item',
			controller:'CeditNotice'
		})
		.when('/items/item/:itemId/docs',{

			templateUrl:'/'+FOLDERADD+'/items/item',
			controller:'Cdocs'
		})
		.when('/items/item/:itemId/docs/upload',{

			templateUrl:'/'+FOLDERADD+'/items/item',
			controller:'CuploadData'
		})
		.when('/items/item/:itemId/docs/:itemdocId/edit',{

			templateUrl:'/'+FOLDERADD+'/items/item',
			controller:'CeditDoc'
		})
		.when('/items/item/:itemId/docs/folder/:itemfolderId/edit',{

			templateUrl:'/'+FOLDERADD+'/items/item',
			controller:'CeditFolder'
		})
		.when('/items/item/:itemId/docs/folder/create',{

			templateUrl:'/'+FOLDERADD+'/items/item',
			controller:'CcreateFolder'
		});
	}

	CeditDoc.$inject=['$scope','$routeParams','FsharedData','nav','$location'];
	function CeditDoc($scope,$routeParams,FsharedData,nav,$location)
	{
		$scope.itemData=[];
		$scope.partialView="";
		$('#docsrightBar').addClass('active');

		getView();

		function getView()
		{
			$.ajax(
			{
				method:'POST',
				url:'/'+FOLDERADD+'/items/geteditdocview',
				data:{'itemdocId':$routeParams.itemdocId,'itemId':$routeParams.itemId},
				beforeSend:function()
				{
					$('#content').hide();
					$('#loading').show();
				}
			})
			.done(function(res)
			{
				if(data != "error")
				{	
					var data=jQuery.parseJSON(res);

					$scope.$apply(function()
					{
						$scope.itemData=data;
						$scope.partialView=data.partialView;
						nav.setSelected('sectionId-'+data.section.sectionId);
					});

					$('#content').show();
					$('#loading').hide();	
				}
				else
				{
					$location.path("#/");
					$scope.apply();
				}
			});
		}

		$scope.addFavorite=function()
		{
			if(FsharedData.addFavorite($scope.itemData.itemId) != "fail")
			{
				$scope.itemData.isFavorite=!$scope.itemData.isFavorite;
			}
		}
	}


	CeditFolder.$inject=['$scope','$routeParams','FsharedData','nav','$location'];
	function CeditFolder($scope,$routeParams,FsharedData,nav,$location)
	{
		$scope.itemData=[];
		$scope.partialView="";
		$('#docsrightBar').addClass('active');

		getView();

		function getView()
		{
			$.ajax(
			{
				method:'POST',
				url:'/'+FOLDERADD+'/items/geteditfolderview',
				data:{'itemfolderId':$routeParams.itemfolderId,'itemId':$routeParams.itemId},
				beforeSend:function()
				{
					$('#content').hide();
					$('#loading').show();
				}
			})
			.done(function(res)
			{
				if(data != "error")
				{	
					var data=jQuery.parseJSON(res);

					$scope.$apply(function()
					{
						$scope.itemData=data;
						$scope.partialView=data.partialView;
						nav.setSelected('sectionId-'+data.section.sectionId);
					});
					$('#content').show();
					$('#loading').hide();	
				}
				else
				{
					$location.path("#/");
					$scope.apply();
				}
			});
		}

		$scope.addFavorite=function()
		{
			if(FsharedData.addFavorite($scope.itemData.itemId) != "fail")
			{
				$scope.itemData.isFavorite=!$scope.itemData.isFavorite;
			}
		}
	}


	CcreateFolder.$inject=['$scope','$routeParams','FsharedData','nav','$location'];
	function CcreateFolder($scope,$routeParams,FsharedData,nav,$location)
	{
		$scope.itemData=[];
		$scope.partialView="";
		$('#docsrightBar').addClass('active');

		getView();

		function getView()
		{
			$.	ajax(
			{
				method:'POST',
				url:'/'+FOLDERADD+'/items/getcreatefolderview',
				data:{'itemId':$routeParams.itemId},
				beforeSend:function()
				{
					$('#content').hide();
					$('#loading').show();
				}
			})
			.done(function(res)
			{
				if(res != "error")
				{		
					var data=jQuery.parseJSON(res);

					$scope.$apply(function()
					{
						$scope.itemData=data;
						$scope.partialView=data.partialView;
						nav.setSelected('sectionId-'+data.section.sectionId);
					});

					$('#content').show();
					$('#loading').hide();
				}
				else
				{
					$location.path("#/");
					$scope.$apply();
				}
			});
		}


		$scope.addFavorite=function()
		{
			if(FsharedData.addFavorite($scope.itemData.itemId) != "fail")
			{
				$scope.itemData.isFavorite=!$scope.itemData.isFavorite;
			}
		}
	}


	CuploadData.$inject=['$scope','$routeParams','FsharedData','nav','$location','realTime'];
	function CuploadData($scope,$routeParams,FsharedData,nav,$location,realTime)
	{
		$scope.itemData=[];
		$scope.partialView="";
		$('#docsrightBar').addClass('active');

		getView();

		function getView()
		{
			$.	ajax(
			{
				method:'POST',
				url:'/'+FOLDERADD+'/items/getuploaddataview',
				data:{'itemId':$routeParams.itemId},
				beforeSend:function()
				{
					$('#content').hide();
					$('#loading').show();
				}
			})
			.done(function(res)
			{
				if(res != "error")
				{		
					var data=jQuery.parseJSON(res);

					$scope.$apply(function()
					{
						$scope.itemData=data;
						$scope.partialView=data.partialView;		
						nav.setSelected('sectionId-'+data.section.sectionId);
					});

					$('#content').show();
					$('#loading').hide();
					$('#upload').click(function(){
						realTime.socket.emit('new_notice');
					})
				}
				else
				{
					$location.path("#/");
					$scope.$apply();
				}
			});
		}

		$scope.addFavorite=function()
		{
			if(FsharedData.addFavorite($scope.itemData.itemId) != "fail")
			{
				$scope.itemData.isFavorite=!$scope.itemData.isFavorite;
			}
		}
	}


	Cdocs.$inject=['$scope','$routeParams','FsharedData','nav','$location'];
	function Cdocs($scope,$routeParams,FsharedData,nav,$location)
	{
		$scope.itemData=[];
		$scope.partialView="";
		$('#docsrightBar').addClass('active');

		getDocs();

		function getDocs()
		{
			$.	ajax(
			{
				method:'POST',
				url:'/'+FOLDERADD+'/items/getdocs',
				data:{'itemId':$routeParams.itemId},
				beforeSend:function()
				{
					$('#content').hide();
					$('#loading').show();
				}
			})
			.done(function(res)
			{
				if(res != "fail" && res != "error")
				{		
					var data=jQuery.parseJSON(res);

					$scope.$apply(function()
					{
						$scope.itemData=data;
						$scope.partialView=data.partialView;	
						nav.setSelected('sectionId-'+data.section.sectionId);
					});

					$('#content').show();
					$('#loading').hide();
				}
				else
				{
					$location.path("#/");
					$scope.$apply();
				}
			});
		}

		$scope.addFavorite=function()
		{
			if(FsharedData.addFavorite($scope.itemData.itemId) != "fail")
			{
				$scope.itemData.isFavorite=!$scope.itemData.isFavorite;
			}
		}

		$scope.getDocs=getDocs;
	}


	Cnotices.$inject=['$scope','$routeParams','FsharedData','nav','$location'];
	function Cnotices($scope,$routeParams,FsharedData,nav,$location)
	{
		$scope.itemData=[];
		$scope.partialView="";
		$('#noticesrightBar').addClass('active');

		getNotices();

		function getNotices()
		{
			$.ajax(
			{
				method:'POST',
				url:'/'+FOLDERADD+'/items/getnotices',
				data:{'itemId':$routeParams.itemId},
				beforeSend:function()
				{
					$('#content').hide();
					$('#loading').show();
				}
			})
			.done(function(res)
			{
				if(res !="error")
				{		
					var data=jQuery.parseJSON(res);

					$scope.$apply(function()
					{
						$scope.itemData=data;
						$scope.partialView=data.partialView;
						nav.setSelected('sectionId-'+data.section.sectionId);
					});

					$('#content').show();
					$('#loading').hide();
				}
				else
				{
					$location.path("#/");
					$scope.$apply();
				}
			});
		}

		$scope.addFavorite=function()
		{
			if(FsharedData.addFavorite($scope.itemData.itemId) != "fail")
			{
				$scope.itemData.isFavorite=!$scope.itemData.isFavorite;
			}
		}

		$scope.getNotices=getNotices;

	}

	CeditNotice.$inject=['$scope','$routeParams','FsharedData','tinyMCE','nav','$location'];
	function CeditNotice($scope,$routeParams,FsharedData,tinyMCE,nav,$location)
	{
		$scope.itemData=[];
		$scope.partialView="";
		$('#noticesrightBar').addClass('active');

		getView();

		function getView()
		{
			$.ajax(
			{
				method:'POST',
				url:'/'+FOLDERADD+'/items/geteditnoticeview',
				data:{'itemnoticeId':$routeParams.itemnoticeId,'itemId':$routeParams.itemId},
				beforeSend:function()
				{
					$('#content').hide();
					$('#loading').show();
				}
			})
			.done(function(res)
			{
				if(data != "error")
				{	
					var data=jQuery.parseJSON(res);

					$scope.$apply(function()
					{
						$scope.itemData=data;
						$scope.partialView=data.partialView;
						nav.setSelected('sectionId-'+data.section.sectionId);
					});
					tinyMCE.init('#details');
					$('#content').show();
					$('#loading').hide();	
				}
				else
				{
					$location.path("#/");
					$scope.$apply();
				}
			});
		}

		$scope.addFavorite=function()
		{
			if(FsharedData.addFavorite($scope.itemData.itemId) != "fail")
			{
				$scope.itemData.isFavorite=!$scope.itemData.isFavorite;
			}
		}
	}

	CcreateNotice.$inject=['$scope','$routeParams','FsharedData','tinyMCE','nav','$location','realTime'];
	function CcreateNotice($scope,$routeParams,FsharedData,tinyMCE,nav,$location,realTime)
	{
		$scope.itemData=[];
		$scope.partialView="";
		$('#noticesrightBar').addClass('active');

		getView();

		function getView()
		{
			$.ajax(
			{
				method:'POST',
				url:'/'+FOLDERADD+'/items/getcreatenoticeview',
				data:{'itemId':$routeParams.itemId},
				beforeSend:function()
				{
					$('#content').hide();
					$('#loading').show();
				}
			})
			.done(function(res)
			{
				if(res != "error" && res != "NotAvailableToCreate")
				{	
					var data=jQuery.parseJSON(res);

					$scope.$apply(function()
					{
						$scope.itemData=data;
						$scope.partialView=data.partialView;
						nav.setSelected('sectionId-'+data.section.sectionId);
					});
					tinyMCE.init('#details');
					$('#content').show();
					$('#loading').hide();
					$('#createNotice').click(function () {
						realTime.socket.emit('new_notice');
					})
				}
				else
				{
					$location.path("#/");
					$scope.$apply();
				}
			});
		}

		$scope.addFavorite=function()
		{
			if(FsharedData.addFavorite($scope.itemData.itemId) != "fail")
			{
				$scope.itemData.isFavorite=!$scope.itemData.isFavorite;
			}
		}
	}

	Citems.$inject=['$scope','$routeParams','FsharedData','nav','$location','$rootScope'];
	function Citems($scope,$routeParams,FsharedData,nav,$location,$rootScope)
	{
		$scope.itemData=[];
		$scope.partialView="";
		$('#informationrightBar').addClass('active');

		getItem();

		function getItem()
		{
			$.	ajax(
			{
				method:'POST',
				url:'/'+FOLDERADD+'/items/getitem',
				data:{'itemId':$routeParams.itemId},
				beforeSend:function()
				{
					$('#content').hide();
					$('#loading').show();
				}
			})
			.done(function(res)
			{
				if(res == "fail" || res == "error")
				{
					$location.path("#/");
					$scope.$apply();
				}
				else
				{
					var data=jQuery.parseJSON(res);

					$scope.$apply(function()
					{
						$scope.itemData=data;
						$scope.partialView=data.partialView;
						
						nav.setSelected('sectionId-'+data.section.sectionId);
					});

					$('#content').show();
					$('#loading').hide();
				}
			});
		}

		$scope.addFavorite=function()
		{
			if(FsharedData.addFavorite($scope.itemData.itemId) != "fail")
			{
				$scope.itemData.isFavorite=!$scope.itemData.isFavorite;
			}
		}

		$scope.goBack = function(typeid,sectionId) {
			var data = new Object();
			if(typeid == 'subSection'){
				data['subSectionId'] = $scope.itemData.subSection.subsectionId;
				if(typeof $scope.itemData.sssection != 'undefined')
					data['sssectionId'] =  $scope.itemData.sssection.subsectionId;
			}
			else{
				data['sssectionId'] =  $scope.itemData.sssection.subsectionId;
			}
			$rootScope.data = {};
			$rootScope.data = data;
			$rootScope.sectionId = sectionId;
		}
	}


	Csection.$inject=['$scope','nav','$routeParams','$rootScope','sectionService','realTime'];
	function Csection($scope,nav,$routeParams,$rootScope,sectionService,realTime)
	{
		nav.setSelected('sectionId-'+$routeParams.sectionId);

		$scope.itemsData=[];
		$scope.moreItems=[];
		if($rootScope.sectionId != $routeParams.sectionId){
			$rootScope.data = {};
		}
		getSections()

		

		function getSections()
		{
			$.ajax(
			{
				method:'POST',
				url:'/'+FOLDERADD+'/sections/getsubitems',
				data:{'sectionId':$routeParams.sectionId},
				beforeSend:function()
				{
					$('#content').hide();
					$('#loading').show();
				}
			})
			.done(function(res)
			{
				sectionService.getUnread();
				var data=jQuery.parseJSON(res);

				$scope.$apply(function()
				{
					$scope.siteTitle=data.name;
					$scope.itemsData=data;
					setTimeout(() => {
						sumSpansSSubsection();
					}, 300);
				});
				$('#content').show();
				$('#loading').hide();
				if (typeof $rootScope.data != 'undefined'){
					var sssectionId = $rootScope.data['sssectionId'];
					$('#subsectionId-'+sssectionId).next().slideToggle();

					if($rootScope.data['subSectionId']!= null){
						var subSectionId = $rootScope.data['subSectionId'];
						$('#subsectionId-'+subSectionId).next().slideToggle();
					}
				}

				realTime.suscribe('section');
				$scope.$on("update_unread_sections",function () {
					$scope.unread = sectionService.unread;
				})
			});
		}


		$scope.showSubItems=function(id)
		{
			$('#'+id).next().slideToggle();
		}

		$scope.showMoreSubItems=function(id)
		{
			$.ajax(
			{
				method:'POST',
				url:'/'+FOLDERADD+'/sections/getmoreitems',
				data:{'subSectionId':id},
				beforeSend:function()
				{
					$('#waitId-'+id).show();
				}
			})
			.done(function(res)
			{
				$scope.$apply(function()
				{
					$scope.moreItems=jQuery.parseJSON(res);
				});

				$('#waitId-'+id).hide();
				$('#'+id).next().slideToggle();

			});	
		}

		$scope.addToView = function (itemId) {
			$.ajax({
			method 	: 'POST',
			url 	: '/'+FOLDERADD+'/sections/addtoview',
			data 	: {'itemId': itemId}
			}).done(function(data) {
				if(data == "success"){
					sectionService.getUnread();
				}
			});
		}

		$scope.sumSpans = function (count,subsectionId,sssection) {
			var counts = $('.count-'+subsectionId);
			for (let i = 0; i < counts.length; i++) {
				const element = counts[i];
				count += parseInt(element.innerHTML);
			}
			if(count > 0){
				if(sssection){
					if(!$('#subsectionId-'+subsectionId)[0].innerHTML.includes(`<span class="messagesnumber ssubsection-${subsectionId}">${count}</span>`)){
						$('#subsectionId-'+subsectionId).append(`<span class="messagesnumber ssubsection-${subsectionId}">${count}</span>`)
					}
				}
				else{
					if(!$('#subsectionId-'+subsectionId)[0].innerHTML.includes(`<span class="messagesnumber subsection-${subsectionId}">`)){
						$('#subsectionId-'+subsectionId).append(`<span class="messagesnumber subsection-${subsectionId}">${count}</span>`)
					}
				}
			}
		}

		function sumSpansSSubsection() {
			for (let i = 0; i < $scope.itemsData.subSections.length; i++) {
				var count = 0;
				const s = $scope.itemsData.subSections[i];
				for (let j = 0; j < s.sssections.length; j++) {
					const ss = s.sssections[j];
					if($('.ssubsection-'+ss.subsectionId).length == 1){
						count += parseInt($('.ssubsection-'+ss.subsectionId)[0].innerHTML);
					}
				}
				if(count > 0){
					if($('.subsection-'+s.subsectionId).length > 0){
						var countHtml = parseInt($('.subsection-'+s.subsectionId)[0].innerHTML);
						$('.subsection-'+s.subsectionId)[0].innerHTML = countHtml+count;
					}
					else{
						$('#subsectionId-'+s.subsectionId).append(`<span class="messagesnumber subsection-${s.subsectionId}">${count}</span>`)
					}
				}
			}
		}
		
	}

	function beforeAddFavorite()
	{
		$('#addFavorite').hide();
		$('#wait').show();
	}

	function successAddFavorite(res)
	{
		if(res != "fail")
		{
			$('#addFavorite').show();
			$('#wait').hide();
		}
	}

})()