(function() {
	'use strict';
	angular
	.module('catnet')
	.config(['$routeProvider', function($routeProvider) {
		$routeProvider
		.when('/chat/unique', {
			templateUrl	: '/'+FOLDERADD+'/chat/unique',
			controller 	: 'Cchatu'
		})
		.when('/chat/unique/:id', {
			templateUrl	: '/'+FOLDERADD+'/chat/unique',
			controller 	: 'Cchatu'
		})
		.when('/chat/newunique', {
			templateUrl	: '/'+FOLDERADD+'/chat/newunique',
			controller 	: 'Cnewchatu'
		})
		.when('/chat/search', {
			templateUrl	: '/'+FOLDERADD+'/chat/search',
			controller 	: 'Csearch'
		})
		;
	}])
	.controller ('Cchatu'		, Cchatu)
	.controller ('Cnewchatu'	, Cnewchatu)
	.controller ('Csearch'		, Csearch)
	.factory 	('chatsu'		, FchatsU)
	.directive 	('schrollBottom', function () {
		return {
			scope: {
				schrollBottom: "="
			},
			link: function (scope, element) {
				scope.$watchCollection('schrollBottom', function (newValue) {
					if (newValue)
					{
						
						setTimeout(function() {
							$(element).scrollTop($(element)[0].scrollHeight - 20);
						}, 2000);
						
					}
				});
			}
		}
	});

	Cnewchatu.$inject = ['$scope', 'nav', '$location', 'realTime', 'chatsu', 'chatsm'];
	function Cnewchatu($scope, nav, $location, realTime, chatsu, chatsm) {
		nav.setSelected('chat');
		$scope.message = '';

		$scope.submitChat 	= submitChat;
		$scope.unreadu 		= chatsu.unread;
		$scope.unreadm 		= chatsm.unread;

		$scope.$on("update_unread_chatu", function() {
			$scope.unreadu = chatsu.unread;
		});

		$scope.$on("update_unread_chatm", function() {
			$scope.unreadm = chatsm.unread;
		});

		function submitChat() {
			var incomplete = false;
			var haveTo = false;

			if ($('input[name="user"]').length <= 0 || $scope.message == '') {
				incomplete = true;
			}

			if(incomplete)
			{
				$('#no-complete').modal('show');
			}
			else
			{
				$('#chatu_create').attr('disabled', 'disabled');
				$('#chatu_create').html("<i class='fa fa-refresh fa-spin fa-lg fa-fw'></i>");
				var data = {
					message : $scope.message,
					user 	: $('input[name="user"]').val()
				};
				$.ajax({
					method 		: 'POST',
					url 		: '/'+FOLDERADD+'/chat/newunique',
					data 		: data,
					dataType 	: 'json'
				}).done(function(data) {
					if (data.status == 'ok') {
						var datas = {
							id 		: data.chat.chatuId,
							message : data.message,
							to 		: [data.chat.fromuserId, data.chat.touserId]
						};	
						realTime.socket.emit('chatu_message', datas);
						$location.path('/chat/unique/'+data.chat.chatuId);
						$scope.$apply()
					}
				})

			}
		}

		$scope.finding = false;
		$scope.users = [];
		$scope.removeUser = function(i) {
			$scope.users.splice(i, 1);
		};
		$scope.addUser = function(i) {
			$scope.users = [];
			$scope.users.push($scope.userOptions[i]);
			$scope.userOptions = [];
			$scope.findLike = '';

		};
		$scope.userOptions = [];
		$scope.findLike = '';
		$scope.searchNumber = 0;
		$scope.searchActual = 0;
		$scope.searchUsers = function() {
			if($scope.findLike != '') 
			{
				$scope.finding = true;
				var thisSearch = $scope.searchNumber;
				$scope.searchNumber = $scope.searchNumber+1;
				$.ajax({
					method 	: "POST",
					url		: '/'+FOLDERADD+'/chat/searchusers',
					data 	: {'like': $scope.findLike},
					dataType : 'json'
				}).done(function (data) {
					if(data.status == 'ok' && thisSearch >= $scope.searchActual && $scope.findLike != '')
					{
						$scope.searchActual = thisSearch;
						$scope.$apply(function() {
							$scope.userOptions = data.users;
							$scope.finding = false;
						});
					}
					else if (thisSearch >= $scope.searchActual)
					{
						$scope.searchActual = thisSearch;
						$scope.$apply(function() {
							$scope.finding = false;
							$scope.userOptions = [];	
						});
					}
				});
			}
			else
			{
				$scope.userOptions = [];
				$scope.finding = false;
			}
		};

		$scope.selectClick = function (e) {
			$('.select-input', e.currentTarget).focus();
		};
	}

	Cchatu.$inject = ['$scope' ,'nav', 'chatsu', 'realTime', '$routeParams', '$filter', '$location', 'chatsm'];
	function Cchatu($scope, nav, chatsu, realTime, $routeParams, $filter, $location, chatsm) {
		nav.setSelected('chat');
		$scope.chatfilter 		= '';
		$scope.chatIdSelected 	= $routeParams.id;
		$scope.unreadu 			= chatsu.unread;
		$scope.unreadm 			= chatsm.unread;

		$scope.getOldMessages 	= getOldMessages;
		$scope.sendMessage		= sendMessage;
		
		$scope.$on("update_unread_chatu", function() {
			$scope.unreadu = chatsu.unread;
		});

		$scope.$on("update_unread_chatm", function() {
			$scope.unreadm = chatsm.unread;
		});

		$scope.$on('$destroy', destroy)

		$scope.$on('chatu_deletedChat',function(){
			if($scope.chatIdSelected){
				chatsu
				.getChatById($scope.chatIdSelected)
				.success(getChatSuccess);
			}
		})

		$('#inputText').keypress(function(e) {
			if(e.which == 13) {
				alert('You pressed enter!');
			}
		});

		chatsu
		.getChats()
		.success(function(data) {
			if (data.status == 'ok') {
				$scope.chats = data.chats;
				realTime.suscribe('chatsu');
				if ($scope.chatIdSelected) {
					chatsu
					.getChatById($scope.chatIdSelected)
					.success(getChatSuccess);
				}
				$scope.$on('new_chatu_message', function() {
					chatsu
					.getChats()
					.success(function(data) {
						if (data.status == 'ok') {
							$scope.chats = data.chats;
						}
					});
					if($scope.chatIdSelected){
						chatsu
						.getChatById($scope.chatIdSelected)
						.success(getChatSuccess);
					}
				});
			}
		});


		function getChatSuccess(data) {
			if (data.status == 'ok') {
				$scope.chat 			= data.chat;
				$scope.chat.messages 	= data.chat.messages;
				$scope.$on('chatu_message '+$scope.chat.chatuId, function(e,m) {
					AppendMessage(m, true);
				});
				realTime.suscribe('chatu', $scope.chat.chatuId);
				if (data.chat.unreads) {
					chatsu.getUnread();
				}

				//set de emojipicker
				setTimeout(() => {
					$("#inputText").emoji({

						// show emoji groups
						showTab: true,
						button: '#emojiPicker',
						// 'fade', slide' or 'none'
						animation: 'none',

						icons: [{
							name: "Emoji", // Emoji name
							path: "catapp/libraries/emoji/img/tieba/",
							maxNum: 50,
							file: ".jpg", // file extension name
							placeholder: ":{alias}:",
							excludeNums: [], // exclude emoji icons
							title: {}, // titles of emoji icons
							alias: {
								1: "hehe",
								2: "haha",
								3: "tushe",
								4: "a",
								5: "ku",
								6: "lu",
								7: "kaixin",
								8: "han",
								9: "lei",
								10: "heixian",
								11: "bishi",
								12: "bugaoxing",
								13: "zhenbang",
								14: "qian",
								15: "yiwen",
								16: "yinxian",
								17: "tu",
								18: "yi",
								19: "weiqu",
								20: "huaxin",
								21: "hu",
								22: "xiaonian",
								23: "neng",
								24: "taikaixin",
								25: "huaji",
								26: "mianqiang",
								27: "kuanghan",
								28: "guai",
								29: "shuijiao",
								30: "jinku",
								31: "shengqi",
								32: "jinya",
								33: "pen",
								34: "aixin",
								35: "xinsui",
								36: "meigui",
								37: "liwu",
								38: "caihong",
								39: "xxyl",
								40: "taiyang",
								41: "qianbi",
								42: "dnegpao",
								43: "chabei",
								44: "dangao",
								45: "yinyue",
								46: "haha2",
								47: "shenli",
								48: "damuzhi",
								49: "ruo",
								50: "OK"
							},
						},
						{
							name: "GIFS",
							path: "catapp/libraries/emoji/img/qq/",
							maxNum: 91,
							excludeNums: [41, 45, 54],
							file: ".gif",
							placeholder: "#qq_{alias}#"
						}, 
						{
							name: "WSEmojis",
							path: "catapp/libraries/emoji/img/emoji/",
							maxNum: 84,
							file: ".png",
							placeholder: "#emoji_{alias}#"
						}]
					});	
				}, 300);
				

			}
		}

		function AppendMessage(message, apply) {
			if (message.userId != userData.userId) {	
				chatsu.viewMessage(message.chatumessageId);
			}

			var exist = $filter('filter')($scope.chat.messages, {chatumessageId : message.chatumessageId});
			if (exist && exist.length == 0) {
				if ($scope.chat.chatuId == message.chatuId) {	
					$scope.chat.messages.push(message);
					if (apply) {$scope.$apply();}
				}
			}
		}


		$('#confirm-delete').on('show.bs.modal', function (event) {
			var id 	= $(event.relatedTarget).data('id')
			$(this).find('#delete-yes').data('chatid', id);
		});

		$('#delete-yes').click(function(e) {
			var btn = $(this);
			btn.hide();
			$('#delete-loading').show();
			chatsu
			.deleteChat(btn.data('chatid'))
			.success(deleteChatSuccess);
		});

		function sendMessage() {
			$scope.chat.newmessage = $('#inputText').html();
			
			if ($scope.chat.newmessage && $scope.chat.newmessage != '') {
				if(!$scope.chat.deleted){
					var data = { 'id' : $scope.chatIdSelected, 'message': $scope.chat.newmessage};
					chatsu
					.sendMessage(data)
					.success(function(data) {
						if (data.status == 'ok') {
							AppendMessage(data.message);
							var data = {
								id 		: data.message.chatuId,
								message : data.message,
								to 		: [$scope.chat.fromuserId, $scope.chat.touserId]
							};
							realTime.socket.emit('chatu_message', data);
						}
						$('#inputText').html("");
					});
					
				}
				
			}
			$scope.chat.newmessage = '';
			console.log('su mensaje ha sido enviado');

		}

		function getOldMessages() {
			$scope.gettingOldMessages = true;
			var first = $filter('orderBy')($scope.chat.messages, 'timestamp')[0].chatumessageId;
			chatsu
			.getOldMessages($scope.chatIdSelected,first)
			.success(getOldMessagesSuccess);
		}

		function getOldMessagesSuccess(data) {
			if (data.status == 'ok') {
				$scope.chat.messages = $scope.chat.messages.concat(data.messages);
				$scope.gettingOldMessages = false;
			}
			else if (data.status == 'empty') {
				$scope.fullOldMessages = true;
				$scope.gettingOldMessages = false;
			}
		}

		function deleteChatSuccess(data) {
			if (data.status == 'ok') {
				$('#confirm-delete').modal('hide');
				var datas = {
					id 		: data.chat.chatuId,
					to 		: [data.chat.fromuserId, data.chat.touserId]
				};
				realTime.socket.emit('chatu_deletedChat',datas);
				$location.path('/chat/unique')
			}
		}

		function destroy() {
			if ($scope.chatIdSelected) {
				realTime.unsuscribe('chatu', $scope.chatIdSelected);
			}
			realTime.unsuscribe('chatsu');
		}
		
		$scope.getMessageInfo = message =>{
			$scope.infoUsers = [];
			chatsu.getMessageInfo(message)
			.success(res=>{
				if(res.status == "success"){
					$scope.infoUsers = res.data;
				}
			})
		}

		$(document).on('keypress',function(e) {
			if(e.which == 13) {
				$scope.sendMessage();
				
			}
		});
	}

	Csearch.$inject = ['$scope', 'nav', '$location','realTime','chatsu'];
	function Csearch($scope, nav,  realTime, $location,chatsu) {
		nav.setSelected('chat');
		
		$scope.chats = [];
		$scope.teamLeaders = [];
		$scope.loadingAll = false;
		$scope.loading = false;

		// setup de datepicker
		$('.input-daterange input').datepicker({language: 'es',autoclose: true});

		//setup de searchusers
		$scope.users = [];
		$scope.searchUsersUrl = "/"+FOLDERADD+'/Users/searchUsers/';
		
		//setup de desplegable de teamleaders
		chatsu.getTeamLeaders()
		.success(response=>{
			if(response.status == "ok"){
				// $scope.loadingAll = false;
				$scope.teamLeaders = response.data;
			}
		});

		$scope.validateLink = (chat)=>{
			$scope.link = chat.type == "unique" ? `#/chat/unique/${chat.chatuId}&s=s` : `#/chat/multi/${chat.chatmId}&s=s`;
		}

		$scope.submitSearch = function () {

			$("#no_users").css('display', 'none');
			
			if ($scope.users.length == 0){
				
				$("#no_users").css('display', 'block');
			}
			else{

				let data = {
					keyword		: $scope.keyword,
					dateStart	: $scope.dateStart,
					dateFinish	: $scope.dateFinish,
					teamLeader	: $scope.teamLeader,
					users 		: [],
				}

				if($scope.users.length > 0){

					for (var i = $scope.users.length - 1; i >= 0; i--) {
						data.users.push($scope.users[i].userId);
					}
				}

				$scope.chats = [];
				$scope.noResults = false;
				chatsu.searchSubmit(data)
				.success(data=>{
					$scope.loading = false;
					if(data.status == "success"){
						$scope.chats = data.chats;
					}
					else{
						$scope.noResults = true;
					}
				})

			}
			
		}

	}

	FchatsU.$inject = ['$rootScope', '$http'];
	function FchatsU($rootScope, $http) {
		var sv = {
			unread 					: 0,
			getChats 				: function() {
				return $http.get('/'+FOLDERADD+'/chat/getchatsu');
			},
			getChatById				: function(id) {
				return $http.get('/'+FOLDERADD+'/chat/getchatubyid?id='+id);
			},
			getOldMessages	: function(id, first) {
				return $http.get('/'+FOLDERADD+'/chat/getoldmessages?id='+id+'&first='+first);
			},
			deleteChat				: function(id) {
				return $http.get('/'+FOLDERADD+'/chat/deletechatu?id='+id);
			},
			sendMessage				: function(data) {
				return $http.post('/'+FOLDERADD+'/chat/newmessageu', data);
			},
			getNewMessages	: function(id, last) {
				return $http.get('/'+FOLDERADD+'/chat/getnewmessagesu?id='+id+'&last='+last);
			},
			viewMessage	: function(id) {
				return $http.get('/'+FOLDERADD+'/chat/addmessageuview?id='+id );
			},
			getUnread 	: function() {
				$http
				.get('/'+FOLDERADD+'/chat/getunreadu')
				.success(function(data) {
					if (data.status == 'ok') {
						sv.unread = data.unread;
						$rootScope.$broadcast('update_unread_chatu');
					}
				})
			},
			getMessageInfo 			: function (id) {
				let data = {
					id:id,
					type: 'unique'
				}
				return $http.post('/'+FOLDERADD+'/chat/getMessageInfo',data);
			},
			//funciones de la vista busqueda
			getTeamLeaders			: function () {
				// $scope.loadingAll = true;
				return $http.get('/'+FOLDERADD+'/users/getTl');
			},
			searchSubmit			: function (data) {
				// $scope.loading = true;
				return $http.post('/'+FOLDERADD+'/chat/searchChats',data)
			}
		}
		return sv;
	}


})();