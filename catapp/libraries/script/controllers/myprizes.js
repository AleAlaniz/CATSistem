(function() {
	'use strict';
	angular
	.module('catnet')
	.config(['$routeProvider', function($routeProvider) {
		$routeProvider
		.when('/gamification/myprizes', {
			templateUrl	: '/'+FOLDERADD+'/gamification/myprizes',
			controller 	: 'Cmyprizes'
		});
	}])
	.controller('Cmyprizes'	, Cmyprizes);

	Cmyprizes.$inject = ['$scope' ,'nav'];
	function Cmyprizes($scope, nav) {
		nav.setSelected('dashboard_d');
		$scope.vouchers 	= [];
		$scope.noRedeemed 	= {'redeemed':'FALSE'};
		$scope.redeemed 	= {'redeemed':'!FALSE'};
		$scope.getMyPrizes 	= getMyPrizes;

		$scope.getMyPrizes();

		function getMyPrizes() {
			$.ajax({
				method 	: 'GET',
				url 	: '/'+FOLDERADD+'/gamification/getmyprizes',
				dataType: 'json'
			}).done(function(data) {
				if (data.status == 'ok') {	
					$scope.$apply(function() {
						$scope.vouchers = data.vouchers;
					});
				}
			});
		}
	}


})();