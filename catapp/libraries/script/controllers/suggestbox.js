(function() {
	'use strict';

	angular
	.module 	('catnet')
	.config 	(sugestboxConfig)
	.factory	('sugestboxService'	, sugestboxService 	)
	.controller	('Csuggestbox'		, Csuggestbox 		)
	.controller	('Csuggestbox_view'	, Csuggestbox_view 	);
	
	sugestboxConfig.$inject = ['$routeProvider'];
	function sugestboxConfig($routeProvider) {
		$routeProvider.
		when('/suggestbox', {
			templateUrl: '/'+FOLDERADD+'/suggestbox',
			controller: 'Csuggestbox'
		}).
		when('/suggestbox/view/:id', {
			templateUrl: function(params){ return '/'+FOLDERADD+'/suggestbox/view/'+params.id;},
			controller: 'Csuggestbox_view',
			cache : false
		}).
		when('/suggestbox/view', {
			templateUrl: '/'+FOLDERADD+'/suggestbox/view/',
			controller: 'Csuggestbox_view',
			cache : false
		});
	}
	sugestboxService.$inject = ['$http', '$rootScope'];
	function sugestboxService($http, $rootScope) {
		var sv =  {
			unread 	: 0,
			getUnread 	: function() {
				$http
				.get('/'+FOLDERADD+'/suggestbox/getunread')
				.success(function(data) {
					if (data.status == 'ok') {
						sv.unread = data.unread;
						$rootScope.$broadcast('update_unread_suggestbox');
					}
				})
			}
		}
		return sv;
	}
	Csuggestbox.$inject = ['$scope','nav', 'sugestboxService', 'realTime'];
	function Csuggestbox($scope,nav, sugestboxService, realTime) {
		nav.setSelected('suggestbox');
		$scope.sections 	= [];
		$scope.mysuggest = null;
		$scope.suggestdata = {
			site 		: '',
			campaign 	: '',
			suggest 	: ''
		};
		$scope.message = '';
		$scope.suggestbox = {
			noRead : sugestboxService.unread
		};

		$scope.$on("update_unread_suggestbox", function() {
			$scope.suggestbox.noRead = sugestboxService.unread;
		});

		$scope.getMysuggest = function() {
			$('#suggestbox_mysuggest_loading').show();
			$('#suggestbox_mysuggest_container').hide();
			$scope.mysuggest 	= [];
			$.ajax({
				method	: "GET",
				url 	: "/"+FOLDERADD+"/suggestbox/getmysuggest"
			}).done(function(data) {
				try{data = $.parseJSON(data);}catch(e){return;}
				if (data.status == 'ok') {
					$scope.$apply(function() {
						$scope.mysuggest = data.suggests;
					});
					$('#suggestbox_mysuggest_loading').hide();
					$('#suggestbox_mysuggest_container').show();
				}
			});
		};
		$scope.getSections = function() {
			$scope.mysuggest 	= [];
			$scope.sections 	= [];
			$.ajax({
				method	: "GET",
				url 	: "/"+FOLDERADD+"/suggestbox/getsections"
			}).done(function(data) {
				try{data = $.parseJSON(data);}catch(e){return;};
				if (data.status == 'ok') {
					$scope.$apply(function() {
						$scope.sections = data.sections;
					});
				}
			});
		};
		$scope.sendSuggest = function() {
			if ($scope.suggestdata.site == "" ||$scope.suggestdata.campaign == "" ||$scope.suggestdata.suggest == "")
			{
				$('#no-complete').modal('show');
			}
			else
			{
				$('#suggestbox_sendsuggest').hide();
				$('#suggestbox_sendingsuggest').show();
				$.ajax({
					method 	: "POST",
					url 	: "/"+FOLDERADD+"/suggestbox/postsuggest",
					data 	: $scope.suggestdata
				}).done(function(data) {
					try{data = $.parseJSON(data);}catch(e){return;};
					if (data.status == 'ok') {
						realTime.socket.emit('new_suggestbox');
						$scope.$apply(function() {
							$scope.message = data.message;
							$scope.initCtrl();
						});
						$scope.suggestdata = {
							site 		: '',
							campaign 	: '',
							suggest 	: ''
						};
						$('#suggestbox_sendsuggest').show();
						$('#suggestbox_sendingsuggest').hide();
					}
				});
			}
		}
		$scope.initCtrl = function() {
			$scope.getMysuggest();
			$scope.getSections();
		};

		$scope.initCtrl();
	}	
	Csuggestbox_view.$inject = ['$scope','nav', '$routeParams', 'sugestboxService'];
	function Csuggestbox_view($scope, nav, $routeParams, sugestboxService) {
		nav.setSelected('suggestbox');
		$scope.sections 	= [];
		$scope.suggests 	= [];
		$scope.message 		= '';
		$scope.suggestbox = {
			noRead : sugestboxService.unread
		};

		$scope.$on("update_unread_suggestbox", function() {
			$scope.suggestbox.noRead = sugestboxService.unread;
		});
		$scope.sectionName 	= '';

		$scope.getSuggests = function() {
			$('#suggestbox_suggests_loading').show();
			$('#suggestbox_suggests_container').hide();
			$scope.suggests = [];
			var id = $routeParams.id || '';
			$.ajax({
				method	: "GET",
				url 	: "/"+FOLDERADD+"/suggestbox/getsuggests/"+id
			}).done(function(data) {
				try{data = $.parseJSON(data);}catch(e){return;};
				if (data.status == 'ok') {
					$scope.$apply(function() {
						$scope.suggests = data.suggests;
						if (data.sectionSelect) {
							$scope.sectionName = data.sectionSelect.name;
						}
					});
					$('#suggestbox_suggests_loading').hide();
					$('#suggestbox_suggests_container').show();
				}
			});
		};

		$scope.getSections = function() {
			$scope.mysuggest 	= [];
			$scope.sections 	= [];
			$.ajax({
				method	: "GET",
				url 	: "/"+FOLDERADD+"/suggestbox/getsections"
			}).done(function(data) {
				try{data = $.parseJSON(data);}catch(e){return;};
				if (data.status == 'ok') {
					$scope.$apply(function() {
						$scope.sections = data.sections;
					});
					sugestboxService.getUnread();

				}
			});
		};
		$('#confirm-take').on('show.bs.modal', function (event) {
			$('#takeok').show();
			$('#suggestbox_taking').hide();
			var button 		= $(event.relatedTarget) 
			var suggestId 	= button.data('suggest')
			var modal 		= $(this)
			modal.find('#takeok').data('url', '/'+FOLDERADD+'/suggestbox/take/'+suggestId);
		})
		$('#takeok').click(function() {
			$('#takeok').hide();
			$('#suggestbox_taking').show();
			$.ajax({
				method 	: "GET",
				url 	: $(this).data('url')
			}).done(function(data) {
				try{data = $.parseJSON(data);}catch(e){return;};
				if (data.status == 'ok') {
					$scope.message = data.message;
					$scope.initCtrl();
					$('#takeok').show();
					$('#suggestbox_taking').hide();
					$('#confirm-take').modal('hide');
				}
			});
		});
		$scope.initCtrl = function() {
			$scope.getSuggests();
			$scope.getSections();

		};
		$scope.initCtrl();
	}

})();