(function() {
	'use strict';

	angular
	.module('catnet')
	.controller('Cpanel',Cpanel);

	Cpanel.$inject = ['$scope' ,'nav', '$http'];
	function Cpanel($scope, nav, $http) {
		nav.setSelected('home');
		$scope.message = '';

		function loadWelcome() {
			$('#welcome_loading').show();
			$('#welcome_container').hide();
			$('#welcome_empty').hide();
			$http({
				method 	: 'get',
				url 	: '/'+FOLDERADD+'/home/getWelcome'
			}).success(function(data) {
				$('#welcome_loading').hide();
				if(data == "null"){
					$('#welcome_empty').show();
				}
				else{
					$('#welcome_container').show();
					$('#welcome_container').html(data.message);
				}

				loadAlerts();
			});
		}
		
		function loadAlerts() {
			$('#panel_alert_loading').show();
			$('#panel_alert_container').hide();
			$scope.alerts = [];
			$http
			.get("/"+FOLDERADD+"/home/getallalerts")
			.success(function(data) {
				if (data.status == 'ok') {
					$scope.alerts = data.alerts;
					$('#panel_alert_loading').hide();
					$('#panel_alert_container').show();
				}
			});
		};

		loadWelcome();

		$('#confirm-delete').on('show.bs.modal', function (event) {
			var id 	= $(event.relatedTarget).data('id')
			$(this).find('[role="deleteInfo"]').data('url','/'+FOLDERADD+'/home/deletealert/'+id);
		});

		$('[role="deleteInfo"]').click(function(e) {
			var btn = $(this);
			$http
			.get(btn.data('url'))
			.success(function(data) {
				if (data.status == 'ok') {
					$('#confirm-delete').modal('hide');
					$scope.message = data.message;
					$scope.loadAlerts();
				}
			});
		});

		function setCookie(cname, cvalue, exdays) {
			var d = new Date();
			d.setTime(d.getTime() + (exdays*24*60*60*1000));
			var expires = "expires="+ d.toUTCString();
			document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
		}

		function getCookie(cname) {
			var name = cname + "=";
			var decodedCookie = decodeURIComponent(document.cookie);
			var ca = decodedCookie.split(';');
			for(var i = 0; i <ca.length; i++) {
				var c = ca[i];
				while (c.charAt(0) == ' ') {
					c = c.substring(1);
				}
				if (c.indexOf(name) == 0) {
					return c.substring(name.length, c.length);
				}
			}
			return "";
		}

		function checkCookie() {
			var userId = $('#userId').val();
			var birthCookie = getCookie("birthDay"+userId);

			var birthDate = $('#birthDate').val();
			var arrDate = birthDate.split('-');
			birthDate = "";
			for (let i = 0; i < arrDate.length; i++) {
				const element = arrDate[i];
				if(element.substring(0,1) == '0'){
					birthDate+= element.substring(1);
				}
				else{
					birthDate+= element;
				}
				if(i < arrDate.length-1){
					birthDate+="-";
				}
			}
			var d = new Date();
			var actualDate = d.getDate() + "-" + (d.getMonth() +1);
			
			if(birthDate == actualDate){
				if (birthCookie == "") {
					setCookie('birthDay'+userId,d.getTime(),1);
					$('#birthDayDialog').modal({show : true});
				} 
			}
		}
		checkCookie();
	}
})();
