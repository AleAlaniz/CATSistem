(function() {
	'use strict';

	angular
	.module('catnet')
	.config(TeamingConfig)
	.controller('Cteaming', Cteaming)
	.controller('Cagreement', Cagreement)
	.controller('Cupdate', Cupdate)
	.controller('CteamingDivisions', CteamingDivisions)
	.controller('CteamingAdminForms', CteamingAdminForms)
	.controller('CteamingRecords', CteamingRecords)
	.controller('CteamingReports', CteamingReports)
	.controller('CteamingRemainingReports', CteamingRemainingReports)
	.controller('CteamingManualAgreementLoad', CteamingManualAgreementLoad)
	.controller('CteamingManualUpdateLoad', CteamingManualUpdateLoad)
	.controller('CteamingCommunicationReports', CteamingCommunicationReports)
	.controller('CteamingnewsletterReports', CteamingnewsletterReports)
	.controller('CteamingsurveyReports', CteamingsurveyReports)
	.controller('CteamingUserDetails', CteamingUserDetails)
	.controller('CteamingSuggestInstitution', CteamingSuggestInstitution)
	.controller('CteamingAdminInstitutions', CteamingAdminInstitutions)
	.controller('CteamingSuggestionDetails', CteamingSuggestionDetails)
	.factory("teamingAgreeService", teamingAgreeService)
	.factory("newCommunication", newCommunication)
	.factory("userData", userData)
	
	.filter("rangeAmount",function () {
		return function (items,from,to) {
			var result = [];
			if (from && to){
				for (var i = items.length - 1; i >= 0; i--) {
					if ((from < items[i]["amount"]) && (items[i]["amount"] < to)){
						result.push(items[i]);
					}
				}
			}
			else{
				result = items;
			}
			return result;
		}
	})//si no sirven eliminarlos!
	.filter("rangeDate",function () {
		return function (items,from,to,field) {
			var fromDate, toDate, compareDate, fromSplit, toSplit, compareSplit, result = [];
			if (from && to){

				fromSplit		=	from.split("/");
				toSplit			=	to.split("/");
				fromDate		=	new Date(fromSplit[2],fromSplit[1]-1,fromSplit[0]);
				toDate			=	new Date(toSplit[2],toSplit[1]-1,toSplit[0]);


				for (var i = items.length - 1; i >= 0; i--) {

					compareSplit = items[i][field].split("/");
					compareDate	 = new Date(compareSplit[2],compareSplit[1]-1,compareSplit[0]);
					if ((fromDate.getTime() <= compareDate.getTime()) && (compareDate.getTime() <= toDate.getTime())){
						result.push(items[i]);
					}
				}
			}
			else{
				result = items;
			}

			return result;
		}
	})
	.directive('onlyNumbers', function () {
		return function (scope, element, attrs) {
			element.bind("keypress", function (event) {
				var charCode = (event.which) ? event.which : event.keyCode;
				if (charCode > 31 && (charCode < 48 || charCode > 57))
					event.preventDefault();
			});
		};
	})
	.directive('celNumber', function () {
		return function (scope, element, attrs) {
			element.bind("keypress", function (event) {
				var charCode = (event.which) ? event.which : event.keyCode;
				if (charCode <= 47 || charCode >= 58){
					if(charCode != 43){
						if(charCode != 8){
							event.preventDefault();
						}
					}
				}
			});
		};
	})
	.directive('alphaInput', function () {
		return function (scope, element, attrs) {
			element.bind("keypress", function (event) {
				var charCode = (event.which) ? event.which : event.keyCode;
				if ((charCode > 47 && charCode < 58))
					event.preventDefault();
			});
		};
	});//si no sirven eliminarlos!

	TeamingConfig.$inject = ['$routeProvider'];
	function TeamingConfig($routeProvider) {
		$routeProvider
		.when('/teaming',{
			templateUrl:'/'+FOLDERADD+'/teaming',
			controller:'Cteaming'
		})
		.when('/teaming/agreement',{
			templateUrl:'/'+FOLDERADD+'/teaming/agreement',
			controller: 'Cagreement',
		})
		.when('/teaming/agreementTerms',{
			templateUrl:'/'+FOLDERADD+'/teaming/agreementTerms',
			controller:'Cteaming'
		})
		.when('/teaming/acceptedForm',{
			templateUrl:'/'+FOLDERADD+'/teaming/acceptedForm',
			controller:'Cteaming'
		})
		.when('/teaming/acceptedSuggestion',{
			templateUrl:'/'+FOLDERADD+'/teaming/acceptedSuggestion',
			controller:'Cteaming'
		})
		.when('/teaming/update',{
			templateUrl:'/'+FOLDERADD+'/teaming/update',
			controller:'Cupdate'
		})
		.when('/teaming/noForm',{
			templateUrl:'/'+FOLDERADD+'/teaming/noForm',
			controller:'Cteaming'
		})
		.when('/teaming/pendingApprove',{
			templateUrl:'/'+FOLDERADD+'/teaming/pendingApprove',
			controller:'Cteaming'
		})
		.when('/teaming/pendingUpdateApprove',{
			templateUrl:'/'+FOLDERADD+'/teaming/pendingUpdateApprove',
			controller:'Cteaming'
		})
		.when('/teaming/divisions',{
			templateUrl:'/'+FOLDERADD+'/teaming/divisions',
			controller:'CteamingDivisions'
		})
		.when('/teaming/pendingForms',{
			templateUrl:'/'+FOLDERADD+'/teaming/pendingForms',
			controller:'CteamingAdminForms'  
		})
		.when('/teaming/records',{
			templateUrl:'/'+FOLDERADD+'/teaming/records',
			controller:'CteamingRecords'
		})
		.when('/teaming/reports',{
			templateUrl:'/'+FOLDERADD+'/teaming/reports',
			controller:'CteamingReports'
		})
		.when('/teaming/remainingReports',{
			templateUrl:'/'+FOLDERADD+'/teaming/remainingReports',
			controller:'CteamingRemainingReports'
		})
		.when('/teaming/manualAgreementLoad',{
			templateUrl:'/'+FOLDERADD+'/teaming/manualAgreementLoad',
			controller:'CteamingManualAgreementLoad'
		})
		.when('/teaming/communicationReports',{
			templateUrl:'/'+FOLDERADD+'/teaming/communicationReports',
			controller:'CteamingCommunicationReports'
		})
		.when('/teaming/newsletterReports',{
			templateUrl:'/'+FOLDERADD+'/teaming/newsletterReports',
			controller:'CteamingnewsletterReports'
		})
		.when('/teaming/surveyReports',{
			templateUrl:'/'+FOLDERADD+'/teaming/surveyReports',
			controller:'CteamingsurveyReports'
		})
		.when('/teaming/manualUpdateLoad',{
			templateUrl:'/'+FOLDERADD+'/teaming/manualUpdateLoad',
			controller:'CteamingManualUpdateLoad'
		})
		.when('/teaming/details/:userId',{
			templateUrl:'/'+FOLDERADD+'/teaming/details',
			controller:'CteamingUserDetails'
		})
		.when('/teaming/sendSuggestion',{
			templateUrl:'/'+FOLDERADD+'/teaming/sendSuggestion',
			controller:'CteamingSuggestInstitution'
		})
		.when('/teaming/institutions',{
			templateUrl:'/'+FOLDERADD+'/teaming/institutions',
			controller:'CteamingAdminInstitutions'
		})
		.when('/teaming/suggestionDetails/:suggestionId',{
			templateUrl:'/'+FOLDERADD+'/teaming/suggestionDetails',
			controller:'CteamingSuggestionDetails'
		});
	}

	Cteaming.$inject = ['$scope' ,'nav', '$window', '$http'];
	function Cteaming($scope, nav, $window, $http) {

		nav.setSelected('teaming');
		$scope.alreadyJoined 	= false;
		$scope.PendingUpdate 	= false;
		$scope.petitionApproved = false;
		$scope.finishedCharging = false;

		$http({
			method: 'POST',
			url: 	'teaming/getUserStatus'
		}).success(function(data) {

			$scope.finishedCharging = true;

			if (data.status == 'ok'){

				$scope.alreadyJoined 	= data.message["alreadyJoined"];
				$scope.PendingUpdate 	= data.message["PendingUpdate"];
				$scope.petitionApproved = data.message["petitionApproved"];
			}
		})
	}

	Cagreement.$inject = ['$scope', '$http','$location','$window','realTime'];
	function Cagreement($scope, $http,$location,$window,realTime) {

		$window.scrollTo(0, 0);
		$scope.formData = 	{};
		$scope.name 	= 	"";
		$scope.lastname =	"";

		$http({
			method: 'GET',
			url : 'teaming/getNameAndDni'
		}).success(function (data) {
			if(data.status=='ok'){
				$scope.name  				= 	data.message["name"];
				$scope.lastName 			=	data.message["lastName"];
				$scope.formData["dni"]		=	data.message["dni"];
				$scope.formData["userId"]	=	data.message["userId"];
				$scope.formData["loadType"]	= 	'online';

			}
		})

		$scope.submit = function() {

			$scope.errors = "";

			$http({
				method  : 'POST',
				url     : 'teaming/sendAgreeForm',
				data    : $scope.formData,  
				headers : {'Content-Type': 'application/x-www-form-urlencoded'}

			}).success(function(data) {

				if ((data.status == 'invalid')||(data.status == 'alreadyJoined')) {

					$scope.errors = data.message;
					$location.path('/teaming');
				}
				else {

					realTime.socket.emit('new_teaming_form');
					$location.path('/teaming/acceptedForm');
				}
			});
		};		
	}

	Cupdate.$inject = ['$scope', '$http','$location','$window','realTime'];
	function Cupdate($scope, $http,$location,$window,realTime) {

		$window.scrollTo(0, 0);

		$scope.formData 		  = {};
		$scope.lastUpdateFormData = {};

		var updateId;

		$http({
			method: 'GET',
			url : 'teaming/getUserId'
		})
		.success(function(data){

			$scope.formData["userId"]	= data.message;
			updateId 					= data.message;

			$http({
				method	: 'POST',
				url	    : 'teaming/getUserLastData',
				data    : {"updateId" : $scope.formData}
			})
			.success(function (data) {

				if (data.status == 'ok'){

					$scope.formData["dni"]				=	data.message["latestUpdate"].updateDni;
					$scope.formData["areaCampaign"]		=	data.message["latestUpdate"].updateAreaCampaign;
					$scope.formData["mobileNumber"]		=	data.message["latestUpdate"].updateMobileNumber;
					$scope.formData["institutionName"]	=	data.message["latestUpdate"].updateInstitutionName;
					$scope.formData["amount"]			=	data.message["latestUpdate"].updateAmount;
					$scope.formData["amountCharacters"]	=	data.message["latestUpdate"].updateAmountCharacters;
					$scope.formData["loadType"]			=	'online';

					$scope.lastUpdateFormData["dni"]			  =	data.message["latestUpdate"].updateDni;
					$scope.lastUpdateFormData["areaCampaign"]	  =	data.message["latestUpdate"].updateAreaCampaign;
					$scope.lastUpdateFormData["mobileNumber"]	  =	data.message["latestUpdate"].updateMobileNumber;
					$scope.lastUpdateFormData["institutionName"]  =	data.message["latestUpdate"].updateInstitutionName;
					$scope.lastUpdateFormData["amount"]			  =	data.message["latestUpdate"].updateAmount;
					$scope.lastUpdateFormData["amountCharacters"] =	data.message["latestUpdate"].updateAmountCharacters;
					$scope.lastUpdateFormData["loadType"]		  =	'online';
					$scope.lastUpdateFormData["acceptterms"]	  =	'true';

					if (data.message["latestUpdate"].updateId){

						$scope.formData["id"]			=	data.message["latestUpdate"].updateId;
						$scope.lastUpdateFormData["id"]	=	data.message["latestUpdate"].updateId;
					}
					else{

						$scope.formData["id"]			=	"0";
						$scope.lastUpdateFormData["id"]	=	"0";
					}
				}
			});
		});

		$scope.toBlack = function (field) {

			$(field).removeClass("redify");
		}

		$scope.update = function() {

			$scope.errors = null;

			$http({
				method  : 'POST',
				url     : 'teaming/sendUpdateForm',
				data    : {'form': $scope.formData,'oldForm':$scope.lastUpdateFormData},
				headers : { 'Content-Type': 'application/x-www-form-urlencoded' }
			})
			.success(function(data) {

				if ((data.status == 'invalid')||(data.status == 'alreadyUpdated')) {

					$scope.errors = data.message;
					$location.path('/teaming');
				}
				else{

					realTime.socket.emit('new_teaming_form');
					$location.path('/teaming/acceptedForm');
				}
			});
		};
	}	


	CteamingDivisions.$inject = ['$scope','$http' ,'nav', '$location','$window'];
	function CteamingDivisions($scope,$http, nav,$location,$window) {

		nav.setSelected('teaming');

		$scope.sections 	  = [];
		$scope.showSaveButton = false;

		getDivisions();

		function getDivisions() {
			$http({
				method	: 'POST',
				url	    : 'teaming/getDivisionsStatus'
			})
			.success(function(data){

				if(data.status=="success"){

					$scope.sections = data.message;
					$scope.showSaveButton = true;
				}
			});
		}

		$scope.submitDivision = function() {

			$scope.showSaveButton = false;
			$scope.updateSuccess  = "";

			$http({
				method	: 'POST',
				url	    : 'teaming/updateDivisionsStatus',
				data 	:  $scope.sections,
			}).success(function(data){

				if(data.status=="success"){

					getDivisions();
					$scope.updateSuccess = '<p style="color:rgb(66,166,42);font-size:14px;"> Las divisiones fueron actualizadas exitosamente </p>'
				}
			});
		}
	}

	CteamingAdminForms.$inject = ['$scope', "$http" ,'$window','nav', '$timeout', '$location','realTime'];
	function CteamingAdminForms($scope, $http,$window, nav, $timeout, $location,realTime) {

		nav.setSelected('teaming');

		$scope.loading  		= true;
		$scope.radioPressed  	= false;
		$scope.pendingAgree  	= false;
		$scope.pendingUpdate 	= false;
		$scope.oldUpdateform 	= {};
		$scope.oldAgreeform  	= {};
		$scope.agreeFormsNum 	= 0;		
		$scope.updateFormsNum 	= 0;	

		var agreeform 		 = {},
		updateform 			 = {}, 
		agreeIndex 			 = -1, 
		updateIndex 		 = -1;

		getAdminforms();

		function getAdminforms(){

			$http({
				method  : 'POST',
				url     : 'teaming/getFormsAdmin'
			})
			.success(function(data) {

				if (data.status == 'ok') {
					$scope.agreeForms = data.message['agreeForms'];
					$scope.updateForms = data.message['updateForms'];


					var date 	= new Date(),
					thisDay	 	= date.getDate(),
					thisMonth	= date.getMonth() + 1,
					keys 		= Object.keys($scope.agreeForms);

					$scope.agreeFormsNum = keys.length;

					keys 		= Object.keys($scope.updateForms);

					$scope.updateFormsNum = keys.length;
					
					if (parseInt(thisDay) >= 1 && parseInt(thisDay) <= 9){
						thisDay= "0"+thisDay;
					}

					if (parseInt(thisMonth) >= 1 && parseInt(thisMonth) <= 9){
						thisMonth= "0"+thisMonth;
					}

					angular.forEach($scope.agreeForms, function(agree){
						agree.value = false;
						agree.agreementDate= thisDay+ "/" + thisMonth + "/" + date.getFullYear();

					});
					
					angular.forEach($scope.updateForms, function(update){
						update.value = false;
						update.updateDate = thisDay+ "/" + thisMonth + "/" + date.getFullYear() ;
					});

					if($scope.agreeForms.length> 0){
						$scope.pendingAgree = true;
					}

					if($scope.updateForms.length > 0){
						$scope.pendingUpdate = true;
					}

					$timeout(function() {
						$('.datepicker').datepicker({ 
							autoclose : true, 
							language: 'es'
						});
					});
					$timeout(function() {
						$('.datatable-join').DataTable({
							language: {
								"sProcessing":     "Procesando...",
								"sLengthMenu":     "Mostrar _MENU_ solicitudes de adhesión",
								"sZeroRecords":    "No se encontraron resultados",
								"sInfo":           "Mostrando solicitudes de adhesión del _START_ al _END_ de un total de _TOTAL_ solicitudes de adhesión",
								"sInfoEmpty":      "Mostrando solicitudes de adhesión del 0 al 0 de un total de 0 solicitudes de adhesión",
								"sInfoFiltered":   "(filtrado de un total de _MAX_ solicitudes de adhesión)",
								"sInfoPostFix":    "",
								"sSearch":         "Buscar:",
								"sUrl":            "",
								"sInfoThousands":  ",",
								"sLoadingRecords": "Cargando...",
								"oPaginate": {
									"sFirst":    "Primero",
									"sLast":     "Último",
									"sNext":     "Siguiente",
									"sPrevious": "Anterior"
								},
								"oAria": {
									"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
									"sSortDescending": ": Activar para ordenar la columna de manera descendente"
								}
							}
						});
					});
					$timeout(function() {
						$('.datatable-update').DataTable({
							language: {
								"sProcessing":     "Procesando...",
								"sLengthMenu":     "Mostrar _MENU_ solicitudes de actualización de monto",
								"sZeroRecords":    "No se encontraron resultados",
								"sInfo":           "Mostrando solicitudes de actualización de monto del _START_ al _END_ de un total de _TOTAL_ solicitudes de actualización de monto",
								"sInfoEmpty":      "Mostrando solicitudes de actualización de monto del 0 al 0 de un total de 0 solicitudes de actualización de monto",
								"sInfoFiltered":   "(filtrado de un total de _MAX_ solicitudes de actualización de monto)",
								"sInfoPostFix":    "",
								"sSearch":         "Buscar:",
								"sUrl":            "",
								"sInfoThousands":  ",",
								"sLoadingRecords": "Cargando...",
								"oPaginate": {
									"sFirst":    "Primero",
									"sLast":     "Último",
									"sNext":     "Siguiente",
									"sPrevious": "Anterior"
								},
								"oAria": {
									"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
									"sSortDescending": ": Activar para ordenar la columna de manera descendente"
								}
							}
						});
					});
				}
				$scope.loading = false;
			});
		}
		

		$scope.agreeFormValueChange = function(agreeForm,position) {

			$scope.oldAgreeform 	= {};
			$scope.oldUpdateform  	= {};
			updateform 				= {};
			$scope.radioPressed 	= true;
			var index				= 0;

			angular.forEach($scope.updateForms, function(update){
				update.value = false;
			});
			angular.forEach($scope.agreeForms, function(agree,index){

				if(position !== index){
					agree.value = false;
				}
				else{
					if (agree.value == false){
						$scope.radioPressed 	= false;
					}
					else{
						agreeIndex								= 	index;
						agreeform 								= 	agree;
						$scope.oldAgreeform["dni"]  			= 	agree.dni;
						$scope.oldAgreeform["amount"]  		   	= 	agree.amount;
						$scope.oldAgreeform["amountCharacters"] = 	agree.amountCharacters;
						$scope.oldAgreeform["acceptterms"]  	= 	agree.acceptterms;
						$scope.oldAgreeform["areaCampaign"]  	= 	agree.areaCampaign;
						$scope.oldAgreeform["mobileNumber"]  	= 	agree.mobileNumber;
						$scope.oldAgreeform["institutionName"]  = 	agree.institutionName;
						$scope.oldAgreeform["userId"] 			= 	agree.userId;
						$scope.oldAgreeform["id"] 				= 	agree.id;
						$scope.oldAgreeform["loadType"] 		= 	agree.loadType;
					}	
				}
			});
		}

		$scope.updateFormValueChange = function(updateForm,position){

			$scope.oldAgreeform 	= {};
			$scope.oldUpdateform 	= {};
			agreeform 				= {};
			var index 				= 0;
			$scope.radioPressed 	= true;

			angular.forEach($scope.agreeForms, function(agree){
				agree.value = false;
			});
			angular.forEach($scope.updateForms, function(update,index){
				if(position !== index){
					update.value = false;
				}
				else{
					if (update.value == false){
						$scope.radioPressed 	= false;
					}
					else{
						updateIndex	= index;
						updateform 	= update;
						$http({
							method	: 'POST',
							url	    : 'teaming/getUserLastData',
							data    : {"updateId" : update}
						})
						.success(function (data) {
							if (data.status == 'ok'){
								$scope.oldUpdateform["dni"] 					=	data.message["latestUpdate"].updateDni;
								$scope.oldUpdateform["areaCampaign"]			=	data.message["latestUpdate"].updateAreaCampaign;
								$scope.oldUpdateform["mobileNumber"]			=	data.message["latestUpdate"].updateMobileNumber;
								$scope.oldUpdateform["institutionName"]			=	data.message["latestUpdate"].updateInstitutionName;
								$scope.oldUpdateform["updatedAmount"]			=	data.message["latestUpdate"].updateAmount;
								$scope.oldUpdateform["updatedAmountCharacters"]	=	data.message["latestUpdate"].updateAmountCharacters;

								if (data.message["latestUpdate"].updateId){
									$scope.oldUpdateform["id"]					=	data.message["latestUpdate"].updateId;
								}
								else{
									$scope.oldUpdateForm["id"]					=	"0";
								}

							}
						});
					}
				}
			});
		}

		$scope.editAvailable = function(){

			if (((agreeform)&&(agreeform.value == true)) || ((updateform)&&(updateform.value == true))){
				$scope.enable = true;
			}
			else{
				$scope.enable = false;
				$scope.errors = "<p style=\"color:red;\">Debe seleccionar un usuario</p>";
			}	
		}

		$scope.saveForm = function(){

			var form = {'agree': agreeform, 'update':updateform};

			$http({
				method	: 	'POST',
				url		:   'teaming/saveForm',
				data 	: 	form,
				headers : 	{'Content-Type': 'application/x-www-form-urlencoded'} 
			})
			.success(function(data){

				if (data.status == 'invalid'){
					$scope.errors = data.message;
				}
				else{
					$scope.enable = false;
					$scope.errors = "";
				}
			});
		}

		$scope.accept = function(){

			$scope.errors = "";

			if (((agreeform)&&(agreeform.value == true)) || ((updateform)&&(updateform.value == true))){

				$http({
					method	: 	'POST',
					url		:   'teaming/sendForm',
					data 	: 	{'agree': agreeform, 'update':updateform, 'is_accepted': true , 'oldAgreeform': $scope.oldAgreeform, 'oldUpdateform': $scope.oldUpdateform},
					headers : 	{'Content-Type': 'application/x-www-form-urlencoded'} 
				})
				.success(function(data){
					$('#removeAlert').modal('hide');
					$scope.radioPressed = false;
					realTime.socket.emit('new_teaming_form');
					$scope.enable = false;

					if(!angular.equals(agreeform, {})){

						$scope.agreeForms.splice(agreeIndex,1);
						var keys 	= Object.keys($scope.agreeForms);
						$scope.agreeFormsNum = keys.length;

						if ($scope.agreeFormsNum == 0){

							$('.datatable-join').DataTable().destroy();
						}
						else{

							$('.datatable-join').DataTable().row(agreeIndex).remove().draw();
						}
						
						agreeIndex = -1;
					}
					else{

						$scope.updateForms.splice(updateIndex,1);
						var keys 		= Object.keys($scope.updateForms);
						$scope.updateFormsNum = keys.length;

						if ($scope.updateFormsNum == 0){

							$('.datatable-update').DataTable().destroy();
						}
						else{

							$('.datatable-update').DataTable().row(updateIndex).remove().draw();
						}
						updateIndex = -1;
					}
					if (data.status == 'ok'){

						$scope.errors = "<p style='color:rgb(66,166,42);font-size:14px;'>El formulario fue aprobado exitosamente</p>";

					}
					else{

						$scope.errors = "<p style=\"color:red;\">El formulario ya fue aprobado.</p>";
					}
				});
			}
			else{

				$scope.errors = "<p style=\"color:red;\">Debe seleccionar un formulario</p>";
			}
		}
		
		$scope.showModal = function () {

			$scope.var_observation = null; 
			$scope.error_modal = false; 
			$('#removeAlert').modal('show');
		}

		$scope.reject = function(){

			$scope.errors = "";

			if($scope.var_observation){

				if (((agreeform)&&(agreeform.value == true)) || ((updateform)&&(updateform.value == true))){

					$http({
						method	: 	'POST',
						url		:   'teaming/sendForm',
						data 	: 	{
							'agree'			: agreeform, 
							'update'		: updateform, 
							'is_accepted'	: false, 
							'oldAgreeform'	: $scope.oldAgreeform, 
							'oldUpdateform' : $scope.oldUpdateform,
							'observation'	: $scope.var_observation},
							headers : 	{'Content-Type': 'application/x-www-form-urlencoded'} 
					}).success(function(data){

						$('#removeAlert').modal('hide');
						$scope.radioPressed = false;
						$scope.enable 		= false;
						realTime.socket.emit('new_teaming_form');
						if(!angular.equals(agreeform, {})){
							$scope.agreeForms.splice(agreeIndex,1);
							var keys 	= Object.keys($scope.agreeForms);
							$scope.agreeFormsNum = keys.length;
							if ($scope.agreeFormsNum == 0){
								$('.datatable-join').DataTable().destroy();
							}
							else{
								$('.datatable-join').DataTable().row(agreeIndex).remove().draw();
							}
							agreeIndex = -1;
						}
						else{
							$scope.updateForms.splice(updateIndex,1);
							var keys 		= Object.keys($scope.updateForms);
							$scope.updateFormsNum = keys.length;
							if ($scope.updateFormsNum == 0){
								$('.datatable-update').DataTable().destroy();
							}
							else{
								$('.datatable-update').DataTable().row(updateIndex).remove().draw();
							}
							updateIndex = -1;
						}
						if (data.message == 'validacion correcta'){
							$scope.errors = "<p style='color:rgb(66,166,42);font-size:14px;'>El formulario fue aprobado exitosamente</p>";
						}
						else if(data.message == 'rechazado correcto'){
							$scope.errors = "<p style='color:rgb(66,166,42);font-size:14px;'>El formulario fue rechazado exitosamente</p>";
						}
						else{
							$scope.errors = "<p style=\"color:red;\">El formulario ya fue aprobado.</p>";
						}
					});
					}
					else{

						$scope.errors = "<p style=\"color:red;\">Debe seleccionar un usuario</p>";
					}
				}
				else{

				$scope.error_modal = true;
				}	
		}

		$scope.print = function() {

			if (((agreeform)&&(agreeform.value == true)) || ((updateform)&&(updateform.value == true))){

				$http({
					method: 'POST',
					url: 'teaming/saveForm',
					data: {'agree': agreeform, 'update': updateform},
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				})
				.success(function(data) {

					if(data.status == 'ok'){

						var userData = ((agreeform)&&(agreeform.value == true)) ? agreeform : updateform;

						$http({
							method: 'POST',
							url: 'teaming/getsign',
							data: {sectionId : userData['sectionId'], siteId : userData['siteId']},
							headers: {'Content-Type': 'application/x-www-form-urlencoded'}
						}).success(getSignSuccess)

						function getSignSuccess(sign)
						{
							if(sign.status == 'ok')
							{
								var printPopup = $window.open('', '_blank');
								$scope.errors = "";
								$scope.enable = false;

								if (Object.keys(updateform).length == 0){
									printPopup.document.open();
									printPopup.document.write('<html><head><link rel="stylesheet" type="text/css" href="/'+APPFOLDERADD+'/libraries/css/style.css"></head><body onload="window.print()"><div class="images" style=" width: 100%;height: 50px;"><img id="teaming-left" src="/'+APPFOLDERADD+'/libraries/images/logo_teaming.png" style="width: 166px;height: 50px; margin-top: 5px; margin-left: 10px;float: left;"><img id="teaming-right" src="/'+APPFOLDERADD+'/libraries/images/logo.png" style=" float: right;margin-right: -6px;width: 120px;height: 50px;"></div><div class="agreement-paragraph" style="text-align: left;margin: 15px 6px 10px 55px;"><span style="font-weight: bold;">Legajo nº'+
										agreeform["id"]+'<br><br>Formulario de Adhesión a Teaming</span><br><br>Teaming es una iniciativa solidaria internacional de micro donaciones por la que los empleados de CAT Technologies donan de forma voluntaria un monto de su sueldo cada mes. Adicionalmente, por cada peso donado, CAT duplica dicho monto para incrementar la donación a instituciones de bien público. Las instituciones pueden ser sugeridas por vos. Tu aporte es muy valioso para nosotros.<br><br>Teaming forma parte de nuestras actividades de RSE (Responsabilidad Social Empresaria) desde el año 2010 y tiene como pilar fundamental fomentar el trabajo en equipo y la cooperación.<br><br>De este modo, sumamos entre todos esfuerzos para ayudar a quienes más lo necesitan.<br><br>Sumate a la iniciativa a través de este formulario.<br><br>¡Entre todos unimos a muchos por muy poco!<br></div><div class="second"-paragraph" style=" text-align: left; margin-left: 8%; line-height: 25px;">Yo, '+
										agreeform["name"]+' '+
										agreeform["lastName"]+',<br>Titular del (DNI/LC) nº'+
										agreeform["dni"]+', autorizo a <span style="font-weight: bold;">'+sign.title+'</span> (desde la fecha que figura al pie de este formulario) a deducir de mi salario la suma de $ '+
										agreeform["amount"]+' <span style="font-weight: bold;">.- EL VALOR MÍNIMO ACEPTADO PARA DONAR ES DE 25 PESOS</span> (escribir en letra la cifra: '+
										agreeform["amountCharacters"]+') para destinarlo como donación a las campañas de ayuda solidaria de la iniciativa TEAMING, mientras dure mi relación laboral con la empresa, o hasta que proceda a darme de baja voluntariamente.<br>Dispongo de la capacidad de modificar el monto asignado o bien definir el período de mi permanencia en tal actividad de RSE (Responsabilidad Social Empresaria).<br><div class="cat-sign"  style="text-align: right;float:right; line-height:1em;">'+sign.content+'</div>Fecha: '+
										agreeform["agreementDate"]+' <br><br><br>Firma:<span class="underline" style="padding-left: 200px;border-bottom: 1px solid black;">&nbsp;</span><input type="checkbox" checked> * Acepto las Políticas de Privacidad.<br>Aclaración:<span style="padding-left: 200px;border-bottom: 1px solid black;">&nbsp;</span><br><br></div> <div class="foot" style="text-align: left;margin-left: 8%;margin-right: 1%;"><div class="panel panel-default" style="border: 1px solid black;"><div class="panel-body">Área / Campaña: '+
										agreeform["areaCampaign"]+'</div><hr class="field-hr" style="margin: 0%;"><div class="panel-body">Teléfono / Celular: '+
										agreeform["mobileNumber"]+'</div><hr class="field-hr" style="margin: 0%;"><div class="panel-body">Si estás interesado en sugerir una institución, por favor incluí los datos a continuación: '+
										agreeform["institutionName"]+'</div></div>*Este documento tendrá validez una vez que se encuentre firmado.<br> Para realizar consultas y sugerir nuevas instituciones, escribinos a: teaming@cat-technologies.com<br><br><br></div></body></html>');
									printPopup.document.close();
								}
								else{
									printPopup.document.open();
									printPopup.document.write('<html><head><link rel="stylesheet" type="text/css" href="/'+APPFOLDERADD+'/libraries/css/style.css"></head><body onload="window.print()"><div class="images" style=" width: 100%;height: 50px;"><img id="teaming-left" src="/'+APPFOLDERADD+'/libraries/images/logo_teaming.png" style="width: 166px;height: 50px; margin-top: 5px; margin-left: 10px;float: left;"><img id="teaming-right" src="/'+APPFOLDERADD+'/libraries/images/logo.png" style=" float: right;margin-right: -6px;width: 120px;height: 50px;"></div><div class="agreement-paragraph" style="text-align: left;margin: 15px 6px 10px 55px;"><span style="font-weight: bold;">Legajo nº'+
										updateform["id"]+'<br><br>Formulario de Actualización de mi Monto a Donar</span><br><br>Teaming es una iniciativa solidaria internacional de micro donaciones por la que los empleados de CAT Technologies donan de forma voluntaria un monto de su sueldo cada mes. Adicionalmente, por cada peso donado, CAT duplica dicho monto para incrementar la donación a instituciones de bien público. Las instituciones pueden ser sugeridas por vos. Tu aporte es muy valioso para nosotros.<br><br>Teaming forma parte de nuestras actividades de RSE (Responsabilidad Social Empresaria) desde el año 2010 y tiene como pilar fundamental fomentar el trabajo en equipo y la cooperación.<br><br>De este modo, sumamos entre todos esfuerzos para ayudar a quienes más lo necesitan.<br><br>Actualizá el monto de tu donación a través de este formulario.<br><br>¡Entre todos unimos a muchos por muy poco!<br></div><div class="second"-paragraph" style=" text-align: left; margin-left: 8%; line-height: 25px;">Yo, '+
										updateform["name"]+' '+
										updateform["lastName"]+',<br>Titular del (DNI/LC) nº'+
										updateform["dni"]+', autorizo a <span style="font-weight: bold;">`'+sign.title+'</span> (desde la fecha que figura al pie de este formulario) a actualizar el monto de mi donación y a deducir de mi salario la suma de $ '+
										updateform["updatedAmount"]+' <span style="font-weight: bold;">.- EL VALOR MÍNIMO ACEPTADO PARA DONAR ES DE 25 PESOS</span> (escribir en letra la cifra: '+
										updateform["updatedAmountCharacters"]+') para destinarlo como donación a las campañas de ayuda solidaria de la iniciativa TEAMING, mientras dure mi relación laboral con la empresa, o hasta que proceda a darme de baja voluntariamente.<br>Dispongo de la capacidad de modificar el monto asignado o bien definir el período de mi permanencia en tal actividad de RSE (Responsabilidad Social Empresaria).<br><div class="cat-sign"  style="text-align: right;float:right;line-height:1em;"> '+sign.content+'</div>Fecha: '+
										updateform["updateDate"]+' <br><br><br>Firma:<span class="underline" style="padding-left: 200px;border-bottom: 1px solid black;">&nbsp;</span><input type="checkbox" checked> * Acepto las Políticas de Privacidad.<br>Aclaración<span style="padding-left: 200px;border-bottom: 1px solid black;">&nbsp;</span><br><br></div><div class="foot" style="text-align: left;margin-left: 8%;margin-right: 1%;"><div class="panel panel-default" style="border: 1px solid black;"><div class="panel-body">Área / Campaña: '+
										updateform["areaCampaign"]+'</div><hr class="field-hr" style="margin: 0%;"><div class="panel-body">Teléfono / Celular: '+
										updateform["mobileNumber"]+'</div><hr class="field-hr" style="margin: 0%;"><div class="panel-body">Si estás interesado en sugerir una institución, por favor incluí los datos a continuación: '+
										updateform["institutionName"]+'</div></div>*Este documento tendrá validez una vez que se encuentre firmado.<br> Para realizar consultas y sugerir nuevas instituciones, escribinos a: teaming@cat-technologies.com<br><br><br></div></body></html>');
									printPopup.document.close();
								}
							}
							else
							{
								$scope.errors = sign.content;
								$scope.enable = true;
							}
						}
					}
					else{
						$scope.errors = data.message;
						$scope.enable = true;
					}
				});
			}
			else{
				$scope.errors = "<p style=\"color:red;\">Debe seleccionar un usuario</p>";
			}
		}
	}		

CteamingRecords.$inject = ['$scope', '$http', 'nav', '$timeout','$window', '$rootScope'];
function CteamingRecords($scope, $http, nav, $timeout, $window, $rootScope) {
	
	$scope.approvedAgreeForms = [];
	$scope.showfilters 		  = false;
	$scope.filters 			  = [];
	$scope.selectedFilters	  = [];
	$scope.activeFilter		  = {};
	
	nav.setSelected('teaming');

	$http({
		method: "POST",
		url 	: 'teaming/getApprovedForms'
	}).success(function (data){

		$scope.approvedAgreeForms = data.message['agreeForms'];
		$scope.filters 			  =	data.message['filters'];
		$scope.socialr 			  = data.message['socialReasons'];

		angular.forEach($scope.approvedAgreeForms, function(form){
			
			for (var i = 0; i < $scope.socialr.length; i++) {

				if($scope.socialr[i].siteId == form.siteId){

					form.socialReason = $scope.socialr[i].name;
				}
			}

			if (form.updatedAmount){

				form.amount 		  = form.updatedAmount;
				form.amountCharacters = form.updatedAmountCharacters;
				form.institutionName  = form.lastInstitution;
				form.areaCampaign 	  = form.lastCampaign;
				form.mobileNumber 	  = form.lastmobileNumber;
				form.id 			  = form.lastId;
			}
		});

		$timeout(function() {

			$('.datepicker').datepicker({
				autoclose : true,
				language: 'es',
				dateFormat: 'dd/mm/yyyy'
			});

			$('.table').DataTable({

				responsive: true,

				dom: 'Brtip',

				"columnDefs": [{ 
					"orderable": false, "targets": 14
				}],

				language: {

					"sProcessing":     "Procesando...",
					"sLengthMenu":     "Mostrar _MENU_ usuarios",
					"sZeroRecords":    "No se encontraron resultados",
					"sEmptyTable":     "No hay usuarios adheridos a Teaming",
					"sInfo":           "Mostrando usuarios del _START_ al _END_ de un total de _TOTAL_ usuarios",
					"sInfoEmpty":      "Mostrando usuarios del 0 al 0 de un total de 0 usuarios",
					"sInfoFiltered":   "(filtrado de un total de _MAX_ usuarios)",
					"sInfoPostFix":    "",
					"sSearch":         "Buscar:",
					"sUrl":            "",
					"sInfoThousands":  ",",
					"sLoadingRecords": "Cargando...",

					"oPaginate": {
						"sFirst":    "Primero",
						"sLast":     "Último",
						"sNext":     "Siguiente",
						"sPrevious": "Anterior"
					},
					"oAria": {
						"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
						"sSortDescending": ": Activar para ordenar la columna de manera descendente"
					}
				}
			});

			$("#7init,#7end").on('changeDate',update)
			$("#9init,#9end").on('changeDate',update)

			$('.table').width("100%");
			angular.forEach($scope.filters,function (filter,key) {

				$('.table').DataTable().column(key).visible(filter.pressed);
			});
			$('#initAmount, #endAmount').keyup( function() {
				$('.table').DataTable().draw();
			} );

			$('.table').DataTable().columns.adjust().draw();

		});
	})
	
	function update() {

		$('.table').DataTable().draw();
	}
	
	$scope.changeInput = function(input,column){

		$('.table').dataTable().fnFilter(input,column);
	}
	
	$scope.addElement = function (selectedFilter) {

		selectedFilter.show = true;
	}
	
	$scope.showFilter = function (selected) {

		return !selected.show;
	}
	
	$scope.format = function (name) {

		return "Ingrese "+name+": ";
	}
	
	$scope.notSelectedFilter = function (selectedFilter) {
		var type = selectedFilter.type;
		$('.table').dataTable().fnFilter('',selectedFilter.column);

		if (selectedFilter.show == false){

			switch(type){
				case "text":
					$scope.activeFilter[selectedFilter.name] = "";
					break;
				case "date":
					$('#'+selectedFilter.column+'init').datepicker('setDate','');
					$('#'+selectedFilter.column+'init').datepicker('setDate','');
					break;
				case "amount":
					$scope.activeFilter[selectedFilter.name+' initAmount'] = "";
					$scope.activeFilter[selectedFilter.name+' endAmount'] = "";
					break;
				case "select":
					delete $scope.activeFilter[selectedFilter.name];
					break;
				case "num":
					$scope.activeFilter[selectedFilter.name] = "";
					break;
			}
			$scope.selected = "nonsense";
		}
	}
	
	$scope.changeState = function (index) {

		var column = $('.table').DataTable().column(index);
		column.visible(!column.visible());
		$scope.selected = column.visible();
		$('.table').DataTable().columns.adjust().draw();
	}
	
	$scope.toDetails  = function (approvedForm) {

		$window.open('#/teaming/details/'+approvedForm.userId).user = approvedForm;
	}
	
	$.fn.dataTable.ext.search.push(
		function( settings, data, dataIndex ) {
			var min = parseInt( $('#initAmount').val(), 10 ),
			max = parseInt( $('#endAmount').val(), 10 ),
				amount = parseFloat( data[3] ) || 0; // use data for the age column

				if ( ( isNaN( min ) && isNaN( max ) ) ||
					( isNaN( min ) && amount <= max ) ||
					( min <= amount   && isNaN( max ) ) ||
					( min <= amount   && amount <= max ) )
				{
					return true;
				}
				return false;
			}
			);
	
	$.fn.dataTable.ext.search.push(
		function( settings, data, dataIndex ) {

			var initDate 	=	$('#7init').datepicker('getDate'),
			endDate 		=	$('#7end').datepicker('getDate'),
			date 			= 	data[7].trim(), 
			joinDate 		=	new Date(date.substring(6,10),date.substring(3,5)-1,date.substring(0,2));

			if ( ( initDate == null && endDate == null ) ||
				( initDate == null && joinDate <= endDate ) ||
				( initDate <= joinDate   && endDate == null ) ||
				( initDate <= joinDate   && joinDate <= endDate ) )
			{
				return true;
			}

			return false;

	});
	
	$.fn.dataTable.ext.search.push(
		function( settings, data, dataIndex ) {

			var initDate 	=	$('#9init').datepicker('getDate'),
			endDate 		=	$('#9end').datepicker('getDate'),
			date 			= 	data[9].trim(),
			joinDate 		=	new Date(date.substring(6,10),date.substring(3,5)-1,date.substring(0,2));

			if ( ( initDate == null && endDate == null ) ||
				( initDate == null && joinDate <= endDate ) ||
				( initDate <= joinDate   && endDate == null ) ||
				( initDate <= joinDate   && joinDate <= endDate ) )
			{
				return true;
			}

			return false;
	});
	
}

CteamingReports.$inject = ['$scope', 'nav', '$http', '$timeout', 'newCommunication', '$window'];
function CteamingReports($scope, nav, $http, $timeout, newCommunication, $window) {
	
	nav.setSelected('teaming');
	$('#teaming-joined').addClass('active');
	
	$scope.loading  			= true;
	$scope.approvedAgreeForms	= [];
	$scope.all 					= false;
	$scope.chosenUsers 			= [];
	$scope.showfilters 			= false;
	$scope.filters 				= [];
	$scope.selectedFilters		= [];
	$scope.activeFilter			= {};
	$scope.approvedAgreeForm 	= {};
	$scope.formIndex			= null;
	
	$http({
		method  : 'POST',
		url     : 'teaming/getApprovedForms'
	})
	.success(function(data) {

		if (data.status == 'ok') {

			$scope.filters = data.message['filters'];
			$scope.socialr = data.message['socialReasons'];

			angular.forEach(data.message['agreeForms'], function(form,key){

				for (var i = $scope.socialr.length - 1; i >= 0; i--) {
					if($scope.socialr[i].siteId == form.siteId){
						form.socialReason = $scope.socialr[i].name;
					}
				}

				if(form.joinedUser != false){

					form.value = false;

					if (form.updatedAmount){

						form.amount 		  = form.updatedAmount;
						form.amountCharacters = form.updatedAmountCharacters;
						form.institutionName  = form.lastInstitution;
						form.areaCampaign 	  = form.lastCampaign;
						form.mobileNumber 	  = form.lastmobileNumber;
						form.id 			  = form.lastId;
					}

					$scope.approvedAgreeForms.push(form);
				}
			});

			if((Object.keys($scope.approvedAgreeForms).length) > 0){
				$scope.pendingAgree = true;
			}

			$scope.loading = false;
		}

		$timeout(function() {

			$('.datepicker').datepicker({
				autoclose : true,
				language: 'es',
				dateFormat: 'dd/mm/yyyy'
			});

			$('.table').DataTable({
				responsive: true,
				dom: 'Brtip',
				language: {
					"sProcessing":     "Procesando...",
					"sLengthMenu":     "Mostrar _MENU_ usuarios",
					"sZeroRecords":    "No se encontraron resultados",
					"sEmptyTable":     "No hay usuarios adheridos a Teaming",
					"sInfo":           "Mostrando usuarios del _START_ al _END_ de un total de _TOTAL_ usuarios",
					"sInfoEmpty":      "Mostrando usuarios del 0 al 0 de un total de 0 usuarios",
					"sInfoFiltered":   "(filtrado de un total de _MAX_ usuarios)",
					"sInfoPostFix":    "",
					"sSearch":         "Buscar:",
					"sUrl":            "",
					"sInfoThousands":  ",",
					"sLoadingRecords": "Cargando...",

					"oPaginate": {
						"sFirst":    "Primero",
						"sLast":     "Último",
						"sNext":     "Siguiente",
						"sPrevious": "Anterior"
					},
					"oAria": {
						"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
						"sSortDescending": ": Activar para ordenar la columna de manera descendente"
					}
				}
			});

			$("#7init,#7end").on('changeDate',update)
			$("#9init,#9end").on('changeDate',update)
			$('.table').width("100%");

			angular.forEach($scope.filters,function (filter,key) {
				$('.table').DataTable().column(key+1).visible(filter.pressed);
			});

			$('#initAmount, #endAmount').keyup( function(){
				$('.table').DataTable().draw();
			});

			$('.table').DataTable().columns.adjust().draw();

		});

	});
	
	$scope.checkAll = function() {

		$scope.errors 	= "";
		$scope.all 		= !$scope.all;

		angular.forEach($scope.approvedAgreeForms, function(form) {
			form.value = $scope.all;
		});
	}
	
	
	$scope.show = function (approvedAgreeForm) {

		$scope.errors = "";
	}
	
	function update() {

		$('.table').DataTable().draw();
	}
	
	$scope.changeInput = function(input,column){

		$('.table').dataTable().fnFilter(input,column+1);
	}
	
	$scope.addElement = function (selectedFilter){

		selectedFilter.show = true;
	}
	
	$scope.showFilter = function (selected){

		return !selected.show;
	}
	
	$scope.format = function (name) {

		return "Ingrese "+name+": ";
	}
	
	$scope.sendCommunication = function (typeOfCoummunication) {

			var none = true;

			angular.forEach($scope.approvedAgreeForms, function(form){

				if (form.value == true) {
					$scope.chosenUsers.push(form);
					none = false;
				}
			});

			if (none == true){

				$scope.errors = "<p style=\"color:red;\"> Debe seleccionar al menos un usuario</p>";
			}
			else{

				newCommunication.setMessage({"action": typeOfCoummunication, "data":$scope.chosenUsers});

				switch(typeOfCoummunication){

					case 'newNewsletter':
						//$window.location.href = '#/newsletter/create';
						break;

					case 'newSurvey':
						//$window.location.href = '#/surveys/create';
						break;
				}
			}
		}
	
	
	
	$scope.showFilter = function (selected) {
		return !selected.show;
	}
	
	$scope.format = function (name) {
		return "Ingrese "+name+": ";
	}
	
	$scope.notSelectedFilter = function (selectedFilter) {

		var type = selectedFilter.type;
		$('.table').dataTable().fnFilter('',selectedFilter.column+1);

		if (selectedFilter.show == false){

			switch(type){

				case "text":
					$scope.activeFilter[selectedFilter.name] = "";
					break;
				case "date":
					$('#'+selectedFilter.column+'init').datepicker('setDate','');
					$('#'+selectedFilter.column+'init').datepicker('setDate','');
					break;
				case "select":
					delete $scope.activeFilter[selectedFilter.name];
					break;
				case "num":
					$scope.activeFilter[selectedFilter.name] = "";
					break;
			}

			$scope.selected = "nonsense";
		}
	}
	
	$scope.changeState = function (index) {

		var column = $('.table').DataTable().column(index+1);
		column.visible(!column.visible());
		$scope.selected = column.visible();
		$('.table').DataTable().columns.adjust().draw();
	}
	
	$.fn.dataTable.ext.search.push(
		function( settings, data, dataIndex ) {

			var min = parseInt( $('#initAmount').val(), 10 ),
			max  	= parseInt( $('#endAmount').val(), 10 ),
			amount  = parseFloat( data[4] ) || 0; 

			if (( isNaN( min ) && isNaN( max )) ||
				( isNaN( min ) && amount <= max ) ||
				( min <= amount   && isNaN( max ) ) ||
				( min <= amount   && amount <= max ))
			{
				return true;
			}
				return false;
		}
	);
	
	$.fn.dataTable.ext.search.push(
		function( settings, data, dataIndex ) {

			var initDate =	$('#7init').datepicker('getDate'),
			endDate 	 =	$('#7end').datepicker('getDate'),
			date 		 = 	data[8].trim(), 
			joinDate 	 =	new Date(date.substring(6,10),date.substring(3,5)-1,date.substring(0,2));

			if (( initDate == null && endDate == null ) ||
				( initDate == null && joinDate <= endDate ) ||
				( initDate <= joinDate   && endDate == null ) ||
				( initDate <= joinDate   && joinDate <= endDate ) )
			{
				return true;
			}
			return false;
	});
	
	$.fn.dataTable.ext.search.push(
		function( settings, data, dataIndex ) {

			var initDate =	$('#9init').datepicker('getDate'),
			endDate 	 =	$('#9end').datepicker('getDate'),
			date 		 = 	data[10].trim(), 
			joinDate 	 =	new Date(date.substring(6,10),date.substring(3,5)-1,date.substring(0,2));

			if ( ( initDate == null && endDate == null ) ||
				( initDate == null && joinDate <= endDate ) ||
				( initDate <= joinDate   && endDate == null ) ||
				( initDate <= joinDate   && joinDate <= endDate ) )
			{
				return true;
			}
				return false;
	});

	$scope.confirmDischarge = function(approvedForm,index) {

		var date = new Date(),
		stDate 	 = date.getFullYear() + "/" + (date.getMonth() + 1) + "/" + date.getDate();

		$scope.approvedAgreeForm 					= approvedForm;
		$scope.approvedAgreeForm["todayDate"] 		= stDate;
		$scope.approvedAgreeForm["rejectReason"] 	= 'r';
		$scope.formIndex							= index;

		$('#removeAlert').modal('show');

		$scope.show_observation = false; 
		$scope.var_observation 	= null; 
		$scope.error_modal 		= false; 
		$scope.show_observation = true; 
	}
	
	$scope.dischargeUser = function() {

		var reason = $scope.approvedAgreeForm["rejectReason"]; 

		if($scope.var_observation){ 

			var realAgreeDate = new Date($scope.approvedAgreeForm["agreementDate"].substring(6,10),$scope.approvedAgreeForm["agreementDate"].substring(3,4)+($scope.approvedAgreeForm["agreementDate"].substring(4,5)-1),$scope.approvedAgreeForm["agreementDate"].substring(0,2)); 

			$scope.approvedAgreeForm["agreementDate"]	= realAgreeDate.getFullYear() + "/" + (realAgreeDate.getMonth() + 1) + "/" + realAgreeDate.getDate(); 
			$scope.approvedAgreeForm["observation"] 	= $scope.var_observation;

			$http({ 
				method: 'POST', 
				url: 'teaming/dischargeTeamingUser', 
				data: $scope.approvedAgreeForm, 
				headers: {'Content-Type': 'application/x-www-form-urlencoded'} 
			}).success(function(data){ 

				$('#removeAlert').modal('hide');

				if(data.status == 'ok'){
					$scope.errors = "<p style='color:rgb(66,166,42);font-size:14px;'>El usuario seleccionado fue dado de baja exitosamente.</p>";
				}
				else{
					$scope.errors = "<p style=\"color:red;\">El usuario seleccionado ya ha sido dado de baja.</p>";
				}

				$scope.approvedAgreeForms.splice($scope.formIndex,1);
				$('.table').DataTable().row($scope.formIndex).remove().draw();
				$scope.formIndex = -1;	
			}) 
		} 
		else { 
			$scope.error_modal = true; 
		} 			
	}

	$scope.print = function(approvedForm){

		$http({
			method: 'POST',
			url: 'teaming/getsign',
			data: {'sectionId' : approvedForm['sectionId'], 'siteId' : approvedForm['siteId']},
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		}).success(getSignSuccess)
	
		function getSignSuccess(sign){
				
			if(sign.status == 'ok'){

				var printPopup = $window.open('', '_blank');
				$scope.errors = "";
				$scope.enable = false;

				printPopup.document.open();
				printPopup.document.write('<html><head><link rel="stylesheet" type="text/css" href="/'+APPFOLDERADD+'/libraries/css/style.css"></head><body onload="window.print()"><div class="images" style=" width: 100%;height: 50px;"><img id="teaming-left" src="/'+APPFOLDERADD+'/libraries/images/logo_teaming.png" style="width: 166px;height: 50px; margin-top: 5px; margin-left: 10px;float: left;"><img id="teaming-right" src="/'+APPFOLDERADD+'/libraries/images/logo.png" style=" float: right;margin-right: -6px;width: 120px;height: 50px;"></div><div class="agreement-paragraph" style="text-align: left;margin: 15px 6px 10px 55px;"><span style="font-weight: bold;">Legajo nº'+
					approvedForm["id"]+'<br><br>Formulario de Adhesión a Teaming</span><br><br>Teaming es una iniciativa solidaria internacional de micro donaciones por la que los empleados de CAT Technologies donan de forma voluntaria un monto de su sueldo cada mes. Adicionalmente, por cada peso donado, CAT duplica dicho monto para incrementar la donación a instituciones de bien público. Las instituciones pueden ser sugeridas por vos. Tu aporte es muy valioso para nosotros.<br><br>Teaming forma parte de nuestras actividades de RSE (Responsabilidad Social Empresaria) desde el año 2010 y tiene como pilar fundamental fomentar el trabajo en equipo y la cooperación.<br><br>De este modo, sumamos entre todos esfuerzos para ayudar a quienes más lo necesitan.<br><br>Sumate a la iniciativa a través de este formulario.<br><br>¡Entre todos unimos a muchos por muy poco!<br></div><div class="second"-paragraph" style=" text-align: left; margin-left: 8%; line-height: 25px;">Yo, '+
					approvedForm["name"]+' '+
					approvedForm["lastName"]+',<br>Titular del (DNI/LC) nº'+
					approvedForm["dni"]+', autorizo a <span style="font-weight: bold;">'+sign.title+'</span> (desde la fecha que figura al pie de este formulario) a deducir de mi salario la suma de $ '+
					approvedForm["amount"]+' <span style="font-weight: bold;">.- EL VALOR MÍNIMO ACEPTADO PARA DONAR ES DE 25 PESOS</span> (escribir en letra la cifra: '+
					approvedForm["amountCharacters"]+') para destinarlo como donación a las campañas de ayuda solidaria de la iniciativa TEAMING, mientras dure mi relación laboral con la empresa, o hasta que proceda a darme de baja voluntariamente.<br>Dispongo de la capacidad de modificar el monto asignado o bien definir el período de mi permanencia en tal actividad de RSE (Responsabilidad Social Empresaria).<br><div class="cat-sign"  style="text-align: right;float:right; line-height:1em;">'+sign.content+'</div>Fecha: '+
					approvedForm["agreementDate"]+' <br><br><br>Firma:<span class="underline" style="padding-left: 200px;border-bottom: 1px solid black;">&nbsp;</span><input type="checkbox" checked> * Acepto las Políticas de Privacidad.<br>Aclaración:<span style="padding-left: 200px;border-bottom: 1px solid black;">&nbsp;</span><br><br></div> <div class="foot" style="text-align: left;margin-left: 8%;margin-right: 1%;"><div class="panel panel-default" style="border: 1px solid black;"><div class="panel-body">Área / Campaña: '+
					approvedForm["areaCampaign"]+'</div><hr class="field-hr" style="margin: 0%;"><div class="panel-body">Teléfono / Celular: '+
					approvedForm["mobileNumber"]+'</div><hr class="field-hr" style="margin: 0%;"><div class="panel-body">Si estás interesado en sugerir una institución, por favor incluí los datos a continuación: '+
					approvedForm["institutionName"]+'</div></div>*Este documento tendrá validez una vez que se encuentre firmado.<br> Para realizar consultas y sugerir nuevas instituciones, escribinos a: teaming@cat-technologies.com / Usuario Teaming de CATnet<br><br><br></div></body></html>');
				printPopup.document.close();
			}
			else{
			
				$scope.errors = sign.content;
				$scope.enable = true;
			}
		}
	}
}

CteamingRemainingReports.$inject = ['$scope' ,'nav', '$http' ,'$timeout','newCommunication','$window'];
function CteamingRemainingReports($scope, nav, $http,$timeout,newCommunication,$window) {
	
	nav.setSelected('teaming');
	$('#teaming-not-joined').addClass('active');
	$scope.all  			= false;
	$scope.loading  		= true;
	$scope.remainingUsers 	=	[];
	$scope.chosenUsers 		= 	[];
	$scope.showfilters 		= 	false;
	$scope.filters 			= 	[];
	$scope.selectedFilters	= 	[];
	$scope.activeFilter		= 	{};

	$http({
		method  : 'POST',
		url     : 'teaming/getRemainingUsers'
	}).success(function(data) {
		if (data.status == 'ok') {

			$scope.filters 			= data.message["filters"];
			$scope.remainingUsers 	= data.message['remainingUsers'];
			$scope.socialr 			= data.message['socialReasons'];

			angular.forEach($scope.remainingUsers, function(user){
				
				for (var i = $scope.socialr.length - 1; i >= 0; i--) {

					if($scope.socialr[i].siteId == user.siteId){

						user.socialReason = $scope.socialr[i].name;
					}
				}

				user.value = false;
			});

			$timeout(function() {

				$('.datepicker').datepicker({
					autoclose : true,
					language: 'es',
					dateFormat: 'dd/mm/yyyy'
				});
				$('.table').DataTable({
					dom: 'Brtip',
					language: {
						"sProcessing":     "Procesando...",
						"sLengthMenu":     "Mostrar _MENU_ usuarios",
						"sZeroRecords":    "No se encontraron resultados",
						"sEmptyTable":     "Ningún dato disponible en esta tabla",
						"sInfo":           "Mostrando usuarios del _START_ al _END_ de un total de _TOTAL_ usuarios",
						"sInfoEmpty":      "Mostrando usuarios del 0 al 0 de un total de 0 usuarios",
						"sInfoFiltered":   "(filtrado de un total de _MAX_ usuarios)",
						"sInfoPostFix":    "",
						"sSearch":         "Buscar:",
						"sUrl":            "",
						"sInfoThousands":  ",",
						"sLoadingRecords": "Cargando...",

						"oPaginate": {
							"sFirst":    "Primero",
							"sLast":     "Último",
							"sNext":     "Siguiente",
							"sPrevious": "Anterior"
						},
						"oAria": {
							"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
							"sSortDescending": ": Activar para ordenar la columna de manera descendente"
						}
					}
				});

				$('.table').DataTable().columns.adjust().draw();
				$("#3init,#3end").on('changeDate',update)
				$('.table').width("100%");

			});
			$scope.loading = false;
		}
	});
	
	$scope.showFilter = function (selected) {
		return !selected.show;
	}
	
	$scope.format = function (name) {
		return "Ingrese "+name+": ";
	}
	
	$scope.notSelectedFilter = function (selectedFilter) {
		var type = selectedFilter.type;
		$('.table').dataTable().fnFilter('',selectedFilter.column+1);

		if (selectedFilter.show == false){
			switch(type){
				case "text":
				$scope.activeFilter[selectedFilter.name] = "";
				break;
				case "date":
				$('#'+selectedFilter.column+'init').datepicker('setDate','');
				$('#'+selectedFilter.column+'init').datepicker('setDate','');
				break;
				case "select":
				delete $scope.activeFilter[selectedFilter.name];
				break;
				case "num":
				$scope.activeFilter[selectedFilter.name] = "";
				break;
			}
			$scope.selected = "nonsense";
		}
	}
	
	$scope.show = function (remainingUser) {

		$scope.errors = "";
	}
	
	$scope.checkAll = function() {

		$scope.errors 	= "";
		$scope.all 		= !$scope.all;

		angular.forEach($scope.remainingUsers, function(form) {

			form.value = $scope.all;
		});
	}
	
	function update() {

		$('.table').DataTable().draw();
	}
	
	$scope.changeInput = function(input,column){

		$('.table').dataTable().fnFilter(input,column+1);
	}
	
	$scope.addElement = function (selectedFilter){

		selectedFilter.show = true;
	}
	
	$scope.showFilter = function (selected){

		return !selected.show;
	}
	
	$scope.format = function (name) {

		return "Ingrese "+name+": ";
	}
	
	$scope.sendCommunication = function (typeOfCoummunication) {

		var none = true;

		angular.forEach($scope.remainingUsers, function(form){

			if (form.value == true) {
				$scope.chosenUsers.push(form);
				none = false;
			}
		});

		if (none == true){
			$scope.errors = "<p style=\"color:red;\"> Debe seleccionar al menos un usuario</p>";
		}
		else{
			newCommunication.setMessage({"action": typeOfCoummunication, "data":$scope.chosenUsers});
			switch(typeOfCoummunication){
				case 'newNewsletter':
				//$window.location.href = '#/newsletter/create';
				break;
				case 'newSurvey'	:
				//$window.location.href = '#/surveys/create';
				break
			}	
		}
	}
	
	$.fn.dataTable.ext.search.push(
		function( settings, data, dataIndex ) {

			var initDate =	$('#3init').datepicker('getDate'),
			endDate 	 =	$('#3end').datepicker('getDate'),
			date 		 = 	data[4].trim(); 
			var joinDate =	new Date(date.substring(6,10),date.substring(3,5)-1,date.substring(0,2));

			if ( ( initDate == null && endDate == null ) ||
				( initDate == null && joinDate <= endDate ) ||
				( initDate <= joinDate   && endDate == null ) ||
				( initDate <= joinDate   && joinDate <= endDate ) )
			{
				return true;
			}
			return false;
		}
	);	
}

CteamingManualAgreementLoad.$inject = ['nav','$http','$scope', '$timeout','$window','realTime'];
function CteamingManualAgreementLoad(nav, $http, $scope,$timeout,$window,realTime){

	$('#teaming-manual-join').addClass('active');
	nav.setSelected('teaming');

	$scope.agreeform = {
		dni: '',
		amount: '',
		amountCharacters: '',
		areaCampaign: '',
		mobileNumber: '',
		institutionName: '',
		agreementDate: '',
		userId: '',
		id: '',
		acceptterms : true,
		loadType:'manual'
	}

	$scope.submit = function(agreeform){

		$scope.errorsAdd ="";
		$scope.errors = null;

		$http({
			method  : 'POST',
			url     : 'teaming/sendAgreeForm',
				data    : $scope.agreeform,  // pass in data as strings
				headers : { 'Content-Type': 'application/x-www-form-urlencoded' }  // set the headers so angular passing info as form data (not request payload)
			}).success(function(data) {

				if ((data.status == 'invalid')||(data.status == 'alreadyJoined')) {
					$scope.errorsAdd = data.message;
				}
				else{

					realTime.socket.emit('new_teaming_form');
					$scope.errorsAdd = '<p style="color:rgb(66,166,42);font-size:14px;"> La solicitud de adhesión fue cargada de manera exitosa.</p>';
					$scope.users = [];

					$scope.agreeform = {
						dni: '',
						amount: '',
						amountCharacters: '',
						areaCampaign: '',
						mobileNumber: '',
						institutionName: '',
						agreementDate: '',
						userId: '',
						id: '',
						acceptterms : true,
						loadType:'manual'
					}
				}
			});
		}

		$scope.selectClick = function (e) {
			$('.select-input', e.currentTarget).focus();
		};

		$scope.addUser = function(i){

			
			$scope.users = [];
			$scope.users.push($scope.userOptions[i]);
			$scope.agreeform["userId"] = $scope.users[0].userId;

			$http({
				method 	: "POST",
				url		: 'teaming/getDni',
				data 	: {'userId': $scope.agreeform["userId"]},
				dataType : 'json'
			}).success(function(data) {
				$scope.agreeform["dni"]=data.message["dni"];
			});

			$scope.userOptions = [];
			$scope.findLike = '';


		};

		$scope.removeUser = function(i) {

			$scope.users.splice(i, 1);
			$scope.agreeform["dni"] 	= "";
			$scope.agreeform["userId"] 	= "";
		};

		$scope.userOptions = [];
		$scope.findLike = '';
		$scope.searchNumber = 0;
		$scope.searchActual = 0;

		$scope.searchUsers = function() {

			if($scope.findLike != ''){

				$scope.finding = true;
				var thisSearch = $scope.searchNumber;
				$scope.searchNumber = $scope.searchNumber+1;

				$.ajax({
					method 	: "POST",
					url		: 'teaming/searchUsers',
					data 	: {'like': $scope.findLike},
					dataType : 'json'
				}).done(function(data) {
					if(data.status == 'ok' && thisSearch >= $scope.searchActual && $scope.findLike != ''){
						$scope.searchActual = thisSearch;
						$scope.$apply(function() {
							$scope.userOptions = data.message["usersAlike"];
							$scope.finding = false;
						});

					}
					else if (thisSearch >= $scope.searchActual){

						$scope.searchActual = thisSearch;

						$scope.$apply(function() {

							$scope.finding = false;
							$scope.userOptions = [];
						});
					}
				});
			}
			else{

				$scope.userOptions = [];
				$scope.finding = false;
			}
		};
	}

	CteamingManualUpdateLoad.$inject = ['nav','$http','$scope', '$timeout','$window','realTime'];
	function CteamingManualUpdateLoad(nav, $http, $scope,$timeout,$window,realTime){

		$('#teaming-manual-update').addClass('active');
		nav.setSelected('teaming');

		$scope.updateform = {
			dni: '',
			amount: '',
			amountCharacters: '',
			areaCampaign: '',
			mobileNumber: '',
			institutionName: '',
			updateDate: '',
			userId: '',
			id: '',
			acceptterms : true,
			loadType:'manual',
		}

		$scope.oldUpdateForm = {
			dni : '',
			amount : '',
			amountCharacters : '',
			acceptterms : 'true',
			areaCampaign : '',
			mobileNumber : '',
			institutionName : '',
			id: '',
			loadType:'online',
		};

		$scope.toBlack = function (field) {
			$(field).removeClass("redify");
		}

		$scope.submitUpdate = function(updateform){

			$scope.errorsUpd ="";
			$scope.errors = null;
			$http({
				method  : 'POST',
				url     : 'teaming/sendUpdateForm',
				data    : {'form': $scope.updateform,'oldForm':$scope.oldUpdateForm},
				headers : {'Content-Type': 'application/x-www-form-urlencoded'}
			}).success(function(data) {
				if ((data.status == 'invalid')||(data.status == 'alreadyUpdated')) {
					$scope.errorsUpd = data.message;
				}
				else{
					$scope.errorsUpd = '<p style="color:rgb(66,166,42);font-size:14px;"> La solicitud de actualización fue cargada de manera exitosa.</p>';

					$scope.updateform = {
						dni: '',
						amount: '',
						amountCharacters: '',
						areaCampaign: '',
						mobileNumber: '',
						institutionName: '',
						updateDate: '',
						userId: '',
						id: '',
						acceptterms : true,
						loadType:'manual'

					}
					$scope.usersUpdate = [];
					realTime.socket.emit('new_teaming_form');
				}
			});
		}

		$scope.selectClickUpdate = function (e) {

			$('.select-input-update', e.currentTarget).focus();
		};

		$scope.addUserUpdate = function(i) {

			$scope.usersUpdate = [];
			$scope.usersUpdate.push($scope.userOptionsUpdate[i]);
			$scope.updateform["userId"]=$scope.usersUpdate[0].userId;

			$http({
				method	: 'POST',
				url	    : 'teaming/getUserLastData',
				data    : {"updateId" : $scope.updateform}
			})
			.success(function (data) {

				if (data.status == 'ok'){

					$scope.updateform["dni"]					=	data.message["latestUpdate"].updateDni;
					$scope.updateform["areaCampaign"]			=	data.message["latestUpdate"].updateAreaCampaign;
					$scope.updateform["mobileNumber"]			=	data.message["latestUpdate"].updateMobileNumber;
					$scope.updateform["institutionName"]		=	data.message["latestUpdate"].updateInstitutionName;
					$scope.updateform["amount"]					=	data.message["latestUpdate"].updateAmount;
					$scope.updateform["amountCharacters"]		=	data.message["latestUpdate"].updateAmountCharacters;

					$scope.oldUpdateForm["dni"]					=	data.message["latestUpdate"].updateDni;
					$scope.oldUpdateForm["areaCampaign"]		=	data.message["latestUpdate"].updateAreaCampaign;
					$scope.oldUpdateForm["mobileNumber"]		=	data.message["latestUpdate"].updateMobileNumber;
					$scope.oldUpdateForm["institutionName"]		=	data.message["latestUpdate"].updateInstitutionName;
					$scope.oldUpdateForm["amount"]				=	data.message["latestUpdate"].updateAmount;
					$scope.oldUpdateForm["amountCharacters"]	=	data.message["latestUpdate"].updateAmountCharacters;

					if (data.message["latestUpdate"].updateId){

						$scope.updateform["id"]					=	data.message["latestUpdate"].updateId;
						$scope.oldUpdateForm["id"]				=	data.message["latestUpdate"].updateId;
					}
					else{

						$scope.updateform["id"]					=	"0";
						$scope.oldUpdateForm["id"]				=	"0";
					}

				}
			})

			$scope.userOptionsUpdate = [];
			$scope.findLikeUpdate = '';
		};

		$scope.removeUserUpdate = function(i) {

			$scope.usersUpdate.splice(i, 1);

			$scope.updateform = {
				dni: '',
				amount: '',
				amountCharacters: '',
				areaCampaign: '',
				mobileNumber: '',
				institutionName: '',
				updateDate: '',
				userId: '',
				id: '',
				acceptterms : true,
				loadType:'manual',
			}
		};

		$scope.userOptionsUpdate = [];
		$scope.findLikeUpdate = '';
		$scope.searchNumberUpdate = 0;
		$scope.searchActualUpdate = 0;

		$scope.searchUsersUpdate = function() {

			if($scope.findLikeUpdate != ''){

				$scope.findingUpdate = true;
				var thisSearchUpdate = $scope.searchNumberUpdate;
				$scope.searchNumberUpdate = $scope.searchNumberUpdate+1;

				$.ajax({
					method 	: "POST",
					url		: 'teaming/searchUsersUpdate',
					data 	: {'like': $scope.findLikeUpdate},
					dataType : 'json'
				}).done(function(data) {

					if(data.status == 'ok' && thisSearchUpdate >= $scope.searchActualUpdate && $scope.findLikeUpdate != ''){

						$scope.searchActualUpdate = thisSearchUpdate;
						$scope.$apply(function() {
							$scope.userOptionsUpdate = data.message["usersAlike"];
							$scope.findingUpdate = false;
						});
					}
					else if (thisSearchUpdate >= $scope.searchActualUpdate){

						$scope.searchActualUpdate = thisSearchUpdate;

						$scope.$apply(function() {
							$scope.findingUpdate = false;
							$scope.userOptionsUpdate = [];
						});
					}
				});
			}
			else{

				$scope.userOptionsUpdate = [];
				$scope.findingUpdate = false;
			}
		};
	}

	CteamingCommunicationReports.$inject = ['nav','$http','$scope', '$timeout','$window'];
	function CteamingCommunicationReports(nav, $http, $scope,$timeout,$window){

		nav.setSelected('teaming');
		$('#teaming_reports').addClass('active');
		//creation of the datepickers
		$('#sandbox-container .input-daterange').datepicker({todayBtn: "linked",language: "es",autoclose: true,todayHighlight: true, endDate: "getdate()"});

		$('#start').on('changeDate',update)
		$('#end').on('changeDate',update)

		var monthNames      	= 	["enero", "febrero", "marzo", "abril", "mayo", "junio", "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre"];
		$scope.maxUpdates 			= 0;
		$scope.joinedInDateRange	= {manual:'',online:''};
		$scope.joinedLastMonth 		= {manual:'',online:''};
		$scope.joinedThisMonth 		= {manual:'',online:''};
		$scope.discharges 			= {d:'',r:'',f:''};
		$scope.updatesInDateRange	= [];
		$scope.loading = false;

		function update (){

			$scope.loading 	 = true;
			$scope.maxAmount = 0;
			$scope.avgAmount = 0.0;

			var startDate 	= $('#start').datepicker('getDate');
			var endDate 	= $('#end').datepicker('getDate');
			var max 		= 0;

			$http({
				method  : 'POST',
				url     : 'teaming/getUserStatistics',
				data    : 	{
					startDate : startDate,
					endDate : endDate,
				}
			}).success(function(data) {

				$scope.registedusers 		= data.message["users"];
				$scope.joinedInDateRange	= data.message["joinedInDateRange"];
				$scope.discharges.d  		= (data.message["discharges"].d)? data.message["discharges"].d.dischargesPerReasonId : 0;
				$scope.discharges.r  		= (data.message["discharges"].r)? data.message["discharges"].r.dischargesPerReasonId : 0;
				$scope.discharges.f  		= (data.message["discharges"].f)? data.message["discharges"].f.dischargesPerReasonId : 0;
				$scope.updatesInDateRange	= (data.message["updatesInDateRange"].length == 0)? 0 : data.message["updatesInDateRange"][0].updatesPerLoadType;

				if (data.message["amounts"].maxAmount.maxAmountUpdated) {

					$scope.maxAmount = (data.message["amounts"].maxAmount.max_amount > data.message["amounts"].maxAmount.maxAmountUpdated)? data.message["amounts"].maxAmount.max_amount : data.message["amounts"].maxAmount.maxAmountUpdated;
				}
				else {

					$scope.maxAmount = (data.message["amounts"].maxAmount.max_amount) ? data.message["amounts"].maxAmount.max_amount : 0;
				}

				$scope.totalAmount 			= 0;

				angular.forEach(data.message["amounts"].amounts,function (amountData) {

					if(amountData.updatedAmount!=null){

						$scope.totalAmount += parseFloat(amountData.updatedAmount);
					}
					else {

						$scope.totalAmount += parseFloat(amountData.amount);
					}
				})
				$scope.loading = false;
			});
		}

		//init the datepicker with the actual date
		var today 	= new Date(),
		day 		= today.getDate(),
		month 		= today.getMonth(),
		year 		= today.getFullYear();

		$('#start').datepicker("setDate",new Date(year,month,day));

	}

		CteamingnewsletterReports.$inject = ['nav','$http','$scope', '$timeout','$window'];
		function CteamingnewsletterReports(nav, $http, $scope,$timeout,$window){
			$('#teaming_newsletter').addClass('active');

			$('#sandbox-container .input-daterange').datepicker({todayBtn: "linked",language: "es",autoclose: true,todayHighlight: true, endDate: "getdate()"});

			$('#start').on('changeDate',update)
			$('#end').on('changeDate',update)

			nav.setSelected('teaming');
			$scope.loading = false; 

			function update() {

				$scope.loading = true; 
				$scope.totalNewsletters = 0;
				$scope.teamingNews = [];

				$scope.newsletter = {
					title:'',
					recipients:[],
					creation_date:''
				}

				$scope.recipient = {
					name:'',
					saw:false,
					isJoinedNow:false
				}

				var startDate = $('#start').datepicker('getDate');
				var endDate = $('#end').datepicker('getDate');

				$http({
					method  : 'POST',
					url     : 'teaming/getNewsletterStatistics',
					data    : 	{
						startDate : startDate,
						endDate : endDate,
					}
				}).success(function(data) {

					if (data.status == 'ok') {

						if (data.message.resTwo.length > 0) {

							$scope.totalNewsletters = data.message["resOne"].totalNews;
							$scope.teamingNews = [];
							var firstNewsletter = (data.message["resTwo"][0].newsletterId) ? data.message["resTwo"][0].newsletterId : 0;
							var index = 0;

							angular.forEach(data.message["resTwo"],function(oneNewsletter){

								$scope.newsletter = {
									title:'',
									recipients:[]
								}

								$scope.newsletter["title"]=oneNewsletter.title;
								$scope.newsletter["creation_date"] = oneNewsletter.creation_date;
								$scope.newsletter["end_date"] = oneNewsletter.creation_date;
								$scope.teamingNews.push($scope.newsletter);

								var recipients = data.message['resThird'];

								for (var second_index = 0; second_index < recipients[index].length; second_index++) {

									$scope.recipient = {
										name:'',
										saw:false,
										isJoinedNow:false
									}

									if(recipients[index][second_index]){

										$scope.recipient['name'] = recipients[index][second_index].name+" "+recipients[index][second_index].lastname

										if(recipients[index][second_index].timestamp){
											$scope.recipient['saw'] = true;
										}

										if(recipients[index][second_index].isApproved == 1){
											$scope.recipient['isApproved'] = true;
										}

										$scope.newsletter['recipients'].push($scope.recipient);
									}
								}
								index++;
							});
						}
						else{

							$scope.noMessages = "<p style=\"color:red\">Aún no se han enviado mensajes.</p>"
						}
					}
					else{

						$scope.noMessages = "<p style=\"color:red\">Aún no se han enviado mensajes.</p>"
					}

					$scope.loading = false;
				});
			}

			//init the datepicker with the actual date
			var today = new Date(),
			day = today.getDate(),
			month = today.getMonth(),
			year = today.getFullYear();

			$('#start').datepicker("setDate",new Date(year,month,day));

			//Change the text of info's buttons
			$('body').on('click', '#recipients button', function(){

				if($(this).html()=="Mostrar datos de destinatarios"){

					$(this).empty();
					$(this).append("Ocultar datos");
				}
				else {

					$(this).empty();
					$(this).append("Mostrar datos de destinatarios");
				}
			})
		}

		CteamingsurveyReports.$inject = ['nav','$http','$scope', '$timeout','$window'];
		function CteamingsurveyReports(nav, $http, $scope,$timeout,$window){

			$('#teaming_survey').addClass('active');
			nav.setSelected('teaming');

			$scope.surveys =  [];
			$scope.loading = true; 

			$http({
				method  : 'POST',
				url     : 'teaming/getSurveyStatistics',
			}).success(function(data) {

				$scope.surveys = data.message["resOne"];
				$scope.loading = false; 
			});
		}

	CteamingUserDetails.$inject = ['$scope', '$routeParams', 'nav', '$http', '$rootScope', '$window'];
	function CteamingUserDetails($scope, $routeParams, nav, $http, $rootScope, $window) {
		
		$scope.teamingData 		= {};
		$scope.personalData 	= {};
		$scope.teamingHistory 	= [];

		$http({
			method	: 	'POST',
			url 	: 	'teaming/dataToView',
			data 	: 	$routeParams.userId
		})
		.success(function (data) {
			
			if(data.status == 'ok'){
				$scope.personalData 	=	data.message["personalData"];
				$scope.teamingHistory	= 	data.message["teamingHistory"];
				$scope.teamingData 		= 	data.message["teamingData"];



				$scope.teamingData["completeName"] = $scope.personalData["name"]+" "+$scope.personalData["lastName"];
			}

		});

		$scope.savePdf = function (address) {
			$window.open(address+$scope.teamingData["userId"],"_blank");
		}

		$scope.format = function (addedEntries){
			if ((typeof addedEntries == 'undefined') || (addedEntries.length == 0)){
				var response="";
			}
			else{
				var response="<div>Los datos ingresados fueron:<br>";
				angular.forEach(addedEntries, function (addedEntry) {
					response+=addedEntry+"<br>";
				});
				response+="</div>";
			}
			return response;
		}

	}

	CteamingSuggestInstitution.$inject = ['$scope', "$http",'$timeout', '$location','nav'];
	function CteamingSuggestInstitution ($scope, $http, $timeout, $location, nav){

		nav.setSelected('teaming');

		$scope.states 		 = null;
		$scope.cities 		 = null;
		$scope.errors  		 = "";

		$scope.institution 	 = {
			name 		: "" ,
			address 	: "" ,
			email 		: "" ,
			number 		: "" ,
			referrer 	: "" ,
			description : "" ,
			state 		: null,
			cityInState : null,
			chosenCity  : "",
			chosenState : "",
		};
		
		$http.get("/" + FOLDERADD + "/teaming/getLocations").success(function(data){

			if (data.status ==	"ok"){

				$scope.states = data.states;
				$scope.cities = data.cities;
			}
		})

		$scope.getCities = function(chosenState) {

			var res = [];
			
			$scope.institution.chosenCity  = "";
			$scope.institution.chosenState = "";

			if(chosenState){

				for (var i = $scope.cities.length - 1; i >= 0; i--) {

					if ($scope.cities[i].stateId == chosenState.stateId){

						res.push($scope.cities[i]);
					}
				}
			}

			return res;
		}

		$scope.sendSuggestionData = function(){

			if(
			   ($scope.institution.address     == "") && 
			   ($scope.institution.email       == "") &&
			   ($scope.institution.number      == "") && 
			   ($scope.institution.referrer    == "") &&
			   ($scope.institution.description == "") 
			){
				$('#noContactData').modal('show');
			}
			else{

				if($scope.institution.state){

					$scope.institution.chosenState = $scope.institution.state.stateId;
				}

				if ($scope.institution.cityInState){

					if ($scope.institution.cityInState.stateId != $scope.institution.state.stateId){

						$scope.institution.cityInState = null;
					}
					else{

						$scope.institution.chosenCity = $scope.institution.cityInState.cityId;
					}
				}


				$http({
					method	: 'POST',
					url 	: 'teaming/sendUserSuggestion',
					data 	: $scope.institution
				}).success(function(data){

					if (data.status ==	"ok"){
						
						$location.path('/teaming/acceptedSuggestion');
					}
					else{
	
						$scope.errors = data.message;
					}
				})
			}			
		}	
	}

	CteamingAdminInstitutions.$inject = ['$scope', '$routeParams', "$http", '$timeout', '$location','nav', '$rootScope', '$window', 'realTime'];
	function CteamingAdminInstitutions ($scope, $routeParams, $http, $timeout, $location, nav, $rootScope, $window, realTime){

		nav.setSelected('teaming');

		$scope.numberSelected  	     = 0;
		$scope.suggestedInstitutions = [];
		$scope.savedInstitution 	 = {
			status         : "",
			division 		: "",
			name 			: "",
			number			: "",
			selected 		: "",
			suggestedFrom 	: "",
			suggestedId 	: "",
			turn 			: "",
			userId 			: "",
		};
		$scope.institutionSelected   = false;
		$scope.isEmpty 				 = false;
		$scope.error  				 = false;
		$scope.states 				 = null;
		$scope.cities 				 = null;
		$scope.loading  			 = true;
		$scope.selectedIndex 		 = null;

		var selectedInstitutions = [];

		$scope.exportPdf = function(address) {
			
			var selected = "";

			for (var i = 0; i < selectedInstitutions.length; i++) {
				
				selected = selected+selectedInstitutions[i]+'/';
			}

			$window.open(address+selected,'_blank');
		}

		getInstitutions();

		function getInstitutions() {

		$http({
			method	: 	'POST',
			url 	: 	'teaming/getInstitutions',
		}).success(function(data){

			if (data.status ==	"ok"){

				$scope.suggestedInstitutions = data.data;

				if (data.status ==	"ok"){
	
					$scope.loading 	= false;
					$timeout(function() {
							$('.datatable').DataTable({
								language: {
									"sProcessing":     "Procesando...",
									"sLengthMenu":     "Mostrar _MENU_ instituciones",
									"sZeroRecords":    "No se encontraron resultados",
									"sEmptyTable":     "No hay instituciones sugeridas",
									"sInfo":           "Mostrando instituciones del _START_ al _END_ de un total de _TOTAL_ instituciones",
									"sInfoEmpty":      "Mostrando instituciones del 0 al 0 de un total de 0 instituciones",
									"sInfoFiltered":   "(filtrado de un total de _MAX_ instituciones)",
									"sInfoPostFix":    "",
									"sSearch":         "Buscar:",
									"sUrl":            "",
									"sInfoThousands":  ",",
									"sLoadingRecords": "Cargando...",
									"oPaginate": {
										"sFirst":    "Primero",
										"sLast":     "Último",
										"sNext":     "Siguiente",
										"sPrevious": "Anterior"
									},
									"oAria": {
										"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
										"sSortDescending": ": Activar para ordenar la columna de manera descendente"
									}
								}
							});
					});
				}
			}
			$scope.loading = false;
		})	
		}


		$scope.select = function (institution) {

			if(institution.selected){
				selectedInstitutions.push(institution.suggestedId);
			}
			else{
				var index =selectedInstitutions.findIndex(i => i == institution.suggestedId);
				selectedInstitutions.splice(index,1);
			}

			$scope.numberSelected = selectedInstitutions.length;
		}

		$scope.exportExcel = function () {

			var columns = [];
			$('#table-joined-users tr:first th').each(function () {
				var text = $(this).text();
				text = text.trim();
				text = text.replace("↵","");
				columns.push(text);
			})
			$('#columns').val(columns);
			
			$('#selectedInstitutions').val(selectedInstitutions);
			
			if(selectedInstitutions.length > 0){
				$("#exportForm").submit();	
			}
		}

		$scope.takeSuggestion = function(id) {

			$scope.loading = true;
			$scope.suggestedInstitutions = [];

			$http({
				method	: 	'POST',
				url 	: 	'teaming/acceptSuggestion',
				data    :   id
			}).success(function(data) {

				var suggested = {

					name : data.message.name,
					by 	 : data.message.userId,
				}

				realTime.socket.emit('accepted_suggestion', suggested);

				$http({
					method	: 	'POST',
					url 	: 	'teaming/getInstitutions',
				}).success(function(data){
					
					if (data.status ==	"ok"){
						
						$scope.loading = false;
						$scope.suggestedInstitutions = data.data;

						
					}
				})		
			})
		}
	}

	CteamingSuggestionDetails.$inject = ['$scope', '$routeParams', 'nav', '$http', '$rootScope', '$window']
	function CteamingSuggestionDetails ($scope, $routeParams, nav, $http, $rootScope, $window){

		nav.setSelected('Teaming');

		$scope.originalSuggestion = null;
		$scope.suggestion 		  = null;
		$scope.success 	  		  = null;
		$scope.errors 	  		  = null;

		getSuggestion();

		function getSuggestion() {
			
			$scope.originalSuggestion = {};

			$http.get("/" + FOLDERADD + "/teaming/getLocations").success(function(data){

				if (data.status ==	"ok"){
	
					$scope.states 	= data.states;
					$scope.cities 	= data.cities;
					$scope.loading 	= false;

					$http({
						method	: 	'POST',
						url 	: 	'teaming/getInstitution',
						data 	: 	$routeParams.suggestionId
					}).success(function(data){
			
						if (data.status == 'ok'){
			
							$scope.suggestion = data.message[0];
							$scope.originalSuggestion.address     	= $scope.suggestion.address;
							$scope.originalSuggestion.description 	= $scope.suggestion.description;
							$scope.originalSuggestion.email 		= $scope.suggestion.email;
							$scope.originalSuggestion.idCity 		= $scope.suggestion.idCity;
							$scope.originalSuggestion.idProvince 	= $scope.suggestion.idProvince;
							$scope.originalSuggestion.locality 		= $scope.suggestion.locality;
							$scope.originalSuggestion.name 			= $scope.suggestion.name;
							$scope.originalSuggestion.number 		= $scope.suggestion.number;
							$scope.originalSuggestion.province 		= $scope.suggestion.province;
							$scope.originalSuggestion.referrer 		= $scope.suggestion.referrer;

							for (var i = $scope.states.length - 1; i >= 0; i--) {

								if ($scope.states[i].stateId == $scope.suggestion.idProvince) {

									$scope.suggestion.newState = $scope.states[i];
								}
							}

							for (var i = $scope.cities.length - 1; i >= 0; i--) {

								if ($scope.cities[i].cityId == $scope.suggestion.idCity) {

									$scope.suggestion.newCity = $scope.cities[i];
								}
							}
						}
						else{
							
							$scope.errors = data.message;
						}
					})
				}
				else{
			
					$scope.errors 	  = data.message;
				}
			})
		}


		$scope.updatesuggestion = function() {

			$scope.success 	= null;
			$scope.errors 	= null;

			var updatedInstitution = $scope.suggestion;

			if (updatedInstitution.newState) {

				updatedInstitution.chosenState = updatedInstitution.newState.stateId;
			}
			else{

				updatedInstitution.chosenState = updatedInstitution.idProvince;
			}

			if (updatedInstitution.newCity) {

				updatedInstitution.chosenCity  = updatedInstitution.newCity.cityId;
			}
			else{

				updatedInstitution.chosenCity  = updatedInstitution.idCity;
			}

			if(
			   (updatedInstitution.address     == "") && 
			   (updatedInstitution.email       == "") &&
			   (updatedInstitution.number      == "") && 
			   (updatedInstitution.referrer    == "") &&
			   (updatedInstitution.description == "") 
			){
				$('#noContactData').modal('show');
			}
			else{
				$http({
					method	: 	'POST',
					url 	: 	'teaming/updateInstitution',
					data 	: 	updatedInstitution
				}).success(function(data) {

					if(data.status == 'ok'){
	
						$scope.success = data.message;
						getSuggestion();
					}
					else{
	
						$scope.errors = data.message;
					}
				})
			}	
		}

		$scope.restore = function() {
			
			$scope.suggestion.address 		= $scope.originalSuggestion.address;     	
			$scope.suggestion.description 	= $scope.originalSuggestion.description; 	
			$scope.suggestion.email 		= $scope.originalSuggestion.email; 		
			$scope.suggestion.idCity 		= $scope.originalSuggestion.idCity; 		
			$scope.suggestion.idProvince 	= $scope.originalSuggestion.idProvince; 	
			$scope.suggestion.locality 		= $scope.originalSuggestion.locality; 		
			$scope.suggestion.name 			= $scope.originalSuggestion.name; 			
			$scope.suggestion.number 		= $scope.originalSuggestion.number; 		
			$scope.suggestion.province 		= $scope.originalSuggestion.province; 		
			$scope.suggestion.referrer 		= $scope.originalSuggestion.referrer; 		
		}

		$scope.getCities = function(chosenState) {

			var res = [];

			if((chosenState) && ($scope.cities)){

				for (var i = $scope.cities.length - 1; i >= 0; i--) {

					if ($scope.cities[i].stateId == chosenState.stateId){

						res.push($scope.cities[i]);
					}
				}
			}

			return res;
		}	
	}


	teamingAgreeService.$inject = ["$http", "$rootScope"];
	function teamingAgreeService($http, $rootScope){
		var t = {
			unread: 0,
			getUnread: function() {
				$http.get("/"+FOLDERADD+"/teaming/GetUnreadForms").success(function(data) {
					if (data.status == "ok"){
						t.unread = data.unread;
						$rootScope.$broadcast("update_unread_forms");
					}
				})
			}
		};
		return t;
	}

	newCommunication.$inject = ["$http", "$rootScope"];
	function newCommunication($http, $rootScope){

		var data = {
			sendMessage : {}
		};

		return{

			getMessage: function () {

				return data.sendMessage;
			},
			setMessage: function (sendMessage) {

				data.sendMessage = sendMessage;
			}
		}
	}

	userData.$inject = ["$http", "$rootScope"];
	function userData($http, $rootScope){
		var data = {
			sendMessage : {}
		};

		return{
			getMessage: function () {
				return data.sendMessage;
			},

			setMessage: function (sendMessage) {
				data.sendMessage = sendMessage;
			}
		}
	}

})();
