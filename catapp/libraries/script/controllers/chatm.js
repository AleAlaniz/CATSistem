(function() {
	'use strict';
	angular
	.module('catnet')
	.config(['$routeProvider', function($routeProvider) {
		$routeProvider
		.when('/chat/multi', {
			templateUrl	: '/'+FOLDERADD+'/chat/multi',
			controller 	: 'Cchatm'
		})
		.when('/chat/multi/:id', {
			templateUrl	: '/'+FOLDERADD+'/chat/multi',
			controller 	: 'Cchatm'
		})
		.when('/chat/newmulti', {
			templateUrl	: '/'+FOLDERADD+'/chat/newmulti',
			controller 	: 'Cnewchatm'
		});
	}])
	.controller ('Cchatm'	, Cchatm)
	.controller ('Cnewchatm', Cnewchatm)
	.factory 	('chatsm'	, FchatsM);

	Cnewchatm.$inject = ['$scope', 'nav', '$location', 'realTime', 'chatsu', 'chatsm'];
	function Cnewchatm($scope, nav, $location, realTime, chatsu, chatsm) {
		nav.setSelected('chat');

		$scope.message = '';
		$scope.subject = '';
		$scope.unreadu 			= chatsu.unread;
		$scope.unreadm 			= chatsm.unread;
		$scope.$on("update_unread_chatu", function() {
			$scope.unreadu = chatsu.unread;
		});
		
		
		$scope.$on("update_unread_chatm", function() {
			$scope.unreadm = chatsm.unread;
		});
		$scope.submitChat = submitChat;

		function submitChat() {
			var incomplete = false;
			var haveTo = false;
			jQuery.each($('input[name="userGroups[]"]'), function(i, userGroup) {
				if(userGroup.checked)
				{
					haveTo = true;
					return;
				}
			});

			if (!haveTo) {
				haveTo = false;
				jQuery.each($('input[name="sites[]"]'), function(i, site) {
					if(site.checked)
					{
						haveTo = true;
						return;
					}
				});
				if (!haveTo) {


					if ($('input[name="users[]"]').length <= 0) {
						incomplete = true;
					}
				}

			}
			if ($scope.subject == '' || $scope.message == '') {
				incomplete = true;
			}

			if(incomplete)
			{
				$('#no-complete').modal('show');
			}
			else
			{
				$('#chatm_create').attr('disabled', 'disabled');
				$('#chatm_create').html("<i class='fa fa-refresh fa-spin fa-lg fa-fw'></i>");
				var data = {
					message : $scope.message,
					subject : $scope.subject,
					user 	: $('input[name="user"]').val()
				};
				$('#chatForm').ajaxSubmit({
					url : '/'+FOLDERADD+'/chat/newmulti',
					dataType : 'json',
					success: function(data) {
						if (data.status == 'ok') {	
							var datas = {
								id 		: data.chat,
								to 		: []
							};	
							for (var i = 0; i < data.to.length; i++) {
								datas.to.push(data.to[i].userId);
							}
							realTime.socket.emit('chatm_message', datas);
							$location.path('/chat/multi/'+data.chat);
							$scope.$apply()
						}
					}
				});
			}
		}
	}

	Cchatm.$inject = ['$scope' ,'nav', 'chatsm', 'realTime', '$routeParams', '$filter', '$location', 'chatsu', '$http', '$compile'];
	function Cchatm($scope, nav, chatsm, realTime, $routeParams, $filter, $location, chatsu, $http, $compile) {
		nav.setSelected('chat');
		$scope.users = [];
		$scope.searchUsersUrl = "/"+FOLDERADD+'/Chat/searchChatUsers/';
		$scope.searchUsersId = $routeParams.id;

		$scope.chatfilter 		= '';
		$scope.chatIdSelected 	= $routeParams.id;
		$scope.chats 			= [];
		$scope.infoOpen 		= false;
		$scope.unreadu 			= chatsu.unread;
		$scope.unreadm 			= chatsm.unread;
		$scope.subjectEdition	= false;
		$scope.previousChatLabel= "";
		$scope.error = "";

		$scope.getOldMessages 	= getOldMessages;
		$scope.sendMessage		= sendMessage;
		$('[data-toggle="tooltip"]').tooltip(); 

		$scope.$on("update_unread_chatu", function() {
			$scope.unreadu = chatsu.unread;
		});

		$scope.$on("update_unread_chatm", function() {
			$scope.unreadm = chatsm.unread;
		});

		$scope.$on('$destroy', destroy)

		$scope.$on('chatm_delete', function(e,id) {
			if ($scope.chatIdSelected && $scope.chatIdSelected == id) {
				$location.path('/chat/multi');
				$scope.$apply();
			}
			else{
				chatsm.getUnread();
				chatsm
				.getChats()
				.success(function(data) {
					if (data.status == 'ok') {
						$scope.chats = data.chats;
					}
				});
			}
		});

		$scope.$on("chatm_new_subject", function() {
			chatsm
			.getChats()
			.success(function(data) {
				if (data.status == 'ok') {
					$scope.chats = data.chats;
					realTime.suscribe('chatsm');
					if ($scope.chatIdSelected) {
						chatsm
						.getChatById($scope.chatIdSelected)
						.success(getChatSuccess);
					}
				}
			});
		});

		$scope.getUserId =function(userId){
			$scope.currentUserId = userId;
			
		}

		$scope.changeSubject = function () {
			$scope.error = "";
			$http({
				method 	: 'POST',
				url 	: '/'+FOLDERADD+'/chat/updateChatSubject',
				data 	: {"chatId": $routeParams.id, "newSubject":$scope.chat.label}
			}).success(function(data) {
				if(data.status == "ok"){
					realTime.socket.emit('chatm_new_subject',data.unread);
					$scope.subjectEdition	= false;
				}
				else{
					if (data.status == 'error'){
						$scope.error = '<p style="color:red;"> La longitud del asunto nuevo no debe superar los 250 caracteres.</p>'
					}
				}
			})

			
		}
		$scope.restoreSubject = function () {
			$scope.chat.label = $scope.previousChatLabel;
			$scope.subjectEdition	= false;
		}
		
		chatsm
		.getChats()
		.success(function(data) {
			if (data.status == 'ok') {
				$scope.chats = data.chats;
				realTime.suscribe('chatsm');
				if ($scope.chatIdSelected) {
					chatsm
					.getChatById($scope.chatIdSelected)
					.success(getChatSuccess);
				}
				$scope.$on('new_chatm_message', function() {
					chatsm
					.getChats()
					.success(function(data) {
						if (data.status == 'ok') {
							$scope.chats = data.chats;
						}
					});
				});
			}
		});
		
		$('#inputText').keypress(function(e) {
			if(e.which == 13) {
				$scope.sendMessage();
			}
		});

		function destroy() {
			if ($scope.chatIdSelected) {
				realTime.unsuscribe('chatm', $scope.chatIdSelected);
			}
			realTime.unsuscribe('chatsm');
		}

		function getChatSuccess(data) {

			if (data.status == 'ok') {
				$scope.chat 			= data.chat;
				$scope.previousChatLabel		= $scope.chat.label
				$scope.to 				= [];
				for (var i = 0; i < $scope.chat.users.length; i++) {
					$scope.to.push($scope.chat.users[i].userId);
				}
				$scope.chat.myuserId 	= $scope.userId;
				$scope.chat.chatmId 	= data.chat.chatmId;
				$scope.$on('chatm_message '+$scope.chat.chatmId, function(e,m) {
					AppendMessage(m, true);
				});
				realTime.suscribe('chatm', $scope.chat.chatmId);
				if (data.chat.unreads) {
					chatsm.getUnread();
				}

				if($scope.chat.userId == $scope.currentUserId){
					var html = '<span ng-bind="chat.label" ng-show="subjectEdition==false" ng-dblclick="subjectEdition = true ; showAdd = false ;" data-toggle="tooltip" aria-label="<?php echo $this->lang->line("hover_double_click");?>" title="<?php echo $this->lang->line("hover_double_click");?>"> </span>';
					var e = $compile(html)($scope);
					angular.element("#el").replaceWith(e);
				}

				setTimeout(() => {
					$("#inputText").emoji({

						// show emoji groups
						showTab: true,
						button: '#emojiPicker',
						// 'fade', slide' or 'none'
						animation: 'none',

						icons: [{
							name: "Emoji", // Emoji name
							path: "catapp/libraries/emoji/img/tieba/",
							maxNum: 50,
							file: ".jpg", // file extension name
							placeholder: ":{alias}:",
							excludeNums: [], // exclude emoji icons
							title: {}, // titles of emoji icons
							alias: {
								1: "hehe",
								2: "haha",
								3: "tushe",
								4: "a",
								5: "ku",
								6: "lu",
								7: "kaixin",
								8: "han",
								9: "lei",
								10: "heixian",
								11: "bishi",
								12: "bugaoxing",
								13: "zhenbang",
								14: "qian",
								15: "yiwen",
								16: "yinxian",
								17: "tu",
								18: "yi",
								19: "weiqu",
								20: "huaxin",
								21: "hu",
								22: "xiaonian",
								23: "neng",
								24: "taikaixin",
								25: "huaji",
								26: "mianqiang",
								27: "kuanghan",
								28: "guai",
								29: "shuijiao",
								30: "jinku",
								31: "shengqi",
								32: "jinya",
								33: "pen",
								34: "aixin",
								35: "xinsui",
								36: "meigui",
								37: "liwu",
								38: "caihong",
								39: "xxyl",
								40: "taiyang",
								41: "qianbi",
								42: "dnegpao",
								43: "chabei",
								44: "dangao",
								45: "yinyue",
								46: "haha2",
								47: "shenli",
								48: "damuzhi",
								49: "ruo",
								50: "OK"
							},
						},
						{
							name: "GIFS",
							path: "catapp/libraries/emoji/img/qq/",
							maxNum: 91,
							excludeNums: [41, 45, 54],
							file: ".gif",
							placeholder: "#qq_{alias}#"
						}, 
						{
							name: "WSEmojis",
							path: "catapp/libraries/emoji/img/emoji/",
							maxNum: 84,
							file: ".png",
							placeholder: "#emoji_{alias}#"
						}]
					});	
				}, 2000);
			}
		}


		function AppendMessage(message, apply) {
			if (message.userId != userData.userId) {	
				chatsm.viewMessage(message.chatmmessageId);
			}

			var exist = $filter('filter')($scope.chat.messages, {chatmmessageId : message.chatmmessageId});
			if (exist && exist.length == 0) {
				if ($scope.chat.chatmId == message.chatmId) {	
					$scope.chat.messages.push(message);
					if (apply) {$scope.$apply();}
				}
			}
		}	

		function getOldMessages() {
			$scope.gettingOldMessages = true;
			var first = $filter('orderBy')($scope.chat.messages, 'chatmmessageId')[0].chatmmessageId;
			chatsm
			.getOldMessages($scope.chatIdSelected,first)
			.success(getOldMessagesSuccess);
		}

		function getOldMessagesSuccess(data) {
			if (data.status == 'ok') {
				$scope.chat.messages = $scope.chat.messages.concat(data.messages);
				$scope.gettingOldMessages = false;
			}
			else if (data.status == 'empty') {
				$scope.fullOldMessages = true;
				$scope.gettingOldMessages = false;
			}
		}

		function sendMessage() {
			$scope.chat.newmessage = $('#inputText').html();
			if ($scope.chat.newmessage && $scope.chat.newmessage != '') {
				var data = { 'id' : $scope.chatIdSelected, 'message': $scope.chat.newmessage};
				chatsm
				.sendMessage(data)
				.success(function(data) {
					if (data.status == 'ok') {
						AppendMessage(data.message);
						var data = {
							id 		: data.message.chatmId,
							message : data.message,
							to 		: $scope.to
						};
						realTime.socket.emit('chatm_message', data);
					}
				});
				setTimeout(() => {
					$('#inputText').html("");
				}, 100);
			}
		}

		$scope.getMessageInfo = id =>{
			chatsm.getMessageInfo(id)
			.success(res=>{
				if(res.status == "success"){
					$scope.infoUsers = res.data;
				}
			})
		}

		$('#confirm-delete').on('show.bs.modal', function (event) {
			var id 	= $(event.relatedTarget).data('id')
			$(this).find('#delete-yes').data('chatid', id);
		});

		$('#delete-yes').click(function(e) {
			var btn = $(this);
			btn.hide();
			$('#delete-loading').show();
			chatsm
			.deleteChat(btn.data('chatid'))
			.success(function(data) {
				if (data.status == 'ok') {
					$('#confirm-delete').modal('hide');
					$location.path('/chat/multi')
				}
			});
		});

		$('#confirm-deleteall').on('show.bs.modal', function (event) {
			var id 	= $(event.relatedTarget).data('id')
			$(this).find('#deleteall-yes').data('chatid', id);
		});

		$('#deleteall-yes').click(function(e) {
			var btn = $(this);
			btn.hide();
			$('#deleteall-loading').show();
			chatsm
			.deleteAllChat(btn.data('chatid'))
			.success(function(data) {
				if (data.status == 'ok') {
					var data = {
						id 		: $scope.chatIdSelected,
						to 		: $scope.to
					};
					realTime.socket.emit('chatm_delete', data);
					$('#confirm-deleteall').modal('hide');
					$location.path('/chat/multi')
				}
			});
		});

		$('#confirm-deleteuser').on('show.bs.modal', function (event) {
			var id 	= $(event.relatedTarget).data('id')
			$(this).find('#deleteuser-yes').data('id', id);
		});

		$('#deleteuser-yes').click(function(e) {
			var btn = $(this);
			var id  = btn.data('id');
			btn.hide();
			$('#deleteuser-loading').show();
			chatsm
			.deleteUser($scope.chatIdSelected, id)
			.success(function(data) {
				if (data.status == 'ok') {
					var data = {
						id 		: $scope.chatIdSelected,
						to 		: [id]
					};
					realTime.socket.emit('chatm_delete', data);
					$('#confirm-deleteuser').modal('hide');
					$location.path('/chat/multi')
				}
			});
		});

		$scope.addNewMember = function () {
			var users = [];
			$scope.loading = true;
			for (var index = 0; index < $scope.users.length; index++) {
				var element = $scope.users[index];
				users.push(element.userId);
			}
			$http({
				method 	: 'POST',
				url 	: '/'+FOLDERADD+'/chat/addNewMember',
				data 	: {
					users : users,
					chatId: $routeParams.id
				}
			}).success(function(data) {
				if(data == 'success'){
					$scope.loading = false;
					$scope.success = true;
					getChatUsers();
					$scope.users = [];
				}
			});
		}

		$scope.closePanel = function () {
			$scope.showAdd = false; 
			$scope.users = [];
			$scope.success = false;
		}

		function getChatUsers() {
			$http({
				method 	: 'POST',
				url 	: '/'+FOLDERADD+'/chat/getChatUsers',
				data 	: {
					chatId : $routeParams.id
				}
			}).success(function(data) {
				$scope.chat.users = data;
			});
		}

		$scope.$watch("users.length",function (newValue,oldValue) {
			if(newValue > 0)
				$scope.success = false;
		});
	}

	FchatsM.$inject = ['$rootScope', '$http'];
	function FchatsM($rootScope, $http) {
		var sv =  {
			unread 					: 0,
			getChats 				: function() {
				return $http.get('/'+FOLDERADD+'/chat/getchatsm');
			},
			deleteChat				: function(id) {
				return $http.get('/'+FOLDERADD+'/chat/deletechatm?id='+id);
			},
			deleteAllChat				: function(id) {
				return $http.get('/'+FOLDERADD+'/chat/deleteallchatm?id='+id);
			},
			getChatById				: function(id) {
				return $http.get('/'+FOLDERADD+'/chat/getchatmbyid?id='+id);
			},
			getOldMessages	: function(id, first) {
				return $http.get('/'+FOLDERADD+'/chat/getoldmmessages?id='+id+'&first='+first);
			},
			getNewMessages	: function(id, last) {
				return $http.get('/'+FOLDERADD+'/chat/getnewmessagesm?id='+id+'&last='+last);
			},
			deleteUser	: function(id, user) {
				return $http.get('/'+FOLDERADD+'/chat/deleteuser?id='+id+'&user='+user);
			},
			sendMessage				: function(data) {
				return $http.post('/'+FOLDERADD+'/chat/newmessagem', data);
			},
			viewMessage	: function(id) {
				return $http.get('/'+FOLDERADD+'/chat/addmessagemview?id='+id );
			},
			getUnread 	: function() {
				$http
				.get('/'+FOLDERADD+'/chat/getunreadm')
				.success(function(data) {
					if (data.status == 'ok') {
						sv.unread = data.unread;
						$rootScope.$broadcast('update_unread_chatm');
					}
				})
			},
			getMessageInfo 			: function (id) {
				let data = {
					id:id,
					type:'multi'
				}
				return $http.post('/'+FOLDERADD+'/chat/getMessageInfo',data);
			},
		}
		return sv;
	}


})();