(function() {
	'use strict';
	angular
	.module('catnet')
	.config(corporatedocsConfig)
	.factory('corporatedocs_message',corporatedocs_message);

	corporatedocsConfig.$inject = ['$routeProvider'];
	function corporatedocsConfig($routeProvider) {
		$routeProvider.
		when('/corporatedocs', {
			templateUrl: '/'+FOLDERADD+'/corporatedocs',
			controller: Ccorporatedocs
		}).
		when('/corporatedocs/create', {
			templateUrl: '/'+FOLDERADD+'/corporatedocs/create',
			controller: Ccorporatedocs_create
		}).
		when('/corporatedocs/createcategory', {
			templateUrl: '/'+FOLDERADD+'/corporatedocs/createcategory',
			controller: Ccorporatedocs_createcategory
		}).
		when('/corporatedocs/editcategory/:id', {
			templateUrl: '/'+FOLDERADD+'/corporatedocs/editcategory',
			// old templateUrl: function(params){ return '/'+FOLDERADD+'/corporatedocs/editcategory/'+params.id;},
			controller: Ccorporatedocs_editcategory
		}).
		when('/corporatedocs/edit/:id', {
			templateUrl: '/'+FOLDERADD+'/corporatedocs/edit',
			controller: Ccorporatedocs_edit
		});
	}

	corporatedocs_message.$inject = ['$rootScope'];
	function corporatedocs_message($rootScope) {
		var data = {}
		data.message = '';
		data.setMessage = function(message, auto_update) {
			this.message = message;
			if (auto_update) {
				$rootScope.$broadcast('corporatedocs_message');
			}
		}
		return data;
	}

	Ccorporatedocs.$inject = ['$scope','nav', 'corporatedocs_message', '$http'];
	function Ccorporatedocs($scope,nav, corporatedocs_message, $http)
	{
		nav.setSelected('corporatedocs');
		$scope.message 		= corporatedocs_message;
		$scope.loading 		= true;
		$scope.categories 	= [];
		$scope.categoriesSelecteds 	= [];
		$scope.docsPrincipal = [];
		$scope.categoriesPrincipal = [];
		$scope.anchoCol 	= 4;
		$scope.docs 		= [];
		$scope.setDelete 	= setDelete;

		$scope.getCorporateDocs = getCorporateDocs;
		$scope.setSelected 		= setSelected;
		$scope.checkView 		= checkView;
		$scope.getReport 		= getReport;
		$scope.toDelete 		= toDelete;
		$scope.abrirCategoria = abrirCategoria;
		$scope.volverAtras = volverAtras;

		$scope.reportOption = '';

		$scope.$on('corporatedocs_message', function () {
			$scope.message = corporatedocs_message;
		});

		$scope.$on("$destroy", function(){
			corporatedocs_message.setMessage('', false);
		});

		function setDelete(type, id) {
			$scope.deleting 	= false;
			$scope.deleteType 	= type;
			$scope.deleteId 	= id;
		}
		//funciones transicion
		function abrirCategoria(category) {
			if($scope.categoriesSelecteds.indexOf(category) == -1) {
				$scope.categoriesSelecteds.push(category);
			}else{
				$scope.categoriesSelectds = $scope.categoriesSelectds.slice(0,$scope.categoriesSelecteds.indexOf(category));
			}
			$scope.docs = category.docs;
			$scope.categories = $scope.allcategories.filter(cat => cat.corporatedoccatfather == category.corporatedoccategoryId);
			resizeCatDiv();
		}
		function volverAtras(){
			$scope.categoriesSelecteds.pop();
			if($scope.categoriesSelecteds.length == 0){
				$scope.categories = $scope.allcategories.filter(cat => cat.corporatedoccatfather == null);
				$scope.docs = $scope.docsPrincipal;
			}else{
				let category = $scope.categoriesSelecteds[$scope.categoriesSelecteds.length-1];
				$scope.categories = $scope.allcategories.filter(cat => cat.corporatedoccatfather == category.corporatedoccategoryId);
				$scope.docs = category.docs;
			}
			resizeCatDiv();
		}
		function resizeCatDiv(){
			if($scope.categories.length % 3 == 0){
				$scope.anchoCol 	= 4;
			}else if($scope.categories.length % 2 == 0 ){
				$scope.anchoCol 	= 6;
			}else if ($scope.categories.length == 1) {
				$scope.anchoCol 	= 12;
			}
		}
		// fin
		function getCorporateDocs() {
			$scope.loading 		= true;
			$scope.categories 	= [];
			$scope.docs 		= [];

			$http
			.get('/'+FOLDERADD+'/corporatedocs/getcorporatedocs')
			.success(function(res) {
				if (res.status == 'ok') {
					$scope.allcategories = res.categories;
					$scope.categories 	= $scope.allcategories.filter(cat => cat.corporatedoccatfather == null);
					$scope.docs 		= res.docs;
					$scope.docsPrincipal= res.docs;
					$scope.loading 		= false;
					resizeCatDiv();
				}
			});
		}
		function findsons(category){
		}
		function setSelected(doc) {
			doc.report 				= null;
			$scope.loadingView 		= false;
			$scope.loadingReport	= false;
			$scope.doc 				= doc;
			$scope.reportOption = '';
		}

		function checkView(doc) {
			$scope.loadingView = true;

			$http
			.get('/'+FOLDERADD+'/corporatedocs/checkview/'+ doc.corporatedocId)
			.success(function(res) {
				if (res.status == 'ok') {
					doc.isView = 1;
					$scope.loadingView 	= false;
				}
			});
		}

		function getReport(doc,option) {

			if(option != $scope.reportOption)
			{
				$scope.reportOption = option;
				$scope.loadingReport = true;

				$http.get('/'+FOLDERADD+'/corporatedocs/getreport/'+ doc.corporatedocId+'/'+$scope.reportOption)
				.success(function(res) {
					if (res.status == 'ok') {
						doc.report = res.data;
						$scope.loadingReport 	= false;
					}
				});
			}


		}

		function toDelete() {
			$scope.deleting = true;

			var url = $scope.deleteType == 'category' ? 'deletecategory' : 'delete';

			$http
			.get('/'+FOLDERADD+'/corporatedocs/'+url+'/'+ $scope.deleteId)
			.success(function(res) {
				if (res != 'invalid') {
					corporatedocs_message.setMessage(res);
					$scope.deleting = false;
					$scope.categoriesSelecteds = [];
					$scope.getCorporateDocs();
					$('#confirm-delete').modal('hide');
				}
			});
		}

		$scope.getCorporateDocs();



		/*$('[role="delete"]').click(function(e) {
			var btn = $(this);
			$.ajax({
				method	: "GET",
				url 	: btn.data('url')
			}).done(function(data) {
				if (data != 'invalid') {
					$('#confirm-delete').modal('hide');
					corporatedocs_message.setMessage(data, true);
					$scope.initCtrl();
				}
			});
});*/
}

Ccorporatedocs_create.$inject = ['$scope','nav', '$location', 'corporatedocs_message'];
function Ccorporatedocs_create($scope, nav, $location, corporatedocs_message)
{
	nav.setSelected('corporatedocs');
	$scope.formData = {};
	$scope.formData.title = '';
	$scope.saving=false;
	$scope.category = -1;
	$scope.cargarCampos = cargarCampos;
	if(typeof String.prototype.trim !== 'function') {
		String.prototype.trim = function() {
			return this.replace(/^\s+|\s+$/g, '');
		}
	}

	$scope.submitcorporatedocs = function () {

		var incomplete = false;
		var haveTo = false;
		$scope.saving=true;

		jQuery.each($('input[name="sections[]"]'), function(i, section) {
			if(section.checked)
			{
				haveTo = true;
				return;
			}
		});

		if (!haveTo) {
			haveTo = false;
			jQuery.each($('input[name="sites[]"]'), function(i, site) {
				if(site.checked)
				{
					haveTo = true;
					return;
				}
			});
			if (!haveTo) {
				haveTo = false;
				jQuery.each($('input[name="roles[]"]'), function(i, role) {
					if(role.checked)
					{
						haveTo = true;
						return;
					}
				});

				if (!haveTo) {
					haveTo = false;
					jQuery.each($('input[name="userGroups[]"]'), function(i, userGroup) {
						if(userGroup.checked)
						{
							haveTo = true;
							return;
						}
					});

					if (!haveTo) {
						haveTo = false;
						if ($('input[name="users[]"]').length <= 0) {
							incomplete = true;
						}
					}
				}

			}

		}

		if ($('#name').val().trim() == "" || $('#description').val().trim() == "") {incomplete=true;}

		if(incomplete)
		{
			$('#no-complete').modal('show');
			console.log('incompleto');
			$scope.saving=false;
		}
		else
		{

			$('#corporatedocsForm').ajaxSubmit({
				url : '/'+FOLDERADD+'/corporatedocs/create',
				dataType : 'json',
				success: function(data) {
					if (data.status == 'ok')
					{
						corporatedocs_message.setMessage(data.message, false);
						$location.path('/corporatedocs');
					}
					else
					{
						$scope.saving=false;
						$('#no-complete').modal('show');
					}
					$scope.$apply()
				}
			});
		}
	};

	$scope.finding = false;
	$scope.users = [];
	$scope.removeUser = function(i) {
		$scope.users.splice(i, 1);
	};
	$scope.addUser = function(i) {
		$scope.users.push($scope.userOptions[i]);
		$scope.userOptions.splice(i, 1);
	};
	$scope.userOptions = [];
	$scope.findLike = '';
	$scope.searchNumber = 0;
	$scope.searchActual = 0;
	$scope.searchUsers = function() {
		if($scope.findLike != '')
		{
			$scope.finding = true;
			var thisSearch = $scope.searchNumber;
			$scope.searchNumber = $scope.searchNumber+1;
			$.ajax({
				method 	: "POST",
				url		: '/'+FOLDERADD+'/corporatedocs/searchusers',
				data 	: {'like': $scope.findLike}
			}).done(function (data) {
				if(data == 'invalid'){
					
				}
				else if(data != 'empty' && thisSearch >= $scope.searchActual && $scope.findLike != '')
				{
					$scope.searchActual = thisSearch;
					$scope.$apply(function() {
						$scope.addUserOptions(data);
						$scope.finding = false;
					});
				}
				else if (thisSearch >= $scope.searchActual)
				{
					$scope.searchActual = thisSearch;
					$scope.$apply(function() {
						$scope.finding = false;
						$scope.userOptions = [];
					});
				}
			});
		}
		else
		{
			$scope.userOptions = [];
			$scope.finding = false;
		}
	};

	$scope.addUserOptions = function (users) {
		try{
			users = $.parseJSON(users);
		}catch(e){
			return;
		}
		$scope.userOptions = [];
		jQuery.each(users, function(i, user) {
			var exist = false;
			jQuery.each($scope.users, function(i, userExist) {
				if (userExist.userId == user.userId) {exist = true;}
			});
			if (!exist) {$scope.userOptions.push(user);}
		});
	};

	$scope.selectClick = function (e) {
		$('.select-input', e.currentTarget).focus();
	};
	//1752021
	function cargarCampos(){
		$.ajax({
			method 	: "post",
			url		: '/'+FOLDERADD+'/corporatedocs/getLinksForCategory',
			data 	: {'category': $scope.category}
		}).done(function (data) {
			if(data == 'invalid'){
				$(location).attr('href', '/'+FOLDERADD+'');
			}
			else if(data != 'empty')
			{
				var datajson = JSON.parse(data);
				$scope.categories = datajson.categories;
				$scope.perfiles = datajson.roles;
				$scope.secciones = datajson.sections;
				$scope.sitios = datajson.sites;
				$scope.grupouser = datajson.usergroups;
				if(datajson.error === "old"){
					if(datajson.sections)
						$scope.sections = Array(datajson.sections.length).fill(true);
					if(datajson.roles)
						$scope.roles = Array(datajson.roles.length).fill(true);
					if(datajson.sites)
						$scope.sites = Array(datajson.sites.length).fill(true);
					if(datajson.usergroups)
						$scope.usergroups = Array(datajson.usergroups.length).fill(true);
					if(datajson.users)
						$scope.users = datajson.users.map(user => {return {completeName : user.name, userId : user.userId, userName : user.userName};})
				}else{
					$scope.roles = [];
					$scope.sections = [];
					$scope.sites = [];
					$scope.usergroups = [];
				}
				$scope.$apply();
			}
		});
	};
	cargarCampos();
}
//1752021
Ccorporatedocs_createcategory.$inject = ['$scope','nav', '$location', 'corporatedocs_message',];
function Ccorporatedocs_createcategory($scope, nav, $location, corporatedocs_message)
{
	nav.setSelected('corporatedocs');

	$scope.formData = {};
	$scope.formData.title = '';
	$scope.saving=false;
	$scope.category = -1;
	$scope.cargarCampos = cargarCampos;

	if(typeof String.prototype.trim !== 'function') {
		String.prototype.trim = function() {
			return this.replace(/^\s+|\s+$/g, '');
		}
	}

	$scope.createCategory = function () {

		var incomplete = false;
		var haveTo = false;
		$scope.saving=true;

		jQuery.each($('input[name="sections[]"]'), function(i, section) {
			if(section.checked)
			{
				haveTo = true;
				return;
			}
		});

		if (!haveTo) {
			haveTo = false;
			jQuery.each($('input[name="sites[]"]'), function(i, site) {
				if(site.checked)
				{
					haveTo = true;
					return;
				}
			});
			if (!haveTo) {
				haveTo = false;
				jQuery.each($('input[name="roles[]"]'), function(i, role) {
					if(role.checked)
					{
						haveTo = true;
						return;
					}
				});

				if (!haveTo) {
					haveTo = false;
					jQuery.each($('input[name="userGroups[]"]'), function(i, userGroup) {
						if(userGroup.checked)
						{
							haveTo = true;
							return;
						}
					});

					if (!haveTo) {
						haveTo = false;
						if ($('input[name="users[]"]').length <= 0) {
							incomplete = true;
						}
					}
				}

			}

		}

		if ($('#name').val().trim() == "") {incomplete=true;}

		if(incomplete)
		{
			$('#no-complete').modal('show');
			console.log('incompleto');
			$scope.saving=false;
		}
		else
		{
			$('#corporatecatForm').ajaxSubmit({
				url : '/'+FOLDERADD+'/corporatedocs/createcategory',
				dataType : 'json',
				success: function(data) {
					if (data.status == 'ok')
					{
						corporatedocs_message.setMessage(data.message, false);
						$location.path('/corporatedocs');
					}
					else
					{
						$scope.saving=false;
						$('#no-complete').modal('show');
					}
					$scope.$apply();
				}
			});
		}
	};
	$scope.finding = false;
	$scope.users = [];
	$scope.removeUser = function(i) {
		$scope.users.splice(i, 1);
	};
	$scope.addUser = function(i) {
		$scope.users.push($scope.userOptions[i]);
		$scope.userOptions.splice(i, 1);
	};
	$scope.userOptions = [];
	$scope.findLike = '';
	$scope.searchNumber = 0;
	$scope.searchActual = 0;
	$scope.searchUsers = function() {
		if($scope.findLike != '')
		{
			$scope.finding = true;
			var thisSearch = $scope.searchNumber;
			$scope.searchNumber = $scope.searchNumber+1;
			$.ajax({
				method 	: "POST",
				url		: '/'+FOLDERADD+'/corporatedocs/searchusers',
				data 	: {'like': $scope.findLike}
			}).done(function (data) {
				if(data == 'invalid'){
					$(location).attr('href', '/'+FOLDERADD+'');
				}
				else if(data != 'empty' && thisSearch >= $scope.searchActual && $scope.findLike != '')
				{
					$scope.searchActual = thisSearch;
					$scope.$apply(function() {
						$scope.addUserOptions(data);
						$scope.finding = false;
					});
				}
				else if (thisSearch >= $scope.searchActual)
				{
					$scope.searchActual = thisSearch;
					$scope.$apply(function() {
						$scope.finding = false;
						$scope.userOptions = [];
					});
				}
			});
		}
		else
		{
			$scope.userOptions = [];
			$scope.finding = false;
		}
	};

	$scope.addUserOptions = function (users) {
		try{
			users = $.parseJSON(users);
		}catch(e){
			return;
		}
		$scope.userOptions = [];
		jQuery.each(users, function(i, user) {
			var exist = false;
			jQuery.each($scope.users, function(i, userExist) {
				if (userExist.userId == user.userId) {exist = true;}
			});
			if (!exist) {$scope.userOptions.push(user);}
		});
	};

	$scope.selectClick = function (e) {
		$('.select-input', e.currentTarget).focus();
	};
	//1752021
	function cargarCampos(){
		$.ajax({
			method 	: "post",
			url		: '/'+FOLDERADD+'/corporatedocs/getLinksForCategory',
			data 	: {'category': $scope.category}
		}).done(function (data) {
			if(data == 'invalid'){
				$(location).attr('href', '/'+FOLDERADD+'');
			}
			else if(data != 'empty')
			{
				var datajson = JSON.parse(data);
				$scope.categories = datajson.categories;
				$scope.perfiles = datajson.roles;
				$scope.secciones = datajson.sections;
				$scope.sitios = datajson.sites;
				$scope.grupouser = datajson.usergroups;
				if(datajson.error === "old"){
					if(datajson.sections)
						$scope.sections = Array(datajson.sections.length).fill(true);
					if(datajson.roles)
						$scope.roles = Array(datajson.roles.length).fill(true);
					if(datajson.sites)
						$scope.sites = Array(datajson.sites.length).fill(true);
					if(datajson.usergroups)
						$scope.usergroups = Array(datajson.usergroups.length).fill(true);
					if(datajson.users)
						$scope.users = datajson.users.map(user => {return {completeName : user.name, userId : user.userId, userName : user.userName};})
				}else{
					$scope.roles = [];
					$scope.sections = [];
					$scope.sites = [];
					$scope.usergroups = [];
				}
				$scope.$apply();
			}
		});
	};
	cargarCampos();
}
Ccorporatedocs_editcategory.$inject = ['$scope','nav', '$location', 'corporatedocs_message', '$routeParams'];
function Ccorporatedocs_editcategory($scope, nav, $location, corporatedocs_message, $routeParams)
{
	nav.setSelected('corporatedocs');
	$scope.name = null;
	$scope.sections = [];
	$scope.sites = [];
	$scope.roles = [];
	$scope.users = [];
	$scope.cargarCampos = cargarCampos;
	$scope.removeUser = function(i) {
		$scope.users.splice(i, 1);
	};

	$scope.addUser = function(i) {
		$scope.users.push($scope.userOptions[i]);
		$scope.userOptions.splice(i, 1);
	};
	$scope.userOptions = [];
	$scope.findLike = '';
	$scope.searchNumber = 0;
	$scope.searchActual = 0;
	$scope.searchUsers = function() {
		if($scope.findLike != '')
		{
			var thisSearch = $scope.searchNumber;
			$scope.searchNumber = $scope.searchNumber+1;
			$.ajax({
				method 	: "POST",
				url		: '/'+FOLDERADD+'/corporatedocs/searchusers',
				data 	: {'like': $scope.findLike}
			}).done(function (data) {
				if(data == 'invalid'){
					$(location).attr('href', '/'+FOLDERADD+'');
				}
				else if(data != 'empty' && thisSearch >= $scope.searchActual)
				{
					$scope.searchActual = thisSearch;
					$scope.$apply($scope.addUserOptions(data));
				}
				else if (thisSearch >= $scope.searchActual)
				{
					$scope.searchActual = thisSearch;
					$scope.$apply($scope.userOptions = []);
				}
			});
		}
		else
		{
			$scope.userOptions = [];
		}
	};

	$scope.addUserOptions = function (users) {
		try{
			users = $.parseJSON(users);
		}catch(e){
			return;
		}
		$scope.userOptions = [];
		jQuery.each(users, function(i, user) {
			var exist = false;
			jQuery.each($scope.users, function(i, userExist) {
				if (userExist.userId == user.userId) {exist = true;}
			});
			if (!exist) {$scope.userOptions.push(user);}
		});
	};

	$scope.selectClick = function (e) {
		$('.select-input', e.currentTarget).focus();
	};
//marcar correctas
	$.ajax({
		method	: "GET",
		url 	: "/"+FOLDERADD+"/corporatedocs/getcategorybyid/"+$routeParams.id
	}).done(function(data) {
		if (data != 'invalid') {
			try{
				data = $.parseJSON(data);
				$scope.links = data.links;
			}catch(e){
				return;
			}
			$scope.$apply(function(){
				$scope.name = data.name;
				$scope.category = data.corporatedoccatfather ? data.corporatedoccatfather : -1;
				cargarCampos();
			});

		}
	});
	// getlinks
	//1752021
	 function cargarCampos(){
		$.ajax({
			method 	: "post",
			url		: '/'+FOLDERADD+'/corporatedocs/getLinksForCategory',
			data 	: {'category': $scope.category,'myId':$routeParams.id}
		}).done(function (data) {
			if(data == 'invalid'){
				$(location).attr('href', '/'+FOLDERADD+'');
			}
			else if(data != 'empty')
			{
				var datajson = JSON.parse(data);
				$scope.categories = datajson.categories;
				$scope.perfiles = datajson.roles;
				if (datajson.roles)
					$scope.roles = datajson.roles.map(value => {
					if(value.checked == null)
						return false;
					else
						return true;
				});
				$scope.secciones = datajson.sections;
				if (datajson.sections)
					$scope.sections = datajson.sections.map(value => {
					if(value.checked == null)
						return false;
					else
						return true;
				});
				$scope.sitios = datajson.sites;
				if (datajson.sites)
					$scope.sites = datajson.sites.map(value => {
					if(value.checked == null)
						return false;
					else
						return true;
				})
				$scope.grupouser = datajson.usergroups;
				if (datajson.usergroups)
					$scope.usergroups = datajson.usergroups.map(value => {
					if(value.checked == null)
						return false;
					else
						return true;
				});
				if($scope.links) {
					var aux = $scope.links.filter(link => link.userId != null);
					if(aux){
						$scope.users = aux.map(link => {
							return {completeName : link.name, userId : link.userId, userName : link.userName};
						});
					}
				}
				//
				$scope.$apply();
			}
		});
	};
	$scope.editCategory = function () {
		var incomplete = false;
		var haveTo = false;
		jQuery.each($('input[name="sections[]"]'), function(i, section) {
			if(section.checked)
			{
				haveTo = true;
				return;
			}
		});

		if (!haveTo) {
			haveTo = false;
			jQuery.each($('input[name="sites[]"]'), function(i, site) {
				if(site.checked)
				{
					haveTo = true;
					return;
				}
			});
			if (!haveTo) {
				haveTo = false;
				jQuery.each($('input[name="roles[]"]'), function(i, role) {
					if(role.checked)
					{
						haveTo = true;
						return;
					}
				});

				if (!haveTo) {
					haveTo = false;
					jQuery.each($('input[name="userGroups[]"]'), function(i, userGroup) {
						if(userGroup.checked)
						{
							haveTo = true;
							return;
						}
					});

					if (!haveTo) {
						haveTo = false;
						if ($('input[name="users[]"]').length <= 0) {
							incomplete = true;
						}
					}
				}

			}
		}
		if (!$scope.name) {incomplete=true;}
		if(incomplete)
		{
			$('#no-complete').modal('show');
		}
		else
		{
			$('#corporatedocs_editcategory').attr('disabled', 'disabled');
			$('#corporatedocs_editcategory').html("<i class='fa fa-refresh fa-spin fa-lg fa-fw'></i>");
			$('#corporatecatForm').ajaxSubmit({
				method 	: "POST",
				url : '/'+FOLDERADD+'/corporatedocs/editCategory/'+$routeParams.id,
				success: function(data) {
					if (data != 'invalid') {
						corporatedocs_message.setMessage(data, false);
						$location.path('/corporatedocs');
						$scope.$apply();
					}
				}
			});
		}
	};
}
Ccorporatedocs_edit.$inject = ['$scope','nav', '$location', 'corporatedocs_message', '$routeParams', 'tinyMCE'];
function Ccorporatedocs_edit($scope,nav, $location, corporatedocs_message, $routeParams, tinyMCE)
{
	nav.setSelected('corporatedocs');
	$scope.corporatedocdata = null;
	$scope.sections = [];
	$scope.sites = [];
	$scope.roles = [];
	$scope.users = [];
	$scope.cargarCampos = cargarCampos;
	$scope.removeUser = function(i) {
		$scope.users.splice(i, 1);
	};

	$scope.addUser = function(i) {
		$scope.users.push($scope.userOptions[i]);
		$scope.userOptions.splice(i, 1);
	};
	$scope.userOptions = [];
	$scope.findLike = '';
	$scope.searchNumber = 0;
	$scope.searchActual = 0;
	$scope.searchUsers = function() {
		if($scope.findLike != '') 
		{
			var thisSearch = $scope.searchNumber;
			$scope.searchNumber = $scope.searchNumber+1;
			$.ajax({
				method 	: "POST",
				url		: '/'+FOLDERADD+'/corporatedocs/searchusers',
				data 	: {'like': $scope.findLike}
			}).done(function (data) {
				if(data == 'invalid'){
					
				}
				else if(data != 'empty' && thisSearch >= $scope.searchActual)
				{
					$scope.searchActual = thisSearch;
					$scope.$apply($scope.addUserOptions(data));
				}
				else if (thisSearch >= $scope.searchActual)
				{
					$scope.searchActual = thisSearch;
					$scope.$apply($scope.userOptions = []);
				}
			});
		}
		else
		{
			$scope.userOptions = [];
		}
	};

	$scope.addUserOptions = function (users) {
		try{
			users = $.parseJSON(users);
		}catch(e){
			return;
		}
		$scope.userOptions = [];
		jQuery.each(users, function(i, user) {
			var exist = false;
			jQuery.each($scope.users, function(i, userExist) {
				if (userExist.userId == user.userId) {exist = true;}
			});
			if (!exist) {$scope.userOptions.push(user);}
		});
	};

	$scope.selectClick = function (e) {
		$('.select-input', e.currentTarget).focus();
	};

	$.ajax({
		method	: "GET",
		url 	: "/"+FOLDERADD+"/corporatedocs/getcorporatedocsbyid/"+$routeParams.id
	}).done(function(data) {
		if (data != 'invalid') {
			try{
				data = $.parseJSON(data);
				$scope.links = data.links;
			}catch(e){
				return;
			}
			$scope.$apply(function() {
				$scope.corporatedocdata = {
					name 		: data.name,
					category 	: data.corporatedoccategoryId || '',
					description : data.description
				};

				$scope.required=(data.required == '1');
				cargarCampos();
			});
		}
	});
	// getlinks
	//1752021
	function cargarCampos(){
		$.ajax({
			method 	: "post",
			url		: '/'+FOLDERADD+'/corporatedocs/getLinksForDocument',
			data 	: {'category': $scope.corporatedocdata.category,'myId':$routeParams.id}
		}).done(function (data) {
			if(data == 'invalid'){
				$(location).attr('href', '/'+FOLDERADD+'');
			}
			else if(data != 'empty')
			{
				var datajson = JSON.parse(data);
				$scope.categories = datajson.categories;
				$scope.perfiles = datajson.roles;
				if (datajson.roles)
					$scope.roles = datajson.roles.map(value => {
						if(value.checked == null)
							return false;
						else
							return true;
					});
				$scope.secciones = datajson.sections;
				if (datajson.sections)
					$scope.sections = datajson.sections.map(value => {
						if(value.checked == null)
							return false;
						else
							return true;
					});
				$scope.sitios = datajson.sites;
				if (datajson.sites)
					$scope.sites = datajson.sites.map(value => {
						if(value.checked == null)
							return false;
						else
							return true;
					})
				$scope.grupouser = datajson.usergroups;
				if (datajson.usergroups)
					$scope.usergroups = datajson.usergroups.map(value => {
						if(value.checked == null)
							return false;
						else
							return true;
					});
				if($scope.links) {
					var aux = $scope.links.filter(link => link.userId != null);
					if(aux){
						$scope.users = aux.map(link => {
							return {completeName : link.name, userId : link.userId, userName : link.userName};
						});
					}
				}
				$scope.$apply();
			}
		});
	};
	$scope.editCorporatedocs = function () {
		var incomplete = false;
		var haveTo = false;

		jQuery.each($('input[name="sections[]"]'), function(i, section) {
			if(section.checked)
			{
				haveTo = true;
				return;
			}
		});

		if (!haveTo) {
			haveTo = false;
			jQuery.each($('input[name="sites[]"]'), function(i, site) {
				if(site.checked)
				{
					haveTo = true;
					return;
				}
			});
			if (!haveTo) {
				haveTo = false;
				jQuery.each($('input[name="roles[]"]'), function(i, role) {
					if(role.checked)
					{
						haveTo = true;
						return;
					}
				});

				if (!haveTo) {
					haveTo = false;
					jQuery.each($('input[name="userGroups[]"]'), function(i, userGroup) {
						if(userGroup.checked)
						{
							haveTo = true;
							return;
						}
					});

					if (!haveTo) {
						haveTo = false;
						if ($('input[name="users[]"]').length <= 0) {
							incomplete = true;
						}
					}
				}

			}
		}
		if (!$scope.corporatedocdata.name || !$scope.corporatedocdata.description) {incomplete=true;}
		if(incomplete)
		{
			$('#no-complete').modal('show');
		}
		else
		{
			$('#corporatedocs_edit').attr('disabled', 'disabled');
			$('#corporatedocs_edit').html("<i class='fa fa-refresh fa-spin fa-lg fa-fw'></i>");
			$('#editCorporatedocs').ajaxSubmit({
				url : '/'+FOLDERADD+'/corporatedocs/edit/'+$routeParams.id,
				success: function(data) {
					if (data != 'invalid') {
						corporatedocs_message.setMessage(data, false);
						$location.path('/corporatedocs');
						$scope.$apply()
					}
				}
			});
		}
	};
}
})();
