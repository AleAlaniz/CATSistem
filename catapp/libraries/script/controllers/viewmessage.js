
app.controller('Cviewmessage',['$scope','nav','$routeParams','$location','inbox_notification','reply_info', 'inboxService',
	function($scope,nav,$routeParams,$location,inbox_notification,reply_info, inboxService)
	{
		nav.setSelected('inbox');

		$scope.tousers=[];
		$scope.message={};

		getMessageById($routeParams.messageid);

		$scope.deleteValue=reply_info.getDeleteValue();
		$scope.replyValue=reply_info.getReplyValue();


		$scope.$on("$destroy",function()
		{
			inbox_notification.setMessage('');
			reply_info.setOptions(true,true);

		});


		function getMessageById(id)
		{
			$.ajax(
			{
				method:'POST',
				url:'/CATSistem/inbox/getmessagebyid',
				data:{'messageId':id},
				beforeSend:function()
				{
					$('#content').hide();	
					$('#loading').show();
				}
			})
			.done(function(res)
			{

				if(res == "error" || res == "fail" )
				{
					$location.path("#/");
					$scope.$apply();

				}
				else
				{
					inboxService.getUnread();
					$scope.$apply($scope.message=jQuery.parseJSON(res).message);
					$scope.$apply($scope.tousers=jQuery.parseJSON(res).to);

					$('#loading').hide();
					$('#content').show();
				}

			});
		}


		$scope.showAlert=function()
		{
			if($('.checkMessagesToRemove:checked').length > 0)
				$('#removeAlert').modal('show');
		}

		$scope.deleteMessage=function()
		{

			var messages=[$scope.message.inboxmessageId];

			$.ajax
			({
				method:'POST',
				url:'/CATSistem/inbox/deletemessagesinbox',
				data:{'messages':messages},
				beforeSend:function()
				{
					$('#delete').attr('class','fa fa-refresh fa-spin fa-lg text-danger');
				}
			})
			.done(function(res)
			{
				if(res != "success")
				{
					$location.path('#/');
					$scope.$apply();
					
				}
				else
				{

					inbox_notification.setMessage('message_move_to_trash');
					$location.path('/home/inbox');
					$scope.$apply();
				}

			});

		}



		$scope.reply=function()
		{
			console.log($scope.message);
			reply_info.setData($scope.message);

			$location.path('/home/inboxnew');

		}

	}]);


