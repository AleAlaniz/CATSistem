(function() {
	'use strict';
	
	angular
	.module('catnet', ['ngRoute', 'ngSanitize'])
	.config(appConfig);

	appConfig.$inject = ['$routeProvider'];
	function appConfig($routeProvider) {
		$routeProvider.
		when('/', {
			templateUrl: '/'+FOLDERADD+'/home/panel',
			controller: 'Cpanel'
		}).
		when('/home/wecat', {
			templateUrl: '/'+FOLDERADD+'/home/wecat',
			controller: 'Cwecat'
		})
		.otherwise({
			redirectTo: '/'
		});
	}
})();
