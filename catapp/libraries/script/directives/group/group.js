(function () {
    angular
    .module('catnet')
    .directive('group',group);
     function group () {
        return{
            restrict    : 'E',
            scope       : {
                division : '='
            },
            templateUrl : 'catapp/libraries/script/directives/group/group.html',
            controller  : function ($scope,$http) {
                $scope.objects = {};
                
                function searchObjects(table) {
                    $http({
                    method 	: 'GET',
                    url 	: "/"+FOLDERADD+"/"+table+"/get"+table
                    }).success(function(data) {
                        $scope.objects = data;
                        switch (table) {
                            case 'Roles':
                                $scope.title = 'Perfiles'
                                break;
                            
                            case 'Sections':
                            $scope.title = 'Divisiones'
                            break;

                            case 'Sites':
                            $scope.title = 'Sitios'
                            break;
                        }
                    });
                }

                searchObjects($scope.division);
            }
        }
    }
        
})();


