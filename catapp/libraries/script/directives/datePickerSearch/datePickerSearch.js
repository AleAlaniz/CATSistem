(function () {
    angular
    .module('catnet')
    .directive('datePickerSearch',datePickerSearch);
    function datePickerSearch() {
        return {
            restrict    : 'E',
            templateUrl : 'catapp/libraries/script/directives/datePickerSearch/datePickerSearch.html',
            controller  : function ($scope,$http) {
                $(document).ready(function(){
                    $('.input-daterange').datepicker({
                        todayBtn: "linked",
                        language: "es",
                        autoclose: true,
                        todayHighlight: true, 
                        format: "dd-mm"
                    });
                });
            }
        }
    }
})()