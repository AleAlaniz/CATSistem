(function () {
    angular
    .module('catnet')
    .directive('filters',filters);
    function filters() {
        return {
            restrict    : 'E',
            templateUrl : 'catapp/libraries/script/directives/filters/filters.html',
            controller  : function ($scope) {
                $scope.filters = [
                    {description: 'Grupo',                 type:'group'},
                    {description: 'Estado Civil',          type :'civil-state'},
                    {description: 'Adherido a Teaming',    type :'teaming'},
                    {description: 'Fecha de cumpleaños',   type :'birthDate'},
                    {description: 'Género',                type :'genre'},
                    {description: 'Turno',                 type :'turn'}
                ];
                $scope.addedFilters = [];
                var options = $scope.filters.length+1;

                $scope.addElement = function (selected) {
                    if($scope.addedFilters.indexOf(selected)== -1){
                        $scope.addedFilters.push(selected);
                    }
                }

                $scope.deleteElement = function (index) {
                    if(index != -1)
                    {
                        $scope.addedFilters.splice(index,1);
                        $scope.selectedFilter = 'nonsense';
                    }
                }
            }
        }
    }
})();