(function () {
    angular
    .module('catnet')
    .directive('civilStates',civilStates);
    function civilStates() {
        return{
            restrict    : 'E',
            scope       : {},
            templateUrl : 'catapp/libraries/script/directives/civilStates/civilStates.html',
            controller  : function ($scope,$http) {
                $scope.civilStates = {};

                function getCivilStates() {
                    $http({
                    method 	: 'GET',
                    url 	: '/'+FOLDERADD+'/CivilState',
                    }).success(function(data) {
                        $scope.civilStates = data;
                    });
                }

                getCivilStates();
            }
        }
    }
})();