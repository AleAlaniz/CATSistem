(function() {
    
        angular
        .module('catnet')
        .directive('searchusers', function() {
            return {
                restrict: 'E',
                transclude: true,
                controller: function($scope,$timeout,$http) {
    
                    $scope.finding_users        = false;
                    $scope.find_user            = "";
                    $scope.searchNumber         = 0;
                    $scope.searchActual         = 0;
                    $scope.userOptions          = [];
                    $scope.selectUsers = function (e) {
                        $('.select-input', e.currentTarget).focus();
                    };
    
                    $scope.searchUsers = function() {
                        if($scope.find_user != ''){
                            $scope.searchTimeout ? $timeout.cancel($scope.searchTimeout) : false;
                            $scope.searchTimeout = $timeout(goSearch, 250);
                        }
                        else{
                            $scope.userOptions   = [];
                            $scope.finding_users = false;
                        }
                    };
                    
                    function goSearch() {
                        $scope.finding_users        = true;
                        var thisSearch              = $scope.searchNumber;
                        $scope.searchNumber         = $scope.searchNumber+1;
                        
                        var id = "";
                        if(typeof $scope.searchUsersId !== 'undefined'){
                            id = $scope.searchUsersId;
                        }
                        $http
                        ({
                            method  : 'POST',
                            url     : $scope.searchUsersUrl,
                            data    : {
                                'id'  : id,  
                                'like': $scope.find_user
                            }
                        })
                        .success(function(data) {
                            if(data.status == 'ok' && thisSearch >= $scope.searchActual && $scope.find_user != '')
                            {   
                                $scope.searchActual = thisSearch;
                                $scope.addUserOptions($scope.users,data.data);
                                $scope.finding_users = false;
                            }
                            else if (thisSearch >= $scope.searchActual)
                            {
                                $scope.searchActual = thisSearch;
                                $scope.finding_users = false;
                                $scope.userOptions = [];    
                            }
                        });
                    }
    
                    $scope.addUserOptions   = function (usersInArray, users) {
                        $scope.userOptions      = [];
                        angular.forEach(users, function(user) {
                            var exist = false;
                            for (var i = usersInArray.length - 1; i >= 0; i--) {
                                if (usersInArray[i].userName == user.userName) {
                                    exist = true;
                                    break;
                                }
                            }
                            if (!exist) {$scope.userOptions.push(user);}
                        });
                    };
    
                    $scope.removeUser   = function (index) {
                        $scope.users.splice(index,1);
                        $scope.userOptions  = [];
                        $scope.find_user    = '';
                    }
    
                    $scope.adduser      = function (index) {
                        $scope.users.push($scope.userOptions[index]);
                        $scope.userOptions  = [];
                        $scope.find_user    = '';
                    }
                },
                templateUrl: 'catapp/libraries/script/directives/searchUsers/searchUsers.html'
            };
        });
    
    })();
    