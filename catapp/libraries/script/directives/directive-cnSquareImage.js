(function() {
	'use strict';
	angular
	.module('catnet')
	.directive('cnSquareImage'	, Dsquareimage);
	
	function Dsquareimage() {
		return {
			'restrict' 	: 'A',
			'scope'		: {
				'url'	: '@',
				'cls'	: '@',
			},
			'template'	: 	'<!--[if lte IE 8]>'
			+					'<div class="{{cls}}" style="filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src=\'{{url}}\',sizingMethod=\'scale\')"></div>'
			+				'<![endif]-->'
			+				'<!--[if !(IE)]><!-->'
			+					'<div class="{{cls}}" style="background-image:url({{url}})"></div>'
			+				'<![endif]-->'
		}
	}
})();