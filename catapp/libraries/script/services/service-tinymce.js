(function() {
	'use strict';
	
	angular
	.module('catnet')
	.service('tinyMCE', tinyMCE);

	function tinyMCE() {
		this.init = function(s) {
			tinymce.baseURL = "/"+APPFOLDERADD+"/libraries/tinymce";
			tinymce.suffix = '.min'; 
			tinymce.init({
				toolbar: "cut copy paste | alignleft aligncenter alignright alignjustify | bullist numlist  | outdent indent | forecolor backcolor fontsizeselect fontselect | bold italic underline | link unlink styleselect  image jbimages media | table",
				plugins: " image link media textcolor table jbimages ",
				language: 'es',
				selector: s,
				image_advtab: true,
				relative_urls: false,
				style_formats: [
				{
					title: 'Ajustar imagen',
					selector: 'img',
					styles: {
						'max-width': '100%', 
						'width' : '619px', 
						'height': '343px'
					}
				},
				{
					title: 'Ajustar imagen sin margen',
					selector: 'img',
					styles: {
						'max-width': 'calc(100% + 30px)', 
						'height': 'auto', 
						'margin': '-15px',
						'margin-left': '-15px',
						'margin-right': '-15px',
					}
				}
				],
				font_formats: 'Andale Mono=andale mono,monospace;Arial=arial,helvetica,sans-serif;Arial Black=arial black,sans-serif;Book Antiqua=book antiqua,palatino,serif;Calibri=calibri;Comic Sans MS=comic sans ms,sans-serif;Courier New=courier new,courier,monospace;Georgia=georgia,palatino,serif;Helvetica=helvetica,arial,sans-serif;Impact=impact,sans-serif;Symbol=symbolTahoma=tahoma,arial,helvetica,sans-serif;Terminal=terminal,monaco,monospace;Times New Roman=times new roman,times,serif;Trebuchet MS=trebuchet ms,geneva,sans-serif;Verdana=verdana,geneva,sans-serif;'
			});
		}
	}
})();
