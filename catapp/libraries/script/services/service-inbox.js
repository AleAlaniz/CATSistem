(function() {
	'use strict';

	angular
	.module 	('catnet')
	.factory 	('inboxService', inboxService);

	inboxService.$inject = ['$http', '$rootScope'];
	function inboxService($http, $rootScope) {
		var sv =  {
			unread 	: 0,
			getUnread 	: function() {
				$http
				.get('/'+FOLDERADD+'/inbox/getunread')
				.success(function(data) {
					if (data.status == 'ok') {
						sv.unread = data.unread;
						$rootScope.$broadcast('update_unread_inbox');
					}
				})
			}
		}
		return sv;
	}
}
)()