(function() {
	'use strict';
	angular
	.module('catnet')
	.factory('realTime', realTime)
	.run(initRealTime);
	
	realTime.$inject = ['$rootScope', 'chatsu', '$timeout', '$filter', 'chatsm', 'sugestboxService', 'newsletterService', 'pressService', 'teamingAgreeService',  '$window','sectionService'];
	function realTime($rootScope, chatsu, $timeout, $filter, chatsm, sugestboxService, newsletterService, pressService, teamingAgreeService,  $window,sectionService) {

		function blinkTab(newMessage) {

			var oldTitle = "CAT - Technologies",
			timeoutId,
			blink = function(){

				document.title = (document.title == newMessage)? 'CAT - Technologies' : newMessage;
			},
			clear = function() {
				clearInterval(timeoutId);
				document.title = oldTitle;
				$window.onmousemove = null;
       		 	timeoutId = null;
			};

			if (!timeoutId) {
 			  	timeoutId = setInterval(blink, 1000);
 			  	document.addEventListener('mousemove', clear);
 			}
		}

		var rt =  {
			suscribe : function(to, param) {
				var channel = rt.channels[to];
				if (!channel) {
					console.warn('Channel '+to+' not found');
				}
				else{
					if (param) {
						rt.subscriptions[to] = {value : true, param : param};
						rt.socket.emit('subscribe', to, param);
					}
					else{

						rt.subscriptions[to] = {value : true};
						rt.socket.emit('subscribe', to);
					}
				}
			},
			unsuscribe : function(to, param) {
				var channel = rt.channels[to];
				if (!channel) {
					console.warn('Channel not found');
				}
				else{
					rt.subscriptions[to] = {value : false};
					if (param) {
						rt.socket.emit('unsubscribe', to, param);	
					}
					else{
						rt.socket.emit('unsubscribe', to);
					}
				}
			},
			channels : {
				'chatsu' 			: true,
				'chatu' 			: true,
				'chatsm' 			: true,
				'chatm' 			: true,
				'unread_suggestbox' : true,
				'all_newsletter' 	: true,
				'section' 			: true,
				'site' 				: true,
				'role' 				: true,
				'user' 				: true,
				'campaign'			: true,
				'press' 			: true,
				'analitycs_online' 	: true,
				'teaming'			: true
			}, 
			socket 	: null,
			init 	: function() {
				rt.socket = io(POLLPATH,  {secure: true,transports:['polling', 'websocket'], query : {id:userData.userId}});

				rt.socket.on('connect', connect);
		
				if (permissions['teaming/pendingforms']){	
					rt.socket.on('new_teaming_form'	, new_teaming_form);
				}
				if(permissions['sections/index']){
					rt.socket.on('new_notice',new_notice)
				}

				if (permissions['suggestbox/view']) {rt.socket.on('new_suggestbox'		, new_suggestbox	);}
				if (permissions['newsletter/index']){rt.socket.on('new_newsletter'		, new_newsletter	);}
				if (permissions['press/index']) 	{rt.socket.on('new_press'			, new_press 		);}
				if (permissions['chat/index']) 		{
					rt.socket.on('chatu_message'	, chatu_message 	);
					rt.socket.on('new_chatu_message', new_chatu_message );
					rt.socket.on('chatu_deletedChat', chatu_deletedChat );
				}
				if (permissions['chat/multi']) 		{
					rt.socket.on('chatm_message'	, chatm_message 	);
					rt.socket.on('new_chatm_message', new_chatm_message );
					rt.socket.on('chatm_delete'		, chatm_delete );
					rt.socket.on('chatm_new_subject', chatm_new_subject);
				}
				if (permissions['analitycs/index']) {rt.socket.on('analitycs_online_change', analitycs_online_change);}

				rt.socket.on('new_tip'		, new_tip 	);
				rt.socket.on('delete_tip'	, delete_tip);

				function new_tip(tip) {

					function inIframe () {
						try {
							return window.self !== window.top;
						} catch (e) {
							return false;
						}
					}

					if (inIframe()) {
						window.top.angular.element('[ng-controller="Cindex"]').scope().addTip(tip);
					}
					else{
						$rootScope.$broadcast('new_tip', tip);
					}
				}

				function delete_tip(tip) {

					function inIframe () {
						try {
							return window.self !== window.top;
						} catch (e) {
							return false;
						}
					}

					if (inIframe()) {
						window.top.angular.element('[ng-controller="Cindex"]').scope().removeTip(tip);
					}
					else{
						$rootScope.$broadcast('delete_tip', tip);
					}
				}

				function new_suggestbox() {
					sugestboxService.getUnread();
				}

				function new_teaming_form() {
					teamingAgreeService.getUnread();
				}

				function new_notice() {
					sectionService.getUnread();
				}

				function accepted_suggestion(data) {
					
					$rootScope.$broadcast('accepted_suggestion');
				}

				function new_newsletter(time) {

					if (newsletterService.lastTime != time) {						
						newsletterService.lastTime = time;
						newsletterService.getUnread();
					}
				}

				function new_press() {
					pressService.getUnread();
				}

				function chatu_message(id, message) {
					$rootScope.$broadcast('chatu_message '+ id, message);
				}

				function chatu_deletedChat() {
					$rootScope.$broadcast('chatu_deletedChat');
				}

				function chatm_message(id, message) {
					$rootScope.$broadcast('chatm_message '+ id, message);
				}

				function chatm_delete(id) {
					$rootScope.$broadcast('chatm_delete', id);
				}

				function chatm_new_subject() {
					$rootScope.$broadcast('chatm_new_subject');
				}

				function new_chatu_message(id) {

					if (!(rt.subscriptions['chatu'] && rt.subscriptions['chatu'].value && rt.subscriptions['chatu'].param == id)) {

						//chatsu.getUnread();
						var alert = 'Ha recibido un nuevo mensaje';
						blinkTab(alert);
					}

					$rootScope.$broadcast('new_chatu_message');
				}

				function new_chatm_message(id) {
					if (!(rt.subscriptions['chatm'] && rt.subscriptions['chatm'].value && rt.subscriptions['chatm'].param == id)) {
						
						//chatsm.getUnread();
						var alert = 'Ha recibido un nuevo mensaje';
						blinkTab(alert);
					}
					$rootScope.$broadcast('new_chatm_message');
				}

				function analitycs_online_change(n) {
					$rootScope.$broadcast('analitycs_online_change', n);
				}

				function connect() {
					angular.forEach(rt.subscriptions, function(value, key) {
						if (value.value) {
							if (value.param) {
								rt.suscribe(key, value.param);
							}
							else
							{
								rt.suscribe(key);
							}
						}
					});
				}
			},
			subscriptions : {}
		}
		return rt;
	}
	initRealTime.$inject = ['realTime'];
	function initRealTime(realTime) {
		realTime.init();
	}

})();

