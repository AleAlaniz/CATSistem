(function() {
	'use strict';
	
	angular
	.module('catnet')
	.filter('HtmlSanitize', FilterHtmlSanitize);
	FilterHtmlSanitize.$inject = ['$sce'];
	function FilterHtmlSanitize($sce) {
		return function (value) {
			return $sce.trustAsHtml(value);
		};
	}
})();