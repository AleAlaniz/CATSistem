<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Activity extends CI_Controller {

    
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Activity_Model','Activity');
		$this->lang->load('activity');
		$this->lang->load('survey');
		$this->lang->load('notifications');
    }
    
    public function index()
    {
        if($this->Identity->Validate('activities/view')){
            $this->load->view('activities/index');
        }
        else{
            show_404();
        }
    }

    public function getactivities()
    {
        if($this->Identity->Validate('activities/view')){

            $data = new StdClass();
            $data->status = 'ok';
            $data->activities = $this->Activity->getActivities();

			echo escapeJsonString($data, FALSE);
        }
        else {
            echo '{"status":"invalid"';
        }
	}
	
	public function getactivity()
	{
		if($this->Identity->Validate('activities/manage')){
			$activityId = $this->uri->segment(3);
			$this->Activity->getActivity($activityId);
		}
		else{
			echo "error";
			return;
		}
	}

    public function getUsers()
    {
        $data = new StdClass();
		$like = $this->input->post('like');
		$activity = $this->input->post('activityId');
		
		if($this->Identity->Validate('activities/view'))
		{	
			$data->status 	= 'ok';
			$data->message 	= $this->Activity->getUsersInactivity($like,$activity);
		}
		else
		{
			$data->status = 'invalid';
		}

		echo json_encode($data);
    }

    public function report()
	{
		if ($this->Identity->Validate('activities/manage'))
		{
			$this->load->view('activities/report');
		}
		else
		{
			show_404();
		}
	}

	public function getactivityreport()
	{
		if ($this->Identity->Validate('activities/manage')) {
			$data 			= new StdClass();
			$data->activity = $this->Activity->GetactivityReport();
			$data->status 	= ($data->activity) ? 'ok' : 'invalid';
			echo escapeJsonString($data, FALSE);
		}
		else
		{
			echo '{"status":"invalid"}';
		}
    }

    public function exportActivityReport($value='')
    {
    	$data = new StdClass();
    	$data = $this->Activity->GetactivityReport();

    	$date = date('d-m-y');

    	$tableAnswered = '<table id="completesTable" class="table table-hover" role="dataTable">';
		$tableAnswered = $tableAnswered.'<thead><tr style="border: 1px solid;background-color:#ABCECE;">';
		$tableAnswered = $tableAnswered.'<th>'.$this->lang->line('teaming_form_id').'</th>';
    	$tableAnswered = $tableAnswered.'<th>'.$this->lang->line('survey_user_name').'</th>';
		$tableAnswered = $tableAnswered.'<th>'.$this->lang->line('survey_user_lastname').'</th>';
		$tableAnswered = $tableAnswered.'<th>'.$this->lang->line('administration_users_campaign').'</th>';
		$tableAnswered = $tableAnswered.'<th>'.$this->lang->line('subcampaign').'</th>';
		$tableAnswered = $tableAnswered.'<th>'.$this->lang->line('general_site').'</th>';
		$tableAnswered = $tableAnswered.'<th>'.$this->lang->line('survey_user_sex').'</th>';
		$tableAnswered = $tableAnswered.'<th>'.$this->lang->line('survey_user_birthday').'</th>';
		$tableAnswered = $tableAnswered.'<th>'.$this->lang->line('survey_user_date').'</th>';
		$tableAnswered = $tableAnswered.'<th>'.$this->lang->line('administration_users_turn').'</th>';
		$tableAnswered = $tableAnswered.'<th>'.$this->lang->line('activity_answer').'</th>';
    	$tableAnswered = $tableAnswered.'</tr></thead><tbody>';

    	foreach ($data->answered as $answer) {

			$tableAnswered = $tableAnswered.'<tr style="border: 1px solid">';
			$tableAnswered = $tableAnswered.'<td>'.$answer->dni.'</td>';
    		$tableAnswered = $tableAnswered.'<td>'.$answer->name.'</td>';
			$tableAnswered = $tableAnswered.'<td>'.$answer->lastName.'</td>';
			$tableAnswered = $tableAnswered.'<td>'.$answer->campaign.'</td>';
			$tableAnswered = $tableAnswered.'<td>'.$answer->subcampaign.'</td>';
			$tableAnswered = $tableAnswered.'<td>'.$answer->site.'</td>';
			$tableAnswered = $tableAnswered.'<td>'.$answer->gender.'</td>';
			$tableAnswered = $tableAnswered.'<td>'.$answer->birthDate.'</td>';
			$tableAnswered = $tableAnswered.'<td>'.$answer->date.'</td>';
			$tableAnswered = $tableAnswered.'<td>'.$answer->turn.'</td>';
			$tableAnswered = $tableAnswered.'<td>'.$answer->answer.'</td>';
			$tableAnswered = $tableAnswered.'</tr>';
    	}
    	
    	$tableAnswered = $tableAnswered.'</tbody></table>';

		$fileName = "Datos_de_la_actividad"."_".$date.".xls";
		$fileName = (filter_var($fileName,FILTER_SANITIZE_EMAIL));
		
		header("Content-type: application/vnd.ms-excel; name='excel'");
		header("Content-Disposition: attachment; filename = $fileName");
		header("Pragma: no-cache");
		header("Expires: 0");

		print_r("Personas que completaron la encuesta");
		print_r($tableAnswered);

    	

    }
    
    public function delete()
	{
		if ($this->Identity->Validate('activities/manage'))
		{
			$data 			= new StdClass();
			$data->status 	= 'ok';
			$data->message 	= $this->Activity->Delete();

			if ($data->message == NULL) {
				$data->status = 'invalid';
			}

			echo escapeJsonString($data, FALSE);
		}
		else
		{
			echo '{"status":"invalid"}';
		}
    }

    public function create()
	{	
		if ($this->Identity->Validate('activities/manage'))
		{
			$this->Activity->Create();
		}
		else
		{
			show_404();
		}
    }
    
    public function edit()
	{
		if ($this->Identity->Validate('activities/manage'))
		{
			$this->load->view('activities/edit');
		}
		else
		{
			show_404();
		}
	}

	public function editActivity()
	{
		if ($this->Identity->Validate('activities/manage'))
		{
			$activityId = $this->uri->segment(3);
			$this->Activity->Edit($activityId);
		}
		else
		{
			echo '{"status":"invalid"}';
		}
    }
    
    public function showPreview()
	{
		if ($this->Identity->Validate('activities/manage'))
		{
			$this->Activity->ViewActivityPreview();
		}
		else
		{
			show_404();
		}
	}

    public function activate()
	{

		if($this->Identity->Validate('activities/manage'))
		{
			$data 			= new StdClass();
			$data->status 	= 'ok';
			$data->message 	= $this->Activity->ActivateActivity();
			if ($data->message == NULL) {
				$data->status = 'invalid';
			}
			echo escapeJsonString($data, FALSE);
		}
		else
		{
			echo '{"status":"invalid"}';
		}
    }
    
    public function completeActivity()
	{
		if ($this->Identity->Validate('activities/view'))
		{
			$this->Activity->CompleteActivityNormal();
		}
		else
		{
			header('Location:/'.FOLDERADD);
		}
	}

	public function showManualCharge()
	{
		if ($this->Identity->Validate('activities/manage')) 
		{
			if($this->input->post('user_id'))

				$this->Activity->CompleteActivityManual($this->input->post('user_id'));
			else

				$this->Activity->Completeactivity(NULL,true);
		}
		else {
			show_404();
		}
	}

	public function changeOpinion(){
		if ($this->Identity->Validate('activities/view'))
		{
			$status = $this->Activity->Discharge();
			if($status == 'ok')
			{
				if($this->uri->segment(3) != NULL){
					header('Location:/'.FOLDERADD.'/activity/completeActivity/'.$this->uri->segment(3));	
				}
			}
			else
			{
				header('Location:/'.FOLDERADD);
			}
		}
		else
		{
			header('Location:/'.FOLDERADD);
		}
	}
    
    public function searchusers()
	{
		if ($this->Identity->Validate('activities/manage'))
		{
			$this->Survey->SearchUsers();
		}
		else
		{
			if ($this->input->post('like')) {

				echo 'invalid';
			}
			else
			{
				header('Location:/'.FOLDERADD);
			}
		}
    }

    public function isJoined()
    {
    	$activityId = json_decode($this->input->raw_input_stream,true);
    	$res 		= new StdClass();
    	$isjoined 	= null;
    	
    	if (isset($activityId))
    	{
    		$res->status = 'invalid';
    	}
    	else{

			$res->status  = 'ok';
    		$res->message = $this->Activity->isJoined($activityId);
    	}


	}
	
	public function updateDate()
	{
		if ($this->Identity->Validate('activities/manage'))
		{
			$req = json_decode(file_get_contents('php://input'), true);
			
			if(isset($req))
			{
				echo $this->Activity->UpdateDate($req["activityId"],$req['endDate']);				
			}
		}
		else
		{
			show_404();
		}
	}
    
}

/* End of file Activity.php */


?>