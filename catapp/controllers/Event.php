<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
    
    class Event extends CI_Controller {
    
        public function __construct() {
            parent::__construct();
            $this->load->model('Event_model','Event');
            $this->lang->load('notifications');
            $this->load->library('form_validation');
        }
        public function index()
        {
            if($this->Identity->Validate('notifications/manage'))
            {
                $this->load->view('notifications/event');
            }
            else
            {
                show_404();
            }
        }

        
        public function geteventsforuser()
        {
            $res = new StdClass();
            $res = $this->Event->getEventsForUser($this->session->UserId);
            echo json_encode($res);
        }
        
        public function getevents($notificationId)
        {
            
            if ($this->Identity->Validate('notifications/manage'))
            {
                $res = new StdClass();
                $res = $this->Event->getEvents($notificationId);
                return $res;
            }
            else
            {
                show_404();
            }
        }

        public function geteventbyid($eventId)
        {
            if ($this->Identity->Validate('notifications/manage'))
            {
                return $this->Event->getEventById($eventId);
            }
        }
        
        public function getusers()
        {
            if ($this->Identity->Validate('notifications/manage'))
            {
                $_POST = json_decode($this->input->raw_input_stream,true);
                $sections   = $_POST['sections'];
                $sites      = $_POST['sites'];
                $roles      = $_POST['roles'];
                $teaming    = $_POST['teaming'];
                $civilState = $_POST['civilState'];
                $genre      = $_POST['genre'];
                $turn       = $_POST['turn'];
                $startDate  = $_POST['startDate'];
                $endDate    = $_POST['endDate'];
                
                $res = new StdClass();
                $res = $this->Event->getUsers($sections,$sites,$roles,$teaming,$civilState,$genre,$turn,$startDate,$endDate);
                return $res;
            }
            else
            {
                show_404();
            }
        }

        public function create()
        {
            
            if($this->uri->segment(3))
            {   
                if ($this->Identity->Validate('notifications/manage')) 
                {
                    $this->form_validation->set_rules('title', 'title', 'required');
                    $this->form_validation->set_rules('startDate', 'startDate', 'required');
                    $this->form_validation->set_rules('endDate', 'endDate', 'required');
                    $this->form_validation->set_rules('users[]', 'hasUsers', 'required');
                    
                    if ($this->form_validation->run() == FALSE)
                    {	
                        $this->load->view('notifications/createEvent');
                    }
                    else
                    {
                        $notificationId = $this->uri->segment(3);
                        if(isset($notificationId))
                        {
                            $exist = $this->db->get_where('notifications',array('notificationId' => $notificationId))->row();
                            if(isset($exist))
                            {
                                $timestamp = time();
                
                                $config['upload_path']          = './catapp/_notifications_images/';
                                $config['allowed_types']        = 'gif|jpg|png';
                                $config['max_size']             = 50480;
                                $config['encrypt_name']         = true;

                                if (isset($_FILES['userfile']) && $_FILES['userfile'] != NULL) {
                                    $config['file_name'] = $timestamp.'-'. $_FILES['userfile']['name'];
                                }
                                $this->load->library('upload', $config);
                                
                                if (! $this->upload->do_upload('userfile'))
                                {
                                    $response = new StdClass();
                                    $response->status  = $this->upload->display_errors();
                                    $response->message = $this->lang->line('notification_completeall');
                                    echo escapeJsonString($response,false);
                                    return;
                                }
                                else
                                {
    
                                    $creation_date = time(date('Y-m-d'));
                                    $startDate = $this->Convert_to_Date($this->input->post('startDate'));
                                    $endDate   = $this->Convert_to_Date($this->input->post('endDate'));
    
                                    $objectInsert = array(
                                        'title'             => $this->input->post('title'),
                                        'image'             => $this->upload->data('file_name'),
                                        'creation_date'     => $creation_date,
                                        'startDate'         => $startDate,
                                        'endDate'           => $endDate,
                                        'userId'            => $this->session->UserId,
                                        'notificationId'    => $notificationId,
                                    );
                                    $users = $this->input->post('users[]');
                                    $users = explode(',',$users[0]);
                                    
                                    $dat = $this->input->post('startDate');
                                    $reminders[] = $this->Convert_to_Date($dat);
    
                                    $dat = $this->input->post('reminders');
                                    if($dat)
                                    {
                                        $dat = $this->Reminders_to_Date_Array($dat);
                                        $cDat = count($dat);
                                        for ($i=0; $i < $cDat; $i++) { 
                                            $reminders[] = $dat[$i];
                                        }
                                    }
                                    if(!isset($reminders) || !isset($startDate) || !isset($endDate))
                                    {
                                        $response->status  = "invalid";
                                        $response->message = $this->lang->line('invalid_date');
                                        echo escapeJsonString($response,false);
                                        return;
                                    }
                                    $this->Event->Create($objectInsert,$users,$reminders);
                                }
                            }
                            else
                            {
                                echo "invalid";
                            }
                        }
                        else
                        {
                            echo "invalid";
                        }
                    }
                }
                else
                {
                    show_404();
                }
            }
            else
            {
                show_404();
            }
            
        }

        public function edit($eventId)
        {
            if ($this->Identity->Validate('notifications/manage'))
            {
                $this->form_validation->set_rules('title', 'title', 'required');
                $this->form_validation->set_rules('startDate', 'startDate', 'required');
                $this->form_validation->set_rules('endDate', 'endDate', 'required');
                $this->form_validation->set_rules('users[]', 'hasUsers', 'required');

                
                if ($this->form_validation->run() == FALSE)
                {	
                    $this->load->view('notifications/editEvent');
                }
                else
                {
                    // $today = date('d/m/Y');
                    $today = new DateTime();
                    $today = $today->getTimestamp();
                    
                    $formStartDate = $this->input->post('startDate');
                    $formEndDate   = $this->input->post('endDate');
                    $eventId = $this->uri->segment(3);
                    if(isset($eventId))
                    {
                        $exist = $this->db->get_where('events',array('eventId' => $eventId))->row();
                        if(isset($exist))
                        {
                            $dbStartDate = date('d/m/Y',$exist->startDate);
                            $dbEndDate   = date('d/m/Y',$exist->endDate);
                            
                            if ($formStartDate != $dbStartDate) {
                                if ($today >= $exist->startDate) {
                                    $response = new StdClass();
                                    $response->status  = "invalid";
                                    $response->message = $this->lang->line('event_wrong_date');
                                    echo escapeJsonString($response,false);
                                    return;
                                }
                            }
                            if ($formEndDate != $dbEndDate) {
                                $fed = $this->Convert_to_Date($formEndDate);

                                if ($fed < $today) {
                                    $response = new StdClass();
                                    $response->status  = "invalid";
                                    $response->message = $this->lang->line('event_wrong_date');
                                    echo escapeJsonString($response,false);
                                    return;
                                }
                            }

                            $timestamp = time();
                            
                            $config['upload_path']          = './catapp/_notifications_images/';
                            $config['allowed_types']        = 'gif|jpg|png';
                            $config['max_size']             = 50480;
                            
                            if (isset($_FILES['userfile']) && $_FILES['userfile'] != NULL) {
                                $config['file_name'] = $timestamp.'-'. $_FILES['userfile']['name'];
                            }
                            $this->load->library('upload', $config);
                                                        
                            $startDate = $this->Convert_to_Date($this->input->post('startDate'));
                            $endDate   = $this->Convert_to_Date($this->input->post('endDate'));
                            $data = array(
                                'eventId'           => $eventId,
                                'title'             => $this->input->post('title'),
                                'startDate'         => $startDate,
                                'endDate'           => $endDate
                            );
                            if ($this->upload->do_upload('userfile'))
                            {
                                if ($exist->image != NULL) {
                                    unlink('./catapp/_notifications_images/'.$exist->image); 
                                }
                                $data['image'] = $this->upload->data('file_name');
                            }
                            $users = $this->input->post('users[]');
                            $users = explode(',',$users[0]);
                            $dat = $this->input->post('startDate');
                            $initReminder = $this->Convert_to_Date($dat);
                            $reminders    = array();
                            if(time() < $initReminder){
                                $reminders[] = $initReminder;
                            } 

                            $dat = $this->input->post('reminders');


                            if($dat)
                            {
                                $dat = $this->Reminders_to_Date_Array($dat);
                                $cDat = count($dat);
                                for ($i=0; $i < $cDat; $i++) { 
                                    $reminders[] = $dat[$i];
                                }
                            }
                            if((!isset($reminders) && !isset($initReminder)) || !isset($startDate) || !isset($endDate))
                            {
                                $response->status  = "invalid";
                                $response->message = $this->lang->line('invalid_date');
                                echo escapeJsonString($response,false);
                                return;
                            }
                            $this->Event->Edit($data,$users,$reminders);     
                        }
                        else
                        {
                            echo "invalid";
                        }
                    }
                    else
                    {
                        echo "invalid";
                    }
                }
            }
            else
            {
                show_404();
            }
        }

        public function getreportdata()
        {
            $_POST = json_decode($this->input->raw_input_stream,true);
            $res = new StdClass();
            $res = $this->Event->GetReportData($_POST['eventId'],$_POST['option']);
            return $res;
        }

        public function showEvent()
        {   
                $this->load->view("notifications/_notifications_header");
                $this->load->view('notifications/showEvent');
        }

        public function checkview()
        {   
            $_POST = json_decode($this->input->raw_input_stream,true);
            $res = new StdClass();
            $res->status ="invalid";
            $inserted = $this->Event->checkView($_POST["eventReminderId"]);
          
            if ($inserted > 0){
                $res->status ="ok";
            } 
            echo json_encode($res);
        }

        function Validate_Date($date){
            $values = explode('/', $date);
            if(count($values) == 3 && checkdate($values[1], $values[0], $values[2])){
                return true;
            }
            return false;
        }

        public function Convert_to_Date($date)
        {
            $fecha = NULL;
            if($this->Validate_Date($date))
            {
                $fecha = explode("/",$date);
                $fecha = "$fecha[0]-$fecha[1]-$fecha[2]";
                $fecha = strtotime($fecha); 
            }
            return $fecha;
        }

        public function Reminders_to_Date_Array($reminders)
        {
            $dates = explode(",",$reminders);
            $cDates = count($dates);
            for ($i=0; $i < $cDates; $i++) { 
                $dates[$i] = $this->Convert_to_Date($dates[$i]);
            }
            return $dates;
        }

        
    }
    /* End of file Event.php */
?>