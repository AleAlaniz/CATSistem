<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Corporatedocs extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Corporatedocs_model', 'Corporatedocs');
		$this->lang->load('corporatedocs');
	}

	public function index()
	{
		if ($this->Identity->Validate('corporatedocs/index'))
		{
			$this->load->view('corporatedocs/corporatedocs');
		}
		else
		{
			show_404();	
		}
	}

	public function getcorporatedocs()
	{
		if ($this->Identity->Validate('corporatedocs/index'))
		{
			$res 				= new StdClass();
			$res->status 		= 'ok';
			$res->categories 	= $this->Corporatedocs->GetCategories();
			$res->docs 			= array();

			$categoriesIndex = array();

			for ($i=0; $i < count($res->categories); $i++) { 
				$categoriesIndex['category_'.$res->categories[$i]->corporatedoccategoryId] = $i;
				$res->categories[$i]->docs = array();
			}


			$docs = $this->Corporatedocs->GetDocs();


			for ($i=0; $i < count($docs); $i++) { 
				if ($docs[$i]->corporatedoccategoryId == NULL || !array_key_exists('category_'.$docs[$i]->corporatedoccategoryId,$categoriesIndex)) {
					$res->docs[] = $docs[$i]; 
				}
				else{
                    $res->categories[$categoriesIndex['category_'.$docs[$i]->corporatedoccategoryId]]->docs[] = $docs[$i];
				}
			}

			echo escapeJsonString($res, FALSE);
		}
		else
		{
			echo '{"status":"invalid"}';
		}
	}
    //El metodo ahora tiene que traer en categories la lista que tiene null en el padre y las demas meterlas
    // en donde corresponda
    //1752021
	public function getcorporatedocsbis()
	{
		if ($this->Identity->Validate('corporatedocs/index'))
		{
			$res 				= new StdClass();
			$res->status 		= 'ok';
			$res->categories 	= array();
			$res->docs 			= array();

            $allCategories = $this->Corporatedocs->GetCategories();
			$categoriesIndex = array();
            //indexamos las categorias y creamos los arrays
			for ($i=0; $i < count($allCategories); $i++) {
				$categoriesIndex['category_'.$allCategories[$i]->corporatedoccategoryId] = $i;
				$allCategories[$i]->docs = array();
                $allCategories[$i]->categories = array();
			}
            //Por ultimo insertamos las categorias en su categoria padre o en la lista de catprincipales
            for($j=0; $j < count($allCategories);$j){
                if($allCategories[$j]->corporatedoccatfather == null){
                    $res->categories[] = $allCategories[$j];
                }else{
                    $allCategories[$categoriesIndex['category_'.$allCategories[$j]->corporatedoccatfather]]->categories =$allCategories[$j];
                }
            }
			//Una vez se obtuvo los documentos lo metemos en la categoria que corresponda
			$docs = $this->Corporatedocs->GetDocs();
			for ($i=0; $i < count($docs); $i++) {
				if ($docs[$i]->corporatedoccategoryId == NULL) {
					$res->docs[] = $docs[$i];
				}
				else{
					$allCategories[$categoriesIndex['category_'.$docs[$i]->corporatedoccategoryId]]->docs[] = $docs[$i];
				}
			}
			echo escapeJsonString($res, FALSE);
		}
		else
		{
			echo '{"status":"invalid"}';
		}
	}

	public function checkview()
	{
		if ($this->Identity->Validate('corporatedocs/index') && $this->uri->segment(3))
		{
			
			if ($this->Corporatedocs->GetNoViewDoc($this->uri->segment(3))) {

				if ($this->Corporatedocs->CheckView($this->uri->segment(3))) {
					echo '{"status":"ok"}';
				}
				else{
					echo '{"status":"fail"}';
				}
			}
			else {
				echo '{"status":"invalid"}';
			}
		}
		else
		{
			echo '{"status":"invalid"}';
		}
	}

	public function open()
	{
		if ($this->Identity->Validate('corporatedocs/index') && $this->uri->segment(3))
		{
			$doc = $this->Corporatedocs->GetViewDoc($this->uri->segment(3));
			if (isset($doc)) {
				$this->load->helper('download');
				force_download('./catapp/_corporate_docs/'.$doc->file, NULL);
			}
			else {
				show_404();
			}
		}
		else
		{
			show_404();
		}
	}

	public function getreport()
    {
        if ($this->Identity->Validate('corporatedocs/report') && $this->uri->segment(3) && $this->uri->segment(4)) {
            $res = new StdClass();
            $res->status = 'ok';
            $res->data = $this->Corporatedocs->GetReport($this->uri->segment(3), $this->uri->segment(4));
            echo escapeJsonString($res, FALSE);
        } else {
            echo '{"status":"invalid"}';
        }
    }

    //1752021
    public function getLinksForCategory(){
        if($this->Identity->Validate('corporatedocs/createcategory')){
            $this->Corporatedocs->getlinksForCategory();
        }else{
            echo "invalid";
        }
    }
    public function getLinksForDocument(){
        if($this->Identity->Validate('corporatedocs/create')){
            $this->Corporatedocs->getLinksForDocument();
        }else{
            echo "invalid";
        }
    }

    //1752021
	public function createcategory()
	{
		if ($this->Identity->Validate('corporatedocs/createcategory'))
		{
			$this->Corporatedocs->createCategory();
		}
		else
		{
			echo "invalid";
		}
	}

	public function deletecategory()
	{
		if ($this->Identity->Validate('corporatedocs/deletecategory'))
		{
			$this->Corporatedocs->DeleteCategory();
		}
		else
		{
			echo "invalid";
		}
	}

	public function editcategory()
	{
		if ($this->Identity->Validate('corporatedocs/editcategory'))
		{
			$this->Corporatedocs->EditCategory();
		}
		else
		{
			echo "invalid";
		}
	}
    //1752021 OLD METHOD
	public function getcategory()
	{
		if ($this->Identity->Validate('corporatedocs/editcategory'))
		{
			$this->Corporatedocs->GetCategory();
		}
		else
		{
			echo "invalid";
		}
	}

	public function create()
	{
		if ($this->Identity->Validate('corporatedocs/create'))
		{
			$this->Corporatedocs->Create();
		}
		else
		{
			show_404();
		}
	}

	public function delete()
	{
		if ($this->Identity->Validate('corporatedocs/delete'))
		{
			$this->Corporatedocs->Delete();
		}
		else
		{
			echo "invalid";
		}
	}

	public function download()
	{
		if ($this->Identity->Validate('corporatedocs/index'))
		{
			$this->Corporatedocs->Download();
		}
		else
		{
			show_404();
		}
	}

	public function searchusers()
	{
		if ($this->Identity->Validate('corporatedocs/create'))
		{
			$this->Corporatedocs->SearchUsers();
		}
		else
		{
			if ($this->input->post('like')) {
				echo 'invalid';
			}
			else
			{
				show_404();
			}
		}
	}


	public function edit()
	{
		if ($this->Identity->Validate('corporatedocs/edit'))
		{
			$this->Corporatedocs->Edit();
		}
		else
		{
			echo "invalid";
		}
	}
	
	public function getcorporatedocsbyid()
	{
		if ($this->Identity->Validate('corporatedocs/edit'))
		{
			$this->Corporatedocs->GetCorporateDocsById();
		}
		else
		{
			echo "invalid";
		}
	}

	public function getcategorybyid()
	{
		if ($this->Identity->Validate('corporatedocs/editcategory'))
		{
			$this->Corporatedocs->GetCategoryById();
		}
		else
		{
			echo "invalid";
		}
	}

	public function readdoc()
	{
		if($this->Identity->Validate('corporatedocs/index'))
			$this->Corporatedocs->ReadDoc();
		else
			show_404();
	}

}
