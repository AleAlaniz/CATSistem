<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Securityquestions extends CI_controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('security_question_model','SecurityQuestionsModel');
	}

	public function create()
	{
		if($this->Identity->Validate('users/security_questions/adm_create'))
		{
			$action = $this->input->post('create', TRUE); 
			if(isset($action))
			{
				$this->form_validation->set_rules('question', 'lang:question_question', 'required|min_length[5]|max_length[1000]');

				if($this->form_validation->run() == TRUE)
				{
					$data = array('question' => $this->input->post('question', TRUE));
					$response = $this->SecurityQuestionsModel->create($data);
					$this->session->set_flashdata('save_success', $response);
					
					if($response['state'] == 'success')
					{
						header('Location:/'.FOLDERADD.'/securityquestions/create');
						exit;
					}
				}
			}

			$this->load->view('_shared/_administrationlayoutheader');
			$this->load->view('securityquestions/adm_create');
			$this->load->view(PRELAYOUTFOOTER);
		}
		else
		{
			header('Location:/'.FOLDERADD);
		}
	}

	public function edit()
	{
		if($this->Identity->Validate('users/security_questions/adm_edit'))
		{
			$uriData = array('securityquestionId' => htmlspecialchars($this->uri->segment(3, NULL)));
			$this->form_validation->set_data($uriData);

			$this->form_validation->set_rules('securityquestionId', ' ', 'required|numeric|greater_than[0]');
			if($this->form_validation->run() == FALSE)
			{
				show_404();
				exit;
			}

			$data = array();
			$data['securityquestionId'] = $uriData['securityquestionId'];
			$action = $this->input->post('edit', TRUE);

			if(isset($action))
			{
				$this->form_validation->set_data($_POST);
				$this->form_validation->reset_validation();
				$this->form_validation->set_rules('question', 'lang:question_question', 'required|min_length[5]|max_length[1000]');
				
				if($this->form_validation->run() == TRUE)
				{
					$data['question'] = $this->input->post('question', TRUE);
					$response = $this->SecurityQuestionsModel->edit($data);

					$this->session->set_flashdata('save_success', $response);

					if($response['state'] == 'success')
					{
						header('Location:/'.FOLDERADD.'/securityquestions/edit/'.$data['securityquestionId']);
						exit;
					}
				}
			}

			$this->load->view('_shared/_administrationlayoutheader');
			$this->load->view('securityquestions/adm_edit', array('question' => $this->SecurityQuestionsModel->get_security_question_by_id($data['securityquestionId'])));
			$this->load->view(PRELAYOUTFOOTER);
		}
		else
		{
			header('Location:/'.FOLDERADD);
		}
	}

	public function delete()
	{
		if($this->Identity->Validate('users/security_questions/adm_delete'))
		{
			$action = $this->input->post('delete', TRUE); 
			if(isset($action))
			{
				$this->form_validation->set_rules('securityquestionId', 'lang:question_security_question_id', 'required|numeric|greater_than[0]');
				
				if($this->form_validation->run() == TRUE)
				{
					$data = array('securityquestionId' => $this->input->post('securityquestionId', TRUE));
					$response = $this->SecurityQuestionsModel->edit($data, TRUE);
					$this->session->set_flashdata('save_success', $response);
				}
			}

			$this->load->view('_shared/_administrationlayoutheader');
			$this->load->view('securityquestions/adm_index', array('questions' => $this->SecurityQuestionsModel->get_security_questions(TRUE)));
			$this->load->view(PRELAYOUTFOOTER);
		}
		else
		{
			header('Location:/'.FOLDERADD);
		}
	}

	public function valid_answer()
	{
		$error_icon = '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span> ';

		$this->form_validation->set_rules('user_name', 'lang:login_username', 'required|min_length[3]|alpha_numeric');
		$this->form_validation->set_rules('question_id', 'lang:question_question', 'required|numeric|greater_than[0]',
			array('greater_than' => $this->lang->line('question_select_valid_question'), 'numeric' => $this->lang->line('question_select_valid_question') ));

		$this->form_validation->set_rules('answer', 'lang:question_answer', 'required|min_length[1]|max_length[500]');

		if($this->form_validation->run() == FALSE)
		{
			echo validation_errors($error_icon,'<br>');
		}
		else
		{
			$data = array();
			$data['userName']  			= $this->input->post('user_name', TRUE);
			$data['securityquestionId'] = $this->input->post('question_id', TRUE);
			$data['answer'] 			= $this->input->post('answer', TRUE);

			if($this->SecurityQuestionsModel->valid_answer($data))
			{
				$this->load->model('User_model');

				if($this->User_model->ResetPasswordByQuestion($data['userName']))
				{
					$this->load->model('Identity_model');
					$_POST['UserName'] = $data['userName'];	
					$_POST['Password'] = $this->User_model->getDefaultPassword();
					
					echo($this->Identity_model->Autenticate());
				}
				else
				{
					echo($error_icon.$this->lang->line('question_error_restore_password'));
				}
			}
			else
			{
				echo($error_icon.$this->lang->line('question_incorrect_answer'));
			}
		}
	}

	public function my_security_questions()
	{
		if($this->Identity->Validate('users/security_questions'))
		{
			$action = $this->input->post('edit', TRUE);
			if(isset($action))
			{
				$data = array();

				for($i = 0; $i < 3; $i++)
				{
					$question_id 	= $this->input->post('question_'.$i, TRUE);
					$answer_id		= $this->input->post('answer_id_'.$i, TRUE);
					$answer			= $this->input->post('answer_'.$i, TRUE);

					$this->form_validation->reset_validation();

					$this->form_validation->set_rules('question_'.$i, 'lang:question_question', 'required|numeric|greater_than[-1]',
						array('numeric' 	=> $this->lang->line('question_select_valid_question'),
							'greater_than' 	=> $this->lang->line('question_select_valid_question')));

					if($this->form_validation->run() == FALSE)
					{
						$this->show_my_security_questions();
						return;
					}

					if($question_id > 0)
					{
						$_POST['data'] = $data;

						$this->form_validation->reset_validation();

						$this->form_validation->set_rules('question_'.$i, 'lang:question_question', 'callback_question_unique', array('question_unique' => $this->lang->line('question_selected_question_exist_already')));
						$this->form_validation->set_rules('answer_'.$i, 'lang:question_answer', 'required|min_length[1]|max_length[500]');

						if($this->form_validation->run() == FALSE)
						{
							$this->show_my_security_questions();
							return;
						}
					}
					else
					{
						$question_id 	= NULL;
						$answer 		= '';
					}

					array_push( $data, array('securityquestionId' => $question_id, 'answer' => $answer, 'answersecurityquestionId' => $answer_id));
				}

				$response = $this->SecurityQuestionsModel->save_security_questions($data);

				$this->session->set_flashdata('save_success', $response);
				header('Location:/'.FOLDERADD.'/securityquestions/my_security_questions');
				exit;
			}

			$this->show_my_security_questions();
		}
		else
		{
			header('Location:/'.FOLDERADD);
		}
	}

	public function question_unique($value)
	{
		$list  = $_POST['data'];
		$found = FALSE;

		for($i = 0; $i < count($list); $i++)
		{
			if($value == $list[$i]['securityquestionId'])
			{
				$found = TRUE;
				break;
			}
		}

		return !$found;
	}

	private function show_my_security_questions()
	{
		$this->load->view(PRELAYOUTHEADER, array('security_questions' => TRUE));
		$this->load->view('securityquestions/my_security_questions', array('my_questions' => $this->SecurityQuestionsModel->get_security_questions_by_user(), 'questions' => $this->SecurityQuestionsModel->get_security_questions()));
		$this->load->view(PRELAYOUTFOOTER);
	}

	public function get_security_questions()
	{
		$result = $this->SecurityQuestionsModel->get_security_questions();
		echo(json_encode($result));
	}

	public function config()
	{
		if($this->Identity->Validate('users/security_questions/adm_index'))
		{
			$this->load->view('_shared/_administrationlayoutheader');
			$this->load->view('securityquestions/adm_index', array('questions' => $this->SecurityQuestionsModel->get_security_questions(TRUE)));
			$this->load->view(PRELAYOUTFOOTER);
		}
		else
		{
			header('Location:/'.FOLDERADD);
		}

	}
}
