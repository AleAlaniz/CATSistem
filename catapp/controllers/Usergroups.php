<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * User Groups Controller Class
 * 
 * Controller User Groups Page
 *
 * @package 	CATNET
 * @subpackage 	Controllers
 * @category 	User Groups
 * @author 		Ivan
*/
class Usergroups extends CI_Controller {

	/**
	 * Class constructor
	 *
	 * Load Model To Use.
	 *
	 * @return	void
	 */
	public function __construct()
	{
		parent::__construct();

		//Load User Group Model
		$this->load->model('Usergroup_model', 'UserGroup');
	}

	// --------------------------------------------------------------------

	/**
	 * User Groups - Index Page
	 *
	 * @return	mixed
	 */
	public function index()
	{
		//Check Permissions
		if ($this->Identity->Validate('usergroups/index')) 
		{
			//Call Corresponding Method
			return $this->UserGroup->ViewUserGroups();
		}
		else
		{
			//Redirect To Home
			header('Location:/'.FOLDERADD);
		}
	}

	// --------------------------------------------------------------------

	/**
	 * User Groups - Create Page
	 *
	 * @return	mixed
	 */
	public function create()
	{
		//Check Permissions
		if ($this->Identity->Validate('usergroups/create')) 
		{
			//Call Corresponding Method
			return $this->UserGroup->CreateUserGroup();
		}
		else
		{
			//Redirect To Home
			header('Location:/'.FOLDERADD);
		}
	}

	// --------------------------------------------------------------------

	/**
	 * User Groups - Details Page
	 *
	 * @return	mixed
	 */
	public function details()
	{
		//Check Permissions
		if($this->Identity->Validate('usergroups/details'))
		{
			//Call Corresponding Method
			return $this->UserGroup->DetailsUserGroup();
		}
		else
		{
			//Redirect To Home
			header('Location:/'.FOLDERADD);
		}
	}

	// --------------------------------------------------------------------

	/**
	 * User Groups - Delete Page
	 *
	 * @return	mixed
	 */
	public function delete()
	{
		//Check Permissions
		if($this->Identity->Validate('usergroups/delete'))
		{
			//Call Corresponding Method
			return $this->UserGroup->DeleteUserGroup();
		}
		else
		{
			//Redirect To Home
			header('Location:/'.FOLDERADD);
		}
	}

	// --------------------------------------------------------------------

	/**
	 * User Groups - Edit Page
	 *
	 * @return	mixed
	 */
	public function edit()
	{
		//Check Permissions
		if($this->Identity->Validate('usergroups/edit'))
		{
			//Call Corresponding Method
			return $this->UserGroup->EditUserGroup();
		}
		else
		{
			//Redirect To Home
			header('Location:/'.FOLDERADD);
		}
	}
}
