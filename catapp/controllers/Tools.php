<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Tools Controller Class
 * 
 * Controller Tools Page
 *
 * @package 	CATNET
 * @subpackage 	Controllers
 * @category 	Tools
 * @author 		Ivan
*/
class Tools extends CI_Controller {

	/**
	 * Class constructor
	 *
	 * Load Model To Use.
	 *
	 * @return	void
	 */
	public function __construct()
	{
		parent::__construct();

		//Load Tools model
		$this->load->model('Tools_model', 'Tools');
		$this->lang->load('tools');
	}

	public function index()
	{
		if ($this->Identity->Validate('tools/index'))
		{
			$this->load->view('tools/tools');
		}
		else
		{
			show_404();
		}
	}

	public function notes()
	{
		if ($this->Identity->Validate('tools/notes'))
		{
			$this->load->view('tools/notes');
		}
		else
		{
			show_404();
		}
	}

	public function getnotes()
	{
		if ($this->Identity->Validate('tools/notes'))
		{
			$data 			= new StdClass();
			$data->status 	= 'ok';
			$data->notes 	= $this->Tools->GetNotes();
			echo escapeJsonString($data, FALSE);
		}
		else
		{
			echo '{"status":"invalid"}';
		}
	}

	// --------------------------------------------------------------------

	/**
	* Save Sticky Note.
	*
	* @return mixed
	*/
	public function savenote()
	{
		//Verify The Variable $_POST This Not Empty
		if (!empty($_POST)) {
			//Check permissions
			if ($this->Identity->Validate('tools/notes/edit'))
			{
				//Call Corresponding Method
				echo $this->Tools->SaveNote();
			}
			else
			{
				//Redirect To Home
				header('Location:/'.FOLDERADD.'/');
			}
		}
		else
		{
			//Redirect To Home
			header('Location:/'.FOLDERADD.'/');
		}
	}


	public function savenoteposition()
	{
		//Verify The Variable $_POST This Not Empty
		if (!empty($_POST)) {
			//Check permissions
			if ($this->Identity->Validate('tools/notes/edit'))
			{
				//Call Corresponding Method
				echo $this->Tools->SaveNotePosition();
			}
			else
			{
				//Redirect To Home
				header('Location:/'.FOLDERADD.'/');
			}
		}
		else
		{
			//Redirect To Home
			header('Location:/'.FOLDERADD.'/');
		}
	}

	public function savenotesize()
	{
		//Verify The Variable $_POST This Not Empty
		if (!empty($_POST)) {
			//Check permissions
			if ($this->Identity->Validate('tools/notes/edit'))
			{
				//Call Corresponding Method
				echo $this->Tools->SaveNoteSize();
			}
			else
			{
				//Redirect To Home
				header('Location:/'.FOLDERADD.'/');
			}
		}
		else
		{
			//Redirect To Home
			header('Location:/'.FOLDERADD.'/');
		}
	}

	public function deletenote()
	{
		//Verify The Variable $_POST This Not Empty
		if (!empty($_POST)) {
			//Check permissions
			if ($this->Identity->Validate('tools/notes/delete'))
			{
				//Call Corresponding Method
				echo $this->Tools->DeleteNote();
			}
			else
			{
				//Redirect To Home
				header('Location:/'.FOLDERADD.'/');
			}
		}
		else
		{
			//Redirect To Home
			header('Location:/'.FOLDERADD.'/');
		}
	}

	public function createnote()
	{
		//Check Permissions
		if ($this->Identity->Validate('tools/notes/create'))
		{
			//Call Corresponding Method
			$this->Tools->CreateNote();
		}
		else
		{
			//Redirect To Home
			header('Location:/'.FOLDERADD);
		}
	}

	public function analitycs()
	{
		if ($this->Identity->Validate('analitycs/index'))
		{
			$this->load->view('tools/analitycs', array('navBar' => $this->load->view('tools/_navbar_analitycs', array('title' => $this->lang->line('tools_analitycs_daily')), TRUE)));
		}
		else
		{
			show_404();
		}
	}

	public function getdiarydata()
	{
		$postdata = json_decode(file_get_contents('php://input'));
		if ($postdata && isset($postdata->date) && isset($postdata->sites)) {
			$date  = strtotime($postdata->date);
			$sites = $postdata->sites;
			if ($date != FALSE) {
				$data = new StdClass();
				$data->status = 'ok';
				$data->data = $this->Tools->GetDiaryData($date,$sites);
				echo escapeJsonString($data, FALSE);
			}
			else {
				echo '{"status":"invalid"}';
			}
		}
		else {
			echo '{"status":"invalid"}';
		}
	}

	public function analitycsusers()
	{
		if ($this->Identity->Validate('analitycs/index'))
		{
			$this->load->view('tools/analitycs_users', array('navBar' => $this->load->view('tools/_navbar_analitycs', array('title' => $this->lang->line('tools_analitycs_perusers')), TRUE)));
		}
		else
		{
			show_404();
		}
	}

	public function getusersdata()
	{
		$postdata = json_decode(file_get_contents('php://input'));
		if ($postdata && isset($postdata->dateStart)&& isset($postdata->dateFinish)&& isset($postdata->sites)) {
			$start 	= strtotime($postdata->dateStart);
			$finish = strtotime($postdata->dateFinish);
			$sites 	= $postdata->sites;
			if ($start != FALSE && $finish != FALSE) {
				$data = new StdClass();
				$data->status = 'ok';
				$data->data = $this->Tools->GetUsersData($start, $finish, $sites);
				echo escapeJsonString($data, FALSE);
			}
			else {
				echo '{"status":"invalid"}';
			}
		}
		else {
			echo '{"status":"invalid"}';
		}
	}


	public function analitycsonline()
	{
		if ($this->Identity->Validate('analitycs/index'))
		{
			$this->load->view('tools/analitycs_online', array('navBar' => $this->load->view('tools/_navbar_analitycs', array('title' => $this->lang->line('tools_analitycs_online')), TRUE)));
		}
		else
		{
			show_404();
		}
	}

	public function analitycslogin()
	{
		if ($this->Identity->Validate('analitycs/index'))
		{
			$data = array();
			$data['navBar'] =  $this->load->view('tools/_navbar_analitycs', array('title' => $this->lang->line('tools_analitycs_userslogin')), TRUE);
			$data['sites'] 	=  $this->Tools->GetSites();

			$this->load->view('tools/analitycs_login', $data);
		}
		else
		{
			show_404();
		}
	}

	public function getuserslogin()
	{
		$dateStart = $this->input->post('dateStart');
		$dateFinish = $this->input->post('dateFinish');
		$sites = $this->input->post('sites[]');
		$users = $this->input->post('users');
		
		if (isset($dateStart) && isset($dateFinish)) {
			if( (isset($sites) && is_array($sites) ) || (isset($users) && is_array($users)) ){
				$dateStart = strtotime($dateStart)+7200;
				$dateFinish = strtotime($dateFinish)+7200;

				$data = new StdClass();
				$data->status = 'ok';
				$data->users = array();
				$users = $this->Tools->GetUsersLogin($dateStart,$dateFinish,$sites,$users);
				$data->users = $users;

				echo escapeJsonString($data, FALSE);
			}
			
			else {
				echo '{"status":"invalid"}';
			}
		}
		else {
			echo '{"status":"invalid"}';
		}
	}

	public function exportToExcel()
	{
		if($this->Identity->Validate('tools/export')){
			$userId = $this->session->UserId;
			$result = $this->Tools->getData($userId);
			$this->ToExcel($result,'Datos'); 
		}
		else {
			show_404();
		}
	}

	public function ToExcel($data,$fileName)
	{
		$date = date('d-m-y');
		header('Content-Disposition: attachment; filename='.$fileName."_".$date.'.csv');
		header('Content-type: application/force-download');
		header("Content-type: text/csv");
        header('Content-Transfer-Encoding: binary');
        header('Pragma: public');
        print "\xEF\xBB\xBF"; // UTF-8 BOM

		$index = 0;
		$element = $data[0];
		$c_studies = 0;
		$c_hobbies = 0;

		$studiesColumns = $this->db->query("SHOW COLUMNS FROM `(BI) Estudios`")->result();
		$hobbiesColumns = $this->db->query("SHOW COLUMNS FROM `(BI) Hobbies`")->result();
		
		foreach ($data as $row) {//obtener la máxima cantidad de estudios y de hobbies
			// esto hay que comentar para utilizar la otra query
			if($c_studies < count($row->studies))
				$c_studies = count($row->studies);
			if($c_hobbies < count($row->hobbies))
				$c_hobbies = count($row->hobbies);
		}
		// echo '<table><tr>';
		foreach ($element as $key => $value) {//creacion de las columnas
			if($key != 'hobbies' && $key != 'studies'){
				$key = strtolower($key);
				$key = ucwords($key);
				// echo '<th style="border:1px #888;color: #FFF;background-color:rgb(25, 187, 216);">'.$key.'</th>';
				echo $key."; ";
			}
		}
		for ($i=0; $i < $c_studies; $i++) { //creacion de columnas adicionales 
			foreach ($studiesColumns as $column) {
				if($column->Field != 'userId')
					// echo '<th style="border:1px #888;color: #FFF;background-color:rgb(25, 187, 216);">'.$column->Field.'</th>';
					echo $column->Field." ;";
			}
		}
		for ($i=0; $i < $c_hobbies; $i++) { //creacion de columnas adicionales 
			foreach ($hobbiesColumns as $column) {
				if($column->Field == 'Hobby')
					// echo '<th style="border:1px #888;color: #FFF;background-color:rgb(25, 187, 216);">'.$column->Field.'</th>';
					echo $column->Field." ;";
			}
		}
		// echo '</tr>';

		foreach ($data as $row) {
			// echo '<tr>';
			echo "\n";
			$h = 0;
			foreach ($row as $key=>$cell) {
				if($key != 'studies' && $key != 'hobbies'){
					// echo '<td style="border:1px #888 solid;color:#555;">'.$cell.'</td>';
					echo $cell." ;";
				}
				else {
					$array = $cell;	// es un array de objetos puede ser de estudios o de hobbies
					if($key == 'studies'){//primero lleno todas las celdas con los estudios
						$rest = 4*($c_studies - count($array));
						for ($i=0; $i < count($array); $i++) { 
							$element = $array[$i];
							// echo "<td style='border:1px #888 solid;color:#555;'>$element->Nivel</td>";
							// echo "<td style='border:1px #888 solid;color:#555;'>$element->Estado</td>";
							// echo "<td style='border:1px #888 solid;color:#555;'>$element->Carrera</td>";
							// echo "<td style='border:1px #888 solid;color:#555;'>$element->Institución</td>";
							echo $element->Nivel. " ;";
							echo $element->Estado. " ;";
							echo $element->Carrera. " ;";
							echo $element->Institución. " ;";
						}
						for ($j=0; $j < $rest; $j++) { //lleno las celdas vacías
							// echo "<td style='border:1px #888 solid;color:#555;'></td>";
							echo ";";	
						}
					}
					if($key == 'hobbies'){// y ahora lleno las de los hobbies
						for ($i=0; $i < count($array); $i++) { 
							$element = $array[0];
							// echo "<td style='border:1px #888 solid;color:#555;'>$element->Hobby</td>";
							echo $element->Hobby. " ;";
						}
					}
					
				}
			}
			// echo '</tr>';
			// echo "\n";
		}

		// echo '</table>';
		
	}
	

	public function faq()
	{
		if ($this->Identity->Validate('tools/faq'))
		{
			$this->load->view('tools/faq');
		}
		else
		{
			show_404();
		}
	}

	public function getfaqs()
	{
		if ($this->Identity->Validate('tools/faq'))
		{
			echo json_encode($this->Tools->GetFaqs());
		}
		else
		{
			show_404();
		}
	}

	public function createquestion()
	{
		if ($this->Identity->Validate('tools/faq/manage'))
		{
			$request = json_decode(file_get_contents('php://input'), true);
			if(isset($request['title']))
			{
				if(isset($request['message']))
				{
					return $this->Tools->CreateQuestion($request['title'],$request['message']);
				}
				else {
					return "error";
				}
			}
			else{
				return "error";
			}
		}
		else {
			show_404();
		}
	}

	public function deletequestion()
	{
		if ($this->Identity->Validate('tools/faq/manage'))
		{
			$request = json_decode(file_get_contents('php://input'), true);
			if(isset($request['id']))
			{
				$this->db->select('faqId');
				$result = $this->db->get_where('faqs',array('faqId' => $request['id']))->row();
				if(isset($result))
				{
					return $this->Tools->DeleteQuestion($request['id']);
				}
			}
			else {
				show_404();
			}
		}
		show_404();
	}

	public function editquestion()
	{
		if ($this->Identity->Validate('tools/faq/manage'))
		{
			$request = json_decode(file_get_contents('php://input'), true);
			if(isset($request['qId']) && $request['title'] && $request['message'])
			{
				$this->db->select('faqId');
				$result = $this->db->get_where('faqs',array('faqId' => $request['qId']))->row();
				if(isset($result))
				{
					return $this->Tools->EditQuestion($request['qId'],$request['title'],$request['message']);
				}
			}
			else{
				show_404();
			}
		}
		else{
			show_404();
		}
	}

	public function to_excel_login_data()
	{
		$date = $_POST['date'];
		$sites = explode(",",$_POST['sites']);

		$this->db->select('name');
		$this->db->where_in('siteId', $sites);
		$sitesNames = $this->db->get('sites')->result();
		
		$fileName = "Datos-Login-de-";
		for ($i=0; $i < count($sitesNames); $i++) { 
			$fileName .= $sitesNames[$i]->name;
			$fileName .= "-";
		}
		$fileName .= $date.".xls";
		$fileName = (filter_var($fileName,FILTER_SANITIZE_EMAIL));
		
		header("Content-type: application/vnd.ms-excel; name='excel'");
		header("Content-Disposition: filename = $fileName");
		header("Pragma: no-cache");
		header("Expires: 0");
		
		echo $_POST['sendData'];
	}
}
