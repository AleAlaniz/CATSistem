
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SubCampaigns extends CI_Controller {

    
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Subcampaigns_model','SubCampaign');
        $this->lang->load('subcampaign');
    }
    

    public function index()
    {
        if($this->Identity->Validate('subcampaigns/index')){
            $data = new StdClass();
            $data->model = $this->SubCampaign->GetSubCampaigns();

            $this->load->view('_shared/_administrationlayoutheader');
            $this->load->view('subcampaigns/subcampaigns', $data);
            $this->load->view(PRELAYOUTFOOTER);
        }
        else{
            header('Location:/'.FOLDERADD);
        }
    }

    public function create()
    {
        if($this->Identity->Validate('subcampaigns/create')){
            
            $this->form_validation->set_rules('name', 'lang:subcampaign_name', 'trim|required|max_length[70]');
            
            
            if ($this->form_validation->run() == FALSE) {
                $this->load->view('_shared/_administrationlayoutheader');
                $this->load->view('subcampaigns/create');
                $this->load->view(PRELAYOUTFOOTER);
            } else {
                $scInsert = array('name' => $this->input->post('name',TRUE));
                $res = $this->SubCampaign->Create($scInsert);
                if($res == "success")
                {
                    $this->session->set_flashdata('flashMessage', 'create');
                    header('Location:/'.FOLDERADD.'/subcampaigns');
                }
            }
        }
        else {
            header('Location:/'.FOLDERADD);
        }
    }

    public function edit()
    {
        if($this->uri->segment(3)){
            if($this->Identity->Validate('subcampaigns/edit')){
                $scId = $this->uri->segment(3);
                $subCampaign = new StdClass();
                $subCampaign = $this->SubCampaign->GetSubCampaignById($scId);

                if(isset($subCampaign))
                {
                    $this->form_validation->set_rules('name', 'lang:subcampaign_name', 'trim|required|max_length[70]');
    
                    if ($this->form_validation->run() == FALSE) {
    
                        $subCampaign->navBar = $this->load->view('subcampaigns/_navbar', $subCampaign, TRUE);
                        $this->load->view('_shared/_administrationlayoutheader');
                        $this->load->view('subcampaigns/edit',$subCampaign);
                        $this->load->view(PRELAYOUTFOOTER);
    
                    } 
                    else {
                        $object = array(
                            'subCampaignId' => $scId, 
                            'name'          => $this->input->post('name',TRUE)
                        );

                        insert_audit_logs('subCampaigns','UPDATE',$subCampaign);

                        $res = $this->SubCampaign->Edit($object);
                        if($res == "success"){
                            $this->session->set_flashdata('flashMessage', 'edit');
                            header('Location:/'.FOLDERADD.'/subcampaigns');
                        }
                    }
                }
                else{
                    header('Location:/'.FOLDERADD);
                }
            }
            else{
                header('Location:/'.FOLDERADD);
            }
        }
        else{
            header('Location:/'.FOLDERADD);
        }
    }

    public function delete()
    {
        if($this->uri->segment(3)){
            if($this->Identity->Validate('subcampaigns/delete')){
                $scId = $this->uri->segment(3);
                $subCampaign = new StdClass();
                $subCampaign = $this->SubCampaign->GetSubCampaignById($scId);

                if(isset($subCampaign))
                {
                    if($this->input->post('subCampaignId') && $this->input->post('subCampaignId') == $scId){
                        
                        insert_audit_logs('subCampaigns','DELETE',$subCampaign);

                        $res = $this->SubCampaign->Delete($scId);
                        if($res == "success"){
                            $this->session->set_flashdata('flashMessage', 'delete');
                            header('Location:/'.FOLDERADD.'/subcampaigns');
                        }
                    } 
                    else {
                        $subCampaign->navBar = $this->load->view('subcampaigns/_navbar', $subCampaign, TRUE);
                        $this->load->view('_shared/_administrationlayoutheader');
                        $this->load->view('subcampaigns/delete',$subCampaign);
                        $this->load->view(PRELAYOUTFOOTER);
                    }

                }
            }
            else{
                header('Location:/'.FOLDERADD);
            }
        }
        else{
            header('Location:/'.FOLDERADD);
        }
    }

}

/* End of file SubCampaigns.php */
?>