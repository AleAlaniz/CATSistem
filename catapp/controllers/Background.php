<?php
    
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Administration Controller Class
 * 
 * Controller Administration Page.
 *
 * @package 	CATNET
 * @subpackage 	Controllers
 * @category 	Administration
 * @author 		Alejandro
*/
class Background extends CI_Controller {

    public function __construct()
	{
		parent::__construct();

		//Load Background Model
		$this->load->model('Background_model', 'Background');
    }
    
    public function index()
    {
        if ($this->Identity->Validate('background/manage')) {
            
            // $data = new StdClass();
            // $imageId = $this->input->get('select');
            // if ($imageId != NULL) {
            //     $this->db->select('userBackgroundId,name');
            //     $image = $this->db->get_where('userBackground', array('userBackgroundId' => $imageId))->row();
            //     if(isset($image)){
            //         $userEdit= array('background' => $image->name);
            //         $this->db->where('userId', $this->session->UserId);
            //         $this->db->update('userComplementaryData', $userEdit);
            //         $data->message = $this->lang->line('administration_users_selectbackground_success');
            //     }             
            // }
			//Load View
            $this->load->view('_shared/_administrationlayoutheader');
            // $this->load->view(PRELAYOUTHEADER);  
			$this->load->view('users/adminBackground');
			$this->load->view(PRELAYOUTFOOTER);
		}
		else
		{
			//Redirect To Home
			header('Location:/'.FOLDERADD);
		}
    }

    public function create()
    {
        if($this->Identity->Validate('background/manage')){
            $userId = $this->session->UserId;

            $timestamp = time();
            $config['upload_path']          = './catapp/_users_backgrounds/';
            $config['allowed_types']        = 'gif|jpg|png';
            $config['max_size']             = 50480;
            $config['encrypt_name']         = true;
            $config['remove_spaces']        = true;

            if (isset($_FILES['userfile']) && $_FILES['userfile'] != NULL) {
                $name = $_FILES['userfile']['name'];
                $config['file_name'] = $timestamp.'-'. $name;
            }
            $this->load->library('upload', $config);
            
            if (! $this->upload->do_upload('userfile'))
            {
                $response = new StdClass();
                $response->status  = $this->upload->display_errors();
                $response->message = $this->lang->line('notification_completeall');
                echo escapeJsonString($response,false);
                return;
            }
            else
            {
                $objectInsert = array(
                    'name'      => $this->upload->data('file_name'),
                    'userId'    => $userId
                );
            }

            echo $this->Background->Create($objectInsert);
        }
        else
        {
            show_404();
        }
        
    }

    public function get()
    {
        if($this->Identity->Validate('background/manage')){
            $backs = new StdClass();
            $backs = $this->Background->Get();
            echo json_encode($backs);
        }
        else{
            show_404();
        }

    }

    public function delete()
    {
        if($this->Identity->Validate('background/manage')){
            $request = json_decode(file_get_contents('php://input'), true);
            if(isset($request['deleteId']))
            {
                $this->db->select('userBackgroundId');
                $result = $this->db->get_where('userBackground',array('userBackgroundId' => $request['deleteId']))->row();
                if(isset($result))
                {
                    echo $this->Background->Delete($request['deleteId']);
                }
            }
            else
            {
                show_404();
            }
        }
        else
        {
            show_404();
        }
    }
    
}

/* End of file Backgrounds.php */
?>