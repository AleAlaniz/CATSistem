<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Suggest Box Controller Class
 * 
 * Controller Suggest Box Page
 *
 * @package 	CATNET
 * @subpackage 	Controllers
 * @category 	Suggest Box
 * @author 		Ivan
*/
class Suggestbox extends CI_Controller {

	/**
	 * Class constructor
	 *
	 * Load Model To Use.
	 *
	 * @return	void
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Suggestbox_model', 'Suggestbox');
		$this->lang->load('suggestbox');
	}

	public function index()
	{
		if ($this->Identity->Validate('suggestbox/index'))
		{
			$this->load->view('suggestbox/suggestbox');
		}
		else
		{
			show_404();
		}
	}

	public function postsuggest()
	{
		if ($this->Identity->Validate('suggestbox/index'))
		{
			echo $this->Suggestbox->PostSuggest();
		}
		else
		{
			echo '{"status":"invalid"}';
		}
	}

	public function getmysuggest()
	{
		if ($this->Identity->Validate('suggestbox/index'))
		{
			echo $this->Suggestbox->GetMySuggest();
		}
		else
		{
			echo '{"status":"invalid"}';
		}
	}

	public function getsections()
	{
		if ($this->Identity->Validate('suggestbox/view'))
		{
			echo $this->Suggestbox->GetSections();
		}
		else
		{
			echo '{"status":"invalid"}';
		}
	}


	public function view()
	{
		if ($this->Identity->Validate('suggestbox/view'))
		{
			$this->load->view('suggestbox/view');
		}
		else
		{
			show_404();
		}
	}

	public function getsuggests()
	{
		if ($this->Identity->Validate('suggestbox/view'))
		{
			echo $this->Suggestbox->GetSuggests();
		}
		else
		{
			echo '{"status":"invalid"}';
		}
	}

	public function take()
	{
		if ($this->Identity->Validate('suggestbox/take'))
		{
			echo $this->Suggestbox->Take();
		}
		else
		{
			echo '{"status":"invalid"}';
		}
	}

	public function getunread()
	{
		if ($this->Identity->Validate('suggestbox/view'))
		{
			$data = new StdClass();
			$data->status = 'ok';
			$data->unread = $this->Suggestbox->GetUnread();
			echo escapeJsonString($data, FALSE);
		}
		else
		{
			echo '{"status":"invalid"}';
		}
	}

}
