<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Press extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Press_model', 'Press');
		$this->lang->load('press');
	}

	public function index()
	{
		if ($this->Identity->Validate('press/index'))
		{
			$this->load->view('press/press');
		}
		else
		{
			show_404();
		}
	}

	public function getallclipping()
	{
		if ($this->Identity->Validate('press/clipping/view'))
		{
			$unread = $this->input->post('unread');
			echo $this->Press->GetAllClipping($unread);
		}
		else
		{
			echo '{"status":"invalid"}';
		}
	}

	public function getallnote()
	{
		if ($this->Identity->Validate('press/note/view'))
		{
			$unread = $this->input->post('unread');
			echo $this->Press->GetAllNote($unread);
		}
		else
		{
			echo '{"status":"invalid"}';
		}
	}

	public function create()
	{
		if ($this->Identity->Validate('press/create'))
		{
			$this->Press->Create();
		}
		else
		{
			show_404();
		}
	}

	public function delete()
	{
		if ($this->Identity->Validate('press/delete'))
		{
			echo $this->Press->Delete();
		}
		else
		{
			echo '{"status":"invalid"}';
		}
	}

	public function edit()
	{
		if ($this->Identity->Validate('press/edit'))
		{
			$this->Press->Edit();
		}
		else
		{
			show_404();
		}
	}

	public function getoldpressnotes()
	{
		
		if ($this->Identity->Validate('press/note/view'))
		{
			echo $this->Press->GetOldPressNotes();
		}
		else
		{
			echo '{"status":"invalid"}';
		}
		
	}

	public function createclipping()
	{
		if ($this->Identity->Validate('press/clipping/create'))
		{
			$this->Press->CreateClipping();
		}
		else
		{
			show_404();
		}
	}

	public function deleteclipping()
	{
		if ($this->Identity->Validate('press/clipping/delete'))
		{
			echo $this->Press->DeleteClipping();
		}
		else
		{
			echo '{"status":"invalid"}';
		}
	}

	public function editclipping()
	{
		if ($this->Identity->Validate('press/clipping/edit'))
		{
			$this->Press->EditClipping();
		}
		else
		{
			show_404();
		}
	}

	public function getclippingbyid()
	{
		if ($this->Identity->Validate('press/clipping/edit'))
		{
			echo $this->Press->GetClippingById();
		}
		else
		{
			echo '{"status":"invalid"}';
		}
	}

	public function getnotebyid()
	{
		if ($this->Identity->Validate('press/clipping/edit'))
		{
			echo $this->Press->GetNoteById();
		}
		else
		{
			echo '{"status":"invalid"}';
		}
	}

	public function getoldpressclipping()
	{
		
		if ($this->Identity->Validate('press/clipping/view'))
		{
			echo $this->Press->GetOldPressClipping();
		}
		else
		{
			echo '{"status":"invalid"}';
		}
	}

	public function getunread()
	{
		if ($this->Identity->Validate('press/index'))
		{
			$data = new StdClass();
			$data->status = 'ok';
			$data->unread = $this->Press->GetUnread();
			echo escapeJsonString($data, FALSE);
		}
		else
		{
			echo '{"status":"invalid"}';
		}
	}
}
