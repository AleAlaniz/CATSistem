<?php
    defined('BASEPATH') OR exit('No direct script access allowed');

    class Referred extends CI_Controller
    {
        public function __construct()
        {
            parent::__construct();
            $this->load->model('Referred_model','Referred');
        }
    
        public function index()
        {
            if($this->Identity->Validate('referred/index') && $this->session->sectionId == 6)
            {
                $this->load->view('referred/referred', array('header' =>  $this->load->view('referred/_referred_header')));
            }
            else
            {
                show_404();
            }
        }

        public function sendReferredV()
        {
            if($this->Identity->Validate('referred/index') && $this->session->sectionId == 6)
            {
                $this->load->view('referred/sendReferred',array('header'=> $this->load->view('referred/_referred_header')));
            }
        }

        public function sendReferred()
        {
            $data = json_decode($this->input->raw_input_stream,true);
            if(isset($data))
            {
                $this->form_validation->set_data($data);
            }

            $this->form_validation->set_rules('dni', 'DNI', 'required|numeric|min_length[8]|max_length[8]');
            $this->form_validation->set_rules('name', $this->lang->line('teaming_form_name'), 'required|max_length[50]|regex_match[/^[a-zñáéíóúüA-ZÑÁÉÍÓÚÜ ,.+]*$/u]');
            $this->form_validation->set_rules('numberPhone',$this->lang->line('teaming_form_celnumber') , 'required|numeric|max_length[15]');
            $this->form_validation->set_rules('mail', $this->lang->line('administration_users_email'), 'trim|valid_email');

            if ($this->form_validation->run() == TRUE) {
                $today = getdate();
                $objectInsert = array(
                    'dni'           => $data['dni'], 
                    'name'          => $data['name'],
                    'numberPhone'   => $data['numberPhone'],
                    'userId'        => $this->session->UserId,
                    'date'          => time(),
                );
                if(isset($data['mail']))
                {
                    $objectInsert['mail'] = $data['mail'];
                }
                echo $this->Referred->createReferred($objectInsert);
            }
            else {
                print_r(validation_errors());
            } 
        }

        public function acceptedReferred()
        {
            if($this->Identity->Validate('referred/index') && $this->session->sectionId == 6)
            {
                $this->load->view('referred/acceptedReferred');
            }
            else{
                show_404();
            }
        }

        public function referredList()
        {
            if($this->Identity->Validate('referred/admin') && $this->session->sectionId == 6)
            {
                $this->load->view('referred/referredList',array('header'=> $this->load->view('referred/_referred_header')));
            }
            else {
                show_404();
            }
        }

        public function getAllCandidates()
        {
            $res = new StdClass();

            $res->status  = 'ok';
            $res->message = $this->Referred->allReferredCandidates();

            if (!$res->message){

                $res->status  = 'empty';
            }

            echo json_encode($res);
        }

        public function editCandidate()
        {
            $res = new StdClass();
            $res->status   = 'invalid';
            $res->message  = 'no se recibio ningun candidato';

            $candidateToEdit = json_decode($this->input->raw_input_stream,true);

            if (isset($candidateToEdit)){

                $editStatus = new StdClass();
                $this->load->model('Referred_model', 'referred');
                $editStatus = $this->referred->editCandidate($candidateToEdit);

                if($editStatus->success == true){

                    $res->status   = 'ok';
                    $res->message   = $candidateToEdit;
                }
                else{
                    
                    $res->status   = 'validation_error';
                    $res->message   = $editStatus;	
                }

            }
            echo json_encode($res);
        }
    }
    
?>