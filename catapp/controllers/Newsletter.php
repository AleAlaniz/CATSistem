<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Newsletter extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Newsletter_model', 'Newsletter');
		$this->lang->load('newsletter');
	}

	public function index()
	{
		if ($this->Identity->Validate('newsletter/index'))
		{
			$this->load->view('newsletter/newsletter');
		}
		else
		{
			show_404();	
		}
	}

	public function getnewsletter()
	{
		if ($this->Identity->Validate('newsletter/index'))
		{
			$this->Newsletter->GetNewsletter();
		}
		else
		{
			echo "invalid";
		}
	}

	public function createcategory()
	{
		if ($this->Identity->Validate('newsletter/createcategory'))
		{
			$this->Newsletter->CreateCategory();
		}
		else
		{
			echo "invalid";
		}
	}

	public function deletecategory()
	{
		if ($this->Identity->Validate('newsletter/deletecategory'))
		{
			$this->Newsletter->DeleteCategory();
		}
		else
		{
			echo "invalid";
		}
	}

	public function editcategory()
	{
		if ($this->Identity->Validate('newsletter/editcategory'))
		{
			$this->Newsletter->EditCategory();
		}
		else
		{
			echo "invalid";
		}
	}

	public function getcategory()
	{
		if ($this->Identity->Validate('newsletter/editcategory'))
		{
			$this->Newsletter->GetCategory();
		}
		else
		{
			echo "invalid";
		}
	}

	public function create()
	{	
		$this->load->model('Teamingaudits_model', 'Teamingaudits');
		if ($this->Identity->Validate('newsletter/create'))
		{	
			
			$this->Newsletter->Create();
			if(isset($_POST["teamingData"])){
				$this->Teamingaudits->sendCommunications($_POST["teamingData"]);
			}
		}
		else
		{
			show_404();
		}
	}

	public function delete()
	{
		if ($this->Identity->Validate('newsletter/delete'))
		{
			$this->Newsletter->Delete();
		}
		else
		{
			echo "invalid";
		}
	}

	public function edit()
	{
		if ($this->Identity->Validate('newsletter/edit'))
		{
			$this->Newsletter->Edit();
		}
		else
		{
			echo "invalid";
		}
	}

	public function getnewsletterbyid()
	{
		if ($this->Identity->Validate('newsletter/edit'))
		{
			$this->Newsletter->GetNewsletterById();
		}
		else
		{
			echo "invalid";
		}
	}

	public function download()
	{
		if ($this->Identity->Validate('newsletter/index'))
		{
			$this->Newsletter->Download();
		}
		else
		{
			show_404();
		}
	}

	public function searchusers()
	{
		if ($this->Identity->Validate('newsletter/create'))
		{
			$this->Newsletter->SearchUsers();
		}
		else
		{
			if ($this->input->post('like')) {
				echo 'invalid';
			}
			else
			{
				show_404();
			}
		}
	}

	public function getunread()
	{
		if ($this->Identity->Validate('newsletter/index'))
		{
			$data = new StdClass();
			$data->status = 'ok';
			$data->unread = $this->Newsletter->GetUnread();
			echo escapeJsonString($data, FALSE);
		}
		else
		{
			echo '{"status":"invalid"}';
		}
	}

	public function getnewsletterreport($value='')
	{
		$res 			= new StdClass();
		$res->status 	= 'ok';
		$res->data 		= $this->Newsletter->GetNewsletterReport($this->uri->segment(3), $this->uri->segment(4));
		echo escapeJsonString($res, FALSE);
	}
}
