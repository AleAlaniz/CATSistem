<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Home Controller Class
 *
 * Controller Home Page
 *
 * @package 	CATNET
 * @subpackage 	Controllers
 * @category 	Home
 * @author 		Ivan
*/
class Home extends CI_Controller {


	public function __construct()
	{
		parent::__construct();
		$this->load->model('Home_model', 'Home');
		$this->lang->load('panel');
	}

	/**
	 * Home - Index Page
	 *
	 * @return	null
	 */
	public function index()
	{
		//Check Permissions
		if ($this->Identity->Validate('home/index'))
		{
			//Check Permissions
			if ($this->Identity->Validate('theme/create') && isset($_FILES['prev']))
			{
				//Create Base 64 Image For Preview
				$data = new StdClass();
				$data->fileContent = base64_encode(file_get_contents($_FILES['prev']['tmp_name']));
				$data->fileType = $_FILES['prev']['type'];

				//Load View (This Is Used To View Images Preview For Themes)
				$this->load->view('home/login', $data);
			}
			else
			{
				$this->load->view('_shared/_layout');
			}
		}
		else
		{
			//Load View
			$this->load->view('home/login');
		}
	}	

	/**
	 * Home - Panel page
	 *
	 * @return null
	 **/
	public function panel()
	{
		if ($this->Identity->Validate('home/index'))
		{
			$query = new StdClass();


			$sql = "SELECT name, lastName, birthDate FROM userPersonalData WHERE userId = ?";
			$userData = $this->db->query($sql, array($this->session->UserId))->row();
			
			$query->userId = $this->session->UserId;
			$birthdayTime = strtotime($userData->birthDate);
			$query->birthDate = date("d", $birthdayTime)."-".date("m", $birthdayTime);
			$query->panelMessage = $this->getPanel($userData);
			
			$this->load->view('home/panel', $query);

		}
		else
		{
			show_404();
		}
	}

	public function getPanel($userData)
	{
		if ($this->Identity->Validate('home/index'))
		{
			$query = new StdClass();
			
			$sql = "SELECT * FROM panelMessages WHERE sectionId = ?";
			$panelMessage = $this->db->query($sql,$this->session->sectionId)->last_row();

			if (isset($panelMessage->message) && $panelMessage->message != NULL) {

				$vars = array(
					'{{name}}'				=>	array(encodeQuery($userData->name)),
					'{{lastname}}'			=>	array(encodeQuery($userData->lastName)),
					);

				foreach ($vars as $key => $val)
				{
					$panelMessage->message = preg_replace('/('.$key.')/',$vars[$key][0],' '.$panelMessage->message.' ');
				}
			}
			
			return $panelMessage;
		}
	}

	public function getWelcome()
	{
		if ($this->Identity->Validate('home/index'))
		{
			$query = new StdClass();
			
			$sql = "SELECT message FROM welcomeMessages WHERE sectionId = ?";
			$welcomeMessage = $this->db->query($sql, $this->session->sectionId)->row();
			echo json_encode($welcomeMessage);
		}
	}

	public function getallalerts()
	{
		if ($this->Identity->Validate('home/alert')) 
		{
			echo $this->Home->GetAllAlerts();
		}
		else
		{
			echo '{"status":"invalid"}';
		}
	}

	public function deletealert(){
		if($this->Identity->Validate('administration/alert')){
			if ($this->uri->segment(3)) {
				echo $this->Home->DeleteAlertById($this->uri->segment(3));
			}
			else
			{
				echo '{"status":"invalid"}';
			}
		}
		else
		{
			echo '{"status":"invalid"}';
		}
	}


	// --------------------------------------------------------------------

	/**
	* Home - We Are Cat Page.
	*
	* @return mixed
	*/
	public function wecat()
	{
		//Check Permissions
		if ($this->Identity->Validate('home/wecat'))
		{
			//Load View
			$this->load->view('home/wecat');
		}
		else
		{
			//Redirect To Home
			//header('Location:/'.FOLDERADD);
			show_404();
		}
	}

	// --------------------------------------------------------------------

	/**
	* Home - Autenticate Page.
	*
	* @return mixed
	*/
	public function autenticate()
	{
		//set_time_limit(0);
		//sleep(46800);
		//Check Permissions
		if ($this->Identity->Validate('home/autenticate'))
		{
			//Check Inputs
			if ($this->input->post('UserName') && $this->input->post('Password') )
			{
				//Write 'success'
				echo 'success';
			}
			else
			{
				//Redirect To Home
				header('Location:/'.FOLDERADD);
			}
		}
		else
		{
			//Call Corresponding Method
			$_SESSION['auth_status'] = $this->Identity->Autenticate();
			$this->session->mark_as_flash('auth_status');

			if (strcmp('success', $_SESSION['auth_status']) === 0){
				header('Location:/'.FOLDERADD);
			}

			$this->load->view('home/login');
		}
	}

	// --------------------------------------------------------------------

	/**
	* Home - Logout Page.
	*
	* @return mixed
	*/
	public function logout()
	{
		//Check Permissions
		if ($this->Identity->Validate('home/logout'))
		{
			//Call Corresponding Method
			return $this->Identity->Logout();
		}
		else
		{
			//Redirect To Home
			header('Location:/'.FOLDERADD);
		}
	}



	/**
	* Home - Find Users Page.
	*
	* @return mixed
	*/
	public function findusers()
	{
		//Verify The Variable $_POST This Not Empty
		if (!empty($_POST)) {
			//Check Permissions
			if ($this->Identity->Validate('home/inbox/findusers'))
			{
				//Load model
				$this->load->model('User_model', 'User');

				//Call Corresponding Method
				$this->User->FindUsers();
			}
			else
			{
				//Write Invalid
				echo "invalid";
			}
		}
		else
		{
			//Redirect To Home
			header('Location:/'.FOLDERADD.'/');
		}
	}

	public function getunread()
	{
		if ($this->session->Loged) {

			$data = new StdClass();
			$data->status = 'ok';

			if ($this->Identity->Validate('chat/index'))
			{
				$data->ChatU = $this->Home->GetUnreadChatU();
			}

			if ($this->Identity->Validate('chat/multi'))
			{
				$data->ChatM = $this->Home->GetUnreadChatM();
			}

			if ($this->Identity->Validate('teaming/pendingforms'))
			{
				$data->Forms = $this->Home->GetUnreadForms();
			}

			if ($this->Identity->Validate('newsletter/index'))
			{
				$data->Newsletter = $this->Home->GetUnreadNewsletter();
			}

			if ($this->Identity->Validate('suggestbox/view'))
			{
				$data->Suggestbox = $this->Home->GetUnreadSuggestbox();
			}

			if ($this->Identity->Validate('press/index'))
			{
				$data->Press = $this->Home->GetUnreadPress();
			}
			if($this->Identity->Validate('sections/index')){
				$data->Section = $this->Home->GetUnreadSections();
			}
			
			echo escapeJsonString($data, FALSE);
		}
		else
		{

			echo '{"status":"invalid"}';
		}
	}
}
