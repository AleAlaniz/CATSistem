<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Surveys extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('Survey_model', 'Survey');
		$this->lang->load('survey');
	}

	public function index()
	{
		if ($this->Identity->Validate('surveys/view'))
		{
			$this->load->view('surveys/index');
		}
		else
		{
			show_404();
		}
	}

	public function getsurveys()
	{
		if ($this->Identity->Validate('surveys/view'))
		{
			$data 			= new StdClass();
			$data->status 	= 'ok';
			$data->surveys 	= $this->Survey->getSurveys();
			echo escapeJsonString($data, FALSE);
		}
		else
		{
			echo '{"status":"invalid"}';
		}
	}

	public function getUsers()
	{
		$data = new StdClass();
		$like = $this->input->post('like');
		$survey = $this->input->post('surveyId');
		
		
		if($this->Identity->Validate('surveys/view'))
		{	
			$data->status 	= 'ok';
			$data->message 	= $this->Survey->getUsersInSurvey($like,$survey);
		}
		else
		{
			$data->status = 'invalid';
		}

		echo json_encode($data);
	}

	public function report()
	{
		if ($this->Identity->Validate('surveys/manage'))
		{
			$this->load->view('surveys/report');
		}
		else
		{
			show_404();
		}
	}

	public function getsurveyreport()
	{
		if ($this->Identity->Validate('surveys/manage')) {
			$data 			= new StdClass();
			$data->survey 	= $this->Survey->GetSurveyReport();
			$data->status 	= ($data->survey) ? 'ok' : 'invalid';
			echo escapeJsonString($data, FALSE);
		}
		else
		{
			echo '{"status":"invalid"}';
		}
	}


	public function details()
	{
		if ($this->Identity->Validate('surveys/manage'))
		{
			$this->load->view('surveys/preview');
		}
		else
		{
			show_404();
		}
	}

	public function getsurveydetails()
	{
		if ($this->Identity->Validate('surveys/manage')) {
			$data 			= new StdClass();
			$data->survey 	= $this->Survey->GetSurveyDetails();
			$data->status 	= ($data->survey) ? 'ok' : 'invalid';
			echo escapeJsonString($data, FALSE);
		}
		else
		{
			echo '{"status":"invalid"}';
		}
	}

	public function delete()
	{
		if ($this->Identity->Validate('surveys/delete'))
		{
			$data 			= new StdClass();
			$data->status 	= 'ok';
			$data->message 	= $this->Survey->Delete();
			if ($data->message == NULL) {
				$data->status = 'invalid';
			}
			echo escapeJsonString($data, FALSE);
		}
		else
		{
			echo '{"status":"invalid"}';
		}
	}

	public function create()
	{	
		$this->load->model('Teamingaudits_model', 'Teamingaudits');
		if ($this->Identity->Validate('surveys/create'))
		{
			$this->Survey->Create();
			if(isset($_POST["teamingData"])){
				$this->Teamingaudits->sendCommunications($_POST["teamingData"]);
			}
		}
		else
		{
			show_404();
		}
	}

	public function edit()
	{
		if ($this->Identity->Validate('surveys/edit'))
		{

			$this->load->view('surveys/edit');
		}
		else
		{
			show_404();
		}
	}

	public function editSurvey()
	{
		if ($this->Identity->Validate('surveys/edit'))
		{

			$this->Survey->Edit();
		}
		else
		{
			echo '{"status":"invalid"}';
		}
	}

	

	public function showPreview()
	{
		if ($this->Identity->Validate('surveys/manage'))
		{
			$this->Survey->ViewSurveyPreview();
		}
		else
		{
			show_404();
		}
	}

	public function getSurveyData()
	{
		if ($this->Identity->Validate('surveys/edit'))
		{

			$this->Survey->GetDataById();
		}
		else
		{
			echo "error";
		}
	}

	public function activate()
	{

		if($this->Identity->Validate('surveys/manage'))
		{
			$data 			= new StdClass();
			$data->status 	= 'ok';
			$data->message 	= $this->Survey->ActivateSurvey();
			if ($data->message == NULL) {
				$data->status = 'invalid';
			}
			echo escapeJsonString($data, FALSE);
		}
		else
		{
			echo '{"status":"invalid"}';
		}
	}


	public function myanswers()
	{
		if ($this->Identity->Validate('surveys/view'))
		{
			$this->load->view('surveys/myanswers');
		}
		else
		{
			show_404();
		}
	}

	public function getsurveyanswers()
	{
		if ($this->Identity->Validate('surveys/view'))
		{
			$data 			= new StdClass();
			$data->survey 	= $this->Survey->GetSurveyAnswers();
			$data->status 	= ($data->survey) ? 'ok' : 'invalid';
			echo escapeJsonString($data, FALSE);
		}
		else
		{
			echo '{"status":"invalid"}';
		}
	}


	public function completesurvey()
	{
		if ($this->Identity->Validate('surveys/view'))
		{
			$this->Survey->CompleteSurveyNormal();
		}
		else
		{
			header('Location:/'.FOLDERADD);
		}
	}

	public function showManualCharge()
	{
		if ($this->Identity->Validate('surveys/manage')) 
		{
			if($this->input->post('user_id'))
				$this->Survey->CompleteSurveyManual($this->input->post('user_id'));
			else
				$this->Survey->CompleteSurvey(NULL,true);
		}
		else {
			show_404();
		}
	}

	public function searchusers()
	{
		if ($this->Identity->Validate('surveys/manage'))
		{
			$this->Survey->SearchUsers();
		}
		else
		{
			if ($this->input->post('like')) {
				echo 'invalid';
			}
			else
			{
				header('Location:/'.FOLDERADD);
			}
		}
	}
	
	public function exportReportSurvey()
	{
		if ($this->Identity->Validate('surveys/manage'))
		{
			$date = date('d-m-y');
			$name = $_POST['surveyName'];
			
			$fileName = "Datos_de_la_encuesta_$name"."_".$date.".xls";
			$fileName = (filter_var($fileName,FILTER_SANITIZE_EMAIL));
			
			header("Content-type: application/vnd.ms-excel; name='excel'");
			header("Content-Disposition: attachment; filename = $fileName");
			header("Pragma: no-cache");
			header("Expires: 0");

			print_r("Personas que completaron la encuesta");
			print_r($_POST['completesSend']);
			print_r("<table><tr></tr></table>");
			print_r("Personas que entraron y no completaron la encuesta");
			print_r($_POST['incompletesSend']);
		}
	}

	public function updateDate()
	{
		if ($this->Identity->Validate('surveys/manage'))
		{
			$req = json_decode(file_get_contents('php://input'), true);
			if(isset($req))
			{
				echo $this->Survey->UpdateDate($req["surveyId"],$req['endDate']);				
			}
		}
		else
		{
			show_404();
		}
	}

	public function collectData()
	{
		if ($this->Identity->Validate('surveys/manage')) 
	  	{
			$this->Survey->exportSurveyResults($_POST['surveyId']);
	  	}	
	}
}
