<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CivilState extends CI_Controller {

    
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Civilstate_model','CivilState');
    }
    
    public function index()
    {
        echo json_encode($this->CivilState->getCivilStates());
    }

}

/* End of file Controllername.php */

?>