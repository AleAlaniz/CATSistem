<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Sites Controller Class
 * 
 * Controller Sites Page
 *
 * @package 	CATNET
 * @subpackage 	Controllers
 * @category 	Sites
 * @author 		Ivan
*/
class Sites extends CI_Controller {

	/**
	 * Class constructor
	 *
	 * Load Model To Use.
	 *
	 * @return	void
	 */
	public function __construct()
	{
		parent::__construct();

		//Load Site Model
		$this->load->model('Site_model', 'Site');
	}

	// --------------------------------------------------------------------

	/**
	 * Sites - Index Page
	 *
	 * @return	mixed
	 */
	public function index()
	{
		//Check Permissions
		if ($this->Identity->Validate('sites/index')) 
		{
			//Call Coressponding Method
			$this->Site->GetSites();
		}
		else
		{
			//Redirect To Home
			header('Location:/'.FOLDERADD);
		}
	}

	public function getSites()
	{
		$data = $this->Site->GetSitesForComponent();
		echo json_encode($data);		
	}
	// --------------------------------------------------------------------

	/**
	 * Sites - Create Page
	 *
	 * @return	mixed
	 */
	public function create()
	{
		//Check Permissions
		if ($this->Identity->Validate('sites/create')) 
		{
			//Call Corresponding Method
			return $this->Site->Create();
		}
		else
		{
			//Redirect To Home
			header('Location:/'.FOLDERADD);
		}
	}

	public function delete()
	{
		if($this->Identity->Validate('sites/delete'))
		{
			return $this->Site->Delete();
		}
		else
		{
			header('Location:/'.FOLDERADD);
		}
	}

	public function details()
	{
		if($this->Identity->Validate('sites/details'))
		{
			return $this->Site->Details();
		}
		else
		{
			header('Location:/'.FOLDERADD);
		}
	}

	public function edit()
	{
		if($this->Identity->Validate('sites/edit')){
			return $this->Site->Edit();
		}
		else
		{
			header('Location:/'.FOLDERADD);
		}
	}

}