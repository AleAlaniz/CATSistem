<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Genders Controller Class
 * 
 * Controller Genders Page
 *
 * @package 	CATNET
 * @subpackage 	Controllers
 * @category 	Campaigns
 * @author 		Lucas
*/
class Campaigns extends CI_Controller {

	/**
	 * Class constructor
	 *
	 * Load Model To Use.
	 *
	 * @return	void
	 */
	public function __construct()
	{
		parent::__construct();

		//Load Site Model
		$this->load->model('Campaigns_model', 'Campaign');
	}

	// --------------------------------------------------------------------

	/**
	 * Campaigns - Index Page
	 *
	 * @return	mixed
	 */
	public function index()
	{
		//Check Permissions
		if ($this->Identity->Validate('campaigns/index')) 
		{
			//Call Coressponding Method
			$this->Campaign->GetCampaigns();
		}
		else
		{
			//Redirect To Home
			header('Location:/'.FOLDERADD);
		}
	}

	// --------------------------------------------------------------------

	/**
	 * Campaigns - Create Page
	 *
	 * @return	mixed
	 */
	public function create()
	{
		//Check Permissions
		if ($this->Identity->Validate('campaigns/create')) 
		{
			//Call Corresponding Method
			return $this->Campaign->Create();
		}
		else
		{
			//Redirect To Home
			header('Location:/'.FOLDERADD);
		}
	}

	public function delete()
	{
		if($this->Identity->Validate('campaigns/delete'))
		{
			return $this->Campaign->Delete();
		}
		else
		{
			header('Location:/'.FOLDERADD);
		}
	}

/*	public function details()
	{
		if($this->Identity->Validate('sites/details'))
		{
			return $this->Site->Details();
		}
		else
		{
			header('Location:/'.FOLDERADD);
		}
	}*/

	public function edit()
	{
		if($this->Identity->Validate('campaigns/edit'))
		{
			return $this->Campaign->Edit();
		}
		else
		{
			header('Location:/'.FOLDERADD);
		}
	}

}