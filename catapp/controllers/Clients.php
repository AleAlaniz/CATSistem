<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Clients extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		//Load Site Model
		$this->load->model('Clients_model', 'Client');
	}

	public function index()
	{
		//Check Permissions
		if ($this->Identity->Validate('clients/index')) 
		{
			//Call Coressponding Method
			$this->Client->Getclients();
		}
		else
		{
			//Redirect To Home
			header('Location:/'.FOLDERADD);
		}
	}

	
	public function create()
	{
		//Check Permissions
		if ($this->Identity->Validate('clients/create')) 
		{
			//Call Corresponding Method
			return $this->Client->Create();
		}
		else
		{
			//Redirect To Home
			header('Location:/'.FOLDERADD);
		}
	}

	public function delete()
	{
		if($this->Identity->Validate('clients/delete'))
		{
			return $this->Client->Delete();
		}
		else
		{
			header('Location:/'.FOLDERADD);
		}
	}

	public function edit()
	{
		if($this->Identity->Validate('clients/edit'))
		{
			return $this->Client->Edit();
		}
		else
		{
			header('Location:/'.FOLDERADD);
		}
	}

}