<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Items Controller Class
 * 
 * Controller Items Page
 *
 * @package 	CATNET
 * @subpackage 	Controllers
 * @category 	Items
 * @author 		Ivan
*/
class Items extends CI_Controller {

	/**
	 * Class constructor
	 *
	 * Load Model To Use.
	 *
	 * @return	void
	 */
	public function __construct()
	{
		parent::__construct();

		//Load Item Model
		$this->load->model('Item_model', 'Item');
		$this->load->model('Notice_model', 'Notice');
	}

	// --------------------------------------------------------------------

	/**
	 * Items - Index Page
	 *
	 * @return	mixed
	 */
	public function index(){
		//Check Permissions
		if ($this->Identity->Validate('items/index')) {
			//Load View
			$this->load->view('_shared/_administrationlayoutheader');
			$this->load->view('items/items', array('model' => $this->Item->GetItems()));
			$this->load->view(PRELAYOUTFOOTER);
		}
		else{
			//Redirect To Home
			header('Location:/'.FOLDERADD);
		}

	}

	// --------------------------------------------------------------------

	/**
	 * Items - Create Page
	 *
	 * @return	mixed
	 */
	public function create(){
		//Check Permissions
		if ($this->Identity->Validate('items/create')) {
			//Call Corresponding Method
			return $this->Item->CreateItem();
		}
		else{
			//Redirect To Home
			header('Location:/'.FOLDERADD);
		}
	}

	// --------------------------------------------------------------------

	/**
	 * Items - Delete Page
	 *
	 * @return	mixed
	 */
	public function delete(){
		//Check Permissions
		if($this->Identity->Validate('items/delete')){
			//Call Corresponding Method
			return $this->Item->DeleteItem();
		}
		else{
			//Redirect To Home
			header('Location:/'.FOLDERADD);
		}
	}

	// --------------------------------------------------------------------

	/**
	 * Items - Details Page
	 *
	 * @return	mixed
	 */
	public function details(){
		//Check Permissions
		if($this->Identity->Validate('items/details')){
			//Call Corresponding Method
			return $this->Item->DetailsItem();
		}
		else{
			//Redirect To Home
			header('Location:/'.FOLDERADD);
		}
	}

	// --------------------------------------------------------------------

	/**
	 * Items - Edit Page
	 *
	 * @return	mixed
	 */
	public function edit(){
		//Check Permissions
		if($this->Identity->Validate('items/edit')){
			//Call Corresponding Method
			return $this->Item->EditItem();
		}
		else{
			//Redirect To Home
			header('Location:/'.FOLDERADD);
		}
	}

	// --------------------------------------------------------------------

	/**
	 * Items - View Item Page
	 *
	 * @return	mixed
	 */
	public function item(){
		//Check Permissions
		if($this->Identity->Validate('items/item')){
			//Call Corresponding Method
			$this->load->view('items/item');
		}
		else{
			//Redirect To Home
			show_404();
		}
	}


	public function getitem()
	{
		//Check Permissions
		if($this->Identity->Validate('items/item')){
			//Call Corresponding Method
			return $this->Item->GetItem(false);
		}
		else{
			//Redirect To Home
			echo "error";
		}
	}

	public function addfavorite()
	{
		
		return $this->Item->AddFavorite();
	}


	public function getcreatenoticeview()
	{
		//Check Permissions
		if($this->Identity->Validate('items/item/notices/actions/create'))
		{

			if($this->Notice->AvailableToCreate($this->input->post('itemId'))){
				$view = $this->Item->GetItem(true);
				return $this->Notice->GetCreateNoticeView($view);
			}
			else{
				echo "NotAvailableToCreate";
			}
			
		}
		else
		{
			//Redirect To Home
			echo "error";
		}
	}

	public function geteditnoticeview()
	{
		//Check Permissions
		if($this->Identity->Validate('items/item/notices/actions/edit'))
		{
			$view=$this->Item->GetItem(true);
			return $this->Notice->GetEditNoticeView($view);
		}
		else
		{
			//Redirect To Home
			echo "error";
		}
	}


	public function createnotices()
	{
		//Check Permissions
		if($this->Identity->Validate('items/item/notices/actions/create'))
		{
			//Call Corresponding Method
			$itemid = $this->input->post('itemId');

			if($this->Notice->AvailableToCreate($itemid)){
				return $this->Notice->CreateNotice();
			}
			else{
				echo "Not available to create";
			}
		}
		else
		{
			//Redirect To Home
			show_404();
		}
	}


	public function editnotice()
	{
		//Check Permissions
		if($this->Identity->Validate('items/item/notices/actions/edit'))
		{
			return $this->Notice->EditNotice();
		}
		else
		{
			//Redirect To Home
			show_404();
		}
	}

	public function deleteallnotices()
	{
		//Check Permissions
		if($this->Identity->Validate('items/item/notices/actions/deleteall'))
		{
			//Call Corresponding Method
			return $this->Notice->DeleteAllNotices();
		}
		else
		{
			//Redirect To Home
			show_404();
		}
	}

	public function deletenotice()
	{
		//Check Permissions
		if($this->Identity->Validate('items/item/notices/actions/delete'))
		{
			//Call Corresponding Method
			return $this->Notice->DeleteNotice();
		}
		else
		{
			//Redirect To Home
			show_404();
		}
	}


	public function getnotices()
	{
		//Check Permissions
		if($this->Identity->Validate('items/item/notices/index'))
		{
			$view=$this->Item->GetItem(true);
			$pageNumber = $this->input->post('pageNumber');
			return $this->Notice->GetNotices($view,$pageNumber);
		}
		else{
			//Redirect To Home
			echo "error";
		}
	}

	public function getdocs()
	{
		//Check Permissions
		if($this->Identity->Validate('items/item/docs/index'))
		{
			$view=$this->Item->GetItem(true);

			$this->load->model('Doc_model', 'Doc');
			return $this->Doc->GetDocs($view);
		}
		else{
			//Redirect To Home
			echo "error";
		}
	}

	public function getuploaddataview()
	{
		//Check Permissions
		if($this->Identity->Validate('items/item/docs/actions/create'))
		{
			$view=$this->Item->GetItem(true);

			$this->load->model('Doc_model', 'Doc');
			return $this->Doc->GetUploadDataView($view);
		}
		else{
			//Redirect To Home
			echo "error";
		}
	}


	public function getcreatefolderview()
	{
		//Check Permissions
		if($this->Identity->Validate('items/item/docs/actions/createfolder'))
		{
			$view=$this->Item->GetItem(true);

			$this->load->model('Doc_model', 'Doc');
			return $this->Doc->GetCreateFolderView($view);
		}
		else{
			//Redirect To Home
			echo "error";
		}
	}

	public function geteditfolderview()
	{
		//Check Permissions
		if($this->Identity->Validate('items/item/docs/actions/editfolder'))
		{
			$view=$this->Item->GetItem(true);

			$this->load->model('Doc_model', 'Doc');
			return $this->Doc->GetEditFolderView($view);
		}
		else{
			//Redirect To Home
			echo "error";
		}
	}


	public function editfolder()
	{
		//Check Permissions
		if($this->Identity->Validate('items/item/docs/actions/editfolder'))
		{
			$this->load->model('Doc_model', 'Doc');
			return $this->Doc->EditFolder();
		}
		else{
			//Redirect To Home
			show_404();
		}
	}

	public function createfolder()
	{
		//Check Permissions
		if($this->Identity->Validate('items/item/docs/actions/createfolder'))
		{
			$this->load->model('Doc_model', 'Doc');
			return $this->Doc->CreateFolder();
		}
		else{
			//Redirect To Home
			show_404();
		}
	}



	public function geteditdocview()
	{
		//Check Permissions
		if($this->Identity->Validate('items/item/docs/actions/edit'))
		{
			$view=$this->Item->GetItem(true);

			$this->load->model('Doc_model', 'Doc');
			return $this->Doc->GetEditDocView($view);
		}
		else{
			//Redirect To Home
			echo "error";
		}
	}


	public function editdoc()
	{
		//Check Permissions
		if($this->Identity->Validate('items/item/docs/actions/edit'))
		{
			$this->load->model('Doc_model', 'Doc');
			return $this->Doc->EditDoc();
		}
		else{
			//Redirect To Home
			show_404();
		}
	}

	public function uploaddoc()
	{
		//Check Permissions
		if($this->Identity->Validate('items/item/docs/actions/create'))
		{
			$this->load->model('Doc_model', 'Doc');
			echo json_encode($this->Doc->CreateDoc());
		}
		else{
			//Redirect To Home
			show_404();
		}
	}

	public function deletefolder()
	{
		//Check Permissions
		if($this->Identity->Validate('items/item/docs/actions/deletefolder'))
		{
			$this->load->model('Doc_model', 'Doc');
			return $this->Doc->DeleteFolder();
		}
		else{
			//Redirect To Home
			show_404();
		}
	}

	public function deletedoc()
	{
		//Check Permissions
		if($this->Identity->Validate('items/item/docs/actions/delete'))
		{
			//Call Corresponding Method
			$this->load->model('Doc_model', 'Doc');
			return $this->Doc->DeleteDoc();
		}
		else
		{
			//Redirect To Home
			show_404();
		}
	}

	

	public function getimage()
	{
		//Check Permissions
		if($this->Identity->Validate('items/item/docs/actions/viewimage'))
		{

			$this->load->model('Doc_model', 'Doc');
			return $this->Doc->GetImageDoc();
		}

	}

	public function getdoc()
	{
		//Check Permissions
		if($this->Identity->Validate('items/item/docs/actions/download'))
		{
			$this->load->model('Doc_model', 'Doc');
			return $this->Doc->DownloadDocument();
		}
	}

}