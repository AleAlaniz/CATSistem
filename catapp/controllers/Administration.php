<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Administration Controller Class
 * 
 * Controller Administration Page.
 *
 * @package 	CATNET
 * @subpackage 	Controllers
 * @category 	Administration
 * @author 		Ivan
*/
class Administration extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}


	/**
	* Administration - Index Page.
	*
	* @return mixed
	*/
	public function index(){
		//Check Permissions
		if ($this->Identity->Validate('administration/index')) {
			//Load View
			$this->load->view('_shared/_administrationlayoutheader');
			$this->load->view('administration/config');
			$this->load->view(PRELAYOUTFOOTER);
		}
		else
		{
			//Redirect To Home
			header('Location:/'.FOLDERADD);
		}
	}

	public function welcomemessage()
	{
		if ($this->Identity->Validate('administration/welcomemessage')) {
			
			$query = new StdClass();
			$this->form_validation->set_rules('message', 'lang:general_welcomemessage', 'required');
			$this->form_validation->set_rules('sectionId', 'sectionId', 'required');

			if ($this->form_validation->run() == FALSE)
			{
				$query->section = NULL;
				if ($this->uri->segment(3)) {
					$sql = "SELECT sectionId, name FROM sections WHERE sectionId = ?";
					$query->section = $this->db->query($sql, $this->uri->segment(3))->row();
					if (isset($query->section)) {
						$sql = "SELECT message FROM welcomeMessages WHERE sectionId = ?";
						$welcomeMessage = $this->db->query($sql, $query->section->sectionId)->row();
						if (isset($welcomeMessage)) {
							$query->message = $welcomeMessage->message; 
						}
						else
						{
							$query->message = '';
						}
					}
				}

				$sql = "SELECT * FROM sections ORDER BY name";
				$query->sections = $this->db->query($sql)->result();

				$this->load->view('_shared/_administrationlayoutheader');
				$this->load->view('administration/welcomemessage', $query);
				$this->load->view(PRELAYOUTFOOTER);
			}
			else
			{
				$sectionId = $this->input->post('sectionId');
				
				$sql = "SELECT message FROM welcomeMessages WHERE sectionId = ?";
				$welcomeMessage = $this->db->query($sql, $sectionId)->row();

				$sql = "SELECT sectionId, name FROM sections WHERE sectionId = ?";
				$query->section = $this->db->query($sql, $sectionId)->row();

				$this->load->model('Home_model','Home');

				$resizedImages = $this->Home->resizeImages($this->input->post('message'),1024,768);
				
				if (isset($query->section)) {
					$messageInsert = array(
						'message' 	=> $resizedImages,
						'timestamp' => time(),
						'userId' 	=> $this->session->UserId,
						'sectionId' => $this->input->post('sectionId')
					);

					if (isset($welcomeMessage)) {
						$this->db->where('sectionId', $sectionId);
						$this->db->update('welcomeMessages', $messageInsert);
					}
					else {
						$this->db->insert('welcomeMessages', $messageInsert);
					}
					
					$this->session->set_flashdata('welcomeMessage', 'edit');
					header('Location:/'.FOLDERADD.'/administration/welcomemessage/');
				}
				else
				{
					show_404();
				}
			}
		}
		else
		{
			show_404();
		}
	}

	// --------------------------------------------------------------------

	/**
	* Administration - Panel Message Config Page.
	*
	* @return mixed
	*/
	public function panelmessage()
	{
		//Check Permissions
		if ($this->Identity->Validate('administration/panelmessage')) {
			//Set Form Validation Rules
			$this->form_validation->set_rules('message', 'lang:administration_panelmessage_title', 'required');
			
			//Check Form Validation
			$query = new StdClass();
			if ($this->form_validation->run() == FALSE)
			{
				$query->section = NULL;
				if ($this->uri->segment(3)) {
					$sql = "SELECT sectionId, name FROM sections WHERE sectionId = ?";
					$query->section = $this->db->query($sql, $this->uri->segment(3))->row();
					if (isset($query->section)) {
						$sql = "SELECT message FROM panelMessages WHERE sectionId = ?";
						$panelMessage = $this->db->query($sql, $query->section->sectionId)->row();
						if (isset($panelMessage)) {
							$query->message = $panelMessage->message; 
						}
						else
						{
							$query->message = '';
						}
					}
				}

				$sql = "SELECT * FROM sections ORDER BY name";
				$query->sections = $this->db->query($sql)->result();

				$this->load->view('_shared/_administrationlayoutheader');
				$this->load->view('administration/panelmessage', $query);
				$this->load->view(PRELAYOUTFOOTER);

			}
			else
			{
				//insert New Panel Message
				$sql = "SELECT sectionId, name FROM sections WHERE sectionId = ?";
				$query->section = $this->db->query($sql, $this->input->post('sectionId'))->row();
				if (isset($query->section)) {

					$data = array(
						'message' 	=> $this->input->post('message'),
						'userId' 	=> $this->session->UserId,
						'timestamp' => time(),
						'sectionId' => $this->input->post('sectionId')
					);

					$sql = "SELECT message FROM panelMessages WHERE sectionId = ?";
					$panelMessage = $this->db->query($sql, $query->section->sectionId)->row();

					if(isset($panelMessage))
					{
						$this->db->where('sectionId', $query->section->sectionId);
						$this->db->update('panelMessages', $data);
					}

					else
					{
						$this->db->insert('panelMessages', $data);
					}

					$this->session->set_flashdata('panelMessage', 'edit');
					header('Location:/'.FOLDERADD.'/administration/panelmessage/');
				}
				else
				{
					show_404();
				}
				
			}
		}
		else
		{
			//Redirect to Home
			header('Location:/'.FOLDERADD);
		}
	}

	// --------------------------------------------------------------------

	/**
	* Administration - Important Information Config Page.
	*
	* @return mixed
	*/
	public function alert()
	{
		//Check Permissions
		if ($this->Identity->Validate('administration/alert')) {

			//Set Form Validation Rules
			$this->form_validation->set_rules('request', 'request', 'required');
			$this->form_validation->set_rules('sectionId', 'sectionId', 'required');
			
			//Check Form Validation
			if ($this->form_validation->run() == FALSE)
			{
				//Create Empty Variable For The View
				$query = new StdClass();

				//Set Division to NULL
				$query->sectionSelected = NULL;

				//Check Parameter Exist
				if ($this->uri->segment(3)) {

					//Get Division WHERE Parameter
					$sql = "SELECT * FROM sections WHERE sectionId = ?";
					$query->sectionSelected = $this->db->query($sql, $this->uri->segment(3))->row();

					//Check Division Exists
					if ($query->sectionSelected != NULL) {
						//Get Important Information For Division
						$sql = "SELECT * FROM alerts WHERE sectionId = ?";
						$lastmessage = $this->db->query($sql, $query->sectionSelected->sectionId)->last_row();
						
						//Check Important Information Cannot be NULL
						if ($lastmessage != NULL) {
							//Set Message
							$query->message = $lastmessage->message; 
						}
					}

				}

				//Get Divisions
				$sql = "SELECT * FROM sections ORDER BY name";
				$query->sections = $this->db->query($sql)->result();

				//Load View
				$this->load->view('_shared/_administrationlayoutheader');
				$this->load->view('administration/alert', $query);
				$this->load->view(PRELAYOUTFOOTER);
			}
			else
			{
				//Insert New Important Information
				$messageInsert = array(
					'message' => $this->input->post('message'),
					'sectionId' => $this->input->post('sectionId'),
					'timestamp' => time(),
					'userId' => $this->session->UserId
				);
				$this->db->insert('alerts', $messageInsert);
				
				//Set Flash Data Message
				$this->session->set_flashdata('alert', 'edit');

				//Redirect to Important Information Config Page
				header('Location:/'.FOLDERADD.'/administration/alert');
			}
		}
		else
		{
			//Redirect to Home
			header('Location:/'.FOLDERADD);
		}
	}

	
	// --------------------------------------------------------------------

	/**
	* Administration - Important Information Delete Page.
	*
	* @return mixed
	*/
	public function deletealert(){

		if($this->Identity->Validate('administration/alert')){

			if ($this->uri->segment(3)) {

				$sql = "SELECT * FROM alerts WHERE alertId = ?";
				$query = $this->db->query($sql,$this->uri->segment(3))->row();

				if (isset($query))
				{
					insert_audit_logs('alerts','DELETE',$query);

					$objectDelete = array(
						'alertId' => $query->alertId
					);
					$this->db->delete('alerts', $objectDelete);

					header('Location:/'.FOLDERADD);
				}
				else
				{
					show_404();
				}
			}
			else
			{
				show_404();
			}
		}
		else
		{
			header('Location:/'.FOLDERADD);
		}
	}


	// --------------------------------------------------------------------


	public function completedata()
	{
		if ($this->Identity->Validate('administration/completedata')) {
			
			$roles = $this->input->post('roles[]');

			if (!($roles && is_array($roles))) {
				$sql 	= "SELECT name, roleId FROM roles ORDER BY name";
				$roles 	= $this->db->query($sql)->result();
				$this->load->view('_shared/_administrationlayoutheader');
				$this->load->view('administration/completedata', array('roles' => $roles));
				$this->load->view(PRELAYOUTFOOTER);
			}
			else {
				
				date_default_timezone_set('America/Argentina/Buenos_Aires');
				$fecha = gmdate("Y-m-d H:i:s",time());
				$dt = new DateTime($fecha);
				$hour = $dt->format('H') - 3;
				if ($hour <= 14){
					$updateDelayTurn = 't';
					$updateDelayTime = 21600;
				}
				else{
					$updateDelayTurn = 'm';
					$updateDelayTime = 64800;
				}

				$time = time();
				$rolesCount = count($roles);

				for ($i = 0; $i < $rolesCount; $i++) { 

					if ($this->input->post('delete')) {
						$sql 	= 'SELECT userId FROM users WHERE roleId = ?';
						$users 	=  $this->db->query($sql, $roles[$i])->result();
						$usersCount = count($users);

						for ($j=0; $j < $usersCount; $j++) { 
							$objectToDelete = array(
								'userId' => $users[$j]->userId
							);
							$this->db->delete('completeDataUsers', $objectToDelete);
						}
					}
					else{
						$time = $time + 120;
						$sql 	= 'SELECT u.userId,ucd.turn 
						FROM users AS u  
						LEFT JOIN (SELECT userId, completeDataUserId FROM completeDataUsers) as cd ON u.userId = cd.userId
						INNER JOIN userComplementaryData ucd ON u.userId = ucd. userId 
						WHERE u.roleId = ? && cd.completeDataUserId IS NULL && u.active = 1';
						$users 	=  $this->db->query($sql, $roles[$i])->result();

						$usersCount = count($users);

						$clusterSize = MAX_USERS_QUEUE;
						$clusters = ceil($usersCount/$clusterSize);
						
						for ($k=1; $k <= $clusters; $k++) {

							$currentClusterSize = $clusterSize;
							if ($k == $clusters){
								$currentClusterSize = ($usersCount % $clusterSize) ;
							}

							for ($j = 0; $j < $currentClusterSize; $j++) { 

								$addTime = ($users[$j+($clusterSize*($k-1))]->turn == $updateDelayTurn)? $updateDelayTime : 0;

								$objectInsert = array(
									'timestamp' => $time+(350*($k-1))+$addTime,
									'userId' 	=> $users[$j+($clusterSize*($k-1))]->userId
								);
								$this->db->insert('completeDataUsers', $objectInsert);
							}
						}
						
					}
				}	

				$this->session->set_flashdata('flashMessage', 'edit');

				header('Location:/'.FOLDERADD.'/administration/completedata');
			}
		}
		else {
			header('Location:/'.FOLDERADD);
		}
	}


	// --------------------------------------------------------------------



}