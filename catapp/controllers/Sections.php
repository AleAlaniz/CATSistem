<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Sections Controller Class
 * 
 * Controller Sections Page
 *
 * @package 	CATNET
 * @subpackage 	Controllers
 * @category 	Sections
 * @author 		Ivan
*/
class Sections extends CI_Controller {

	/**
	 * Class constructor
	 *
	 * Load Model To Use.
	 *
	 * @return	void
	 */
	public function __construct()
	{
		parent::__construct();

		//Load Section Model
		$this->load->model('Section_model', 'Section');
		$this->load->model('Notice_model', 'Notice');
		$this->load->model('Doc_model', 'Doc');
	}

	// --------------------------------------------------------------------

	/**
	 * Sections - Index Page
	 *
	 * @return	mixed
	 */
	public function index(){
		//Check Permissions
		if ($this->Identity->Validate('sections/index')) 
		{
			//Load View
			$this->load->view('_shared/_administrationlayoutheader');
			$this->load->view('sections/sections',array('model' =>  $this->Section->GetSections()));
			$this->load->view(PRELAYOUTFOOTER);
		}
		else
		{
			//Redirect To Home
			show_404();
		}
	}
	public function getSections()
	{
		$data = $this->Section->GetSectionsForComponent();
		echo json_encode($data);		
	}
		// --------------------------------------------------------------------

	/**
	 * Sections - View Section Page
	 *
	 * @return	mixed
	 */
	public function section()
	{
		//Check Permissions
		if($this->Identity->Validate('sections/section'))
		{
			//Call Corresponding Method
			$this->load->view('sections/section');
			//return $this->Section->GetSection();
		}
		else
		{
			//Redirect To Home
			show_404();
		}
	}


	public function getsubsections()
	{
		//Check Permissions
		if($this->Identity->Validate('sections/section'))
		{
			//Call Corresponding Method
			return $this->Section->GetSubSections();
		}
		else
		{
			//Redirect To Home
			show_404();
		}
	}



	public function getsubitems()
	{
		//Check Permissions
		if($this->Identity->Validate('sections/section'))
		{
			//Call Corresponding Method
			return $this->Section->GetSection();
		}
		else
		{
			//Redirect To Home
			show_404();
		}
	}



	public function getmoreitems()
	{
		//Check Permissions
		if($this->Identity->Validate('sections/section'))
		{
			//Call Corresponding Method
			return $this->Section->GetMoreItems();
		}
		else
		{
			//Redirect To Home
			show_404();
		}
	}

	// --------------------------------------------------------------------

	/**
	 * Sections - Create Page
	 *
	 * @return	mixed
	 */
	public function create()
	{
		//Check Permissions
		if ($this->Identity->Validate('sections/create')) 
		{
			//Call Corresponding Method
			return $this->Section->CreateSection();
		}
		else
		{
			//Redirect To Home
			header('Location:/'.FOLDERADD);
		}
	}

	// --------------------------------------------------------------------

	/**
	 * Sections - Delete Page
	 *
	 * @return	mixed
	 */
	public function delete()
	{
		//Check Permissions
		if($this->Identity->Validate('sections/delete'))
		{
			//Call Corresponding Method
			return $this->Section->DeleteSection();
		}
		else
		{
			//Redirect To Home
			header('Location:/'.FOLDERADD);
		}
	}

	// --------------------------------------------------------------------

	/**
	 * Sections - Details Page
	 *
	 * @return	mixed
	 */
	public function details()
	{
		//Check Permissions
		if($this->Identity->Validate('sections/details'))
		{
			//Call Corresponding Method
			return $this->Section->DetailsSection();
		}
		else
		{
			//Redirect To Home
			header('Location:/'.FOLDERADD);
		}
	}

	// --------------------------------------------------------------------

	/**
	 * Sections - Edit Page
	 *
	 * @return	mixed
	 */
	public function edit()
	{
		//Check Permissions
		if($this->Identity->Validate('sections/edit'))
		{
			//Call Corresponding Method
			return $this->Section->EditSection();
		}
		else
		{
			//Redirect To Home
			header('Location:/'.FOLDERADD);
		}
	}

	public function getunread()
	{
		if($this->Identity->Validate('sections/index')){
			$data = new StdClass();
			$data->status = 'ok';
			$sections = $this->Section->GetSections();
			
			foreach ($sections as $section) {
				$totalunread = 0;
				$subsectionUnread = $this->Notice->GetUnread($section->sectionId);
				$docUnread = $this->Doc->GetUnread($section->sectionId);
				
				$c_doc = count($docUnread);
				
				for ($i=0; $i < $c_doc; $i++) { 
					$subsectionUnread[] = $docUnread[$i];
				}	
				
				for ($i=0; $i < count($subsectionUnread); $i++) { 
					
					for ($j=0; $j < count($subsectionUnread); $j++) { 
						if($subsectionUnread[$i]->itemId == $subsectionUnread[$j]->itemId){
							if($i <> $j){
								$subsectionUnread[$i]->c += $subsectionUnread[$j]->c;
								array_splice($subsectionUnread,$j,1);
							}
						}
					}
					$totalunread+= $subsectionUnread[$i]->c;	
				}

				$object = new StdClass();
				$object->sectionId	= $section->sectionId;
				$object->totalunread = $totalunread;
				$object->subsectionUnread = $subsectionUnread;
				
				$data->unreads[] = $object;	
			}
			
			echo escapeJsonString($data,FALSE);
		}
		else{
			echo '{"status":"invalid"}';
		}
	}

	public function addtoview()
	{
		$res = 'error';
		$itemId = $this->input->post('itemId');
		if(isset($itemId))
		{
			$this->Notice->AddToView($itemId);
			$this->Doc->AddToView($itemId);
			$res = 'success';
		}
		echo $res;
	}
}