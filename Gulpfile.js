var gulp 		= require('gulp');
var minify 		= require('gulp-minify');
var rename 		= require('gulp-rename');
var cleanCSS 	= require('gulp-clean-css');
var bs 			= require('browser-sync').create();

gulp.task('serve', function() {
	bs.init({
		proxy : 'http://localhost/CATSistem'
	});

	gulp.watch('catapp/views/**/*.php'	, ['reload']);
	gulp.watch('dev/js/**/*.js' 		, ['minify:js'	, 'reload']);
	gulp.watch('dev/css/*.css' 			, ['minify:css'	, 'reload']);
});


gulp.task('minify:js', function() {
	gulp
	.src('dev/js/**/*.js')
	.pipe(minify({
		ext:{
			src : '.js',
			min : '.min.js'
		},
		//noSource : true,
		ignoreFiles: ['*.back.js','*.min.js']
	}).on('error', minifyJsError))
	.pipe(gulp.dest('catapp/libraries/script'));
});

gulp.task('minify:css', function() {
	return gulp
	.src('dev/css/*.css')
	.pipe(cleanCSS({compatibility : "ie8"}))
	.pipe(rename({
		suffix: '.min'
	}))
	.pipe(gulp.dest('catapp/libraries/css'));
});


gulp.task('reload', function() {
	bs.reload();
});

gulp.task('default', ['serve']);

function minifyJsError() {
	console.log(' \x1b[41m\x1b[37m ERROR \x1b[0m Un archivo javascript contiene errores :(');
}
